<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Title
      |--------------------------------------------------------------------------
      |
      | The default title of your admin panel, this goes into the title tag
      | of your page. You can override it per page with the title section.
      | You can optionally also specify a title prefix and/or postfix.
      |
     */

    'title' => env('APP_NAME', 'Global Village'),
    'title_prefix' => 'Admin-',
    'title_postfix' => '',
    /*
      |--------------------------------------------------------------------------
      | Logo
      |--------------------------------------------------------------------------
      |
      | This logo is displayed at the upper left corner of your admin panel.
      | You can use basic HTML here if you want. The logo has also a mini
      | variant, used for the mini side bar. Make it 3 letters or so
      |
     */
    'logo' => '<b>GV</b>',
    'logo_mini' => 'GV',
    /*
      |--------------------------------------------------------------------------
      | Skin Color
      |--------------------------------------------------------------------------
      |
      | Choose a skin color for your admin panel. The available skin colors:
      | blue, black, purple, yellow, red, and green. Each skin also has a
      | light variant: blue-light, purple-light, purple-light, etc.
      |
     */
    'skin' => 'blue',
    /*
      |--------------------------------------------------------------------------
      | Layout
      |--------------------------------------------------------------------------
      |
      | Choose a layout for your admin panel. The available layout options:
      | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
      | removes the sidebar and places your menu in the top navbar
      |
     */
    'layout' => null,
    /*
      |--------------------------------------------------------------------------
      | Collapse Sidebar
      |--------------------------------------------------------------------------
      |
      | Here we choose and option to be able to start with a collapsed side
      | bar. To adjust your sidebar layout simply set this  either true
      | this is compatible with layouts except top-nav layout option
      |
     */
    'collapse_sidebar' => false,
    /*
      |--------------------------------------------------------------------------
      | Control Sidebar (Right Sidebar)
      |--------------------------------------------------------------------------
      |
      | Here we have the option to enable a right sidebar.
      | When active, you can use @section('right-sidebar')
      | The icon you configured will be displayed at the end of the top menu,
      | and will show/hide de sidebar.
      | The slide option will slide the sidebar over the content, while false
      | will push the content, and have no animation.
      | You can also choose the sidebar theme (dark or light).
      | The right Sidebar can only be used if layout is not top-nav.
      |
     */
    'right_sidebar' => false,
    'right_sidebar_icon' => 'fas fa-cogs',
    'right_sidebar_theme' => 'dark',
    'right_sidebar_slide' => true,
    /*
      |--------------------------------------------------------------------------
      | URLs
      |--------------------------------------------------------------------------
      |
      | Register here your dashboard, logout, login and register URLs. The
      | logout URL automatically sends a POST request in Laravel 5.3 or higher.
      | You can set the request to a GET or POST with logout_method.
      | Set register_url to null if you don't want a register link.
      |
     */
    'dashboard_url' => 'admin/',
    'logout_url' => 'admin/logout',
    'logout_method' => null,
    'login_url' => 'admin/login',
    'register_url' => '',
    'password_email_url' => 'admin/password/email',
    'password_reset_url' => 'admin/password/reset',
    /*
      |--------------------------------------------------------------------------
      | Menu Items
      |--------------------------------------------------------------------------
      |
      | Specify your menu items to display in the left sidebar. Each menu item
      | should have a text and a URL. You can also specify an icon from Font
      | Awesome. A string instead of an array represents a header in sidebar
      | layout. The 'can' is a filter on Laravel's built in Gate functionality.
     */
    'menu' => [
        ['header' => 'main_navigation'],
        [
            'text' => 'Admin users',
            'url' => 'admin/users',
            'icon' => 'users',
            'guard' => ['admin', 'super-admin'],
            'can' => 'user_read',
        ],
        // [
        //     'text' => 'Season',
        //     'url' => 'admin/season',
        //     'guard' => ['admin', 'super-admin'],
        //     'icon' => 'users',
        //     'can' => 'user_read',
        // ],
        [
            'text' => 'Packs',
            'url' => 'admin/packs',
            'guard' => ['admin', 'super-admin'],
            'icon' => 'users',
            'can' => 'pack_read',
        ],
        [
            'text' => 'Import Wonderpass',
            'url' => 'admin/import/wonderpass',
            'guard' => ['admin', 'super-admin'],
            'icon' => 'users',
            'can' => 'user_read',
        ],
        [
            'text' => 'Offers',
            'url' => 'admin/offers',
            'guard' => ['admin', 'super-admin'],
            'icon' => 'users',
            'can' => 'user_read',
        ],
        [
            'text' => 'Shop Cart Transactions',
            'icon' => 'fa fa-fw fa-shopping-cart',
            'can' => 'shoptransaction_read',
            'submenu' => [
                [
                    'text' => 'Shopping Transaction (Call)',
                    'url' => 'admin/shop-cart-transaction-cc',
                    'icon' => 'fa fa-fw fa-shopping-cart',
                    'guard' => ['admin', 'super-admin'],
                    'can' => 'shoptransaction_read',
                ],
                [
                    'text' => 'Shopping Transaction (Finance)',
                    'url' => 'admin/shop-cart-transaction',
                    'icon' => 'fa fa-fw fa-shopping-cart',
                    'guard' => ['admin', 'super-admin'],
                    'can' => 'shoptransaction_read',
                ],
            ]
        ],
        /* [
            'text' => 'Transactions History',
            'icon' => 'fa fa-fw fa-shopping-cart',
            'can' => 'shoptransaction_read',
            'submenu' => [
                [
                    'text' => 'Shopping Transaction (Call)',
                    'url' => 'admin/shop-cart-history-transaction-cc',
                    'icon' => 'fa fa-fw fa-shopping-cart',
                    'guard' => ['admin', 'super-admin'],
                    'can' => 'shoptransaction_read',
                ],
                [
                    'text' => 'Shopping Transaction (Finance)',
                    'url' => 'admin/shop-cart-history-transaction',
                    'icon' => 'fa fa-fw fa-shopping-cart',
                    'guard' => ['admin', 'super-admin'],
                    'can' => 'shoptransaction_read',
                ],
            ]
        ], */

        /*[
            'text' => 'Shopping Archive',
            'url' => 'admin/shop-cart-transaction-old',
            'icon' => 'fa fa-fw fa-shopping-cart',
            'guard' => ['admin', 'super-admin'],
            'can' => 'shoptransaction_read',
        ],*/
        [
            'text' => 'Car Plate Edit Info',
            'url' => 'admin/car-plate-info',
            'icon' => 'fa fa-fw fa-car',
            'guard' => ['admin', 'super-admin'],
            'can' => 'carplateinfo_read',
        ],
        [
            'text' => 'Parking Transaction',
            'url' => 'admin/parking-transaction',
            'icon' => 'fa fa-fw fa-car',
            'guard' => ['admin', 'super-admin'],
            'can' => 'parking transaction_read',
        ],

        [
            'text' => 'Fitness',
            'icon' => 'fa fa-fw fa-running',
            'any_can' => 'fitness_read,fitness_daily tracks',
            'submenu' => [
                [
                    'text' => 'Seasons',
                    'url' => 'admin/fitness-season',
                    'icon' => 'fa fa-fw fa-tree ',
                    'can' => 'fitness_read',
                    'active' => ['admin/fitness-season', "admin/fitness-season/*"]
                ],
                [
                    'text' => 'Daily Tracks',
                    'url' => 'admin/fitness-tracks/daily',
                    'can' => 'fitness_daily tracks',
                ]

            ]
        ],
        [
            'text' => 'Vehicle Count',
            'icon' => 'fa fa-fw fa-car',
            'any_can' => 'vehicle count_read,vehicle count_update',
            'submenu' => [
                [
                    'text' => 'Parking Area',
                    'url' => 'admin/parking-area',
                    'can' => 'vehicle count_read',
                    'active' => ['admin/parking-area', "admin/parking-area/*"]
                ],
                [
                    'text' => 'Services',
                    'url' => 'admin/parking-services',
                    'can' => 'vehicle count_read',
                    'active' => ['admin/parking-services', "admin/parking-services/*"]
                ],
                [
                    'text' => 'Locations',
                    'url' => 'admin/vehicle-plocations',
                    'can' => 'vehicle count_read',
                    'active' => ['admin/vehicle-plocations', "admin/vehicle-plocations/*"]
                ],
                [
                    'text' => 'Set vehicles In',
                    'url' => 'admin/vehicle-inside',
                    'can' => 'vehicle count_update',
                    'active' => ['admin/vehicle-inside']
                ]
            ]
        ],
        /*[
            'text' => 'Shake and Win',
            'icon' => 'fa fa-fw fa-mobile',
            'can' => 'shake and win_read',
            'submenu' => [
                [
                    'text' => 'Campaigns',
                    'url' => 'admin/snw-campaigns',
                    'active' => ['admin/snw-campaigns', "admin/snw-campaigns/*", "admin/snw-prizes?*", "admin/snw-prizes/*"]
                ]
            ]
        ],*/
        [
            'text' => 'Ramadan Majlis',
            'icon' => 'fa fa-university',
            'any_can' => 'Ramadan Majlis_table read,Ramadan Majlis_slot read,Ramadan Majlis_program read,Ramadan Majlis_booking summary,Ramadan Majlis_onsite collection',
            'submenu' => [
                [
                    'text' => 'Manage Slots',
                    'route' => 'admin.slots.index',
                    'can' => 'Ramadan Majlis_slot read',
                    'active' => ['admin/slots', "admin/slots/*"]
                ],
                [
                    'text' => 'Manage Tables',
                    'route' => 'admin.tables.index',
                    'can' => 'Ramadan Majlis_table read',
                    'active' => ['admin/tables', "admin/tables/*"]
                ],
                [
                    'text' => 'Manage Programs',
                    'route' => 'admin.programs.index',
                    'can' => 'Ramadan Majlis_program read',
                    'active' => ['admin/programs', "admin/programs/*"]
                ],
                [
                    'text' => 'Booking Summary',
                    'route' => 'admin.program.day.report',
                    'can' => 'Ramadan Majlis_booking summary',
                    'active' => ['admin/program-report']
                ],
                [
                    'text' => 'Find Bookings',
                    'route' => 'admin.find.majlis.bookings',
                    'can' => 'Ramadan Majlis_find bookings',
                    'active' => ['admin/find-majlis-bookings']
                ],
                [
                    'text' => 'Onsite Collections',
                    'route' => 'admin.program.onsite.collections',
                    'can' => 'Ramadan Majlis_onsite collection',
                    'active' => ['admin/program-onsite-collections']
                ]
            ]
        ],
        [
            'text' => 'Cabana',
            'icon' => 'fa fa-ship',
            'can' => 'Cabana_read',
            'submenu' => [
                [
                    'text' => 'Available Days',
                    'url' => 'admin/cabana-days',
                    'active' => ['admin/cabana-days', 'admin/cabana-days/*']
                ],
                [
                    'text' => 'Available Products',
                    'url' => 'admin/cabana-products',
                    'active' => ['admin/cabana-products', 'admin/cabana-products/*']
                ]
            ]
        ],
        [
            'text' => 'FIFA',
            'icon' => 'fa fa-duotone fa-futbol',
            'can' => 'Fifa_read',
            'submenu' => [
                [
                    'text' => 'Import Matches',
                    'route' => 'admin.fifa-matches.create',
                    'can' => 'Fifa_create',
                    'active' => ['admin/fifa-matches/create']
                ],
                [
                    'text' => 'Manage Matches',
                    'route' => 'admin.fifa-matches.index',
                    'can' => 'Fifa_read',
                    'active' => ['admin/fifa-matches']
                ],
            ]
        ],
        [
            'text' => 'Logs',
            'url' => 'admin/logs',
            'guard' => ['admin', 'super-admin'],
            'icon' => 'fa fa-tasks',
            'can' => 'log_read',
        ],
        [
            'text' => 'Notifications',
            'url' => 'admin/notifications',
            'guard' => ['admin', 'super-admin'],
            'icon' => 'fa fa-bell',
            'can' => 'notifications_read',
        ],
        [
            'text' => 'VIP Pack Activation Settings',
            'url' => 'admin/current-season',
            'guard' => ['admin', 'super-admin'],
            //'icon' => 'users',
            'can' => 'pack_read',
        ],
        
        [
            'text' => 'Plate prefix not to updated in designa',
            'url' => 'admin/removed-plate-prefixes',
            'guard' => ['admin', 'super-admin'],
            //'icon' => 'users',
            'can' => 'removed plate prefix_read',
        ],
        [
            'text' => 'Configuration',
            'url' => 'admin/configuration',
            'guard' => ['admin', 'super-admin'],
            'can' => 'user_read',
            //'icon' => 'users',
        ],
        [
            'text' => 'Plate Prefix Settings',
            'url' => 'admin/plate-prefix',
            'guard' => ['admin', 'super-admin'],
            'can' => 'user_read',
            //'icon' => 'users',
        ],
        [
            'text' => 'Product Id Settings',
            'url' => 'admin/pack-product-config',
            'guard' => ['admin', 'super-admin'],
            'can' => 'user_read',
            //'icon' => 'users',
        ],
        [
            'text' => 'Dynamic texts',
            'url' => 'admin/dynamic-texts',
            'guard' => ['admin', 'super-admin'],
            //'icon' => 'users',
            'can' => 'dynamic text_read',
        ],
        [
            'text' => 'Email templates',
            'url' => 'admin/email-templates',
            'guard' => ['admin', 'super-admin'],
            //'icon' => 'users',
            'can' => 'email template_read',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Menu Filters
      |--------------------------------------------------------------------------
      |
      | Choose what filters you want to include for rendering the menu.
      | You can add your own filters to this array after you've created them.
      | You can comment out the GateFilter if you don't want to use Laravel's
      | built in Gate functionality
      |
     */
    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SearchFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        //JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
        App\Myadmin\MyMenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\LangFilter::class,
    ],
    /*
      |--------------------------------------------------------------------------
      | Plugins Initialization
      |--------------------------------------------------------------------------
      |
      | Configure which JavaScript plugins should be included. At this moment,
      | DataTables, Select2, Chartjs and SweetAlert are added out-of-the-box,
      | including the Javascript and CSS files from a CDN via script and link tag.
      | Plugin Name, active status and files array (even empty) are required.
      | Files, when added, need to have type (js or css), asset (true or false) and location (string).
      | When asset is set to true, the location will be output using asset() function.
      |
     */
    'plugins' => [
        [
            'name' => 'Datatables',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    //'location' => '//cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.js',
                    'location' => '//cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    //'location' => '//cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.css',
                    'location' => '//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/responsive/2.2.6/css/responsive.dataTables.min.css',
                ],
            ],
        ],
        [
            'name' => 'Select2',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css',
                ],
            ],
        ],
        [
            'name' => 'Chartjs',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => 'js/Chart.js',
                ],
            ],
        ],
        [
            'name' => 'Sweetalert2',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//unpkg.com/sweetalert/dist/sweetalert.min.js',
                ],
            ],
        ],
        [
            'name' => 'Pace',
            'active' => true,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => true,
                    'location' => 'css/pace-theme-loading-bar.min.css',
                ],
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => 'js/pace.min.js',
                ],
            ],
        ],
    ],
];
