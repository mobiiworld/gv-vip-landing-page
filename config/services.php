<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, SparkPost and others. This file provides a sane default
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    //'designa_base_url' => (env('APP_ENV') == 'production') ? 'http://192.168.2.39/webservice/ServiceOperation.asmx' : 'https://extapp.gvnet.local:8443/webservice/serviceOperation.asmx', // discussion with Ananth 15 sep 2022 : designa has same url for dev, ppr & production - changed by Amrutha
    'designa_base_url' => (env('APP_ENV') == 'production') ? 'http://192.168.2.39/webservice/ServiceOperation.asmx' : 'http://192.168.2.39/webservice/ServiceOperation.asmx',


];
