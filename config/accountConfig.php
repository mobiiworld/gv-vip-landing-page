<?php

return [
    //"pprUrl" => "http://ppr-gvsnapp.gvnet.local/service?format=json",
    "pprUrl" => (env('APP_ENV') == 'production') ? "http://gvsnapp.gvnet.local/service?format=json" : "http://ppr-gvsnapp.gvnet.local/service?format=json",
    // "workStationId" => "EB830644-577C-C4D3-4275-016E7EED58DE",
    // "CodeAliasTypeCode" => "VIP-PACKNO",
    // "carCodeAliasTypeCode" => "VIPcarplate",
    // "SaveAccountEntityType" => 15,
    // "SaveCodeAliasesEntityType" => 21,
    // "SaleAccountType" => 2,
    // "accountMetaDataListKey" => [
    // 	0 => "firstName",
    // 	1 => "lastName",
    // 	2 => "ageGroup",
    // 	3 => "gender",
    // 	4 => "nationality",
    // 	5 => "mobile",
    // 	6 => "email",
    // 	7 => "keepUpdated",
    // 	8 => "termsAgree",
    // ],
    // "accountMetaDataList" => [
    // 	"firstName" => "FT1",
    // 	"lastName" => "FT3",
    // 	"ageGroup" => "AgeGroup",
    // 	"gender" => "FT19",
    // 	"nationality" => "NtnltyTxt",
    // 	"mobile" => "MOBNO",
    // 	"email" => "FT21",
    // 	"keepUpdated" => "keepupdated",
    // 	"termsAgree" => "termsagree",
    // ],
    "ageGroup" => [
        0 => "Below18",
        1 => "18-24",
        2 => "25-39",
        3 => "40-59",
        4 => "60Plus",
    ],
    "ripleyCountUrl" => "http://192.168.2.54:8033/api/ProductPlayBalance/GetProductLists",
    "parkingCount" => ['url' => "https://192.168.4.10/api/v3", 'apiKey' => 'co4swiaEUnf1oAoXeuinivOE2ID6E944']
];
