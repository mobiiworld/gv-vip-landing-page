<?php

return [
    'status' => [
        'Enable' => 1,
        'Disable' => 2
    ],
    'types' => [
        //'vip' => 'Vip',
        //'normal' => 'Normal'
        'standard' => 'Standard',
        'premium' => 'Premium'
    ],
    'smoking_types' => [
        1 => 'Yes',
        2 => 'No'
    ],
    'seat_types' => [
        '2' => 2,
        '4' => 4,
        '6' => 6
    ],
    'parking_transaction_status' => [
        1 => 'Created',
        2 => 'Initiated',
        3 => 'Authenticate',
        4 => 'Paid',
    ],
    'pack_categories' => [
        'silver'=>100,
        'gold'=>101,
        'platinum'=>102,
        'platinum plus'=>103,
        'mini'=>10,
        'mini pack'=>11,
        'complimentary' => 104,
        'private' => 105,
        'diamond' => 106
    ],
    'pack_categories_colorcodes' => [
        'silver'=>'#8EAF83',
        'gold'=> '#000000',
        'platinum'=> '#00496D',
        'platinum plus'=>' #00496D',
        /*'mini'=>'#2D8DBC',
        'mini pack'=>'#2D8DBC',
        'complimentary' => '#F26F22',
        'private' => '#020202',*/
        'mini'=>'#8EAF83',
        'mini pack'=>'#8EAF83',
        'complimentary' => '#8EAF83',
        'private' => '#5d2940',
        'diamond' => '#353535'
    ],
    'pack_categories_background_colorcodes' => [
        'silver'=>'#8EAF83',
        'gold'=> '#000000',
        'platinum'=> '#00496D',
        'platinum plus'=>' #00496D',
        /*'mini'=>'#F0FAFF',
        'mini pack'=>'#F0FAFF',
        'complimentary' => '#ffe7da',
        'private' => '#E6E6E6',*/
        'mini'=>'#8EAF83',
        'mini pack'=>'#8EAF83',
        'complimentary' => '#8EAF83',
        'private' => '#5d2940',
        'diamond' => '#353535'
    ],
    'pack_product_slug' => [
        'entry_ticket' => 'Entry Ticket',
        'parking_voucher' => 'Parking Voucher',
        'vip_wonder_pass' => 'Vip Wonder Pass'
    ],
    'pack_categories_wonderpass_class' => [
        'silver'=>'silver',
        'gold'=>'gold',
        'platinum'=>'platinum',
        'platinum plus'=>'platinum',
        'mini'=>'general',
        'mini pack'=>'general',
        'complimentary' => 'complimentary',
        'private' => 'private',
        'diamond' => 'diamond'
    ],
];
