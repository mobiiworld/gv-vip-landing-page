<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
 Route::get('/activate', function() {
     return Redirect::to('en');
 });

Route::get('/', function() {
    return Redirect::to('en',301);
});

Route::get('testMails', ['as' => 'test-mails', 'uses' => 'Web\TestMailController@index']);
Route::get('testPass', ['as' => 'test-mails', 'uses' => 'Web\TestMailController@testPass']);

Route::get('test-exp', ['as' => 'test-mails', 'uses' => 'Web\TestMailController@export']);

Route::group(['prefix' => '{lang?}', 'middleware' => ['web', 'setlocale'], 'where' => ['lang' => 'en|ar']], function () {
    Route::get('/', 'Web\HomeController@index')->name('home');
    Route::post('check-activation-code', 'Web\HomeController@checkActivationCode')->name('check.activationCode');
    Route::post('generate-otp', 'Web\HomeController@generateOtp')->name('generateOtp');
    Route::post('get-emirates', 'Web\HomeController@getEmirates')->name('get.emirates');
    Route::post('get-plate-prefix', 'Web\HomeController@getPlatePrefix')->name('get.platePrefix');
    Route::post('activate-pack', 'Web\HomeController@activatePack')->name('pack.activate');
    Route::get('my-activated-pack/{id}', 'Web\HomeController@myActivatedPack')->name('my.activated.pack');
    Route::post('pack-offers', 'Web\HomeController@packOffers')->name('pack.offers');

    Route::get('offer-download/{id}', 'Web\HomeController@packOfferDownload')->name('pack-offer-download');
    Route::get('offer-hide/{id}', 'Web\HomeController@hideOffer')->name('hideOffer');

    Route::get('test', 'Web\HomeController@test')->name('pack.offers-test');

    Route::get('generate-qrcode', 'Web\HomeController@generateTestQrCode')->name('pack.qr-test');

    Route::post('load-popup-content', 'Web\HomeController@load_popup_content')->name('load-popup-content');
    Route::post('manage-car-detail', 'Web\HomeController@manage_car_detail')->name('manage-car-detail');

    Route::post('add-more-car', 'Web\HomeController@addMoreCar')->name('addMoreCar');


    Route::get('testSaleforce', 'Web\HomeController@testSaleforce')->name('testSaleforce');
    Route::get('testSaleforceUrl', 'Web\HomeController@testSaleforceUrl')->name('testSaleforceUrl');

});
