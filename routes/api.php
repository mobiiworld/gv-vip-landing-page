<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

//if(env('APP_ENV') == 'local'){
  Route::get('delete-fitness-users/{id}', 'Api\V1\User\FitnessController@deleteUser');
//}

Route::group(['middleware' => ['assign.guard'], 'namespace' => 'Api\V1\User'], function () {
    Route::post('register', 'UserController@register');
    Route::post('login', 'UserController@login');
    Route::post('user-packs', 'HomeController@userPacks');
    Route::post('user-pack-details', 'HomeController@userPackDetails');
    Route::post('add-car', 'HomeController@addCar');
    Route::post('edit-car', 'HomeController@editCar');
    //Route::post('check-activation-code', 'HomeController@checkActivationCode');
    //Route::post('activate-pack', 'HomeController@activatePack');
    Route::post('update-offer-consume', 'HomeController@updateOfferConsume');
    Route::post('ticket-usage-details', 'HomeController@getPackTicketUsageDetails');
    Route::post('user-highest-pack', 'HomeController@userHighestPack');
    Route::post('user-highest-pack-v1', 'HomeController@userHighestPackV1');
    Route::get('plate-codes', 'VipController@getPlateCodes');
    Route::post('generate-otp', 'VipController@generateOtp');
   //Route::post('validate-otp', 'VipController@validateOtp');
    Route::post('check-activation-code-v1', 'VipController@checkActivationCode');
    Route::post('activate-pack-v1', 'VipController@activatePack');

    Route::any('get-apple-pass', 'PassController@getApplePass');
    Route::post('get-wonderpass-card', 'PassController@getCard');
    Route::post('get-wonderpass-card-details', 'PassController@getCardDetails');

    //Program slot booking
    Route::post('get-slots', 'SlotBookingController@index')->name('api.get.slots');
    Route::post('create-slot-booking', 'SlotBookingController@createBooking')->name('api.create.slot.booking');
    Route::post('upcoming-program-bookings', 'SlotBookingController@ucProgramBookings');
    //Route::post('booking-status-change', 'SlotBookingController@bookingStatus');
    Route::post('get-booking-status', 'SlotBookingController@getStatus');
    Route::post('release-booking', 'SlotBookingController@releaseBooking');
    //Route::post('get-program-data', 'SlotBookingController@getProgramData');
    Route::post('get-user-table-count', 'UserController@getUserTableCount');

    //Fitness APIs
    Route::get('user-challenges/{id}', 'FitnessController@getChallenges');
    Route::post('join-today-challenge', 'FitnessController@joinToday');
    Route::post('end-today-challenge', 'FitnessController@endChallenge');
    Route::post('sync-steps', 'FitnessController@syncSteps');
    //Vehicle count APIs
    Route::get('vehicle-count', 'VehiclePlocationController@getCount');
    Route::post('save-my-parking', 'VehiclePlocationController@saveMyParking');
    Route::post('get-my-parking', 'VehiclePlocationController@getMyParking');
    Route::post('remove-my-parking', 'VehiclePlocationController@removeMyParking');
    Route::post('vehicle-count-v1', 'VehiclePlocationControllerV1@getCountV1');
    Route::post('save-my-parking', 'VehiclePlocationController@saveMyParking');
    Route::post('get-my-parking', 'VehiclePlocationController@getMyParking');
    Route::post('remove-my-parking', 'VehiclePlocationController@removeMyParking');
    Route::get('notifications/{wants}', 'NotificationController@index');
    //User Cars
    Route::get('list-user-cars', 'UserCarController@listCars');
    Route::post('add-user-car', 'UserCarController@addCar');
    Route::post('delete-user-car', 'UserCarController@deleteCar');
    Route::post('cabana-slots', 'CabanaController@getSlots');


    Route::post('get-cabana-remaining-highest-pack', 'HomeController@getCabanaHighestPack');
    Route::post('fifa-matches', 'FifaMatchesController@index');
});
Route::group(['middleware' => ['assign.guard:api', 'jwt.auth'], 'namespace' => 'Api\V1\User'], function () {
    Route::post('details', 'UserController@details');
});
