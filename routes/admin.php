<?php

/*
  |--------------------------------------------------------------------------
  | Admin Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register Admin routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::get('/', function () {
    return Redirect::to('admin/login');
});
Route::group(['namespace' => 'Auth', 'as' => 'admin.'], function () {
    Route::get('/login', 'AdminLoginController@showLoginForm')->name('login');
    Route::post('/login', 'AdminLoginController@login')->name('login.submit');
    Route::post('/logout', 'AdminLoginController@logout')->name('logout');

    //admin password reset routes
    Route::post('/password/email', 'AdminForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/password/reset', 'AdminForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/reset', 'AdminResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'AdminResetPasswordController@showResetForm')->name('password.reset');
});

Route::group(['middleware' => 'isAdmin', 'namespace' => 'Admin', 'as' => 'admin.'], function () {
    Route::get('/', 'HomeController@index')->name('dashboard');
    Route::resource('users', 'AdminController');
    Route::any('users/dt', 'AdminController@datatable')->name('users.datatable');
    Route::get('user/profile', 'AdminController@profile')->name('user.profile');
    Route::post('user/profile', 'AdminController@profilePost')->name('user.profile.post');
    Route::post('user/profile/avatardelete', 'AdminController@avatarDelete')->name('user.avatardelete');

    Route::resource('roles', 'RoleController');
    Route::resource('permissions', 'PermissionController');

    //Slot
    Route::resource('slots', 'SlotController');
    Route::any('slots/datatable', 'SlotController@datatable')->name('slots.datatable');
    Route::post('slots/changeStatus', 'SlotController@changeStatus')->name('slots.changeStatus');

    //Table
    Route::resource('tables', 'TableController');
    Route::any('tables/datatable', 'TableController@datatable')->name('tables.datatable');
    Route::post('tables/changeStatus', 'TableController@changeStatus')->name('tables.changeStatus');

    //Import
    Route::get('import/{type}', 'ImportController@import')->name('import');
    Route::post('import-packs}', 'ImportController@importPacks')->name('import.packs');
    Route::post('import-wonderpass}', 'ImportController@importWonderpass')->name('import.wonderpass');
    Route::post('import-offers}', 'ImportController@importOffers')->name('import.offers');

    //shopcart transaction data
    Route::resource('shop-cart-transaction', 'ShopCartTransactionController');
    Route::any('shop-cart-transaction/dt', 'ShopCartTransactionController@datatable')->name('shop-cart-transaction.datatable');

    // Route::get('shop-cart-transaction-export', 'ShopCartTransactionController@export');

    //shopcart transaction data
    Route::get('shop-cart-transaction-cc', 'ShopCartTransactionController@getCallCenterData');
    Route::any('shop-cart-transaction-cc/dt', 'ShopCartTransactionController@getCCDatatable')->name('shop-cart-transaction-cc.datatable');

    Route::get('shop-cart-transaction-old', 'ShopCartTransactionController@getArchieve');
    Route::any('shop-cart-transaction-old/dt', 'ShopCartTransactionController@arhieveDatatable')->name('shop-cart-transaction-old.datatable');

    /**
     * History Transactions
     */
    //shopcart transaction data
    Route::resource('shop-cart-history-transaction', 'ShopCartTransactionHistoryController');
    Route::any('shop-cart-history-transaction/dt', 'ShopCartTransactionHistoryController@datatable')->name('shop-cart-history-transaction.datatable');

    // Route::get('shop-cart-transaction-export', 'ShopCartTransactionHistoryController@export');

    //shopcart transaction data
    Route::get('shop-cart-history-transaction-cc', 'ShopCartTransactionHistoryController@getCallCenterData');
    Route::any('shop-cart-history-transaction-cc/dt', 'ShopCartTransactionHistoryController@getCCDatatable')->name('shop-cart-history-transaction-cc.datatable');

    Route::get('shop-cart-history-transaction-old', 'ShopCartTransactionHistoryController@getArchieve');
    Route::any('shop-cart-history-transaction-old/dt', 'ShopCartTransactionHistoryController@arhieveDatatable')->name('shop-cart-history-transaction-old.datatable');
    /**
     * End History Transactions
     */

    Route::get('car-plate-info', 'CarPlateInfoController@index')->name('car-plate-info');
    Route::any('car-plate-info/dt', 'CarPlateInfoController@datatable')->name('car-plate-info.datatable');

    Route::post('car-plate-sync-designa', 'CarPlateInfoController@syncDesigna')->name('car-info.sync.designa');

    Route::post('load-popup-content', 'AdminCommonController@load_popup_content')->name('load-popup-content');
    Route::post('show-audit-log/{type}', 'AdminCommonController@show_audit_logs')->name('show-audit-log');

    Route::any('packs/datatable', 'PackController@datatable')->name('packs.datatable');
    Route::post('packs/activate', 'PackController@activate')->name('packs.activate');
    Route::post('packs/syncDrupal', 'PackController@syncDrupal')->name('packs.syncDrupal');
    Route::post('packs/syncAllDrupal', 'PackController@syncAllDrupal')->name('packs.syncAllDrupal');
    Route::post('packs-sync-designa', 'PackController@syncDesigna')->name('packs.sync.designa');
    Route::get('packs/edit-user/{id}', 'PackController@editUser')->name('packs.editUser');
    Route::post('packs/save-user/{id}', 'PackController@saveUser')->name('packs.saveUser');
    Route::get('packs/remove-user/{id}', 'PackController@removeUser')->name('packs.removeUser');
    Route::get('packs/export', 'PackController@export')->name('packs.export');
    Route::get('packs/updateTestPacks', 'PackController@updateTestPacks')->name('packs.updateTestPacks');
    Route::get('packs/removeOffers/{id}', 'PackController@removeOffers')->name('packs.removeOffers');
    Route::get('packs/blockUser/{id}', 'PackController@blockUser')->name('packs.blockUser');
    Route::get('packs/removetestOffers/{from}/{to}', 'PackController@removetestOffers')->name('packs.removetestOffers');
    Route::get('packs/rm-urs', 'PackController@removeAllSeasonUsers')->name('packs.removeAllUsers');

    Route::get('packs/updateUserCars', 'PackController@updateUserCars')->name('packs.updateUserCars');

    Route::resource('packs', 'PackController');
    Route::resource('dynamic-texts', 'DynamicTextController');
    Route::resource('email-templates', 'EmailTemplateController');
    Route::resource('removed-plate-prefixes', 'RemovedPlatePrefixController');

    Route::get('plate-prefix', 'ApiConfigurationController@platePrefix')->name('platePrefix');
    Route::get('plate-prefix/{type}/{code}/edit', 'ApiConfigurationController@platePrefixEdit')->name('platePrefixEdit');
    Route::post('plate-prefix/{type}/{code}/update', 'ApiConfigurationController@platePrefixUpdate')->name('platePrefixUpdate');
    Route::resource('configuration', 'ApiConfigurationController');

    Route::any('parking-transaction/dt', 'ParkingTransactionController@datatable')->name('parking.transaction.datatable');
    Route::resource('parking-transaction', 'ParkingTransactionController');

    Route::resource('user-cars', 'UserCarController')->only('create', 'store', 'edit', 'update');

    Route::get('logs', 'AdminCommonController@getLog')->name('getLog');
    Route::any('getAllLogs', 'AdminCommonController@getAllLogs')->name('getAllLogs');

    Route::post('offers/change-status/{type}/{status}', 'OfferController@changeStatus')->name('offers.changeStatus');
    Route::get('offers/edit/{parentName}/{merchanteName}', 'OfferController@edit')->name('offers.edit');
    Route::post('offers/update/{parentName}/{merchanteName}', 'OfferController@update')->name('offers.update');
    Route::any('offers/datatable', 'OfferController@datatable')->name('offers.datatable');
    Route::get('offers/updateExistingOrder', 'OfferController@updateExistingOrder')->name('offers.updateExistingOrder');
    Route::get('offers', 'OfferController@index')->name('offers.index');
    Route::get('offers/update-existing', 'OfferController@updateExisting')->name('offers.updateExisting');

    //Programs
    Route::any('programs/datatable', 'ProgramController@datatable')->name('programs.datatable');
    Route::any('programs/{id}/slots/datatable', 'ProgramSlotController@datatable')->name('programs.slots.datatable');
    Route::get('programs/{id}/slots/export', 'ProgramSlotController@export')->name('programs.slots.export');
    Route::post('programs/{id}/slots/changeStatus', 'ProgramSlotController@changeStatus')->name('programs.slots.changeStatus');
    Route::resource('programs/{id}/slots', 'ProgramSlotController', ['as' => 'programs']);
    Route::post('programs/changeStatus', 'ProgramController@changeStatus')->name('programs.changeStatus');
    Route::resource('programs', 'ProgramController');
    Route::any('book-programs-slots/{id}', 'ProgramController@getSlots')->name('programs.slots.book');
    Route::post('book-programs-confirm', 'ProgramController@cofirmSlotBooking')->name('programs.book.confirm');
    Route::post('cancel-booking-slot', 'ProgramSlotController@cancelSlotBooking')->name('programs.booked-slot.cancel');
    Route::post('release-booking-slot', 'ProgramSlotController@releaseSlotBooking')->name('programs.booked-slot.release');
    Route::post('get-tables-by-type', 'TableController@getTablesByType')->name('tables.get.by.type');
    Route::any('program-report', 'ProgramReportController@index')->name('program.day.report');
    Route::post('program-report-details', 'ProgramReportController@show')->name('program.day.report.details');
    Route::any('program-onsite-collections', 'MajlisBookingController@onsiteIndex')->name('program.onsite.collections');
    Route::post('program-onsite-collection-details', 'MajlisBookingController@onsiteShow')->name('program.onsite.collection.details');
    Route::post('program-change-table', 'ProgramReportController@changeTable')->name('program.change.table');
    Route::post('program-change-table-store', 'ProgramReportController@storeChangedTable')->name('program.changed.table.store');
    Route::post('program-change-date', 'ProgramReportController@changeDate')->name('program.change.date');
    Route::get('program-release-slots', 'ProgramReportController@getReleaseSlots')->name('program.get.release.slots');
    Route::post('program-get-qrcodes', 'ProgramReportController@getQrcodes')->name('program.get.qrcodes');
    Route::get('find-majlis-bookings', 'MajlisBookingController@findBookings')->name('find.majlis.bookings');
    Route::post('majlis-bookings-datatable', 'MajlisBookingController@datatable')->name('majlis.bookings.datatable');
    Route::get('onsite-collections/export', 'MajlisBookingController@export')->name('program.onsite.collections.export');

    Route::get('promocode-update', 'DbUpdateController@promocode')->name('promocode');

    //Season
    Route::get('season/packs/{id}', 'SeasonController@packs')->name('season.packs');
    Route::get('season/offers/{id}', 'SeasonController@offers')->name('season.offers');
    Route::get('season/download/{id}', 'SeasonController@download')->name('season.download');
    Route::get('season/downloadOffer/{id}', 'SeasonController@downloadOffer')->name('season.downloadOffer');
    Route::any('season/packdatatable', 'SeasonController@packdatatable')->name('season.packdatatable');
    Route::any('season/datatable', 'SeasonController@datatable')->name('season.datatable');
    Route::post('season/changeStatus', 'SeasonController@changeStatus')->name('season.changeStatus');
    Route::post('season/changePackActivation', 'SeasonController@changePackActivation')->name('season.changePackActivation');
    Route::resource('season', 'SeasonController');
    Route::get('current-season', 'SeasonController@currentSeason')->name('current-season');
    Route::post('current-season-update/{id}', 'SeasonController@currentSeasonUpdate')->name('current-season-update');

    Route::any('pack-product-config/datatable', 'PackProductConfigController@datatable')->name('pack-product-config.datatable');
    Route::resource('pack-product-config', 'PackProductConfigController');

    Route::resource('fitness-season', 'FitnessSeasonsController');
    Route::any('fitness-season/dt', 'FitnessSeasonsController@datatable')->name('fitness-season.datatable');
    Route::post('fitness-season-status', 'FitnessSeasonsController@publish')->name('fitness-season.fitness-season-status');
    Route::post('post-sortable', 'FitnessSeasonsController@sortable_update')->name('fitness-season.post-sortable');
    Route::get('delete-fitness-season-image/{id}', 'FitnessSeasonsController@delete_fitness_season_image')->name('fitness-season.delete-fitness-season-image');
    Route::get('fitness-tracks/{cat}', 'FitnessTrackController@index')->name('fitness-track.index');
    Route::any('fitness-daily-tracks-dt', 'FitnessTrackController@dailyTracks')->name('fitness.daily-tracks.dt');
    Route::post('fitness-season-images-url', 'FitnessSeasonsController@updateImagesType');
    Route::post('fitness-season-images-setdefault', 'FitnessSeasonsController@setDefaultImage');
    Route::post('fitness-mark-collected-tracks/{type}', 'FitnessTrackController@markAsCollected')->name('fitness.mark.track');
    Route::get('fitness-track-delete', 'FitnessTrackController@getDeletePage')->name('fitness-track-delete');
    Route::get('fitness-track-delete-record', 'FitnessTrackController@deleteRecords')->name('fitness-track-delete-record');

    Route::any('vehicle-plocations-dt', 'VehiclePlocationController@datatable')->name('vehicle-plocations.datatable');
    Route::post('vehicle-plocations-status', 'VehiclePlocationController@changeStatus')->name('vehicle-plocations.status');
    Route::post('vehicle-plocations-sync', 'VehiclePlocationController@syncCount')->name('vpl.sync.count');
    Route::post('clear-user-parkings-cronstatus', 'VehiclePlocationController@clearUserParkingsCronStatus')->name('clear.user.parkings.cronstatus');
    Route::get('parking-area', 'VehiclePlocationController@parkingArea')->name('parking.area');
    Route::post('parking-area', 'VehiclePlocationController@saveParkingArea')->name('parking.area.save');
    Route::get('clear-user-parkings', 'VehiclePlocationController@clearUserParkings')->name('clear.user.parkings');
    Route::resource('vehicle-plocations', 'VehiclePlocationController');
    Route::get('del-vpl-image/{id}', 'VehiclePlocationController@delImage')->name('vpl.delete.image');
    Route::get('vehicle-inside', 'VehiclePlocationController@getVinside')->name('vpl.get.vinside');
    Route::post('vehicle-inside', 'VehiclePlocationController@postVinside')->name('vpl.post.vinside');
    Route::any('parking-services/dt', 'ParkingServicesController@datatable')->name('parking.services.datatable');
    Route::resource('parking-services', 'ParkingServicesController');

    /*Route::any('snw-campaigns/dt', 'SnwCampaignController@datatable')->name('snw-campaigns.datatable');
    Route::resource('snw-campaigns', 'SnwCampaignController');
    Route::post('snw-json-status', 'SnwCampaignController@changeStatus')->name('snw.status');
    Route::any('snw-prizes/dt', 'SnwPrizeController@datatable')->name('snw-prizes.datatable');
    Route::resource('snw-prizes', 'SnwPrizeController');*/
    Route::any('notifications/dt', 'NotificationController@datatable')->name('notifications.datatable');
    Route::resource('notifications', 'NotificationController');


    Route::get('offer-link-update', 'OfferController@offerLinks')->name('offer.links');

    Route::any('offersdelete/datatable', 'OfferDeleteController@datatable')->name('offersdelete.datatable');
    Route::resource('offersdelete', 'OfferDeleteController');

    Route::any('saleforce/datatable', 'SalesForceLogController@datatable')->name('saleforce.datatable');
    Route::resource('saleforce', 'SalesForceLogController');
    Route::get('saleforce-pending', 'SalesForceLogController@pending')->name('saleforce.pending');
    Route::any('cabana-days/datatable', 'CabanaController@datatable')->name('cabana-days.datatable');
    Route::post('cabana-days/changeStatus', 'CabanaController@changeStatus')->name('cabana-days.changeStatus');
    Route::resource('cabana-days', 'CabanaController');
    Route::any('cabana-products/datatable', 'CabanaProductController@datatable')->name('cabana-products.datatable');
    Route::post('cabana-products/changeStatus', 'CabanaProductController@changeStatus')->name('cabana-products.changeStatus');
    Route::resource('cabana-products', 'CabanaProductController');

    Route::get('fifa-matches/export', 'FifaMatchesController@export')->name('fifa-matches.export');
    Route::any('fifa-matches/datatable', 'FifaMatchesController@datatable')->name('fifa-matches.datatable');
    Route::resource('fifa-matches', 'FifaMatchesController');


    Route::get('debug-booking/order_confirmed', 'DebugBookingController@index')->name('debug-booking');
    Route::get('debug-booking/salecode', 'DebugBookingController@salecodeMissing')->name('debug-booking-salecode');
});
