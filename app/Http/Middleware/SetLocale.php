<?php

namespace App\Http\Middleware;

use Closure;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->lang;
        $locale = ($locale == 'ar') ? 'ar' : 'en';
        app()->setLocale($locale);
        return $next($request);
    }
}
