<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\ApiController;

class AssignGuard {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {
        $api = new ApiController();
        $language = $api->setApiLanguage($request->header('language', 'en'));
        if (empty($guard)) {
            if ($request->hasHeader('Authorization')) {
                if ($request->bearerToken() != env('API_TOKEN')) {
                    return systemResponse(trans('api.token.invalid'));
                }

                return $next($request);
            } else {
                return systemResponse(trans('api.token.notexist'));
            }
        } else {
            auth()->shouldUse($guard);
        }
        return $next($request);
    }

}
