<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Validator;
use App\Pack;
use App\Country;
use App\CarCountry;
use App\PlateCategory;
use App\Emirate;
use App\User;
use App\UserCar;
use App\NewUserCar;
use App\Wonderpass;
use App\PartnerOffer;
use DNS1D;
use PDF;
use App\Jobs\ProcessSync;
use Carbon\Carbon;
use App\Traits\GvpTraits;
use App\Traits\CarTraits;
use App\Traits\AccountActivationTraits;
use App\Traits\DrupalTraits;
use App\Traits\PackBlockTraits;
use App\Traits\SalesForceTraits;
use App\Traits\VipTraits;

class HomeController extends Controller {

    use GvpTraits;
    use CarTraits,DrupalTraits,SalesForceTraits,
        AccountActivationTraits,PackBlockTraits,VipTraits;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
       
        $locale = app()->getLocale();
        $countries = Country::select('id', "name_{$locale} as name", 'code', 'dial_code')->get();
        $car_countries = CarCountry::select('id', "name_{$locale} as name", 'code')->get();
        $plate_categories = PlateCategory::select('id', "name_{$locale} as name")->get();
        $emirates = Emirate::select('id', "name_{$locale} as name", 'code')->get();
        $uae = Country::select('id', "name_{$locale} as name", 'code', 'dial_code')->where('dial_code','+971')->first();
        $uaeId = $uae->id;
        $season = \App\Season::where('status',1)->first();

        $message = trans('web.pack.notfound');
        $popup_title = '';
        $popup_image = '';
        $is_activate_pack = (!empty($season) && $season->pack_activation_enabled) ? true : false;
        if(!empty($season) && !empty($season->pack_activation_disbaled_msg_en)){
            $message = (app()->getLocale() == 'ar' && !empty($season->pack_activation_disbaled_msg_ar)) ? $season->pack_activation_disbaled_msg_ar : $season->pack_activation_disbaled_msg_en;
            $field = 'pack_activation_disabled_title_'.$locale;
            $popup_title = $season->$field;
            $popup_image = $season->pack_activation_disabled_image;
        }
        $show_register_popup = (!empty($season) && $season->show_register_popup) ? true : false;
        return view('web.index', compact('countries', 'car_countries', 'plate_categories','uaeId','emirates','season','message','popup_title','popup_image','is_activate_pack','show_register_popup'));
    }


    public function generateTestQrCode(Request $request) {
      $code = $request->code;
      return view('web.qr-generate', compact('code'));

    }

    public function test() {
        $pdf = PDF::loadView('web.pdf.offer-download', array());
        return $pdf->download('invoice.pdf');
        echo DNS1D::getBarcodeSVG('4445645656', 'PHARMA2T');
        exit;
    }

    public function checkActivationCode(Request $request) {
        $rules = [
            'vip_pack_number' => 'required',
            'activation_code' => 'required',
            'email' => 'required|email:rfc,dns,strict,filter',
            'otp' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) {
            return response()->json(["status" => false, "message" => trans('web.required_fields')]);
        }

        $season = \App\Season::where('status',1)->first();
        if(empty($season)){            
            return response()->json(["status" => false, "message" => trans('web.pack.notfound')]);
        }
        if(!$season->pack_activation_enabled){
            $message = trans('web.pack.notfound');
            if(!empty($season->pack_activation_disbaled_msg_en)){
                $message = (app()->getLocale() == 'ar' && !empty($season->pack_activation_disbaled_msg_ar)) ? $season->pack_activation_disbaled_msg_ar : $season->pack_activation_disbaled_msg_en;
            }
            
            return response()->json(["status" => false, "message" => $message, 'show_alert' => true]);
        }

        $seasonId =(!(empty($season))) ? $season->id : NULL;
        $pack = Pack::where('pack_number', $request->vip_pack_number)
                ->where('activation_code', $request->activation_code)
                ->where('season_id',$seasonId)
                ->where('status', 1)
                ->first();
         $carCount = 0;
         $otp_id = NULL;
        if ($pack) {
            $otp_response = $this->validateGuestOtp($request);
            
            if($otp_response['statusCode'] != 1000){
                
                return response()->json(["status" => false, "message" => trans('passwords.otp.validate_failed')]);
            }
            $otp_id = $otp_response['otp_id'];
            $drupal_user_id = NULL;
            if ($pack->used) {
                return response()->json(["status" => false, "message" => trans('web.pack.used') ]);
            }
            $prevSeasonOrder = $season->ordering - 1;
            $prevSeason = \App\Season::where('status',0)->where('ordering',$prevSeasonOrder)->first();
            $user = $car = $otherCars = [];
            $carhtml = $emirate_html =  $prefix_html = '';
            $lastAddedCnt = 0;
            $otherCars = $carData = [];
           

            $user = $this->getDrupalUser($request->email);
            $drupal_user_id = (isset($user['drupal_user_id'])) ? $user['drupal_user_id'] : NULL;
            if(!empty($user)){
                $user['user_mobile'] = (isset($user['user_mobile']) && !empty($user['user_mobile']) && substr(trim($user['user_mobile']), 0, 1) === '+') ? $user['user_mobile'] : ((!empty($user['user_mobile'])) ?  "+".$user['user_mobile'] : '') ;
            }

            if(!empty($drupal_user_id)){
                //To list the cars not used in this season : $carData = not used in this season, $otherCars = all other cars
                
                $otherCars = \App\NewUserCar::select('id','plate_number','country_id','emirate_id','plate_prefix','type')->where('drupal_user_id',$drupal_user_id);
                $otherCars = $otherCars->get();
                //dd(\DB::getQueryLog());
                
            }
            //dd($otherCars,$carData);
            $lastAddedCnt += count($otherCars) + count($carData) ;
            $carCount = count($carData) + count($otherCars);
            if(count($carData) > 0 || count($otherCars) > 0 ){
                $carhtml =  view('web.more_cars',compact('otherCars', 'lastAddedCnt', 'carData'))->render();
            }

            $add_car_text = ((count($carData) + count($otherCars)) > 0) ? trans('content.registered_Car_or_new') :  trans('content.add_new_car') ;
            
            return response()->json(["status" => true, "car_activation_allowed" => $pack->car_activation_allowed,'user'=>$user,'car'=>$car, 'carhtml' => $carhtml,'carCount' => $carCount,'emirate_html'=>$emirate_html,'prefix_html'=>$prefix_html, 'add_car_text' => $add_car_text, 'otp_id' => $otp_id]);
        }
        return response()->json(["status" => false, "message" => trans('web.pack.notfound')]);
    }

    public function addMoreCar(Request $request){
        $otherCars = [];
        $carData = [];
        $lastAddedCnt = $request->lastAddedCnt;
        $more = new \StdClass;
        $more->type = $request->car_type;
        $more->country_id = $request->car_country;
        $more->emirate_id = $request->emirate;
        $more->plate_prefix = $request->plate_prefix;
        $more->plate_number = $request->plate_number;
        $carhtml =  view('web.more_cars',compact('otherCars', 'lastAddedCnt', 'more','carData'))->render();
        return response()->json(["status" => true,  'carhtml' => $carhtml]);
    }

    public function getEmirates(Request $request) {
        $locale = app()->getLocale();
        $emirates = Emirate::select('id', "name_{$locale} as name", 'code')
                ->where('country_id', $request->id)
                ->get();
        return view('web.emirates', compact('emirates'));
    }

    public function getPlatePrefix(Request $request) {
        $prefixes = json_decode(getPprefixes($request));
        return view('web.prefixes', compact('prefixes'));

        /* $prefixes = [];
          if ($prefix) {
          switch ($prefix->plate_prefix) {
          case 'A to Z':
          $prefixes = range('A', 'Z');
          break;
          case '1 to 9':
          $prefixes = range(1, 9);
          break;
          case '1 to 99':
          $prefixes = range(1, 99);
          break;
          }
          }
          return view('web.prefixes', compact('prefixes')); */
    }

    public function activatePack(Request $request) {
        $locale = app()->getLocale();
        $uae = Country::select('id')->where('dial_code','+971')->first();
        $uaeId = $uae->id;
        $rules = [
            'vip_pack_number' => 'required',
            'activation_code' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email:rfc,dns,strict,filter',
            //'confirm_email' => 'required',
            'mobile_dial_code' => 'required',
            'mobile' => 'required',
            //'gender' => 'required',
            //'nationality' => 'required',
            //'residence' => 'required',
            //'dob' => 'required',
            'car_country.*' => 'required',
            'plate_prefix.*' => 'required',
            'plate_number.*' => 'required',
           // 'emirate.*' => 'required_if:car_country,1',
            'g-recaptcha-response' => 'required',
            //'emirate_residence' => 'required_if:residence,'.$uaeId,
            'otp_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        //dd($request->all());
        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return response()->json(["status" => false, "message" => trans('web.required_fields'),'extra' => $errorMessage]);
        }

        // if ($request->email != $request->confirm_email) {
        //     return response()->json(["status" => false, "message" => trans('web.email_not_matched')]);
        // }

        if (empty($request->terms)) {
            return response()->json(["status" => false, "message" => trans('web.read_terms')]);
        }
        if(empty($request->registered_car) && empty($request->registered_car_old)){
             return response()->json(["status" => false, "message" => trans('web.required_fields')]);
        }
        if ((!empty($request->dob)) && time() < strtotime('+18 years', strtotime($request->dob))) {
            return response()->json(["status" => false, "message" => trans('web.dob_error')]);
        }

        $secretKey = env('CAPTCHA_SERVER_KEY');

        $captcha = $_POST['g-recaptcha-response'];

        // post request to server
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) . '&response=' . urlencode($captcha);
        $response = file_get_contents($url);
        $responseKeys = json_decode($response, true);

        if (empty($responseKeys["success"])) {           
            return response()->json(["status" => false, "message" => trans('web.captcha_error')]);
        }



        $locale = app()->getLocale();
        $season = \App\Season::where('status',1)->first();

        if(empty($season)){            
            return response()->json(["status" => false, "message" => trans('web.pack.notfound')]);
        }
        if(!$season->pack_activation_enabled){
            $message = trans('web.pack.notfound');
            if(!empty($season->pack_activation_disbaled_msg_en)){
                $message = (app()->getLocale() == 'ar' && !empty($season->pack_activation_disbaled_msg_ar)) ? $season->pack_activation_disbaled_msg_ar : $season->pack_activation_disbaled_msg_en;
            }
            return response()->json(["status" => false, "message" => $message]);
        }


        $seasonId =(!(empty($season))) ? $season->id : NULL;
        $pack = Pack::where('pack_number', $request->vip_pack_number)
                ->where('activation_code', $request->activation_code)
                ->where('status', 1)
                ->where('season_id',$seasonId)
                ->first();
        if ($pack) {
            $user_otp = \App\UserOtp::where('id',$request->otp_id)->where('email',$request->email)->first();
            if(empty($user_otp)){
                
                return response()->json(["status" => false, "message" => trans('passwords.otp.unauthorized')]);
            }
            
            if ($pack->used) {
                return response()->json(["status" => false, "message" => trans('web.pack.used')]);
            }
            $userAddedcars =  0;
            //check the car_activation_allowed is matching the user request
            if(!empty($request->registered_car)){
                $userAddedcars += count(array_filter($request->registered_car));
            }
            if(!empty($request->registered_car_old)){
                $userAddedcars += count(array_filter($request->registered_car_old));
            }
            
            if ($userAddedcars > $pack->car_activation_allowed) {
                return response()->json(["status" => false, "message" => trans('web.pack.car_allowed', ['count' => $pack->car_activation_allowed]), "car_activation_allowed" => $pack->car_activation_allowed]);
            }

            try {
                $user_otp->delete();

                $user = User::where('email', $request->email)->where("season_id",$seasonId)->first();
                if(empty($user)){
                    $user = new User();
                    $user->email = $request->email;
                }
                
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;                
                $user->mobile_dial_code = $request->mobile_dial_code;
                $user->mobile = str_replace(' ', '', $request->mobile);
                $user->gender = $request->gender;
                $user->dob = (!empty($request->dob)) ? date('Y-m-d', strtotime($request->dob)) : NULL;
                $user->nationality_id = $request->nationality;
                $user->residence_id = $request->residence;
                $user->emirate_id = ($request->residence == $uaeId) ? $request->emirate_residence : NULL;
                //$user->pack_id = $pack->id;
                $user->season_id = $seasonId;
                $user->recieve_updates = $request->has('agree_offers') ? true : false;
                $user->device_type = 'web';
                $user->save();

                $userPack = new \App\UserPack();
                $userPack->user_id = $user->id;
                $userPack->pack_id = $pack->id;
                $userPack->save();
                $user->user_pack_id = $userPack->id;
                $pack->used = true;
                $pack->save();
                $drupal_user_id = $user->drupal_user_id;
                if(empty($drupal_user_id)){
                    $duser = $this->getDrupalUser($request->email);
                    $drupal_user_id = (isset($duser['drupal_user_id'])) ? $duser['drupal_user_id'] : NULL;
                }

                //Save user cars
                if(!empty($request->registered_car_old)){
                    foreach($request->registered_car_old as $ek=>$ev){
                        $carId = !empty($request->car_id[$ek]) ? $request->car_id[$ek] : '';
                        
                        if(!empty($carId)){
                            $car = \App\NewUserCar::where('id',$carId)->where('drupal_user_id',$drupal_user_id)->first(); 
                            if(!empty($car)){
                                \App\UserCarPack::firstOrCreate([
                                    'pack_id' => $pack->id,
                                    'user_car_id' => $carId
                                ]);
                            }
                            
                        }
                        
                    }
                }
                if(!empty($request->registered_car)){
                    foreach ($request->registered_car as $k => $v) {
                        if (!empty($v)) {
                            $country_id = !empty($request->car_country[$k]) ? $request->car_country[$k] : '';
                            $emirate_id = NULL;
                            if (!empty($request->emirate[$k])) {
                                $emirate_id = !empty($request->emirate[$k]) ? $request->emirate[$k] : '';
                            }
                            $type = 1;
                            if (!empty($request->car_type[$k])) {
                                $type = $request->car_type[$k];
                            }                        
    
                            $plate_prefix = !empty($request->plate_prefix[$k]) ? $request->plate_prefix[$k] : '';
                            $plate_number = !empty($request->plate_number[$k]) ? $request->plate_number[$k] : '';

                            if(!empty($drupal_user_id)){
                                $user_car = \App\NewUserCar::firstOrCreate([
                                    'drupal_user_id' => $drupal_user_id,                                    
                                    'type' => $type,
                                    'country_id' => $country_id,
                                    'emirate_id' => $emirate_id,                
                                    'plate_prefix' => $plate_prefix,
                                    'plate_number' => $plate_number,
                                ]);
                            }else{
                                $user_car = new NewUserCar();                                 
                            }
                            $user_car->user_id = $user->id;                        
                            $user_car->drupal_user_id = $drupal_user_id;                     
                            $user_car->type = $type;
                            $user_car->country_id = $country_id;
                            $user_car->emirate_id = $emirate_id;
                            $user_car->plate_prefix = $plate_prefix;
                            $user_car->plate_number = $plate_number;
                            $user_car->save();
                               
                            $userCarPack = \App\UserCarPack::firstOrCreate([
                                'pack_id' => $pack->id,
                                'user_car_id' => $user_car->id
                            ]);
                            
                        }
                    }
                }
                
            } catch (\Exception $e) {
                // echo $e->getMessage();
                // exit;
                \Log::error($e->getMessage());
                return response()->json(["status" => false, "message" => 'Invalid data entered']);
            }
            $pType = trans("web.pack_cat_for_popup.{$pack->category}");
            $name = $user->first_name . ' ' . $user->last_name;
           
            $success_message = trans('web.activation_success',['pack_type' => $pType, 'user_name' => $name, 'pack_number' => $pack->pack_number]);            
            $success_data = \App\DynamicText::whereIn('slug',['web-activation-success-title','web-activation-success-message','web-activation-success-image','web-activation-success-button'])->get()->keyBy('slug')->toArray();
            
            if(!empty($success_data['web-activation-success-message'])){
                $success_message = $success_data['web-activation-success-message']['text_'.$locale];
                $success_message = str_replace(':user_name', $name, $success_message);
                $success_message = str_replace(':pack_type', $pType, $success_message);
                $success_message = str_replace(':pack_number', $pack->pack_number, $success_message);
            }
            

            $data = [
                'name' => $name,
                'pack_type' => $pType,
                'pack_id' => $pack->pack_number,
                'pack_url' => route('my.activated.pack', [$locale, $userPack->id]),
                'success_message' => $success_message,
            ];
           

            try {

                $agree_offers = $request->has('agree_offers') ? true : false;
                $sales_force_ref_id = $this->vipSubmissionToSalesForce($user,$pack,$agree_offers,'Website','vip_activation','');

                $userPack->sales_force_ref_id = (!empty($sales_force_ref_id)) ? $sales_force_ref_id : NULL;
                $userPack->save();

            } catch (\Exception $e) {

            }

            

            $jobObj = new \StdClass;
            $jobObj->packId = $pack->id;
            $job = (new ProcessSync($jobObj))
                    ->delay(Carbon::now()->addSeconds(1));
            dispatch($job);

            return response()->json(["status" => true, "data" => $data]);
        }
        return response()->json(["status" => false, "message" => trans('web.pack.notfound')]);
    }

    public function myActivatedPack($locale, $id) {
        $season = \App\Season::where('status',1)->first();
        $userPack = \App\UserPack::where('id', $id)->first();
        if(empty($userPack)){
            return redirect()->route('home');
        }
        $user = User::where('id',$userPack->user_id)->where('blocked_status','no')->orderby('created_at','desc')->first();
        if(empty($user)){
            return redirect()->route('home');
        }
        $seasonId = (!empty($season)) ?  $season->id : NULL;
        $pack = Pack::where('id', $userPack->pack_id)->where('season_id',$seasonId)->first();
        if(empty($pack)){
            return redirect()->route('home');
        }
        //dd($pack);
        $cards = [];
        $activeSeason = 'no';
        $configurations = \App\ApiConfiguration::all()->keyBy('name');

        $cars = \App\UserCarPack::join("new_user_cars","new_user_cars.id","user_car_packs.user_car_id")->select('new_user_cars.*','user_car_packs.designa_card_uid','user_car_packs.designa_card_id','user_car_packs.designa_park_id','user_car_packs.designa_setv_status','user_car_packs.designa_acc_status','user_car_packs.id as ucp_id')
        
        ->where("user_car_packs.pack_id",$userPack->pack_id)->get()->each(function($car){
            $car->country = \App\CarCountry::find($car->country_id);
            $car->emirate = \App\Emirate::find($car->emirate_id);
        });
       // dd($cars);
        if($season->id == $pack->season_id){
            $cards = Wonderpass::where('pack_id', $pack->id)->get();
            $activeSeason = 'yes';
            if(!empty($cards)){
                foreach($cards as $key=>$card){
                    $mediaCount = $this->getMediaUseCount($configurations,$card->card_number);
                    $cards[$key]->balance_points = $mediaCount['walletBanalce'];
                    $cards[$key]->ripleys_count = $mediaCount['ripleyBalance'];
                    $cards[$key]->popoverText = $mediaCount['popoverText'];
                    $cards[$key]->carnavalBal = $mediaCount['carnavalBal'];
                }
            }
        }

        
        
        $completionPage = 1;
        $car_activation_allowed_limit = 0;
        $car_adding_count = count($cars);
        $car_activation_allowed_limit = $pack->car_activation_allowed;
        $is_adding_car = 1;
        if ($car_activation_allowed_limit <= $car_adding_count) {
            $is_adding_car = 0;
        }
        $partnerOffers = $closedOffers =  [];
        $locale = app()->getLocale();
        $partnerOffers = [];
        if($season->id == $pack->season_id){
            $partnerOffers = \App\PartnerOffer::select('id', "merchant_name_en", "merchant_name_{$locale} as merchant_name", "offer_name_{$locale} as offer_name", 'valid_from', 'valid_to', 'promo_code', 'code_type', 't_and_c', 'link',"closed_message_{$locale} as closed_message", "open_status","offer_name_en","consumed")
                ->addselect(\DB::raw('LOWER(REPLACE(partner_offers.merchant_name_en, " ", "")) as offerName'))
                ->addselect(\DB::raw('CONCAT(LOWER(REPLACE(partner_offers.merchant_name_en, " ", "")),offer_name_en) as offerNameForGroupBy'))
                    ->where('pack_id',$pack->id)
                    ->where('active_status',1)
                    //->orderByRaw("FIELD(merchant_name_en,'Expo 2020 Dubai', 'Roxy Cinemas','Laguna Waterpark','The Green Planet','DPR - All Parks','Bollywood Parks Dubai','Motiongate Dubai','LEGOLAND Dubai','LEGOLAND WaterPark')")
                    ->orderby('ordering','asc')
                    ->orderby('created_at','asc')
                    
                    ->groupby('offerNameForGroupBy')
                    ->get()
                    ->keyBy('offerName')
                    // ->each(function($item) use(&$partnerOffers){
                    //     $partnerOffers[$item->offerName]['merchant_name_en'] = $item->merchant_name_en;
                    //     $partnerOffers[$item->offerName]['offerName'] = $item->offerName;
                    //     $partnerOffers[$item->offerName]['open_status'] = $item->open_status;
                    //     $partnerOffers[$item->offerName]['closed_message'] = $item->closed_message;
                    //     $partnerOffers[$item->offerName]['offers'][] = $item;
                    // })
                    ;
            //dd($partnerOffers);
            $closedOffers = \App\PartnerOffer::select("merchant_name_en")
                ->addselect(\DB::raw('LOWER(REPLACE(partner_offers.merchant_name_en, " ", "")) as offerName'))
                ->where('pack_id',$pack->id)
                ->where('active_status',1)
                ->where('open_status',0)
                ->groupby('merchant_name_en')
                ->pluck('merchant_name_en');
        }

        //API to  vgs
        
        $usageDetails = $this->getUsageDetails($pack,$configurations);
        
        $usageDetails['remaining_tables']= ($pack->remaining_tables > 0)?$pack->remaining_tables:0;
        $usageDetails['total_majlis_tables']= ($pack->allowed_tables > 0)?$pack->allowed_tables:0;
        $usageDetails['remaining_tables_percent']= ($pack->allowed_tables > 0)?$pack->remaining_tables/$pack->allowed_tables:0;
        $is_activate_pack = (!empty($season) && $season->pack_activation_enabled) ? true : false;

        

        return view('web.my-activated-pack', compact('pack', 'cards', 'completionPage', 'user', 'is_adding_car','partnerOffers','closedOffers','activeSeason','usageDetails','is_activate_pack','cars'));
    }

    public function packOffers(Request $request) {
        $locale = app()->getLocale();

        $offers = PartnerOffer::select('id', "merchant_name_en", "merchant_name_{$locale} as merchant_name", "offer_name_{$locale} as offer_name", 'valid_from', 'valid_to', 'promo_code', 'code_type', 't_and_c', 'link','offer_name_en',"consumed")
                ->addselect(\DB::raw('LOWER(REPLACE(partner_offers.merchant_name_en, " ", "")) as offerName'))
                ->where('pack_id', $request->pack_id)
                ->where('merchant_name_en', $request->offer_name)
                ->orderby('ordering','asc')
                ->orderby('created_at','asc')
                ->get();
        //echo $offers->count();exit;
        $pack = \App\Pack::find($request->pack_id);
        return view('web.offer-list', compact('offers','pack'));
    }

    public function packOfferDownload($locale, $id) {
        $locale = app()->getLocale();

        $offer = PartnerOffer::select('id', "merchant_name_{$locale} as merchant_name", "offer_name_{$locale} as offer_name", 'valid_from', 'valid_to', 'promo_code', 'code_type', 't_and_c', 'link', 'merchant_name_en','offer_name_en','pack_id')
        ->addselect(\DB::raw('LOWER(REPLACE(partner_offers.merchant_name_en, " ", "")) as offerName'))
                        ->where('id', $id)->first();
        //return view('web.pdf.offer-download', compact('offer'));
        //return view('web.pdf.offer-download', compact('offer'));
        $fileName = 'offer_ticket_' . time() . '.pdf';
        $pack = \App\Pack::find($offer->pack_id);
        $pdf = PDF::loadView('web.pdf.offer-download', compact('offer','pack'));
        return $pdf->download($fileName);
        exit;
        $pdf = PDF::loadView('web.pdf.offer-download', compact('offer'));
        dd($pdf);
        exit;
        $fileName = 'offer_ticket_' . time() . '.pdf';
        return $pdf->download($fileName);
        //return view('web.pdf.offer-download', compact('offer'));
    }

    public function load_popup_content(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                        'action_type' => 'required',
                            ], [
                        'action_type.required' => 'Action type not found!',
            ]);
            if ($validator->fails()) {
                return response()->json(['data' => "", 'message' => implode(" ", $validator->errors()->all())], 404);
            }
            $result = array();
            $result['action_type'] = $request->action_type;
            $result['action_id'] = $request->action_id;
            $result['extra_action'] = $request->extra_action;
            $result['extra_id'] = $request->extra_id;
            $result['pack_id'] = $request->pack_id;
            
            $user = \App\User::find($request->user_id);
            
            
            $pack = \App\Pack::find($request->pack_id);
            

            $locale = app()->getLocale();
            if ($result['extra_action'] == "edit") {
                $userCar = \App\UserCarPack::find($request->action_id);
                $result['user_car'] = \App\NewUserCar::where('id', $userCar->car_id)->first();
                $result['emirate'] = Emirate::select('id', "name_{$locale} as name", 'code')
                        ->where('country_id', $result['user_car']->country_id)
                        ->get();

                if (!empty($result['user_car']->emirate_id)) {
                    $prefix = Emirate::select('plate_prefix')
                            ->where('id', $result['user_car']->emirate_id)
                            ->first();
                } else {
                    $prefix = CarCountry::select('plate_prefix_new as plate_prefix')
                            ->where('id', $result['user_car']->country_id)
                            ->first();
                }

                $prefixes = [];
                if ($prefix) {
                    switch ($prefix->plate_prefix) {
                        case 'A to Z':
                            $prefixes = range('A', 'Z');
                            break;
                        case '1 to 9':
                            $prefixes = range(1, 9);
                            break;
                        case '1 to 99':
                            $prefixes = range(1, 99);
                            break;
                        default:
                            $prefixes = json_decode($prefix->plate_prefix);
                            break;
                    }
                }
                $result['plate_prefixes'] = $prefixes;
            }
            $result['car_countries'] = CarCountry::select('id', "name_{$locale} as name", 'code')->get();
            //$user_pack_detail = User::where('id', $request->extra_id)->first();
            $car_activation_allowed_limit = $pack->car_activation_allowed;
            $car_adding_count = \App\UserCarPack::where('pack_id', $pack->id)->count();
            

            if ($car_activation_allowed_limit <= $car_adding_count) {
                if ($result['extra_action'] == "edit") {
                    $result['is_adding_car'] = 1;
                } else {
                    $result['is_adding_car'] = 0;
                }
            } else {
                $result['is_adding_car'] = 1;
            }


            $view = view('web.common.load_popup_content')->with(['result' => $result])->render();
            return response()->json(['data' => $view, 'message' => ""], 200);
        } catch (Exception $e) {
            Log::error('load_popup_content', ['Exception' => $e->getMessage()]);
            return response()->json(['data' => $e->getMessage(), 'message' => 'Something went wrong'], 404);
        }
    }

    public function manage_car_detail(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                        'car_country' => 'required',
                        'plate_number' => 'required',
                        'plate_prefix' => 'required',
                            ], [
                        'action_type.required' => 'Action type not found!',
            ]);
            if ($validator->fails()) {
                return response()->json(['data' => "", 'message' => implode(" ", $validator->errors()->all())], 404);
            }
            $upack = \App\UserPack::where('pack_id', $request->pack_id)->where('user_id',$request->user_id)->first();
            $user = \App\User::find($request->user_id);
            $pack = \App\Pack::find($request->pack_id);
            $country_id = !empty($request->car_country) ? $request->car_country : NULL;
            $emirate_id = !empty($request->emirate) ? $request->emirate : NULL;
            $plate_prefix = !empty($request->plate_prefix) ? $request->plate_prefix : NULL;
            $plate_number = !empty($request->plate_number) ? $request->plate_number : NULL;
            $type = !empty($request->type) ? $request->type : NULL;
            $drupal_user_id = $user->drupal_user_id;
            $userCar = \App\NewUserCar::firstOrCreate([
                'drupal_user_id' => $drupal_user_id,
                //'user_id' => $user->id,
                'type' => $type,
                'country_id' => $country_id,
                'emirate_id' => $emirate_id,                
                'plate_prefix' => $plate_prefix,
                'plate_number' => $plate_number,
            ]);
            $userCar->user_id = $user->id;
            $userCar->save();
           $userCarPack = \App\UserCarPack::firstOrCreate([
                'pack_id' => $pack->id,
                'user_car_id' => $userCar->id
            ]);

            $message = trans('content.car_detail_add_success');
            $auditType = 'create-car';
            $event = 'User car ceated';
            $oldCar = '';

            $this->syncUsertoDesigna($upack->id);
            $configurations = \App\ApiConfiguration::all()->keyBy('name');
            $userPack = Pack::select('packs.id', 'packs.prefix', 'packs.pack_number', 'packs.sale_id', 'users.id as user_id', 'users.first_name', 'users.last_name', 'users.email', 'users.mobile', 'users.mobile_dial_code', 'users.gender', 'users.dob', 'users.nationality_id', 'users.recieve_updates', 'users.drupal_user_id')
                ->join('user_packs', 'user_packs.pack_id', 'packs.id')
                ->join('users', 'users.id', 'user_packs.user_id')
                ->where('user_packs.id', $upack->id)
                ->first();
           
            $this->savecode($userPack, $configurations);
            $user->user_pack_id = $upack->id;
            
            $agree_offers = ($user->recieve_updates) ? 'True' : 'False';
            //$this->vipSubmissionToSalesForce($user,$pack,$agree_offers,'web','add_car_web');

            $arrayData = array("audit_type"=>$auditType,'table_id'=>$userCar->user_id,'table_name'=>'new_user_cars',"admin_id"=>'',"admin_type"=>'user',"event"=>$event,"old_values"=>$oldCar,"new_values"=>$userCar,"url"=>"");
            manageAuditLogs($arrayData);

            return response()->json(['data' => $userCar, 'message' => $message, 'status' => 'success'], 200);
        } catch (Exception $e) {
            Log::error('manage_car_detail', ['Exception' => $e->getMessage()]);
            return response()->json(['data' => $e->getMessage(), 'message' => 'Something went wrong'], 404);
        }
    }

    public function hideOffer($lang,$offerId){
       // dd($offerId);
        $offers = \App\PartnerOffer::find($offerId);
        $status = 'error';
        $html = '';
        if(!empty($offers)){
            if($offers->consumed == 'yes'){
                $offers->consumed = 'no';
                $offers->consumed_date = NULL;
                $html = '<a href="javascript:void(0)" data-href="'.url(app()->getLocale().'/offer-hide/'.$offers->id).'" class="offerHide  offer-hide">'.trans('content.hide').'</a>';
            }else{
                $offers->consumed = 'yes';
                $offers->consumed_date = date('Y-m-d H:i:s');
                $html = '<div class="consumed-div"><span  class="consumed-class">'.trans('content.consumed').'</span><a href="javascript:void(0)" data-href="'.url(app()->getLocale().'/offer-hide/'.$offers->id).'" class="offerHide  offer-unhide">'.trans('content.unhide').'</a></div>';
            }
            $offers->save();
            $status = 'success';
            
        }
        return response()->json(['html' => $html,  'status' => $status], 200);
    }

    public function testSaleforce(){
        $userPack = \App\Pack::select('packs.id', 'packs.prefix', 'packs.pack_number','packs.category', 'packs.sale_id', 'users.id as user_id', 'users.first_name', 'users.last_name', 'users.email', 'users.mobile', 'users.mobile_dial_code', 'users.gender', 'users.dob', 'users.nationality_id', 'users.recieve_updates','user_packs.id as user_pack_id')
                ->join('user_packs', 'user_packs.pack_id', 'packs.id')
                ->join('users', 'users.id', 'user_packs.user_id')
                ->where('users.email', 'amrutha@mobiiworld.com')->first();
        if(!empty($userPack)){
            $user = \App\User::find($userPack->user_id);
            $pack = \App\Pack::find($userPack->id);
            $access = $this->getAccessToken();
            dd($access);
            $sales_force_ref_id = $this->vipSubmissionToSalesForce($user,$pack,true,'Mobile App','vip_activation','iOS');
            dd($sales_force_ref_id , $pack, $user);
        }else{
            echo "No data";
        }
        
    }

    public function testSaleforceUrl(){
        $headers = ['Content-Type: application/x-www-form-urlencoded', 'content: application/x-www-form-urlencoded'];
        $requestData = http_build_query( [
          'grant_type' => 'urn:ietf:params:oauth:grant-type:jwt-bearer',
          'assertion' => 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIzTVZHOWxsUVk1a005VDZjMThrZDhFRmdicEN3dzJ3SGFEVVBOanpEQVBRT1NvcUF3c2kxYktfTTJ5MlUzWjRKLmdSYXNwaXV4dFBibGhGdjRaVzRWIiwic3ViIjoiaW50ZWdyYXRpb24udXNlckBkaGV3ZWJzaXRlLmNvbS51YXQiLCJhdWQiOiJodHRwczovL3Rlc3Quc2FsZXNmb3JjZS5jb20iLCJleHAiOiIxNzU0OTkxMzg2In0.jq0R0qDAkhWuueLxQh3uxaOEdHqKKxjk2uVPJjMhPcKczzqkEV5-QASbAc9AFfQ3Qyha981PP0BkRy8FD0DGIF6N6QqydTGTW3uieetfkQ_p1XrAc-45QwIsfIcDXD7-WKqa3uD2FdtK_w7kBtZBCUHE7eryvbC8g6v6tJr2K-K-HhZQxwQ06C6cRoU29dKzIxu26f3JwEs0XEYMBrv-AbqUZ-4uSkhEoR5weOq1uZcf2s9kAFK-nVClkNe_KgKvUY9vNkGhIUQ7g0thpWvtqY63z5FkayrNg5LkVnA2kgB6Zu-b3V5n55aoq_T6-dH6LWdaXcKfor5DedKMvpzb8A',
        ]);
        $url = 'https://test.salesforce.com/services/oauth2/token?'.$requestData;
        $response = curlRequestPost($url,[],$headers);  

        $liveheaders = ['Content-Type: application/x-www-form-urlencoded', 'content: application/x-www-form-urlencoded'];
        $liverequestData = http_build_query( [
          'grant_type' => 'urn:ietf:params:oauth:grant-type:jwt-bearer',
          'assertion' => 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIzTVZHOV9rWmNMZGU3VTVxM08xSzhvbHJwb1FCRGVpSWRaR1lPRDBabm91YWlrVEk0UDVYcm9yTXYzMnVHeDNmOC5EVk1LSkdnb0psTTRDVkVkZEhrIiwic3ViIjoib25lY3JtLmludGVncmF0aW9uQGRoZW50ZXJ0YWlubWVudC5hZSIsImF1ZCI6Imh0dHBzOi8vbG9naW4uc2FsZXNmb3JjZS5jb20iLCJleHAiOiIxNzUxMzc2MDA1In0.R5O_GiIlJlK6oMr1vs0UvXA2bdF-kDvBmD15JjvLUBA3Qy44hmlEAKZJyz663bTAT6MW7nGlG18RGqpGQvNTKirnfqHqLOxlMgNNXJ37xr5cLdAoWbuSkb-wh5fTYO-zYPsO54ZcqVB2mQ1CcTTg1bvFA7onwLjCHljHDkJv_4AEYIk-QsxS5XLqkTr2m6lvFLI-DzN8kKXb-ny7_Eul9gJL8UA-qh4sXUIB2qtcg4QRkhXyxxxAFKu3KJAETb9NLL2pXSVFsEmi9iAkQU4uD_rBxnfpLqc8pBWDiD7yhpLiYcLPsOZVMb_LbE2mxF3JpW2qD_mNMCJdtiYj1OwnaQ',
        ]);
        $liveurl = 'https://login.salesforce.com/services/oauth2/token?'.$liverequestData;
        $liveresponse = curlRequestPost($liveurl,[],$liveheaders);
        dd($response,$liveresponse);
    }

    public function generateOtp(Request $request){    
        $season = \App\Season::where('status',1)->first();
        if(empty($season)){            
            return errorResponse( trans('web.pack.notfound'));
        }
        if(!$season->pack_activation_enabled){
            $message = trans('web.pack.notfound');
            if(!empty($season->pack_activation_disbaled_msg_en)){
                $message = (app()->getLocale() == 'ar' && !empty($season->pack_activation_disbaled_msg_ar)) ? $season->pack_activation_disbaled_msg_ar : $season->pack_activation_disbaled_msg_en;
            }
            
            return errorResponse( $message ,['show_alert'=>true]);
        }    
        $rules = [
            'vip_pack_number' => 'required',
            'activation_code' => 'required',
            'email' => 'required||email:rfc,dns,strict,filter'
        ];

        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) {
            
            return errorResponse(trans('web.required_fields'));
        }

        return $this->generateGuestOtp($request);
    }

}
