<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Validator;
//use GuzzleHttp\Client;
//use GuzzleHttp\Exception\RequestException;
use Twilio\Rest\Client;
use Yajra\Datatables\Datatables;
use Excel;
use Carbon\Carbon;

use Lang;
use Hash;

use DB;
use App;
use Mail;
use App\Mail\CarMail;
use PKPass\PKPass;


class TestMailController extends Controller {

  public function export(){

    set_time_limit(0);

    $expRow = DB::Connection('mysql2')->select(DB::raw("select gv_checkout_transaction.description, gv_checkout_transaction.salecode, gv_user_payment_sessions.platform, gv_checkout_transaction.totalamount,
     gv_checkout_transaction.id, gv_checkout_transaction.shopcartid, gv_checkout_transaction.email,
     gv_checkout_transaction.created_date from gv_checkout_transaction
     join `gv_user_payment_sessions` on `gv_user_payment_sessions`.`shopcartid` = `gv_checkout_transaction`.`shopcartid`
     where gv_checkout_transaction.status = 1 and gv_user_payment_sessions.status = 4 and
     gv_checkout_transaction.created_date >= '2021-02-15 00:00:00' AND gv_checkout_transaction.created_date < '2021-02-23 00:00:00'
     order by  gv_checkout_transaction.created_date asc"));
    //SELECT id,referenceCode,fName,lName,activeCountry,created_at FROM `drivers` WHERE referenceCode is not null and deleted_at is null order by `activeCountry` ASC, `created_at` ASC LIMIT 300
    $i=1;
    $exportData = array();
    foreach($expRow as $expRowDetail){
      $ticketList = array();
      if(!empty($expRowDetail->description)){

            $ticketList = explode(",", $expRowDetail->description);
            foreach($ticketList as $ticket){
                  if(!empty($ticket)){
                        $exportData[$i]['id'] = $expRowDetail->id;
                        $exportData[$i]['description'] = $ticket;
                        $exportData[$i]['salecode'] = $expRowDetail->salecode;
                        $exportData[$i]['platform'] = $expRowDetail->platform;
                        $exportData[$i]['totalamount'] = $expRowDetail->totalamount;
                        $exportData[$i]['shopcartid'] = $expRowDetail->shopcartid;
                        $exportData[$i]['email'] = $expRowDetail->email;
                        $exportData[$i]['created_date'] = $expRowDetail->created_date;

                        $i++;
                  }
              }
          }
      }
//dd($exportData); exit;
      if(!empty($exportData)){


            $startDate = '2021-02-15';
            $endDate = '2021-02-22';
            //.$startDate.' - '.$endDate.
            $fileName = 'monthly_report_'.$startDate.'---'.$endDate.time();

            //$datas = NewsletterSubscription::get($dataExported);
            //$dataArray = [];
            //$dataArray[] = $exportData;
            /*foreach ($datas as $data) {
                $dataArray[] = $data->toArray();
            }*/

            $dataHeader[] = ['id','description', 'salecode', 'platform', 'totalamount',
             'shopcartid', 'email', 'created_date'];

            $result = array_merge($dataHeader, $exportData);

            Excel::create($fileName, function($excel) use ($result,$startDate,$endDate ) {
                $excel->setTitle('Monthly Sales'.$startDate.' - '.$endDate);
                $excel->setCreator('Admin')->setCompany('GV');
                $excel->setDescription('Monthly Sales');
                $excel->sheet('sheet1', function($sheet) use ($result) {
                    $sheet->fromArray($result, null, 'A1', false, false);
                });

            })->download('csv');

      }
      dd($exportData); exit;




  }

  public function testPass(Request $request){

          // Replace the parameters below with the path to your .p12 certificate and the certificate password!

      $path = storage_path('certs/GVPassCerti.p12');
      $pass = new PKPass($path, '123456');

      // Pass content
      /*$data = [
        'description' => 'Entry Ticket',
        'formatVersion' => 1,
        'organizationName' => 'Global village',
        'passTypeIdentifier' => 'pass.com.demo.gv', // Change this!
        'serialNumber' => '12345678',
        'teamIdentifier' => 'YUJ5S863SW', // Change this!
        'boardingPass' => [
            'primaryFields' => [
                [
                    'key' => 'origin',
                    'label' => 'San Francisco',
                    'value' => 'SFO',
                ],
                [
                    'key' => 'destination',
                    'label' => 'London',
                    'value' => 'LHR',
                ],
            ],
            'secondaryFields' => [
                [
                    'key' => 'gate',
                    'label' => 'Gate',
                    'value' => 'F12',
                ],
                [
                    'key' => 'date',
                    'label' => 'Departure date',
                    'value' => '07/11/2012 10:22',
                ],
            ],
            'backFields' => [
                [
                    'key' => 'passenger-name',
                    'label' => 'Passenger',
                    'value' => 'John Appleseed',
                ],
            ],
            'transitType' => 'PKTransitTypeAir',
        ],
        'barcode' => [
            'format' => 'PKBarcodeFormatQR',
            'message' => '1W8GHJCG7F7F9R',
            'messageEncoding' => 'iso-8859-1',
        ],
        'backgroundColor' => 'rgb(32,73,146)',
        'logoText' => 'Entry tickets - People of determination',
        'relevantDate' => date('Y-m-d\TH:i:sP')
      ];*/

      $data = [
   "formatVersion" => 1,
   "passTypeIdentifier" => "pass.com.demo.gv",
   "serialNumber" => "12345678",
   "teamIdentifier" => "YUJ5S863SW",
   "relevantDate" => "2011-12-08T13:00-08:00",
   "locations" => [
         [
            "longitude" => -122.3748889,
            "latitude" => 37.6189722
         ],
         [
               "longitude" => -122.03118,
               "latitude" => 37.33182
            ]
      ],
   "barcode" => [
                  "message" => "123456789",
                  "format" => "PKBarcodeFormatPDF417",
                  "messageEncoding" => "iso-8859-1"
               ],
   "organizationName" => "Global Village",
   "description" => "Entry tickets",
   "foregroundColor" => "rgb(255, 255, 255)",
   "backgroundColor" => "rgb(24, 58, 117)",
   "eventTicket" => [
                     "primaryFields" => [
                        [
                           "key" => "event",
                           "label" => "EVENT",
                           "value" => "Entry tickets - People of determination"
                        ]
                     ],
                     "secondaryFields" => [
                              [
                                 "key" => "loc",
                                 "label" => "Date Pursahse",
                                 "value" => "2021-01-05"
                              ],
                              [
                                 "key" => "datepur",
                                 "label" => "Valid Until",
                                 "value" => "2021-01-25"
                              ]
                           ]
                  ]
];

      //echo json_encode($data); exit;

      $pass->setData($data);
$pass->setName('siju');
      // Add files to the pass package
      $pass->addRemoteFile(url('images/pass/icon.png'));
      $pass->addRemoteFile(url('images/pass/icon@2x.png'));
      $pass->addRemoteFile(url('images/pass/logo.png'));

      $pass->addRemoteFile(url('images/pass/thumbnail.png'));
      $pass->addRemoteFile(url('images/pass/thumbnail@2x.png'));

      $pass->addRemoteFile(url('images/pass/background.png'));
      $pass->addRemoteFile(url('images/pass/background@2x.png'));

      // Create and output the pass
      if(!$pass->create(true)) {
        echo 'Error: ' . $pass->getError();
      }

      exit;


  }


    public function index(Request $request) {

          $lang = !empty($request->lang) ? $request->lang : 'en';
          App::setLocale($lang);


          $type = $request->type;
          $emailTo = $request->emailTo;

          switch ($type) {

                case 'welcome':
                  $this->sendRiderWelcomeEmail($request);
                  break;

                default:

                break;


          }

          echo 'Mail sent succesfully...';
          exit;

    }


    public function testSmtp(){

      $password = Hash::make('123456');
echo $password; exit;
      $data = array();



      try {
        Mail::send('emails.welcome', $data, function ($message) {
            $message->from('test@lymo.com', 'Majed Samawi');
            $message->subject('Test Mail');
            $message->replyTo('majed@lymo.com', 'Majed Samawi');
            $message->to('siju@mobiiworld.com');

        });
      }catch(\Exception $e){

      }
              echo 'send...--'.time(); exit;
    }


  private function sendAdminNotifyEmail($request){
        $user = App\Driver::where('mobile', '+'.$request->mobile)->first();
        $docRequired = trans('driverApi.doc_required');
        $pushTitle = 'Notification: Vehicle Created';
        $pushBody = 'gdf dfgdfgf';



        $vehicle = \App\Vehicle::where('id', '856d0e10-764b-11e9-90f5-f5c13ae06b8e')->first();
        $language = \App\Language::where('code', 'en')->first();
        $lanID = $language->id;




        try {
          $vehicleMake = $vehicle->make($lanID)->name;
          $vehicleModel = $vehicle->model($lanID)->name;
          $vehicleName = $vehicle->name;
          $vehicleSpec = $vehicleMake.', '.$vehicleModel;

          Mail::to('support@lymo.com')
                  ->send(new AdminNotificationMail($user, ['subject' => $pushTitle, 'body' => $pushBody,
                  'vehicleName' => $vehicleName,
                  'vehicleSpec' => $vehicleSpec
                ]));
        } catch (\Exception $e) {
        }

        return;
    }

    private function sendDriverDocEmail($request){
          $user = App\Driver::where('mobile', '+'.$request->mobile)->first();
          $docRequired = trans('driverApi.doc_required');
          $pushTitle = $docRequired['title'];
          $pushBody = '<ol style="text-align:center;font-family: Tahoma,Helvetica,Arial,sans-serif;margin: 0px;color: #838383;font-size: 16px;line-height: 29px;width: 100%;margin: 0;padding: 0; padding-left: 0px;">';
          $pushBody .= '<li>' . implode('</li><li>', $docRequired['docs']) . '</li></ol>';




          try {
            Mail::to($request->emailTo)
                    ->send(new DriverNotificationMail($user, ['subject' => $pushTitle, 'body' => $pushBody]));
          } catch (\Exception $e) {

          }

          return;
      }


      private function sendRiderWelcomeEmailPromo($request){

          $welcomeMailTitle = 'Découvrez Lymo avant tout le monde';

          $user = App\User::where('mobile', '+'.$request->mobile)->first();
          try {
              Mail::to($request->emailTo)
                      ->send(new WelcomeMailPromo($user, ['subject' => $welcomeMailTitle]));
          } catch (\Exception $e) {

          }

          return;
      }

    private function sendRiderWelcomeEmail($request){

        $welcomeMailTitle = 'test';

        //$user = App\User::where('mobile', '+'.$request->mobile)->first();
        $user ='';

        $car = \App\UserCar::where('id', 'a2e7ed00-3323-11eb-ab74-65a34beb0c62')->first();

        $data = array();
        $data['subject'] = 'Edit Car Details Sucessfully';
        $data['lang'] = 'ar';
        $data['carCountry'] = \App\CarCountry::where('id', $car->country_id)->first()->name_en;
        $data['carEmirate'] = \App\Emirate::where('id', $car->emirate_id)->first()->name_en;
        $data['platePrefix'] = $car->plate_prefix;
        $data['plateNumber'] = $car->plate_number;

        $user = \App\User::where('id', $car->user_id)->first();

/*
        $text = 'Peter is a boy.'; // english
$text = 'بيتر هو صبي.'; // arabic
//$text = 'פיטר הוא ילד.'; // hebrew

mb_regex_encoding('UTF-8');

if(mb_ereg('[\x{0600}-\x{06FF}]', $text)) // arabic range
//if(mb_ereg('[\x{0590}-\x{05FF}]', $text)) // hebrew range
{
    echo "Text has some arabic/hebrew characters.";
}
else
{
    echo "Text doesnt have arabic/hebrew characters.";
}
exit;*/

        //dd($user->email); exit;

        try {
            Mail::to($request->emailTo)
                    ->send(new CarMail($user, $data));
        } catch (\Exception $e) {
            //echo $e->getMessage();
        }

        return;
    }

    private function sendDriverWelcomeEmail($request){

        $welcomeMailTitle = trans('email.driver.welcome_subject');

        $driver = App\Driver::where('mobile', '+'.$request->mobile)->first();
        try {
            Mail::to($request->emailTo)
                    ->send(new DriverWelcomeMail($driver, ['subject' => $welcomeMailTitle]));
        } catch (\Exception $e) {

        }

        return;
    }

    public function layoutView(){
         $DFS = config('globalConstants.DRIVER_REFERRAL');

        set_time_limit(0);
        App::setLocale('fr');

        $invoice = \App\TripInvoice::where('mailSent', 0)->first();
           // foreach ($tripInvoices as $invoice) {
                //$responseData = $this->invoiceDetails($invoice->invoiceId);
                //dd($invoice);
               // if (isset($responseData[0])) {
                   // if(!empty($responseData[0]->validated)){
                            $driver = \App\Driver::where('id', $invoice->driverId)->first();
                            //$wealthing = $driver->wealthings()->select('bankId','accountNumber','swiftBic', 'bankName', 'bankAddress')->where('position', 0)->first();
                            $wealthing = \App\DriverWealthing::where('bankId', $invoice->bankId)->where('driverId', $invoice->driverId)->first();
                            $data =array();
                            //$data['email']='siju@mobiiworld.com';
                            $data['email']= $driver->email;
                            $data['currency']= $driver->country->currency;
                            $data['driverName'] =  $driver->fName;
                            $data['journalEntryId']= $invoice->journalEntryId;

                            $data['payout'] = !empty($invoice->payoutAmount) ? getformattedNumber($invoice->payoutAmount)  :  getformattedNumber($invoice->netAmount - ($invoice->lymoFees + $invoice->cardServiceCharge + $invoice->navigationCharge));

                            $data['payoutValuePositive'] =0;

                            $data['payoutPeriod']= date('d M, Y', strtotime($invoice->weekStartDate)) .' - '. date('d M, Y', strtotime($invoice->invoiceDate));
                            $data['reportDate']= date('d M, Y', time());

                            $data['iban']= trim( chunk_split($wealthing->accountNumber, 4, ' ') );

                            $data['creditSum']= getformattedNumber($invoice->cardSum);
                            $data['cashSum']= getformattedNumber($invoice->cashSum);
                            $data['freeRides']= getformattedNumber($invoice->discountAmount);

                            $totalFares = $invoice->cardSum + $invoice->cashSum + $invoice->discountAmount;
                            $data['totalFares']= getformattedNumber($totalFares);


                            $data['lymoFees']= getformattedNumber($invoice->lymoFees);
                            $data['lymoFeesUnitPrice']= !empty($invoice->lymoFeesUnitPrice) ? $invoice->lymoFeesUnitPrice : 5;
                            $data['lymoFeesCount']= (!empty($invoice->lymoFees) && !empty($invoice->lymoFeesUnitPrice)) ? (getformattedNumber($invoice->lymoFees) / $invoice->lymoFeesUnitPrice) : '0';

                            $data['bonusAmount']= getformattedNumber($invoice->bonusAmount);

                            //blueCardBonus
                            $bonusDetails = !empty($invoice->bonusDetails) ? json_decode($invoice->bonusDetails, true) : '';
                            $data['blueCardBonus']= !empty($bonusDetails['blueCardBonus']) ? getformattedNumber($bonusDetails['blueCardBonus']) : 0;
                            $data['blueCardUnitPrice']= !empty($bonusDetails['blueCardUnitPrice']) ? getformattedNumber($bonusDetails['blueCardUnitPrice']) : 0;
                            $data['blueCardBonusCount']= (!empty($bonusDetails['blueCardBonus']) && !empty($bonusDetails['blueCardUnitPrice'])) ? (getformattedNumber($bonusDetails['blueCardBonus']) / $bonusDetails['blueCardUnitPrice']) : '0';

                            //goldCardBonus
                            $data['goldCardBonus']= !empty($bonusDetails['goldCardBonus']) ? getformattedNumber($bonusDetails['goldCardBonus']) : 0;
                            $data['goldCardBonusUnitPrice']= !empty($bonusDetails['goldCardBonusUnitPrice']) ? getformattedNumber($bonusDetails['goldCardBonusUnitPrice']) : 0;
                            $data['goldCardBonusCount']= (!empty($bonusDetails['goldCardBonus']) && !empty($bonusDetails['goldCardBonusUnitPrice'])) ? (getformattedNumber($bonusDetails['goldCardBonus']) / $bonusDetails['goldCardBonusUnitPrice']) : '0';

                            //driverBonus
                            $data['driverBonus']= !empty($bonusDetails['driverBonus']) ? getformattedNumber($bonusDetails['driverBonus']) : 0;
                            $data['driverBonusUnitPrice']= !empty($bonusDetails['driverBonusUnitPrice']) ? getformattedNumber($bonusDetails['driverBonusUnitPrice']) : 0;
                            $data['driverBonusCount']= (!empty($bonusDetails['driverBonus']) && !empty($bonusDetails['driverBonusUnitPrice'])) ? (getformattedNumber($bonusDetails['driverBonus']) / $bonusDetails['driverBonusUnitPrice']) : '0';

                            //driverRegBonus
                            $data['blueCardRegBonus']= !empty($bonusDetails['blueCardRegBonus']) ? getformattedNumber($bonusDetails['blueCardRegBonus']) : 0;
                            $data['blueCardRegUnitPrice']= !empty($bonusDetails['blueCardRegUnitPrice']) ? getformattedNumber($bonusDetails['blueCardRegUnitPrice']) : 0;
                            $data['blueCardRegBonusCount']= (!empty($bonusDetails['blueCardRegBonus']) && !empty($bonusDetails['blueCardRegUnitPrice'])) ? (getformattedNumber($bonusDetails['blueCardRegBonus']) / $bonusDetails['blueCardRegUnitPrice']) : '0';

                            //tripCompleteBonus
                            $data['tripCompleteBonus']= !empty($bonusDetails['tripCompleteBonus']) ? getformattedNumber($bonusDetails['tripCompleteBonus']) : 0;
                            $data['tripCompleteUnitPrice']= !empty($bonusDetails['tripCompleteUnitPrice']) ? getformattedNumber($bonusDetails['tripCompleteUnitPrice']) : 0;
                            $data['tripCompleteBonusCount']= (!empty($bonusDetails['tripCompleteBonus']) && !empty($bonusDetails['tripCompleteUnitPrice'])) ? (getformattedNumber($bonusDetails['tripCompleteBonus']) / $bonusDetails['tripCompleteUnitPrice']) : '0';

                            //peakHourBonus
                            $data['peakHourBonus']= !empty($bonusDetails['peakHourBonus']) ? getformattedNumber($bonusDetails['peakHourBonus']) : 0;
                            $data['peakHourBonusUnitPrice']= !empty($bonusDetails['peakHourBonusUnitPrice']) ? getformattedNumber($bonusDetails['peakHourBonusUnitPrice']) : 0;
                            $data['peakHourBonusCount']= (!empty($bonusDetails['peakHourBonus']) && !empty($bonusDetails['peakHourBonusUnitPrice'])) ? (getformattedNumber($bonusDetails['peakHourBonus']) / $bonusDetails['peakHourBonusUnitPrice']) : '0';

                            $data['cardServiceCharge']= getformattedNumber($invoice->cardServiceCharge);
                            $data['navigationCharge']= getformattedNumber($invoice->navigationCharge);

                            $totalCharges = $invoice->lymoFees + $invoice->cardServiceCharge + $invoice->navigationCharge;

                            $data['totalCharges']= getformattedNumber($totalCharges);

                            $data['ambassadorAmount']= getformattedNumber($invoice->ambassadorAmount);
                            $data['ambassadorCount']= (!empty ($invoice->ambassadorAmount) && !empty($invoice->ambassadorUnitPrice)) ? (getformattedNumber($invoice->ambassadorAmount) / $invoice->ambassadorUnitPrice) : '0';
                            $data['ambassadorUnitPrice']= !empty($invoice->ambassadorUnitPrice) ? $invoice->ambassadorUnitPrice : 100;

                            $data['tollAmount']= getformattedNumber($invoice->tollAmount);

                            $data['mailSubject']= 'Your Lymo payment for the week of '.date('d.m.Y', strtotime($invoice->weekStartDate)).' to '.date('d.m.Y', strtotime($invoice->invoiceDate));


        return view('emails.drivers.payout', compact('driver','data'));
                    }
               // }
           // }
   // }
}
