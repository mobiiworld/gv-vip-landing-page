<?php

namespace App\Http\Controllers\Api\V1\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use Validator;
use App\User;
use App\UserActiveToken;
use Auth;

//use App\Http\Controllers\Api\V1\Traits\UserTrait;
//use App\Events\TwilioPush;
//use App\Events\TwilioRegister;
//use Mail;

class UserController extends ApiController {

    //use UserTrait;
    //use ImageTraits;

    public function __construct() {
        $this->middleware('api');
    }

    function register(Request $request) {
        $rules = [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt(trim($request->password));

        $user->save();

        $token = auth('api')->login($user);

        $userActiveToken = new UserActiveToken();
        $userActiveToken->user_id = $user->id;
        $userActiveToken->token = $token;
        $userActiveToken->save();

        $data = [
            'token_type' => 'Bearer',
            'access_token' => $token,
        ];
        return successResponse(trans('api.registration_success'), $data);
    }

    function login(Request $request) {
        $rules = [
            'email' => 'required',
            'password' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        $credentials = request(['email', 'password']);

        if (!$token = auth('api')->attempt($credentials)) {
            return errorResponse(trans('api.invalid_credentials'));
        }

        $user = auth('api')->user();
        $data = [
            'token_type' => 'Bearer',
            'access_token' => $token,
            'user' => $user
        ];

        //Invalidate old token and save current token
        $userActiveToken = $user->activeToken;
        auth('api')->setToken($userActiveToken->token)->invalidate(true);
        $userActiveToken->token = $token;
        $userActiveToken->save();

        return successResponse(trans('api.login_success'), $data);
    }

    public function details() {
        return successResponse('', auth('api')->user());
    }

    public function getUserTableCount(Request $request) {
        $rules = [
            'userId' => 'required',
            'email' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        $season = \App\Season::where('status', 1)->first();

        $tableCount = \App\Pack::join('users', 'users.pack_id', 'packs.id')
                ->where(function ($q) use ($request) {
                    $q->where('packs.drupal_user_id', $request->userId)
                    ->orWhere('users.email', $request->email);
                })
                ->selectRaw("SUM(CASE WHEN (packs.category='platinum' OR packs.category='diamond' OR packs.category='private') THEN packs.remaining_tables END) AS full_table,SUM(CASE WHEN (packs.category='gold' OR packs.category='silver' OR packs.category='complimentary') THEN packs.remaining_tables END) AS hourly_table")
                ->where('packs.season_id', $season->id)
                ->first();

        switch (true) {
            case ($tableCount->full_table > 0):
                $data['remainingTableCount'] = $tableCount->full_table;
                $data['type'] = 'fullday';
                break;
            case ($tableCount->hourly_table > 0):
                $data['remainingTableCount'] = $tableCount->hourly_table;
                $data['type'] = 'hourly';
                break;
            default:
                $data = [
                    'remainingTableCount' => 0,
                    'type' => null
                ];
                break;
        }

        return successResponse('success', $data);
    }

}
