<?php

namespace App\Http\Controllers\Api\V1\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use Validator;
use App\Notification;
use App\NotificationUser;

class NotificationController extends ApiController {

    public function __construct() {
        $this->middleware('api');
    }

    public function index(Request $request) {
        $rules = [
            'drupal_id' => 'required|integer',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        $user = NotificationUser::select('id', 'last_read_at')->where('drupal_id', $request->drupal_id)->first();

        switch ($request->wants) {
            case 'count':
                if ($user) {
                    $data['count'] = Notification::where('created_at', '>=', $user->last_read_at)->count();
                } else {
                    $dateToCheck = date('Y-m-d', strtotime('-1 day', time()));
                    $data['count'] = Notification::whereDate('created_at', '>=', $dateToCheck)->count();
                }
                break;
            case 'list':
                $lan = app()->getLocale();
                $data = Notification::select('id', "titles->{$lan} as title", "messages->{$lan} as message")
                        ->orderBy('created_at', 'DESC')
                        ->paginate(20);
                if ($user) {
                    $user->last_read_at = date('Y-m-d H:i:s');
                } else {
                    $user = new NotificationUser();
                    $user->drupal_id = $request->drupal_id;
                    $user->last_read_at = date('Y-m-d H:i:s');
                }
                $user->save();
                break;
        }
        return successResponse(trans('api.success'), $data);
    }

}
