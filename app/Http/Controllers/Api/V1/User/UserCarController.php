<?php

namespace App\Http\Controllers\Api\V1\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use Validator;
use App\User;
use App\NewUserCar;
use App\Traits\DrupalTraits;
class UserCarController extends ApiController {
    use DrupalTraits;

    public function __construct() {
        $this->middleware('api');
    }

    public function listCars(Request $request) {
        $rules = [
            'drupal_user_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        if(!$this->validateDrupalUser($request->drupal_user_id, $request->header('GV-User-Token'))){
            return errorResponse(trans('api.unauthorized'));
        }

        //$userIds = User::select('id')->where('drupal_user_id', $request->drupal_user_id)->where('season_id', $seasonId)->pluck('id');

        /*
        $cars = UserCar::select('id', 'country_id', 'emirate_id', 'plate_prefix', 'plate_number', 'type', 'is_vip')
                ->with('country:id,code')
                ->with('emirate:id,code')
                ->where('drupal_user_id', $request->drupal_user_id)
                //->where('is_vip', false)
                ->orderBy('created_at','DESC')
                ->get();

        */
        $carData = \App\NewUserCar::select('id','plate_number','country_id','emirate_id','plate_prefix','type');
        $carData->where('drupal_user_id',$request->drupal_user_id);
        $cars = $carData->get()->each(function($car){
            $car->country = \App\CarCountry::find($car->country_id);
            $car->emirate = \App\Emirate::find($car->emirate_id);
            $car->is_vip = 0;
            $packCarCount = \App\UserCarPack::where('user_car_id',$car->id)->count();
            if($packCarCount > 0){
                $car->is_vip = 1;
            }
        });

        return successResponse(trans('api.success'), $cars);

    }

    public function addCar(Request $request) {
        $rules = [
            'drupal_user_id' => 'required',
            'county_code' => 'required',
            //'emirate_code' => 'required',
            'plate_prefix' => 'required',
            'plate_number' => 'required',
            'type' => 'required|integer',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }
        if(!$this->validateDrupalUser($request->drupal_user_id, $request->header('GV-User-Token'))){
            return errorResponse(trans('api.unauthorized'));
        }

        /*$user = User::select('id')->where('drupal_user_id', $request->drupal_user_id)->first();
        if ($user) {
            $country = \App\CarCountry::where('code', $request->county_code)->first();
            if ($country) {
                if (!empty($request->car_id)) {
                    echo  $user->id; exit;
                    $userCar = UserCar::where('id', $request->car_id)
                            //->where('user_id', $user->id)
                            ->first();
                    if (empty($userCar)) {
                        return errorResponse(trans('api.user.car_not_found'));
                    }
                } else {
                    $userCar = new UserCar();
                    $userCar->user_id = $user->id;
                    $userCar->is_vip = false;
                }
                $userCar->country_id = $country->id;
                if (!empty($request->emirate_code)) {
                    $emirate = \App\Emirate::where('code', $request->emirate_code)->first();
                    if ($emirate) {
                        $userCar->emirate_id = $emirate->id;
                    } else {
                        return errorResponse(trans('api.user.invalid_emirate'));
                    }
                }
                $userCar->plate_prefix = $request->plate_prefix;
                $userCar->plate_number = $request->plate_number;
                $userCar->type = $request->type;
                $userCar->save();
                return successResponse(trans('api.success'));
            }
            return errorResponse(trans('api.user.invalid_car_country'));
        }*/


        $country = \App\CarCountry::where('code', $request->county_code)->first();
        if ($country) {
            if (!empty($request->car_id)) {
                $userCar = NewUserCar::where('id', $request->car_id)
                            ->where('drupal_user_id', $request->drupal_user_id)
                            ->first();
                if (empty($userCar)) {
                    return errorResponse(trans('api.user.car_not_found'));
                }
            } else {
                $userCar = new NewUserCar();
                $userCar->drupal_user_id = $request->drupal_user_id;
                //$userCar->is_vip = false;
            }

            $userCar->country_id = $country->id;

            if (!empty($request->emirate_code)) {
                $emirate = \App\Emirate::where('code', $request->emirate_code)->first();
                if ($emirate) {
                    $userCar->emirate_id = $emirate->id;
                } else {
                    return errorResponse(trans('api.user.invalid_emirate'));
                }
            }
            $userCar->plate_prefix = $request->plate_prefix;
            $userCar->plate_number = $request->plate_number;
            $userCar->type = $request->type;
            $userCar->save();
            return successResponse(trans('api.success'));


        }else{
            return errorResponse(trans('api.user.invalid_car_country'));
        }



        return errorResponse(trans('api.user.not_found'));
    }

    public function deleteCar(Request $request) {
        $rules = [
            'drupal_user_id' => 'required',
            'car_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }
        if(!$this->validateDrupalUser($request->drupal_user_id, $request->header('GV-User-Token'))){
            return errorResponse(trans('api.unauthorized'));
        }

        $userCar = NewUserCar::where('id', $request->car_id)
                ->where('drupal_user_id',  $request->drupal_user_id)
               // ->whereNull('user_id')
                ->first();
        if ($userCar) {
            $carPackCount = \App\UserCarPack::where('user_car_id',$userCar->id)->count();
            if($carPackCount > 0){
                return errorResponse(trans('api.user.car_not_found'));
            }
            $userCar->delete();
            return successResponse(trans('api.success'));
        } else {
            return errorResponse(trans('api.user.car_not_found'));
        }
    return errorResponse(trans('api.user.not_found'));
    }

}
