<?php

namespace App\Http\Controllers\Api\V1\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use Validator;
use App\FitnessSeason;
use App\FitnessUser;
use App\FitnessDailyTracks;
use App\FitnessMonthlyTrack;
use Carbon\Carbon;
use DB;
use Mail;
use App\Mail\CarMail;
use App\Traits\SalesForceTraits;

class FitnessController extends ApiController {

    use SalesForceTraits;

    public function __construct() {
        $this->middleware('api');
    }

    public function deleteUser($id) {
        $season = FitnessSeason::select('id')
                ->where('is_active', 1)
                ->first();
        $fuser = FitnessUser::select('id')->where('drupal_id', $id)
                ->where('fitness_season_id', $season->id)
                ->delete();

        return successResponse('User deleted');
    }

    public function getChallenges($id) {
        $lan = app()->getLocale();
        $data['season'] = FitnessSeason::select('id', "names->{$lan} as name", "popup_titles->{$lan} as fiteness_popup_title", "popup_messages->{$lan} as fiteness_popup_message", 'target_steps', 'start_date', 'end_date', "program_name->{$lan} as program_names", "program_description->{$lan} as program_descriptions", 'is_show_homepage','start_time','end_time','is_next_day','duration')
                ->with("prizes:id,fitness_season_id,names->{$lan} as name,descriptions->{$lan} as description,icon,type")
                ->with(["seasonImages" => function ($q) use ($lan) {
                        $q->select('id', 'fitness_season_id', 'image', 'url', 'type', 'index_no', 'default');
                        $q->where('language', $lan)->orderBy('index_no');
                    }])
                ->where('is_active', 1)
                ->first();
        if ($data['season']) {
            $participated_date = $this->getParticipatedDate($data['season']);
            $data['season']->join_challenge = false;
            $data['season']->end_challenge = false;
            $data['season']->join_time = null;
            $data['season']->joined_date = null;
            $today = date('Y-m-d');
            if (!empty($participated_date) && $data['season']->start_date <= $today && $data['season']->end_date >= $participated_date) {
                $data['season']->join_challenge = true;
            }
            if ($id != 'guest') {
                $fuser = FitnessUser::select('id')->where('drupal_id', $id)
                        ->where('fitness_season_id', $data['season']->id)
                        ->first();
            } else {
                $fuser = null;
            }
            if ($fuser) {
                
                $data['challenges']['daily'] = FitnessDailyTracks::select('id', 'platform', 'steps', 'status', 'coupon_code', DB::raw('DATE(created_at) date'), DB::raw('TIME(created_at) time'), DB::raw('DATE(participated_date) participated_date'))
                        ->where('fitness_user_id', $fuser->id)
                        ->orderBy('participated_date', 'DESC')
                        ->take(31)
                        ->get();
                if (isset($data['challenges']['daily'][0])) {
                    if ($data['challenges']['daily'][0]->participated_date == $participated_date) {
                        $data['season']->join_challenge = false;
                        if ($data['challenges']['daily'][0]->status == 0) {
                            $data['season']->end_challenge = true;
                            $data['season']->join_time = $data['challenges']['daily'][0]->time;
                            $data['season']->joined_date = $data['challenges']['daily'][0]->date;
                            $data['season']->participated_date = $data['challenges']['daily'][0]->participated_date;
                            //$data['challenges']['daily'][0]->status = 3; //in progress
                            $data['challenges']['daily'] = $data['challenges']['daily']->forget(0)->flatten();
                        }
                    }
                }
            } else {
                $data['challenges'] = null;
            }
        } else {
            $data['challenges'] = null;
        }
        return successResponse(trans('api.success'), $data);
    }

    public function joinToday(Request $request) {
        $rules = [
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required',
            //'mobile' => 'required',
            'drupal_id' => 'required|integer',
            'platform' => 'required',
            'latitude' => 'required',
            'longitude' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        $season = FitnessSeason::select('id', 'start_date', 'end_date', DB::raw('ST_CONTAINS(in_area, point(' . $request->latitude . ',' . $request->longitude . ')) as area'), 'geofence','is_next_day','start_time','end_time')
                ->where('is_active', 1)
                ->first();

        if ($season) {
            if ($season->geofence) {
                if ($season->area <= 0) {
                    return errorResponse(trans('api.fitness.geofence'));
                }
            }
            $fuser = FitnessUser::firstOrCreate(
                            ['fitness_season_id' => $season->id, 'drupal_id' => $request->drupal_id],
                            ['fname' => $request->fname, 'lname' => $request->lname, 'email' => $request->email, 'mobile' => $request->mobile]
            );

            $today = date('Y-m-d');
            $participated_date = $this->getParticipatedDate($season); 
            if(empty($participated_date)){
                return errorResponse(trans('api.fitness.no_active_challenge'));
            }
            if ($season->start_date <= $today && $season->end_date >= $participated_date) {                          
                $existChallenge = FitnessDailyTracks::select('id')
                        ->whereDate('participated_date', $participated_date)
                        ->where('fitness_user_id', $fuser->id)
                        ->first();
                if ($existChallenge) {
                    return errorResponse(trans('api.fitness.challenge_exist'));
                }
                $todayChalleng = new FitnessDailyTracks();
                $todayChalleng->fitness_user_id = $fuser->id;
                $todayChalleng->fitness_season_id = $season->id;
                $todayChalleng->platform = $request->platform;
                $todayChalleng->participated_date = $participated_date;
                $todayChalleng->save();

                //Create month track if not exist //commented since monthly and season cahllenge not using now
                // $monthChallenge = FitnessMonthlyTrack::where('fitness_user_id', $fuser->id)
                //         ->whereYear('created_at', date('Y'))
                //         ->whereMonth('created_at', date('m'))
                //         ->first();
                // if (empty($monthChallenge)) {
                //     $monthChallenge = new FitnessMonthlyTrack();
                //     $monthChallenge->fitness_user_id = $fuser->id;
                //     $monthChallenge->fitness_season_id = $season->id;
                //     $monthChallenge->platform = $request->platform;
                //     $monthChallenge->save();
                // }
                return successResponse(trans('api.fitness.success'));
            }
            return errorResponse(trans('api.fitness.season_expired'));
        }
        return errorResponse(trans('api.something_wrong'));
    }

    public function getParticipatedDate($season) {
        $participated_date = NULL;
        if($season->is_next_day){
            if(date('H:i:s') >= $season->start_time && date('H:i:s') <= "23:59:59"){
                $participated_date = date("Y-m-d");
            }
            if(date('H:i:s') <= $season->end_time && date('H:i:s') >= "00:00:00"){
                $participated_date = date("Y-m-d", strtotime("-1 days"));                        
            }
        }else{
            if(date('H:i:s') >= $season->start_time && date('H:i:s') <= $season->end_time){
                $participated_date = date("Y-m-d");
            }            
        }
        return $participated_date;
    }

    public function endChallenge(Request $request) {
        $rules = [
            'drupal_id' => 'required|integer',
            'steps' => 'required|integer',
            'latitude' => 'required',
            'longitude' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        $today = date('Y-m-d');
        $isDWinner = true;

        $season = FitnessSeason::select('id', 'target_steps', 'max_ups_prize_count', DB::raw('ST_CONTAINS(in_area, point(' . $request->latitude . ',' . $request->longitude . ')) as area'), 'geofence','is_next_day','start_time','end_time','end_date')
                //->where('end_date', '>=', $today)
                ->where('is_active', 1)
                ->first();
        $locale = app()->getLocale();
        $success_data = \App\DynamicText::whereIn('slug',['fitness-success-title','fitness-success-message','fitness-success-image','fitness-failure-message'])->get()->keyBy('slug')->toArray();
        $fitness_messages = [];
        if(!empty($success_data['fitness-success-title'])){
            $fitness_messages['success_title'] = $success_data['fitness-success-title']['text_'.$locale];
        }
        if(!empty($success_data['fitness-success-message'])){
            $fitness_messages['success_message'] = $success_data['fitness-success-message']['text_'.$locale];
        }
        if(!empty($success_data['fitness-success-image'])){
            $fitness_messages['success_image'] = $success_data['fitness-success-image']['text_'.$locale];
        }
        if(!empty($success_data['fitness-failure-message'])){
            $fitness_messages['failure_message'] = $success_data['fitness-failure-message']['text_'.$locale];
        }
        if ($season) {
            $participated_date = $this->getParticipatedDate($season); 
            if ($season->end_date < $participated_date ) {
                return errorResponse(trans('api.fitness.season_expired'));
            } 
            if ( empty($participated_date)) {
                return errorResponse(trans('api.fitness.no_active_challenge'));
            }            
            if ($season->geofence) {
                if ($season->area <= 0) {
                    return errorResponse(trans('api.fitness.geofence'));
                }
            }
            $fuser = FitnessUser::where('drupal_id', $request->drupal_id)
                    ->where('fitness_season_id', $season->id)
                    ->first();
            if ($fuser) {
                $successRspns = ['statusCode' => 3000, 'message' => trans('api.success')];
                $successRspns['fitness_messages'] = $fitness_messages;
                $dailyChallenge = FitnessDailyTracks::whereDate('participated_date', $participated_date)
                        ->where('fitness_user_id', $fuser->id)
                        ->where('status', 0)
                        ->first();
                if ($dailyChallenge) {
                    $dailyChallenge->steps = $request->steps;
                    //check daily target achieved
                    $withinTime = true;
                    if($season->duration > 0){
                        $withinTime = false;
                        $challengeEndTime = date("Y-m-d H:i:s",strtotime('+'.$season->duration.' minutes', strtotime($dailyChallenge->created_at)));
                        if(date('Y-m-d H:i:s') <= $challengeEndTime){
                            $withinTime = true;
                        }
                    }
                    if ($request->steps >= $season->target_steps['daily'] && $withinTime) {
                        //Get win count and check max for season
                        $winCount = FitnessDailyTracks::where('fitness_user_id', $fuser->id)
                                ->where('status', 1)
                                ->count();
                        if ($winCount < $season->max_ups_prize_count) {
                            $dailyChallenge->coupon_code = strtoupper(random_strings(7));
                        } else {
                            $isDWinner = false;
                        }
                        $dailyChallenge->status = 1;
                        $successRspns['statusCode'] = 4000;
                        $successRspns['message'] = trans('api.fitness.day_success');
                    } else {
                        $dailyChallenge->status = 2;
                        $successRspns['message'] = trans('api.fitness.day_failed');
                        $isDWinner = false;
                    }
                    $dailyChallenge->save();
                    //Get total steps for the cur month and check target achieved
                    // $monthChallenge = FitnessMonthlyTrack::where('fitness_user_id', $fuser->id)
                    //         ->whereYear('created_at', date('Y'))
                    //         ->whereMonth('created_at', date('m'))
                    //         ->first();
                    // $monthChallenge->steps += $request->steps;
                    // if ($monthChallenge->status == 0) {
                    //     if ($monthChallenge->steps >= $season->target_steps['monthly']) {
                    //         $monthChallenge->status = 1;
                    //         $monthChallenge->coupon_code = random_strings(7);
                    //         $successRspns['statusCode'] = 4000;
                    //         $successRspns['message'] = ($dailyChallenge->status == 2) ? trans('api.fitness.month_success') : trans('api.fitness.day_month_success');
                    //     }
                    // }
                    // $monthChallenge->save();
                    //Calculate total season steps and check target achieved
                    $fuser->total_steps += $request->steps;
                    if ($fuser->is_winner == 0) {
                        if ($fuser->total_steps >= $season->target_steps['season']) {
                            $fuser->is_winner = 1;
                            $fuser->coupon_code = strtoupper(random_strings(7));
                            $successRspns['statusCode'] = 4000;
                            $successRspns['message'] = trans('api.fitness.season_success');
                        }
                    }
                    $fuser->save();
                    //send alert for daily target achieved or failed
                    //if ($dailyChallenge->status == 1) {
                    //$this->saleseForceEmail($fuser, $dailyChallenge, $isDWinner);
                    $mail_data = [
                        'coupon_code' => $dailyChallenge->coupon_code,
                        'email' => $fuser->email,
                        'user_name' => $fuser->fname . ' ' . $fuser->lname,
                        'type' => ($isDWinner == true) ? 'fitness-success' : 'fitness-failed'
                    ];
                    sendCommonEmail($mail_data);
                    //}
                    //send alert for monthly target achieved
                    //send alert for season target achieved
                } else {
                    $successRspns['statusCode'] = 5000;
                    $successRspns['message'] = trans('api.fitness.no_active_challenge');
                }
                return response()->json($successRspns);
            }
            return errorResponse(trans('api.fitness.user_notfound'));
        }
        return errorResponse(trans('api.fitness.season_expired'));
    }

    private function saleseForceEmail(&$user, &$tracks, $isWinner) {
        $languages = ['en' => 'English', 'ar' => 'Arabic'];
        $param['email'] = $user->email;
        $param['language'] = $languages[app()->getLocale()];
        $param['firstName'] = $user->fname;
        $param['lastName'] = $user->lname;
        $param['fullName'] = $user->fname . ' ' . $user->lname;
        $param['completionDate'] = date('m/d/Y');
        $param['winningCode'] = $tracks->coupon_code;
        $param['isWinner'] = $isWinner;

        $sfResponse = $this->fitnessEmail($param);
        $tracks->sf_request = $param;
        $tracks->sf_response = $sfResponse;
        $tracks->save();

        return 1;
    }

    private function eloquaEmail(&$user, $code) {
        $languages = ['en' => 'english', 'ar' => 'arabic'];
        try {
            if (empty($code)) {
                $param = [
                    'elqFormName' => "2021FitnessChallengeNonCompletedForm",
                ];
            } else {
                $param = [
                    'elqFormName' => "2021FitnessChallengeWinnerForm",
                    'winningCode' => $code,
                ];
            }

            $param['elqSiteID'] = "681364326";
            $param['elqCookieWrite'] = 0;
            $param['elqCustomerGUID'] = 0;
            $param['firstName'] = $user->fname;
            $param['lastName'] = $user->lname;
            $param['fullName1'] = $user->fname . ' ' . $user->lname;
            $param['emailAddress'] = $user->email;
            $param['language'] = $languages[app()->getLocale()];

            //dd("https://s681364326.t.eloqua.com/e/f2?" . http_build_query($param));
            $payload = file_get_contents("https://s681364326.t.eloqua.com/e/f2?" . http_build_query($param));
            //dd($payload);
        } catch (\Exception $e) {
            //dd($e);
        }
        return 1;
    }

    public function syncSteps(Request $request) {
        $rules = [
            'drupal_id' => 'required|integer',
            'steps' => 'required|integer',
            'latitude' => 'required',
            'longitude' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        $today = date('Y-m-d');

        $season = FitnessSeason::select('id', 'target_steps', DB::raw('ST_CONTAINS(in_area, point(' . $request->latitude . ',' . $request->longitude . ')) as area'), 'geofence','is_next_day','start_time','end_time','end_date')
                //->where('end_date', '>=', $today)
                ->where('is_active', 1)
                ->first();

        if ($season) {
            $participated_date = $this->getParticipatedDate($season); 
            if ($season->end_date < $participated_date ) {
                return errorResponse(trans('api.fitness.season_expired'));
            }
            if ( empty($participated_date)) {
                return errorResponse(trans('api.fitness.no_active_challenge'));
            }
            if ($season->geofence) {
                if ($season->area <= 0) {
                    return errorResponse(trans('api.fitness.geofence'));
                }
            }
            $fuser = FitnessUser::where('drupal_id', $request->drupal_id)
                    ->where('fitness_season_id', $season->id)
                    ->first();
            if ($fuser) {                
                $dailyChallenge = FitnessDailyTracks::whereDate('participated_date', $participated_date)
                        ->where('fitness_user_id', $fuser->id)
                        ->whereIn('status', [0, 1])
                        ->first();
                if ($dailyChallenge) {
                    if ($request->steps > 0) {
                        // $monthChallenge = FitnessMonthlyTrack::where('fitness_user_id', $fuser->id)
                        //         ->whereYear('created_at', date('Y'))
                        //         ->whereMonth('created_at', date('m'))
                        //         ->first();
                        // $monthChallenge->steps = ($monthChallenge->steps - $dailyChallenge->steps) + $request->steps;
                        $fuser->total_steps = ($fuser->total_steps - $dailyChallenge->steps) + $request->steps;
                        $dailyChallenge->steps = $request->steps;
                        $dailyChallenge->save();
                        // $monthChallenge->save();
                        $fuser->save();
                        return successResponse(trans('api.fitness.synced'));
                    }
                }
                return errorResponse(trans('api.fitness.no_active_challenge'));
            }
            return errorResponse(trans('api.fitness.user_notfound'));
        }
        return errorResponse(trans('api.fitness.season_expired'));
    }

}
