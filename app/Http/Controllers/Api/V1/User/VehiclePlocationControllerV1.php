<?php

namespace App\Http\Controllers\Api\V1\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use Validator;
use App\VehiclePlocation;
use App\VehiclePsetting;
use App\Traits\VehicleCountTraits;
use DB;
use App\VehiclePlocationTiming;

class VehiclePlocationControllerV1 extends ApiController {

    use VehicleCountTraits;

    public function __construct() {
        $this->middleware('api');
    }

    /**
     * Post Request for the get count
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getCountV1(Request $request) {
        $rules = [
            'sort_type' => 'integer',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        $latitude = !empty($request->input('latitude')) ? $request->input('latitude') : null;
        $longitude = !empty($request->input('longitude')) ? $request->input('longitude') : null;
        $sort = !empty($request->sort_type) && $request->sort_type <= 3 ? $request->sort_type : 3;

        $vps = VehiclePsetting::first();
        if ($vps) {
            $lan = app()->getLocale();
            $vpls = VehiclePlocation::select('id', 'is_custom', 'vgs_id', "zone_names->{$lan} as zone", "names->{$lan} as name", "descriptions->{$lan} as description", 'total_capacity', 'inside', 'beco_name', 'latitude', 'longitude', 'closed', 'extras', \DB::raw('ST_AsText(polygons) as polygons_text'),'is_undefined_location',\DB::raw('IF(is_undefined_location, 0,ST_CONTAINS(polygons, point(' . $latitude . ',' . $longitude . '))) as inside_area'))
                    //->where('is_undefined_location', 0)
                    ->where('status', 1)
                    ->with(["services:id,vehicle_plocation_id,parking_service_id,beco_name,descriptions->{$lan} as description", "services.service:id,names->{$lan} as name,descriptions->{$lan} as description,icon"])
                    ->with(["images" => function ($q) use ($lan) {
                    $q->select('id', 'plocation_id', 'image');
                    $q->where('language', $lan)->orderBy('display_order');
                }]);
            if (!empty($latitude) && !empty($longitude)) {
                $vpls->addSelect(DB::raw("( 6371 * acos( cos( radians(" . $latitude . ") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(" . $longitude . ") ) + sin( radians(" . $latitude . ") ) * sin( radians( latitude ) ) ) ) AS distance"));
               // $vpls->whereRaw( 'ST_CONTAINS(polygons, point(' . $latitude . ',' . $longitude . ')) = 1');
            }
            if((!$request->has('type')) || $request->type != 'find-my-car'){
                $vpls->where('is_undefined_location', 0);
            }
            if ($sort == 1) {
                $vpls = $vpls->orderBy("zone_names->{$lan}", "asc");
            } else if ($sort == 2) {
                $vpls = $vpls->orderBy("zone_names->{$lan}", "desc");
            } else {
                if (!empty($latitude) && !empty($longitude)) {
                    $vpls->orderBy('inside_area', 'desc');
                    $vpls->orderBy('is_undefined_location', 'asc');
                    $vpls = $vpls->orderBy('distance', 'asc');
                } else {
                    $vpls = $vpls->orderBy('display_order');
                }
            }
            
            $vpls = $vpls->get();
            /*
            if ($vpls->isEmpty()) {
                $vpls = VehiclePlocation::select('id', 'is_custom', 'vgs_id', "zone_names->{$lan} as zone", "names->{$lan} as name", "descriptions->{$lan} as description", 'total_capacity', 'inside', 'beco_name', 'latitude', 'longitude', 'closed', 'extras', \DB::raw('ST_AsText(polygons) as polygons_text'),'is_undefined_location')
                    ->where('is_undefined_location', 1)
                    ->where('status', 1)
                    ->with(["services:id,vehicle_plocation_id,parking_service_id,beco_name,descriptions->{$lan} as description", "services.service:id,names->{$lan} as name,descriptions->{$lan} as description,icon"])
                    ->with(["images" => function ($q) use ($lan) {
                    $q->select('id', 'plocation_id', 'image');
                    $q->where('language', $lan)->orderBy('display_order');
                }]);
                if ($sort == 1) {
                    $vpls = $vpls->orderBy("zone_names->{$lan}", "asc");
                } else if ($sort == 2) {
                    $vpls = $vpls->orderBy("zone_names->{$lan}", "desc");
                } else {
                    $vpls = $vpls->orderBy('display_order');
                }                
                $vpls = $vpls->get();
            }
            */
            if (!$vpls->isEmpty()) {
                $has_inside = FALSE;
                //check api expired
                $lut = \Carbon\Carbon::parse($vps->last_updated_at)->addMinutes($vps->api_interval)->format('Y-m-d H:i:s');
                if (date('Y-m-d H:i:s') >= $lut || empty($vps->api_data)) {
                    //Expired call vgs and update
                    //$vpCounts = $this->getVGSVehicleCount($vpls->pluck('vgs_id')->toArray());
                    $vpCounts = $this->parkingDetails();
                    if (!empty($vpCounts)) {
                        $vps->api_data = $vpCounts;
                        $vps->last_updated_at = date('Y-m-d H:i:s');
                        $vps->save();
                    } else {
                        return errorResponse(trans('api.something_wrong'));
                    }
                }
                $vpls->each(function ($q) use (&$vps,&$has_inside) {
                    $current_time = date('H:i:s');
                    $prev_day =  date('N') - 1;
                    $prev_day = ($prev_day == 0) ? 7 : $prev_day;
                    /* https://mobiiworld-jira.atlassian.net/browse/GL28GT-815
                    $timing = VehiclePlocationTiming::where('vehicle_plocation_id', $q->id)->where('start_time', '<=', $current_time)->where('end_time', '>=', $current_time)->where('day', date('N'))->where('is_next_day',0)->first(); // normal scenario
                    if(empty($timing)){ // check current and next day conditions
                        $timing = VehiclePlocationTiming::where('vehicle_plocation_id', $q->id)->where('is_next_day',1)->where(function($quer) use($current_time, $prev_day){
                            $quer->where(function($query) use($current_time){
                                $query->where('start_time', '<=', $current_time)->where('day', date('N'));                            
                            })
                            ->orWhere(function($query) use($current_time, $prev_day){
                                $query->where('end_time', '>=', $current_time)->where('day', $prev_day);                            
                            });
                        })->first();
                    }
                    
                    $q->closed = !empty($timing) ? 0 : 1;
                    */

                    $q->closed = 0; // always open : https://mobiiworld-jira.atlassian.net/browse/GL28GT-815
                    if ($q->is_custom) {
                        $q->vacant = $q->total_capacity - $q->inside;
                    } else {
                        if (isset($vps->api_data[$q->vgs_id])) {
                            $q->inside = $q->total_capacity - $vps->api_data[$q->vgs_id][0]['Availableslots'];
                            $q->vacant = $vps->api_data[$q->vgs_id][0]['Availableslots'];
                        } else {
                            $q->vacant = $q->total_capacity;
                        }
                    }
                    $q->polygons = !empty($q->polygons_text) ? $this->getPolygonCoordinates($q->polygons_text) : [];
                    if($q->inside_area == 1){
                        $has_inside = TRUE;
                    }
                });
                //dd($has_inside,$vpls);
                if((!$has_inside) && $vpls->last()->is_undefined_location == 1){
                    $undefined_item = $vpls->pop();
                    $vpls->prepend($undefined_item);
                }
                $globArea = \App\VehicleParea::selectRaw('ST_AsText(in_area) AS shape,latitude,longitude')->first();
                $globArea = !empty($globArea) ? ["center_latitude" => $globArea->latitude, "center_longitude" => $globArea->longitude, "gv_area" => $this->getPolygonCoordinates($globArea->shape)] : ["center_latitude" => null, "center_longitude" => null, "gv_area" => []];
                return successResponse(trans('api.success'), ["locations" => $vpls] + $globArea);
            }
        }
        return errorResponse(trans('api.no_data'));
    }

    public function getPolygonCoordinates($pols) {
        preg_match_all('/([\d\.]+)\s([\d\.]+)/', $pols, $matches);
        $coordinates = [];
        for ($i = 0; $i < count($matches[0]); $i++) {
            $coordinates[] = [
                'latitude' => $matches[1][$i],
                'longitude' => $matches[2][$i],
            ];
        }
        return $coordinates;
    }

}
