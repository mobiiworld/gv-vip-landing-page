<?php

namespace App\Http\Controllers\Api\V1\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use Validator;
use App\FifaMatch;
use Carbon\Carbon;

class FifaMatchesController extends ApiController {

    public function __construct() {
        $this->middleware('api');
    }

    public function index(Request $request) {
        $rules = [
            'from_date' => 'required|date:y-m-d',
            'to_date' => 'required|date:y-m-d',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        $lan = app()->getLocale();
        $season = \App\Season::where('status', 1)->first();

        $matches = FifaMatch::select('id', "titles->{$lan} as title", 'match_date', 'match_time', 'performance_id')
                ->where('season_id', $season->id);

        if (!empty($request->from_date)) {
            if ($request->from_date == date('Y-m-d')) {
                $matches->whereRaw("CONCAT_WS(' ',match_date,match_time) >= '" . date('Y-m-d H:i:00') . "'");
            } else {
                $matches->where('match_date', '>=', $request->from_date);
            }
        }
        if (!empty($request->to_date)) {
            $matches->where('match_date', '<=', $request->to_date);
        }

        return successResponse(trans('api.success'), $matches->orderBy('match_date')->orderBy('match_time')->get());
    }

}
