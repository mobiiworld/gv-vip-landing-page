<?php

namespace App\Http\Controllers\Api\V1\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use Validator;
use App\CabanaDay;
use App\CabanaProduct;
use Carbon\Carbon;

class CabanaController extends ApiController {

    public function __construct() {
        $this->middleware('api');
    }

    public function getSlots(Request $request) {
        $rules = [
            'from_date' => 'required|date:y-m-d',
            'from_time' => 'required|date_format:H:i',
            'quantity' => 'required|integer',
            'hours' => 'required|integer',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        $product = CabanaProduct::where('hours', $request->hours)->first();
        if (empty($product)) {
            return errorResponse(trans('api.cabana.no_product'));
        }

        $cabanaDays = CabanaDay::select('*')
                ->whereDate('start_dt', $request->from_date)
                ->where('status', 1)
                ->first();
        if ($cabanaDays) {
            $cabanaConfig = \App\ApiConfiguration::where('name', 'work_station_id')
                    ->orWhereRaw('name like "%cabana_%"')
                    ->pluck('value', 'name');
            $cabanaConfig['cabana_fullday_rate'] = (double) $cabanaConfig['cabana_fullday_rate'];
            $fromDt = date('Y-m-d H:i:00', strtotime($request->from_date . ' ' . $request->from_time));
            $endDt = date('Y-m-d H:i:00', strtotime("+{$request->hours} hour", strtotime($fromDt)));
            if ($fromDt >= $cabanaDays->start_dt) {
                if ($endDt > $cabanaDays->end_dt) {
                    return errorResponse(trans('api.cabana.exceeds'));
                }
                $slots = $this->getCabanaVgs($request, $cabanaConfig);
                if (isset($slots->Answer->GetProductResourceAvailability->AvailabilitySlotList)) {
                    if (in_array(strtolower(date('l', strtotime($request->from_date))), ['friday', 'saturday'])) {
                        $slotRate = $product->weekend_price;
                        $prodId = $cabanaConfig['cabana_anyday_product_id'];
                    } else {
                        $slotRate = $product->workingday_price;
                        $prodId = $cabanaConfig['cabana_value_product_id'];
                    }

                    $vipPrices = json_decode(json_encode($product->vip_prices, JSON_NUMERIC_CHECK));

                    $data = [
                        'best_match' => null,
                        'full_evening' => null,
                        'also_available' => []
                    ];
                    $slots = $slots->Answer->GetProductResourceAvailability->AvailabilitySlotList;
                    if (count($slots) == 1) {
                        if ($slots[0]->Quantity >= $request->quantity) {
                            $data['best_match'] = [
                                'start_dt' => $fromDt,
                                'end_dt' => ($endDt <= $cabanaDays->end_dt) ? $endDt : $cabanaDays->end_dt,
                                'slot_rate' => $slotRate,
                                'total_price' => $request->quantity * $slotRate,
                                'vip_price' => $vipPrices->generic_price,
                                'vip_total_price' => $request->quantity * $vipPrices->generic_price,
                                'product_id' => $prodId
                            ];
                            /* $data['full_evening'] = [
                              'start_dt' => $fromDt,
                              'end_dt' => $cabanaDays->end_dt,
                              'full_evening_rate' => $cabanaConfig['cabana_fullday_rate'],
                              'total_price' => $request->quantity * $cabanaConfig['cabana_fullday_rate'],
                              'product_id' => $cabanaConfig['cabana_fullday_product_id']
                              ]; */

                            //Also available
                            $fromDt = date('Y-m-d H:i:00', strtotime('+15 minutes', strtotime($fromDt)));
                            $j = 0;
                            for ($fd = $fromDt; $fd <= $cabanaDays->end_dt; $fd = date('Y-m-d H:i:00', strtotime('+15 minutes', strtotime($fd)))) {
                                $ed = date('Y-m-d H:i:00', strtotime("+{$request->hours} hour", strtotime($fd)));
                                if ($ed <= $cabanaDays->end_dt && $j <= 3) {
                                    array_push($data['also_available'], [
                                        'start_dt' => $fd,
                                        'end_dt' => $ed,
                                        'slot_rate' => $slotRate,
                                        'total_price' => $request->quantity * $slotRate,
                                        'vip_price' => $vipPrices->generic_price,
                                        'vip_total_price' => $request->quantity * $vipPrices->generic_price,
                                        'product_id' => $prodId
                                    ]);
                                    $j++;
                                } else {
                                    break;
                                }
                            }
                        } else {
                            return errorResponse(trans('api.cabana.no_slots'));
                        }
                    } else {
                        $endDt = ($endDt <= $cabanaDays->end_dt) ? $endDt : $cabanaDays->end_dt;
                        array_walk_recursive($slots, function (&$slot) {
                            $slot->DateTimeFrom = str_replace('T', ' ', strstr($slot->DateTimeFrom, '.', true)); //substr($slot->DateTimeFrom, 0, strpos($slot->DateTimeFrom, "."));
                            $slot->DateTimeTo = str_replace('T', ' ', strstr($slot->DateTimeTo, '.', true));
                        });

                        $exactMatch = $this->findBestMatch($slots, $fromDt, $endDt, $request->quantity);
                        //dd($exactMatch);
                        $data['best_match'] = (!empty($exactMatch)) ? [
                            'start_dt' => $fromDt,
                            'end_dt' => $endDt,
                            'slot_rate' => $slotRate,
                            'total_price' => $request->quantity * $slotRate,
                            'vip_price' => $vipPrices->generic_price,
                            'vip_total_price' => $request->quantity * $vipPrices->generic_price,
                            'product_id' => $prodId
                                ] : null;
                        //check for full day
                        //$exactMatch = $this->findBestMatch($slots, $fromDt, $cabanaDays->end_dt, $request->quantity);
                        //$data['full_evening'] = (!empty($exactMatch)) ? ['start_dt' => $fromDt, 'end_dt' => $cabanaDays->end_dt, 'full_evening_rate' => $cabanaConfig['cabana_fullday_rate'], 'total_price' => $request->quantity * $cabanaConfig['cabana_fullday_rate'], 'product_id' => $cabanaConfig['cabana_fullday_product_id']] : null;
                        //Also available
                        $fromDt = date('Y-m-d H:i:00', strtotime('+15 minutes', strtotime($fromDt)));
                        $j = 0;
                        for ($fd = $fromDt; $fd <= $cabanaDays->end_dt; $fd = date('Y-m-d H:i:00', strtotime('+15 minutes', strtotime($fd)))) {
                            $ed = date('Y-m-d H:i:00', strtotime("+{$request->hours} hour", strtotime($fd)));
                            if ($ed <= $cabanaDays->end_dt && $j <= 3) {
                                $alsoav = $this->findBestMatch($slots, $fd, $ed, $request->quantity);
                                if (!empty($alsoav)) {
                                    array_push($data['also_available'], [
                                        'start_dt' => $fd,
                                        'end_dt' => $ed,
                                        'slot_rate' => $slotRate,
                                        'total_price' => $request->quantity * $slotRate,
                                        'vip_price' => $vipPrices->generic_price,
                                        'vip_total_price' => $request->quantity * $vipPrices->generic_price,
                                        'product_id' => $prodId
                                    ]);
                                    $j++;
                                }
                            } else {
                                break;
                            }
                        }
                    }

                    if (!array_filter($data)) {
                        return errorResponse(trans('api.cabana.no_slots'));
                    }
                    $data['allow_vip'] = ($product->allow_vip) ? true : false;
                    $data['vip_product_id'] = $cabanaConfig['cabana_vip_product_id'];
                    $data['vip_prices'] = $vipPrices;
                    return successResponse(trans('api.success'), $data);
                }
            } else {
                return errorResponse(trans('api.cabana.not_started'));
            }
        }
        return errorResponse(trans('api.cabana.no_slots'));
    }

    private function getCabanaVgs(&$request, &$cabanaConfig) {
        $url = config('accountConfig.pprUrl') . "&cmd=Resource";
        $requestData = array(
            'Header' => array('WorkstationId' => $cabanaConfig['work_station_id']),
            'Request' => array(
                'Command' => 'GetProductResourceAvailability',
                'GetProductResourceAvailability' => array(
                    "DateFrom" => $request->from_date,
                    "DateTo" => date('Y-m-d', strtotime($request->from_date . ' + 1 days')),
                    "ProductId" => $cabanaConfig['cabana_search_slot_product_id']
                )
            )
        );
        $header = array('Content-Type: application/json');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        $output = curl_exec($ch);
        return json_decode($output);
    }

    private function findBestMatch($slots, $fromDt, $endDt, $quantity) {
        $matches = [];
        foreach ($slots as $slot) {
            if (($fromDt >= $slot->DateTimeFrom && $fromDt <= $slot->DateTimeTo) ||
                    ($endDt >= $slot->DateTimeFrom && $endDt <= $slot->DateTimeTo) ||
                    ($slot->DateTimeFrom >= $fromDt && $slot->DateTimeFrom <= $endDt) ||
                    ($slot->DateTimeTo >= $fromDt && $slot->DateTimeTo <= $endDt)
            ) {
                array_push($matches, $slot);
            }
        }
        $matches = array_unique($matches, SORT_REGULAR);
        usort($matches, function ($element1, $element2) {
            $datetime1 = strtotime($element1->DateTimeFrom);
            $datetime2 = strtotime($element2->DateTimeFrom);
            return $datetime1 - $datetime2;
        });
        //dd($matches);
        //checking consecutive values for DateTimeFrom and DateTimeTo along with user quqntity match
        $foundExactMatch = true;
        for ($i = 0; $i < count($matches) - 1; $i++) {
            if ($matches[$i]->DateTimeTo == $matches[$i + 1]->DateTimeFrom) {
                if (($matches[$i]->Quantity < $quantity) || ($matches[$i + 1]->Quantity < $quantity)) {
                    $foundExactMatch = false;
                }
            } else {
                $foundExactMatch = false;
            }
        }

        //checking first DateTimeFrom and last DateTimeTo is in between user selection
        if ($foundExactMatch) {
            //dd($matches[0]->DateTimeFrom);
            if (($fromDt >= $matches[0]->DateTimeFrom) && ($endDt <= $matches[count($matches) - 1]->DateTimeTo)) {
                return 'sucess';
            }
        }
        return null;
    }

}
