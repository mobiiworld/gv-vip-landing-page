<?php

namespace App\Http\Controllers\Api\V1\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use Validator;
use App\User;
use App\Traits\GvpTraits;
use App\Traits\CarTraits;
use App\Traits\AccountActivationTraits;
use App\Jobs\ProcessSync;
use Carbon\Carbon;
use PKPass\PKPass;
use App\Wonderpass;
use App\Pack;
use Mail;
use App\Mail\CarMail;

class PassController extends ApiController
{
    use GvpTraits;
    use CarTraits, AccountActivationTraits;
    public function __construct() {
        $this->middleware('api');
    }

    public function getApplePass(Request $request){
        $rules = [
            'eventTitle' => 'required',
            'ticketName' => 'required',
            'datePurchase' => 'required',
            'validUntil' => 'required',
            'fileName' => 'required',
            'mediaCode' => 'required'
        ];


        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        $langCode  = !empty($request->langCode) ? $request->langCode : 'en';

        if($langCode == 'ar'){
            $codeLabel = "رمز التذكرة";
            $datePurchaseLabel = "تاريخ الشراء";
            $validUntilLabel = "صالحة لغاية";
            $eventLabel = "الفعاليات";
        }else {
            $codeLabel = 'Ticket Code';
            $datePurchaseLabel = 'Date Purchase';
            $validUntilLabel = 'Valid Until';
            $eventLabel = 'Event';
        }

        /*$codeType ='';
        if($request->codeType == 'QRCode'){
            $codeType = 'PKBarcodeFormatQR';
        }elseif($request->codeType == 'BarCode'){
            $codeType = 'PKBarcodeFormatPDF417';
        }elseif($request->codeType == 'PromoCode'){

        }*/

        //$path = storage_path('certs/GVTicketPass.p12');
        $path = storage_path('certs/GVTicketAddToWallet.p12');
        $pass = new PKPass($path, env('PKPASS_TOKEN'));

        $relevantDate = date('Y-m-d').'T23:59-01:00'; //"2011-12-08T13:00-08:00";


          $data = [
                   "formatVersion" => 1,
                   "passTypeIdentifier" => env('PASS_IDENTIFIER'),
                   "serialNumber" => $request->fileName,
                   "teamIdentifier" => env('TEAM_IDENTIFIER'),
                   "relevantDate" => $relevantDate,
                   "locations" => [
                      ],
                   "barcode" => [
                                  "message" => $request->mediaCode,
                                  "format" => 'PKBarcodeFormatQR',
                                  "messageEncoding" => "iso-8859-1"
                               ],
                   "organizationName" => "Global Village",
                   "description" => $request->ticketName,
                   "labelColor" => "rgb(255, 255, 255)",
                   "foregroundColor" => "rgb(255, 255, 255)",
                   "backgroundColor" => "rgb(24, 58, 117)",
                   "eventTicket" => [
                                     "primaryFields" => [
                                        [
                                           "key" => "event",
                                           "label" => $eventLabel,
                                           "value" => $request->eventTitle
                                        ]
                                     ],
                                     "secondaryFields" => [
                                              [
                                                 "key" => "datepurchase",
                                                 "label" => $datePurchaseLabel,
                                                 "value" => $request->datePurchase
                                              ],
                                              [
                                                 "key" => "validuntil",
                                                 "label" => $validUntilLabel,
                                                 "value" => $request->validUntil
                                              ]
                                           ],
                                      "auxiliaryFields" => [
                                              [
                                                "key" => "level",
                                                "label" => $codeLabel,
                                                "value" => $request->mediaCode
                                              ]
                                            ]
                                  ]
                ];



        //echo json_encode($data); exit;

        $pass->setName($request->fileName);
        $pass->setData($data);

        // Add files to the pass package
        $pass->addRemoteFile(url('images/pass/icon.png'));
        $pass->addRemoteFile(url('images/pass/icon@2x.png'));
        $pass->addRemoteFile(url('images/pass/logo@3x.png'));

        $pass->addRemoteFile(url('images/pass/thumbnail.png'));
        $pass->addRemoteFile(url('images/pass/thumbnail@2x.png'));

        $pass->addRemoteFile(url('images/pass/background.png'));
        $pass->addRemoteFile(url('images/pass/background@2x.png'));

        // Create and output the pass
        if(!$pass->create(true)) {
          echo 'Error: ' . $pass->getError();
        }

        exit;
    }

    public function getCard(Request $request) {
        set_time_limit(0);
        $rules = [
            'actualmediacode' => 'required',

        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }
        $data = [];
        $wonderpass = Wonderpass::where('card_number', $request->actualmediacode)->first();
        if(!empty($wonderpass)) {
            $pack = Pack::where('id', $wonderpass->pack_id)->first();
            if(!empty($pack)) {
                $data['pack_uid'] =  $pack->id;
                $data['pack_number'] = $pack->pack_number;
                $data['category'] =  $pack->category;
            }
        }
        return successResponse('success', $data);
    }

    public function getCardDetails(Request $request) {
        set_time_limit(0);
        $rules = [
            'actualmediacode' => 'required',

        ];
        $actualmediacode = explode(',',$request->actualmediacode);

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }
        $data = [];
        $wonderpass = Wonderpass::whereIn('card_number', $actualmediacode)->get();
        if(!empty($wonderpass)) {
            foreach ($wonderpass as $key => $wonderpas) {
                $pack = Pack::where('id', $wonderpas->pack_id)->first();
                if (!empty($pack)) {
                    $data[$key]['pack_uid'] =  $pack->id;
                    $data[$key]['pack_number'] = $pack->pack_number;
                    $data[$key]['category'] =  $pack->category;
                    $data[$key]['actualmediacode'] =  $wonderpas->card_number;
                }
            }
        }
        return successResponse('success', $data);
    }

}
