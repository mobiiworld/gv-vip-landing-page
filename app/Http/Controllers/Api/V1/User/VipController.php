<?php

namespace App\Http\Controllers\Api\V1\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use Validator;
use App\User;
use App\NewUserCar;
use App\Traits\GvpTraits;
use App\Traits\CarTraits;
use App\Traits\AccountActivationTraits;
use App\Jobs\ProcessSync;
use Carbon\Carbon;
use DB;
use App\Traits\DrupalTraits;

use Mail;
use App\Mail\CarMail;
use App\Traits\PackBlockTraits;
use App\Traits\SalesForceTraits;
use App\Traits\VipTraits;

class VipController extends ApiController
{
     use GvpTraits;
	 use CarTraits, DrupalTraits,AccountActivationTraits,PackBlockTraits,SalesForceTraits,VipTraits;
	 public function __construct() {
	    $this->middleware('api');
	 }

     public function getPlateCodes(){
        $carCountry = \App\CarCountry::select('id','code','name_en','name_ar','plate_prefix_new')->get();
        $finalData = [];
        if(!empty($carCountry )){
            foreach($carCountry as $country){
                $clist = [
                    "name" => $country->name_en,
                    "nameIntl" => [
                        "ar" => $country->name_ar,
                        "en" => $country->name_en
                    ],
                    "code" => $country->code
                ];
                if(strtolower($country->code) == 'uae'){
                    $emirate = \App\Emirate::select('id','code','name_en','name_ar','plate_prefix')->get();
                    $emList = [];
                    foreach($emirate as $em){
                        $emList[] = [
                            "name" => $em->name_en,
                            "nameIntl" => [
                                "ar" => $em->name_ar,
                                "en" => $em->name_en
                            ],
                            "code" => $em->code,
                            "prefixes" => array_values(json_decode($em->plate_prefix,true))
                        ]; 
                    }
                    $clist['cities'] = $emList;
                }else{
                    $clist['prefixes'] = array_values(json_decode($country->plate_prefix_new,true));
                }

                $finalData[] = $clist;
            }
        }
        $data["countries"] = $finalData;
        return successResponse(trans('api.success'), $data);
     }
	 public function generateOtp(Request $request){        
        $rules = [
            'vip_pack_number' => 'required',
            'activation_code' => 'required',
            'email' => 'required|email:rfc,dns,strict,filter'
        ];

        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) {
            return errorResponse( trans('web.required_fields'));
        }

        return $this->generateGuestOtp($request);
    }

    /*public function validateOtp(Request $request){        
        $rules = [
            
            'otp' => 'required',
            'email' => 'required|email'
        ];

        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) {
            return errorResponse( trans('web.required_fields'));
        }

        return $this->validateGuestOtp($request);
    }*/
    
	 public function checkActivationCode(Request $request) {
        $rules = [
            'vip_pack_number' => 'required',
            'activation_code' => 'required',
            'email' => 'required|email:rfc,dns,strict,filter',
            'otp' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return errorResponse( trans('web.required_fields'));
        }
        $season = \App\Season::where('status',1)->first();
        $seasonId = (!empty($season)) ?  $season->id : NULL;
        if(empty($season)){
            return errorResponse( trans('web.pack.notfound'));
        }
        if(!$season->pack_activation_enabled){
            $message = trans('web.pack.notfound');
            if(!empty($season->pack_activation_disbaled_msg_en)){
                $message = (app()->getLocale() == 'ar' && !empty($season->pack_activation_disbaled_msg_ar)) ? $season->pack_activation_disbaled_msg_ar : $season->pack_activation_disbaled_msg_en;
            }
            return errorResponse( $message );
        }
        $pack = \App\Pack::where('pack_number', $request->vip_pack_number)
                ->where('activation_code', $request->activation_code)
                ->where('season_id',$seasonId)
                ->where('status', 1)
                ->first();
        if ($pack) {
            $otp_response = $this->validateGuestOtp($request);
            
            if($otp_response['statusCode'] != 1000){
                return errorResponse(trans('passwords.otp.validate_failed'));
            }
            $data['otp_id'] = $otp_response['otp_id'];
            if ($pack->used) {
                return errorResponse( trans('web.pack.used'));
            }
            $prevSeasonOrder = $season->ordering - 1;
            //$prevSeason = \App\Season::where('status',0)->where('ordering',$prevSeasonOrder)->first();
            $user = $car = $allCars =  [];
          
            $user = $this->getDrupalUser($request->email);
            $drupal_user_id = (isset($user['drupal_user_id'])) ? $user['drupal_user_id'] : NULL;

            if(!empty($drupal_user_id)){ 
               
                $allCarData =  \App\NewUserCar::where('drupal_user_id',$drupal_user_id)
                                ->orderby('created_at','asc')
                                ->select('plate_number','country_id','emirate_id','plate_prefix','type','id')
                                //->distinct()
                                ->get();
               // dd(\DB::getQueryLog());
                if(!empty($allCarData)){
                    foreach($allCarData as $cd){
                        $country = \App\CarCountry::find($cd->country_id);
                        $emirate = \App\Emirate::find($cd->emirate_id);
                        $allCars[] = array(
                            "id" => $cd->id,
                            "type" => $cd->type,
                            "plate_number" => $cd->plate_number,
                            "country" => (!empty($country )) ? $country->code : NULL,
                            "plate_prefix" => $cd->plate_prefix,
                            "emirate" =>  (!empty($emirate )) ? $emirate->code : NULL
                        );
                    }
                }                    
                    
            }

            $emirate = \App\Emirate::select('id','code','name_en','name_ar')->get();
            $data['emirates'] = $emirate;
            $data['user'] = (!empty($user)) ?  $user : NULL;
            $data['car'] = (!empty($car)) ?  $car : NULL;
            $data['allCars'] = (!empty($allCars)) ?  $allCars : NULL;
            $data['pack'] = $pack;

            return successResponse('success',$data);
        }

        return errorResponse( trans('web.pack.notfound'));
    }

    public function activatePack(Request $request) {
        set_time_limit(0);
        $uae = \App\Country::select('id',  'code', 'dial_code')->where('dial_code','+971')->first();
        $uaeId = $uae->id;
        $rules = [
            'vip_pack_number' => 'required',
            'activation_code' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email:rfc,dns,strict,filter',
            //'confirm_email' => 'required',
            //'mobile_dial_code' => 'required',
            'mobile' => 'required',
            //'gender' => 'in:Male,Female,male,female',
            //'nationality' => 'required',
            //'residence' => 'required',
            //'dob' => 'required',
            'otp_id' => 'required',
            //'emirate' => 'required_if:residence,'.$uaeId
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse( trans('web.required_fields'),$errorMessage);
        }

        // if ($request->email != $request->confirm_email) {
        //     return errorResponse( trans('web.email_not_matched'));
        // }

        // if (empty($request->terms)) {
        //     return response()->json(["status" => false, "message" => trans('web.read_terms')]);
        // }

        if ((!empty($request->dob)) && time() < strtotime('+18 years', strtotime($request->dob))) {
            return response()->json(["status" => false, "message" => trans('web.dob_error')]);
        }


        $locale = app()->getLocale();
        $season = \App\Season::where('status',1)->first();
        if(empty($season)){
            return errorResponse( trans('web.pack.notfound'));
        }
        if(!$season->pack_activation_enabled){
            $message = trans('web.pack.notfound');
            if(!empty($season->pack_activation_disbaled_msg_en)){
                $message = (app()->getLocale() == 'ar' && !empty($season->pack_activation_disbaled_msg_ar)) ? $season->pack_activation_disbaled_msg_ar : $season->pack_activation_disbaled_msg_en;
            }
            return errorResponse( $message );
        }

        $seasonId = (!empty($season)) ?  $season->id : NULL;
        $pack = \App\Pack::where('pack_number', $request->vip_pack_number)
                ->where('activation_code', $request->activation_code)
                ->where('status', 1)
                ->where('season_id',$seasonId)
                ->first();
        if ($pack) {
            $user_otp = \App\UserOtp::where('id',$request->otp_id)->where('email',$request->email)->first();
            if(empty($user_otp)){
                return errorResponse( trans('passwords.otp.unauthorized'));
            }
            
            if ($pack->used) {
                return errorResponse( trans('web.pack.used'));
            }

            //check the car_activation_allowed is matching the user request
            $userAddedcars =  0;
            //check the car_activation_allowed is matching the user request
            if(!empty($request->user_car)){
                $userAddedcars += count(array_filter($request->user_car));
            }
            if(!empty($request->registered_car_ids)){
                $userAddedcars += count(array_filter($request->registered_car_ids));
            }
            if($userAddedcars == 0){
                return errorResponse( trans('web.required_fields'),'User car not added');
            }
            if ($userAddedcars > $pack->car_activation_allowed) {
                return errorResponse( trans('web.pack.car_allowed', ['count' => $pack->car_activation_allowed]));
            }

            $countryOfResidence = $emirateOfResidence = $nationality = '';

            try {
                $user_otp->delete();
                $user = User::where('email', $request->email)->where("season_id",$seasonId)->first();
                if(empty($user)){
                    $user = new User();
                    $user->email = $request->email;
                }
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->email = $request->email;
                $user->mobile_dial_code = $request->mobile_dial_code;
                $user->mobile = str_replace(' ', '', $request->mobile);
                $user->gender = (!empty($request->gender)) ? strtolower($request->gender) : NULL;
                $user->dob = (!empty($request->dob)) ? date('Y-m-d', strtotime($request->dob)) : NULL;

                $nationality_code = (strtolower($request->nationality) == 'are') ? 'UAE' : $request->nationality;
                $country = \App\Country::where('code',$nationality_code)->first();
                // if(empty($country)){
                //     $country = \App\Country::where('code','UAE')->first();
                // }
                $nationality = (!empty($country)) ? $country->elq_code : '';

                $user->nationality_id = !empty($country) ? $country->id : NULL;
                $res_code =  (strtolower($request->residence) == 'are') ? 'UAE' : $request->residence;
                $residence = \App\Country::where('code',$res_code)->first();
                // if(empty($residence)){
                //     $residence = \App\Country::where('code','UAE')->first();
                // }
                $countryOfResidence = (!empty($residence)) ? $residence->elq_code : '';

                $user->residence_id = !empty($residence) ? $residence->id : NULL;
                //$user->pack_id = $pack->id;
                $user->season_id = $seasonId;
                $user->recieve_updates = ($request->has('agree_offers') && $request->agree_offers == true) ? true : false;
                $remirate = \App\Emirate::where('code',$request->emirate)->first();
                $emirateOfResidence = (isset($remirate->code)) ? $remirate->code : NULL;
                $user->emirate_id = ($user->residence_id == $uaeId && !empty( $remirate)) ?  $remirate->id :NULL;
                $user->device_type = $request->source;
                $user->save();

                $userPack = new \App\UserPack();
                $userPack->user_id = $user->id;
                $userPack->pack_id = $pack->id;
                $userPack->save();

                $user->user_pack_id = $userPack->id;
                
                $pack->used = true;
                $pack->save();
                $drupal_user_id = $user->drupal_user_id;
                if(empty($drupal_user_id)){
                    $duser = $this->getDrupalUser($request->email);
                    $drupal_user_id = (isset($duser['drupal_user_id'])) ? $duser['drupal_user_id'] : NULL;
                }

                //Save user cars
                
                if(!empty($request->registered_car_ids)){
                    foreach($request->registered_car_ids as $ek=>$carId){ 
                        $car = \App\NewUserCar::where('id',$carId)->where('drupal_user_id',$drupal_user_id)->first();                       
                        if(!empty($carId) && !empty($car)){
                            \App\UserCarPack::firstOrCreate([
                                'pack_id' => $pack->id,
                                'user_car_id' => $carId
                            ]);
                        }
                        
                    }
                }
                if(!empty($request->user_car)){
                    foreach ($request->user_car as $k => $v) {
                        if (!empty($v)) {
                            $carCountry = \App\CarCountry::where('code',$v['car_country'])->first();
                            $country_id = !empty($carCountry) ? $carCountry->id : '';
                            $emirate_id = NULL;
                            if (!empty($v['emirate'])) {
                                $emirate = \App\Emirate::where('code',$v['emirate'])->first();
                                $emirate_id = !empty($emirate) ? $emirate->id : '';
                            }
                            $type = 1;
                            if(!empty($v['type'])){
                                $type = $v['type'];
                            }

                            $plate_prefix = !empty($v['plate_prefix']) ? $v['plate_prefix'] : '';
                            $plate_number = !empty($v['plate_number']) ? $v['plate_number'] : '';

                            if(!empty($drupal_user_id)){
                                $user_car = \App\NewUserCar::firstOrCreate([
                                    'drupal_user_id' => $drupal_user_id,                                    
                                    'type' => $type,
                                    'country_id' => $country_id,
                                    'emirate_id' => $emirate_id,                
                                    'plate_prefix' => $plate_prefix,
                                    'plate_number' => $plate_number,
                                ]);
                            }else{
                                $user_car = new NewUserCar();
                            }
                            $user_car->user_id = $user->id;                        
                            $user_car->drupal_user_id = $drupal_user_id;                     
                            $user_car->type = $type;
                            $user_car->country_id = $country_id;
                            $user_car->emirate_id = $emirate_id;
                            $user_car->plate_prefix = $plate_prefix;
                            $user_car->plate_number = $plate_number;
                            $user_car->save();
    
                            $userCarPack = \App\UserCarPack::firstOrCreate([
                                'pack_id' => $pack->id,
                                'user_car_id' => $user_car->id
                            ]);
                            
                        }
                    }
                }

            } catch (\Exception $e) {

                return errorResponse( $e->getMessage());
            }
            $success_image = asset('web/images/congrulation.svg');
            $success_title = trans('web.pack.modal_head');            
            $success_btn = trans('content.ok');
            $name = $user->first_name . ' ' . $user->last_name;
            $pType = trans("web.pack_cat_for_popup.{$pack->category}");
            $success_message = trans('web.activation_success',['pack_type' => $pType, 'user_name' => $name, 'pack_number' => $pack->pack_number]); 
            $success_data = \App\DynamicText::whereIn('slug',['mobile-activation-success-title','mobile-activation-success-message','mobile-activation-success-image','mobile-activation-success-button'])->get()->keyBy('slug')->toArray();
            if(!empty($success_data['mobile-activation-success-title'])){
                $success_title = $success_data['mobile-activation-success-title']['text_'.$locale];
            }
            
            if(!empty($success_data['mobile-activation-success-image'])){
                $success_image = $success_data['mobile-activation-success-image']['text_'.$locale];
            }
            if(!empty($success_data['mobile-activation-success-button'])){
                $success_btn = $success_data['mobile-activation-success-button']['text_'.$locale];
            }

            if(!empty($success_data['mobile-activation-success-message'])){
                $success_message = $success_data['mobile-activation-success-message']['text_'.$locale];
                $success_message = str_replace(':user_name', $name, $success_message);
                $success_message = str_replace(':pack_type', $pType, $success_message);
                $success_message = str_replace(':pack_number', $pack->pack_number, $success_message);
            }

            $data = [
                'name' => $user->first_name . ' ' . $user->last_name,
                'pack_type' => trans("web.pack.{$pack->category}"),
                'pack_id' => $pack->pack_number,
                'pack_url' => route('my.activated.pack', [$locale, $userPack->id]),                
                'success_popup' => [
                    'success_image' => $success_image,
                    'success_title' => $success_title,
                    'success_message' => $success_message,
                    'success_btn' => $success_btn
                ]
            ];
           


            try {               

                $agree_offers = ($request->has('agree_offers')  && $request->agree_offers == true) ? true : false;
                
                $sales_force_ref_id = $this->vipSubmissionToSalesForce($user,$pack,$agree_offers,'Mobile App','vip_activation',$request->source);

                $userPack->sales_force_ref_id = (!empty($sales_force_ref_id)) ? $sales_force_ref_id : NULL;
                $userPack->save();
            } catch (\Exception $e) {
                \Log::error("catch exception: ".$e->getMessage());
            }
            try {
                $jobObj = new \StdClass;
                $jobObj->packId = $pack->id;
                $job = (new ProcessSync($jobObj))
                        ->delay(Carbon::now()->addSeconds(1));
                dispatch($job);
            } catch (\Exception $e) {

            }
        $packs = $this->getVIPUserPacks($user->email,true,'highest');
        $data['packs'] = (!empty($packs)) ? $packs : NULL;

            return successResponse('success',$data);
        }
        return errorResponse( trans('web.pack.notfound'));
    }
}
