<?php

namespace App\Http\Controllers\Api\V1\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use Validator;
use App\User;
use App\Traits\GvpTraits;
use App\Traits\CarTraits;
use App\Traits\AccountActivationTraits;
use App\Jobs\ProcessSync;
use Carbon\Carbon;
use DB;
use App\Traits\DrupalTraits;

use Mail;
use App\Mail\CarMail;
use App\Traits\PackBlockTraits;
use App\Traits\SalesForceTraits;
use App\Traits\VipTraits;

class HomeController extends ApiController
{
     use GvpTraits;
     use CarTraits, DrupalTraits,AccountActivationTraits,PackBlockTraits,SalesForceTraits,VipTraits;
	 public function __construct() {
        $this->middleware('api');
     }

     public function userPacks(Request $request){
        $rules = [
            'email' => 'required',

        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        $season = \App\Season::where('status',1)->first();
        $seasonId = (!empty($season)) ?  $season->id : NULL;
        $packs = $this->getVIPUserPacks($request->email,false,'lowest');
        $data['packs'] = (!empty($packs)) ? $packs : NULL;
        return successResponse('success', $data);
    }

    public function userPackDetails(Request $request){
        /* Do not return error response in this API. Since it will be an issue in app side*/
        if ($request->drupal_user_id == '' && $request->email == '') {
            return errorResponse(trans('api.required_fields'));
        }
        $drupal_user_id = $request->drupal_user_id;
        $lan = app()->getLocale();
        //$catagories = array('silver'=>100,'gold'=>101,'platinum'=>102,'platinum plus'=>103,'mini'=>10,'mini pack'=>11);
        $catagories = config('globalconstants.pack_categories');
        $colorcodes = config('globalconstants.pack_categories_colorcodes');
        $colorcodesBack = config('globalconstants.pack_categories_background_colorcodes');
        $season = \App\Season::where('status',1)->first();
        $seasonId = (!empty($season)) ?  $season->id : NULL;
        $user = NULL;
        if(!empty($request->drupal_user_id) && !empty($request->email)){
            $user = \App\User::where('season_id',$seasonId)->where('email', $request->email)->where('drupal_user_id', $request->drupal_user_id)->first();
        }
        if(!empty($request->drupal_user_id)){
            $user = \App\User::where('season_id',$seasonId)->where('drupal_user_id', $request->drupal_user_id)->first();
        }
        if( !empty($request->email)){
            $user = \App\User::where('season_id',$seasonId)->where('email', $request->email)->first();
        }
        
        $allCars = [];
        $userCarIds = [];
        $packs = [];
        if(!empty($user)){
            $packs = $this->getVIPUserPacks($user->email,true,'highest');
            if(!empty($user->drupal_user_id)){
                $drupal_user_id = $user->drupal_user_id;
            }            
        }
        


       /* $packs = \App\Pack::select('packs.id as pId','packs.prefix','packs.pack_number','packs.sale_id', 'packs.allowed_tables', 'packs.remaining_tables', 'users.id as user_id','users.first_name','users.last_name','users.email','users.mobile','users.mobile_dial_code','users.gender','users.dob','users.recieve_updates','users.account_id','packs.category','packs.car_activation_allowed','packs.park_entry_count','packs.wonderpass_count','packs.parking_voucher_count','users.blocked_status as is_vip_blocked','packs.drupal_user_id','packs.cabana_total_count','packs.cabana_remaining_count','packs.cabana_discount_percentage','packs.inpark_taxi_voucher','packs.carwash_services_voucher','packs.porter_services_voucher','packs.stunt_show')
        ->selectRaw('(CASE WHEN packs.category = "platinum plus" THEN "platinum" WHEN (packs.category = "mini" OR packs.category = "mini pack") THEN "mini" ELSE packs.category END) as category')
        ->where('packs.season_id',$seasonId)

        ->leftJoin('users','users.pack_id','packs.id');
        if($request->email != ''){

            $packs->where('users.email',$request->email);
        }else{
            $packs->where('packs.drupal_user_id',$request->drupal_user_id);
        }

        if($request->apiFrom == 'website'){
           // $packs->where('users.blocked_status','no');
        }
        $allCars = [];
        $userCarIds = [];
        $packs = $packs
        //->orderby('users.created_at','desc')
        ->orderByRaw("FIELD(packs.category,'silver', 'gold','platinum','platinum plus','diamond','mini','private','complimentary')")
        ->get()->each(function($item) use($lan,$catagories,$colorcodes,$colorcodesBack, &$drupal_user_id){
            $item->category_type = (isset($catagories[$item->category])) ? $catagories[$item->category] : 1;
            $item->category_color = (isset($colorcodes[$item->category])) ? $colorcodes[$item->category] : NULL;
            $item->category_background_color = (isset($colorcodesBack[$item->category])) ? $colorcodesBack[$item->category] : NULL;

            $item->activated_url = route('my.activated.pack',[app()->getLocale(),$item->user_id]).'?source=mobile';
            $wonderpasses = \App\Wonderpass::select('card_number','balance_points','carnaval_ride_points','ripleys_count')->where('pack_id',$item->pId)->get();
            $item->wonderpasses = !empty($wonderpasses) ? $wonderpasses : NULL ;
            $item->wonderpass_count = !empty($wonderpasses) ? count($wonderpasses) : 0 ;



            $item->offers = NULL ;
            $offers = $this->formatoffers($item);
            $item->partneroffers = !empty($offers) ? $offers : NULL ;
            $cars = [];
            $userCars = \App\UserCar::where('user_id',$item->user_id)->get();
            //dd($userCars);
            if(!empty($userCars )){
                foreach($userCars as $us){
                    $cars[] = array(
                        'id' => $us->id,
                        'plate_prefix' => $us->plate_prefix,
                        'plate_number' => $us->plate_number,
                        'country' => isset($us->country->code) ? $us->country->code : NULL,
                        'emirate' => isset($us->emirate->code) ? $us->emirate->code : NULL ,
                        'type' => isset($us->type) ? $us->type : NULL
                    );

                    $allCars[] = array(
                        'plate_prefix' => $us->plate_prefix,
                        'plate_number' => $us->plate_number,
                        'country' => isset($us->country->code) ? $us->country->code : NULL,
                        'emirate' => isset($us->emirate->code) ? $us->emirate->code : NULL ,
                        'type' => isset($us->type) ? $us->type : NULL
                    );

                }
            }

            $item->cars = !empty($cars) ? $cars : NULL ;
            if(!empty($item->drupal_user_id)){
                $drupal_user_id = $item->drupal_user_id;
            }
            
        });*/
        $data['packs'] = (!empty($packs)) ? $packs : [];
       // dd($drupal_user_id);
       
        if(!empty($drupal_user_id)){
            $allCars = [];
            
            $allCarData =  \App\NewUserCar::where('drupal_user_id',$drupal_user_id)
                                    ->orderby('created_at','desc')
                                    ->select('plate_number','country_id','emirate_id','plate_prefix','type','id')
                                    ->get();

            if(!empty($allCarData )){
                foreach($allCarData as $us){
                    $allCars[] = array(
                        'id' => $us->id,
                        'plate_prefix' => $us->plate_prefix,
                        'plate_number' => $us->plate_number,
                        'country' => isset($us->country->code) ? $us->country->code : NULL,
                        'emirate' => isset($us->emirate->code) ? $us->emirate->code : NULL ,
                        'type' => isset($us->type) ? $us->type : NULL
                    );

                }
            }
        }

        $data['allCars'] = (!empty($allCars)) ? $allCars : NULL;
        return successResponse('success', $data);
    }

    public function addCar(Request $request){
        // $rules = [
        //     // 'packId' => 'required',
        //     'countryCode' => 'required',
        //     //'cityCode' => 'required',
        //     'platePrefix' => 'required',
        //     'plateNumber' => 'required',

        // ];

        // $validator = Validator::make($request->all(), $rules);

        // if ($validator->fails()) {
        //     $errorMessage = $validator->messages();
        //     return errorResponse(trans('api.required_fields'), $errorMessage);
        // }
        if(empty($request->countryCode) && empty($request->platePrefix) && empty($request->plateNumber) && empty($request->carId)){
            return errorResponse(trans('api.required_fields'));
        }
        if(empty($request->carId) && (empty($request->platePrefix) || empty($request->plateNumber) || empty($request->countryCode))){
            return errorResponse(trans('api.required_fields'));
        }
        $season = \App\Season::where('status',1)->first();
        $seasonId = (!empty($season)) ?  $season->id : NULL;
        $userPack = [];
        if(!empty($request->packId)) {
            $userPack = \App\Pack::select('packs.id', 'packs.prefix', 'packs.pack_number', 'packs.sale_id', 'users.id as user_id', 'users.first_name', 'users.last_name', 'users.email', 'users.mobile', 'users.mobile_dial_code', 'users.gender', 'users.drupal_user_id', 'users.residence_id', 'users.nationality_id', 'users.recieve_updates','user_packs.id as user_pack_id','user_packs.designa_customer_uid','packs.car_activation_allowed')
                ->join('user_packs', 'user_packs.pack_id', 'packs.id')
                ->join('users', 'users.id', 'user_packs.user_id')
                ->where('user_packs.pack_id', $request->packId)->first();
            if(empty($userPack )){
                return errorResponse('Pack not exists or activated');
            }
            //dd($userPack);
            if(!empty($userPack)) {
                $totalCarCount = \App\UserCarPack::where('pack_id', $userPack->id)->count();
                if($totalCarCount >= $userPack->car_activation_allowed){
                    return errorResponse('Max car count exceeded');
                }
            }
        }
        $country = \App\CarCountry::where('code',$request->countryCode)->first();
        $emirate = \App\Emirate::where('code',$request->cityCode)->first();
        $user = \App\User::where('drupal_user_id', $request->userId)->first();
        
        $user_id = !empty($userPack->user_id) ? $userPack->user_id : (!empty($user) ? $user->id : NULL);
        $drupal_user_id = !empty($userPack) ? $userPack->drupal_user_id : (!empty($user) ? $user->drupal_user_id : $request->userId);
        $country_id = (!empty($country)) ? $country->id : NULL ;
        $emirate_id = (!empty($emirate)) ? $emirate->id : NULL;
        $plate_prefix = $request->platePrefix;
        $plate_number = $request->plateNumber;

        if(!empty($request->type)){
            $type = $request->type;
        }
        if(!empty($request->carId)){
            $userCar =  \App\NewUserCar::find($request->carId);
        }else{
            $userCar = \App\NewUserCar::firstOrCreate([
                'drupal_user_id' => $drupal_user_id,            
                'type' => $type,
                'country_id' => $country_id,
                'emirate_id' => $emirate_id,                
                'plate_prefix' => $plate_prefix,
                'plate_number' => $plate_number,
            ]);
        }

        
        if(empty($userCar->user_id)){
            $userCar->user_id = $user_id;
            $userCar->save();
        }
        

        //$userCar->is_vip = (empty($request->is_vip) && $request->is_vip == false) ? 0 : 1;
        //$userCar->car_type = $request->carType;
        //$userCar->save();
        if(!empty($userPack)) {
            $userCarPack = \App\UserCarPack::firstOrCreate([                
                'pack_id' => $userPack->id,
                'user_car_id' => $userCar->id
            ]);
            $userDet = \App\User::where('id',$userPack->user_id)->first();
            $pack = \App\Pack::where('id',$userPack->id)->first();
            $agree_offers = ($userDet->recieve_updates) ? 'True' : 'False';
            $source = ($request->source) ? $request->source : '';
            //$this->vipSubmissionToSalesForce($userDet,$pack,$agree_offers,$source,'add_car_mobile');

            $this->syncUsertoDesigna($userPack->user_pack_id);
        }
        $configurations = \App\ApiConfiguration::all()->keyBy('name');
        if (!empty($userPack)) {
            

            $this->savecode($userPack, $configurations);

        }
        
        $arrayData = array("audit_type"=>'create-car','table_id'=>$userCar->user_id,'table_name'=>'new_user_cars',"admin_id"=>'',"admin_type"=>'user',"event"=>"User car ceated","old_values"=>'',"new_values"=>$userCar,"url"=>"");
        manageAuditLogs($arrayData);

        return successResponse('success');
    }

    public function editCar(Request $request){
         $rules = [
            // 'packId' => 'required',
            'carId' => 'required',
            'countryCode' => 'required',
            //'cityCode' => 'required',
            'platePrefix' => 'required',
            'plateNumber' => 'required',

        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        $data = array();
        $season = \App\Season::where('status',1)->first();
        $seasonId = (!empty($season)) ?  $season->id : NULL;

        if(!empty($request->packId)) {
            $userPack = \App\Pack::select('packs.id', 'packs.prefix', 'packs.pack_number', 'packs.sale_id', 'users.id as user_id', 'users.first_name', 'users.last_name', 'users.email', 'users.mobile', 'users.mobile_dial_code', 'users.gender', 'users.drupal_user_id', 'users.residence_id', 'users.nationality_id', 'users.recieve_updates','user_packs.id as user_pack_id','user_packs.designa_customer_uid','packs.car_activation_allowed')
            ->join('user_packs', 'user_packs.pack_id', 'packs.id')
            ->join('users', 'users.id', 'user_packs.user_id')
            ->where('user_packs.pack_id', trim($request->packId))->first();
            if(empty($userPack )){
                return errorResponse('Pack not exists or activated');
            }
        }


        $userCar =  \App\NewUserCar::find($request->carId);
        if(empty($userCar )){
            return errorResponse('Car does not exists');
        }
        $oldCar = \App\NewUserCar::find($request->carId);
        if(empty($oldCar->user_id)){
            $oldCar->user_id = $userPack->user_id;
            $oldCar->save();
        }
        $request->merge(["plate_prefix"=>$request->platePrefix,"plate_number"=>$request->plateNumber]);
        $userCarPack = \App\UserCarPack::where('user_car_id', $request->carId)->where('pack_id', $userPack->id)->first();
        $syncCar = $this->syncCar($request->carId, $request, $userCarPack);
        $data['designa_sync_status'] = $syncCar;
        //if($this->syncCar($request->carId, $request, $userCarPack)){
            
            $sameCarOtherPackExists = \App\UserCarPack::where('user_car_id',$request->carId)->where('pack_id','!=',$userPack->id)->first();
            if(!empty($sameCarOtherPackExists)){
                $userCar = new \App\NewUserCar();
            }
            $country = \App\CarCountry::where('code',$request->countryCode)->first();
            $emirate = \App\Emirate::where('code',$request->cityCode)->first();
            $user = \App\User::where('drupal_user_id', $request->userId)->first();
            $userCar->user_id = !empty($userPack->user_id) ? $userPack->user_id : null;
            $userCar->drupal_user_id = !empty($userPack->drupal_user_id) ? $userPack->drupal_user_id : null;
            $userCar->country_id = (!empty($country)) ? $country->id : NULL ;
            $userCar->emirate_id = (!empty($emirate)) ? $emirate->id : NULL;
            $userCar->plate_prefix = $request->platePrefix;
            $userCar->plate_number = $request->plateNumber;

            if(!empty($request->type)){
                $userCar->type = $request->type;
            }
           
            $userCar->save();

            $userCarPack->user_car_id = $userCar->id;
            $userCarPack->save();

            //carinfo table update
            /*DB::Connection('mysql2')->table('gv_car_info')
                        ->where('carid', $request->carId)
                        ->update(['designa_status' => 1]);*/


            $configurations = \App\ApiConfiguration::all()->keyBy('name');
            if(!empty($userPack) && $syncCar) {
                $this->savecode($userPack,$configurations);
                 $userDet = \App\User::where('id',$userPack->user_id)->first();
                 $pack = \App\Pack::where('id',$userDet->pack_id)->first();
                $agree_offers = ($userDet->recieve_updates) ? 'True' : 'False';
                $source = ($request->source) ? $request->source : '';
                //$this->vipSubmissionToSalesForce($userDet,$pack,$agree_offers,$source,'edit_car_mobile');
            }

            //mail
            /*$data = array();
            $data['subject'] = 'Edit Car Details Sucessfully';

            $data['carCountry'] = \App\CarCountry::where('id', $userCar->country_id)->first()->name_en;
            $data['carEmirate'] = \App\Emirate::where('id', $userCar->emirate_id)->first()->name_en;
            $data['platePrefix'] = $userCar->plate_prefix;
            $data['plateNumber'] = $userCar->plate_number;

            $user = \App\User::where('id', $userCar->user_id)->first();
            $data['lang'] = 'en';

            if(mb_ereg('[\x{0600}-\x{06FF}]', $user->first_name)) // arabic range
            //if(mb_ereg('[\x{0590}-\x{05FF}]', $text)) // hebrew range
            {
                $data['lang'] = 'ar';
            }

            try {
                Mail::to($user->email)
                        ->send(new CarMail($user, $data));
            } catch (\Exception $e) {
                //echo $e->getMessage();
            }*/
            //mail



            $arrayData = array("audit_type"=>'edit-car','table_id'=>$userCar->user_id,'table_name'=>'new_user_cars',"admin_id"=>'',"admin_type"=>'user',"event"=>"User car updated","old_values"=>$oldCar,"new_values"=>$userCar,"url"=>"");
            manageAuditLogs($arrayData);

            return successResponse('success', $data);
        // }else{
        //     return errorResponse( trans('web.designa_error_after_payment') );
        // }

    }

   
    public function updateOfferConsume(Request $request) {
        $rules = [
            'id' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse( trans('web.required_fields'),$errorMessage);
        }
        $offers = \App\PartnerOffer::find($request->id);
        if(empty($offers)){
            return errorResponse( trans('api.no_offfer'));
        }
        if($offers->consumed == 'yes'){
            $offers->consumed = 'no';
            $offers->consumed_date = NULL;
        }else{
            $offers->consumed = 'yes';
            $offers->consumed_date = date('Y-m-d H:i:s');
        }

        $offers->save();
        $catagories = config('globalconstants.pack_categories');
        $colorcodes = config('globalconstants.pack_categories_colorcodes');
        $colorcodesBack = config('globalconstants.pack_categories_background_colorcodes');
        $lan = app()->getLocale();
        $userPack = \App\UserPack::where('pack_id',$offers->pack_id)->first();
        $user = \App\User::where('id',$userPack->user_id)->first();
        $packs = $this->getVIPUserPacks($user->email,true,'highest');

        $data['packs'] = (!empty($packs)) ? $packs : NULL;
        return successResponse('success', $data);
    }

    public function getPackTicketUsageDetails(Request $request){
        $rules = [
            'id' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse( trans('web.required_fields'),$errorMessage);
        }
        $pack = \App\Pack::find($request->id);
        if(empty($pack)){
            return errorResponse('Pack not exists');
        }
        //API to  vgs
        $configurations = \App\ApiConfiguration::all()->keyBy('name');
        $usageDetails = $this->getUsageDetails($pack,$configurations);
        return successResponse('success',$usageDetails);
    }

    public function userHighestPack(Request $request){
        $rules = [
            'email' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        $season = \App\Season::where('status',1)->first();
        $seasonId = (!empty($season)) ?  $season->id : NULL;
        $data = $this->highestVIPpack($seasonId, $request->email);
        return successResponse('success', $data);
    }


    public function userHighestPackV1(Request $request){
        $rules = [
            'drupal_user_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        if(!$this->validateDrupalUser($request->drupal_user_id, $request->header('GV-User-Token'))){
            return errorResponse(trans('api.unauthorized'));
        }

        $colorcodes = config('globalconstants.pack_categories_colorcodes');
        $colorcodesBack = config('globalconstants.pack_categories_background_colorcodes');

        $season = \App\Season::where('status',1)->first();
        $seasonId = (!empty($season)) ?  $season->id : NULL;
        $user = \App\User::where('drupal_user_id',$request->drupal_user_id)->first();
        $data = NULL;
        if(!empty($user)){
            $data = $this->highestVIPpack($seasonId, $user->email);
        }        
        return successResponse('success', $data);

    }



    public function getCabanaHighestPack(Request $request){
         $rules = [
            'drupal_user_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        $season = \App\Season::where('status',1)->first();
        $seasonId = (!empty($season)) ?  $season->id : NULL;
        $data = [];
        $data['packs'] = NULL;
        $packs = \App\Pack::select('packs.id as pack_id','packs.prefix','packs.pack_number','packs.sale_id','users.id as user_id','users.first_name','users.last_name','users.email','users.mobile','users.mobile_dial_code','users.gender','users.dob','users.recieve_updates','users.account_id','packs.category','users.blocked_status as is_vip_blocked','packs.cabana_total_count','packs.cabana_remaining_count','packs.cabana_discount_percentage','packs.category as packCategory')       
       
        ->leftJoin('users','users.pack_id','packs.id')
        ->where('users.drupal_user_id',$request->drupal_user_id )
        ->where('packs.season_id',$seasonId)
        ->where('packs.cabana_remaining_count','>',0)
        ->where('users.blocked_status','no')
        ->orderByRaw("FIELD(packCategory,'diamond','private','complimentary','platinum', 'gold','silver', 'mini')")
        ->first();
        $data['packs'] = (!empty($packs)) ? $packs : NULL;
        return successResponse('success', $data);
    }

    
}
