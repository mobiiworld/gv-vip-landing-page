<?php

namespace App\Http\Controllers\Api\V1\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use Validator;
use App\VehiclePlocation;
use App\VehiclePsetting;
use App\SaveMyParking;
use App\Traits\VehicleCountTraits;
use DB;
use App\VehiclePlocationService;
use App\ParkingServices;

class VehiclePlocationController extends ApiController {

    use VehicleCountTraits;

    public function __construct() {
        $this->middleware('api');
    }

    public function getCount(Request $request) {
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        $vps = VehiclePsetting::first();
        if ($vps) {
            $lan = app()->getLocale();
            $vpls = VehiclePlocation::select('id', 'is_custom', 'vgs_id', "zone_names->{$lan} as zone", "names->{$lan} as name", "descriptions->{$lan} as description", 'total_capacity', 'inside', 'beco_name', 'latitude', 'longitude', 'closed')
                    ->with(["images" => function ($q) use ($lan) {
                            $q->select('id', 'plocation_id', 'image');
                            $q->where('language', $lan)->orderBy('display_order');
                        }])
                    ->where('status', 1);

            if(!empty($latitude) && !empty($longitude)) {
                $vpls = $vpls->orderBy(DB::raw("3959 * acos( cos( radians({$latitude}) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(-{$longitude}) ) + sin( radians({$latitude}) ) * sin(radians(latitude)) )"), 'DESC');
            } else {
                $vpls = $vpls->orderBy('display_order');
            }

            $vpls = $vpls->get();
            if (!$vpls->isEmpty()) {
                //check api expired
                $lut = \Carbon\Carbon::parse($vps->last_updated_at)->addMinutes($vps->api_interval)->format('Y-m-d H:i:s');
                if (date('Y-m-d H:i:s') >= $lut || empty($vps->api_data)) {
                    //Expired call vgs and update
                    //$vpCounts = $this->getVGSVehicleCount($vpls->pluck('vgs_id')->toArray());
                    $vpCounts = $this->parkingDetails();
                    if (!empty($vpCounts)) {
                        $vps->api_data = $vpCounts;
                        $vps->last_updated_at = date('Y-m-d H:i:s');
                        $vps->save();
                    } else {
                        return errorResponse(trans('api.something_wrong'));
                    }
                }
                $vpls->each(function ($q) use (&$vps) {
                    if ($q->is_custom) {
                        $q->vacant = $q->total_capacity - $q->inside;
                    } else {
                        if (isset($vps->api_data[$q->vgs_id])) {
                            $q->inside = $q->total_capacity - $vps->api_data[$q->vgs_id][0]['Availableslots'];
                            $q->vacant = $vps->api_data[$q->vgs_id][0]['Availableslots'];
                        } else {
                            $q->vacant = $q->total_capacity;
                        }
                    }
                });
                return successResponse(trans('api.success'), $vpls);
            }
        }
        return errorResponse(trans('api.no_data'));
    }

    /**
     * Store my parking.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveMyParking(Request $request) {
        $rules = [
            'zone_id' => 'required',
            'parking_id' => 'required',
            //'slot_number' => 'required',
            'drupal_id' => 'required|integer',
            'latitude' => 'required',
            'longitude' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }
        $exist = VehiclePlocation::where('id', $request->zone_id)->first();

        if (empty($exist)) {
            return errorResponse(trans('content.invalid_slots'));
        }
        $my_parking = new SaveMyParking();
        $my_parking->vehicle_plocation_id = $request->zone_id;
        $my_parking->vgs_id = $request->parking_id;
        $my_parking->slot_number = $request->slot_number;
        $my_parking->drupal_id = $request->drupal_id;
        $my_parking->latitude = $request->latitude;
        $my_parking->longitude = $request->longitude;
        $my_parking->note = $request->note;
        $my_parking->save();
        return successResponse(trans('content.save_success'));
    }

    /**
     * Get my parking.
     *
     * @return \Illuminate\Http\Response
     */
    public function getMyParking(Request $request) {
        $rules = [
            'drupal_id' => 'required|integer',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        $my_parkings = SaveMyParking::where(['drupal_id' => $request->drupal_id])->get();
        $data = [];
        if (!empty($my_parkings)) {
            $lan = app()->getLocale();
            foreach ($my_parkings as $my_parking) {
                if ($my_parking->status) {
                    $vehiclePlocation = VehiclePlocation::where('id', $my_parking->vehicle_plocation_id)->first();
                    $data['zone_id'] = $my_parking->vehicle_plocation_id;
                    $data['zone_name'] = !empty($vehiclePlocation) ? $vehiclePlocation->zone_names[$lan] : '';
                    $data['name'] = !empty($vehiclePlocation) ? $vehiclePlocation->names[$lan] : '';
                    $data['slot_number'] = $my_parking->slot_number;
                    $data['vgs_id'] = $my_parking->vgs_id;
                    $data['drupal_id'] = $my_parking->drupal_id;
                    $data['latitude'] = $my_parking->latitude;
                    $data['longitude'] = $my_parking->longitude;
                    $data['note'] = $my_parking->note;
                    $data['extra'] = $vehiclePlocation->extras;
                    $data['beco_name'] = !empty($vehiclePlocation) ? $vehiclePlocation->beco_name : '';
                    $data['services'] = VehiclePlocationService::select("id", "parking_service_id", "beco_name", "descriptions->{$lan} as description")
                            ->with("service:id,names->{$lan} as name,descriptions->{$lan} as description,icon")
                            ->where('vehicle_plocation_id', $my_parking->vehicle_plocation_id)
                            ->get();
                    /* $data['services'] = VehiclePlocationService::select('ps.id', "ps.names->{$lan} as name", 'ps.icon', "ps.descriptions->{$lan} as desc", "vehicle_plocation_services.descriptions->{$lan} as popup_message")
                      ->join('parking_services as ps', 'ps.id', 'vehicle_plocation_services.parking_service_id')
                      ->where('vehicle_plocation_services.vehicle_plocation_id', $my_parking->vehicle_plocation_id)
                      ->get(); */
                    /* $services = VehiclePlocationService::where('vehicle_plocation_id', $my_parking->vehicle_plocation_id)->pluck('parking_service_id');
                      if (!empty($services)) {
                      foreach ($services as $key => $service) {
                      $ser = ParkingServices::where('id', $service)->first();
                      $data['services'][$key]['id'] = $ser->id;
                      $data['services'][$key]['name'] = $ser->names[$lan];
                      $data['services'][$key]['image'] = $ser->icon;
                      $data['services'][$key]['desc'] = $ser->descriptions[$lan];
                      }
                      } */
                    return successResponse(trans('api.success'), $data);
                }
            }
        }
        return errorResponse(trans('api.something_wrong'));
    }

    /**
     * Remove my parking.
     *
     * @return \Illuminate\Http\Response
     */
    public function removeMyParking(Request $request) {
        $rules = [
            'drupal_id' => 'required|integer',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }
        SaveMyParking::where('drupal_id', $request->drupal_id)->where('status', 1)->update(['status' => 0]);
        return successResponse(trans('api.success'));
    }

}
