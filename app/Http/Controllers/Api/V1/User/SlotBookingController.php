<?php

namespace App\Http\Controllers\Api\V1\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use App\Program;
use App\ProgramSlot;

class SlotBookingController extends Controller {

    public function getProgramData(Request $request) {
        $rules = [
            'programDate' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }
        $program = Program::where('program_uid', $request->programId)->where('status', 1)->first();
        if (empty($program)) {
            return errorResponse(trans('api.program_unavailable'));
        }

        $tableCount = \App\VipTable::where('status', 1)->count();
        $totalTable = ($tableCount > 0) ? $tableCount : 1;
        $data['totalTable'] = $totalTable;
        $data['program'] = ['id' => $program->id,
            'full_amount_4' => $program->full_amount,
            'full_amount_6' => $program->full_amount_6,
            'vgs_pc_daily_4_s' => $program->vgs_product_code_daily,
            'vgs_pc_daily_4_ns' => $program->vgs_product_code_daily_ns,
            'vgs_pc_daily_4_vs' => $program->vgs_product_code_daily_vs,
            'vgs_pc_daily_4_vns' => $program->vgs_product_code_daily_vns,
            'vgs_pc_daily_6_s' => $program->vgs_product_code_daily_6,
            'vgs_pc_daily_6_ns' => $program->vgs_product_code_daily_6_ns,
            'vgs_pc_daily_6_vs' => $program->vgs_product_code_daily_6_vs,
            'vgs_pc_daily_6_vns' => $program->vgs_product_code_daily_6_vns,
            'full_amount_2' => $program->full_amount_2,
            'vgs_pc_daily_2_s' => $program->vgs_product_code_daily_2,
            'vgs_pc_daily_2_ns' => $program->vgs_product_code_daily_2_ns,
            'vgs_pc_daily_2_vs' => $program->vgs_product_code_daily_2_vs,
            'vgs_pc_daily_2_vns' => $program->vgs_product_code_daily_2_vns,
            'full_evening_table_limit' => $program->full_evening_table_limit,
            'suggestedTableCount' => $program->suggesetd_table_count,
            'maxWeightCount' => $program->max_weight_count,
            'has_priority' => ($program->has_priority == 1) ? true : false,
            'dailySlotAvailable' => false
        ];

        return successResponse('success', $data);
    }

    public function index(Request $request) {
        $rules = [
            'programDate' => 'required',
            'isVip' => 'required',
            'table_type' => 'required|in:standard,premium',
            'programId' => 'required',
            'noOfTables2' => 'required',
            'noOfTables4' => 'required',
            'noOfTables6' => 'required',
            'smokingType' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        if ($request->noOfTables4 == 0 && $request->noOfTables6 == 0 && $request->noOfTables2 == 0) {
            return errorResponse(trans('api.required_fields'));
        }

        $program = Program::where('program_uid', $request->programId)->where('status', 1)->first();
        if (empty($program)) {
            return errorResponse(trans('api.program_unavailable'));
        }

        $time = ($program->hours_before_booking) != NULL ? $program->hours_before_booking : 0;

        $startTime = new \DateTime();
        $startTime->modify('+' . $time . ' hours');
        $starttimeModified = $startTime->format('Y-m-d H:i:s');

        $programSlot = array();
        // \DB::enableQueryLog();
        if (date("Y-m-d", strtotime($request->programDate)) >= date("Y-m-d")) {
            $programSlot = ProgramSlot::where('program_slots.program_id', $program->id)
                    ->where('program_slots.status', 1)
                    ->where('program_slots.program_date', date("Y-m-d", strtotime($request->programDate)))
                    ->join('vip_tables as tb', 'tb.id', '=', 'program_slots.table_id')
                    ->join('slots', 'slots.id', '=', 'program_slots.slot_id')
                    ->orderby('slots.priority', 'asc')
                    ->select('slots.id', 'slots.name as slotName', 'slots.priority', 'program_slots.program_date', 'slots.start_time', 'slots.end_time', 'slots.weight_count')
                    ->where('tb.status', 1)
                    ->where('tb.admin_only', 0)
                    ->where('tb.smoking_status', $request->smokingType)
                    ->where('tb.type', $request->table_type)
                    ->where('slots.status', 1);
            if ($request->noOfTables4 > 0 && $request->noOfTables6 > 0 && $request->noOfTables2 > 0) {
                $programSlot->whereIn('tb.seat_type', [2, 4, 6]);
            } else if ($request->noOfTables4 > 0 && $request->noOfTables6 > 0) {
                $programSlot->whereIn('tb.seat_type', [4, 6]);
            } else if ($request->noOfTables2 > 0 && $request->noOfTables4 > 0) {
                $programSlot->whereIn('tb.seat_type', [2, 4]);
            } else if ($request->noOfTables2 > 0 && $request->noOfTables6 > 0) {
                $programSlot->whereIn('tb.seat_type', [2, 6]);
            } else {
                if ($request->noOfTables4 > 0) {
                    $programSlot->where('tb.seat_type', 4);
                } elseif ($request->noOfTables6 > 0) {
                    $programSlot->where('tb.seat_type', 6);
                } elseif ($request->noOfTables2 > 0) {
                    $programSlot->where('tb.seat_type', 2);
                }
            }
            $programSlot = $programSlot->distinct()->orderby('slots.start_time', 'asc')->get()->toArray();
        }

        if (!empty($programSlot)) {
            foreach ($programSlot as $key => $pgSlot) {
                $availableCountQuery = ProgramSlot::where('program_slots.status', 1)
                        ->where('program_slots.program_status', 1)
                        ->where('program_slots.program_date', date("Y-m-d", strtotime($request->programDate)))
                        ->join('vip_tables as tb', 'tb.id', '=', 'program_slots.table_id')
                        ->join('slots', 'slots.id', '=', 'program_slots.slot_id')
                        ->whereNull('program_slots.booking_id')
                        ->orderby('slots.priority', 'asc')
                        ->selectRaw('COUNT(CASE WHEN (tb.seat_type=4) THEN 1 END) AS table_h4,COUNT(CASE WHEN (tb.seat_type=6) THEN 1 END) AS table_h6,COUNT(CASE WHEN (tb.seat_type=2) THEN 1 END) AS table_h2')
                        ->where('tb.status', 1)
                        ->where('tb.admin_only', 0)
                        ->where('slots.id', $pgSlot['id'])
                        ->where('slots.status', 1)
                        ->where('tb.type', $request->table_type)
                        ->where('tb.smoking_status', $request->smokingType);
                $availableCount = $availableCountQuery->first();

                if (($pgSlot['program_date'] . ' ' . $pgSlot['start_time']) < $starttimeModified) {
                    unset($programSlot[$key]);
                } else {
                    if (($availableCount->table_h4 >= $request->noOfTables4) && ($availableCount->table_h6 >= $request->noOfTables6) && ($availableCount->table_h2 >= $request->noOfTables2)) {
                        $programSlot[$key]['isAvailable'] = TRUE;
                    } else {
                        $programSlot[$key]['isAvailable'] = FALSE;
                    }
                }
            }
        }

        $caseCheck = $request->table_type . '|' . $request->smokingType;
        $data['program'] = ['id' => $program->id,
            'suggestedTableCount' => $program->suggesetd_table_count,
            'maxWeightCount' => $program->max_weight_count,
            'has_priority' => ($program->has_priority == 1) ? true : false,
            'vgs_products' => vgsMajlisPcodes($caseCheck, $program),
        ];

        $data['slots'] = (count($programSlot) > 0) ? array_values($programSlot) : NULL;
        return successResponse('success', $data);
    }

    public function createBooking(Request $request) {
        $rules = [
            //'userId' => 'required',
            'programId' => 'required',
            'programDate' => 'required',
            'slots.*' => 'required',
            'isVip' => 'required',
            'table_type' => 'required|in:standard,premium',
            'noOfTables2' => 'required',
            'noOfTables4' => 'required',
            'noOfTables6' => 'required',
            'smokingType' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }
        if ($request->noOfTables4 == 0 && $request->noOfTables6 == 0 && $request->noOfTables2 == 0) {
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        $program = Program::where('program_uid', $request->programId)->where('status', 1)->first();
        if (empty($program)) {
            return errorResponse(trans('api.program_unavailable'));
        }

        $programId = $mainSlotArray = $mainSlotArray6 = $tableIds = $tableIds6 = $mainSlotArray2 = $tableIds2 = NULL;

        $slotIds = $request->slots;

        if ($request->noOfTables4 > 0 && $request->noOfTables6 > 0 && $request->noOfTables2 > 0) {
            $reponse = $this->threeTableCheck($slotIds, $request);
            if (count($reponse['allTableIds4']) < $request->noOfTables4 || count($reponse['allTableIds6']) < $request->noOfTables6 || count($reponse['allTableIds2']) < $request->noOfTables2) {
                $slotNames = \App\Slot::whereIn('id', $reponse['unAvailableSlots'])->orderby('start_time', 'asc')->get()->pluck('name');
                $data['unavailableSlots'] = $slotNames;
                return errorResponseWithData(trans('api.slot_unavailable'), $data);
            }

            $tableIds = array_slice($reponse['allTableIds4'], 0, $request->noOfTables4);
            $tableIds6 = array_slice($reponse['allTableIds6'], 0, $request->noOfTables6);
            $tableIds2 = array_slice($reponse['allTableIds2'], 0, $request->noOfTables2);
            $mainSlotArray = $reponse['mainSlotArray4'];
            $mainSlotArray6 = $reponse['mainSlotArray6'];
            $mainSlotArray2 = $reponse['mainSlotArray2'];
        } else if ($request->noOfTables2 > 0 && $request->noOfTables4 > 0) {
            $reponse = $this->multiTableCheck($slotIds, $request, [2, 4]);

            if (count($reponse['allTableIds4']) < $request->noOfTables2 || count($reponse['allTableIds6']) < $request->noOfTables4) {
                $slotNames = \App\Slot::whereIn('id', $reponse['unAvailableSlots'])->orderby('start_time', 'asc')->get()->pluck('name');
                $data['unavailableSlots'] = $slotNames;
                return errorResponseWithData(trans('api.slot_unavailable'), $data);
            }

            $tableIds = array_slice($reponse['allTableIds4'], 0, $request->noOfTables2);
            $tableIds6 = array_slice($reponse['allTableIds6'], 0, $request->noOfTables4);
            $mainSlotArray = $reponse['mainSlotArray4'];
            $mainSlotArray6 = $reponse['mainSlotArray6'];
        } else if ($request->noOfTables2 > 0 && $request->noOfTables6 > 0) {
            $reponse = $this->multiTableCheck($slotIds, $request, [2, 6]);

            if (count($reponse['allTableIds4']) < $request->noOfTables2 || count($reponse['allTableIds6']) < $request->noOfTables6) {
                $slotNames = \App\Slot::whereIn('id', $reponse['unAvailableSlots'])->orderby('start_time', 'asc')->get()->pluck('name');
                $data['unavailableSlots'] = $slotNames;
                return errorResponseWithData(trans('api.slot_unavailable'), $data);
            }

            $tableIds = array_slice($reponse['allTableIds4'], 0, $request->noOfTables2);
            $tableIds6 = array_slice($reponse['allTableIds6'], 0, $request->noOfTables6);
            $mainSlotArray = $reponse['mainSlotArray4'];
            $mainSlotArray6 = $reponse['mainSlotArray6'];
        } else if ($request->noOfTables4 > 0 && $request->noOfTables6 > 0) {
            $reponse = $this->multiTableCheck($slotIds, $request, [4, 6]);

            if (count($reponse['allTableIds4']) < $request->noOfTables4 || count($reponse['allTableIds6']) < $request->noOfTables6) {
                $slotNames = \App\Slot::whereIn('id', $reponse['unAvailableSlots'])->orderby('start_time', 'asc')->get()->pluck('name');
                $data['unavailableSlots'] = $slotNames;
                return errorResponseWithData(trans('api.slot_unavailable'), $data);
            }

            $tableIds = array_slice($reponse['allTableIds4'], 0, $request->noOfTables4);
            $tableIds6 = array_slice($reponse['allTableIds6'], 0, $request->noOfTables6);
            $mainSlotArray = $reponse['mainSlotArray4'];
            $mainSlotArray6 = $reponse['mainSlotArray6'];
        } else {
            if ($request->noOfTables4 > 0) {
                $reponse = $this->singleTableCheck($slotIds, 4, $request);
                $noOfTables = $request->noOfTables4;
            }
            if ($request->noOfTables6 > 0) {
                $reponse = $this->singleTableCheck($slotIds, 6, $request);
                $noOfTables = $request->noOfTables6;
            }
            if ($request->noOfTables2 > 0) {
                $reponse = $this->singleTableCheck($slotIds, 2, $request);
                $noOfTables = $request->noOfTables2;
            }

            if (count($reponse['allTableIds']) < $noOfTables) {
                $slotNames = \App\Slot::whereIn('id', $reponse['unAvailableSlots'])->orderby('start_time', 'asc')->get()->pluck('name');
                $data['unavailableSlots'] = $slotNames;
                return errorResponseWithData(trans('api.slot_unavailable'), $data);
            }

            $tableIds = array_slice($reponse['allTableIds'], 0, $noOfTables);
            $mainSlotArray = $reponse['mainSlotArray'];
        }


        // Getting first n number of tables, since $progrmaSlot result in the ascending order of table_number
        // Getting program slot ids for booking, satisfying all slots with tables
        $bookingSlotIds = [];
        if (!empty($mainSlotArray) && !empty($tableIds)) {
            foreach ($mainSlotArray as $mainSlot) {
                foreach ($tableIds as $tb) {
                    if (isset($mainSlot[$tb]['id'])) {
                        $bookingSlotIds[] = $mainSlot[$tb]['id'];
                        //$programId = $mainSlot[$tb]['program_id'];
                    }
                }
            }
        }
        if (!empty($mainSlotArray6) && !empty($tableIds6)) {
            foreach ($mainSlotArray6 as $mainSlot) {
                foreach ($tableIds6 as $tb) {
                    if (isset($mainSlot[$tb]['id'])) {
                        $bookingSlotIds[] = $mainSlot[$tb]['id'];
                        //$programId = $mainSlot[$tb]['program_id'];
                    }
                }
            }
        }
        if (!empty($mainSlotArray2) && !empty($tableIds2)) {
            foreach ($mainSlotArray2 as $mainSlot) {
                foreach ($tableIds2 as $tb) {
                    if (isset($mainSlot[$tb]['id'])) {
                        $bookingSlotIds[] = $mainSlot[$tb]['id'];
                    }
                }
            }
        }
        if (!empty($bookingSlotIds)) {
            //$program = Program::where('id', $programId)->first();
            $booking = new \App\BookSlot;
            $booking->program_id = $program->id;
            $booking->table_type = $request->table_type;
            $booking->status = 'pending';
            $booking->status_updated_at = date('Y-m-d H:i:s');

            if ($request->has('adminId')) {
                $booking->admin_id = $request->adminId;
                $booking->payment_mode = $request->paymentMode;
            } else {
                $booking->booking_user_id = $request->userId;
            }

            $booking->payment_extras = [
                'smokingType' => $request->smokingType,
                'noOfTables4' => $request->noOfTables4,
                'noOfTables6' => $request->noOfTables6,
                'noOfTables2' => $request->noOfTables2,
                'tableType' => $request->table_type,
                'vipType' => $request->vip_type,
                'vipCount' => $request->vip_count
            ];

            $booking->program_slot_id = join(",", $bookingSlotIds);
            $booking->program_date = date("Y-m-d", strtotime($request->programDate));
            //Find least slot time and get the actual date
            $leastSlot = ProgramSlot::select('slots.id', 'slots.start_time', 'actual_start_time')
                    ->join('slots', 'slots.id', 'program_slots.slot_id')
                    ->whereIn('program_slots.id', $bookingSlotIds)
                    ->orderBy('slots.priority')
                    ->first();
            if (!empty($leastSlot->actual_start_time)) {
                $booking->actual_program_dt = date('Y-m-d', strtotime($booking->program_date . ' + 1 days')) . ' ' . $leastSlot->actual_start_time;
            } else {
                $booking->actual_program_dt = $booking->program_date . ' ' . $leastSlot->start_time;
            }

            //$booking->booking_type = $bookingType;
            if ($request->email != '') {
                $booking->email = $request->email;
            }
            if ($request->firstName != '') {
                $booking->first_name = $request->firstName;
            }
            if ($request->lastName != '') {
                $booking->last_name = $request->lastName;
            }
            if ($request->mobile != '') {
                $booking->mobile = $request->mobile;
            }
            if ($request->shopCartId != '') {
                $booking->shop_cart_id = $request->shopCartId;
            }
            if ($request->saleCode != '') {
                $booking->sale_code = $request->saleCode;
            }
            if ($request->amount != '') {
                $booking->amount = $request->amount;
            }

            $booking->special_notes = $request->specialNotes;
            $booking->is_vip = $request->isVip;
            if (date('Y-m-d H:i') <= date('Y-m-d') . ' 05:59') {
                $booking->operational_date = date("Y-m-d", strtotime("yesterday"));
            } else {
                $booking->operational_date = date('Y-m-d');
            }
            $booking->save();

            if (!empty($bookingSlotIds)) {
                /* foreach ($bookingSlotIds as $id) {
                  $history = new \App\ProgramSlotStatusHistory;
                  $history->program_slot_id = $id;
                  $history->program_status = 1;
                  $history->save();
                  } */
                $history = new \App\MajlisBookingStatusHistory();
                $history->booking_id = $booking->id;
                $history->status = 'pending';
                $history->description = 'Booking Created';
                $history->save();
            }


            ProgramSlot::whereIn('id', $bookingSlotIds)->update(['booking_id' => $booking->id]);
            $data['request'] = $request->all();
            $data['bookingId'] = $booking->id;
            $data['program'] = ['id' => $program->id,
                'suggestedTableCount' => $program->suggesetd_table_count,
                'maxWeightCount' => $program->max_weight_count,
                'has_priority' => ($program->has_priority == 1) ? true : false,
            ];
            return successResponse('success', $data);
        }

        $slotNames = \App\Slot::whereIn('id', $slotIds)->orderby('start_time', 'asc')->get()->pluck('name');
        $data['unavailableSlots'] = $slotNames;

        return errorResponseWithData(trans('api.slot_unavailable'), $data);
    }

    /* public function bookingStatus(Request $request) {
      $rules = [
      'bookingId' => 'required',
      'type' => 'required'
      ];
      $validator = Validator::make($request->all(), $rules);
      if ($validator->fails()) {
      $errorMessage = $validator->messages();
      return errorResponse(trans('api.required_fields'), $errorMessage);
      }
      $booking = \App\BookSlot::find($request->bookingId);
      if (empty($booking)) {
      return errorResponse(trans('api.no_data'));
      }
      if ($booking->status == 'timeout') {
      return errorResponse(trans('api.booking_timeout'));
      }
      $slotIds = explode(",", $booking->program_slot_id);
      if ($request->type == "waiting_confirmation" && ( $booking->status == 'waiting_confirmation' || $booking->status == 'pending')) {

      $programSlotCount = ProgramSlot::where('program_slots.status', 1)
      ->where('program_slots.program_status', 1)
      ->whereIN('program_slots.id', $slotIds)
      ->where("program_slots.booking_id", $booking->id)
      ->count();
      // dd($slotIds);
      if ($programSlotCount != count($slotIds)) {
      return errorResponse(trans('api.slot_unavailable'));
      }
      $booking->status = 'waiting_confirmation';
      $booking->save();

      if (!empty($slotIds)) {
      foreach ($slotIds as $id) {
      $history = new \App\ProgramSlotStatusHistory;
      $history->program_slot_id = $id;
      $history->program_status = 2;
      $history->save();
      }
      }
      $data['id'] = $booking->id;
      $data['status'] = $booking->status;

      return successResponse('success', $data);
      } else if ($request->type == "waiting_payment" && $booking->status == 'waiting_confirmation') {
      if (!empty($request->majlisProductIds)) {
      $booking->vgs_product_codes = implode(',', $request->majlisProductIds);
      }
      $booking->status = 'waiting_payment';
      $booking->save();
      ProgramSlot::where('booking_id', $booking->id)->update(['program_status' => 2]);

      if (!empty($slotIds)) {
      foreach ($slotIds as $id) {
      $history = new \App\ProgramSlotStatusHistory;
      $history->program_slot_id = $id;
      $history->program_status = 3;
      $history->save();
      }
      }
      $data['id'] = $booking->id;
      $data['status'] = $booking->status;
      return successResponse('success', $data);
      } else if ($request->type == "order_confirm" && $booking->status == 'waiting_payment') {
      if (!empty($request->majlisProductIds)) {
      $booking->vgs_product_codes = implode(',', $request->majlisProductIds);
      }
      $booking->status = 'order_confirmed';
      $booking->save();
      ProgramSlot::where('booking_id', $booking->id)->update(['program_status' => 3]);

      if (!empty($slotIds)) {
      foreach ($slotIds as $id) {
      $history = new \App\ProgramSlotStatusHistory;
      $history->program_slot_id = $id;
      $history->program_status = 4;
      $history->save();
      }
      }
      $data['id'] = $booking->id;
      $data['status'] = $booking->status;
      return successResponse('success', $data);
      }
      return errorResponse(trans('api.slot_unavailable'));
      } */

    public function getStatus(Request $request) {
        $rules = [
            'bookingId' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }
        $booking = \App\BookSlot::find($request->bookingId);
        if (empty($booking)) {
            return errorResponse(trans('api.no_data'));
        }

        $slotIds = explode(",", $booking->program_slot_id);
        //if ($request->type == "waiting_confirmation" && ( $booking->status == 'waiting_confirmation' || $booking->status == 'pending')) {
        if ($request->type == "waiting_confirmation") {
            if ($booking->status == 'pending') {
                $booking->status = 'waiting_confirmation';
                $programSlotCount = ProgramSlot::where('program_slots.status', 1)
                        ->where('program_slots.program_status', 1)
                        ->whereIn('program_slots.id', $slotIds)
                        ->where("program_slots.booking_id", $booking->id)
                        ->count();
                if ($programSlotCount == count($slotIds)) {
                    $history = new \App\MajlisBookingStatusHistory();
                    $history->booking_id = $booking->id;
                    $history->status = 'waiting_confirmation';
                    $history->description = 'Requested waiting confirmation.Status was pending';
                    $history->save();
                }
            }

            $booking->status_updated_at = date('Y-m-d H:i:s');
            $booking->save();
            //\Log::info("Waiting confirmation");
        } else if ($request->type == "waiting_payment" && $booking->status == 'waiting_confirmation') {
            if (!empty($request->majlisProductIds)) {
                $booking->vgs_product_codes = implode(',', $request->majlisProductIds);
            }
            $booking->status = 'waiting_payment';
            $booking->status_updated_at = date('Y-m-d H:i:s');
            $booking->save();
            ProgramSlot::where('booking_id', $booking->id)->update(['program_status' => 2]);
            if (!empty($slotIds)) {
                /* foreach ($slotIds as $id) {
                  $history = new \App\ProgramSlotStatusHistory;
                  $history->program_slot_id = $id;
                  $history->program_status = 3;
                  $history->save();
                  } */
                $history = new \App\MajlisBookingStatusHistory();
                $history->booking_id = $booking->id;
                $history->status = 'waiting_payment';
                $history->description = 'Requested waiting payment.Status was waiting confirmation';
                $history->save();
            }
            //\Log::info("Waiting payment");
        } else if ($request->type == "order_confirm") {
            //&& $booking->status == 'waiting_payment'
            if (!empty($request->majlisProductIds)) {
                $booking->vgs_product_codes = implode(',', $request->majlisProductIds);
            }
            $booking->status = 'order_confirmed';
            $booking->status_updated_at = date('Y-m-d H:i:s');
            $booking->ticket_ids = $request->ticket_ids;
            $booking->save();

            $eixstPslots = ProgramSlot::select('id', 'booking_id')->whereIn('id', $slotIds)->get();
            if ($eixstPslots->where('booking_id', $booking->id)->count() == 0) {
                if ($eixstPslots->where('booking_id', null)->count() == count($slotIds)) {
                    ProgramSlot::whereIn('id', $slotIds)->update(['booking_id' => $booking->id, 'program_status' => 3]);
                }
            } else {
                ProgramSlot::where('booking_id', $booking->id)->update(['program_status' => 3]);
            }

            if (!empty($slotIds)) {
                /* foreach ($slotIds as $id) {
                  $history = new \App\ProgramSlotStatusHistory;
                  $history->program_slot_id = $id;
                  $history->program_status = 4;
                  $history->save();
                  } */
                $history = new \App\MajlisBookingStatusHistory();
                $history->booking_id = $booking->id;
                $history->status = 'order_confirmed';
                $history->description = 'Requested order confirmed.Status was waiting payment';
                $history->save();
            }
            //check for vip free table;
            if ($booking->is_vip && ($booking->payment_extras['vipCount'] > 0)) {
                $season = \App\Season::where('status', 1)->first();
                $majlisVipTableCountUsed = $booking->payment_extras['vipCount'];

                $userPacks = \App\Pack::select('packs.id', 'packs.remaining_tables')
                        ->join('users', 'users.pack_id', 'packs.id')
                        ->where(function ($q) use ($request) {
                            $q->where('packs.drupal_user_id', $request->drupalUserId)
                            ->orWhere('users.email', $request->email);
                        })
                        ->where('packs.season_id', $season->id)
                        ->where('packs.remaining_tables', '>', 0);
                if ($booking->payment_extras['vipType'] == 'fullday') {
                    $userPacks->whereIn('packs.category', ['private', 'platinum', 'diamond'])
                            ->orderByRaw("FIELD(packs.category,'private','platinum','diamond')");
                } else {
                    $userPacks->whereIn('packs.category', ['gold', 'silver', 'complimentary'])
                            ->orderByRaw("FIELD(packs.category,'gold','silver','complimentary')");
                }
                $userPacks->get()->each(function ($pack)use (&$majlisVipTableCountUsed) {
                    if ($majlisVipTableCountUsed > 0) {
                        if ($pack->remaining_tables <= $majlisVipTableCountUsed) {
                            $majlisVipTableCountUsed = $majlisVipTableCountUsed - $pack->remaining_tables;
                            $pack->remaining_tables = 0;
                            $pack->save();
                        } else {
                            $pack->remaining_tables = $pack->remaining_tables - $majlisVipTableCountUsed;
                            $majlisVipTableCountUsed = 0;
                            $pack->save();
                        }
                    }
                });
            }
        }

        if ($request->email != '') {
            $booking->email = $request->email;
        }
        if ($request->firstName != '') {
            $booking->first_name = $request->firstName;
        }
        if ($request->lastName != '') {
            $booking->last_name = $request->lastName;
        }
        if ($request->mobile != '') {
            $booking->mobile = $request->mobile;
        }
        if ($request->shopCartId != '') {
            $booking->shop_cart_id = $request->shopCartId;
        }
        if ($request->saleCode != '') {
            $booking->sale_code = $request->saleCode;
        }
        if ($request->amount != '') {
            $booking->amount = $request->amount;
        }
        $booking->save();

        $data['id'] = $request->bookingId;
        $data['status'] = in_array($booking->status, ['pending_timeout', 'waiting_confirmation_timeout', 'waiting_payment_timeout', 'vgs_admin_timeout']) ? 'timeout' : $booking->status;
        return successResponse('success', $data);
    }

    public function releaseBooking(Request $request) {
        $rules = [
            'bookingId' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }
        $booking = \App\BookSlot::find($request->bookingId);

        if (empty($booking)) {
            return errorResponse(trans('api.no_data'));
        }
        
        ProgramSlot::where('booking_id', $booking->id)->update(['program_status' => 1, 'booking_id' => NULL]);
        $booking->status = 'released';
        $booking->status_updated_at = date('Y-m-d H:i:s');
        $booking->save();

        /*if ($booking->status != 'order_confirmed') {
            $payment = \DB::Connection('mysql2')
                    ->table('gv_shopcart_transaction as st')
                    ->join('gv_checkout_transaction as ct', 'ct.shopcartid', '=', 'st.shopcartid')
                    ->join('gv_media_transaction as mt', 'mt.transactionid', '=', 'ct.transactionid')
                    ->where('st.majlisBookingRefId', $booking->id)
                    ->whereNotNull('ct.salecode')
                    ->select('st.id', 'ct.salecode', 'st.shopcartid')
                    ->selectRaw("GROUP_CONCAT(mt.majlis_ticket_id) as ticket_ids")
                    ->first();
            if ($payment) {
                $booking->status = 'order_confirmed';
                $booking->sale_code = $payment->salecode;
                $booking->shop_cart_id = $payment->shopcartid;
                $booking->ticket_ids = !empty($payment->ticket_ids) ? $payment->ticket_ids : null;
                $booking->save();

                //check for vip free table;
                if ($booking->is_vip && ($booking->payment_extras['vipCount'] > 0)) {
                    $season = \App\Season::where('status', 1)->first();
                    $majlisVipTableCountUsed = $booking->payment_extras['vipCount'];

                    $userPacks = \App\Pack::select('packs.id', 'packs.remaining_tables')
                            ->join('users', 'users.pack_id', 'packs.id')
                            ->where(function ($q) use ($booking) {
                                $q->where('packs.drupal_user_id', $booking->booking_user_id)
                                ->orWhere('users.email', $booking->email);
                            })
                            ->where('packs.season_id', $season->id)
                            ->where('packs.remaining_tables', '>', 0);
                    if ($booking->payment_extras['vipType'] == 'fullday') {
                        $userPacks->whereIn('packs.category', ['private', 'platinum', 'diamond'])
                                ->orderByRaw("FIELD(packs.category,'private','platinum','diamond')");
                    } else {
                        $userPacks->whereIn('packs.category', ['gold', 'silver', 'complimentary'])
                                ->orderByRaw("FIELD(packs.category,'gold','silver','complimentary')");
                    }
                    $userPacks->get()->each(function ($pack)use (&$majlisVipTableCountUsed) {
                        if ($majlisVipTableCountUsed > 0) {
                            if ($pack->remaining_tables <= $majlisVipTableCountUsed) {
                                $majlisVipTableCountUsed = $majlisVipTableCountUsed - $pack->remaining_tables;
                                $pack->remaining_tables = 0;
                                $pack->save();
                            } else {
                                $pack->remaining_tables = $pack->remaining_tables - $majlisVipTableCountUsed;
                                $majlisVipTableCountUsed = 0;
                                $pack->save();
                            }
                        }
                    });
                }
            } else {
                ProgramSlot::where('booking_id', $booking->id)->update(['program_status' => 1, 'booking_id' => NULL]);
                $booking->status = 'released';
                $booking->status_updated_at = date('Y-m-d H:i:s');
                $booking->save();
            }
        }*/
        return successResponse(trans('api.success'));
    }

    public function multiTableCheck(&$slotIds, &$request, $tabletypes) {
        $type = $request->table_type;
        $mainSlotArray = $mainSlotArray6 = $previous = $unAvailableSlots = $allTableIds = $allTableIds6 = [];
        $prevSlot = NULL;
        // tabletypes =    [2,4],[2,6],[4,6]
        $seat_type1 = $tabletypes[0]; // request_name1, progrmaSlot4, allTableIds, mainSlotArray4
        $seat_type2 = $tabletypes[1]; // request_name2, progrmaSlot6, allTableIds6, mainSlotArray6

        $request_name1 = ($seat_type1 == 2) ? $request->noOfTables2 : $request->noOfTables4;
        $request_name2 = ($seat_type2 == 4) ? $request->noOfTables4 : $request->noOfTables6;

        foreach ($slotIds as $slot) {
            $progrmaSlot4 = ProgramSlot::where('program_slots.status', 1)
                    ->select('program_slots.slot_id', 'program_slots.table_id', 'program_slots.id', 'program_slots.program_id')
                    ->where('program_slots.program_status', 1)
                    ->where('program_slots.slot_id', $slot)
                    ->where('program_slots.program_date', date("Y-m-d", strtotime($request->programDate)))
                    ->whereNull("program_slots.booking_id")
                    ->join('vip_tables as tb', 'tb.id', '=', 'program_slots.table_id')
                    ->where('tb.status', 1)
                    ->where('tb.type', $type)
                    ->where('tb.smoking_status', $request->smokingType)
                    ->where('tb.seat_type', $seat_type1);
            if (!$request->has('adminId')) {
                $progrmaSlot4->where('tb.admin_only', 0);
            }
            $progrmaSlot4 = $progrmaSlot4->orderby('tb.table_number', 'asc')
                    ->get()
                    ->keyby('table_id')
                    ->toArray();

            $progrmaSlot6 = ProgramSlot::where('program_slots.status', 1)
                    ->select('program_slots.slot_id', 'program_slots.table_id', 'program_slots.id', 'program_slots.program_id')
                    ->where('program_slots.program_status', 1)
                    ->where('program_slots.slot_id', $slot)
                    ->where('program_slots.program_date', date("Y-m-d", strtotime($request->programDate)))
                    ->whereNull("program_slots.booking_id")
                    ->join('vip_tables as tb', 'tb.id', '=', 'program_slots.table_id')
                    ->where('tb.status', 1)
                    ->where('tb.type', $type)
                    ->where('tb.smoking_status', $request->smokingType)
                    ->where('tb.seat_type', $seat_type2);
            if (!$request->has('adminId')) {
                $progrmaSlot6->where('tb.admin_only', 0);
            }
            $progrmaSlot6 = $progrmaSlot6->orderby('tb.table_number', 'asc')
                    ->get()
                    ->keyby('table_id')
                    ->toArray();

            if (!empty($progrmaSlot4) && !empty($progrmaSlot6)) {
                if ($prevSlot != NULL) {
                    $currentIds = array_keys($progrmaSlot4);
                    $allTableIds = array_intersect($allTableIds, $currentIds);
                    $currentIds6 = array_keys($progrmaSlot6);
                    $allTableIds6 = array_intersect($allTableIds6, $currentIds6);
                } else {
                    $allTableIds = array_keys($progrmaSlot4);
                    $allTableIds6 = array_keys($progrmaSlot6);
                }
                //$prevSlot = $slot;
                if ((count($allTableIds) < $request_name1 || count($progrmaSlot4) < $request_name1) || (count($allTableIds6) < $request_name2 || count($progrmaSlot6) < $request_name2)) {
                    $unAvailableSlots[] = $slot;
                } else {
                    $mainSlotArray[$slot] = $progrmaSlot4;
                    $mainSlotArray6[$slot] = $progrmaSlot6;
                }
            } else {
                $unAvailableSlots[] = $slot;
            }
            $prevSlot = $slot;
        }

        return [
            'allTableIds4' => $allTableIds,
            'allTableIds6' => $allTableIds6,
            'mainSlotArray4' => $mainSlotArray,
            'mainSlotArray6' => $mainSlotArray6,
            'unAvailableSlots' => $unAvailableSlots
        ];
    }

    public function singleTableCheck(&$slotIds, $seatType, &$request) {
        $type = $request->table_type;
        $mainSlotArray = $previous = $unAvailableSlots = $allTableIds = [];
        $prevSlot = NULL;

        $noOfTables = ($seatType == 4) ? $request->noOfTables4 : (($seatType == 6) ? $request->noOfTables6 : $request->noOfTables2);
        foreach ($slotIds as $slot) {
            $progrmaSlot = ProgramSlot::where('program_slots.status', 1)
                    ->select('program_slots.slot_id', 'program_slots.table_id', 'program_slots.id', 'program_slots.program_id')
                    ->where('program_slots.program_status', 1)
                    ->where('program_slots.slot_id', $slot)
                    ->where('program_slots.program_date', date("Y-m-d", strtotime($request->programDate)))
                    ->whereNull("program_slots.booking_id")
                    ->join('vip_tables as tb', 'tb.id', '=', 'program_slots.table_id')
                    ->where('tb.status', 1)
                    ->where('tb.type', $type)
                    ->where('tb.smoking_status', $request->smokingType)
                    ->where('tb.seat_type', $seatType);
            if (!$request->has('adminId')) {
                $progrmaSlot->where('tb.admin_only', 0);
            }
            $progrmaSlot = $progrmaSlot->orderby('tb.table_number', 'asc')
                    ->get()
                    ->keyby('table_id')
                    ->toArray();

            if (!empty($progrmaSlot)) {
                if ($prevSlot != NULL) {
                    $currentIds = array_keys($progrmaSlot);
                    $allTableIds = array_intersect($allTableIds, $currentIds);
                } else {
                    $allTableIds = array_keys($progrmaSlot);
                }
                if ((count($allTableIds) < $noOfTables || count($progrmaSlot) < $noOfTables)) {
                    $unAvailableSlots[] = $slot;
                } else {
                    $mainSlotArray[$slot] = $progrmaSlot;
                }
            } else {
                $unAvailableSlots[] = $slot;
            }
            $prevSlot = $slot;
        }

        return [
            'allTableIds' => $allTableIds,
            'mainSlotArray' => $mainSlotArray,
            'unAvailableSlots' => $unAvailableSlots
        ];
    }

    public function threeTableCheck(&$slotIds, &$request) {
        $type = $request->table_type;
        $mainSlotArray = $mainSlotArray6 = $mainSlotArray2 = $previous = $unAvailableSlots = $allTableIds = $allTableIds6 = $allTableIds2 = [];
        $prevSlot = NULL;
        foreach ($slotIds as $slot) {
            $progrmaSlot2 = ProgramSlot::where('program_slots.status', 1)
                    ->select('program_slots.slot_id', 'program_slots.table_id', 'program_slots.id', 'program_slots.program_id')
                    ->where('program_slots.program_status', 1)
                    ->where('program_slots.slot_id', $slot)
                    ->where('program_slots.program_date', date("Y-m-d", strtotime($request->programDate)))
                    ->whereNull("program_slots.booking_id")
                    ->join('vip_tables as tb', 'tb.id', '=', 'program_slots.table_id')
                    ->where('tb.status', 1)
                    ->where('tb.type', $type)
                    ->where('tb.smoking_status', $request->smokingType)
                    ->where('tb.seat_type', 2);
            if (!$request->has('adminId')) {
                $progrmaSlot2->where('tb.admin_only', 0);
            }
            $progrmaSlot2 = $progrmaSlot2->orderby('tb.table_number', 'asc')
                    ->get()
                    ->keyby('table_id')
                    ->toArray();

            $progrmaSlot4 = ProgramSlot::where('program_slots.status', 1)
                    ->select('program_slots.slot_id', 'program_slots.table_id', 'program_slots.id', 'program_slots.program_id')
                    ->where('program_slots.program_status', 1)
                    ->where('program_slots.slot_id', $slot)
                    ->where('program_slots.program_date', date("Y-m-d", strtotime($request->programDate)))
                    ->whereNull("program_slots.booking_id")
                    ->join('vip_tables as tb', 'tb.id', '=', 'program_slots.table_id')
                    ->where('tb.status', 1)
                    ->where('tb.type', $type)
                    ->where('tb.smoking_status', $request->smokingType)
                    ->where('tb.seat_type', 4);
            if (!$request->has('adminId')) {
                $progrmaSlot4->where('tb.admin_only', 0);
            }
            $progrmaSlot4 = $progrmaSlot4->orderby('tb.table_number', 'asc')
                    ->get()
                    ->keyby('table_id')
                    ->toArray();

            $progrmaSlot6 = ProgramSlot::where('program_slots.status', 1)
                    ->select('program_slots.slot_id', 'program_slots.table_id', 'program_slots.id', 'program_slots.program_id')
                    ->where('program_slots.program_status', 1)
                    ->where('program_slots.slot_id', $slot)
                    ->where('program_slots.program_date', date("Y-m-d", strtotime($request->programDate)))
                    ->whereNull("program_slots.booking_id")
                    ->join('vip_tables as tb', 'tb.id', '=', 'program_slots.table_id')
                    ->where('tb.status', 1)
                    ->where('tb.type', $type)
                    ->where('tb.smoking_status', $request->smokingType)
                    ->where('tb.seat_type', 6);
            if (!$request->has('adminId')) {
                $progrmaSlot6->where('tb.admin_only', 0);
            }
            $progrmaSlot6 = $progrmaSlot6->orderby('tb.table_number', 'asc')
                    ->get()
                    ->keyby('table_id')
                    ->toArray();

            if (!empty($progrmaSlot4) && !empty($progrmaSlot6) && !empty($progrmaSlot2)) {
                if ($prevSlot != NULL) {
                    $currentIds = array_keys($progrmaSlot4);
                    $allTableIds = array_intersect($allTableIds, $currentIds);
                    $currentIds6 = array_keys($progrmaSlot6);
                    $allTableIds6 = array_intersect($allTableIds6, $currentIds6);
                    $currentIds2 = array_keys($progrmaSlot2);
                    $allTableIds2 = array_intersect($allTableIds2, $currentIds2);
                } else {
                    $allTableIds = array_keys($progrmaSlot4);
                    $allTableIds6 = array_keys($progrmaSlot6);
                    $allTableIds2 = array_keys($progrmaSlot2);
                }

                if ((count($allTableIds) < $request->noOfTables4 || count($progrmaSlot4) < $request->noOfTables4) || (count($allTableIds6) < $request->noOfTables6 || count($progrmaSlot6) < $request->noOfTables6) || (count($allTableIds2) < $request->noOfTables2 || count($progrmaSlot2) < $request->noOfTables2)) {
                    $unAvailableSlots[] = $slot;
                } else {
                    $mainSlotArray[$slot] = $progrmaSlot4;
                    $mainSlotArray6[$slot] = $progrmaSlot6;
                    $mainSlotArray2[$slot] = $progrmaSlot2;
                }
            } else {
                $unAvailableSlots[] = $slot;
            }
            $prevSlot = $slot;
        }

        return [
            'allTableIds4' => $allTableIds,
            'allTableIds6' => $allTableIds6,
            'mainSlotArray4' => $mainSlotArray,
            'mainSlotArray6' => $mainSlotArray6,
            'unAvailableSlots' => $unAvailableSlots,
            'allTableIds2' => $allTableIds2,
            'mainSlotArray2' => $mainSlotArray2
        ];
    }

    public function ucProgramBookings(Request $request) {
        $rules = [
            'userId' => 'required',
                //'programId' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return errorResponse(trans('api.required_fields'), $errorMessage);
        }

        $upComings = \App\BookSlot::select('book_slots.id', 'book_slots.actual_program_dt', 'book_slots.table_type')
                ->join('programs', 'programs.id', 'book_slots.program_id')
                ->where('book_slots.booking_user_id', $request->userId)
                ->where('book_slots.status', 'order_confirmed')
                ->whereNotNull('book_slots.program_slot_id')
                ->whereRaw('(book_slots.actual_program_dt >= NOW()) AND (book_slots.actual_program_dt <= NOW() + INTERVAL programs.home_display_hours HOUR)')
                ->get()
                ->each(function ($book) {
            $slots = ProgramSlot::select('program_slots.slot_id', 'slots.name as slotName', 'slots.priority', 'slots.start_time', 'slots.end_time', 'vip_tables.seat_type', 'vip_tables.smoking_status', 'vip_tables.type')
                    ->join('slots', 'slots.id', 'program_slots.slot_id')
                    ->join('vip_tables', 'vip_tables.id', 'program_slots.table_id')
                    ->where('program_slots.booking_id', $book->id)->get();
            $book->noOfTables2 = $slots->where('seat_type', 2)->unique('seat_type')->count();
            $book->noOfTables4 = $slots->where('seat_type', 4)->unique('seat_type')->count();
            $book->noOfTables6 = $slots->where('seat_type', 6)->unique('seat_type')->count();
            $book->smokingType = $slots->first()->smoking_status;
            $book->tableType = $slots->first()->type;
            $book->slots = $slots;
        });

        return successResponse(trans('api.success'), $upComings);
    }

}
