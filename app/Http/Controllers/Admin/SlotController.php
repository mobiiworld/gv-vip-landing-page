<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Slot;
use Auth;
use URL;
//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
//Enables us to output flash messaging
use Session;
use Image;

class SlotController extends Controller {

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Ramadan Majlis_slot read', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['index']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Ramadan Majlis_slot create', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['create', 'store']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Ramadan Majlis_slot update', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['edit', 'update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //Get all slots and pass it to the view
        return view('admin.slots.index');
    }

    /**
     * To display dynamic table by datatable
     *
     * @return mixed
     */
    public function datatable() {
        $currentUser = Auth::guard('admin')->user();
        $isSuperAdmin = $currentUser->hasRole('super-admin', 'admin');
        $slots = Slot::select('name', 'start_time', 'end_time', 'status', 'id', 'priority')->orderby('priority', 'asc');

        return Datatables::of($slots)
                        ->rawColumns(['actions', 'status'])
                        ->editColumn('actions', function ($slots) {
                            $b = '<a href="' . URL::route('admin.slots.edit', $slots->id) . '" class="btn btn-primary "><i class="fa fa-edit"></i> Edit </a>';
                            return $b;
                        })
                        ->editColumn('status', function ($slots) use ($currentUser, $isSuperAdmin) {
                            if ($isSuperAdmin || $currentUser->hasPermissionTo('Ramadan Majlis_slot update', 'admin')) {
                                $checked = ($slots->status == 1) ? 'checked' : '';
                                return '<input type="checkbox" name="status" id="status_' . $slots->id . '"  data-on-text="Enabled" data-off-text="Disabled" value="1" ' . $checked . ' class="bsSwitch" autocomplete="off" data-id="' . $slots->id . '">';
                            } else {
                                return ($slots->status == 1) ? '<label class="label label-success">Enabled</label>' : '<label class="label label-danger">Disabled</label>';
                            }
                        })
                        ->editColumn('start_time', function ($slots) {
                            return ($slots->start_time != NULL) ? date("h:i A", strtotime($slots->start_time)) : NULL;
                        })
                        ->editColumn('end_time', function ($slots) {
                            return ($slots->end_time != NULL) ? date("h:i A", strtotime($slots->end_time)) : NULL;
                        })
                        ->make(true);
    }

    public function changeStatus(Request $request) {
        $slot = Slot::find($request->id);
        $slot->status = ($slot->status == 0) ? 1 : 0;
        $slot->save();
        return response()->json(["status" => true, "message" => 'Successfully changed']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.slots.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:120',
            //'priority' => 'required|unique:slots,priority,' . $id,
            'start_time' => 'required',
            'actual_start_time' => 'required_if:nextDay,=,on',
            'actual_end_time' => 'required_if:nextDay,=,on',
        ]);

        date_default_timezone_set('UTC');

        $startTime = new \DateTime(date('h:i a', strtotime($request->starttime)));
        $endTime = new \DateTime(date('h:i a', strtotime($request->endtime)));

        if ($startTime > $endTime) {
            return redirect()->back()->withInput()->with("error", 'Start time is greater than end time.');
        }

        $slot = new Slot;
        $slot->name = $request->name;
        $slot->start_time = $startTime->format('H:i');
        if (!empty($request->actual_start_time)) {
            $actual_start_time = new \DateTime(date('h:i a', strtotime($request->actual_start_time)));
            $slot->actual_start_time = $actual_start_time->format('H:i');
            $actual_end_time = new \DateTime(date('h:i a', strtotime($request->actual_end_time)));
            $slot->actual_end_time = $actual_end_time->format('H:i');
        } else {
            $slot->actual_start_time = null;
            $slot->actual_end_time = null;
        }
        // $endTime = $startTime->modify('+60 minutes');
        $slot->end_time = $endTime->format('H:i');
        $slot->priority = ($request->priority != null) ? $request->priority : 0;
        $slot->weight_count = $request->weight_count;
        $slot->save();

        //Redirect to the slots.index view and display message
        alert()->success('Slot successfully added.', 'Added');
        return redirect()->route('admin.slots.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return redirect('slots');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $user = Slot::findOrFail($id); //Get user with specified id
// dd($user);
        return view('admin.slots.edit', compact('user')); //pass user and roles data to view
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required|max:120',
            //'priority' => 'required|unique:slots,priority,' . $id,
            'start_time' => 'required',
            'actual_start_time' => 'required_if:nextDay,=,on',
            'actual_end_time' => 'required_if:nextDay,=,on',
                ]);

        date_default_timezone_set('UTC');

        $startTime = new \DateTime(date('h:i a', strtotime($request->start_time)));
        $endTime = new \DateTime(date('h:i a', strtotime($request->end_time)));

        if ($startTime > $endTime) {
            return redirect()->back()->withInput()->with("error", 'Start time is greater than end time.');
        }

        $slot = Slot::find($id);
        $slot->name = $request->name;
        $slot->start_time = $startTime->format('H:i');
        if (!empty($request->actual_start_time)) {
            $actual_start_time = new \DateTime(date('h:i a', strtotime($request->actual_start_time)));
            $slot->actual_start_time = $actual_start_time->format('H:i');
            $actual_end_time = new \DateTime(date('h:i a', strtotime($request->actual_end_time)));
            $slot->actual_end_time = $actual_end_time->format('H:i');
        } else {
            $slot->actual_start_time = null;
            $slot->actual_end_time = null;
        }
        $slot->end_time = $endTime->format('H:i');
        $slot->priority = $request->priority;
        $slot->weight_count = $request->weight_count;

        $slot->save();

        alert()->success('Slot details successfully updated.', 'Updated');
        return redirect()->route('admin.slots.index');
    }

}
