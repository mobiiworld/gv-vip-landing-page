<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PartnerOffer;

use Yajra\Datatables\Datatables;
use Auth;
use URL;

class OfferDeleteController extends Controller
{
    public function __construct() {
        set_time_limit(0);
    }
    public function index(){
    	$season = \App\Season::where('status',1)->first();
        if(empty($season)){
            alert()->error('No active season', 'Warning!');
            return redirect()->route('admin.season.index');
        }
        $seasonId = $season->id;
    	return view('admin.offer.del_ind');
    }

    public function datatable(Request $request) {
    	$currentUser = Auth::guard('admin')->user();
        $isSuperAdmin = $currentUser->hasRole('super-admin', 'admin');
        $season = \App\Season::where('status',1)->first();
        $seasonId = $season->id;
    	$offers = PartnerOffer::select('partner_offers.parent_merchant_name_en','partner_offers.merchant_name_en','partner_offers.active_status','partner_offers.open_status','partner_offers.closed_message_en','partner_offers.closed_message_ar','partner_offers.ordering')
        ->addselect(\DB::raw('LOWER(REPLACE(partner_offers.merchant_name_en, " ", "")) as offerName'))
        ->join('packs','packs.id','=','partner_offers.pack_id')->where('packs.season_id',$seasonId)
        ->orderby('ordering','asc')
        ->groupby('offerName');
    	return Datatables::of($offers)
                        ->rawColumns(['actions','active_status','open_status'])
                        ->editColumn('actions', function ($offers) use($isSuperAdmin) {
                        	$b = '';
                        	if ($isSuperAdmin ) {
		                        $b .= ' <a href="' . URL::route('admin.offersdelete.destroy', $offers->offerName) . '" class="btn btn-danger btn-xs destroy"><i class="fa fa-trash"></i></a>';
		                    }
                        	return $b;
                        })
                        ->editColumn('active_status', function ($offers) {                        	
                        	if($offers->active_status == 1) {
                        		return '<label class="label label-success">Active</label>';
                        	}else{
                        		return '<label class="label label-default">Inactive</label>';

                        	} 
                        })
                        ->editColumn('open_status', function ($offers) {
                        	
                        	if($offers->open_status == 1) {
                        		return '<label class="label label-success">Open</label>';
                        	}else{
                        		return '<label class="label label-default">Closed</label>';

                        	}
                        })
                        ->make(true);
    }

    public function destroy($offerName) {
        PartnerOffer::whereRaw('LOWER(REPLACE(partner_offers.merchant_name_en, " ", "")) = "'.$offerName.'" AND created_at >= "2022-08-01 00:00:00"')->delete();
    }
}
