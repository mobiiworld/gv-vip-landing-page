<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use Importer;
use Webpatser\Uuid\Uuid;
use Validator;

class ImportController extends Controller {

    public function __construct() {
       /* $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['import']]);*/
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function import($type) {
        //Get all users and pass it to the view
        $season = \App\Season::where('status',1)->first();
        if(empty($season)){
            alert()->error('No active season', 'Warning!');
            return redirect()->route('admin.season.index');
        }
        $seasonId = $season->id;
        $user = Auth::guard('admin')->user();
        if($type == 'packs'){
            if (!($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('pack_create', 'admin'))) {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }else{
            $user = Auth::guard('admin')->user();
            if (!$user->hasRole('super-admin', 'admin')) {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            } 
        }
        return view('admin.import.' . $type);
    }

    function importPacks(Request $request) {
        $user = Auth::guard('admin')->user();
        if (!($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('pack_create', 'admin'))) {
            alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
            return redirect()->route('admin.dashboard');
        }
        $this->validate($request, [
            'import_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('import_file')->getRealPath();

        $excel = Importer::make('Excel');
        $excel->hasHeader(true);
        $excel->load($path);
        $collection = $excel->getCollection();

        $season = \App\Season::where('status',1)->first();
        if(empty($season)){
            alert()->error('No active season', 'Warning!');
            return redirect()->route('admin.season.index');
        }
        $seasonId = $season->id;
        $d = 0;
        $s = 0;

        set_time_limit(0);

        if ($collection->count() > 0) {
            foreach ($collection->toArray() as $key => $value) {
                $exist = DB::table('packs')->where('season_id',$season->id)->where(function($query) use($value){
                    $query->where('sale_id', trim($value['SaleId']))->orWhere('pack_number', trim($value['Pack Number']));
                })->first();
                if ($exist) {
                    /* DB::table('packs')->where('sale_id', trim($value['SaleId']))
                      ->update(
                      [
                      'sector' => isset($value['Sector/Service']) ? $value['Sector/Service'] : null,
                      'image' => isset($value['Sector/Service']) ? $value['Sector/Service'] . '.png' : 'placeholder-listing.png'
                      ]
                      ); */
                    $d++;
                } else {
                    if(isset($value['Original Category'])){
                        $original_category = strtolower(trim($value['Original Category']));
                        //$category = strtolower(trim($value['Category']));
                        $category = (strtolower(trim($value['Category'])) == 'mini') ? 'complimentary' : strtolower(trim($value['Category']));
                    }else{
                        $original_category = strtolower(trim($value['Category']));
                        $category = ($original_category == 'mini') ? 'complimentary' : $original_category;
                    }
                    $insert_data = array(
                        'id' => Uuid::generate()->string,
                        'sale_id' => trim($value['SaleId']),
                        'activation_code' => trim($value['Activation Code']),
                        'prefix' => trim($value['Prefix']),
                        'pack_number' => trim($value['Pack Number']),
                        'car_activation_allowed' => trim($value['Carplate Activation Allowed']),
                        'park_entry_count' => trim($value['Park Entry Count']),
                        'wonderpass_count' => trim($value['Wonderpass Count']),
                        'parking_voucher_count' => trim($value['Parking Voucher Count']),
                        'allowed_tables' => trim($value['Tables Allowed']),
                        'remaining_tables' => trim($value['Tables Allowed']),
                        'category' => $category,
                        'original_category' => $original_category,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        'season_id' => $seasonId
                    );
                    DB::table('packs')->insert($insert_data);
                    $s++;
                }
            }
        }
        alert()->success($s . ' Packs imported.Duplictates Found: ' . $d, 'Imported')->persistent("Close");
        return redirect()->route('admin.import', ['type' => $request->type]);
    }

    function importWonderpass(Request $request) {
        $this->validate($request, [
            'import_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('import_file')->getRealPath();

        $excel = Importer::make('Excel');
        $excel->hasHeader(true);
        $excel->load($path);
        $collection = $excel->getCollection();

        $c = 0;
        $d = 0;
        $s = 0;

        set_time_limit(0);

        $season = \App\Season::where('status',1)->first();
        if(empty($season)){
            alert()->error('No active season', 'Warning!');
            return redirect()->route('admin.season.index');
        }
        $seasonId = $season->id;

        if ($collection->count() > 0) {
            foreach ($collection->toArray() as $key => $value) {
                $pack = DB::table('packs')->select('id')
                        ->where('pack_number', trim($value['vipPackNumber']))
                        ->where('season_id',$seasonId)
                        ->first();

                if (!empty($pack)) {
                    $exist = DB::table('wonderpasses')
                            ->where('pack_id', $pack->id)
                            ->where('card_number', trim($value['cardNumber']))
                            ->first();
                    if ($exist) {
                        $d++;
                    } else {
                        $insert_data = array(
                            'id' => Uuid::generate()->string,
                            'pack_id' => $pack->id,
                            'card_number' => trim($value['cardNumber']),
                            'balance_points' => (isset($value['balancePoints'])) ? trim($value['balancePoints']) : NULL,
                            'carnaval_ride_points' => (isset($value['carnavalRidePoints'])) ? trim($value['carnavalRidePoints']) : NULL,
                            'ripleys_count' => (isset($value['ripleysCount'])) ? trim($value['ripleysCount']) : NULL,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        );
                        DB::table('wonderpasses')->insert($insert_data);
                        $s++;
                    }
                } else {
                    $c++;
                }
            }
        }
        alert()->success($c . ' pack numbers not found.' . $s . ' Cards imported.Duplictates Cards Found: ' . $d, 'Imported')->persistent("Close");
        return redirect()->route('admin.import', ['type' => $request->type]);
    }

    function importOffers(Request $request) {

      $validator = Validator::make($request->all(), [
                  'import_file' => 'required|mimes:csv,txt',
      ]);

      if ($validator->fails()) {
          return redirect()
                          ->back()
                          ->withErrors($validator)
                          ->withInput();
      }

      $season = \App\Season::where('status',1)->first();
        if(empty($season)){
            alert()->error('No active season', 'Warning!');
            return redirect()->route('admin.season.index');
        }
        $seasonId = $season->id;

      if ($request->hasFile('import_file')) {
          set_time_limit(0);

          $fileD = fopen($request->file('import_file'), "r");
          $column = fgetcsv($fileD);
          while (!feof($fileD)) {
              $rowData[] = fgetcsv($fileD);
          }
          $c = 0;
          $d = 0;
         // dd($rowData);
          foreach ($rowData as $key => $value) {
            if(empty( trim($value['0']))){
                continue;
            }
                $pack = DB::table('packs')->select('id')
                        ->where('pack_number', trim($value['0']))
                        ->where('season_id',$seasonId)
                        ->first();
                if ($pack) {
                  $merchantName = strtolower(str_replace(' ', '', trim($value['2'])));
                  // $existOffer = DB::table('partner_offers')->whereRaw('LOWER(REPLACE(partner_offers.merchant_name_en, " ", "")) = "'.$merchantName.'"')->orderby('created_at','desc')->where('ordering','>',0)->first();
                  // if(!empty($existOffer)){
                  //   $ordering = $existOffer->ordering;
                  // }else{
                  //   $maxOrder = DB::table('partner_offers')->max('ordering');

                  //   $ordering = $maxOrder + 1;
                  // }
                  try{

                    $insert_data = array(
                        'id' => Uuid::generate()->string,
                        'pack_id' => $pack->id,
                        'parent_merchant_name_en' => trim($value['1']),
                        'parent_merchant_name_ar' => trim($value['1']),
                        'merchant_name_en' => trim($value['2']),
                        'merchant_name_ar' => trim($value['2']),
                        'offer_name_en' => trim($value['7']),
                        'offer_name_ar' => trim($value['8']),
                        'valid_from' => date('Y-m-d', strtotime(trim($value['3']))),
                        'valid_to' => date('Y-m-d', strtotime(trim($value['4']))),
                        'promo_code' => trim($value['5']),
                        'code_type' => trim($value['6']),
                        't_and_c' => trim($value['9']),
                        'link' => trim($value['10']),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                       // 'ordering' => $ordering
                    );
                    DB::table('partner_offers')->insert($insert_data);
                    /*\App\PartnerOffer::firstOrCreate([
                        'pack_id' => $pack->id,
                         'promo_code' => trim($value['5'])
                        ],
                        [                            
                            'parent_merchant_name_en' => trim($value['1']),
                            'parent_merchant_name_ar' => trim($value['1']),
                            'merchant_name_en' => trim($value['2']),
                            'merchant_name_ar' => trim($value['2']),
                            'offer_name_en' => trim($value['7']),
                            'offer_name_ar' => trim($value['8']),
                            'valid_from' => date('Y-m-d', strtotime(trim($value['3']))),
                            'valid_to' => date('Y-m-d', strtotime(trim($value['4']))),                            
                            'code_type' => trim($value['6']),
                            't_and_c' => trim($value['9']),
                            'link' => trim($value['10']),
                        ]
                    );*/
                    $d++;
                  }catch(\Exception $e){
                    //dd($value); exit;
                  }
                } else {
                    $c++;
                }

          }

          alert()->success($c . ' pack numbers not found.' . $d . ' Offers imported.', 'Imported')->persistent("Close");
          return redirect()->route('admin.import', ['type' => $request->type]);

        }

        return redirect()->route('admin.import', ['type' => $request->type]);

        $this->validate($request, [
            'import_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('import_file')->getRealPath();

        $excel = Importer::make('Excel');
        $excel->hasHeader(true);
        $excel->load($path);
        $collection = $excel->getCollection();

        $c = 0;
        $d = 0;

        set_time_limit(0);

        $season = \App\Season::where('status',1)->first();
        if(empty($season)){
            alert()->error('No active season', 'Warning!');
            return redirect()->route('admin.season.index');
        }
        $seasonId = $season->id;
        if ($collection->count() > 0) {
            foreach ($collection->toArray() as $key => $value) {
                $pack = DB::table('packs')->select('id')
                        ->where('pack_number', trim($value['vipPackNumber']))
                        ->where('season_id',$seasonId)
                        ->first();
                if ($pack) {

                  try{


                    $insert_data = array(
                        'id' => Uuid::generate()->string,
                        'pack_id' => $pack->id,
                        'parent_merchant_name_en' => trim($value['ParentMerchant_name']),
                        'parent_merchant_name_ar' => trim($value['ParentMerchant_name']),
                        'merchant_name_en' => trim($value['Merchant']),
                        'merchant_name_ar' => trim($value['Merchant']),
                        'offer_name_en' => trim($value['Description']),
                        'offer_name_ar' => trim($value['Description']),
                        'valid_from' => date('Y-m-d', strtotime(trim($value['ValidityStart']))),
                        'valid_to' => date('Y-m-d', strtotime(trim($value['ValidityEnd']))),
                        'promo_code' => trim($value['promoCode']),
                        'code_type' => trim($value['codeType']),
                        't_and_c' => trim($value['T&Cs']),
                        'link' => trim($value['Links']),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    );
                    DB::table('partner_offers')->insert($insert_data);

                    $d++;
                  }catch(\Exception $e){
                    //dd($value); exit;
                  }
                } else {
                    $c++;
                }
            }
        }
        alert()->success($c . ' pack numbers not found.' . $d . ' Offers imported.', 'Imported')->persistent("Close");
        return redirect()->route('admin.import', ['type' => $request->type]);
    }

}
