<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Yajra\Datatables\Datatables;
use App\Notification;

class NotificationController extends Controller {

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('notifications_read', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['index', 'show']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('notifications_create', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['create', 'store']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('notifications_update', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['edit', 'update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('admin.notifications.index');
    }

    public function datatable(Request $request) {
        $currentUser = Auth::guard('admin')->user();
        $isSuperAdmin = $currentUser->hasRole('super-admin', 'admin');
        $datas = Notification::select('*');

        return Datatables::of($datas)
                        ->rawColumns(['actions'])
                        ->editColumn('actions', function ($data) use ($currentUser, $isSuperAdmin) {
                            $b = '';
                            if ($isSuperAdmin || $currentUser->hasPermissionTo('notifications_update', 'admin')) {
                                $b .= '<a class="btn btn-primary btn-xs" href="' . route('admin.notifications.edit', $data->id) . '"><i class="fa fa-edit"></i></a>';
                            }
                            if ($isSuperAdmin || $currentUser->hasPermissionTo('notifications_delete', 'admin')) {
                                $b .= ' <a href="' . route('admin.notifications.destroy', $data->id) . '" class="btn btn-danger btn-xs destroy"><i class="fa fa-trash"></i></a>';
                            }
                            return $b;
                        })
                        ->editColumn('created_at', function ($data) {
                            return $data->created_at->format('F d, Y h:ia');
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.notifications.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'titles.*' => 'required',
            'messages.*' => 'required'
        ]);

        $data = new Notification();
        $data->titles = $request->titles;
        $data->messages = $request->messages;
        $data->save();

        alert()->success('Successfully Created.', 'Created');
        return redirect()->route('admin.notifications.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = Notification::findOrFail($id);
        return view('admin.notifications.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'titles.*' => 'required',
            'messages.*' => 'required'
        ]);

        $data = Notification::findOrFail($id);
        $data->titles = $request->titles;
        $data->messages = $request->messages;
        $data->save();

        alert()->success('Successfully Updated.', 'updated!');
        return redirect()->route('admin.notifications.index');
    }

    public function destroy($id) {
        Notification::destroy($id);
    }

}
