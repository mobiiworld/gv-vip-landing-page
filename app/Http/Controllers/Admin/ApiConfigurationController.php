<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ApiConfiguration;
use Validator;

class ApiConfigurationController extends Controller
{
    public function index()
    {
        //$this->xmlData();
        $configurations = ApiConfiguration::all();
        return view('admin.configuration.index',compact('configurations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $configuration = ApiConfiguration::find($id);
        return view('admin.configuration.edit',compact('configuration'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $configuration = ApiConfiguration::find($id);
        $configuration->value = $request->value;
        $configuration->save();
        alert()->success('Configuration updated.', 'Updated');
        return redirect()->route('admin.configuration.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function platePrefix(){
        $platePrefixes = \App\CarCountry::leftJoin('emirates','emirates.country_id','car_countries.id')->select('car_countries.id as countryId','emirates.id as emirateId','car_countries.code as countryCode','emirates.code as emirateCode',\DB::raw("CONCAT_WS(' - ',car_countries.name_en,emirates.name_en) as displayName"),\DB::raw('IF(emirates.id is null,"country","emirate") as type'),\DB::raw('IF(emirates.id is null,car_countries.plate_prefix_new,emirates.plate_prefix) as platePrefix'),\DB::raw('IF(emirates.id is null,car_countries.code,emirates.code) as code'))->get();
        return view('admin.configuration.plate_prefix',compact('platePrefixes'));

    }

    public function platePrefixEdit($type,$code){
        switch ($type) {
            case 'country':
                $plate = \App\CarCountry::where('code',$code)->select('plate_prefix_new as plate_prefix')->first();
                break;
            case 'emirate':
                $plate = \App\Emirate::where('code',$code)->select('plate_prefix')->first();
                break;
            
            default:
                $plate = \App\CarCountry::where('code',$code)->select('plate_prefix_new as plate_prefix')->first();
                break;
        }

        if(empty($plate)){
            return redirect()->route('admin.platePrefix');
        }

        return view('admin.configuration.plate_prefix_edit',compact('plate','type','code'));
    }

    public function platePrefixUpdate(Request $request, $type,$code){
        $this->validate($request, [
            'prefix' => 'required'
        ]);
        switch ($type) {
            case 'country':
                $plate = \App\CarCountry::where('code',$code)->first();
                break;
            case 'emirate':
                $plate = \App\Emirate::where('code',$code)->first();
                break;
            
            default:
                $plate = \App\CarCountry::where('code',$code)->first();
                break;
        }

        if(empty($plate)){
            return redirect()->route('admin.platePrefix');
        }
        $prefix = array_map('trim',explode(",",$request->prefix));
        //$platePrefix = array_fill_keys($prefix,$prefix);
        $platePrefix = [];
        foreach($prefix as $p){
            if($p != ''){
                $platePrefix[$p] = $p;
            }
        }
        if($type == 'country'){
            $plate->plate_prefix_new = json_encode($platePrefix);
        }else{
            $plate->plate_prefix = json_encode($platePrefix);
        }
        
        $plate->save();
       
        alert()->success('Plate Prefix updated.', 'Updated');
        return redirect()->route('admin.platePrefix');
    }
}
