<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Auth;
use URL;
use App\Program;
use App\ProgramSlot;
use Carbon\Carbon;
use Excel;
use App\Exports\BookingExport;

class ProgramSlotController extends Controller {

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Ramadan Majlis_program read', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['index']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Ramadan Majlis_program create', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['create', 'store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($programId) {
        $slots = \App\Slot::where('status', 1)->orderby('priority', 'asc')->get();
        $tables = \App\VipTable::where('status', 1)->orderby('table_number', 'asc')->get();
        return view('admin.program.slots.index', compact('programId', 'slots', 'tables'));
    }

    public function datatable(Request $request, $programId) {
        $currentUser = Auth::guard('admin')->user();
        $isSuperAdmin = $currentUser->hasRole('super-admin', 'admin');
        $program = ProgramSlot::select('*')->where('program_id', $programId)->orderby('program_date', 'desc');

        if ($request->program_date != '') {
            $program->where('program_date', date('Y-m-d', strtotime($request->program_date)));
        }
        if ($request->slot != '') {
            $program->where('slot_id', $request->slot);
        }
        if ($request->table != '') {
            $program->where('table_id', $request->table);
        }
        if ($request->status != '') {
            $program->where('status', $request->status);
        }
        //dd($programSlot);
        return Datatables::of($program)
                        ->rawColumns(['actions', 'status', 'tableStatus', 'slotStatus', 'bookingStatus'])
                        ->editColumn('actions', function ($program) {
                            
                        })
                        ->editColumn('status', function ($program) use ($currentUser, $isSuperAdmin) {
                            if ($program->booking_id == NULL) {
                                if ($isSuperAdmin || $currentUser->hasPermissionTo('Ramadan Majlis_program update', 'admin')) {
                                    $checked = ($program->status == 1) ? 'checked' : '';
                                    return '<input type="checkbox" name="status" id="status_' . $program->id . '"  data-on-text="Enabled" data-off-text="Disabled" value="1" ' . $checked . ' class="bsSwitch" autocomplete="off" data-id="' . $program->id . '">';
                                } else {
                                    return ($program->status == 1) ? '<label class="label label-success">Enabled</label>' : '<label class="label label-danger">Disabled</label>';
                                }
                            } else {
                                return ($program->status == 1) ? '<label class="label label-success">Enabled</label>' : '<label class="label label-danger">Disabled</label>';
                            }
                        })
                        ->addColumn('slot', function ($program) {
                            return $program->slot->name;
                        })
                        ->addColumn('tableName', function ($program) {
                            return $program->fTable->name . '-' . $program->fTable->type;
                        })
                        ->addColumn('tableNumber', function ($program) {
                            return $program->fTable->table_number;
                        })
                        ->addColumn('bookingStatus', function ($program) use ($currentUser, $isSuperAdmin) {
                            if ($program->booking_id == NULL) {
                                return '';
                            }
                            $booking = \App\BookSlot::find($program->booking_id);
                            $statusArray = ['pending' => 'Pending', 'waiting_confirmation' => 'Waiting Confirmation', 'waiting_payment' => 'Waiting Payment', 'order_confirmed' => 'Order Confirmed', 'timeout' => 'Time out'];
                            $b = (isset($statusArray[$booking->status])) ? $statusArray[$booking->status] : $booking->status;
                            if (($booking->status == 'order_confirmed') && ($program->program_date >= date('Y-m-d'))) {
                                /*if ($isSuperAdmin || $currentUser->hasPermissionTo('Ramadan Majlis_cancel table', 'admin')) {
                                    $b .= ' <button type="button" data-id="' . $program->id . '" class="cancel-book btn btn-lg btn-danger btn-xs">Cancel</button>';
                                }*/
                                if ($isSuperAdmin || $currentUser->hasPermissionTo('Ramadan Majlis_release table', 'admin')) {
                                    $b .= ' <button type="button" data-id="' . $program->id . '" class="release-book btn btn-lg btn-warning btn-xs">Release</button>';
                                }
                            }
                            return $b;
                        })
                        ->addColumn('tableStatus', function ($program) {
                            if (isset($program->fTable->status) && $program->fTable->status == 1) {
                                return '<label class="label label-success">Enabled</label>';
                            } else {
                                return '<label class="label label-danger">Disabled</label>';
                            }
                        })
                        ->addColumn('slotStatus', function ($program) {
                            if (isset($program->slot->status) && $program->slot->status == 1) {
                                return '<label class="label label-success">Enabled</label>';
                            } else {
                                return '<label class="label label-danger">Disabled</label>';
                            }
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($programId) {
        $slots = \App\Slot::where('status', 1)->orderby('priority', 'asc')->get();
        $tables = \App\VipTable::where('status', 1)->orderby('table_number', 'asc')->get()->groupBy('seat_type');
        return view('admin.program.slots.create', compact('programId', 'slots', 'tables'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $programId) {

        // $this->validate($request, [
        // 'date' => 'required',
        // 'slots.*' => 'required',
        // 'tables.*' => 'required',
        // ]);


        $date = ($request->date != null) ? $request->date : $request->daterange;
        if ($request->date == null) {
            $dateRange = explode(" ", $date);
            date_default_timezone_set('UTC');
            $date = [];
            $start_date = date("Y-m-d", strtotime(str_replace("/", "-", $dateRange[0])));
            $end_date = date("Y-m-d", strtotime(str_replace("/", "-", $dateRange[2])));

            while ($start_date <= $end_date) {
                $date[] = $start_date;
                $start_date = date("Y-m-d", strtotime("+1 days", strtotime($start_date)));
            }

            foreach ($request->slots as $slot) {
                foreach ($request->tables as $tab) {
                    foreach ($date as $d) {
                        ProgramSlot::firstOrCreate(['program_id' => $programId, 'slot_id' => $slot, 'table_id' => $tab, 'program_date' => $d]);
                    }
                }
            }
        } else {
            foreach ($request->slots as $slot) {
                foreach ($request->tables as $tab) {
                    ProgramSlot::firstOrCreate(['program_id' => $programId, 'slot_id' => $slot, 'table_id' => $tab, 'program_date' => date("Y-m-d", strtotime($request->date))]);
                }
            }
        }

        alert()->success('Program Slots successfully created.', 'Added');
        return redirect()->route('admin.programs.slots.index', $programId);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function changeStatus(Request $request, $programId) {
        $programSlot = ProgramSlot::find($request->id);
        $programSlot->status = ($programSlot->status == 2) ? 1 : 2;
        $programSlot->save();
        return response()->json(["status" => true, "message" => 'Successfully changed']);
    }

    public function export(Request $request) {
        $input = ['startDate' => $request->startDate, 'endDate' => $request->endDate];
        return Excel::download(new BookingExport($input), 'bookings.xlsx');
    }

    public function cancelSlotBooking(Request $request) {
        $programSlot = ProgramSlot::select('program_slots.id', 'program_slots.booking_id')
                ->selectRaw('CASE WHEN slots.actual_end_time IS NOT NULL THEN (CONCAT(DATE_ADD(program_slots.program_date,INTERVAL 1 DAY)," ",slots.actual_end_time)) ELSE CONCAT(program_slots.program_date," ",slots.end_time) END AS end_time')
                ->join('slots', 'slots.id', '=', 'program_slots.slot_id')
                ->where('program_slots.id', $request->id)
                ->first();
        if ($programSlot->end_time <= date('Y-m-d H:i:00')) {
            return response()->json(["status" => false, "message" => 'Slot Expired']);
        } else {
            $booking = \App\BookSlot::find($programSlot->booking_id);
            $slotIds = explode(',', $booking->program_slot_id);
            $slotIds = implode(',', array_diff($slotIds, [$request->id]));
            $booking->program_slot_id = (empty($slotIds) ? null : $slotIds);

            $cancelledSlot = new \App\CancelledBookingSlot();
            $cancelledSlot->program_slot_id = $request->id;
            $cancelledSlot->booking_id = $programSlot->booking_id;
            $cancelledSlot->admin_id = Auth::guard('admin')->user()->id;

            $programSlot->booking_id = null;
            $programSlot->program_status = 1;
            $programSlot->save();
            $booking->save();
            $cancelledSlot->save();
            return response()->json(["status" => true, "message" => 'Successfully cancelled']);
        }
    }

    public function releaseSlotBooking(Request $request) {
        if ($request->has('slotIds')) {
            $booking = \App\BookSlot::find($request->booking_id);
            $slotIds = explode(',', $booking->program_slot_id);
            $slotIds = implode(',', array_diff($slotIds, $request->slotIds));
            $booking->program_slot_id = (empty($slotIds) ? null : $slotIds);
            $booking->save();

            ProgramSlot::where('booking_id', $request->booking_id)
                    ->whereIn('id', $request->slotIds)
                    ->update(['booking_id' => null, 'program_status' => 1]);

            foreach ($request->slotIds as $sId) {
                $releaseddSlot = new \App\ReleasedBookingSlot();
                $releaseddSlot->program_slot_id = $sId;
                $releaseddSlot->booking_id = $request->booking_id;
                $releaseddSlot->admin_id = auth('admin')->user()->id;
                $releaseddSlot->save();
            }
        } else {
            $programSlot = ProgramSlot::select('program_slots.id', 'program_slots.booking_id')
                    ->selectRaw('CASE WHEN slots.actual_end_time IS NOT NULL THEN (CONCAT(DATE_ADD(program_slots.program_date,INTERVAL 1 DAY)," ",slots.actual_end_time)) ELSE CONCAT(program_slots.program_date," ",slots.end_time) END AS end_time')
                    ->join('slots', 'slots.id', '=', 'program_slots.slot_id')
                    ->where('program_slots.id', $request->id)
                    ->first();
            if ($programSlot->end_time <= date('Y-m-d H:i:00')) {
                return response()->json(["status" => false, "message" => 'Slot Expired']);
            } else {
                $booking = \App\BookSlot::find($programSlot->booking_id);
                $slotIds = explode(',', $booking->program_slot_id);
                $slotIds = implode(',', array_diff($slotIds, [$request->id]));
                $booking->program_slot_id = (empty($slotIds) ? null : $slotIds);

                $cancelledSlot = new \App\ReleasedBookingSlot();
                $cancelledSlot->program_slot_id = $request->id;
                $cancelledSlot->booking_id = $programSlot->booking_id;
                $cancelledSlot->admin_id = Auth::guard('admin')->user()->id;

                $programSlot->booking_id = null;
                $programSlot->program_status = 1;
                $programSlot->save();
                $booking->save();
                $cancelledSlot->save();
            }
        }
        return response()->json(["status" => true, "message" => 'Successfully released']);
    }

}
