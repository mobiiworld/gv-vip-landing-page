<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PartnerOffer;

use Yajra\Datatables\Datatables;
use Auth;
use URL;

class OfferController extends Controller
{
    public function __construct() {
        set_time_limit(0);
    }
    public function index(){
    	$season = \App\Season::where('status',1)->first();
        if(empty($season)){
            alert()->error('No active season', 'Warning!');
            return redirect()->route('admin.season.index');
        }
        $seasonId = $season->id;
    	return view('admin.offer.index');
    }

    public function datatable(Request $request) {
        $season = \App\Season::where('status',1)->first();
        $seasonId = $season->id;
    	$offers = PartnerOffer::select('partner_offers.parent_merchant_name_en','partner_offers.merchant_name_en','partner_offers.active_status','partner_offers.open_status','partner_offers.closed_message_en','partner_offers.closed_message_ar','partner_offers.ordering')
        ->addselect(\DB::raw('LOWER(REPLACE(partner_offers.merchant_name_en, " ", "")) as offerName'))
        ->join('packs','packs.id','=','partner_offers.pack_id')->where('packs.season_id',$seasonId)
        ->orderby('ordering','asc')
        ->groupby('offerName');
    	return Datatables::of($offers)
                        ->rawColumns(['actions','active_status','open_status'])
                        ->editColumn('actions', function ($offers) {
                        	$active = ($offers->active_status == 1) ? 'Deactivate' : 'Activate';
                        	$btn = ($offers->active_status == 1) ? 'primary' : 'success';
                        	$b = '<a href="'.route('admin.offers.changeStatus',['active_status',$active]).'" data-parent="'.$offers->parent_merchant_name_en.'" data-merchant="'.$offers->offerName.'" class="btn btn-sm btn-'.$btn.' statusChange"> '.$active.'</a>';
                            if($offers->closed_message_en != '' && $offers->closed_message_ar != ''){
                                $active = ($offers->open_status == 1) ? 'Close' : 'Open';
                                $btn = ($offers->open_status == 1) ? 'info' : 'success';
                                $b .= ' <a href="'.route('admin.offers.changeStatus',['open_status',$active]).'" class="btn btn-sm btn-'.$btn.' statusChange" data-parent="'.$offers->parent_merchant_name_en.'" data-merchant="'.$offers->offerName.'" >Change to '.$active.'</a>';
                            }
                        	
                        	$b .= ' <a href="'.route('admin.offers.edit',[$offers->parent_merchant_name_en,$offers->offerName]).'" class="btn btn-sm btn-warning">Update Message</a>';
                        	return $b;
                        })
                        ->editColumn('active_status', function ($offers) {                        	
                        	if($offers->active_status == 1) {
                        		return '<label class="label label-success">Active</label>';
                        	}else{
                        		return '<label class="label label-default">Inactive</label>';

                        	} 
                        })
                        ->editColumn('open_status', function ($offers) {
                        	
                        	if($offers->open_status == 1) {
                        		return '<label class="label label-success">Open</label>';
                        	}else{
                        		return '<label class="label label-default">Closed</label>';

                        	}
                        })
                        ->make(true);
    }

    public function changeStatus(Request $request, $type,$status){
        

    	$parentName = $request->parent;
    	$merchantName = $request->merchant;
    	//dd($type);
    	if($type == 'active_status'){
    		$updateStatus = ($status == 'Activate') ? 1 : 0;
    	}else{
    		$updateStatus = ($status == 'Open') ? 1 : 0;
    	}
        $season = \App\Season::where('status',1)->first();
        $seasonId =(!(empty($season))) ? $season->id : NULL;
        $packIds = \App\Pack::where('season_id',$seasonId)->pluck('id');
    	PartnerOffer::
        //where('parent_merchant_name_en',$parentName)
        //->where('merchant_name_en',$merchantName)
        whereRaw('LOWER(REPLACE(partner_offers.merchant_name_en, " ", "")) = "'.$merchantName.'"')
        ->whereIn('pack_id',$packIds)
        ->update([$type => $updateStatus]);
    	$message = "Update successfully";
    	return response()->json(["status" => true, "message" => $message]);
    }

    public function edit($parentName, $merchantName){
        $season = \App\Season::where('status',1)->first();
        $seasonId = (!(empty($season))) ? $season->id : NULL;
        $packIds = \App\Pack::where('season_id',$seasonId)->pluck('id');
    	$note = PartnerOffer::
        //where('parent_merchant_name_en',$parentName)->where('merchant_name_en',$merchantName)
        whereRaw('LOWER(REPLACE(partner_offers.merchant_name_en, " ", "")) = "'.$merchantName.'"')
        ->whereIn('pack_id',$packIds)->first();
    	return view('admin.offer.edit',compact('note'));
    }

    public function update(Request $request, $parentName, $merchantName){
        $season = \App\Season::where('status',1)->first();
        $seasonId = (!(empty($season))) ? $season->id : NULL;
        $packIds = \App\Pack::where('season_id',$seasonId)->pluck('id');
        //\DB::enableQueryLog();
    	PartnerOffer::
        //where('parent_merchant_name_en',$parentName)->where('merchant_name_en',$merchantName)
        whereRaw('LOWER(REPLACE(partner_offers.merchant_name_en, " ", "")) = "'.$merchantName.'"')
        ->whereIn('pack_id',$packIds)->update(['closed_message_en' => $request->closed_message_en, 'closed_message_ar' => $request->closed_message_ar, 'ordering' =>$request->ordering]);
        //dd(\DB::getQueryLog());
    	alert()->success('Message', 'updated!');
        return redirect()->route('admin.offers.index');
    }

    public function updateExistingOrder(){

        $offers = array(
                    'Expo 2020 Dubai', 
                    'Roxy Cinemas',
                    'Laguna Waterpark',
                    'Laguna Water Park',
                    'The Green Planet',
                    'DPR - All Parks',
                    'Dubai Parks and Resorts',
                    'Bollywood Parks Dubai',
                    'Motiongate Dubai',
                    'LEGOLAND Dubai',
                    'LEGOLAND WaterPark',
                    'LEGOLAND Water Park'
        );
        $ordering = 1;
        foreach($offers as $ofr){
            PartnerOffer::where('merchant_name_en',$ofr)->update(['ordering' => $ordering]);
            $ordering++;
        }
    }

    public function updateExisting(){
    	$offerNotes = [
    		'LEGOLAND WaterPark' => [
    			'en' => 'LEGOLAND® Water Park is currently closed. Stay tuned as the re-opening dates will be announced soon!',
    			'ar' => 'تعلن دبي باركس آند ريزورتس عن تمديد تعليق العمليات التشغيلية في ليجولاند ووتر بارك.  ولا يسعنا الانتظار للترحيب بكم مرة أخرى، كما نرجو منكم زيارة موقعنا الإلكتروني للتعرف على آخر المستجدات.',
    		],
    		'Bollywood Parks Dubai' => [
    			'en' => 'BOLLYWOOD PARKS™ Dubai is currently closed. Stay tuned as the re-opening dates will be announced soon!',
    			'ar' => 'تعلن دبي باركس آند ريزورتس عن تمديد تعليق العمليات التشغيلية في بوليوود باركس دبي.  ولا يسعنا الانتظار للترحيب بكم مرة أخرى، كما نرجو منكم زيارة موقعنا الإلكتروني للتعرف على آخر المستجدات.',
    		],
    		'DPR - All Parks' => [
    			'en' => 'Bollywood Park Dubai, LEGOLAND Dubai and LEGOLAND Water Park are currently closed. Thus Four Parks Annual Pass product is temporarily unavailable. Stay tuned as the re-opening dates will be announced soon',
    			'ar' => 'بوليوود بارك دبي و LEGOLAND Dubai و LEGOLAND Water Park مغلقة حاليًا. وبالتالي، فإن جواز التذكرة السنوية لأربعة مدن ترفيهية هو غير متاح مؤقتًا. ابق على اتصال حيث سيتم الإعلان عن مواعيد إعادة الافتتاح قريبًا',
    		],
    		'LEGOLAND Dubai' => [
    			'en' => 'LEGOLAND® Dubai is currently closed. Stay tuned as the re-opening dates will be announced soon!',
    			'ar' => 'تعلن دبي باركس آند ريزورتس عن تمديد تعليق العمليات التشغيلية في ليجولاند دبي. و لا يسعنا الانتظار للترحيب بكم مرة أخرى، كما نرجو منكم زيارة موقعنا الإلكتروني للتعرف على آخر المستجدات.',
    		]
    	];
    	foreach($offerNotes as $key=>$val){
    		PartnerOffer::where('merchant_name_en',$key)->update(['closed_message_en' => $val['en'], 'closed_message_ar' => $val['ar']]);
    	}
    	PartnerOffer::whereIn('merchant_name_en',['LEGOLAND WaterPark','DPR - All Parks'])->update(['open_status' => 0]);
    	PartnerOffer::where('merchant_name_en','Laguna Waterpark')->update(['active_status' => 0]);
    }

    public function offerLinks(Request $request){
        set_time_limit(0);
        $link = ($request->link != '') ? $request->link : 'https://www.dubaiparksandresorts.com/en/GVSeason26-VIPOffers';
        $mercahnt_list = '"dubaiparksandresorts","dpr","dpr-allparks","bollywoodparksdubai","motiongatedubai","legolanddubai","legolandwaterpark"';
        PartnerOffer::whereRaw('LOWER(REPLACE(partner_offers.merchant_name_en, " ", "")) IN ('.$mercahnt_list.')')
        ->whereRaw('LOWER(REPLACE(partner_offers.code_type, " ", "")) = "promocode"')
        ->update(['link' => $link]);
        echo "updated";
    }

     
}
