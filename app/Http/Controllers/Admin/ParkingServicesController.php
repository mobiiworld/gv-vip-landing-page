<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ParkingServices;
use Yajra\Datatables\Datatables;
use App\Traits\ImageTraits;
use Auth;
use URL;

class ParkingServicesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use ImageTraits;

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('vehicle count_read', 'admin')) {
                return $next($request);
            } else {
                alert()->error('You don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['index', 'show']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('vehicle count_create', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['create', 'store']]);

        $this->middleware(
                function ($request, $next) {
                    $user = Auth::guard('admin')->user();
                    if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('vehicle count_update', 'admin')) {
                        return $next($request);
                    } else {
                        alert()->error('You don\'t have permission to acces this page.', 'No Access!');
                        return redirect()->route('admin.dashboard');
                    }
                }, ['only' => ['edit', 'update']]
        );
    }

    public function index() {

        return view('admin.vehicle_pl.services.index');
    }

    public function datatable(Request $request) {

        $services = ParkingServices::select('*');

        return Datatables::of($services)
                        ->rawColumns(['actions'])
                        ->editColumn('actions', function ($fs) {
                            $b = ' <a class="btn btn-primary btn-sm " href="' . route('admin.parking-services.edit', $fs->id) . '">Edit</a>';
                            return $b;
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.vehicle_pl.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'names.*' => 'required',
            'icon' => 'required|image|mimes:jpeg,png,jpg,gif'
        ]);

        $service = new ParkingServices();
        $service->names = $request->names;
        $service->descriptions = $request->descriptions;
        $service->icon = $this->resize($request->icon, 'service_icons/');
        $service->save();

        alert()->success('Successfully created.', 'Created');
        return redirect()->route('admin.parking-services.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $service = ParkingServices::where('id', $id)->first();
        return view('admin.vehicle_pl.services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'names.*' => 'required',
        ]);

        $service = ParkingServices::findOrFail($id);
        $service->names = $request->names;
        $service->descriptions = $request->descriptions;
        if ($request->file('icon') != null) {
            $newIcon = $this->resize($request->icon, 'service_icons/');
            if (!empty($service->icon)) {
                \Storage::disk('azure')->delete('service_icons/' . $service->getOriginal('icon'));
            }
            $service->icon = $newIcon;
        }
        $service->save();

        alert()->success('Successfully updated.', 'Updated');
        return redirect()->back();
    }

}
