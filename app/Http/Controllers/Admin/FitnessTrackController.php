<?php

namespace App\Http\Controllers\Admin;

use App\FitnessSeason;
use App\FitnessDailyTracks;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Auth;

class FitnessTrackController extends Controller {

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('fitness_daily tracks', 'admin')) {
                return $next($request);
            } else {
                alert()->error('You don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['index', 'show']]);
    }

    public function index(Request $request, $cat) {
        $seasons = FitnessSeason::select('id', "names->en as name")
                ->orderBy('start_date', 'DESC')
                ->pluck('name', 'id');
        return view("admin.fitness_seasons.{$cat}_tracks", compact('seasons'));
    }

    public function dailyTracks(Request $request) {
        $tracks = FitnessDailyTracks::select('fitness_daily_tracks.id', 'fitness_daily_tracks.steps', 'fitness_daily_tracks.coupon_code', 'fitness_daily_tracks.prize_collected_on', 'fitness_daily_tracks.participated_date', 'fu.fname', 'fu.lname', 'fu.email')
                ->join('fitness_users as fu', 'fu.id', 'fitness_daily_tracks.fitness_user_id')
                ->where('fitness_daily_tracks.fitness_season_id', $request->sid);
        if (!empty($request->start_date)) {
            $tracks->whereDate('fitness_daily_tracks.participated_date', '>=', date('Y-m-d', strtotime($request->start_date)));
        }
        if (!empty($request->end_date)) {
            $tracks->whereDate('fitness_daily_tracks.participated_date', '<=', date('Y-m-d', strtotime($request->end_date)));
        }
        if ($request->status != 0) {
            $tracks->where('fitness_daily_tracks.status', $request->status);
        }

        return Datatables::of($tracks)
                        ->rawColumns(['prize_collected_on'])
                        ->editColumn('participated_date', function ($track) {
                            return date('M d Y', strtotime($track->participated_date));
                        })
                        ->editColumn('prize_collected_on', function ($track) {
                            if (empty($track->prize_collected_on)) {
                                if (!empty($track->coupon_code)) {
                                    return '<a class="btn btn-primary btn-xs markCollected" data-id="' . $track->id . '" href="javascript:void(0);">Mark as collected</a>';
                                }
                                return '';
                            }
                            return date('M d, Y', strtotime($track->prize_collected_on));
                        })
                        ->make(true);
    }

    public function markAsCollected(Request $request, $type) {
        $cDate = date('Y-m-d', strtotime($request->collected_on));
        switch ($type) {
            case 'daily':
                FitnessDailyTracks::where('id', $request->track_id)
                        ->update(['prize_collected_on' => $cDate]);
                break;
        }
        return response()->json(["status" => true]);
    }

    public function getDeletePage(){
        $seasons = FitnessSeason::select('id', "names->en as name")
                ->orderBy('start_date', 'DESC')
                ->pluck('name', 'id');
        return view("admin.fitness_seasons.delete_record",compact('seasons'));
    }
    public function deleteRecords(){
        $currentUser = Auth::guard('admin')->user();
        $isSuperAdmin = $currentUser->hasRole('super-admin');
        if($isSuperAdmin ){
            // \Schema::disableForeignKeyConstraints();
            // \DB::table('fitness_daily_tracks')->truncate();
            // \DB::table('fitness_monthly_tracks')->truncate();
            // \DB::table('fitness_users')->truncate();       
            // \Schema::enableForeignKeyConstraints();     
            alert()->success('Records deleted.', 'Success');
            return redirect()->route('admin.fitness-track-delete');
        }else{
            alert()->error('You dont have permission to delete.', 'Sorry');
            return redirect()->route('admin.fitness-track-delete');
        }
    }
}
