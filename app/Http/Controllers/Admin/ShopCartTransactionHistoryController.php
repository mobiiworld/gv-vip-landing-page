<?php

namespace App\Http\Controllers\Admin;

use App\AuditLog;
use App\Http\Controllers\Controller;
use App\ShoppingCartTransaction;
use App\UserPaymentSessions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Datatables;
use App\Admin;
use Auth;
use URL;
use DB;
use Excel;
use App\Exports\ShopCartTransactionExport;
//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
//Enables us to output flash messaging
use Validator;

class ShopCartTransactionHistoryController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get all users and pass it to the view
        return view('admin.cart_history_transaction.index');
    }
    //using php curl (sudo apt-get install php-curl)
    public function httpPost($url, $data)
    {
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        return file_get_contents($url, false, $context);
    }

    /**
     * To display dynamic table by datatable
     *
     * @return mixed
     */
    public function datatable(Request $request)
    {
        $transaction = DB::Connection('mysql3')
            ->table('gv_shopcart_transaction')
            /*->join('gv_user_payment_sessions', 'gv_shopcart_transaction.shopcartid', '=', 'gv_user_payment_sessions.shopcartid')
            ->leftJoin( "gv_checkout_transaction", function ($q) {
              $q->on('gv_shopcart_transaction.shopcartid', '=',
               'gv_checkout_transaction.shopcartid')
               ->whereNotNull('gv_checkout_transaction.shopcartid');
            })
            ->leftJoin( "gv_user_payments", function ($q) {
              $q->on('gv_user_payment_sessions.id', '=',
               'gv_user_payments.sessionid')
               ->whereNotNull('gv_user_payments.sessionid');
            })*/
            ->join('gv_user_payment_sessions', 'gv_shopcart_transaction.shopcartid', '=', 'gv_user_payment_sessions.shopcartid')
            //->leftJoin('gv_checkout_transaction', 'gv_shopcart_transaction.shopcartid', '=', 'gv_checkout_transaction.shopcartid')
            //->leftJoin('gv_user_payments', 'gv_user_payment_sessions.id', '=', 'gv_user_payments.sessionid')
            ->select(
                'gv_shopcart_transaction.salecode',
                'gv_shopcart_transaction.order_id',
                'gv_shopcart_transaction.totalAmount',
                'gv_shopcart_transaction.email',
                'gv_shopcart_transaction.firstname',
                'gv_shopcart_transaction.lastname',
                'gv_shopcart_transaction.mobile',
                'gv_shopcart_transaction.created_date',
                'gv_shopcart_transaction.shopcartid',
                'gv_shopcart_transaction.id',
                'gv_user_payment_sessions.platform',
                'gv_shopcart_transaction.transaction_status',
                'gv_user_payment_sessions.status as payment_status',
                'gv_shopcart_transaction.status',
                'gv_shopcart_transaction.ticket_type',
            );

        // $season = \App\Season::where('status',1)->first();
        // $season_number = (isset($season->season_number)) ? $season->season_number : 28;
        $season_number = !empty($request->season_no) ? $request->season_no : '27';
        $transaction->where('gv_user_payment_sessions.season_number', $season_number);


        //            ->whereRaw('gv_shopcart_transaction.created_date IN (select MAX(gv_int.created_date) FROM gv_shopcart_transaction as gv_int GROUP BY gv_int.shopcartid)');

        if (!empty($request->transaction_status)) {
            $status = ($request->transaction_status == "active") ? 1 : 0;
            $transaction = $transaction->where('gv_shopcart_transaction.status', $status);
        }
        if (!empty($request->payment_status)) {
            $transaction_status =  $request->payment_status;
            $transaction = $transaction->where('gv_user_payment_sessions.status', $transaction_status);
        }
        if (!empty($request->plate_form)) {
            $plate_form =  $request->plate_form;
            $transaction = $transaction->where('gv_user_payment_sessions.platform', $plate_form);
        }
        if (!empty($request->start_date) && !empty($request->end_date)) {
            $start_date = date('Y-m-d', strtotime($request->start_date));
            $end_date = date('Y-m-d', strtotime($request->end_date));
            $transaction =  $transaction->whereDate('gv_shopcart_transaction.created_date', '>=', $start_date)->whereDate('gv_shopcart_transaction.created_date', '<=', $end_date);
        } else {
            if (
                empty($request->transaction_status) && empty($request->payment_status)
                && empty($request->plate_form) && empty($request->salecode)
                && empty($request->order_ref_no) && empty($request->firstname)
                && empty($request->lastname) && empty($request->email)
                && empty($request->mobile)
            ) {

                $start_date = date('Y-m-d');
                $transaction =  $transaction->whereDate('gv_shopcart_transaction.created_date', '>=', $start_date);
            }
        }
        if (!empty($request->salecode)) {
            $search_val = $request->salecode;
            $transaction = $transaction->where('gv_shopcart_transaction.salecode', 'LIKE', '%' . $search_val . '%');
        }
        if (!empty($request->order_ref_no)) {
            $search_val = $request->order_ref_no;
            $transaction = $transaction->where('gv_shopcart_transaction.order_id', 'LIKE', '%' . $search_val . '%');
        }
        if (!empty($request->firstname)) {
            $search_val = $request->firstname;
            $transaction = $transaction->where('gv_shopcart_transaction.firstname', 'LIKE', '%' . $search_val . '%');
        }
        if (!empty($request->lastname)) {
            $search_val = $request->lastname;
            $transaction = $transaction->where('gv_shopcart_transaction.lastname', 'LIKE', '%' . $search_val . '%');
        }
        if (!empty($request->email)) {
            $search_val = $request->email;
            $transaction = $transaction->where('gv_shopcart_transaction.email', 'LIKE', '%' . $search_val . '%');
        }
        if (!empty($request->mobile)) {
            $search_val = $request->mobile;
            $transaction = $transaction->where('gv_shopcart_transaction.mobile', 'LIKE', '%' . $search_val . '%');
        }

        $transaction =  $transaction->groupBy('gv_shopcart_transaction.shopcartid')->orderBy('gv_shopcart_transaction.created_date', 'desc');

        return Datatables::of($transaction)
            ->rawColumns(['transaction_status', 'payment_status', 'action'])
            ->editColumn('action', function ($transaction) {
                $b = '';
                $b .= '<a href="' . URL::route('admin.shop-cart-transaction.show', $transaction->id) . '" class="btn btn-outline-primary "><i class="fa fa-eye"></i></a>';

                if ($transaction->transaction_status) {
                    $b .= '<a title="Send Mail" href="javascript:void(0)" style="width:95px;"  class="open-popup-content btn btn-primary btn-xs " data-popup-action-type = "send-mail" data-popup-action-id = "' . $transaction->id . '"><i class="fa fa-envelope-o"></i> Send Email</a>';
                } else {
                    if ($transaction->status == 1 && $transaction->payment_status == 4) {
                        $b .= ' <a title="Generate Ticket" href="javascript:void(0)" style="width:95px;"  class="open-popup-content btn btn-success btn-xs " data-popup-action-type = "generate-ticket" data-popup-action-id = "' . $transaction->id . '"><i class="fa fa-ticket"></i> Generate Ticket</a>';
                    } else {
                        $b .= ' <a disabled title="Generate Ticket" href="javascript:void(0)" style="width:95px;"  class="btn btn-success btn-xs"><i class="fa fa-ticket"></i> Generate Ticket</a>';
                    }
                }

                return $b;
            })
            ->editColumn('created_date', function ($transaction) {
                return (!empty($transaction->created_date)) ? date('F d, Y h:ia', strtotime($transaction->created_date)) : '';
            })->editColumn('transaction_status', function ($transaction) {
                if ($transaction->transaction_status) {
                    return '<span class="label label-success">Complete</span>';
                } else {
                    if ($transaction->status == 1) {
                        return '<span class="label label-warning">Active</span>';
                    } else {
                        return '<span class="label label-danger">Inactive</span>';
                    }
                }
            })->editColumn('payment_status', function ($transaction) {
                $label = '';
                if ($transaction->payment_status == 1) {
                    $label .= '<span class="label label-danger">Created</span>';
                } elseif ($transaction->payment_status == 2) {
                    $label .= '<span class="label label-info">Initiated</span>';
                } elseif ($transaction->payment_status == 3) {
                    $label .= '<span class="label label-warning">Authenticate</span>';
                } elseif ($transaction->payment_status == 4) {
                    $label .= '<span class="label label-success">Paid</span>';
                } else {
                    $label .= '<span class="label label-default">Not Paid</span>';
                }
                $label .= '&nbsp;&nbsp; <a href="javascript:void(0)" data-popup-action-type = "check-payment-status" data-popup-action-id = "' . $transaction->id . '" style="cursor: pointer;" class="open-popup-content" title="Check Payment Status"><i class="fa fa-reply" aria-hidden="true"></i></a>';
                return $label;
            })
            ->make(true);
    }

    /**
     * To export dynamic table by datatable
     *
     * @return mixed
     */
    /*
    public function export(Request $request) {

        ini_set('max_execution_time', '0');
        $input = [
            'transaction_status' => $request->transaction_status,
            'payment_status' => $request->payment_status,
            'plate_form' => $request->plate_form,
            'start_date'=> $request->start_date,
            'end_date'=> $request->end_date,
            'salecode'=> $request->salecode,
            'order_ref_no'=> $request->order_ref_no,
            'payment_status'=> $request->payment_status,
            'firstname'=> $request->firstname,
            'lastname'=> $request->lastname,
            'email'=> $request->email,
            'mobile'=> $request->mobile
        ];
        return Excel::download(new ShopCartTransactionExport($input), 'transactions_'.time().'.xlsx');

    }*/




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getArchieve()
    {
        //Get all users and pass it to the view
        return view('admin.cart_history_transaction.archieve');
    }


    /**
     * To display dynamic table by datatable
     *
     * @return mixed
     */
    public function arhieveDatatable(Request $request)
    {

        $transaction = DB::Connection('mysql3')
            ->table('gv_shopcart_transaction')
            /*->join('gv_user_payment_sessions', 'gv_shopcart_transaction.shopcartid', '=', 'gv_user_payment_sessions.shopcartid')
            ->leftJoin( "gv_checkout_transaction", function ($q) {
              $q->on('gv_shopcart_transaction.shopcartid', '=',
               'gv_checkout_transaction.shopcartid')
               ->whereNotNull('gv_checkout_transaction.shopcartid');
            })
            ->leftJoin( "gv_user_payments", function ($q) {
              $q->on('gv_user_payment_sessions.id', '=',
               'gv_user_payments.sessionid')
               ->whereNotNull('gv_user_payments.sessionid');
            })*/
            ->join('gv_user_payment_sessions', 'gv_shopcart_transaction.shopcartid', '=', 'gv_user_payment_sessions.shopcartid')
            ->leftJoin('gv_checkout_transaction', 'gv_shopcart_transaction.shopcartid', '=', 'gv_checkout_transaction.shopcartid')
            ->leftJoin('gv_user_payments', 'gv_user_payment_sessions.id', '=', 'gv_user_payments.sessionid')
            ->select(
                'gv_checkout_transaction.salecode',
                'gv_user_payments.order_id',
                'gv_checkout_transaction.totalamount',
                'gv_shopcart_transaction.email',
                'gv_shopcart_transaction.firstname',
                'gv_shopcart_transaction.lastname',
                'gv_shopcart_transaction.mobile',
                'gv_shopcart_transaction.created_date',
                'gv_shopcart_transaction.shopcartid',
                'gv_shopcart_transaction.id',
                'gv_user_payment_sessions.platform',
                'gv_checkout_transaction.status as transaction_status',
                'gv_user_payment_sessions.status as payment_status',
                'gv_shopcart_transaction.status'
            );


        //            ->whereRaw('gv_shopcart_transaction.created_date IN (select MAX(gv_int.created_date) FROM gv_shopcart_transaction as gv_int GROUP BY gv_int.shopcartid)');

        if (!empty($request->transaction_status)) {
            $status = ($request->transaction_status == "active") ? 1 : 0;
            $transaction = $transaction->where('gv_shopcart_transaction.status', $status);
        }
        if (!empty($request->payment_status)) {
            $transaction_status =  $request->payment_status;
            $transaction = $transaction->where('gv_user_payment_sessions.status', $transaction_status);
        }
        if (!empty($request->plate_form)) {
            $plate_form =  $request->plate_form;
            $transaction = $transaction->where('gv_user_payment_sessions.platform', $plate_form);
        }
        if (!empty($request->start_date) && !empty($request->end_date)) {
            $start_date = date('Y-m-d', strtotime($request->start_date));
            $end_date = date('Y-m-d', strtotime($request->end_date));
            $transaction =  $transaction->whereDate('gv_shopcart_transaction.created_date', '>=', $start_date)->whereDate('gv_shopcart_transaction.created_date', '<=', $end_date);
        } else {
            $start_date = date('Y-m-d');
            $transaction =  $transaction->whereDate('gv_shopcart_transaction.created_date', '>=', $start_date);
        }
        if (!empty($request->salecode)) {
            $search_val = $request->salecode;
            $transaction = $transaction->where('gv_checkout_transaction.salecode', 'LIKE', '%' . $search_val . '%');
        }
        if (!empty($request->order_ref_no)) {
            $search_val = $request->order_ref_no;
            $transaction = $transaction->where('gv_user_payments.order_id', 'LIKE', '%' . $search_val . '%');
        }
        if (!empty($request->firstname)) {
            $search_val = $request->firstname;
            $transaction = $transaction->where('gv_shopcart_transaction.firstname', 'LIKE', '%' . $search_val . '%');
        }
        if (!empty($request->lastname)) {
            $search_val = $request->lastname;
            $transaction = $transaction->where('gv_shopcart_transaction.lastname', 'LIKE', '%' . $search_val . '%');
        }
        if (!empty($request->email)) {
            $search_val = $request->email;
            $transaction = $transaction->where('gv_shopcart_transaction.email', 'LIKE', '%' . $search_val . '%');
        }
        if (!empty($request->mobile)) {
            $search_val = $request->mobile;
            $transaction = $transaction->where('gv_shopcart_transaction.mobile', 'LIKE', '%' . $search_val . '%');
        }
        $transaction =  $transaction->groupBy('gv_shopcart_transaction.shopcartid')->orderBy('gv_shopcart_transaction.created_date', 'desc');

        return Datatables::of($transaction)
            ->rawColumns(['transaction_status', 'payment_status', 'action'])
            ->editColumn('action', function ($transaction) {
                $b = '';
                $b .= '<a href="' . URL::route('admin.shop-cart-transaction.show', $transaction->id) . '" class="btn btn-outline-primary "><i class="fa fa-eye"></i></a>';

                if ($transaction->transaction_status) {
                    $b .= '<a title="Send Mail" href="javascript:void(0)" style="width:95px;"  class="open-popup-content btn btn-primary btn-xs " data-popup-action-type = "send-mail" data-popup-action-id = "' . $transaction->id . '"><i class="fa fa-envelope-o"></i> Send Email</a>';
                } else {
                    if ($transaction->status == 1 && $transaction->payment_status == 4) {
                        $b .= ' <a title="Generate Ticket" href="javascript:void(0)" style="width:95px;"  class="open-popup-content btn btn-success btn-xs " data-popup-action-type = "generate-ticket" data-popup-action-id = "' . $transaction->id . '"><i class="fa fa-ticket"></i> Generate Ticket</a>';
                    } else {
                        $b .= ' <a disabled title="Generate Ticket" href="javascript:void(0)" style="width:95px;"  class="btn btn-success btn-xs"><i class="fa fa-ticket"></i> Generate Ticket</a>';
                    }
                }

                return $b;
            })
            ->editColumn('created_date', function ($transaction) {
                return (!empty($transaction->created_date)) ? date('F d, Y h:ia', strtotime($transaction->created_date)) : '';
            })->editColumn('transaction_status', function ($transaction) {
                if ($transaction->transaction_status) {
                    return '<span class="label label-success">Complete</span>';
                } else {
                    if ($transaction->status == 1) {
                        return '<span class="label label-warning">Active</span>';
                    } else {
                        return '<span class="label label-danger">Inactive</span>';
                    }
                }
            })->editColumn('payment_status', function ($transaction) {
                $label = '';
                if ($transaction->payment_status == 1) {
                    $label .= '<span class="label label-danger">Created</span>';
                } elseif ($transaction->payment_status == 2) {
                    $label .= '<span class="label label-info">Initiated</span>';
                } elseif ($transaction->payment_status == 3) {
                    $label .= '<span class="label label-warning">Authenticate</span>';
                } elseif ($transaction->payment_status == 4) {
                    $label .= '<span class="label label-success">Paid</span>';
                } else {
                    $label .= '<span class="label label-default">Not Paid</span>';
                }
                $label .= '&nbsp;&nbsp; <a href="javascript:void(0)" data-popup-action-type = "check-payment-status" data-popup-action-id = "' . $transaction->id . '" style="cursor: pointer;" class="open-popup-content" title="Check Payment Status"><i class="fa fa-reply" aria-hidden="true"></i></a>';
                return $label;
            })
            ->make(true);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCallCenterData()
    {
        //Get all users and pass it to the view
        return view('admin.cart_history_transaction.callcenter');
    }


    /**
     * To display dynamic table by datatable
     *
     * @return mixed
     */
    public function getCCDatatable(Request $request)
    {

        $transaction = DB::Connection('mysql3')
            ->table('gv_shopcart_transaction')
            ->select(
                'gv_shopcart_transaction.salecode',
                'gv_shopcart_transaction.order_id',
                'gv_shopcart_transaction.totalAmount',
                'gv_shopcart_transaction.email',
                'gv_shopcart_transaction.firstname',
                'gv_shopcart_transaction.lastname',
                'gv_shopcart_transaction.mobile',
                'gv_shopcart_transaction.created_date',
                'gv_shopcart_transaction.shopcartid',
                'gv_shopcart_transaction.id',
                //'gv_user_payment_sessions.platform',
                'gv_shopcart_transaction.transaction_status',
                //'gv_user_payment_sessions.status as payment_status',
                'gv_shopcart_transaction.status',
                'gv_shopcart_transaction.ticket_type'
            );


        //            ->whereRaw('gv_shopcart_transaction.created_date IN (select MAX(gv_int.created_date) FROM gv_shopcart_transaction as gv_int GROUP BY gv_int.shopcartid)');
        $season = \App\Season::where('status', 1)->first();
        //   $season_number = (isset($season->season_number)) ? $season->season_number : 28;
        $season_number = !empty($request->season_no) ? $request->season_no : '27';
        $transaction->where('gv_shopcart_transaction.season_number', $season_number);

        if (!empty($request->transaction_status)) {
            $status = ($request->transaction_status == "active") ? 1 : 0;
            $transaction = $transaction->where('gv_shopcart_transaction.status', $status);
        }
        if (!empty($request->payment_status)) {
            //$transaction_status =  $request->payment_status;
            //$transaction = $transaction->where('gv_user_payment_sessions.status',$transaction_status);
        }
        if (!empty($request->plate_form)) {
            $plate_form =  $request->plate_form;
            //$transaction = $transaction->where('gv_user_payment_sessions.platform',$plate_form);
        }
        if (!empty($request->parking_type)) {
            $parking_type = $request->parking_type;
            $transaction = $transaction->where('gv_shopcart_transaction.ticket_type', $parking_type);
        }
        if (!empty($request->start_date) && !empty($request->end_date)) {
            $start_date = date('Y-m-d', strtotime($request->start_date));
            $end_date = date('Y-m-d', strtotime($request->end_date));
            $transaction =  $transaction->whereDate('gv_shopcart_transaction.created_date', '>=', $start_date)->whereDate('gv_shopcart_transaction.created_date', '<=', $end_date);
        } else {
            if (
                empty($request->transaction_status) && empty($request->salecode)
                && empty($request->order_ref_no) && empty($request->firstname)
                && empty($request->lastname) && empty($request->email)
                && empty($request->mobile)
            ) {
                $start_date = date('Y-m-d');
                $transaction =  $transaction->whereDate('gv_shopcart_transaction.created_date', '>=', $start_date);
            }
        }

        if (!empty($request->salecode)) {
            $search_val = $request->salecode;
            $transaction = $transaction->where('gv_shopcart_transaction.salecode', 'LIKE', '%' . $search_val . '%');
        }
        if (!empty($request->order_ref_no)) {
            $search_val = $request->order_ref_no;
            $transaction = $transaction->where('gv_shopcart_transaction.order_id', 'LIKE', '%' . $search_val . '%');
        }
        if (!empty($request->firstname)) {
            $search_val = $request->firstname;
            $transaction = $transaction->where('gv_shopcart_transaction.firstname', 'LIKE', '%' . $search_val . '%');
        }
        if (!empty($request->lastname)) {
            $search_val = $request->lastname;
            $transaction = $transaction->where('gv_shopcart_transaction.lastname', 'LIKE', '%' . $search_val . '%');
        }
        if (!empty($request->email)) {
            $search_val = $request->email;
            $transaction = $transaction->where('gv_shopcart_transaction.email', 'LIKE', '%' . $search_val . '%');
        }
        if (!empty($request->mobile)) {
            $search_val = $request->mobile;
            $transaction = $transaction->where('gv_shopcart_transaction.mobile', 'LIKE', '%' . $search_val . '%');
        }
        //DB::enableQueryLog();
        $transaction =  $transaction->groupBy('gv_shopcart_transaction.shopcartid')->orderBy('gv_shopcart_transaction.created_date', 'desc');

        return Datatables::of($transaction)
            ->rawColumns(['transaction_status', 'payment_status', 'action'])
            ->editColumn('action', function ($transaction) {
                $b = '';
                $b .= '<a href="' . URL::route('admin.shop-cart-transaction.show', $transaction->id) . '" class="btn btn-outline-primary "><i class="fa fa-eye"></i></a>';

                /*if($transaction->transaction_status){
                  $b .= '<a title="Send Mail" href="javascript:void(0)" style="width:95px;"  class="open-popup-content btn btn-primary btn-xs " data-popup-action-type = "send-mail" data-popup-action-id = "' . $transaction->id . '"><i class="fa fa-envelope-o"></i> Send Email</a>';
              }else{
                  if ($transaction->status == 1 && $transaction->payment_status == 4) {
                      $b .= ' <a title="Generate Ticket" href="javascript:void(0)" style="width:95px;"  class="open-popup-content btn btn-success btn-xs " data-popup-action-type = "generate-ticket" data-popup-action-id = "' . $transaction->id . '"><i class="fa fa-ticket"></i> Generate Ticket</a>';
                  }else{
                      $b .= ' <a disabled title="Generate Ticket" href="javascript:void(0)" style="width:95px;"  class="btn btn-success btn-xs"><i class="fa fa-ticket"></i> Generate Ticket</a>';
                  }
              }*/

                return $b;
            })
            ->editColumn('created_date', function ($transaction) {
                return (!empty($transaction->created_date)) ? date('F d, Y h:ia', strtotime($transaction->created_date)) : '';
            })->editColumn('transaction_status', function ($transaction) {
                if ($transaction->transaction_status) {
                    return '<span class="label label-success">Complete</span>';
                } else {
                    if ($transaction->status == 1) {
                        return '<span class="label label-warning">Active</span>';
                    } else {
                        return '<span class="label label-danger">Inactive</span>';
                    }
                }
            })
            /*->editColumn('payment_status', function ($transaction) {
              $label = '';
              if($transaction->payment_status == 1){
                  $label .= '<span class="label label-danger">Created</span>';
              }elseif($transaction->payment_status == 2){
                  $label .= '<span class="label label-info">Initiated</span>';
              }elseif($transaction->payment_status == 3){
                  $label .= '<span class="label label-warning">Authenticate</span>';
              }elseif($transaction->payment_status == 4) {
                  $label .= '<span class="label label-success">Paid</span>';
              }else{
                  $label .= '<span class="label label-default">Not Paid</span>';
              }
              $label .= '&nbsp;&nbsp; <a href="javascript:void(0)" data-popup-action-type = "check-payment-status" data-popup-action-id = "' . $transaction->id . '" style="cursor: pointer;" class="open-popup-content" title="Check Payment Status"><i class="fa fa-reply" aria-hidden="true"></i></a>';
              return $label;
          })*/
            ->make(true);

        //dd(DB::getQueryLog());
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'action_type' => 'required',
                'cart_transaction_id' => 'required',
            ], [
                'action_type.required' => 'Action type not found!',
                'cart_transaction_id.required' => 'Transaction id not found!',
            ]);
            if ($validator->fails()) {
                return response()->json(['status' => 0, 'data' => "", 'title' => "Error", 'message' => implode(" ", $validator->errors()->all())], 404);
            }
            $auth_detail =  Auth::guard('admin')->user();
            $transaction_detail = ShoppingCartTransaction::where('id', $request->cart_transaction_id)->first();

            if ($request->action_type == "send-mail") {

                if ($transaction_detail->getCheckoutDetail) {
                    //$url = env('E_API_URL').'/resendtransemail/'.$transaction_detail->getCheckoutDetail->saleid.'/'.$request->sEmail;
                    $url = env('E_API_URL') . '/resendtransemail';
                    $post_data = [
                        'saleId' => $transaction_detail->getCheckoutDetail->saleid,
                        'email' => $request->sEmail
                    ];

                    $response = $this->curlPost($url, $post_data);
                    $array_data = array("audit_type" => $request->action_type, 'table_id' => $transaction_detail->id, 'table_name' => 'gv_shopcart_transaction', "admin_id" => $auth_detail->id, "admin_type" => $auth_detail->type, "event" => "Send mail", "old_values" => "", "new_values" => "", "url" => "");
                    manageAuditLogs($array_data);
                    if ($response) {

                        //echo json_encode($response);
                        //exit;
                        if ($response->statusCode == 1000) {
                            return response()->json(['status' => 1, 'data' => $response, 'title' => "Send Mail", 'message' => "Send mail successfully"]);
                        } else {
                            return response()->json(['status' => 0, 'data' => "", 'title' => "Error", 'message' => $response->message]);
                        }
                    } else {
                        return response()->json(['status' => 0, 'data' => "", 'title' => "Error", 'message' => "Something went wrong"]);
                    }
                } else {
                    return response()->json(['status' => 0, 'data' => "", 'title' => "Error", 'message' => "Record not found"]);
                }
            } elseif ($request->action_type == "generate-ticket") {
                if ($transaction_detail && !empty($transaction_detail->shopcartid)) {
                    $url = env('E_API_URL') . '/checkout';
                    $post_data = [
                        'shopcartId' => $transaction_detail->shopcartid
                    ];

                    $response = $this->curlPost($url, $post_data);

                    $array_data = array("audit_type" => $request->action_type, 'table_id' => $transaction_detail->id, 'table_name' => 'gv_shopcart_transaction', "admin_id" => $auth_detail->id, "admin_type" => $auth_detail->type, "event" => "generate ticket", "old_values" => "", "new_values" => "", "url" => "");
                    manageAuditLogs($array_data);


                    if ($response) {
                        if ($response->statusCode == 2000) {
                            return response()->json(['status' => 0, 'data' => "", 'title' => "Error", 'message' => $response->message . ' ' . $response->expired]);
                        } else {
                            return response()->json(['status' => 1, 'data' => '', 'title' => "Generate Ticket", 'message' => "Generate ticket successfully"]);
                        }
                    } else {
                        return response()->json(['status' => 0, 'data' => "", 'title' => "Error", 'message' => "Something went wrong"]);
                    }
                } else {
                    return response()->json(['status' => 0, 'data' => "", 'title' => "Error", 'message' => "Shop cart id not found"]);
                }
            } else {

                if ($transaction_detail->getSessionDetail && $transaction_detail->getSessionDetail->getUserPayment) {
                    $url = env('E_API_URL') . '/order';
                    $post_data = [
                        'orderId' => $transaction_detail->getSessionDetail->getUserPayment->order_id
                    ];
                    $response = $this->curlPost($url, $post_data);
                    $array_data = array("audit_type" => $request->action_type, 'table_id' => $transaction_detail->id, 'table_name' => 'gv_shopcart_transaction', "admin_id" => $auth_detail->id, "admin_type" => $auth_detail->type, "event" => "check payment status", "old_values" => "", "new_values" => "", "url" => "");
                    manageAuditLogs($array_data);
                    if ($response->data && $response->data->result == "SUCCESS") {
                        $totalAuthorizedAmount =  $response->data->totalAuthorizedAmount;
                        $totalCapturedAmount =  $response->data->totalCapturedAmount;
                        $totalRefundedAmount =  $response->data->totalRefundedAmount;
                        $status =  $response->data->status;

                        $post_data = [
                            'totalAuthorizedAmount' => $totalAuthorizedAmount,
                            'totalCapturedAmount' => $totalCapturedAmount,
                            'totalRefundedAmount' => $totalRefundedAmount,
                            'json_response' => $response,
                            'status' => $status
                        ];
                        return response()->json(['status' => 1, 'data' => $post_data, 'title' => "Check Payment Status", 'message' => "Payment detail retrieved successfully."]);
                    } else {
                        return response()->json(['status' => 0, 'data' => "", 'title' => "Error", 'message' => "Something went wrong"]);
                    }
                } else {
                    return response()->json(['status' => 0, 'data' => "", 'title' => "Error", 'message' => "Order detail is not found"]);
                }
            }
        } catch (\Exception $e) {
            Log::error('load_popup_content', ['Exception' => $e->getMessage()]);
            return response()->json(['status' => 0, 'data' => "", 'title' => "Error", 'message' => "Something went wrong"]);
        }
    }
    public function curlPost($url, $data)
    {
        $postdata = json_encode($data);
        $uniqueKey = env('DRUPAL_UNIQUE_KEY', '7f3^`H_Gx~7neyl73TZykP_sY`J)Jp');
        $header = array('Content-Type: application/json', 'GV-Language: en', 'GV-Unique-Key: ' . $uniqueKey);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $result = curl_exec($ch);
        curl_close($ch);
        $obj = json_decode($result);
        return $obj;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = array();
        $result['id'] = $id;
        $result['cart_detail'] = ShoppingCartTransaction::where('id', $id)->first();
        $auth_detail =  Auth::guard('admin')->user();
        $exist_log = AuditLog::where('table_id', $id)->where('table_name', 'gv_shopcart_transaction')->whereDate('created_at', date('Y-m-d'))->where('audit_type', 'view-transaction')->first();
        if (!$exist_log) {
            $array_data = array("audit_type" => "view-transaction", 'table_id' => $id, 'table_name' => 'gv_shopcart_transaction', "admin_id" => $auth_detail->id, "admin_type" => $auth_detail->type, "event" => "view transaction detail", "old_values" => "", "new_values" => "", "url" => "");
            manageAuditLogs($array_data);
        }
        return view('admin.cart_history_transaction.show')->with(['result' => $result]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
