<?php

namespace App\Http\Controllers\Admin;

use App\AuditLog;
use App\Http\Controllers\Controller;
use App\ShoppingCartTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Datatables;
use App\Admin;
use Auth;
use URL;
//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
//Enables us to output flash messaging
use Session;
use Exception;
use Image;
use Illuminate\Support\Facades\Validator;
class AdminCommonController extends Controller {
    public function __construct() {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('log_read', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['getLog']]);

    }

    public function load_popup_content(Request $request)    {
        try {
            $validator = Validator::make($request->all(), [
                'action_type' => 'required',
            ], [
                'action_type.required' => 'Action type not found!',
            ]);
            if ($validator->fails()) {
                return response()->json(['data' => "", 'message' => implode(" ", $validator->errors()->all())], 404);
            }
            $result = array();
            $result['action_type'] = $request->action_type;
            $result['action_id'] = $request->action_id;
            $result['extra_action'] = $request->extra_action;

            if($request->action_type == "send-mail" || $request->action_type == "generate-ticket" || $request->action_type == "check-payment-status"){
                $result['cart_detail'] = ShoppingCartTransaction::where('id',$result['action_id'])->first();
            }
            $view = view('admin.common.common_popup_content')->with(['result' => $result])->render();
            return response()->json(['data' => $view, 'message' => ""], 200);
        } catch (Exception $e) {
            Log::error('load_popup_content', ['Exception' => $e->getMessage()]);
            return response()->json(['data' => $e->getMessage(), 'message' => 'Something went wrong'], 404);
        }
    }
    public function show_audit_logs(Request $request) {
       
        $audit = AuditLog::where('table_id',$request->transaction_id)->whereIn('audit_type',['generate-ticket','check-payment-status','view-transaction','send-mail']);
        if($request->audit_type){
            $audit = $audit->where('audit_type',$request->audit_type);
        }
        $audit = $audit->orderBy('id','desc');
        return Datatables::of($audit)
            ->editColumn('admin_id', function ($audit) {
               if($audit->getAdminDetail){
                 return  $audit->getAdminDetail->email;
               }else{
                   return "";
               }

            })
            ->editColumn('created_at', function ($audit) {
                if(!empty($audit->created_at)){
                    return date('F d, Y h:ia',strtotime($audit->created_at));
                }else {
                    return "";

                }

            })->make(true);
    }

    public function getLog(){
        return view('admin.logs.index');
    }

    public function getAllLogs(Request $request) {
       
        $audit = AuditLog::whereIn('audit_type',['create-car','edit-car','edit-user']);
        if($request->audit_type){
            $audit = $audit->where('audit_type',$request->audit_type);
        }
        $audit = $audit->orderBy('id','desc');

        return Datatables::of($audit)
            ->editColumn('admin_id', function ($audit) {
               if($audit->getAdminDetail){
                 return  $audit->getAdminDetail->email;
               }else{
                   return "";
               }

            })
             ->editColumn('old_values', function ($audit) {
                return view('admin.logs.data',['data' => $audit->old_values]);
            })
            ->editColumn('new_values', function ($audit) {
                return view('admin.logs.data',['data' => $audit->new_values]);
            })
            ->editColumn('created_at', function ($audit) {
                if(!empty($audit->created_at)){
                    return date('F d, Y h:ia',strtotime($audit->created_at));
                }else {
                    return "";

                }

            })->make(true);
    }

}
