<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;

use App\Season;

use Auth;
use URL;
use App\Pack;
use App\Traits\AccountActivationTraits;
use App\Traits\GvpTraits;
use App\Traits\DrupalTraits;
use App\Traits\SalesForceTraits;

use Excel;
use App\Exports\PacksExport;
use Zip;
use PDF;

class SalesForceLogController extends Controller
{
    use SalesForceTraits;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.salesforce.index');
    }

    public function datatable(Request $request) {
        $currentUser = Auth::guard('admin')->user();
        $isSuperAdmin = $currentUser->hasRole('super-admin', 'admin');

        $admins = \App\SalesforceLog::select('*')->orderby('created_at','desc')
                ;
        
        return Datatables::of($admins)
                        ->rawColumns(['actions'])
                        ->editColumn('created_at', function ($user) {
                            return $user->created_at->format('F d, Y h:ia');
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pending(Request $request){
        $limit = ($request->limit > 0) ? $request->limit : 10;
        $date =  ($request->date != "") ? date("Y-m-d H:i:s",strtotime($request->date)) : "2024-09-01 00:00:00" ;
        $pending_query = \App\UserPack::whereNull('sales_force_ref_id')->where("created_at",">=",$date)->whereNotNull("pack_id")->select("id");
        if(!empty($request->id)){
            $pending_query->where("id","=",$request->id);
        }
        $pending_count = $pending_query->count();

        $pending_list = $pending_query->orderby('created_at',"ASC")->limit($limit)->get();
        echo "Started : ".date("Y-m-d H:i:s")."</br>";
        echo "<b>Total unsynced records : $pending_count</b></br></br>";
        $success_count = $failed_count = 0;
        //dd($pending_count,$pending_list);
        if(!empty($pending_list)){
            foreach($pending_list as $pend){
                $user = \App\User::find($pend->user_id);
                if(!empty($pend) && !empty($pend->pack_id)){
                    $pack = \App\Pack::find($pend->pack_id);
                    if(!empty($pack)){
                        $agree_offers = ($user->recieve_updates == 1)  ? true : false;     
                        $source = (in_array(strtolower($user->device_type),['ios','android'])) ? 'web' : 'mobile';               
                        $sales_force_ref_id = $this->vipSubmissionToSalesForce($user,$pack,$agree_offers,$source,'vip_activation',$user->device_type);
                        if(!empty($sales_force_ref_id )){
                            $pend->sales_force_ref_id = (!empty($sales_force_ref_id)) ? $sales_force_ref_id : NULL;
                            $pend->save();

                            print '<div class="alert alert-success">Salesforce update success : '.$user->email." ( Pack: $pack->pack_number)  ".$sales_force_ref_id. '</div>' ;
                            $success_count++;
                        }else{
                            $failed_count++;
                        }
                    }
                }
            }
        }

        echo "</br></br><b>Synced records : $success_count</b></br>";
        echo "<b>Failed records : $failed_count</b></br>";
        echo "Finished : ".date("Y-m-d H:i:s")."</br>";
    }


    function vipResyncSubmissionToSalesForce($user,$pack,$agree_offers=true,$source='web',$log_type="failed_batch_update"){
        
        
        $requestData = [
            'grant_type' => 'password',
            'client_id' => env('SALES_FORCE_CLIENT_ID'),
            'client_secret' => env('SALES_FORCE_CLIENT_SECRET'),
            'username' => env('SALES_FORCE_USERNAME'),
            'password' => env('SALES_FORCE_PASSWORD'),
          ];
          $query_string = http_build_query($requestData);
          $url = env('SALES_FORCE_AUTH_URL').$query_string;
          $response = curlRequestPost($url,[],[]);

         
        if (isset($response["access_token"])) {
            $accessToken = $response['access_token'];       
          
            $locale = app()->getLocale();
            $country = \App\Country::where('id',$user->nationality_id)->first();
            $nationality = (isset($country->name_en)) ? $country->name_en : '';
            $residence = \App\Country::where('id',$user->residence_id)->first();
            $countryOfResidence = (isset($residence->name_en)) ?  $residence->name_en : '';
            $remirate = \App\Emirate::where('id',$user->emirate_id)->first();
            $emirateOfResidence = (isset($remirate->name_en)) ? $remirate->name_en : NULL;

            $userCars = \App\UserCar::where('user_id',$user->id)->get();
            $userCarArray = [];
            if(!empty($userCars)){
                foreach($userCars as $car){
                    $carCountry = \App\CarCountry::find($car->country_id);
                    $country_code = (!empty($carCountry) && isset($carCountry->name_en)) ? $carCountry->name_en : '';

                    $emirate_code = '';
                    if(!empty($car->emirate_id)){
                        $emirate = \App\Emirate::find($car->emirate_id);
                        $emirate_code = (!empty($emirate) && isset($emirate->name_en)) ? $emirate->name_en : '';
                    }

                    $userCarArray[] = [
                        'carPlateId' => $car->id,
                        'carType' => ($car->type == 2) ? 'Sedan' : 'SUV',
                        'carPlateCountry' => $country_code,
                        'carPlateEmirate' => $emirate_code,
                        'carPlateNumber' => $car->plate_number,
                        'carPlateCode' => $car->plate_prefix,
                    ];
                }
            }

            $mobile_number = $user->mobile_dial_code.$user->mobile;
            $param = [                
                    'emailAddress' => $user->email,
                    'firstName' => $user->first_name,
                    'lastName' => $user->last_name,
                    'DateofBirth1'=> ($user->dob != NULL)  ? date("m/d/Y",strtotime($user->dob)) : NULL ,
                    'Country' => $countryOfResidence ,
                    'gender' => $user->gender,  
                    'mobilePhone' => $mobile_number,
                    'nationality1' => $nationality  ,
                    'packnumber' => (string) $pack->pack_number,
                    'PersonalisedURL' => route('my.activated.pack', [$locale, $user->id]),
                    'language1' => ($locale == 'en') ? "English" : "Arabic",
                    'emirateofResidence1' => $emirateOfResidence ,
                    'packclassification' => trans("web.sales_pack_categories.{$pack->category}"),
                    'source' => $source ,
                    'Subscribe' => $agree_offers,
                    'carDetails' => $userCarArray,
                    
            ];

            $url = env('SALES_FORCE_VIP_BASE_URL').'createVIPActivation';
            $headers = array(
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Bearer $accessToken",
            );
            
            $response = curlRequestPost($url,$param,$headers);

            $sales_force_log = new \App\SalesforceLog;
            $sales_force_log->type = $log_type;
            $sales_force_log->refenrence_id = $pack->id;
            $sales_force_log->response = json_encode($response);
            $sales_force_log->sale_request = json_encode($param);
            $sales_force_log->user_id = $user->id;
            $sales_force_log->save();

            if (isset($response["code"]) && isset($response["id"]) && $response["code"] == 200 && !empty($response["id"])) {
                return $response["id"];
            }
            print '<div class="alert alert-danger">Salesforce update failed - update error : <pre>';
            //print_r($param);
            print_r($response);
            print "</pre></div>"; 
            return false;
        }else{
            print '<div class="alert alert-danger">Salesforce update failed - authentication : <pre>';
            print_r($response);
            print "</pre></div>"; 
            return false;
        }
     }
}
