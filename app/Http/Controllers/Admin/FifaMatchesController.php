<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Auth;
use App\FifaMatch;
use App\Season;
use DB;
use Importer;
use Webpatser\Uuid\Uuid;
use Validator;
use Excel;
use App\Exports\FifaMatchExport;

class FifaMatchesController extends Controller {

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Fifa_read', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['index']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Fifa_create', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['create', 'store']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Fifa_delete', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['destroy']]);
    }

    public function index() {
        return view('admin.fifa.index');
    }

    public function datatable(Request $request) {
        $currentUser = Auth::guard('admin')->user();
        $isSuperAdmin = $currentUser->hasRole('super-admin', 'admin');
        $program = FifaMatch::select('*')->orderBy('match_date');
        if ($request->date_start != '') {
            $program->where('match_date', '>=', date('Y-m-d', strtotime($request->date_start)));
        }
        if ($request->date_end != '') {
            $program->where('match_date', '<=', date('Y-m-d', strtotime($request->date_end)));
        }
        return Datatables::of($program)
                        ->rawColumns(['actions'])
                        ->editColumn('match_date', function ($program) {
                            return date('d M Y', strtotime($program->match_date));
                        })
                        ->editColumn('match_time', function ($program) {
                            return date('h:i a', strtotime($program->match_time));
                        })
                        ->editColumn('actions', function ($program) use ($currentUser, $isSuperAdmin) {
                            $b = '';
                            if ($isSuperAdmin || $currentUser->hasPermissionTo('Fifa_delete', 'admin')) {
                                $b .= ' <a href="' . route('admin.fifa-matches.destroy', $program->id) . '" class="btn btn-danger btn-xs destroy"><i class="fa fa-trash"></i></a>';
                            }

                            return $b;
                        })
                        ->make(true);
    }

    public function create() {
        $season = \App\Season::where('status', 1)->first();
        if (empty($season)) {
            alert()->error('No active season', 'Warning!');
            return redirect()->route('admin.fifa-matches.index');
        }
        return view('admin.fifa.create', compact('season'));
    }

    public function store(Request $request) {
        $this->validate($request, [
            'import_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('import_file')->getRealPath();

        $import = new \App\Imports\FifaMatchImport($request->season_id);
        Excel::import($import, $request->import_file);
        alert()->success($import->getRowCount() . ' records has been processed successfully.')->persistent("Close");
        return redirect()->route('admin.fifa-matches.index');
    }

    public function export(Request $request) {
        $rules = [
            'startDate' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            alert()->error('Start Date is required', 'Required');
            return redirect()->route('admin.fifa-matches.index');
        }

        $season = \App\Season::where('status', 1)->first();

        if (empty($season)) {
            alert()->error('No active season', 'Warning!');
            return redirect()->route('admin.fifa-matches.index');
        }
        ini_set('max_execution_time', '0');
        $input = ['seasonId' => $season->id, 'startDate' => $request->startDate, 'endDate' => $request->endDate];
        return Excel::download(new FifaMatchExport($input), 'fifa-matches-with-id.xlsx');
    }

    public function destroy($id) {
        FifaMatch::destroy($id);
    }

    /*
     * old
     * $excel = Importer::make('Excel');
      $excel->hasHeader(true);
      $excel->load($path);
      $collection = $excel->getCollection();
      $seasonId = $request->season_id;
      $up = 0;
      $in = 0;

      set_time_limit(0);

      foreach ($collection->toArray() as $key => $value) {
      if (isset($value['id']) && !empty($value['id'])) {
      $exist = DB::table('fifa_matches')->where('id', trim($value['id']))->first();
      if ($exist) {
      DB::table('fifa_matches')->where('id', trim($value['id']))
      ->update(
      [
      'titles' => json_encode(['en' => $value['Title English'], 'ar' => $value['Title Arabic']]),
      'match_date' => !empty($value['Date']) ? (is_object($value['Date'])) ? $value['Date']->format('Y-m-d') : date('Y-m-d', strtotime($value['Date'])) : null,
      'match_time' => !empty($value['Time']) ? (is_object($value['Time'])) ? $value['Time']->format('H:i:00') : date('H:i:00', strtotime($value['Time'])) : null,
      'performance_id' => $value['Performance ID']
      ]
      );
      $up++;
      }
      } else {
      $insert_data = array(
      'id' => Uuid::generate()->string,
      'season_id' => $seasonId,
      'titles' => json_encode(['en' => $value['Title English'], 'ar' => $value['Title Arabic']]),
      'match_date' => !empty($value['Date']) ? (is_object($value['Date'])) ? $value['Date']->format('Y-m-d') : date('Y-m-d', strtotime($value['Date'])) : null,
      'match_time' => !empty($value['Time']) ? (is_object($value['Time'])) ? $value['Time']->format('H:i:00') : date('H:i:00', strtotime($value['Time'])) : null,
      'performance_id' => $value['Performance ID']
      );
      DB::table('fifa_matches')->insert($insert_data);
      $in++;
      }
      }
      $msg = '';
      if ($in == 0 && $up == 0) {
      $msg = 'No recored found to import';
      } else {
      if ($in > 0) {
      $msg .= $in . ' Matches inserted.';
      }
      if ($up > 0) {
      $msg .= $up . ' Matches Updated.';
      }
      }
      alert()->success($msg)->persistent("Close");
      return redirect()->route('admin.fifa-matches.index');
     */
}
