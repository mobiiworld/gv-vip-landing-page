<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Auth;
use App\BookSlot;
use App\Program;
use DB;
use Importer;
use Validator;
use Excel;
use App\Exports\MajlisCollectionExport;

class MajlisBookingController extends Controller {

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Ramadan Majlis_find bookings', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['findBookings']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Ramadan Majlis_onsite collection', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['onsiteIndex', 'onsiteShow']]);
    }

    public function findBookings() {
        $program = Program::where('status', 1)
                ->orderBy('created_at', 'DESC')
                ->first();
        return view('admin.program.bookings', compact('program'));
    }

    public function datatable(Request $request) {
        $program = BookSlot::select('*')
                ->where('program_id', $request->programId)
                ->whereIn('status', ['order_confirmed', 'waiting_payment_timeout'])
                ->orderBy('created_at', 'DESC');
        if ($request->name != '') {
            $program->where('first_name', 'like', '%' . $request->name . '%');
        }
        if ($request->email != '') {
            $program->where('email', $request->email);
        }
        if ($request->saleCode != '') {
            $program->where('sale_code', $request->saleCode);
        }
        return Datatables::of($program)
                        ->editColumn('is_vip', function ($program) {
                            return (empty($program->is_vip) ? 'No' : 'Yes');
                        })
                        ->editColumn('program_date', function ($program) {
                            return date('d M, Y', strtotime($program->program_date));
                        })
                        ->editColumn('status', function ($program) {
                            $stat = 'Confirmed';
                            switch ($program->status) {
                                case 'waiting_payment_timeout':
                                    $stat = 'Timeout at payment';
                                    break;
                                case 'order_confirmed':
                                    if (empty($program->program_slot_id)) {
                                        $released = \App\ReleasedBookingSlot::where('booking_id', $program->id)
                                                ->with('admin:id,name,email')
                                                ->first();
                                        if ($released) {
                                            $stat = 'Slots released by ' . $released->admin->name . '|' . $released->admin->email;
                                        } else {
                                            $stat = 'Something Wrong.';
                                        }
                                    }
                                    break;
                            }
                            return $stat;
                        })
                        ->editColumn('created_at', function ($program) {
                            return date('d M, Y', strtotime($program->created_at));
                        })
                        ->make(true);
    }

    public function onsiteIndex(Request $request) {
        if ($request->ajax()) {
            $bookings = \App\BookSlot::select('book_slots.id', 'book_slots.admin_id', 'admins.name', 'admins.email')
                    ->selectRaw('SUM(CASE WHEN payment_mode=1 THEN amount ELSE 0 END) AS totCash,SUM(CASE WHEN payment_mode=2 THEN amount ELSE 0 END) AS totCard')
                    ->join('admins', 'admins.id', 'book_slots.admin_id')
                    ->where('book_slots.program_id', $request->pid)
                    ->whereNotNull('book_slots.admin_id')
                    //->where('book_slots.operational_date', $dateTocheck)
                    ->groupBy('book_slots.admin_id');
            if ($request->opFDate != '') {
                $bookings->where('book_slots.operational_date', '>=', date("Y-m-d", strtotime($request->opFDate)));
            }
            if ($request->opTDate != '') {
                $bookings->where('book_slots.operational_date', '<=', date("Y-m-d", strtotime($request->opTDate)));
            }
            return Datatables::of($bookings)->make(true);
        }
        $program = Program::where('status', 1)->first();
        return view('admin.program.reports.onsite-index', compact('program'));
    }

    public function onsiteShow(Request $request) {
        //$dateTocheck = date("Y-m-d", strtotime($request->opDate));
        $bookings = \App\BookSlot::select('id', 'sale_code', 'amount', \DB::raw("CASE WHEN payment_mode=1 THEN 'CASH' ELSE 'CARD' END AS pmode"), 'created_at')
                ->where('book_slots.program_id', $request->pid)
                ->where('book_slots.admin_id', $request->adminId)
                //->where('book_slots.operational_date', $dateTocheck)
                ->orderBy('created_at');
        if ($request->opFDate != '') {
            $bookings->where('book_slots.operational_date', '>=', date("Y-m-d", strtotime($request->opFDate)));
        }
        if ($request->opTDate != '') {
            $bookings->where('book_slots.operational_date', '<=', date("Y-m-d", strtotime($request->opTDate)));
        }
        $bookings = $bookings->get();
        return view('admin.program.reports.onsite-splitup', compact('bookings'));
    }

    public function export(Request $request) {
        $rules = [
            'pid' => 'required',
            'startDate' => 'required',
            'endDate' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            alert()->error('Please fill all fields', 'Required');
            return redirect()->route('admin.program.onsite.collections');
        }

        ini_set('max_execution_time', '0');
        $input = ['programId' => $request->pid, 'startDate' => $request->startDate, 'endDate' => $request->endDate];
        return Excel::download(new MajlisCollectionExport($input), 'onsite-collections.xlsx');
    }

}
