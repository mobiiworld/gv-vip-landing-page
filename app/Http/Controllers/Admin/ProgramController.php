<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Traits\ImageTraits;
use Auth;
use URL;
use App\Program;

class ProgramController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use ImageTraits;

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Ramadan Majlis_program read', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['index']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Ramadan Majlis_program create', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['create', 'store']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Ramadan Majlis_program update', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['edit', 'update']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Ramadan Majlis_booking', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['getSlots']]);
    }

    public function index() {
        return view('admin.program.index');
    }

    public function datatable(Request $request) {
        $currentUser = Auth::guard('admin')->user();
        $isSuperAdmin = $currentUser->hasRole('super-admin', 'admin');
        $program = Program::select('*');
        if ($request->status != '') {
            $program->where('status', $request->status);
        }
        return Datatables::of($program)
                        ->rawColumns(['actions', 'status'])
                        ->editColumn('actions', function ($program) use ($currentUser, $isSuperAdmin) {
                            $b = ' <a class="btn btn-primary btn-sm " href="' . route('admin.programs.edit', $program->id) . '">Edit</a>';
                            $b .= ' <a class="btn btn-info btn-sm " href="' . route('admin.programs.slots.index', $program->id) . '">Program Slots</a>';
                            if ($program->status == 1) {
                                if ($isSuperAdmin || $currentUser->hasPermissionTo('Ramadan Majlis_booking', 'admin')) {
                                    $b .= ' <a class="btn btn-warning btn-sm " href="' . route('admin.programs.slots.book', $program->id) . '">Book Slots</a>';
                                }
                            }
                            return $b;
                        })
                        ->editColumn('status', function ($program) use ($currentUser, $isSuperAdmin) {
                            if ($isSuperAdmin || $currentUser->hasPermissionTo('Ramadan Majlis_program update', 'admin')) {
                                $checked = ($program->status == 1) ? 'checked' : '';
                                return '<input type="checkbox" name="status" id="status_' . $program->id . '"  data-on-text="Enabled" data-off-text="Disabled" value="1" ' . $checked . ' class="bsSwitch" autocomplete="off" data-id="' . $program->id . '">';
                            } else {
                                return ($program->status == 1) ? '<label class="label label-success">Enabled</label>' : '<label class="label label-danger">Disabled</label>';
                            }
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.program.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'hours_before_booking' => 'required',
            'home_display_hours' => 'required',
            'program_uid' => 'required|unique:programs',
            'vgs_pcodes.standard.2_seater.*' => 'required',
            'vgs_pcodes.premium.2_seater.amount' => 'required',
            'vgs_pcodes.premium.2_seater.nonsmoking' => 'required',
            'vgs_pcodes.premium.2_seater.smoking' => 'required',
            'vgs_pcodes.standard.4_seater.*' => 'required',
            'vgs_pcodes.premium.4_seater.amount' => 'required',
            'vgs_pcodes.premium.4_seater.nonsmoking' => 'required',
            'vgs_pcodes.premium.4_seater.smoking' => 'required',
            'vgs_pcodes.standard.6_seater.*' => 'required',
            'vgs_pcodes.premium.6_seater.amount' => 'required',
            'vgs_pcodes.premium.6_seater.nonsmoking' => 'required',
            'vgs_pcodes.premium.6_seater.smoking' => 'required',
                ], [
            'vgs_pcodes.standard.2_seater.*.required' => 'This is required',
            'vgs_pcodes.premium.2_seater.*.required' => 'This is required',
            'vgs_pcodes.standard.4_seater.*.required' => 'This is required',
            'vgs_pcodes.premium.4_seater.*.required' => 'This is required',
            'vgs_pcodes.standard.6_seater.*.required' => 'This is required',
            'vgs_pcodes.premium.6_seater.*.required' => 'This is required',
        ]);
        $program = new Program;
        $program->name = $request->name;
        $program->program_uid = $request->program_uid;
        $program->description = $request->description;
        $program->hours_before_booking = $request->hours_before_booking;
        $program->home_display_hours = $request->home_display_hours;
        $program->has_priority = ($request->has_priority) ? 1 : 0;
        $program->suggesetd_table_count = $request->suggesetd_table_count;
        $program->max_weight_count = $request->max_weight_count;
        $program->vgs_pcodes = $request->vgs_pcodes;

        $program->save();

        alert()->success('Program successfully added.', 'Added');
        return redirect()->route('admin.programs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $program = Program::where('id', $id)->first();
        return view('admin.program.edit', compact('program'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
            'hours_before_booking' => 'required',
            'home_display_hours' => 'required',
            'program_uid' => 'required|unique:programs,program_uid,' . $id,
            'vgs_pcodes.standard.2_seater.*' => 'required',
            'vgs_pcodes.premium.2_seater.amount' => 'required',
            'vgs_pcodes.premium.2_seater.nonsmoking' => 'required',
            'vgs_pcodes.premium.2_seater.smoking' => 'required',
            'vgs_pcodes.standard.4_seater.*' => 'required',
            'vgs_pcodes.premium.4_seater.amount' => 'required',
            'vgs_pcodes.premium.4_seater.nonsmoking' => 'required',
            'vgs_pcodes.premium.4_seater.smoking' => 'required',
            'vgs_pcodes.standard.6_seater.*' => 'required',
            'vgs_pcodes.premium.6_seater.amount' => 'required',
            'vgs_pcodes.premium.6_seater.nonsmoking' => 'required',
            'vgs_pcodes.premium.6_seater.smoking' => 'required',
                ], [
            'vgs_pcodes.standard.2_seater.*.required' => 'This is required',
            'vgs_pcodes.premium.2_seater.*.required' => 'This is required',
            'vgs_pcodes.standard.4_seater.*.required' => 'This is required',
            'vgs_pcodes.premium.4_seater.*.required' => 'This is required',
            'vgs_pcodes.standard.6_seater.*.required' => 'This is required',
            'vgs_pcodes.premium.6_seater.*.required' => 'This is required',
        ]);
        $program = Program::find($id);
        $program->name = $request->name;
        $program->program_uid = $request->program_uid;
        $program->description = $request->description;
        $program->hours_before_booking = $request->hours_before_booking;
        $program->home_display_hours = $request->home_display_hours;
        $program->has_priority = ($request->has_priority) ? 1 : 0;
        $program->suggesetd_table_count = $request->suggesetd_table_count;
        $program->max_weight_count = $request->max_weight_count;
        $program->vgs_pcodes = $request->vgs_pcodes;

        $program->save();

        alert()->success('Program successfully updated.', 'Updated');
        return redirect()->route('admin.programs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function changeStatus(Request $request) {
        $program = Program::find($request->id);
        $program->status = ($program->status == 2) ? 1 : 2;
        $program->save();
        return response()->json(["status" => true, "message" => 'Successfully changed']);
    }

    public function getSlots(Request $request, $id) {
        $program = Program::find($id);
        if ($request->ajax()) {
            $dateTocheck = date("Y-m-d", strtotime($request->programDate));
            $programSlot = \App\ProgramSlot::where('program_slots.program_id', $program->id)
                    ->where('program_slots.status', 1)
                    ->where('program_slots.program_date', $dateTocheck)
                    ->join('vip_tables as tb', 'tb.id', '=', 'program_slots.table_id')
                    ->join('slots', 'slots.id', '=', 'program_slots.slot_id')
                    ->orderby('slots.priority', 'asc')
                    ->select('slots.id', 'slots.name as slotName', 'slots.priority', 'program_slots.program_date')
                    ->selectRaw('(CASE WHEN slots.actual_end_time IS NOT NULL THEN (CONCAT(DATE_ADD(program_slots.program_date,INTERVAL 1 DAY)," ",slots.actual_end_time)) ELSE CONCAT(program_slots.program_date," ",slots.end_time) END) AS end_time')
                    ->where('tb.status', 1)
                    ->where('tb.type', $request->table_type)
                    ->where('tb.smoking_status', $request->smokingType)
                    ->where('slots.status', 1);

            if (!empty($request->tableId2) || !empty($request->tableId4) || !empty($request->tableId6)) {
                $tIds = [];
                if (!empty($request->tableId2)) {
                    $tIds[] = $request->tableId2;
                }
                if (!empty($request->tableId4)) {
                    $tIds[] = $request->tableId4;
                }
                if (!empty($request->tableId6)) {
                    $tIds[] = $request->tableId6;
                }
                $programSlot->whereIn('program_slots.table_id', $tIds);
            } else {
                if ($request->noOfTables4 > 0 && $request->noOfTables6 > 0 && $request->noOfTables2 > 0) {
                    $programSlot->whereIn('tb.seat_type', [2, 4, 6]);
                } else if ($request->noOfTables4 > 0 && $request->noOfTables6 > 0) {
                    $programSlot->whereIn('tb.seat_type', [4, 6]);
                } else if ($request->noOfTables2 > 0 && $request->noOfTables4 > 0) {
                    $programSlot->whereIn('tb.seat_type', [2, 4]);
                } else if ($request->noOfTables2 > 0 && $request->noOfTables6 > 0) {
                    $programSlot->whereIn('tb.seat_type', [2, 6]);
                } else {
                    if ($request->noOfTables4 > 0) {
                        $programSlot->where('tb.seat_type', 4);
                    } elseif ($request->noOfTables6 > 0) {
                        $programSlot->where('tb.seat_type', 6);
                    } elseif ($request->noOfTables2 > 0) {
                        $programSlot->where('tb.seat_type', 2);
                    }
                }
            }

            $programSlot = $programSlot->distinct()->orderby('slots.start_time', 'asc')->get()->toArray();

            if (!empty($programSlot)) {
                foreach ($programSlot as $key => $pgSlot) {
                    if ($dateTocheck <= date('Y-m-d') && $pgSlot['end_time'] <= date('Y-m-d H:i:00')) {
                        unset($programSlot[$key]);
                    } else {
                        $availableCountQuery = \App\ProgramSlot::where('program_slots.status', 1)
                                ->where('program_slots.program_status', 1)
                                ->where('program_slots.program_date', $dateTocheck)
                                ->join('vip_tables as tb', 'tb.id', '=', 'program_slots.table_id')
                                ->join('slots', 'slots.id', '=', 'program_slots.slot_id')
                                ->whereNull('program_slots.booking_id')
                                ->orderby('slots.priority', 'asc')
                                ->where('tb.status', 1)
                                ->where('slots.id', $pgSlot['id'])
                                ->where('slots.status', 1)
                                ->where('tb.type', $request->table_type)
                                ->where('tb.smoking_status', $request->smokingType);
                        if (!empty($tIds)) {
                            $programSlot[$key]['isAvailable'] = TRUE;
                            $availableCountQuery = $availableCountQuery
                                    ->select('program_slots.slot_id', 'program_slots.table_id')
                                    ->whereIn('program_slots.table_id', $tIds)
                                    ->get();
                            foreach ($tIds as $k => $tId) {
                                if (empty($availableCountQuery->where('slot_id', $pgSlot['id'])->where('table_id', $tId)->first())) {
                                    $programSlot[$key]['isAvailable'] = FALSE;
                                }
                            }
                        } else {
                            $availableCountQuery->selectRaw('COUNT(CASE WHEN (tb.seat_type=4) THEN 1 END) AS table_h4,COUNT(CASE WHEN (tb.seat_type=6) THEN 1 END) AS table_h6,COUNT(CASE WHEN (tb.seat_type=2) THEN 1 END) AS table_h2');
                            $availableCount = $availableCountQuery->first();
                            if (($availableCount->table_h4 >= $request->noOfTables4) && ($availableCount->table_h6 >= $request->noOfTables6) && ($availableCount->table_h2 >= $request->noOfTables2)) {
                                $programSlot[$key]['isAvailable'] = TRUE;
                            } else {
                                $programSlot[$key]['isAvailable'] = FALSE;
                            }
                        }
                    }
                }
            }

            $response['program'] = ['id' => $program->id,
                'amount_2' => $program->vgs_pcodes[$request->table_type]['2_seater']['amount'],
                'amount_4' => $program->vgs_pcodes[$request->table_type]['4_seater']['amount'],
                'amount_6' => $program->vgs_pcodes[$request->table_type]['6_seater']['amount'],
            ];
            $response['slots'] = (count($programSlot) > 0) ? array_values($programSlot) : NULL;

            return view('admin.program.book-form', compact('request', 'response'));
        }

        $tables = \App\VipTable::where('status', 1)
                ->orderby('seat_type', 'asc')
                ->orderby('table_number', 'asc')
                ->where('type', 'standard')
                ->where('smoking_status', 1)
                ->get();

        return view('admin.program.book', compact('program', 'tables'));
    }

    public function cofirmSlotBooking(Request $request) {
        $postData = $request->all();
        $slotNames = explode(",", $request->slotNames);
        if ($slotNames[0] == end($slotNames)) {
            $slotNames = $slotNames[0];
        } else {
            $s1 = explode("-", $slotNames[0]);
            $s2 = explode("-", end($slotNames));
            $slotNames = $s1[0] . '-' . $s2[1];
        }
        $nofSlots = count($request->slots);
        $postData['adminId'] = Auth::guard('admin')->user()->id;
        $program = Program::where('program_uid', $request->programId)->where('status', 1)->first();
        if (empty($program)) {
            return response()->json(["status" => false, "message" => 'Program not found.']);
        }

        if ((!empty($postData['tableId2']) || !empty($postData['tableId4']) || !empty($postData['tableId6']))) {
            $response = json_encode($this->bookByTables($postData, $program));
            //make nof tables to 1 conditionaly
            if (!empty($postData['tableId2'])) {
                $postData['noOfTables2'] = 1;
            }
            if (!empty($postData['tableId4'])) {
                $postData['noOfTables4'] = 1;
            }
            if (!empty($postData['tableId6'])) {
                $postData['noOfTables6'] = 1;
            }
        } else {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => route('api.create.slot.booking'),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode($postData),
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Bearer ' . env('API_TOKEN'),
                    'Content-Type: application/json',
                    'Accept: application/json',
                    'language: en'
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
        }
        $response = json_decode($response);
        if (isset($response->statusCode)) {
            if ($response->statusCode == 1000) {
                $cartItems = [];
                $productCodes = [];
                switch ($request['smokingType']) {
                    case 1:
                        if ($postData['noOfTables2'] > 0) {
                            $cartItems[] = [
                                'Product' => ['ProductId' => $program->vgs_pcodes[$postData['table_type']]['2_seater']['smoking']],
                                'QuantityDelta' => $postData['noOfTables2'],
                                'Description' => ucfirst($postData['table_type']) . ' Smoking Table for two:' . $slotNames,
                                'Price' => $nofSlots * $program->vgs_pcodes[$postData['table_type']]['2_seater']['amount'],
                                'ValidDateFrom' => date('Y-m-d', strtotime($postData['programDate'])),
                                'ValidDateTo' => date('Y-m-d', strtotime($postData['programDate']))
                            ];
                            array_push($productCodes, $program->vgs_pcodes[$postData['table_type']]['2_seater']['smoking']);
                        }
                        if ($postData['noOfTables4'] > 0) {
                            $cartItems[] = [
                                'Product' => ['ProductId' => $program->vgs_pcodes[$postData['table_type']]['4_seater']['smoking']],
                                'QuantityDelta' => $postData['noOfTables4'],
                                'Description' => ucfirst($postData['table_type']) . ' Smoking Table for four:' . $slotNames,
                                'Price' => $nofSlots * $program->vgs_pcodes[$postData['table_type']]['4_seater']['amount'],
                                'ValidDateFrom' => date('Y-m-d', strtotime($postData['programDate'])),
                                'ValidDateTo' => date('Y-m-d', strtotime($postData['programDate']))
                            ];
                            array_push($productCodes, $program->vgs_pcodes[$postData['table_type']]['4_seater']['smoking']);
                        }
                        if ($postData['noOfTables6'] > 0) {
                            $cartItems[] = [
                                'Product' => ['ProductId' => $program->vgs_pcodes[$postData['table_type']]['6_seater']['smoking']],
                                'QuantityDelta' => $postData['noOfTables6'],
                                'Description' => ucfirst($postData['table_type']) . ' Smoking Table for six:' . $slotNames,
                                'Price' => $nofSlots * $program->vgs_pcodes[$postData['table_type']]['6_seater']['amount'],
                                'ValidDateFrom' => date('Y-m-d', strtotime($postData['programDate'])),
                                'ValidDateTo' => date('Y-m-d', strtotime($postData['programDate']))
                            ];
                            array_push($productCodes, $program->vgs_pcodes[$postData['table_type']]['6_seater']['smoking']);
                        }
                        break;
                    case 2:
                        if ($postData['noOfTables2'] > 0) {
                            $cartItems[] = [
                                'Product' => ['ProductId' => $program->vgs_pcodes[$postData['table_type']]['2_seater']['nonsmoking']],
                                'QuantityDelta' => $postData['noOfTables2'],
                                'Description' => ucfirst($postData['table_type']) . ' Non-Smoking Table for two:' . $slotNames,
                                'Price' => $nofSlots * $program->vgs_pcodes[$postData['table_type']]['2_seater']['amount'],
                                'ValidDateFrom' => date('Y-m-d', strtotime($postData['programDate'])),
                                'ValidDateTo' => date('Y-m-d', strtotime($postData['programDate']))
                            ];
                            array_push($productCodes, $program->vgs_pcodes[$postData['table_type']]['2_seater']['nonsmoking']);
                        }
                        if ($postData['noOfTables4'] > 0) {
                            $cartItems[] = [
                                'Product' => ['ProductId' => $program->vgs_pcodes[$postData['table_type']]['4_seater']['nonsmoking']],
                                'QuantityDelta' => $postData['noOfTables4'],
                                'Description' => ucfirst($postData['table_type']) . ' Non-Smoking Table for four:' . $slotNames,
                                'Price' => $nofSlots * $program->vgs_pcodes[$postData['table_type']]['4_seater']['amount'],
                                'ValidDateFrom' => date('Y-m-d', strtotime($postData['programDate'])),
                                'ValidDateTo' => date('Y-m-d', strtotime($postData['programDate']))
                            ];
                            array_push($productCodes, $program->vgs_pcodes[$postData['table_type']]['4_seater']['nonsmoking']);
                        }
                        if ($postData['noOfTables6'] > 0) {
                            $cartItems[] = [
                                'Product' => ['ProductId' => $program->vgs_pcodes[$postData['table_type']]['6_seater']['nonsmoking']],
                                'QuantityDelta' => $postData['noOfTables6'],
                                'Description' => ucfirst($postData['table_type']) . ' Non-Smoking Table for six:' . $slotNames,
                                'Price' => $nofSlots * $program->vgs_pcodes[$postData['table_type']]['6_seater']['amount'],
                                'ValidDateFrom' => date('Y-m-d', strtotime($postData['programDate'])),
                                'ValidDateTo' => date('Y-m-d', strtotime($postData['programDate']))
                            ];
                            array_push($productCodes, $program->vgs_pcodes[$postData['table_type']]['6_seater']['nonsmoking']);
                        }
                        break;
                }

                $vgsResponse = $this->addToVgs($postData, $cartItems);
                if ($vgsResponse['status']) {
                    \App\BookSlot::where('id', $response->data->bookingId)->update([
                        'status' => 'order_confirmed',
                        'status_updated_at' => date('Y-m-d H:i:s'),
                        'shop_cart_id' => $vgsResponse['extras']['shop_cart_id'],
                        'sale_code' => $vgsResponse['extras']['sale_code'],
                        'vgs_product_codes' => implode(',', $productCodes),
                        'ticket_ids' => implode(',', $vgsResponse['extras']['ticket_ids']),
                    ]);
                    \App\ProgramSlot::where('booking_id', $response->data->bookingId)->update(['program_status' => 3]);
                    alert()->success('Booking completed successfully.', 'Booking Completed')->persistent();
                    return response()->json(["status" => true, "message" => 'Booking Completed.']);
                } else {
                    \App\BookSlot::where('id', $response->data->bookingId)->update([
                        'status' => 'vgs_admin_timeout',
                        'status_updated_at' => date('Y-m-d H:i:s'),
                        'vgs_product_codes' => implode(',', $productCodes)
                    ]);
                    \App\ProgramSlot::where('booking_id', $response->data->bookingId)->update(['program_status' => 1, 'booking_id' => NULL]);
                    return response()->json(["status" => false, "message" => $vgsResponse['message']]);
                }
            }
            return response()->json(["status" => false, "message" => $response->message]);
        }

        return response()->json(["status" => false, "message" => 'Something went wrong.']);
    }

    private function addToVgs(&$request, &$cartItems) {
        $configurations = \App\ApiConfiguration::whereIn('name', ['work_station_id', 'payment_method_card_admin_id', 'order_doc_template_id'])->pluck('value', 'name')->toArray();
        $url = config('accountConfig.pprUrl');
        $userAcntId = null;
        $vgsRspns = ['status' => 0, 'message' => 'Something went wrong on sync VGS.'];
        //First check acount exist or not
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "&cmd=ACCOUNT",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode([
                'Header' => ["WorkstationId" => $configurations['work_station_id']],
                'Request' => [
                    'Command' => 'Search',
                    'Search' => ['FullText' => $request['email'], 'AccountStatus' => 1]
                ]
            ]),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'cache-control: no-cache',
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response);
        if (isset($response->Answer)) {
            if ($response->Answer->Search->TotalRecordCount > 0) {
                $userAcntId = $response->Answer->Search->AccountList[0]->AccountId;
            } else {
                //Call Save account
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url . "&cmd=ACCOUNT",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode([
                        'Header' => ["WorkstationId" => $configurations['work_station_id']],
                        'Request' => [
                            'Command' => 'SaveAccount',
                            'SaveAccount' => [
                                'AccountStatus' => 1,
                                'EmailAddress' => $request['email'],
                                'EntityType' => 15,
                                'MetaDataList' => [
                                    ['MetaFieldCode' => 'FT1', 'Value' => $request['firstName']],
                                    ['MetaFieldCode' => 'FT3', 'Value' => $request['lastName']],
                                    ['MetaFieldCode' => 'FT21', 'Value' => $request['email']],
                                    ['MetaFieldCode' => 'MOBNO', 'Value' => $request['mobile']]
                                ]
                            ]
                        ]
                    ]),
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json',
                        'cache-control: no-cache',
                    ),
                ));

                $response = curl_exec($curl);
                curl_close($curl);
                $response = json_decode($response);
                $userAcntId = $response->Answer->SaveAccount->AccountId;
            }
        } else {
            return ['status' => 0, 'message' => 'Failed VGS call.'];
        }
        if (!empty($userAcntId)) {
            $shopCartId = '';
            if (!empty($cartItems)) {
                $shopCartId = $this->addToCart($cartItems, $configurations['work_station_id']);
            }

            if (!empty($shopCartId)) {
                //Set Account
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url . "&cmd=SHOPCART",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode([
                        'Header' => ["WorkstationId" => $configurations['work_station_id']],
                        'Request' => [
                            'Command' => 'SetOwnerAccount',
                            'ShopCartId' => $shopCartId,
                            'LangISO' => 'en',
                            'SetOwnerAccount' => [
                                'AccountId' => $userAcntId
                            ]
                        ]
                    ]),
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json',
                        'cache-control: no-cache',
                    ),
                ));

                $response = curl_exec($curl);
                curl_close($curl);
                $response = json_decode($response);
                if (isset($response->Answer->ShopCart)) {
                    //Call Chekout
                    $saleCode = $this->checkOut($shopCartId, $configurations, $request);
                    if (!empty($saleCode)) {
                        $vgsRspns = [
                            'status' => 1,
                            'message' => 'VGS suucessfully synced.',
                            'extras' => [
                                'shop_cart_id' => $shopCartId,
                                'sale_code' => $saleCode['saleCode'],
                                'ticket_ids' => $saleCode['ticketIds']
                            ]
                        ];
                        //Empty Cart
                        $this->emptyCart($shopCartId, $configurations['work_station_id']);
                    } else {
                        $vgsRspns = ['status' => 0, 'message' => 'VGS checkout API failed.'];
                    }
                } else {
                    $vgsRspns = ['status' => 0, 'message' => 'Set user VGS account failed.'];
                }
            } else {
                $vgsRspns = ['status' => 0, 'message' => 'VGS add to cart API failed.'];
            }
        } else {
            $vgsRspns = ['status' => 0, 'message' => 'Create VGS user account failed.'];
        }
        return $vgsRspns;
    }

    private function addToCart($cartItems, $workstationId) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => config('accountConfig.pprUrl') . "&cmd=SHOPCART",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode([
                'Header' => ["WorkstationId" => $workstationId],
                'Request' => [
                    'Command' => 'AddToCart',
                    'ShopCartId' => '',
                    'LangISO' => 'en',
                    'AddToCart' => [
                        'ItemList' => $cartItems,
                    ]
                ]
            ]),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'cache-control: no-cache',
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response);
        if (isset($response->Answer->ShopCart)) {
            return $response->Answer->ShopCart->ShopCartId;
        }
        return null;
    }

    private function checkOut($shopCartId, $configurations, $request) {
        $pExtras = [
            'PaymentType' => (int) $request['paymentMode'],
            'PaymentAmount' => (string) $request['amount']
        ];
        if ($request['paymentMode'] == 2) {
            $pExtras['PaymentMethodId'] = $configurations['payment_method_card_admin_id'];
            $pExtras['CreditCard'] = [
                'AuthorizationCode' => $request['approvalCode'],
                'CardNumber' => $request['cardNumber'],
                'CardHolderName' => $request['firstName'],
                'CardType' => $request['cardType']
            ];
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => config('accountConfig.pprUrl') . "&cmd=TRANSACTION",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode([
                'Header' => ["WorkstationId" => $configurations['work_station_id']],
                'Request' => [
                    'Command' => 'PostTransaction',
                    'PostTransaction' => [
                        'ShopCartId' => $shopCartId,
                        'PreparePahDownload' => true,
                        'CreateOrderConfirmation' => true,
                        'SendOrderConfirmation' => true,
                        'IncludeOrderConfirmationTickets' => true,
                        'ReceiptEmailAddress' => $request['email'],
                        'OrderDocTemplateId' => $configurations['order_doc_template_id'],
                        'Approved' => true,
                        'Paid' => true,
                        'Encoded' => true,
                        'Printed' => true,
                        'Completed' => true,
                        'Validated' => true,
                        'ReturnFullTransaction' => true,
                        'PaymentList' => [$pExtras]
                    ]
                ]
            ]),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'cache-control: no-cache',
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response);
        $checkoutValues = [
            'saleCode' => '',
            'ticketIds' => []
        ];
        if (isset($response->Answer->PostTransaction->PostTransactionRecap->SaleCode)) {
            $checkoutValues['saleCode'] = $response->Answer->PostTransaction->PostTransactionRecap->SaleCode;
        }
        if (isset($response->Answer->PostTransaction->FullTransaction->TicketList)) {
            $tIds = $response->Answer->PostTransaction->FullTransaction->TicketList;
            if (!empty($tIds)) {
                foreach ($tIds as $tId) {
                    array_push($checkoutValues['ticketIds'], $tId->TicketId);
                }
            }
        }
        return $checkoutValues;
    }

    private function emptyCart($shopCartId, $workstationId) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => config('accountConfig.pprUrl') . "&cmd=SHOPCART",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode([
                'Header' => ["WorkstationId" => $workstationId],
                'Request' => [
                    'LangISO' => 'en',
                    'Command' => 'EmptyShopCart',
                    'ShopCartId' => $shopCartId,
                    'EmptyShopCart' => [
                        'HoldReleaseIfNeeded' => false
                    ]
                ]
            ]),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'cache-control: no-cache',
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response);
        return 1;
    }

    private function bookByTables(&$postData, &$program) {
        $pSolts = [];
        $pDate = date("Y-m-d", strtotime($postData['programDate']));
        $i = 0;
        foreach ($postData['slots'] as $slot) {
            $i++;
            if (!empty($postData['tableId2'])) {
                $pgs = \App\ProgramSlot::where('program_id', $program->id)
                        ->where('slot_id', $slot)
                        ->where('table_id', $postData['tableId2'])
                        ->where('program_date', $pDate)
                        ->whereNull("booking_id")
                        ->where('program_status', 1)
                        ->where('status', 1)
                        ->first();
                if ($pgs) {
                    $pSolts[] = $pgs->id;
                } else {
                    return [
                        "statusCode" => 2000,
                        "message" => "Table 2 seater is not available for slot-{$i}"
                    ];
                }
            }
            if (!empty($postData['tableId4'])) {
                $pgs = \App\ProgramSlot::where('program_id', $program->id)
                        ->where('slot_id', $slot)
                        ->where('table_id', $postData['tableId4'])
                        ->where('program_date', $pDate)
                        ->whereNull("booking_id")
                        ->where('program_status', 1)
                        ->where('status', 1)
                        ->first();
                if ($pgs) {
                    $pSolts[] = $pgs->id;
                } else {
                    return[
                        "statusCode" => 2000,
                        "message" => "Table 4 seater is not available for slot-{$i}"
                    ];
                }
            }
            if (!empty($postData['tableId6'])) {
                $pgs = \App\ProgramSlot::where('program_id', $program->id)
                        ->where('slot_id', $slot)
                        ->where('table_id', $postData['tableId6'])
                        ->where('program_date', $pDate)
                        ->whereNull("booking_id")
                        ->where('program_status', 1)
                        ->where('status', 1)
                        ->first();
                if ($pgs) {
                    $pSolts[] = $pgs->id;
                } else {
                    return [
                        "statusCode" => 2000,
                        "message" => "Table 6 seater is not available for slot-{$i}"
                    ];
                }
            }
        }

        $booking = new \App\BookSlot;
        $booking->program_id = $program->id;
        $booking->admin_id = $postData['adminId'];
        $booking->payment_mode = $postData['paymentMode'];
        $booking->status = 'pending';
        $booking->payment_extras = [
            'smokingType' => $postData['smokingType'],
            'tableId2' => $postData['tableId2'],
            'tableId4' => $postData['tableId4'],
            'tableId6' => $postData['tableId6'],
            'tableType' => $postData['table_type'],
        ];
        $booking->program_slot_id = implode(",", $pSolts);
        $booking->program_date = $pDate;
        $leastSlot = \App\ProgramSlot::select('slots.id', 'slots.start_time', 'actual_start_time')
                ->join('slots', 'slots.id', 'program_slots.slot_id')
                ->whereIn('program_slots.id', $pSolts)
                ->orderBy('slots.priority')
                ->first();
        if (!empty($leastSlot->actual_start_time)) {
            $booking->actual_program_dt = date('Y-m-d', strtotime($booking->program_date . ' + 1 days')) . ' ' . $leastSlot->actual_start_time;
        } else {
            $booking->actual_program_dt = $booking->program_date . ' ' . $leastSlot->start_time;
        }
        $booking->table_type = $postData['table_type'];
        $booking->first_name = $postData['firstName'];
        $booking->last_name = $postData['lastName'];
        $booking->email = $postData['email'];
        $booking->mobile = $postData['mobile'];
        $booking->special_notes = $postData['specialNotes'];
        $booking->amount = $postData['amount'];
        $booking->is_vip = $postData['isVip'];
        if (date('Y-m-d H:i') <= date('Y-m-d') . ' 05:59') {
            $booking->operational_date = date("Y-m-d", strtotime("yesterday"));
        } else {
            $booking->operational_date = date('Y-m-d');
        }
        $booking->status_updated_at = date('Y-m-d H:i:s');
        $booking->save();
        foreach ($pSolts as $id) {
            $history = new \App\ProgramSlotStatusHistory;
            $history->program_slot_id = $id;
            $history->program_status = 1;
            $history->save();
        }

        \App\ProgramSlot::whereIn('id', $pSolts)->update(['booking_id' => $booking->id]);
        return [
            "statusCode" => 1000,
            "message" => 'Booking completed successfully.',
            "data" => [
                'bookingId' => $booking->id,
                'program' => [
                    'id' => $program->id
                ]
            ]
        ];
    }

}
