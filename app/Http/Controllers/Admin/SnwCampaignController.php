<?php

namespace App\Http\Controllers\Admin;

use App\SnwCampaign;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Auth;

class SnwCampaignController extends Controller {

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('shake and win_read', 'admin')) {
                return $next($request);
            } else {
                alert()->error('You don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['index', 'show']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('shake and win_create', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['create', 'store']]);

        $this->middleware(
                function ($request, $next) {
                    $user = Auth::guard('admin')->user();
                    if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('shake and win_update', 'admin')) {
                        return $next($request);
                    } else {
                        alert()->error('You don\'t have permission to acces this page.', 'No Access!');
                        return redirect()->route('admin.dashboard');
                    }
                }, ['only' => ['edit', 'update']]
        );
    }

    public function index() {

        return view('admin.snw.index');
    }

    public function datatable(Request $request) {
        $currentUser = Auth::guard('admin')->user();
        $isSuperAdmin = $currentUser->hasRole('super-admin', 'admin');
        $datas = SnwCampaign::select('*');

        return Datatables::of($datas)
                        ->rawColumns(['actions', 'status'])
                        ->editColumn('actions', function ($data) use ($currentUser, $isSuperAdmin) {
                            $b = '';
                            if ($isSuperAdmin || $currentUser->hasPermissionTo('shake and win_update', 'admin')) {
                                $b .= '<a class="btn btn-primary btn-sm " href="' . route('admin.snw-campaigns.edit', $data->id) . '">Edit</a>';
                            }
                            if ($isSuperAdmin || $currentUser->hasPermissionTo('shake and win_read', 'admin')) {
                                $b .= ' <a class="btn btn-success btn-sm " href="' . route('admin.snw-prizes.index') . '?campaign=' . $data->id . '">Prizes</a>';
                            }
                            return $b;
                        })
                        ->editColumn('status', function ($data) {
                            $checked = ($data->status == 1) ? 'checked' : '';
                            return '<input type="checkbox" name="status" id="status_' . $data->id . '"  data-on-text="Enabled" data-off-text="Disabled" ' . $checked . ' class="bsSwitch" autocomplete="off" data-id="' . $data->id . '">';
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.snw.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'names.*' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date'
        ]);

        $data = new SnwCampaign();
        $data->names = $request->names;
        $data->start_date = $request->start_date;
        $data->end_date = $request->end_date;
        $data->save();

        alert()->success('Successfully Created.', 'Created');
        return redirect()->route('admin.snw-campaigns.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = SnwCampaign::where('id', $id)->first();
        return view('admin.snw.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'names.*' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date'
        ]);

        $data = SnwCampaign::find($id);
        $data->names = $request->names;
        $data->start_date = $request->start_date;
        $data->end_date = $request->end_date;
        $data->save();

        alert()->success('Successfully updated.', 'Updated');
        return redirect()->route('admin.snw-campaigns.index');
    }

    public function changeStatus(Request $request) {
        switch ($request->module) {
            case 'campaigns':
                $data = SnwCampaign::find($request->id);
                break;
            case 'prizes':
                $data = \App\SnwPrize::find($request->id);
                break;
        }
        $data->status = ($data->status == 1) ? 0 : 1;
        $data->save();
        return response()->json(["status" => 'success', "message" => 'Successfully changed']);
    }

}
