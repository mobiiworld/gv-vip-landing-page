<?php

namespace App\Http\Controllers\Admin;

use App\AuditLog;
use App\Http\Controllers\Controller;
use App\ShoppingCartTransaction;
use App\UserPaymentSessions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Datatables;
use App\Traits\CarTraits;
use App\Admin;
use Auth;
use URL;
use DB;
//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
//Enables us to output flash messaging
use Validator;

class CarPlateInfoController extends Controller {

    use CarTraits;

    public function __construct() {
           }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        //Get all users and pass it to the view
        $total = DB::Connection('mysql2')
            ->table('gv_user_payment_sessions')
            ->leftJoin('gv_car_info', 'gv_user_payment_sessions.shopcartid', '=', 'gv_car_info.uniquegeneratedid')
            ->count();
        return view('admin.car_plate.index',compact('total'));
    }
    //using php curl (sudo apt-get install php-curl)


    /**
     * To display dynamic table by datatable
     *
     * @return mixed
     */
    public function datatable(Request $request) {
        ini_set('memory_limit', '-1');
       // $transaction = UserPaymentSessions::with('getCarInfo')->where('payment_category','changeVehicleId');
        $transaction = DB::Connection('mysql2')
            ->table('gv_user_payment_sessions')
            ->leftJoin('gv_car_info', 'gv_user_payment_sessions.shopcartid', '=', 'gv_car_info.uniquegeneratedid')
            ->select(
                'gv_car_info.id',
                'gv_car_info.saleid',
                'gv_car_info.packid',
                'gv_car_info.carid',
                'gv_car_info.packnumber',
                //'gv_car_info.designa_status',
                'gv_car_info.email',
                'gv_user_payment_sessions.platform',
                'gv_car_info.newcountrycode',
                'gv_car_info.newcitycode',
                'gv_car_info.newplateprefix',
                'gv_car_info.newplatenumber',
                'gv_car_info.oldcountrycode',
                'gv_car_info.oldcitycode',
                'gv_car_info.oldplateprefix',
                'gv_car_info.oldplatenumber',
                'gv_car_info.designa_sync_status',
                DB::raw('CONCAT(gv_car_info.newcountrycode, " ", gv_car_info.newcitycode, " ", gv_car_info.newplateprefix, " ", gv_car_info.newplatenumber) AS new_plate_number'),
                DB::raw('CONCAT(gv_car_info.oldcountrycode, " ", gv_car_info.oldcitycode, " ", gv_car_info.oldplateprefix, " ", gv_car_info.oldplatenumber) AS old_plate_number'),
                'gv_user_payment_sessions.amount',
                'gv_user_payment_sessions.status',
                'gv_user_payment_sessions.created_date'
            )->where('gv_user_payment_sessions.payment_category','changeVehicleId');

        $season = \App\Season::where('status',1)->first();
        $season_number = (isset($season->season_number)) ? $season->season_number : 28;
        $transaction->where('gv_user_payment_sessions.season_number',$season_number);
        
        if(!empty($request->payment_status)){
                $transaction = $transaction->where('gv_user_payment_sessions.status', $request->payment_status);
        }
        if(!empty($request->plate_form)) {
            $transaction = $transaction->where('gv_user_payment_sessions.platform', $request->plate_form);
        }
        if(!empty($request->start_date) && !empty($request->end_date)){
            $start_date = date('Y-m-d',strtotime($request->start_date));
            $end_date = date('Y-m-d',strtotime($request->end_date));
            $transaction =  $transaction->whereDate('gv_user_payment_sessions.created_date','>=',$start_date)->whereDate('gv_user_payment_sessions.created_date','<=',$end_date);
        }else{
            $start_date = date('Y-m-d');
            $transaction =  $transaction->whereDate('gv_user_payment_sessions.created_date','>=',$start_date);
        }
        if(!empty($request->sync_status)) {
            if($request->sync_status == 2){
                $request->sync_status =0;
            }
            $transaction = $transaction->where('gv_car_info.designa_status', $request->sync_status);
        }

        $transaction = $transaction->orderBy('gv_user_payment_sessions.created_date','desc');
        return Datatables::of($transaction)
            ->rawColumns(['status','designa_sync_status'])
            //->rawColumns(['status','designa_status'])
            ->filterColumn('new_plate_number', function($query, $keyword) {
                $query->whereRaw('CONCAT(gv_car_info.newcountrycode, " ", gv_car_info.newcitycode, " ", gv_car_info.newplateprefix, " ", gv_car_info.newplatenumber) like ?', ["%{$keyword}%"]);
            })->filterColumn('old_plate_number', function($query, $keyword) {
                $query->whereRaw('CONCAT(gv_car_info.oldcountrycode, " ", gv_car_info.oldcitycode, " ", gv_car_info.oldplateprefix, " ", gv_car_info.oldplatenumber) like ?', ["%{$keyword}%"]);
            })
            ->editColumn('created_date', function ($transaction) {
               return (!empty($transaction->created_date))?date('F d, Y h:ia',strtotime($transaction->created_date)):'';

            })->editColumn('status', function ($transaction) {
                $label = '';

                     if($transaction->status == 1){
                         $label .= '<span class="label label-danger">Created</span>';
                     }elseif($transaction->status == 2){
                         $label .= '<span class="label label-info">Initiated</span>';
                     }elseif($transaction->status == 3){
                         $label .= '<span class="label label-warning">Authenticate</span>';
                     }elseif($transaction->status == 4){
                         $label .= '<span class="label label-success">Paid</span>';
                     }else{
                         $label .= '<span class="label label-default">Not Paid</span>';
                     }

                return $label;            
            })->editColumn('designa_sync_status', function ($transaction) {
                $label = '';

                     if($transaction->designa_sync_status == 1){
                         $label .= '<span class="label label-success">Synced</span>';
                     }else{
                         $label .= '<span class="label label-danger">Not Synced</span>';
                     }

                return $label;
            })
            /*->editColumn('designa_status', function ($transaction) {
                $b='';
                if($transaction->status == 4){
                  if($transaction->designa_status == 1){
                      $b = '<span class="label label-success">Synced</span>';
                  }else{
                      $b = '<span class="label label-danger">Not synced</span>';
                      $b .= '&nbsp;&nbsp;  <a class="btn btn-primary btn-sm btnSyncDesigna" style="margin-top:8px;" href="javascript:void(0)" data-id="' . $transaction->id . '">Sync Designa</a>';
                  }

                }
                return $b;
            })*/
            ->make(true);
    }

    public function syncDesigna(Request $request) {
        //return response()->json(["status" => true, "message" => 'User cars synced.', 'responseLog' => ['request' => ['sad'=>31,'sad'=>56], 'response' => 'aasdjgasdjgj']]);
        $errArray = [];

        $carInfo = DB::Connection('mysql2')
            ->table('gv_car_info')->where('id', $request->id)->first();

        if(empty($carInfo)){
            return response()->json(["status" => false, "message" => 'User has no cars to sync.']);
        }

        $car = \App\NewUserCar::where('id', $carInfo->carid)->first();

        if(empty($car)){
            return response()->json(["status" => false, "message" => 'User has no cars to sync.']);
        }

        if (!empty($carInfo)) {
            $carPack = \App\UserCarPack::where('user_car_id', $car->id)->where('pack_id',$car->pack_id)->first();
            if (!empty($carPack) && !empty($carPack->designa_card_uid)) {
                $syncRequest = new Request([
                    'countryCode' => $carInfo->newcountrycode,
                    'cityCode' => !empty($carInfo->newcitycode) ? $carInfo->newcitycode : null,
                    'plate_prefix' => $carInfo->newplateprefix,
                    'plate_number' => $carInfo->newplatenumber,
                ]);

                try{
                      if ($this->syncCar($car->id, $syncRequest,$carPack)) {
                          //update designa sync status

                          DB::Connection('mysql2')->table('gv_car_info')
                                      ->where('id', $request->id)
                                      ->update(['designa_status' => 1]);


                          return response()->json(["status" => true, "message" => 'User cars synced.']);
                      } else {
                          return response()->json(["status" => false, "message" => 'Designa sync Failed.']);
                      }

                  } catch (\Exception $e) {
                      return response()->json(["status" => false, "message" => 'Designa sync Failed.']);
                  }
            }else{
                return response()->json(["status" => false, "message" => 'Car details not found designa.']);
            }

        } else {
            return response()->json(["status" => false, "message" => 'User has no cars to sync.']);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

    }

}
