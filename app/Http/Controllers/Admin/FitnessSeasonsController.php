<?php

namespace App\Http\Controllers\Admin;

use App\FitnessPrize;
use App\FitnessSeason;
use App\FitnessSeasonImages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Traits\ImageTraits;
use Auth;
use URL;
use App\Program;

class FitnessSeasonsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use ImageTraits;

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('fitness_read', 'admin')) {
                return $next($request);
            } else {
                alert()->error('You don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['index', 'show']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('fitness_create', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['create', 'store']]);

        $this->middleware(
                function ($request, $next) {
                    $user = Auth::guard('admin')->user();
                    if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('fitness_update', 'admin')) {
                        return $next($request);
                    } else {
                        alert()->error('You don\'t have permission to acces this page.', 'No Access!');
                        return redirect()->route('admin.dashboard');
                    }
                }, ['only' => ['edit', 'update']]
        );
    }

    public function index() {

        return view('admin.fitness_seasons.index');
    }

    public function datatable(Request $request) {

        $fitness_season = FitnessSeason::select('*')->withCount(['participants', 'dailyWinners', 'monthlyWinners', 'seasonWinners']);

        return Datatables::of($fitness_season)
                        ->rawColumns(['actions', 'daily_target', 'monthly_target', 'season_target'])
                        ->editColumn('actions', function ($fs) {
                            $b = ' <a class="btn btn-primary btn-sm " href="' . route('admin.fitness-season.edit', $fs->id) . '">Edit</a>';
                            return $b;
                        })
                        ->editColumn('daily_target', function ($fs) {
                            $b = '<span class="label label-warning">Target Steps: ' . $fs->target_steps['daily'] . '</span>';
                            $b .= '<span class="label label-success sec-label">Winners: ' . $fs->daily_winners_count . '</span>';
                            return $b;
                        })
                        ->editColumn('monthly_target', function ($fs) {
                            $b = '<span class="label label-warning">Target Steps: ' . $fs->target_steps['monthly'] . '</span>';
                            $b .= '<span class="label label-success sec-label">Winners: ' . $fs->monthly_winners_count . '</span>';
                            return $b;
                        })
                        ->editColumn('season_target', function ($fs) {
                            $b = '<span class="label label-warning">Target Steps: ' . $fs->target_steps['season'] . '</span>';
                            $b .= '<span class="label label-success sec-label">Winners: ' . $fs->season_winners_count . '</span>';
                            return $b;
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.fitness_seasons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'names.*' => 'required',
            'target_steps.*' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
        ]);

        $fitness_season = new FitnessSeason();
        $fitness_season->names = $request->names;
        $fitness_season->target_steps = $request->target_steps;
        $fitness_season->program_name = $request->program_name;
        $fitness_season->program_description = $request->program_description;
        $fitness_season->popup_titles = $request->popup_titles;
        $fitness_season->popup_messages = $request->popup_messages;
        $fitness_season->duration = $request->duration;
        $fitness_season->is_active = 1;
        $fitness_season->is_show_homepage = isset($request->is_show_homepage) ? 1 : 0;
        $fitness_season->is_next_day = isset($request->is_next_day) ? 1 : 0;
        $fitness_season->start_date = date('Y-m-d', strtotime($request->start_date));
        $fitness_season->end_date = date('Y-m-d', strtotime($request->end_date));
        $fitness_season->start_time = date('H:i:s', strtotime($request->start_time));
        $fitness_season->end_time = date('H:i:s', strtotime($request->end_time));
        if (!empty($request->area_cordinates_map)) {
            $polygon_values = $this->setPolygon($request->area_cordinates_map);
            $fitness_season->in_area = \DB::raw('ST_GeomFromText(\'POLYGON((' . $polygon_values . '))\')');
        }

        $fitness_season->max_ups_prize_count = $request->max_ups_prize_count;
        $fitness_season->save();
        $fitness_prizes_type = ['daily', 'season', 'monthly'];

        if ($fitness_season) {
            foreach ($fitness_prizes_type as $type) {
                $fitness_season_prizes = new FitnessPrize();
                $fitness_season_prizes->fitness_season_id = $fitness_season->id;
                $fitness_season_prizes->names = $request->{'fitness_prizes_names_' . $type};
                $fitness_season_prizes->descriptions = $request->{'fitness_prizes_description_' . $type};
                $fitness_season_prizes->type = $type;
                if ($request->hasFile($type . '_icon')) {
                    $resizedThumbImage = $this->resize($request->file($type . '_icon'), 'admin/fitness_season/icons/');
                    $fitness_season_prizes->icon = $resizedThumbImage;
                }
                $fitness_season_prizes->save();
            }

            if ($request->file('images') != null) {
                $files = $request->file('images');
                foreach ($files as $lang => $imgs) {
                    foreach ($imgs as $key => $file) {
                        //$maxIndex++;
                        $relPath = 'admin/fitness_season/';
                        $fitness_images = new FitnessSeasonImages();
                        $fitness_images->fitness_season_id = $fitness_season->id;
                        $fitness_images->image = $this->resize($file, $relPath);
                        $fitness_images->index_no = $key + 1;
                        $fitness_images->language = $lang;
                        $fitness_images->save();
                    }
                }
            }
        }

        alert()->success('Fitness Season successfully added.', 'Added');
        return redirect()->route('admin.fitness-season.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $fitness_seasons = FitnessSeason::with('seasonImages')->select(
                        \DB::raw('ST_AsGeoJSON(in_area) as cordinates'),
                        'id',
                        'names',
                        'start_date',
                        'end_date',
                        'target_steps',
                        'max_ups_prize_count',
                        'program_name',
                        'program_description',
                        'popup_titles',
                        'popup_messages',
                        'is_show_homepage',
                        'start_time',
                        'end_time',
                        'is_next_day',
                        'duration'
                )->where('id', $id)->first();

        //$regionCordinates = Region::select(\DB::raw('ST_AsGeoJSON(area_cordinates) as cordinates'))->where('id', $id)->first();
        $cordinates = json_decode($fitness_seasons->cordinates, true);
        $mapCordinates = json_encode($cordinates['coordinates'][0]);
        $fitness_prizes_daily = FitnessPrize::where('type', 'daily')->where('fitness_season_id', $id)->first();
        $fitness_prizes_monthly = FitnessPrize::where('type', 'monthly')->where('fitness_season_id', $id)->first();
        $fitness_prizes_season = FitnessPrize::where('type', 'season')->where('fitness_season_id', $id)->first();

        return view('admin.fitness_seasons.edit', compact('fitness_seasons', 'mapCordinates', 'fitness_prizes_daily', 'fitness_prizes_monthly', 'fitness_prizes_season'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'names.*' => 'required',
            'target_steps.*' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
        ]);
        //dd($request->has_priority);
        $fitness_season = FitnessSeason::find($id);
        $fitness_season->names = $request->names;
        $fitness_season->target_steps = $request->target_steps;
        $fitness_season->start_date = date('Y-m-d', strtotime($request->start_date));
        $fitness_season->end_date = date('Y-m-d', strtotime($request->end_date));
        $fitness_season->start_time = date('H:i:s', strtotime($request->start_time));
        $fitness_season->end_time = date('H:i:s', strtotime($request->end_time));
        $fitness_season->program_name = $request->program_name;
        $fitness_season->program_description = $request->program_description;
        $fitness_season->popup_titles = $request->popup_titles;
        $fitness_season->duration = $request->duration;
        $fitness_season->popup_messages = $request->popup_messages;
        $fitness_season->is_show_homepage = isset($request->is_show_homepage) ? 1 : 0;
        $fitness_season->is_next_day = isset($request->is_next_day) ? 1 : 0;
        if (!empty($request->area_cordinates_map)) {
            $polygon_values = $this->setPolygon($request->area_cordinates_map);
            $fitness_season->in_area = \DB::raw('ST_GeomFromText(\'POLYGON((' . $polygon_values . '))\')');
        }
        $fitness_season->max_ups_prize_count = $request->max_ups_prize_count;
        $fitness_season->save();

        $fitness_prizes_type = ['daily', 'season', 'monthly'];

        if ($fitness_season) {
            foreach ($fitness_prizes_type as $type) {
                $fitness_season_prizes = FitnessPrize::where('type', $type)->where('fitness_season_id', $fitness_season->id)->first();
                if ($fitness_season_prizes) {
                    
                } else {
                    $fitness_season_prizes = new FitnessPrize();
                    $fitness_season_prizes->fitness_season_id = $fitness_season->id;
                }

                $fitness_season_prizes->names = $request->{'fitness_prizes_names_' . $type};
                $fitness_season_prizes->descriptions = $request->{'fitness_prizes_description_' . $type};
                $fitness_season_prizes->type = $type;
                if ($request->hasFile($type . '_icon')) {
                    $resizedThumbImage = $this->resize($request->file($type . '_icon'), 'admin/fitness_season/icons/');
                    if (!empty($fitness_season_prizes->icon)) {
                        \Storage::disk('azure')->delete('admin/fitness_season/icons/' . $fitness_season_prizes->getOriginal('icon'));
                    }
                    $fitness_season_prizes->icon = $resizedThumbImage;
                }
                $fitness_season_prizes->save();
            }
            if ($request->file('images') != null) {
                $files = $request->file('images');
                foreach ($files as $lang => $imgs) {
                    $maxIndex = FitnessSeasonImages::where('fitness_season_id', $fitness_season->id)->where('language', $lang)->max('index_no');
                    $maxIndex = ($maxIndex) ? $maxIndex : 0;
                    foreach ($imgs as $key => $file) {
                        $maxIndex++;
                        $relPath = 'admin/fitness_season/';
                        $fitness_images = new FitnessSeasonImages();
                        $fitness_images->fitness_season_id = $fitness_season->id;
                        $fitness_images->image = $this->resize($file, $relPath);
                        $fitness_images->index_no = $maxIndex;
                        $fitness_images->language = $lang;
                        $fitness_images->save();
                    }
                }
            }
        }
        alert()->success('Fitness Season successfully updated.', 'Updated');
        return redirect()->back();
    }

    function setPolygon($string) {
        $pArray = json_decode($string, true);
        $nar = [];
        foreach ($pArray as $cord) {
            array_push($nar, implode(' ', $cord));
        }
        array_push($nar, $pArray[0]['lat'] . ' ' . $pArray[0]['lng']);
        return join(',', $nar);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function publish(Request $request) {
        $fitness_season = FitnessSeason::find($request->dataId);
        if ($request->data_action_type == "is_active") {
            $fitness_season->is_active = $request->dataValue;
        }
        if ($request->data_action_type == "geofence") {
            $fitness_season->geofence = $request->dataValue;
        }
        $fitness_season->save();
        return;
    }

    public function sortable_update(Request $request) {

        foreach ($request->order as $order) {
            FitnessSeasonImages::where('id', $order['id'])->update(['index_no' => $order['position']]);
        }
    }

    public function delete_fitness_season_image($season_image_id) {
        $img = FitnessSeasonImages::where('id', $season_image_id)->first();
        \Storage::disk('azure')->delete('admin/fitness_season/' . $img->getOriginal('image'));
        $img->delete();
        alert()->success('Successfully deleted.', 'Updated');
        return redirect()->back();
    }

    public function updateImagesType(Request $request) {
        $img = FitnessSeasonImages::find($request->id);
        if (empty($request->link)) {
            $img->type = 'image';
            $img->url = null;
        } else {
            $img->type = 'youtube';
            $img->url = $request->link;
        }
        $img->save();
        return response()->json(["status" => true, "type" => $img->type]);
    }

    public function setDefaultImage(Request $request) {
        $id = explode('|', $request->id);
        FitnessSeasonImages::where('fitness_season_id', $request->season_id)
                ->where('language', $id[1])
                ->where('default', 1)
                ->update(['default' => 0]);
        FitnessSeasonImages::where('id', $id[0])->update(['default' => 1]);
        return response()->json(["status" => true]);
    }

}
