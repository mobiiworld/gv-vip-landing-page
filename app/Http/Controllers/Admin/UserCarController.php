<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Auth;
use URL;
use App\Pack;
use App\NewUserCar;
use App\CarCountry;
use App\Emirate;
use App\Validator;
use App\Traits\CarTraits;
use App\Traits\AccountActivationTraits;
use App\Traits\GvpTraits;

use Mail;
use App\Mail\CarMail;
use App\Traits\SalesForceTraits;

class UserCarController extends Controller {

    use CarTraits,
        GvpTraits,SalesForceTraits,
        AccountActivationTraits;

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $car_countries = CarCountry::select('id', "name_en as name")->pluck('name', 'id')->prepend('Select', '');
        $emirates = ['' => 'Select'];
        $pprefixes = ['' => 'Select'];


        return view('admin.user_cars.create', compact( 'car_countries', 'emirates', 'pprefixes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'country_id' => 'required',
            //'emirate_id' => 'required',
            'plate_prefix' => 'required',
            'plate_number' => 'required',
        ]);

        $userPack = \App\Pack::select('packs.id', 'packs.prefix', 'packs.pack_number', 'packs.sale_id', 'users.id as user_id', 'users.first_name', 'users.last_name', 'users.email', 'users.mobile', 'users.mobile_dial_code', 'users.gender', 'users.dob', 'users.nationality_id', 'users.recieve_updates','packs.car_activation_allowed','users.drupal_user_id','user_packs.id as upid')
                    ->join('user_packs', 'user_packs.pack_id', 'packs.id')
                    ->join('users', 'users.id', 'user_packs.user_id')
                    ->where('packs.id', $request->pack_id)->first();
        if(empty($userPack )){
            //return errorResponse('Pack not exists or activated');
            alert()->error('Pack not exists or activated', 'User Car');
            return redirect()->back();
        }
        $totalCarCount = \App\UserCarPack::where('pack_id', $userPack->id)->count();
        if($totalCarCount >= $userPack->car_activation_allowed){
            //return errorResponse('Max car count exceeded');
            alert()->error('Max car count exceeded', 'User Car');
            return redirect()->back();
        }
        
        $drupal_user_id = $userPack->drupal_user_id;
        $car = \App\NewUserCar::firstOrCreate([
            'drupal_user_id' => $drupal_user_id,            
            'type' => $request->type,
            'country_id' => $request->country_id,
            'emirate_id' => !empty($request->emirate_id) ? $request->emirate_id : null,                
            'plate_prefix' => $request->plate_prefix,
            'plate_number' => $request->plate_number,
        ]);
        $car->user_id = $userPack->user_id;
        $car->save();

        $user = \App\User::find($userPack->user_id);
        $pack = \App\Pack::where('id',$userPack->id)->first();
        \App\UserCarPack::firstOrCreate([
            'pack_id' => $pack->id,
            'user_car_id' => $car->id
        ]);
        $agree_offers = ($user->recieve_updates) ? 'True' : 'False';
        //$this->vipSubmissionToSalesForce($user,$pack,$agree_offers,'web','add_car_web_cms');

         $arrayData = array("audit_type"=>'create-car','table_id'=>$car->user_id,'table_name'=>'new_user_cars',"admin_id"=>Auth::guard('admin')->user()->id,"admin_type"=>Auth::guard('admin')->user()->type,"event"=>"User car created","old_values"=>"","new_values"=>$car,"url"=>"");
         manageAuditLogs($arrayData);

        $this->syncUsertoDesigna($userPack->upid);
        $configurations = \App\ApiConfiguration::all()->keyBy('name');
        $this->savecode($userPack,$configurations);
        alert()->success('User Car', 'Created!');
        return redirect($request->referer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $ucar = \App\NewUserCar::findOrFail($id);
        $car_countries = CarCountry::select('id', "name_en as name")->pluck('name', 'id')->prepend('Select', '');
        $emirates = ['' => 'Select'];
        $pprefixes = ['' => 'Select'];
        if (!empty($ucar->country_id)) {
            $emirates = Emirate::select('id', "name_en as name")
                            ->where('country_id', $ucar->country_id)
                            ->pluck('name', 'id')->prepend('Select', '');
            $creq = new \StdClass;
            if (!empty($ucar->emirate_id)) {
                $creq->type = 'city';
                $creq->id = $ucar->emirate_id;
            } else {
                $creq->type = 'country';
                $creq->id = $ucar->country_id;
            }
            $pprefixes = ['' => 'Select'] + json_decode(getPprefixes($creq), true);
        }
        return view('admin.user_cars.edit', compact('ucar', 'car_countries', 'emirates', 'pprefixes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'country_id' => 'required',
            //'emirate_id' => 'required',
            'plate_prefix' => 'required',
            'plate_number' => 'required',
        ]);

        $userPack = Pack::select('packs.id', 'packs.prefix', 'packs.pack_number', 'packs.sale_id', 'users.id as user_id', 'users.first_name', 'users.last_name', 'users.email', 'users.mobile', 'users.mobile_dial_code', 'users.gender', 'users.dob', 'users.nationality_id', 'users.recieve_updates', 'packs.car_activation_allowed','users.drupal_user_id')
                        ->join('user_packs', 'user_packs.pack_id', 'packs.id')
                        ->join('users', 'users.id', 'user_packs.user_id')
                        ->where('packs.id', $request->pack_id)->first();

        if (empty($userPack)) {
            alert()->error('Not found', 'Pack not exists or activated');
            return redirect($request->referer);
        }
        $car = NewUserCar::where('id', $id)->first();
        $sameCarOtherPackExists = \App\UserCarPack::where('user_car_id',$id)->where('pack_id','!=',$userPack->id)->first();
        $userCarPack = \App\UserCarPack::where('user_car_id', $id)->where('pack_id', $userPack->id)->first();
        if(!empty($sameCarOtherPackExists)){
            $car = new \App\NewUserCar();
            $car->drupal_user_id = $userPack->drupal_user_id;
            $car->user_id = $userPack->user_id;
            $designa_card_uid = NULL;
        }else{            
            $designa_card_uid = $userCarPack->designa_card_uid;
        }
        
        $oldCar = NewUserCar::where('id', $id)->first();
        $saveCode = false;
        if (!empty($designa_card_uid)) {
            $syncRequest = new Request([
                'countryCode' => CarCountry::where('id', $request->country_id)->first()->code,
                'cityCode' => !empty($request->emirate_id) ? Emirate::where('id', $request->emirate_id)->first()->code : null,
                'plate_prefix' => $request->plate_prefix,
                'plate_number' => $request->plate_number,
            ]);
            if ($this->syncCar($id, $syncRequest,$userCarPack)) {
                $saveCode = true;
            } else {
                alert()->error('Designa Failed', 'Not found in designa!');
                return redirect($request->referer);
            }
        }

        $car->type = $request->type;
        $car->country_id = $request->country_id;
        $car->emirate_id = !empty($request->emirate_id) ? $request->emirate_id : null;
        $car->plate_prefix = $request->plate_prefix;
        $car->plate_number = $request->plate_number;
        $car->save();

        $userCarPack->user_car_id = $car->id;
        $userCarPack->save();
        //mail
        /*$data = array();
        $data['subject'] = 'Car Details Updation';


        $data['carCountry'] = \App\CarCountry::where('id', $car->country_id)->first()->name_en;
        $data['carEmirate'] = \App\Emirate::where('id', $car->emirate_id)->first()->name_en;
        $data['platePrefix'] = $car->plate_prefix;
        $data['plateNumber'] = $car->plate_number;

        $user = \App\User::where('id', $car->user_id)->first();
        $data['lang'] = 'en';

        if(mb_ereg('[\x{0600}-\x{06FF}]', $user->first_name)) // arabic range
        //if(mb_ereg('[\x{0590}-\x{05FF}]', $text)) // hebrew range
        {
            $data['lang'] = 'ar';
        }


        try {
            Mail::to($user->email)
                    ->send(new CarMail($user, $data));
        } catch (\Exception $e) {
            //echo $e->getMessage();
        }*/
        //mail

        $user = \App\User::find($userPack->user_id);
        $pack = \App\Pack::where('id',$userPack->id)->first();
        $agree_offers = ($user->recieve_updates) ? 'True' : 'False';
       // $this->vipSubmissionToSalesForce($user,$pack,$agree_offers,'web','edit_car_web_cms');

        $arrayData = array("audit_type"=>'edit-car','table_id'=>$car->user_id,'table_name'=>'new_user_cars',"admin_id"=>Auth::guard('admin')->user()->id,"admin_type"=>Auth::guard('admin')->user()->type,"event"=>"User car updated","old_values"=>$oldCar,"new_values"=>$car,"url"=>"");
         manageAuditLogs($arrayData);
        if ($saveCode) {
            $configurations = \App\ApiConfiguration::all()->keyBy('name');
            $this->savecode($userPack, $configurations);
        }
        alert()->success('User Car', 'Updated!');
        return redirect($request->referer);
    }

}
