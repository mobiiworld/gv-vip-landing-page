<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Yajra\Datatables\Datatables;
use App\VehiclePlocation;
use App\Traits\ImageTraits;
use App\Traits\VehicleCountTraits;
use App\VehiclePlocationTiming;
use App\VehiclePlocationService;

class VehiclePlocationController extends Controller {

    use ImageTraits,
        VehicleCountTraits;

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('vehicle count_read', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['index', 'show']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('vehicle count_create', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['create', 'store']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('vehicle count_update', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['edit', 'update', 'getVinside', 'postVinside']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $configurations = \App\ApiConfiguration::where('name', 'clear_user_parkings_cron')->first();
        return view('admin.vehicle_pl.index', compact('configurations'));
    }

    public function datatable(Request $request) {
        $vpls = VehiclePlocation::select('*')->orderBy('display_order');
        return Datatables::of($vpls)
                        ->rawColumns(['actions', 'status', 'closed'])
                        ->editColumn('actions', function ($vpl) {
                            $b = ' <a class="btn btn-primary btn-sm " href="' . route('admin.vehicle-plocations.edit', $vpl->id) . '">Edit</a>';
                            return $b;
                        })
                        ->editColumn('status', function ($vpl) {
                            $checked = ($vpl->status == 1) ? 'checked' : '';
                            $st = 'status';
                            return '<input type="checkbox" name="status" id="status_' . $vpl->id . '"  data-on="Enabled" data-off="Disabled" value="1" ' . $checked . ' data-st="' . $st . '" class="bsSwitch" autocomplete="off" data-id="' . $vpl->id . '">';
                        })
                        ->editColumn('closed', function ($vpl) {
                            $checked = ($vpl->closed == 0) ? 'checked' : '';
                            $st = 'closed';
                            return '<input type="checkbox" name="status" id="status_' . $vpl->id . '"  data-on="Open" data-off="Closed" value="1" ' . $checked . ' data-st="' . $st . '" class="bsSwitch" autocomplete="off" data-id="' . $vpl->id . '">';
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $day_array = [1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday'];
        $services = \App\ParkingServices::select('id', 'names->en as name')->get();
        return view('admin.vehicle_pl.create', compact('day_array', 'services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'is_custom' => 'required',
            'vgs_id' => 'required_if:is_custom,0',
            'total_capacity' => 'required|integer',
            'beco_name' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'zone_names.*' => 'required',
            'names.*' => 'required',
            'descriptions.*' => 'required',
                ], ['required' => 'This is required']);
        $undefinedExists = VehiclePlocation::where('is_undefined_location', 1)->count();
        if($undefinedExists > 0 && $request->has('is_undefined_location')) {
            alert()->error('Undefined location already exists.', 'Error!');
            return redirect()->back()->withInput();
        }
        $maxIndex = VehiclePlocation::max('display_order');
        $maxIndex = ($maxIndex) ? $maxIndex + 1 : 1;

        $location = new VehiclePlocation();
        $location->is_custom = $request->is_custom;
        $location->vgs_id = $request->vgs_id;
        $location->zone_names = $request->zone_names;
        $location->names = $request->names;
        $location->descriptions = $request->descriptions;
        $location->total_capacity = $request->total_capacity;
        $location->beco_name = $request->beco_name;
        $location->latitude = $request->latitude;
        $location->longitude = $request->longitude;
        $location->is_undefined_location = $request->has('is_undefined_location') ? 1 : 0;
        $location->extras = [
            "pay_park" => isset($request->extras['pay_park']) ? true : false,
            "car_wash" => (isset($request->extras['car_wash']) && (!$request->has('is_undefined_location'))) ? true : false,
            "directions" => isset($request->extras['directions']) ? true : false
        ];
        if (!empty($request->area_cordinates_map)) {
            if($request->has('is_undefined_location')){
                $location->polygons = NULL;
            }else{
                $polygon_values = $this->setPolygon($request->area_cordinates_map);
                $location->polygons = \DB::raw('ST_GeomFromText(\'POLYGON((' . $polygon_values . '))\')');
            }
            
        }
        $location->display_order = $maxIndex;
        $location->save();

        if ($request->file('images') != null) {
            $files = $request->file('images');
            foreach ($files as $lang => $imgs) {
                foreach ($imgs as $key => $file) {
                    $relPath = 'vpl/';
                    $pimages = new \App\VehiclePimage();
                    $pimages->plocation_id = $location->id;
                    $pimages->image = $this->resize($file, $relPath);
                    $pimages->language = $lang;
                    $pimages->display_order = $key + 1;
                    $pimages->save();
                }
            }
        }
        $start_time = $request->start_time;
        $end_time = $request->end_time;
        $is_next_day = $request->is_next_day;
        if (!empty($start_time)) {
            foreach ($start_time as $key => $_start_time) {
                $starttime = date('H:i:s', strtotime($_start_time));
                $endtime = date('H:i:s', strtotime($end_time[$key]));
                $timing = new VehiclePlocationTiming();
                $timing->vehicle_plocation_id = $location->id;
                $timing->day = $key;
                $timing->start_time = $starttime;
                $timing->end_time = $endtime;
                $timing->is_next_day = isset($is_next_day[$key]) ? 1 : 0;
                $timing->save();
            }
        }

        if ((!empty($request->services))  && (!$request->has('is_undefined_location'))) {
            foreach ($request->services as $key => $service) {
                $vService = new VehiclePlocationService();
                $vService->vehicle_plocation_id = $location->id;
                $vService->parking_service_id = $key;
                $vService->beco_name = $request->bename[$key];
                $vService->descriptions = $request->sdescriptions[$key];
                $vService->save();
            }
        }

        alert()->success('Vehicle Parking Location', 'added!');

        return redirect()->route('admin.vehicle-plocations.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $vpl = VehiclePlocation::select('*', \DB::raw('ST_AsGeoJSON(polygons) as polygons'))->whereId($id)->first();
        $mapCordinates = !empty($vpl->polygons) ? json_encode(json_decode($vpl->polygons, true)['coordinates'][0]) : json_encode([]);
        $vplservs = VehiclePlocationService::select('id', 'parking_service_id', 'beco_name', 'descriptions')->where('vehicle_plocation_id', $id)->get();
        $vpl->services = $vplservs->pluck('id', 'parking_service_id');
        $vpl->bename = $vplservs->pluck('beco_name', 'parking_service_id');
        $vpl->sdescriptions = $vplservs->pluck('descriptions', 'parking_service_id');
        $day_array = [1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday'];
        $services = \App\ParkingServices::select('id', 'names->en as name')->get();
        return view('admin.vehicle_pl.edit', compact('vpl', 'day_array', 'services', 'mapCordinates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'is_custom' => 'required',
            'vgs_id' => 'required_if:is_custom,0',
            'total_capacity' => 'required|integer',
            'beco_name' => 'required',
            'display_order' => 'required|integer',
            'latitude' => 'required',
            'longitude' => 'required',
            'zone_names.*' => 'required',
            'names.*' => 'required',
            'descriptions.*' => 'required',
                ], ['required' => 'This is required', 'vgs_id.required_if' => 'This required for API updates.']);

        $undefinedExists = VehiclePlocation::where('is_undefined_location', 1)->where("id","!=",$id)->count();
        if($undefinedExists > 0 && $request->has('is_undefined_location')) {
            alert()->error('Undefined location already exists.', 'Error!');
            return redirect()->back()->withInput();
        }
        $location = VehiclePlocation::findOrFail($id);
        $location->is_custom = $request->is_custom;
        $location->vgs_id = $request->vgs_id;
        $location->zone_names = $request->zone_names;
        $location->names = $request->names;
        $location->descriptions = $request->descriptions;
        $location->total_capacity = $request->total_capacity;
        $location->beco_name = $request->beco_name;
        $location->latitude = $request->latitude;
        $location->longitude = $request->longitude;
        $location->is_undefined_location = $request->has('is_undefined_location') ? 1 : 0;
        $location->extras = [
            "pay_park" => isset($request->extras['pay_park']) ? true : false,
            "car_wash" => (isset($request->extras['car_wash']) && (!$request->has('is_undefined_location'))) ? true : false,
            "directions" => isset($request->extras['directions']) ? true : false
        ];
        if (!empty($request->area_cordinates_map) ) {
            if($request->has('is_undefined_location')){
                $location->polygons = NULL;
            }else{
                $polygon_values = $this->setPolygon($request->area_cordinates_map);
                $location->polygons = \DB::raw('ST_GeomFromText(\'POLYGON((' . $polygon_values . '))\')');
            }
        }
        $location->display_order = $request->display_order;
        $location->save();
        if ($request->file('images') != null) {
            $files = $request->file('images');
            foreach ($files as $lang => $imgs) {
                $maxIndex = \App\VehiclePimage::where('plocation_id', $location->id)->where('language', $lang)->max('display_order');
                $maxIndex = ($maxIndex) ? $maxIndex : 0;
                foreach ($imgs as $key => $file) {
                    $maxIndex++;
                    $relPath = 'vpl/';
                    $pimages = new \App\VehiclePimage();
                    $pimages->plocation_id = $location->id;
                    $pimages->image = $this->resize($file, $relPath);
                    $pimages->language = $lang;
                    $pimages->display_order = $maxIndex++;
                    $pimages->save();
                }
            }
        }
        $start_time = $request->start_time;
        $end_time = $request->end_time;
        $is_next_day = $request->is_next_day;
        VehiclePlocationTiming::where('vehicle_plocation_id', $id)->delete();
        if (!empty($start_time)) {
            foreach ($start_time as $key => $_start_time) {
                $starttime = date('H:i:s', strtotime($_start_time));
                $endtime = date('H:i:s', strtotime($end_time[$key]));
                $timing = new VehiclePlocationTiming();
                $timing->vehicle_plocation_id = $id;
                $timing->day = $key;
                $timing->start_time = $starttime;
                $timing->end_time = $endtime;
                $timing->is_next_day = isset($is_next_day[$key]) ? 1 : 0;
                $timing->save();
            }
        }
        VehiclePlocationService::where('vehicle_plocation_id', $id)->delete();
        if ((!empty($request->services))  && (!$request->has('is_undefined_location'))) {
            foreach ($request->services as $key => $service) {
                $vService = new VehiclePlocationService();
                $vService->vehicle_plocation_id = $id;
                $vService->parking_service_id = $key;
                $vService->beco_name = $request->bename[$key];
                $vService->descriptions = $request->sdescriptions[$key];
                $vService->save();
            }
        }
        alert()->success('Vehicle Parking Location', 'updated!');
        return redirect()->route('admin.vehicle-plocations.index');
    }

    public function changeStatus(Request $request) {
        $season = VehiclePlocation::find($request->id);
        switch ($request->st) {
            case 'status':
                $season->status = ($season->status == 1) ? 0 : 1;
                break;
            case 'closed':
                $season->closed = ($season->closed == 1) ? 0 : 1;
                break;
        }
        $season->save();
        return response()->json(["status" => 'success', "message" => 'Successfully changed']);
    }

    public function delImage($imageId) {
        $img = \App\VehiclePimage::where('id', $imageId)->first();
        \Storage::disk('azure')->delete('vpl/' . $img->getOriginal('image'));
        $img->delete();
        alert()->success('Successfully deleted.', 'Updated');
        return redirect()->back();
    }

    public function getVinside() {
        $locations = VehiclePlocation::select('id', 'zone_names->en as zone_name', 'names->en as name', 'total_capacity', 'inside')
                        ->where('is_custom', 1)->orderBy('display_order')->get();
        return view('admin.vehicle_pl.v_inside', compact('locations'));
    }

    public function postVinside(Request $request) {
        foreach ($request->insides as $k => $v) {
            VehiclePlocation::where('id', $k)->update(['inside' => $v]);
        }
        alert()->success('Successfully Updated.', 'Updated');
        return redirect()->back();
    }

    public function syncCount() {
        $vps = \App\VehiclePsetting::first();
        if ($vps) {
            $vpCounts = $this->parkingDetails();
            if (!empty($vpCounts)) {
                $vps->api_data = $vpCounts;
                $vps->last_updated_at = date('Y-m-d H:i:s');
                $vps->save();
                return response()->json(["status" => true, "message" => 'Synced successfully.']);
            } else {
                return response()->json(["status" => false, "message" => 'No response from api.']);
            }
        }
    }

    public function clearUserParkingsCronStatus() {
        $configurations = \App\ApiConfiguration::where('name', 'clear_user_parkings_cron')->first();
        if (($configurations)) {
            $oldValue = $configurations->value;
            $configurations->value = ($configurations->value == "enable") ? "disable" : "enable";
            $configurations->save();
            return response()->json(["status" => true, "old_state" => strtoupper($oldValue), "current_state" => strtoupper($configurations->value . 'D')]);
        } else {
            return response()->json(["status" => false, "message" => 'No such config exists.']);
        }
    }

    public function clearUserParkings() {
        $user = Auth::guard('admin')->user();
        if ($user->hasRole('super-admin', 'admin')) {
            $totalrecords = \App\SaveMyParking::count();
            \DB::table('save_my_parkings')->truncate();
            echo $totalrecords . ' rows deleted';
        } else {
            echo 'Your don\'t have permission to acces this page.', 'No Access!';
        }
    }

    function setPolygon($string) {
        $pArray = json_decode($string, true);
        $nar = [];
        foreach ($pArray as $cord) {
            array_push($nar, implode(' ', $cord));
        }
        array_push($nar, $pArray[0]['lat'] . ' ' . $pArray[0]['lng']);
        return join(',', $nar);
    }

    public function parkingArea() {
        $parea = \App\VehicleParea::select(
                        'id',
                        'latitude',
                        'longitude',
                        \DB::raw('ST_AsGeoJSON(in_area) as cordinates'),
                )->first();
        if ($parea) {
            if (!empty($parea->cordinates)) {
                $cordinates = json_decode($parea->cordinates, true);
                $mapCordinates = json_encode($cordinates['coordinates'][0]);
            } else {
                $mapCordinates = json_encode([]);
            }
        } else {
            $mapCordinates = json_encode([]);
        }
        return view('admin.vehicle_pl.parea', compact('parea', 'mapCordinates'));
    }

    public function saveParkingArea(Request $request) {
        $this->validate($request, [
            'latitude' => 'required',
            'longitude' => 'required',
                ], ['required' => 'This is required', 'vgs_id.required_if' => 'This required for API updates.']);

        $area = \App\VehicleParea::first();
        if (empty($area)) {
            $area = new \App\VehicleParea();
        }
        $area->latitude = $request->latitude;
        $area->longitude = $request->longitude;
        if (!empty($request->area_cordinates_map)) {
            $polygon_values = $this->setPolygon($request->area_cordinates_map);
            $area->in_area = \DB::raw('ST_GeomFromText(\'POLYGON((' . $polygon_values . '))\')');
        }
        $area->save();
        alert()->success('Vehicle Parking Area', 'updated!');
        return redirect()->route('admin.parking.area');
    }

}
