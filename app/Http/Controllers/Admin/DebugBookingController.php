<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DebugBookingController extends Controller
{
    
    public function index(Request $request){
    	set_time_limit(0);
    	$select = 'SELECT book_slots.id,book_slots.created_at ,book_slots.program_date,book_slots.email,book_slots.created_at,sale_code,ticket_ids, (SELECT COUNT(*) FROM program_slots WHERE program_slots.booking_id = book_slots.id) AS slotcnt,(CHAR_LENGTH(program_slot_id) - CHAR_LENGTH(REPLACE(program_slot_id, \',\', \'\')) + 1) as bookedPslots FROM book_slots WHERE book_slots.status = "order_confirmed"';
    	if($request->booking_date != ''){
    		$select .= ' AND  book_slots.program_date = "'.$request->booking_date.'" ';    		
    	}
    	if($request->created_date != ''){
    		$select .= ' AND  DATE(book_slots.created_at) >= "'.$request->created_date.'" ';    		
    	}
    	$select .= '  order by book_slots.created_at desc';
    	$results = \DB::select($select);
    	//dd($results);
    	return view('admin.debug_booking.index',compact('results'));
    }

    public function salecodeMissing(Request $request){
    	set_time_limit(0);
    	$booking_query = 'SELECT sale_code FROM book_slots where sale_code IS NOT NULL ';
    	if($request->created_date != ''){
    		$booking_query .= ' AND  DATE(created_at) >= "'.$request->created_date.'" ';    		
    	}
    	$booking_results = \DB::select($booking_query);
    	
    	$sale_code_list = NULL;
    	if(!empty($booking_results)){
    		$sale_code_list = (!empty($booking_results)) ? join(",",array_column($booking_results, 'sale_code')) : NULL;
    	}
    	

    	$main_product_codes = (env('APP_ENV') == 'production') ? 
    	"'CF2E7736-485A-9B86-1784-0186BD863CEF','9745DE99-6C12-5A8C-07C0-0186BD8281E0','E4E5E8CC-2B3D-09EF-1997-0186BD869922','8DBCED03-3F96-4633-1560-0186BD85D844','8DBCED03-3F96-4633-1560-0186BD85D844','2EC00571-7EB0-6F7B-31B5-0186BD899AA1','BA4F04EB-AD11-6ECE-1F74-0186BD873CE0','499CF5C6-EAFE-7C78-1F12-0186BD86EC1A','9A544075-7BD7-83A3-2099-0186BD87ACA4','A337F5BE-234D-7D27-21D7-0186BD880400','46A22F58-63CE-E47F-3673-0186BD8A85EE','7990DD1B-F81F-3935-3509-0186BD8A382B','79EFEE3D-26A1-373C-23B1-0186BD88543F','43DB7884-6B07-B127-2A22-0186BD889EE5','62930158-437F-31CE-2ED2-0186BD88F025','21E730DB-1BAB-90F6-3030-0186BD894744','8768140D-0BF7-4DDA-3715-0186BD8B280B','BCD04A95-5FCB-7C45-36C5-0186BD8AD88E','A6B818A7-A876-DF25-3395-0186BD89EDC9'" 
    	:
    	 "'33695052-C66A-5D63-1920-01869D1582BC','D55D560A-C761-5ECC-18A0-01869D12D1FC','F979A55B-26E2-1351-1942-01869D161A04','804DF15F-358E-C63A-18FE-01869D1495CE','3671F302-02A5-392A-1B8E-01869D1DE174','05CFF99F-9345-8985-1B4D-01869D1D72F6','9450A750-456A-A187-1A83-01869D199BFE','8E5FA700-E1B2-E384-1981-01869D18D9D7','9450A750-456A-A187-1A83-01869D199BFE','9E2BD7FE-A0B2-24C4-1A69-01869D1946D3','BC1F2DBA-5A06-3582-1BDA-01869D1E7B75','C05B15AC-6C14-E6A4-1BC2-01869D1E4871','895D49A1-DD90-6411-1AF3-01869D1ACFE4','BFB1A0E1-F9F6-0061-1ABF-01869D1A3BFC','84521531-1317-87C1-1B0D-01869D1B149D','715E22CE-3B74-3586-1AD9-01869D1A8007','43FC1EAA-5CEB-D9B5-1C22-01869D1EFF2A','15CB15AC-E914-6A72-1C3B-01869D1F2DEC'";

    	$select = 'SELECT ct.salecode,st.shopcartid,st.majlisBookingRefId,mt.majlis_ticket_id,mt.created_date,IF(FIND_IN_SET(ct.salecode,"'.$sale_code_list.'"), "exists", "not exists") as salecode_present,mt.email,mt.mainproductname FROM gv_shopcart_transaction as st JOIN gv_checkout_transaction as ct ON ct.shopcartid = st.shopcartid JOIN gv_media_transaction as mt ON mt.transactionid = ct.transactionid WHERE mt.mainproductid IN ('.$main_product_codes.')';
    	if($request->created_date != ''){
    		$select .= ' AND  DATE(mt.created_date) >= "'.$request->created_date.'" ';    		
    	}
    	if($request->sale_code != ''){
    		$select .= ' AND  ct.sale_code = "'.$request->sale_code.'" ';    		
    	}
    	$select .= '  order by mt.created_date desc';

    	$results = \DB::Connection('mysql2')->select($select);

    	return view('admin.debug_booking.salecode',compact('results'));
    	
    }
}
