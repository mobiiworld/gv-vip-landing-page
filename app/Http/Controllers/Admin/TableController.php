<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\VipTable;
use Auth;
use URL;
//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
//Enables us to output flash messaging
use Session;
use Image;

class TableController extends Controller {

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Ramadan Majlis_table read', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['index']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Ramadan Majlis_table create', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['create', 'store']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Ramadan Majlis_table update', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['edit', 'update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //Get all slots and pass it to the view
        $types = config('globalconstants.types');
        return view('admin.tables.index', compact('types'));
    }

    /**
     * To display dynamic table by datatable
     *
     * @return mixed
     */
    public function datatable(Request $request) {
        $currentUser = Auth::guard('admin')->user();
        $isSuperAdmin = $currentUser->hasRole('super-admin', 'admin');
        $tables = VipTable::select('name', 'table_number', 'type', 'status', 'seat_type', 'smoking_status', 'id')
                ->orderby('seat_type', 'asc')
                ->orderby('table_number', 'asc')
                ->orderby('type', 'asc');

        if ($request->seat_type != 0) {
            $tables->where('seat_type', $request->seat_type);
        }
        if ($request->type != '') {
            $tables->where('type', $request->type);
        }
        if ($request->smoking_status != 0) {
            $tables->where('smoking_status', $request->smoking_status);
        }

        return Datatables::of($tables)
                        ->rawColumns(['actions', 'status'])
                        ->editColumn('actions', function ($tables) {

                            $b = '<a href="' . URL::route('admin.tables.edit', $tables->id) . '" class="btn btn-primary "><i class="fa fa-edit"></i> Edit </a>';
                            return $b;
                        })
                        ->editColumn('smoking_status', function ($tables) {

                            $smStatus = ($tables->smoking_status == 1) ? 'Smoking' : 'Non Smoking';
                            return $smStatus;
                        })
                        ->editColumn('status', function ($tables) use ($currentUser, $isSuperAdmin) {
                            if ($isSuperAdmin || $currentUser->hasPermissionTo('Ramadan Majlis_table update', 'admin')) {
                                $checked = ($tables->status == 1) ? 'checked' : '';
                                return '<input type="checkbox" name="status" id="status_' . $tables->id . '"  data-on-text="Enabled" data-off-text="Disabled" value="1" ' . $checked . ' class="bsSwitch" autocomplete="off" data-id="' . $tables->id . '">';
                            } else {
                                return ($tables->status == 1) ? '<label class="label label-success">Enabled</label>' : '<label class="label label-danger">Disabled</label>';
                            }
                        })
                        ->make(true);
    }

    public function changeStatus(Request $request) {
        $tables = VipTable::find($request->id);
        $tables->status = ($tables->status == 0) ? 1 : 0;
        $tables->save();
        return response()->json(["status" => true, "message" => 'Successfully changed']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $types = config('globalconstants.types');

        return view('admin.tables.create', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:120|unique:vip_tables',
            'type' => 'required'
        ]);

        $tables = new VipTable;
        $tables->name = $request->name;
        $tables->type = $request->type;
        $tables->smoking_status = $request->smoking_status;
        $tables->seat_type = $request->seat_type;
        $tables->table_number = ($request->table_number != null) ? $request->table_number : 1;
        //$tables->is_hourly = ($request->is_hourly) ? 1 : 0;
        $tables->admin_only = ($request->admin_only) ? 1 : 0;
        $tables->save();

        //Redirect to the tables.index view and display message
        alert()->success('Table successfully added.', 'Added');
        return redirect()->route('admin.tables.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return redirect('tables');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $types = config('globalconstants.types');
        $smoking_types = config('globalconstants.smoking_types');
        $seat_types = config('globalconstants.seat_types');

        $tables = VipTable::findOrFail($id); //Get user with specified id

        return view('admin.tables.edit', compact('tables', 'types', 'smoking_types', 'seat_types')); //pass user and roles data to view
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $user = VipTable::findOrFail($id);
        //Get role specified by id
        //Validate name, email and password fields  
        $this->validate($request, [
            'name' => 'required|max:120|unique:vip_tables,name,' . $id,
            'type' => 'required',
        ]);

        $tables = VipTable::find($id);
        $tables->name = $request->name;
        $tables->type = $request->type;
        $tables->smoking_status = $request->smoking_status;
        $tables->seat_type = $request->seat_type;
        $tables->table_number = ($request->table_number != null) ? $request->table_number : 1;
        //$tables->is_hourly = ($request->is_hourly) ? 1 : 0;
        $tables->admin_only = ($request->admin_only) ? 1 : 0;
        $tables->save();

        alert()->success('Table details successfully updated.', 'Updated');
        return redirect()->route('admin.tables.index');
    }

    public function getTablesByType(Request $request) {
        $tables = \App\VipTable::select('id', 'name', 'seat_type')
                ->where('status', 1)
                ->orderby('seat_type', 'asc')
                ->orderby('table_number', 'asc')
                ->where('type', $request->type)
                ->where('smoking_status', $request->sType)
                ->get();
        return response()->json(["status" => true, "data" => [
                        'table2' => $tables->where('seat_type', 2),
                        'table4' => $tables->where('seat_type', 4),
                        'table6' => $tables->where('seat_type', 6),
        ]]);
    }

}
