<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Auth;
use URL;

class PackProductConfigController extends Controller
{
    public function index()
    {
        return view('admin.pack_product.index');
    }

    public function datatable(Request $request) {

        $prod = \App\VgsProductConfig::select('*');
        
        return Datatables::of($prod)
                        ->rawColumns(['actions','status'])
                        ->editColumn('actions', function ($prod) {
                            
                            $b = ' <a class="btn btn-danger btn-sm destroy" href="' . route('admin.pack-product-config.destroy', $prod->id) . '">Delete</a>';
                            
                            return $b;
                        })
                        
                        ->make(true);
    }

    public function create()
    {
    	$pack_product_slug = config('globalconstants.pack_product_slug');
        return view('admin.pack_product.create',compact('pack_product_slug'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'slug' => 'required',   
            'product' => 'required',   
            'product_name' => 'required',   
            'product_id' => 'required|unique:vgs_product_configs',
        ]);
        
        
        $prduct = new \App\VgsProductConfig;
		$prduct->slug = $request->slug;
		$prduct->product = $request->product;
		$prduct->product_name = $request->product_name;
		$prduct->product_id = $request->product_id;
		$prduct->save();
        alert()->success('Product Id successfully added.', 'Added');
        return redirect()->route('admin.pack-product-config.index');
    }
    public function destroy($id)
    {
        \App\VgsProductConfig::destroy($id);
    }
}
