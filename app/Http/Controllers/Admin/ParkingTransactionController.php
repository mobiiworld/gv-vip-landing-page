<?php

namespace App\Http\Controllers\Admin;

use App\AuditLog;
use App\Http\Controllers\Controller;
use App\UserPaymentSessions;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Auth;
use DB;
use Validator;

class ParkingTransactionController extends Controller {

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('parking transaction_read', 'admin')) {
                return $next($request);
            } else {
                alert()->error('You don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        });

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('parking transaction_update', 'admin')) {
                return $next($request);
            } else {
                alert()->error('You don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['edit', 'update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $parkingTransactionStatus = ['' => 'All'] + config('globalconstants.parking_transaction_status');
        $total = DB::Connection('mysql2')
                ->table('gv_user_payment_sessions')
                ->count();
        return view('admin.parking_transaction.index', compact('parkingTransactionStatus','total'));
    }

    /**
     * To display dynamic table by datatable
     *
     * @return mixed
     */
    public function datatable(Request $request) {
        $parkingTransactionStatus = config('globalconstants.parking_transaction_status');

        $transaction = DB::Connection('mysql2')
                ->table('gv_user_payment_sessions')
                ->leftJoin('gv_user_payments', 'gv_user_payment_sessions.id', '=', 'gv_user_payments.sessionid')
                ->leftJoin('gv_parking_details', 'gv_user_payment_sessions.shopcartid', '=', 'gv_parking_details.pmstring')
                ->leftJoin('gv_shopcart_transaction', 'gv_user_payment_sessions.shopcartid', '=', 'gv_shopcart_transaction.shopcartid')
                ->select(
                'gv_user_payment_sessions.id',
                'gv_user_payment_sessions.platform',
                'gv_user_payment_sessions.shopcartid',
                'gv_user_payment_sessions.amount',
                'gv_user_payment_sessions.status',
                'gv_user_payment_sessions.created_date',
                'gv_user_payments.order_id',
                'gv_user_payments.receipt',
                'gv_parking_details.ticketNumber',
                'gv_parking_details.platePrefix',
                'gv_parking_details.plateNumber',
                'gv_parking_details.countryCode',
                'gv_parking_details.cityCode',
                'gv_shopcart_transaction.email'
        );

        $transaction = $transaction->where('gv_user_payment_sessions.payment_category', 'parkingId');

        $season = \App\Season::where('status',1)->first();
        $season_number = (isset($season->season_number)) ? $season->season_number : 28;
        $transaction->where('gv_user_payment_sessions.season_number',$season_number);

        if (!empty($request->transaction_status)) {
            $transaction = $transaction->where('gv_user_payment_sessions.status', $request->transaction_status);
        }
        if (!empty($request->platform)) {
            $transaction = $transaction->where('gv_user_payment_sessions.platform', $request->platform);
        }
        if (!empty($request->start_date)) {
            $start_date = date('Y-m-d', strtotime($request->start_date));
            $transaction = $transaction->whereDate('gv_user_payment_sessions.created_date', '>=', $start_date);
        }
        if (!empty($request->end_date)) {
            $end_date = date('Y-m-d', strtotime($request->end_date));
            $transaction = $transaction->whereDate('gv_user_payment_sessions.created_date', '<=', $end_date);
        }
        $transaction = $transaction->orderBy('gv_user_payment_sessions.created_date', 'desc');

        return Datatables::of($transaction)
                        ->rawColumns(['status', 'action'])
                        /* ->editColumn('action', function ($transaction) {
                          $b = '';
                          return $b;
                          }) */
                        ->editColumn('created_date', function ($transaction) {
                            return (!empty($transaction->created_date)) ? date('F d, Y h:ia', strtotime($transaction->created_date)) : '';
                        })->editColumn('status', function ($transaction) use($parkingTransactionStatus) {
                            return !empty($transaction->status) ? $parkingTransactionStatus[$transaction->status] : '';
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

    }

}
