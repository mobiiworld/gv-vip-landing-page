<?php

namespace App\Http\Controllers\Admin;

use App\SnwCampaign;
use App\SnwPrize;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Auth;

class SnwPrizeController extends Controller {

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('shake and win_read', 'admin')) {
                return $next($request);
            } else {
                alert()->error('You don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['index', 'show']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('shake and win_create', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['create', 'store']]);

        $this->middleware(
                function ($request, $next) {
                    $user = Auth::guard('admin')->user();
                    if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('shake and win_update', 'admin')) {
                        return $next($request);
                    } else {
                        alert()->error('You don\'t have permission to acces this page.', 'No Access!');
                        return redirect()->route('admin.dashboard');
                    }
                }, ['only' => ['edit', 'update']]
        );
    }

    public function index(Request $request) {
        if ($request->has('campaign')) {
            $campaign = SnwCampaign::select('id', 'names->en as name')->where('id', $request->campaign)->first();
            return view('admin.snw.prizes.index', compact('campaign'));
        }
        return redirect()->back();
    }

    public function datatable(Request $request) {
        $currentUser = Auth::guard('admin')->user();
        $isSuperAdmin = $currentUser->hasRole('super-admin', 'admin');
        $datas = SnwPrize::select('*');

        return Datatables::of($datas)
                        ->rawColumns(['actions', 'status'])
                        ->editColumn('actions', function ($data) use ($currentUser, $isSuperAdmin) {
                            $b = '';
                            if ($isSuperAdmin || $currentUser->hasPermissionTo('shake and win_update', 'admin')) {
                                $b .= '<a class="btn btn-primary btn-sm " href="' . route('admin.snw-prizes.edit', $data->id) . '">Edit</a>';
                            }
                            return $b;
                        })
                        ->addColumn('totalCoupons', function ($data) {
                            return $data->coupons()->count();
                        })
                        ->addColumn('usedCoupons', function ($data) {
                            return $data->coupons()->where('status', 1)->count();
                        })
                        ->editColumn('status', function ($data) {
                            $checked = ($data->status == 1) ? 'checked' : '';
                            return '<input type="checkbox" name="status" id="status_' . $data->id . '"  data-on-text="Enabled" data-off-text="Disabled" ' . $checked . ' class="bsSwitch" autocomplete="off" data-id="' . $data->id . '">';
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        if ($request->has('campaign')) {
            $campaign = SnwCampaign::select('id', 'names->en as name')->where('id', $request->campaign)->first();
            return view('admin.snw.prizes.create', compact('campaign'));
        }
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'names.*' => 'required',
            'descriptions.*' => 'required',
            'eloqua_form' => 'required'
        ]);

        $data = new SnwPrize();
        $data->campaign_id = $request->campaign;
        $data->names = $request->names;
        $data->descriptions = $request->descriptions;
        $data->eloqua_form = $request->eloqua_form;
        $data->save();

        alert()->success('Successfully Created.', 'Created');
        return redirect()->action('Admin\SnwPrizeController@index', ['campaign' => $request->campaign]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = SnwPrize::where('id', $id)->with('campaign:id,names->en as name')->first();
        return view('admin.snw.prizes.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'names.*' => 'required',
            'descriptions.*' => 'required',
            'eloqua_form' => 'required'
        ]);

        $data = SnwPrize::find($id);
        $data->names = $request->names;
        $data->descriptions = $request->descriptions;
        $data->eloqua_form = $request->eloqua_form;
        $data->save();

        alert()->success('Successfully updated.', 'Updated');
        return redirect()->action('Admin\SnwPrizeController@index', ['campaign' => $data->campaign_id]);
    }

}
