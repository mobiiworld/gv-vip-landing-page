<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Auth;
use App\CabanaDay;

class CabanaController extends Controller {

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Cabana_read', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['index']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Cabana_create', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['create', 'store']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Cabana_update', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['edit', 'update']]);
    }

    public function index() {
        return view('admin.cabana.index');
    }

    public function datatable(Request $request) {
        $currentUser = Auth::guard('admin')->user();
        $isSuperAdmin = $currentUser->hasRole('super-admin', 'admin');
        $program = CabanaDay::select('*');
        if ($request->status != '') {
            $program->where('status', $request->status);
        }
        return Datatables::of($program)
                        ->rawColumns(['actions', 'status'])
                        ->editColumn('actions', function ($program) use ($currentUser, $isSuperAdmin) {
                            $b = '';
                            if ($isSuperAdmin || $currentUser->hasPermissionTo('Cabana_update', 'admin')) {
                                $b = ' <a class="btn btn-primary btn-sm " href="' . route('admin.cabana-days.edit', $program->id) . '">Edit</a>';
                            }
                            return $b;
                        })
                        ->editColumn('status', function ($program) use ($currentUser, $isSuperAdmin) {
                            if ($isSuperAdmin || $currentUser->hasPermissionTo('Cabana_update', 'admin')) {
                                $checked = ($program->status == 1) ? 'checked' : '';
                                return '<input type="checkbox" name="status" id="status_' . $program->id . '"  data-on-text="Enabled" data-off-text="Disabled" value="1" ' . $checked . ' class="bsSwitch" autocomplete="off" data-id="' . $program->id . '">';
                            } else {
                                return ($program->status == 1) ? '<label class="label label-success">Enabled</label>' : '<label class="label label-danger">Disabled</label>';
                            }
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $program = CabanaDay::where('id', $id)->first();
        $program->start_time = date('h:i a', strtotime($program->start_dt));
        $program->end_time = date('h:i a', strtotime($program->end_dt));
        $program->nextDay = (date('Y-m-d', strtotime($program->start_dt)) == date('Y-m-d', strtotime($program->end_dt))) ? false : true;
        return view('admin.cabana.edit', compact('program'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if ($request->has('nextDay')) {
            $endDate = date('Y-m-d', strtotime($request->start_dt . ' + 1 days'));
        } else {
            $endDate = $request->start_dt;
        }
        $exist = CabanaDay::where('id', '!=', $id)
                ->where(function ($query) use ($request, $endDate) {
                    $query->whereDate('start_dt', $request->start_dt)
                    ->orWhereDate('end_dt', $endDate);
                })
                ->first();
        if ($exist) {
            alert()->error('These Date period exist.', 'Error');
            return redirect()->back()->withInput();
        } else {
            $program = CabanaDay::where('id', $id)->first();
            $program->start_dt = date('Y-m-d H:i:00', strtotime($request->start_dt . ' ' . $request->start_time));
            $program->end_dt = date('Y-m-d H:i:00', strtotime($endDate . ' ' . $request->end_time));
            $program->save();
            alert()->success('Successfully Updated.', 'Updated');
            return redirect()->route('admin.cabana-days.index');
        }
    }

    public function changeStatus(Request $request) {
        $program = CabanaDay::find($request->id);
        $program->status = ($program->status == 1) ? 0 : 1;
        $program->save();
        return response()->json(["status" => true, "message" => 'Successfully changed']);
    }

}
