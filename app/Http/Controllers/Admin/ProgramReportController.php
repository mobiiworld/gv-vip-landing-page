<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Program;
use App\ProgramSlot;
use Yajra\Datatables\Datatables;

class ProgramReportController extends Controller {

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Ramadan Majlis_booking summary', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['index', 'show']]);
    }

    public function index(Request $request) {
        if ($request->ajax()) {
            $dateTocheck = date("Y-m-d", strtotime($request->pdate));
            $pSlots = ProgramSlot::select('program_slots.id', 'program_slots.slot_id', 'program_slots.booking_id', 'slots.name', 'tb.name as tName', 'tb.type', 'tb.smoking_status', 'tb.id as tableId')
                    ->selectRaw('CASE WHEN slots.actual_end_time IS NOT NULL THEN (CONCAT(DATE_ADD(program_slots.program_date,INTERVAL 1 DAY)," ",slots.actual_end_time)) ELSE CONCAT(program_slots.program_date," ",slots.end_time) END AS end_time')
                    //->selectRaw('CASE WHEN program_slots.booking_id IS NOT NULL THEN (SELECT book_slots.first_name FROM book_slots WHERE book_slots.id=program_slots.booking_id) ELSE Null END AS userName')
                    //->selectRaw('CASE WHEN program_slots.booking_id IS NOT NULL THEN (SELECT CASE WHEN book_slots.admin_id IS NULL THEN book_slots.first_name ELSE CONCAT("<div class=\'onsite-container\'>",book_slots.first_name,"<div class=\'onsite\'>Onsite</div></div>") END FROM book_slots WHERE book_slots.id=program_slots.booking_id) ELSE Null END AS userName')
                    ->join('slots', 'slots.id', '=', 'program_slots.slot_id')
                    ->join('vip_tables as tb', 'tb.id', '=', 'program_slots.table_id')
                    ->where('program_slots.program_id', $request->pid)
                    ->where('program_slots.program_date', $dateTocheck)
                    ->where('slots.status', 1)
                    ->where('program_slots.status', 1)
                    ->where('tb.status', 1)
                    ->orderby('slots.priority', 'asc')
                    ->orderby('tb.seat_type', 'asc')
                    ->orderby('tb.type', 'asc')
                    ->orderby('tb.table_number', 'asc')
                    ->get();
            $sHeads = $pSlots->groupBy('name')->keys();
            $pSlots = $pSlots->groupBy('tName');
            //dd($pSlots);

            return view('admin.program.reports.pslots', compact('sHeads', 'pSlots'));
        }
        $program = Program::where('status', 1)->first();
        return view('admin.program.reports.index', compact('program'));
    }

    public function show(Request $request) {
        $booking = \App\BookSlot::where('id', $request->bid)->first();
        $tables = ProgramSlot::select('program_slots.id', 'slots.name', 'slots.priority', 'tb.name as tName', 'type', 'tb.seat_type', 'tb.smoking_status')
                        ->join('slots', 'slots.id', '=', 'program_slots.slot_id')
                        ->join('vip_tables as tb', 'tb.id', '=', 'program_slots.table_id')
                        ->where('program_slots.booking_id', $request->bid)
                        //->where('program_slots.program_status', 1)
                        ->where('program_slots.status', 1)
                        ->where('tb.status', 1)
                        ->orderby('tb.seat_type', 'asc')
                        ->orderby('tb.table_number', 'asc')
                        ->get()->groupBy('tName');
        $timelines = [];

        \App\ReleasedBookingSlot::select('slots.name', 'tb.name as tName', 'type', 'tb.seat_type', 'tb.smoking_status', \DB::raw("DATE(released_booking_slots.created_at) as date"), 'released_booking_slots.created_at', 'admin_id')
                ->join('program_slots', 'program_slots.id', '=', 'released_booking_slots.program_slot_id')
                ->join('slots', 'slots.id', '=', 'program_slots.slot_id')
                ->join('vip_tables as tb', 'tb.id', '=', 'program_slots.table_id')
                ->where('released_booking_slots.booking_id', $request->bid)
                ->get()->each(function ($qry)use (&$timelines) {
            $tabelDetails = ucwords($qry->type);
            $tabelDetails .= ($qry->smoking_status == 1) ? ' Smoking ' : ' Non Smoking ';
            $tabelDetails .= $qry->seat_type . ' seater table(' . $qry->tName . ') - ' . $qry->name;
            $timelines[$qry->date][] = [
                'heading' => 'Table Released',
                'time' => $qry->created_at,
                'description' => $tabelDetails,
                'admin' => $qry->admin
            ];
        });

        /* \App\CancelledBookingSlot::select('slots.name', 'tb.name as tName', 'type', 'tb.seat_type', 'tb.smoking_status', \DB::raw("DATE(cancelled_booking_slots.created_at) as date"), 'cancelled_booking_slots.created_at', 'admin_id')
          ->join('program_slots', 'program_slots.id', '=', 'cancelled_booking_slots.program_slot_id')
          ->join('slots', 'slots.id', '=', 'program_slots.slot_id')
          ->join('vip_tables as tb', 'tb.id', '=', 'program_slots.table_id')
          ->where('cancelled_booking_slots.booking_id', $request->bid)
          ->get()->each(function ($qry)use (&$timelines) {
          $tabelDetails = ucwords($qry->type);
          $tabelDetails .= ($qry->smoking_status == 1) ? ' Smoking ' : ' Non Smoking ';
          $tabelDetails .= $qry->seat_type . ' seater table(' . $qry->tName . ') - ' . $qry->name;
          $timelines[$qry->date][] = [
          'heading' => 'Table Cancelled',
          'time' => $qry->created_at,
          'description' => $tabelDetails
          ];
          }); */

        \App\ChangedBookingTable::select('tb1.name as fromTable', 'tb1.type', 'tb1.seat_type', 'tb1.smoking_status', 'tb2.name as toTable', \DB::raw("DATE(changed_booking_tables.created_at) as date"), 'changed_booking_tables.created_at', 'admin_id')
                ->join('vip_tables as tb1', 'tb1.id', '=', 'changed_booking_tables.tid_changed_from')
                ->join('vip_tables as tb2', 'tb2.id', '=', 'changed_booking_tables.tid_changed_to')
                ->where('changed_booking_tables.booking_id', $request->bid)
                ->get()->each(function ($qry)use (&$timelines) {
            $tabelDetails = 'From ' . ucwords($qry->type);
            $tabelDetails .= ($qry->smoking_status == 1) ? ' Smoking ' : ' Non Smoking ';
            $tabelDetails .= $qry->seat_type . ' seater table(' . $qry->fromTable . ') To ' . $qry->toTable;
            $timelines[$qry->date][] = [
                'heading' => 'Table Changed',
                'time' => $qry->created_at,
                'description' => $tabelDetails,
                'admin' => $qry->admin
            ];
        });

        \App\BookingDateChanged::select('changed_from', 'changed_to', \DB::raw("DATE(created_at) as date"), 'created_at', 'admin_id')
                ->where('booking_id', $request->bid)
                ->get()->each(function ($qry)use (&$timelines) {
            $timelines[$qry->date][] = [
                'heading' => 'Booking Date Changed',
                'time' => $qry->created_at,
                'description' => 'Changed from ' . date('d, M Y', strtotime($qry->changed_from)) . ' to ' . date('d, M Y', strtotime($qry->changed_to)),
                'admin' => $qry->admin
            ];
        });
        krsort($timelines);
        return view('admin.program.reports.show', compact('booking', 'tables', 'timelines'));
    }

    public function changeTable(Request $request) {
        if (auth('admin')->user()->hasRole('super-admin', 'admin') || auth('admin')->user()->hasPermissionTo('Ramadan Majlis_change table', 'admin')) {
            $table = \App\VipTable::select('id', 'name', 'type', 'seat_type', 'smoking_status')
                    ->where('id', $request->tId)
                    ->first();
            $existTableSlots = ProgramSlot::select('program_slots.id', 'program_slots.slot_id', 'program_slots.program_date', 'slots.name')
                    ->join('slots', 'slots.id', '=', 'program_slots.slot_id')
                    ->where('program_slots.booking_id', $request->bid)
                    ->where('program_slots.table_id', $request->tId)
                    ->get();

            $tabelDetails = ucwords($table->type);
            $tabelDetails .= ($table->smoking_status == 1) ? ' Smoking ' : ' Non Smoking ';
            $tabelDetails .= $table->seat_type . ' seater table';
            $slotNames = '<b>' . $table->name . '</b> - ' . $existTableSlots->pluck('name')->implode(',');

            $slotIds = $existTableSlots->pluck('slot_id');
            $slotCounts = $slotIds->count();

            $availableTables = ProgramSlot::select('program_slots.table_id', 'tb.name')
                    ->join('vip_tables as tb', 'tb.id', '=', 'program_slots.table_id')
                    ->where('program_slots.program_date', $existTableSlots[0]->program_date)
                    ->whereNull('program_slots.booking_id')
                    ->where('program_slots.program_status', 1)
                    ->where('tb.type', $table->type)
                    ->where('tb.seat_type', $table->seat_type)
                    ->where('tb.smoking_status', $table->smoking_status)
                    ->whereIn('program_slots.slot_id', $slotIds)
                    ->groupBy('program_slots.table_id', 'tb.name')
                    ->havingRaw("COUNT(program_slots.table_id) = {$slotCounts}")
                    ->get();

            $extras = [
                'tabelDetails' => $tabelDetails,
                'slotNames' => $slotNames,
                'fromTableId' => $table->id,
                'bookingId' => $request->bid,
                'fromPslots' => $existTableSlots->pluck('id')->implode(','),
                'slotIds' => $slotIds->implode(','),
            ];

            return view('admin.program.reports.change-table', compact('extras', 'availableTables'));
        } else {
            echo '<p class="text-center text-red">Your don\'t have permission to do this action.</p>';
        }
    }

    public function storeChangedTable(Request $request) {
        $booking = \App\BookSlot::where('id', $request->booking_id)->first();
        $slotIds = explode(',', $request->slot_ids);
        $releasedPslotIds = explode(',', $request->released_pslot_ids);
        $tableTo = ProgramSlot::select('id')
                ->where('table_id', $request->toTableId)
                ->whereIn('slot_id', $slotIds)
                ->where('program_date', $booking->program_date)
                ->get();
        if (count($slotIds) == $tableTo->count()) {
            $tableTo = $tableTo->pluck('id')->toArray();
            ProgramSlot::whereIn('id', $tableTo)->update(['booking_id' => $booking->id, 'program_status' => 3]);

            $newPslots = array_merge(explode(',', $booking->program_slot_id), $tableTo);
            $booking->program_slot_id = implode(',', array_diff($newPslots, $releasedPslotIds));
            $booking->save();

            ProgramSlot::where('booking_id', $booking->id)
                    ->whereIn('id', $releasedPslotIds)
                    ->update(['booking_id' => null, 'program_status' => 1]);

            $changedTableHistory = new \App\ChangedBookingTable();
            $changedTableHistory->admin_id = auth('admin')->user()->id;
            $changedTableHistory->booking_id = $booking->id;
            $changedTableHistory->tid_changed_from = $request->tid_changed_from;
            $changedTableHistory->tid_changed_to = $request->toTableId;
            $changedTableHistory->slot_ids = $request->slot_ids;
            $changedTableHistory->released_pslot_ids = $request->released_pslot_ids;
            $changedTableHistory->save();

            return response()->json(["status" => true, "message" => 'Table successfully changed.']);
        }
        return response()->json(["status" => false, "message" => 'Requested table is not available now.Please try again.']);
    }

    public function getReleaseSlots(Request $request) {
        if (auth('admin')->user()->hasRole('super-admin', 'admin') || auth('admin')->user()->hasPermissionTo('Ramadan Majlis_release table', 'admin')) {
            $table = \App\VipTable::select('id', 'name', 'type', 'seat_type', 'smoking_status')
                    ->where('id', $request->tId)
                    ->first();
            $existTableSlots = ProgramSlot::select('program_slots.id', 'slots.name')
                            ->selectRaw('CASE WHEN slots.actual_end_time IS NOT NULL THEN (CONCAT(DATE_ADD(program_slots.program_date,INTERVAL 1 DAY)," ",slots.actual_end_time)) ELSE CONCAT(program_slots.program_date," ",slots.end_time) END AS end_time')
                            ->join('slots', 'slots.id', '=', 'program_slots.slot_id')
                            ->where('program_slots.booking_id', $request->bid)
                            ->where('program_slots.table_id', $request->tId)
                            ->orderBy('slots.priority', 'asc')
                            ->get()->where('end_time', '>', date('Y-m-d H:i:00'));

            $tabelDetails = ucwords($table->type);
            $tabelDetails .= ($table->smoking_status == 1) ? ' Smoking ' : ' Non Smoking ';
            $tabelDetails .= $table->seat_type . ' seater table(' . $table->name . ')';

            $extras = [
                'tabelDetails' => $tabelDetails,
                'bookingId' => $request->bid,
            ];
            return view('admin.program.reports.change-slots', compact('extras', 'existTableSlots'));
        } else {
            echo '<p class="text-center text-red">Your don\'t have permission to do this action.</p>';
        }
    }

    public function changeDate(Request $request) {
        $booking = \App\BookSlot::where('id', $request->booking_id)->first();
        if (empty($booking->ticket_ids)) {
            return response()->json(["status" => false, "message" => 'No ticket ids available for this booking.']);
        }
        if ($booking->program_date == date("Y-m-d", strtotime($request->to_date))) {
            $booking->special_notes = $request->s_notes;
            $booking->save();
            return response()->json(["status" => true, "message" => 'Updated Successfully.']);
        } else {
            $existTableSlots = ProgramSlot::select('program_slots.id', 'program_slots.slot_id')
                    //->join('slots', 'slots.id', '=', 'program_slots.slot_id')
                    ->where('program_slots.booking_id', $booking->id)
                    ->get();

            $oldPdate = $booking->program_date;
            $oldPslots = $existTableSlots->pluck('id');
            $slotIds = $existTableSlots->unique('slot_id')->pluck('slot_id');

            $releasedSlotIds = \App\ReleasedBookingSlot::select('released_booking_slots.program_slot_id', 'program_slots.slot_id')
                    ->join('program_slots', 'program_slots.id', '=', 'released_booking_slots.program_slot_id')
                    ->where('released_booking_slots.booking_id', $booking->id)
                    ->get();
            if (!$releasedSlotIds->isEmpty()) {
                $releasedSlotIds = $releasedSlotIds->unique('slot_id')->pluck('slot_id');
                $slotIds = $slotIds->merge($releasedSlotIds)->unique();
            }

            $slotCounts = $slotIds->count();

            $tableSeats = [
                'bookingId' => $booking->id,
                'programDate' => date("Y-m-d", strtotime($request->to_date)),
                'tableType' => $booking->table_type,
                'smokingType' => (int) $booking->payment_extras['smokingType'],
                'noOfTables2' => 0,
                'noOfTables4' => 0,
                'noOfTables6' => 0,
            ];

            if ((isset($booking->payment_extras['tableId2']))) {
                if (!empty($booking->payment_extras['tableId2'])) {
                    $tableSeats['noOfTables2'] = 1;
                }
                if (!empty($booking->payment_extras['tableId4'])) {
                    $tableSeats['noOfTables4'] = 1;
                }
                if (!empty($booking->payment_extras['tableId6'])) {
                    $tableSeats['noOfTables6'] = 1;
                }
            } else {
                if (isset($booking->payment_extras['noOfTables2'])) {
                    $tableSeats['noOfTables2'] = (int) $booking->payment_extras['noOfTables2'];
                }
                if (isset($booking->payment_extras['noOfTables4'])) {
                    $tableSeats['noOfTables4'] = (int) $booking->payment_extras['noOfTables4'];
                }
                if (isset($booking->payment_extras['noOfTables6'])) {
                    $tableSeats['noOfTables6'] = (int) $booking->payment_extras['noOfTables6'];
                }
            }

            $newPslots = [];
            $allOkay = true;
            if ($tableSeats['noOfTables2'] > 0) {
                $spSlots = $this->getTablePslots(2, $slotIds, $tableSeats, $slotCounts);
                if (!empty($spSlots)) {
                    $newPslots = array_merge($newPslots, $spSlots);
                } else {
                    $allOkay = false;
                }
            }
            if ($tableSeats['noOfTables4'] > 0) {
                $spSlots = $this->getTablePslots(4, $slotIds, $tableSeats, $slotCounts);
                if (!empty($spSlots)) {
                    $newPslots = array_merge($newPslots, $spSlots);
                } else {
                    $allOkay = false;
                }
            }
            if ($tableSeats['noOfTables6'] > 0) {
                $spSlots = $this->getTablePslots(6, $slotIds, $tableSeats, $slotCounts);
                if (!empty($spSlots)) {
                    $newPslots = array_merge($newPslots, $spSlots);
                } else {
                    $allOkay = false;
                }
            }
            $msg = 'Booking not available for the selected date.';
            if ($allOkay) {
                //call vgs to update date.
                $ticketIds = explode(',', $booking->ticket_ids);
                $vgsDateUpdated = $this->vgsChangeBookingDate($tableSeats['programDate'], $ticketIds, $booking->sale_code);
                if ($vgsDateUpdated['status']) {
                    $booking->program_date = $tableSeats['programDate'];
                    $booking->special_notes = $request->s_notes;
                    if ($oldPdate == date("Y-m-d", strtotime($booking->actual_program_dt))) {
                        $booking->actual_program_dt = $tableSeats['programDate'] . ' ' . date("H:i:00", strtotime($booking->actual_program_dt));
                    } else {
                        $booking->actual_program_dt = date('Y-m-d', strtotime($tableSeats['programDate'] . ' + 1 days')) . ' ' . date("H:i:00", strtotime($booking->actual_program_dt));
                    }
                    $booking->program_slot_id = implode(',', $newPslots);
                    $booking->save();

                    ProgramSlot::where('booking_id', $booking->id)
                            ->whereIn('id', $oldPslots)
                            ->update(['booking_id' => null, 'program_status' => 1]);

                    $changedHistory = new \App\BookingDateChanged();
                    $changedHistory->admin_id = auth('admin')->user()->id;
                    $changedHistory->booking_id = $booking->id;
                    $changedHistory->changed_from = $oldPdate;
                    $changedHistory->changed_to = $tableSeats['programDate'];
                    $changedHistory->old_pslot_ids = $oldPslots->implode(',');
                    $changedHistory->save();

                    //call drupal update
                    if (empty($booking->admin_id)) {
                        $this->updateDrupalBookingDate($booking->program_date, $booking->sale_code);
                    }

                    return response()->json(["status" => true, "message" => $vgsDateUpdated['message']]);
                }
                $msg = $vgsDateUpdated['message'];
            }
            if (!empty($newPslots)) {
                ProgramSlot::where('booking_id', $booking->id)
                        ->whereIn('id', $newPslots)
                        ->update(['booking_id' => null, 'program_status' => 1]);
            }
            return response()->json(["status" => false, "message" => $msg]);
        }
    }

    public function getTablePslots($seatType, &$slotIds, &$configs, $slotCounts) {
        $tableLimit = $configs['noOfTables' . $seatType];
        $availableTables = ProgramSlot::select('program_slots.table_id', 'tb.name', \DB::raw('GROUP_CONCAT(program_slots.id) as pslots'))
                ->join('vip_tables as tb', 'tb.id', '=', 'program_slots.table_id')
                ->where('program_slots.program_date', $configs['programDate'])
                ->whereNull('program_slots.booking_id')
                ->where('program_slots.program_status', 1)
                ->where('tb.type', $configs['tableType'])
                ->where('tb.seat_type', $seatType)
                ->where('tb.smoking_status', $configs['smokingType'])
                ->whereIn('program_slots.slot_id', $slotIds)
                ->orderBy('tb.table_number', 'asc')
                ->groupBy('program_slots.table_id', 'tb.name')
                ->havingRaw("COUNT(program_slots.table_id) = {$slotCounts}")
                ->take($tableLimit)
                ->get();

        if ($availableTables->count() == $tableLimit) {
            $pslots = $availableTables->pluck('pslots')->implode(',');
            $pslots = explode(',', $pslots);
            ProgramSlot::whereNull('booking_id')
                    ->whereIn('id', $pslots)
                    ->update(['booking_id' => $configs['bookingId'], 'program_status' => 3]);
            return $pslots;
        }
        return null;
    }

    public function vgsChangeBookingDate($pDate, $tIds, $saleCode) {
        $configurations = \App\ApiConfiguration::select('value')
                ->where('name', 'work_station_id')
                ->first();
        $tickets = [];
        $vgsRspns = [
            'status' => 0,
            'extras' => ['saleCode' => null, 'SaleId' => null],
            'message' => 'Something went wrong on Date Upadte VGS API.'
        ];
        foreach ($tIds as $tId) {
            array_push($tickets, ['TicketId' => $tId]);
        }
        $url = config('accountConfig.pprUrl');
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "&cmd=TICKET",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode([
                'Header' => ["WorkstationId" => $configurations->value],
                'Request' => [
                    'Command' => 'UpdateTicket',
                    'UpdateTicket' => [
                        'TicketList' => $tickets,
                        'ValidDateFrom' => $pDate,
                        'ValidDateTo' => $pDate,
                    ]
                ]
            ]),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'cache-control: no-cache',
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response);
        if (isset($response->Answer->UpdateTicket->TransactionRecap->SaleId)) {
            $vgsRspns['extras']['saleCode'] = $response->Answer->UpdateTicket->TransactionRecap->SaleCode;
            $vgsRspns['extras']['SaleId'] = $response->Answer->UpdateTicket->TransactionRecap->SaleId;
            //GetSaleId
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url . "&cmd=SALESEARCH",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode([
                    'Header' => ["WorkstationId" => $configurations->value],
                    'Request' => [
                        'SaleCode' => $saleCode
                    ]
                ]),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'cache-control: no-cache',
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($response);
            if (isset($response->Answer->SaleList[0])) {
                $emailSend = $this->sendVgsEmal($configurations->value, $response->Answer->SaleList[0]->SaleId);
                if ($emailSend['status']) {
                    $vgsRspns['status'] = 1;
                    $vgsRspns['message'] = 'Date successfully updated.';
                } else {
                    $vgsRspns['status'] = 1;
                    $vgsRspns['message'] = 'Date successfully updated.But email not send.' . $emailSend['message'];
                }
            } else {
                $vgsRspns['status'] = 1;
                $vgsRspns['message'] = 'Date successfully updated.But email not send.Search Sale id API failed.';
            }
        }
        return $vgsRspns;
    }

    public function sendVgsEmal($workstationId, $saleId) {
        //Search Action
        $url = config('accountConfig.pprUrl');
        $vgsRspns = ['status' => 0, 'message' => 'Get Action Id API failed.'];
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "&cmd=ACTION",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode([
                'Header' => ["WorkstationId" => $workstationId],
                'Request' => [
                    'Command' => 'Search',
                    'Search' => [
                        'PagePos' => 1,
                        'RecordPerPage' => 100,
                        'LinkEntityId' => $saleId
                    ]
                ]
            ]),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'cache-control: no-cache',
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response);
        if (isset($response->Answer->Search->ActionList[0]->ActionId)) {
            //Load Action
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url . "&cmd=ACTION",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode([
                    'Header' => ["WorkstationId" => $workstationId],
                    'Request' => [
                        'Command' => 'Load',
                        'Load' => [
                            'ActionId' => $response->Answer->Search->ActionList[0]->ActionId
                        ]
                    ]
                ]),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'cache-control: no-cache',
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($response);
            if (isset($response->Answer->Load->Action->ActionId)) {
                //Send Email
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url . "&cmd=ACTION",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode([
                        'Header' => ["WorkstationId" => $workstationId],
                        'Request' => [
                            'Command' => 'SendEmailAction',
                            'SendEmailAction' => [
                                'ActionId' => $response->Answer->Load->Action->ActionId,
                                'AddressTo' => $response->Answer->Load->Action->DataEmail->AddressTo,
                                'AddressFrom' => 'tickets@globalvillage.ae',
                            ]
                        ]
                    ]),
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json',
                        'cache-control: no-cache',
                    ),
                ));
                $response = curl_exec($curl);
                curl_close($curl);
                $response = json_decode($response);
                if (isset($response->Header->StatusCode)) {
                    $vgsRspns['status'] = 1;
                    $vgsRspns['message'] = 'Email send successfully.';
                } else {
                    $vgsRspns['message'] = 'Send Email API failed.';
                }
            } else {
                $vgsRspns['message'] = 'Load Action API failed.';
            }
        }
        return $vgsRspns;
    }

    public function updateDrupalBookingDate($pDate, $saleCode) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('E_API_URL', 'https://gv-web-d9-beta.dev.mobiiworld.com') . '/ramadan-booking-update',
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode([
                'salecode' => $saleCode,
                'validFrom' => $pDate,
                'validTo' => $pDate,
            ]),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'GV-Unique-Key: ' . env('DRUPAL_UNIQUE_KEY', '7f3^`H_Gx~7neyl73TZykP_sY`J)Jp'),
                'GV-Language: en'
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        return 1;
    }

    public function getQrcodes(Request $request) {
        $configurations = \App\ApiConfiguration::select('value')
                ->where('name', 'work_station_id')
                ->first();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => config('accountConfig.pprUrl') . "&cmd=SALE",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode([
                'Header' => ["WorkstationId" => $configurations->value],
                'Request' => [
                    'Command' => 'LoadEntSale',
                    'LoadEntSale' => ['SaleCode' => $request->saleCode]
                ]
            ]),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'cache-control: no-cache',
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response);
        if (isset($response->Answer->LoadEntSale->Sale->Sale->MediaList)) {
            $mediaCodes = $response->Answer->LoadEntSale->Sale->Sale->MediaList;
            return view('admin.program.reports.qrcodes', compact('mediaCodes'));
            //dd($response->Answer->LoadEntSale->Sale->Sale->MediaList);
        } else {
            echo '<p class="text-center text-red">No Media Codes from VGS response.</p>';
        }
    }

}
