<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Season;

use Auth;
use URL;
use App\Pack;
use App\Traits\AccountActivationTraits;
use App\Traits\GvpTraits;
use App\Traits\DrupalTraits;
use App\Traits\ImageTraits;

use Excel;
use App\Exports\PacksExport;
use Zip;
use PDF;

class SeasonController extends Controller
{
    use ImageTraits;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.season.index');
    }

    public function datatable(Request $request) {

        $season = Season::select('*')->orderby('ordering','desc');
        
        return Datatables::of($season)
                        ->rawColumns(['actions','status','pack_activation_enabled'])
                        ->editColumn('actions', function ($season) {
                            
                            $b = ' <a class="btn btn-primary btn-sm " href="' . route('admin.season.edit', $season->id) . '">Edit</a>';
                            //$b = ' <a class="btn btn-info btn-sm " href="' . route('admin.season.packs', $season->id) . '">Packs</a>';
                            return $b;
                        })
                        ->editColumn('status', function ($season) {
                             $checked = ($season->status == 1) ? 'checked' : '';
                             $st = ($season->status == 1) ? 'active' : 'inactive';
                            return '<input type="checkbox" name="status" id="status_' . $season->id . '"  data-on-text="Enabled" data-off-text="Disabled" value="1" ' . $checked . ' data-st="'.$st.'" class="bsSwitch" autocomplete="off" data-id="' . $season->id . '">';
                        })
                        ->editColumn('pack_activation_enabled', function ($season) {
                             $checked = ($season->pack_activation_enabled == 1) ? 'checked' : '';
                             $st = ($season->pack_activation_enabled == 1) ? 'active' : 'inactive';
                            return '<input type="checkbox" name="status" id="pack_activation_enabled_' . $season->id . '"  data-on-text="Enabled" data-off-text="Disabled" value="1" ' . $checked . ' data-st="'.$st.'" class="bsSwitchPackActv" autocomplete="off" data-id="' . $season->id . '">';
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.season.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',   
            'season_number' => 'required|unique:seasons',
        ]);
        $seasonOrdering = Season::max('ordering') + 1;
        $season = new Season;
        $season->name = $request->name;
        $season->season_number = $request->season_number;
        $season->pack_activation_disbaled_msg_en = $request->pack_activation_disbaled_msg_en;
        $season->pack_activation_disbaled_msg_ar = $request->pack_activation_disbaled_msg_ar;
        $season->ordering = $seasonOrdering;
        $season->status = 0;
        $season->save();
        alert()->success('Season successfully added.', 'Added');
        return redirect()->route('admin.season.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $season =  Season::find($id);
        return view('admin.season.edit',compact('season'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',   
            'season_number' => 'required|unique:seasons,season_number,'.$id,
        ]);
       
        $season =  Season::find($id);
        $season->name = $request->name;
        $season->season_number = $request->season_number;  
        $season->pack_activation_disbaled_msg_en = $request->pack_activation_disbaled_msg_en;
        $season->pack_activation_disbaled_msg_ar = $request->pack_activation_disbaled_msg_ar;     
        $season->save();
        alert()->success('Season successfully updated.', 'Updated');
        return redirect()->route('admin.season.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changeStatus(Request $request){
        if($request->st == 'inactive'){
            $exist = Season::where('status',1)->first();
            if(!empty($exist)){
                return response()->json(["status" => 'error', "message" => 'Active season exists.']);
            }
        }
        $season = Season::find($request->id);
        $season->status = ($season->status == 1) ? 0 : 1;
        $season->save();
         return response()->json(["status" => 'success', "message" => 'Successfully changed']);

    }

    public function changePackActivation(Request $request){
        
        $season = Season::find($request->id);
        $season->pack_activation_enabled = ($season->pack_activation_enabled == 1) ? 0 : 1;
        $season->save();
         return response()->json(["status" => 'success', "message" => 'Successfully changed']);

    }

    public function packs($seasonId)
    {
        
        $userCounts = \App\User::selectRaw('count(*) AS total, sum(case when account_activation_status = 3 then 1 else 0 end) AS vgs_synced, sum(case when account_activation_status != 3 then 1 else 0 end) AS vgs_not_synced, sum( IF( (select count(*) from user_cars where user_cars.user_id = users.id  AND designa_card_uid IS NOT NULL AND  designa_setv_status = 1 AND designa_acc_status = 1) > 0, 1,0) ) as designa_synced, sum( IF( (select count(*) from user_cars where user_cars.user_id = users.id  AND (designa_card_uid IS NULL OR  designa_setv_status = 0 OR designa_acc_status = 0)) > 0, 1,0) ) as designa_not_synced')->join('packs', 'users.pack_id', 'packs.id')->where('packs.season_id',$seasonId);
        $configurations = \App\ApiConfiguration::where('name','valid_pack_limit')->first();

        if(isset($request->limit) && $request->limit == 'valid'){            
            $userCounts->where('packs.pack_number','<=',(isset($configurations->value) ? $configurations->value : 10000));
        }
        $userCounts = $userCounts->first();
        $userWithoutCar = 0;
        $userCarIds = \App\UserCar::pluck("user_id");
        if (!empty($userCarIds)) {
            $userWithoutCar = \App\User::join('packs', 'users.pack_id', 'packs.id')->whereNotIn('users.id', $userCarIds)->where('packs.season_id',$seasonId);
            if(isset($request->limit) && $request->limit == 'valid'){ 
                $userWithoutCar->where('packs.pack_number','<=',(isset($configurations->value) ? $configurations->value : 10000));
            }
            $userWithoutCar = $userWithoutCar->count();
        }

        $limit = (isset($request->limit) && $request->limit == 'valid') ? 'valid' : 'all';
        
        
        
        return view('admin.season.pack', compact('userCounts', 'userWithoutCar','limit','seasonId'));
    }

    public function packdatatable(Request $request) {

        $packs = Pack::select('packs.id', 'packs.pack_number', 'users.first_name', 'users.last_name', 'users.email', 'users.mobile', 'users.id as user_id', 'users.account_activation_status', 'packs.drupal_user_id','packs.activation_code','packs.prefix','packs.category')
                ->leftJoin('users', 'users.pack_id', 'packs.id')
                ->where('packs.season_id',$request->seasonId)->orderby('pack_number','asc')
                ->whereNotNull('packs.sale_id')
                ->where('packs.sale_id','!=','')
                ;
        if ($request->designa_status != '') {
            if ($request->designa_status == 'synced') {
                $packs->whereNotNull('users.designa_customer_id');
                $packs->whereExists(function ($query) {
                    $query->select(\DB::raw(1))
                            ->from('user_cars as uc')
                            ->whereRaw("uc.user_id = users.id AND designa_card_uid IS NOT NULL AND  designa_setv_status = 1 AND designa_acc_status = 1");
                });
            } else {
                //$packs->whereNull('users.designa_customer_id');
                $packs->whereExists(function ($query) {
                    $query->select(\DB::raw(1))
                            ->from('user_cars as uc')
                            ->whereRaw("uc.user_id = users.id AND (designa_card_uid IS NULL OR  designa_setv_status = 0 OR designa_acc_status = 0)");
                });
            }
        }
        if ($request->limit == 'valid') {
            $configurations = \App\ApiConfiguration::where('name','valid_pack_limit')->first();
            $packs->where('packs.pack_number','<=',(isset($configurations->value) ? $configurations->value : 10000) );
        }
        if ($request->vgs_status != '') {
            if ($request->vgs_status == 'synced') {
                $packs->where('users.account_activation_status', 3);
            } else {
                $packs->where('users.account_activation_status', '!=', 3);
            }
        }
        if ($request->blocked_status != '') {
            $packs->where('users.blocked_status', $request->blocked_status );
        }
        return Datatables::of($packs)
                        ->rawColumns(['actions'])
                        ->editColumn('actions', function ($packs) {
                            $b = '';
                            //$b .= ' <a class="btn btn-info btn-sm " href="' . route('admin.packs.show', $packs->id) . '">View</a>';
                            $b .= ' <a class="btn btn-primary btn-sm " href="' . route('admin.season.download', $packs->id) . '"> Download</a>';
                            $b .= ' <a class="btn btn-info btn-sm " href="' . route('admin.season.offers', $packs->id) . '">List</a>';
                    
                                
                            
                            return $b;
                        })
                        ->make(true);
    }

    public function download($packId){
        set_time_limit(0);
        $locale = 'en';
        $packs = \App\Pack::select('packs.id', 'packs.pack_number', 'users.first_name', 'users.last_name', 'users.email', 'users.mobile', 'users.id as user_id', 'users.account_activation_status', 'packs.drupal_user_id','packs.activation_code','packs.prefix','packs.category')
                ->leftJoin('users', 'users.pack_id', 'packs.id')
                ->where('packs.id',$packId)->whereNotNull('users.pack_id')
                ->first();
        if(empty($packs)){
            return redirect()->route('admin.season.index');
        }
        $allOffers = \App\PartnerOffer::select('id', "merchant_name_{$locale} as merchant_name", "offer_name_{$locale} as offer_name", 'valid_from', 'valid_to', 'promo_code', 'code_type', 't_and_c', 'link', 'merchant_name_en')
        ->addselect(\DB::raw('LOWER(REPLACE(partner_offers.merchant_name_en, " ", "")) as offerName'))
                        ->where('pack_id', $packId)->get();
        
        $zipper = new \Chumper\Zipper\Zipper;
        $zip_file = storage_path() .  '/app/public/' .$packs->pack_number.'.zip';
       
        $zipper->make($zip_file);
            foreach($allOffers  as $offer){
                $fileName = $offer->offerName.'_' . time() . '.pdf';
                $pdf = PDF::loadView('web.pdf.offer-download', compact('offer'))->output();
                //$zip->addFile($pdf, $offer->offerName.'/'.$fileName);
                //$zip->setPath($offer->offerName.'/'.$fileName)->add($pdf);
                 $zipper->folder($offer->offerName)->addString($fileName,$pdf);
            }

           
        $zipper->close();
       
        return response()->download($zip_file)->deleteFileAfterSend(true);
    }

    public function offers($packId){
        $locale = 'en';
        $packs = \App\Pack::select('packs.id', 'packs.pack_number', 'users.first_name', 'users.last_name', 'users.email', 'users.mobile', 'users.id as user_id', 'users.account_activation_status', 'packs.drupal_user_id','packs.activation_code','packs.prefix','packs.category')
                ->leftJoin('users', 'users.pack_id', 'packs.id')
                ->where('packs.id',$packId)->whereNotNull('users.pack_id')
                ->first();
        if(empty($packs)){
            return redirect()->route('admin.season.index');
        }
        $allOffers = \App\PartnerOffer::select('id', "merchant_name_{$locale} as merchant_name", "offer_name_{$locale} as offer_name", 'valid_from', 'valid_to', 'promo_code', 'code_type', 't_and_c', 'link', 'merchant_name_en')
        ->addselect(\DB::raw('LOWER(REPLACE(partner_offers.merchant_name_en, " ", "")) as offerName'))
                        ->where('pack_id', $packId)->get();
                        
        return view('admin.season.offer', compact('allOffers','packId'));
    }

    public function downloadOffer(Request $request,$packId){
        set_time_limit(0);
        $locale = 'en';
        $packs = \App\Pack::select('packs.id', 'packs.pack_number', 'users.first_name', 'users.last_name', 'users.email', 'users.mobile', 'users.id as user_id', 'users.account_activation_status', 'packs.drupal_user_id','packs.activation_code','packs.prefix','packs.category')
                ->leftJoin('users', 'users.pack_id', 'packs.id')
                ->where('packs.id',$packId)->whereNotNull('users.pack_id')
                ->first();
        if(empty($packs)){
            return redirect()->route('admin.season.index');
        }
         $ids = explode('|',$request->ids);
        
        if(empty($ids)){
            return redirect()->route('admin.season.index');
        }
        $allOffers = \App\PartnerOffer::select('id', "merchant_name_{$locale} as merchant_name", "offer_name_{$locale} as offer_name", 'valid_from', 'valid_to', 'promo_code', 'code_type', 't_and_c', 'link', 'merchant_name_en')
        ->addselect(\DB::raw('LOWER(REPLACE(partner_offers.merchant_name_en, " ", "")) as offerName'))
                        ->where('pack_id', $packId)
                        ->whereIn('id',$ids)
                        ->get();
        
        $zipper = new \Chumper\Zipper\Zipper;
        $zip_file = storage_path() .  '/app/public/' .$packs->pack_number.'.zip';
       
        $zipper->make($zip_file);
            foreach($allOffers  as $offer){
                $fileName = $offer->offerName.'_' . time() . '.pdf';
                $pdf = PDF::loadView('web.pdf.offer-download', compact('offer'))->output();
                //$zip->addFile($pdf, $offer->offerName.'/'.$fileName);
                //$zip->setPath($offer->offerName.'/'.$fileName)->add($pdf);
                 $zipper->folder($offer->offerName)->addString($fileName,$pdf);
            }

           
        $zipper->close();
       
        return response()->download($zip_file)->deleteFileAfterSend(true);
    }    

    public function currentSeason()
    {
        $season =  Season::where('status',1)->first();
        if(empty($season)){
            alert()->error('Not available', 'Sorry');
            return redirect()->route('admin.dashboard');
        }
        return view('admin.season.current_season',compact('season'));
    }

    public function currentSeasonUpdate(Request $request, $id)
    {
        
       
        $season =  Season::find($id);        
        $season->pack_activation_enabled = $request->has('pack_activation_enabled') ? 1 : 0;
        $season->pack_activation_disbaled_msg_en = $request->pack_activation_disbaled_msg_en;
        $season->pack_activation_disbaled_msg_ar = $request->pack_activation_disbaled_msg_ar;     
        $season->pack_activation_disabled_title_en = $request->pack_activation_disabled_title_en;     
        $season->pack_activation_disabled_title_ar = $request->pack_activation_disabled_title_ar;  
        if($request->has('image')){
            $relPath = 'season/';
            $season->pack_activation_disabled_image = $this->resize($request->image, $relPath);
        }  
        
        $season->show_register_popup = $request->has('show_register_popup') ? 1 : 0;
        $season->fixed_popup = $request->has('fixed_popup') ? 1 : 0;
       
        $season->save();
        alert()->success('Settings successfully updated.', 'Updated');
        return redirect()->route('admin.current-season');
    }
}
