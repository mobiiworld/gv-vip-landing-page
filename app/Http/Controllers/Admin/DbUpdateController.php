<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DbUpdateController extends Controller
{
    public function promocode(Request $request){
    	set_time_limit(0);
    	$limit = ($request->limit != '') ? $request->limit : 100;
    	$packs = \App\Pack::join('partner_offers as po','po.pack_id','=','packs.id')
    				->select('po.pack_id')
    				->where('po.migrated_status','!=',1)
    				->where('po.merchant_name_en','LIKE','LEGOLAND Dubai')
    				->limit($limit)
    				->distinct()
    				->get();
    	$updated = 0;
    	if(!empty($packs)){
    		foreach($packs as $pac){
    			$oneComplementeryOffer = \App\PartnerOffer::where('pack_id',$pac->pack_id)
    										->where('migrated_status','!=',1)
    										->where('merchant_name_en','LIKE','LEGOLAND Dubai')
    										->where(function($query){
    											$query->where('offer_name_en','LIKE','1 complimentary Entry Ticket');
    										})
    										->first();
    			$twentyFiveOffer = \App\PartnerOffer::where('pack_id',$pac->pack_id)
    										->where('migrated_status','!=',1)
    										->where('merchant_name_en','LIKE','LEGOLAND Dubai')
    										->where(function($query){
    											$query->where('offer_name_en','LIKE','25% discount on additional tickets (in a single transaction)')
    											->orWhere('offer_name_en','LIKE','25% discount on additional entry tickets (in a single transaction)');
    										})
    										->first();
    			// print "<pre>";
    			// print_r($oneComplementeryOffer);
    			// print_r($twentyFiveOffer);
    			if(!empty($oneComplementeryOffer) && !empty($twentyFiveOffer)){
    				$oneComplementeryOffer->old_promo_code = $oneComplementeryOffer->promo_code; 
    				$twentyFiveOffer->old_promo_code = $twentyFiveOffer->promo_code;

    				$twentyFiveOffer->promo_code = $oneComplementeryOffer->old_promo_code;
    				$oneComplementeryOffer->promo_code = $twentyFiveOffer->old_promo_code;
    				$oneComplementeryOffer->migrated_status = 1;
    				$twentyFiveOffer->migrated_status = 1;
    				$twentyFiveOffer->save();
    				$oneComplementeryOffer->save();

    				$updated++;
    			}
    		}
    	}
    	$pendingCount =  \App\Pack::join('partner_offers as po','po.pack_id','=','packs.id')
    				->select(\DB::raw('count(DISTINCT(po.pack_id)) as count'))
    				->where('po.migrated_status','!=',1)
    				->where('po.merchant_name_en','LIKE','LEGOLAND Dubai')
    				->first();
    	echo "Updated Pack Count : ".$updated. " , Pending Pack Count : ".$pendingCount;
    }
}
