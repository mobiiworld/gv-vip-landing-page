<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Auth;
use URL;
use App\Pack;
use App\Traits\AccountActivationTraits;
use App\Traits\GvpTraits;
use App\Traits\DrupalTraits;
use App\Traits\PackBlockTraits;
use App\Traits\SalesForceTraits;

use Excel;
use App\Exports\PacksExport;

class PackController extends Controller {
    public function __construct() {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('pack_read', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['index', 'show']]);




    }

    use AccountActivationTraits,
        GvpTraits,
        PackBlockTraits,
        SalesForceTraits,
        DrupalTraits;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if($request->season != ''){
            $seasonId = $request->season;
         }else{
            $season = \App\Season::where('status',1)->first();
            if(empty($season)){
                alert()->error('No active season', 'Warning!');
                return redirect()->route('admin.season.index');
            }
            $seasonId = $season->id;
         }
        $userCounts = \DB::table('user_packs')->selectRaw('count(*) AS total, sum(case when account_activation_status = 3 then 1 else 0 end) AS vgs_synced, sum(case when account_activation_status != 3 then 1 else 0 end) AS vgs_not_synced, sum( IF( (select count(*) from user_car_packs where user_car_packs.pack_id = user_packs.pack_id  AND designa_card_uid IS NOT NULL AND  designa_setv_status = 1 AND designa_acc_status = 1) > 0, 1,0) ) as designa_synced, sum( IF( (select count(*) from user_car_packs where user_car_packs.pack_id = user_packs.pack_id  AND (designa_card_uid IS NULL OR  designa_setv_status = 0 OR designa_acc_status = 0)) > 0, 1,0) ) as designa_not_synced')->join('packs', 'user_packs.pack_id', 'packs.id')->where('packs.season_id',$seasonId);
        $configurations = \App\ApiConfiguration::where('name','valid_pack_limit')->first();

        if(isset($request->limit) && $request->limit == 'valid'){
            $userCounts->where('packs.pack_number','<=',(isset($configurations->value) ? $configurations->value : 10000));
        }
        $userCounts = $userCounts->first();
        $userWithoutCar = 0;
        /*$userCarIds = \DB::table('user_cars')->whereNotNull('user_id')->pluck("user_id");
        if (!empty($userCarIds)) {
            $userWithoutCar = \DB::table('users')->join('packs', 'users.pack_id', 'packs.id')->whereNotIn('users.id', $userCarIds)->where('packs.season_id',$seasonId);
            if(isset($request->limit) && $request->limit == 'valid'){
                $userWithoutCar->where('packs.pack_number','<=',(isset($configurations->value) ? $configurations->value : 10000));
            }
            $userWithoutCar = $userWithoutCar->count();
        }*/
        
        $userWithCar = \App\UserCarPack::join('packs','packs.id','user_car_packs.pack_id')            
                                ->where('packs.season_id',$seasonId)
                                ;
        if(isset($request->limit) && $request->limit == 'valid'){
            $userWithCar->where('packs.pack_number','<=',(isset($configurations->value) ? $configurations->value : 10000));
        }
        $user_pack_ids = $userWithCar->pluck('packs.id');
        if(!empty($user_pack_ids)){
            $userWithoutCar = \App\UserPack::join('packs','packs.id','user_packs.pack_id')->where('packs.season_id',$seasonId);
            if(isset($request->limit) && $request->limit == 'valid'){
                $userWithoutCar->where('packs.pack_number','<=',(isset($configurations->value) ? $configurations->value : 10000));
            }
            $userWithoutCar = $userWithoutCar->whereNotIn('user_packs.pack_id',$user_pack_ids)->count();
           // dd($userWithoutCar);
        }

        $limit = (isset($request->limit) && $request->limit == 'valid') ? 'valid' : 'all';



        return view('admin.pack.index', compact('userCounts', 'userWithoutCar','limit','seasonId'));
    }

    public function datatable(Request $request) {

        $packs = \DB::table('packs')->select('packs.id', 'packs.pack_number', 'users.first_name', 'users.last_name', 'users.email', 'users.mobile', 'users.id as user_id', 'user_packs.account_activation_status', 'users.drupal_user_id','packs.activation_code','packs.prefix','packs.category','user_packs.id as upId')
                ->leftJoin('user_packs', 'user_packs.pack_id', 'packs.id')
                ->leftJoin('users', 'users.id', 'user_packs.user_id')
                ->where('packs.season_id',$request->seasonId)->orderby('pack_number','asc')
                ->whereNotNull('packs.sale_id')
                ->where('packs.sale_id','!=','')
                ;
        if ($request->designa_status != '') {
            if ($request->designa_status == 'synced') {
                $packs->whereNotNull('user_packs.designa_customer_id');
                $packs->whereExists(function ($query) {
                    $query->select(\DB::raw(1))
                            ->from('user_car_packs as uc')
                            ->whereRaw("uc.pack_id = packs.id AND designa_card_uid IS NOT NULL AND  designa_setv_status = 1 AND designa_acc_status = 1");
                });
            } else {
                //$packs->whereNull('users.designa_customer_id');
                $packs->whereExists(function ($query) {
                    $query->select(\DB::raw(1))
                            ->from('user_car_packs as uc')
                            ->whereRaw("uc.pack_id = packs.id AND (designa_card_uid IS NULL OR  designa_setv_status = 0 OR designa_acc_status = 0)");
                });
            }
        }
        if ($request->limit == 'valid') {
            $configurations = \App\ApiConfiguration::where('name','valid_pack_limit')->first();
            $packs->where('packs.pack_number','<=',(isset($configurations->value) ? $configurations->value : 10000) );
        }
        if ($request->vgs_status != '') {
            if ($request->vgs_status == 'synced') {
                $packs->where('user_packs.account_activation_status', 3);
            } else {
                $packs->where('user_packs.account_activation_status', '!=', 3);
            }
        }
        if ($request->blocked_status != '') {
            $packs->where('user_packs.blocked_status', $request->blocked_status );
        }
        return Datatables::of($packs)
                        ->rawColumns(['actions'])
                        ->editColumn('actions', function ($packs) {
                            $user = Auth::guard('admin')->user();
                            $b = '';
                            $b .= ' <a class="btn btn-info btn-sm " href="' . route('admin.packs.show', $packs->id) . '">View</a>';
                            if ($packs->email != NULL && ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('pack_update', 'admin'))) {
                                $b .= ' <a class="btn btn-primary btn-sm btnActivate" href="javascript:void(0)" data-id="' . $packs->id . '">Sync</a>';
                            }
                            if ($packs->account_activation_status == 3) {
                                $b .= ' <label class="label label-success">VGS synced</label>';
                            }
                            if (!empty($packs->user_id)) {
                                $carCount = \App\UserCarPack::where('pack_id', $packs->id)
                                        ->where(function($q) {
                                            $q->whereNull('designa_card_uid')
                                            ->orWhere('designa_setv_status', 0)
                                            ->orWhere('designa_acc_status', 0);
                                        })
                                        ->count();
                                if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('pack_update', 'admin')) {
                                    if ($carCount > 0) {
                                        $b .= ' <a class="btn btn-primary btn-sm btnSyncDesigna" href="javascript:void(0)" data-id="' . $packs->upId . '">Sync Designa</a>';
                                    }
                                    //if ($packs->drupal_user_id == NULL && env('ENABLE_DRUPAL_SYNC') == TRUE) {
                                    $b .= ' <a class="btn btn-primary btn-sm btnSyncDrupal" href="javascript:void(0)" data-id="' . $packs->id . '">Sync Drupal</a>';
                                }
                                if ($packs->drupal_user_id != NULL) {
                                    $b .= ' <label class="label label-success">Drupal synced</label>';
                                }
                                // }
                                //$b .= ' <a class="btn btn-warning btn-sm" href="' . route('admin.packs.edit', $packs->user_id) . '">Edit</a>';
                                 $currentUser = Auth::guard('admin')->user();
                                 if($currentUser->email == "amrutha@mobiiworldcorp.com"){
                                    $b .= ' <a class="btn btn-danger btn-sm" href="' . route('admin.packs.removeOffers', $packs->id) . '">Remove Offers</a>';
                                 }

                                 if($user->hasRole('super-admin', 'admin') && !empty($packs->user_id) && env('APP_ENV') != 'production'){
                                    $b .= ' <a class="btn btn-warning btn-xs" style="margin-top:2px!important;" target="_blank" href="' . route('my.activated.pack',['en', $packs->upId]) . '">Activated Page</a>';
                                 }

                            }
                            return $b;
                        })
                        ->make(true);
    }

    public function removeOffers($packId){
        $currentUser = Auth::guard('admin')->user();
        if($currentUser->email == "amrutha@mobiiworldcorp.com"){
            \App\PartnerOffer::where('pack_id',$packId)->delete();
            alert()->success('Offer Removed', 'removed!');

        }else{
            alert()->error('No permission', 'permission denied!');
        }
        return redirect()->route('admin.packs.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $pack = \App\Pack::select('packs.id as pId', 'packs.prefix', 'packs.pack_number', 'packs.sale_id', 'users.id as user_id', 'users.first_name', 'users.last_name', 'users.email', 'users.mobile', 'users.mobile_dial_code', 'users.gender', 'users.dob', 'users.nationality_id', 'users.recieve_updates', 'user_packs.account_activation_status', 'user_packs.account_id', 'user_packs.designa_customer_uid', 'user_packs.designa_customer_id','packs.car_activation_allowed','user_packs.blocked_status','user_packs.id as upId')
                ->leftJoin('user_packs', 'user_packs.pack_id', 'packs.id')
                ->leftJoin('users', 'users.id', 'user_packs.user_id')
                ->where('packs.id', $id)
                ->first();
        if(empty($pack)){
            alert()->success('No packs found', 'Not found!');
            return redirect()->route('admin.packs.index');
        }

        $userCars =  [];
        if(!empty($pack->user_id)) {
            $userCars =  \App\UserCarPack::join("new_user_cars","new_user_cars.id","user_car_packs.user_car_id")->select('new_user_cars.*','user_car_packs.designa_card_uid','user_car_packs.designa_card_id','user_car_packs.designa_park_id','user_car_packs.designa_setv_status','user_car_packs.designa_acc_status','user_car_packs.id as ucp_id')->where("user_car_packs.pack_id",$pack->pId)->get()->each(function($car){
                $car->country = \App\CarCountry::find($car->country_id);
                $car->emirate = \App\Emirate::find($car->emirate_id);
            }); 
        } 

        $blockHistory = \App\PackBlockHistory::where('user_id',$pack->user_id)->orderby('created_at','desc')->get();
        $adminList = \App\Admin::all()->keyBy('id');
        //dd($adminList);
        return view('admin.pack.show', compact('pack', 'userCars','blockHistory','adminList'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $userPack = \App\UserPack::findOrFail($id);
        $user = \App\User::where('id', $userPack->user_id)->first();
        $pack = \App\Pack::where('id', $userPack->pack_id)->first();
        $user->pack = $pack;
        $userCars = \App\UserCarPack::join("new_user_cars","new_user_cars.id","user_car_packs.user_car_id")->select('new_user_cars.*','user_car_packs.designa_card_uid','user_car_packs.designa_card_id','user_car_packs.designa_park_id','user_car_packs.designa_setv_status','user_car_packs.designa_acc_status','user_car_packs.id as ucp_id')        
        ->where("user_car_packs.pack_id",$userPack->pack_id)->get();
        //dd($user);
        $user->car = $userCars;
        $user->dob = !empty($user->dob) ? date('d-m-Y', strtotime($user->dob)) : null;
        $cnts = \App\Country::select('id', "name_en as name", 'code', 'dial_code')->get();
        $countries = $cnts->pluck('name', 'id');
        $dialcodes = [];
        foreach ($cnts as $c) {
            $dialcodes[str_replace('+', '', $c['dial_code'])] = $c['dial_code'];
        }

        $car_countries = \App\CarCountry::select('id', "name_en as name")->pluck('name', 'id')->prepend('Select');
        return view('admin.pack.edit', compact('userPack','user', 'countries', 'dialcodes', 'car_countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //dd($request->all());
        $user = \App\User::findOrFail($id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->mobile_dial_code = $request->mobile_dial_code;
        $user->mobile = $request->mobile;
        $user->gender = $request->gender;
        $user->dob = date('Y-m-d', strtotime($request->dob));
        $user->nationality_id = $request->nationality_id;
        $user->residence_id = $request->residence_id;
        $user->save();

        foreach ($request->car_id as $k => $v) {
            if ($v != 'new') {
                $user_car = \App\UserCar::where('id', $v)->update([
                    'country_id' => !empty($request->car_country[$k]) ? $request->car_country[$k] : null,
                    'emirate_id' => !empty($request->emirate[$k]) ? $request->emirate[$k] : null,
                    'plate_prefix' => !empty($request->plate_prefix[$k]) ? $request->plate_prefix[$k] : null,
                    'plate_number' => !empty($request->plate_number[$k]) ? $request->plate_number[$k] : null,
                ]);
            } else {
                if (!empty($request->plate_prefix[$k])) {
                    $user_car = new \App\UserCar();
                    $user_car->user_id = $user->id;
                    $user_car->country_id = !empty($request->car_country[$k]) ? $request->car_country[$k] : null;
                    $user_car->emirate_id = !empty($request->emirate[$k]) ? $request->emirate[$k] : null;
                    $user_car->plate_prefix = $request->plate_prefix[$k];
                    $user_car->plate_number = !empty($request->plate_number[$k]) ? $request->plate_number[$k] : null;
                    $user_car->save();
                }
            }
        }

        

        alert()->success('User Info', 'updated!');
        return redirect()->route('admin.packs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function activate(Request $request) {
        $userPack = \App\Pack::select('packs.id', 'packs.prefix', 'packs.pack_number', 'packs.sale_id', 'users.id as user_id', 'users.first_name', 'users.last_name', 'users.email', 'users.mobile', 'users.mobile_dial_code', 'users.gender', 'users.dob', 'users.nationality_id', 'users.recieve_updates','user_packs.id as user_pack_id')
        ->join('user_packs', 'user_packs.pack_id', 'packs.id')
        ->join('users', 'users.id', 'user_packs.user_id')
        ->where('packs.id', $request->id)->first();
        $this->activateAccount($userPack);
        $uPack = \App\UserPack::find($userPack->user_pack_id);       
        $message = 'All Synced';
        if ($uPack->account_activation_status == 3) {
            $message = 'All Synced';
        } elseif ($uPack->account_activation_status == 2) {
            $message = 'Not synced all. Save code synced.';
        } elseif ($uPack->account_activation_status == 1) {
            $message = 'Not synced all. Assign account synced.';
        } else {
            $message = 'Not synced';
        }
        return response()->json(["status" => true, "message" => $message]);
    }

    public function syncDesigna(Request $request) {
        //return response()->json(["status" => true, "message" => 'User cars synced.', 'responseLog' => ['request' => ['sad'=>31,'sad'=>56], 'response' => 'aasdjgasdjgj']]);
        $errArray = [];
        $config = \App\ApiConfiguration::select('name', 'value')->get()->keyBy('name')->toArray();
        $user = \App\Pack::select('packs.id', 'packs.prefix', 'packs.pack_number', 'packs.sale_id', 'users.id as user_id', 'users.first_name', 'users.last_name', 'users.email', 'users.mobile', 'users.mobile_dial_code', 'users.gender', 'users.dob', 'users.nationality_id', 'users.recieve_updates','user_packs.id as user_pack_id','users.residence_id')
                ->join('user_packs', 'user_packs.pack_id', 'packs.id')
                ->join('users', 'users.id', 'user_packs.user_id')
                ->where('user_packs.id', $request->id)
                ->first();
        $user->residence = (!empty($user->residence_id)) ? \App\Country::find($user->residence_id) : NULL;
        $cars = \App\UserCarPack::join("new_user_cars","new_user_cars.id","user_car_packs.user_car_id")->select('new_user_cars.*','user_car_packs.designa_card_uid','user_car_packs.designa_card_id','user_car_packs.designa_park_id','user_car_packs.designa_setv_status','user_car_packs.designa_acc_status','user_car_packs.id as ucp_id')->where("user_car_packs.pack_id",$user->id)->get()->each(function($car){
            $car->country = \App\CarCountry::find($car->country_id);
            $car->emirate = \App\Emirate::find($car->emirate_id);
        });
        $userActivatedPack = \App\UserPack::find($user->user_pack_id);
        if ($user) {
            try {
                if (empty($userActivatedPack->designa_customer_uid)) {
                    $details3 = '';
                    foreach ($cars as $car) {
                        $details3 .= ($car->country) ? $car->country->code : null;
                        if ($car->emirate) {
                            $details3 .= '$' . $car->emirate->code;
                        }
                        $pl = generatePlateCode($car->plate_prefix,$car->plate_number,'$');
                        $details3 .= '$' . $pl . '^';
                    }

                    $userRequest = [
                        'user' => $config['designa_user']['value'],
                        'pwd' => $config['designa_password']['value'],
                        'lastName' => $user->last_name,
                        'firstName' => $user->first_name,
                        'country' => (isset($user->residence) && !empty($user->residence)) ?  $user->residence->code : '',
                        'phone' => $user->mobile_dial_code . $user->mobile,
                        'email' => $user->email,
                        'detail1' => $user->prefix . $user->pack_number,
                        'detail2' => $user->id,
                        'detail3' => trim($details3, '^'),
                        'erroLog' => true
                    ];
                    $xml = $this->createCustomer($userRequest);
                    if (!empty($xml)) {
                        $xml = simplexml_load_string($xml);
                        $xml->registerXPathNamespace('ns1', 'http://www.designa.de/');
                        $xml = $xml->xpath('//ns1:createCustomerFullResponse/ns1:createCustomerFullResult');
                        if (isset($xml[0])) {
                            $xml = $xml[0];
                            $json = json_encode($xml);
                            $response = json_decode($json, true);
                            if (isset($response['@attributes'])) {
                                $userActivatedPack->designa_customer_uid = $response['@attributes']['UId'];
                                $userActivatedPack->designa_customer_id = $response['@attributes']['ID'];
                                $userActivatedPack->save();
                            }
                        } else {
                            array_push($errArray, ['request' => $userRequest, 'response' => $xml]);
                        }
                    }else{
                        array_push($errArray, ['request' => $userRequest, 'response' =>'Designa createCustomerFull API Failed']);
                    }
                }//if designa user uid null
                //Create Designa Card for each car
                if (!empty($userActivatedPack->designa_customer_id)) {
                    foreach ($cars as $car) {
                        $ucar = \App\UserCarPack::find($car->ucp_id);
                        if (empty($ucar->designa_card_uid)) {
                            $cardRequest = [
                                'user' => $config['designa_user']['value'],
                                'pwd' => $config['designa_password']['value'],
                                'personID' => $userActivatedPack->designa_customer_id,
                                'cardType' => $config['designa_card_type']['value'],
                                'carparkID' => $config['designa_carpark_id']['value'],
                                'groupID' => $config['designa_group_id']['value'],
                                'applicID' => $config['designa_application_id']['value'],
                                'expiryDate' => trim($config['designa_expiration_date']['value']),
                                'erroLog' => true
                            ];

                            $xml = $this->createCard($cardRequest);
                            if (!empty($xml)) {
                                $xml = simplexml_load_string($xml);
                                $xml->registerXPathNamespace('ns1', 'http://www.designa.de/');
                                $xml = $xml->xpath('//ns1:createCardResponse/ns1:createCardResult');
                                if (isset($xml[0])) {
                                    $xml = $xml[0];
                                    $json = json_encode($xml);
                                    $response = json_decode($json, true);
                                    if (isset($response['@attributes'])) {
                                        $ucar->designa_card_uid = $response['@attributes']['UId'];
                                        $ucar->designa_card_id = $response['@attributes']['ID'];
                                        $ucar->designa_park_id = isset($response['Carpark']['@attributes']) ? $response['Carpark']['@attributes']['UId'] : null;
                                    }
                                } else {
                                    array_push($errArray, ['request' => $cardRequest, 'response' => $xml]);
                                }
                            }else{
                                array_push($errArray, ['request' => $cardRequest, 'response' =>'Designa createCard API Failed']);
                            }
                        }//designa_card_uid empty

                        if ($ucar->designa_setv_status == 0) {
                            $ccd = (!empty($car->country) ? $car->country->code : null);
                            $ecd = (!empty($car->emirate) ? $car->emirate->code : null);
                            $vehicleRequest = [
                                'user' => $config['designa_user']['value'],
                                'pwd' => $config['designa_password']['value'],
                                'personGuid' => $userActivatedPack->designa_customer_uid,
                                'CountryCode' => $ccd . $ecd,
                                'LicensePlate' => generatePlateCode($car->plate_prefix, $car->plate_number),
                                'VIPCard' => $ucar->designa_card_uid,
                                'erroLog' => true
                            ];
                            $xml = $this->setVehicle($vehicleRequest);
                            if (!empty($xml)) {
                                $xml = simplexml_load_string($xml);
                                $xml->registerXPathNamespace('ns1', 'http://www.designa.de/');
                                $xml = $xml->xpath('//ns1:setVehicleResponse/ns1:setVehicleResult');
                                if (isset($xml[0])) {
                                    $xml = $xml[0];
                                    $json = json_encode($xml);
                                    $response = json_decode($json, true);
                                    if (isset($response[0])) {
                                        if ($response[0] == 0) {
                                            $ucar->designa_setv_status = 1;
                                        }
                                    }
                                } else {
                                    array_push($errArray, ['request' => $vehicleRequest, 'response' => $xml]);
                                }
                            }else{
                                array_push($errArray, ['request' => $vehicleRequest, 'response' =>'Designa vehicle API Failed']);
                            }
                        }//if designa set vehicle not done

                        if ($ucar->designa_acc_status == 0) {
                            $ccd = (!empty($car->country) ? $car->country->code : null);
                            $ecd = (!empty($car->emirate) ? $car->emirate->code : null);
                            $pl = generatePlateCode( $car->plate_prefix,$car->plate_number,' ');
                            $assignRequest = [
                                'user' => $config['designa_user']['value'],
                                'pwd' => $config['designa_password']['value'],
                                'cardUId' => $ucar->designa_card_uid,
                                'cardCarrierKindId' => $config['designa_card_carrier_kind_id']['value'],
                                'carrierString' => $ccd . $ecd . '-' . $pl,
                                'erroLog' => true
                            ];
                            $xml = $this->assignCardCarrier($assignRequest);
                            if (!empty($xml)) {
                                $xml = simplexml_load_string($xml);
                                $xml->registerXPathNamespace('ns1', 'http://www.designa.de/');
                                $xml = $xml->xpath('//ns1:assignCardCarrierResponse/ns1:assignCardCarrierResult');
                                if (isset($xml[0])) {
                                    $xml = $xml[0];
                                    $json = json_encode($xml);
                                    $response = json_decode($json, true);
                                    if (isset($response[0])) {
                                        if ($response[0] == 0) {
                                            $ucar->designa_acc_status = 1;
                                        }
                                    }
                                } else {
                                    array_push($errArray, ['request' => $assignRequest, 'response' => $xml]);
                                }
                            }else{
                                array_push($errArray, ['request' => $assignRequest, 'response' =>'Designa assignCardCarrier API Failed']);
                            }
                        }//if designa assign card career not done
                        $ucar->save();
                    }
                }//if designa_customer_id not null
                return response()->json(["status" => true, "message" => 'User cars synced.', 'responseLog' => $errArray]);
            } catch (\Exception $e) {
                return response()->json(["status" => false, "message" => $e->getMessage()]);
            }
        } else {
            return response()->json(["status" => false, "message" => 'User has no cars to sync.']);
        }
    }

    public function syncDrupal(Request $request) {
        set_time_limit(0);
        if ($response = $this->createDrupalUser($request->id)) {
            return response()->json(["status" => true, "message" => $response]);
        }
        return response()->json(["status" => false, "message" => "Not synced."]);
    }

    public function syncAllDrupal(Request $request) {
        set_time_limit(0);
        $season = \App\Season::where('status',1)->first();
        $seasonId = (!(empty($season))) ? $season->id : NULL;
        $packs = \App\Pack::select('packs.id as pId')
                ->join('users', 'users.pack_id', 'packs.id')
                ->whereNull('packs.drupal_user_id')
                ->where('packs.season_id',$seasonId)
                ->get();
        foreach ($packs as $pk) {
            $this->createDrupalUser($pk->pId);
        }
        return response()->json(["status" => true, "message" => "Synced successfully."]);
    }

    public function editUser($id){
        $userPack = \App\UserPack::findOrFail($id);
        $user = \App\User::where('id', $userPack->user_id)->first();
        $pack = \App\Pack::where('id', $userPack->pack_id)->first();
        $user->pack = $pack;
        $userCars = \App\UserCarPack::join("new_user_cars","new_user_cars.id","user_car_packs.user_car_id")->select('new_user_cars.*','user_car_packs.designa_card_uid','user_car_packs.designa_card_id','user_car_packs.designa_park_id','user_car_packs.designa_setv_status','user_car_packs.designa_acc_status','user_car_packs.id as ucp_id')        
        ->where("user_car_packs.pack_id",$userPack->pack_id)->get();
        //dd($user);
        $user->car = $userCars;

        $user->dob = !empty($user->dob) ? date('d-m-Y', strtotime($user->dob)) : null;
        $cnts = \App\Country::select('id', "name_en as name", 'code', 'dial_code')->get();
        $countries = $cnts->pluck('name', 'id');
        $emirate = \App\Emirate::select('id','code','name_en','name_ar')->get()->pluck('name_en', 'id');
        $dialcodes = [];
        $uaeId = '';
        $dialcodes[''] = '--Select--';
        foreach ($cnts as $c) {
            $dialcodes[str_replace('+', '', $c['dial_code'])] = $c['dial_code'];
            if( $c['dial_code'] == '+971'){
                $uaeId = $c['id'];
            }
        }
        $emirate->prepend('--Select--','');

        return view('admin.pack.edit_user', compact('user','userPack', 'countries', 'dialcodes','emirate','uaeId'));
    }
     public function saveUser(Request $request, $id) {
        //dd($request->all());
        $uPack = \App\UserPack::findOrFail($id);
        $user = \App\User::findOrFail($uPack->user_id);
        $oldUser = $user;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        if($uPack->account_id == NULL){
            $user->email = $request->email;
        }
        $user->mobile_dial_code = $request->mobile_dial_code;
        $user->mobile = $request->mobile;
        $user->gender = $request->gender;
        $user->dob = date('Y-m-d', strtotime($request->dob));
        $user->nationality_id = $request->nationality_id;
        $user->residence_id = $request->residence_id;
        $user->emirate_id = $request->emirate_id;
        $user->save();
        if($uPack->account_id != NULL){
            $userPack = Pack::select('packs.id', 'packs.prefix', 'packs.pack_number', 'packs.sale_id', 'users.id as user_id', 'users.first_name', 'users.last_name', 'users.email', 'users.mobile', 'users.mobile_dial_code', 'users.gender', 'users.dob', 'users.nationality_id', 'users.recieve_updates','user_packs.account_id')
                        ->join('user_packs', 'user_packs.pack_id', 'packs.id')
                        ->join('users', 'users.id', 'user_packs.user_id')
                        ->where('packs.id', $uPack->pack_id)->first();
            $configurations = \App\ApiConfiguration::all()->keyBy('name');
            $this->updateVGSAccount($userPack,$configurations);
        }
        $pack = \App\Pack::where('id',$uPack->pack_id)->first();
        $agree_offers = ($user->recieve_updates) ? 'True' : 'False';
        //$this->vipSubmissionToSalesForce($user,$pack,$agree_offers,'web','user_update_web_cms');

        $arrayData = array("audit_type"=>'edit-user','table_id'=>$user->id,'table_name'=>'users',"admin_id"=>Auth::guard('admin')->user()->id,"admin_type"=>Auth::guard('admin')->user()->type,"event"=>"User updated","old_values"=>$oldUser,"new_values"=>$user,"url"=>"");
         manageAuditLogs($arrayData);
        alert()->success('User Info', 'updated!');
        return redirect()->route('admin.packs.index');
    }

    public function removeUser($userId){
        $uPack = \App\UserPack::findOrFail($userId);
        $user = \App\User::findOrFail($uPack->user_id);
        
        $email = $user->email;
        $pack = \App\Pack::find($uPack->pack_id);        
        $pack->used = 0;
        $pack->drupal_user_id = NULL;
        $pack->save();
        
        $user->pack_id = NULL;
        $user->account_id = NULL;        
        $user->save();

        \App\UserCarPack::where('pack_id',$uPack->pack_id)->delete();
        \App\UserPack::where('pack_id',$uPack->pack_id)->delete();

        $wonderpasses = \App\Wonderpass::select('card_number as mediaCode')->where('pack_id',$pack->id)->get()->toArray();
        $wonderpass = (count($wonderpasses) > 0) ? array_column($wonderpasses, 'mediaCode') : [];
        
        $this->removeDrupalUser($email,$wonderpass);
        return response()->json(["status" => true, "message" => "User removed from pack."]);
    }

    public function export(Request $request){
        ini_set('max_execution_time', '0');
        $input = ['vgs_status' => $request->vgs_status, 'designa_status' => $request->designa_status, 'limit' => $request->limit,'season'=>$request->season];
        return Excel::download(new PacksExport($input), 'packs.xlsx');
    }

    public function updateUserCars(Request $request){

        set_time_limit(0); 

        $limit = !empty($request->limit) ? $request->limit : 10;


        $season = \App\Season::where('status',1)->first();
        $seasonId = (!empty($season)) ?  $season->id : NULL;         
                    
        $usercarsQuery = \App\UserCar::select('users.drupal_user_id', 'user_cars.id', 'user_cars.user_id')
                                    ->join('users', 'users.id', 'user_cars.user_id')
                                    ->where('user_cars.is_vip', 1)
                                    ->where('users.season_id', $seasonId)
                                    ->whereNull('user_cars.drupal_user_id')
                                    ->whereNotNull('users.drupal_user_id')->orderBy('user_cars.id', 'asc')->limit($limit);

        $usercars =   $usercarsQuery->get();

        $usercarsCount = $usercarsQuery->count();

        echo 'Total-'. $usercarsCount;
        $i=0;
         foreach($usercars as $usercar) {
           // dd( $usercar->drupal_user_id); exit;
           $i++;

            $userCr = \App\UserCar::find($usercar->id);
            $userCr->drupal_user_id = $usercar->drupal_user_id;
            $userCr->save();

         }                          


        echo '<br> Updated..-'. $i; 

        echo  '<br> Remianing-' . \App\UserCar::select('users.drupal_user_id', 'user_cars.id', 'user_cars.user_id')
        ->join('users', 'users.id', 'user_cars.user_id')
        ->where('user_cars.is_vip', 1)
        ->where('users.season_id', $seasonId)
        ->whereNull('user_cars.drupal_user_id')
        ->whereNotNull('users.drupal_user_id')->count();


    }


    public function updateTestPacks(){
        set_time_limit(0);
        /*
        $activeSeason = \App\Season::where('status',1)->first();
        $prevSeasonOrder = $activeSeason->ordering - 1;
        $prevSeason = \App\Season::where('status',0)->where('ordering',$prevSeasonOrder)->first();
        $packs = \App\Pack::where('season_id',$prevSeason->id)->where('pack_number','>',10000)->get();
        foreach($packs as $pack){
            $exists = \App\Pack::where('sale_id',$pack->sale_id)->where('pack_number',$pack->pack_number)->where('season_id',$activeSeason->id)->first();
            if(empty($exists)){
                $newPack = new \App\Pack;
                $newPack->sale_id = $pack->sale_id;
                $newPack->activation_code = $pack->activation_code;
                $newPack->prefix = 'S26-';
                $newPack->pack_number = $pack->pack_number;
                $newPack->car_activation_allowed = $pack->car_activation_allowed;
                $newPack->park_entry_count = $pack->park_entry_count;
                $newPack->wonderpass_count = $pack->wonderpass_count;
                $newPack->parking_voucher_count = $pack->parking_voucher_count;
                $newPack->category = $pack->category;
                $newPack->season_id = $activeSeason->id;
                $newPack->save();
            }

        }
        */

    }

     public function blockUser( Request $request,$id){

        $uPack = \App\UserPack::findOrFail($id);
        $user = \App\User::findOrFail($uPack->user_id);
        if($uPack->blocked_status == 'yes'){
            $blockStatus = 'no';
            $blockedDate = NULL;
            $blockedBy = NULL;
            $message = 'User unblocked.';
            $bStatus = 'unblocked';
        }else{

            $message = 'User blocked.';
            $blockStatus = 'yes';
            $blockedDate =  date('Y-m-d H:i:s');
            $blockedBy = Auth::guard('admin')->user()->id;
            $bStatus = 'blocked';
        }
        $uPack->blocked_status = $blockStatus;
        $uPack->blocked_date = $blockedDate;
        $uPack->blocked_by = $blockedBy;
        $uPack->save();

        $blockHistory = new \App\PackBlockHistory;
        $blockHistory->user_id = $uPack->user_id;
        $blockHistory->pack_user_id = $uPack->id;
        $blockHistory->blocked_by = Auth::guard('admin')->user()->id;
        $blockHistory->status = $bStatus;
        $blockHistory->vgs_status = 'Pending';
        $blockHistory->reference_number = $request->reference;
        $blockHistory->save();

        /*If user activated multiple packs*/
        // \App\User::where('email',$user->email)->orWhere(function($query) use ($user){
        //     $query->where('drupal_user_id',$user->drupal_user_id)->whereNotNull('drupal_user_id');

        // })
        // ->where('season_id',$user->season_id)
        // ->update([
        //     'blocked_status' => $blockStatus,
        //     'blocked_date' => $blockedDate,
        //     'blocked_by' => $blockedBy
        // ]);
        //API to drupal
        // $unblockedCount = \App\User::where('email',$user->email)->orWhere(function($query) use ($user){
        //      $query->where('drupal_user_id',$user->drupal_user_id)->whereNotNull('drupal_user_id');
        //  })
        // ->where('season_id',$user->season_id)->count();
        $unblockedCount = \App\UserPack::where('user_id',$user->id)->where('blocked_status','no')->count();
        if($user->drupal_user_id == NULL){
           $this->createDrupalUser($uPack->id);
           $user = \App\User::find($uPack->user_id);
        }
        $pack = \App\Pack::find($uPack->pack_id);
        if($user->drupal_user_id != NULL){
            $this->blockDrupalUser($user->drupal_user_id,$blockStatus,$unblockedCount,$pack->pack_number, $user->id);
        }
        //API to  vgs
        $configurations = \App\ApiConfiguration::all()->keyBy('name');
        $this->blockPackVgs($pack,$configurations,$bStatus,$blockHistory->id);


        return response()->json(["status" => true, "message" => $message]);
    }

    public function removeAllSeasonUsers(){
        $season = \App\Season::where('status',1)->first();
        $allUsers = \App\User::where('season_id',$season->id)->whereNotNull('pack_id')->get();
        foreach($allUsers as $user){            
            $email = $user->email;
            $pack = \App\Pack::find($user->pack_id);        
            $pack->used = 0;
            $pack->drupal_user_id = NULL;
            $pack->save();
            
            $user->pack_id = NULL;
            $user->account_id = NULL;        
            $user->save();

            $wonderpasses = \App\Wonderpass::select('card_number as mediaCode')->where('pack_id',$pack->id)->get()->toArray();
            $wonderpass = (count($wonderpasses) > 0) ? array_column($wonderpasses, 'mediaCode') : [];
            
            //$this->removeDrupalUser($email,$wonderpass);
        }
        
        return response()->json(["status" => true, "message" => "User removed from pack."]);
    }

    public function removetestOffers($from,$to){
        if(env('OFFER_REMOVE') == 'yes'){
             $season = \App\Season::where('status',1)->first();
            $packs = \App\Pack::where('packs.season_id',$season->id)->where('pack_number','>=',$from)->where('pack_number','<=',$to)->get();
            foreach($packs as $pack){
                \App\PartnerOffer::where('pack_id',$pack->id)->delete();
            }
            alert()->success('Offer Removed', 'removed!');
        }else{
            alert()->error('Cant remove offer in production', 'Warning!');
        }

        return redirect()->route('admin.packs.index');
    }

}
