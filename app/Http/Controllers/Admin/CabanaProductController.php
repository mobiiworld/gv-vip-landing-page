<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Auth;
use App\CabanaProduct;

class CabanaProductController extends Controller {

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Cabana_read', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['index']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Cabana_create', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['create', 'store']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('Cabana_update', 'admin')) {
                return $next($request);
            } else {
                alert()->error('Your don\'t have permission to acces this page.', 'No Access!');
                return redirect()->route('admin.dashboard');
            }
        }, ['only' => ['edit', 'update']]);
    }

    public function index() {
        return view('admin.cabana.products.index');
    }

    public function datatable(Request $request) {
        $currentUser = Auth::guard('admin')->user();
        $isSuperAdmin = $currentUser->hasRole('super-admin', 'admin');
        $products = CabanaProduct::select('*');
        if ($request->status != '') {
            $products->where('status', $request->status);
        }
        $products->orderBy('hours');
        return Datatables::of($products)
                        ->rawColumns(['actions', 'status'])
                        ->editColumn('actions', function ($product) use ($currentUser, $isSuperAdmin) {
                            $b = '';
                            if ($isSuperAdmin || $currentUser->hasPermissionTo('Cabana_update', 'admin')) {
                                $b = ' <a class="btn btn-primary btn-sm " href="' . route('admin.cabana-products.edit', $product->id) . '">Edit</a>';
                            }
                            return $b;
                        })
                        ->editColumn('allow_vip', function ($product) {
                            return ($product->allow_vip == 1) ? 'Yes' : 'No';
                        })
                        ->editColumn('status', function ($product) use ($currentUser, $isSuperAdmin) {
                            if ($isSuperAdmin || $currentUser->hasPermissionTo('Cabana_update', 'admin')) {
                                $checked = ($product->status == 1) ? 'checked' : '';
                                return '<input type="checkbox" name="status" id="status_' . $product->id . '"  data-on-text="Enabled" data-off-text="Disabled" value="1" ' . $checked . ' class="bsSwitch" autocomplete="off" data-id="' . $product->id . '">';
                            } else {
                                return ($product->status == 1) ? '<label class="label label-success">Enabled</label>' : '<label class="label label-danger">Disabled</label>';
                            }
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.cabana.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if ($request->has('allow_vip')) {
            $request->allow_vip = true;
        } else {
            $request->allow_vip = false;
        }
        $exist = CabanaProduct::where('hours', $request->hours)->first();
        if ($exist) {
            alert()->error('This hour is already added.', 'Exist');
            return redirect()->back()->withInput();
        }
        $product = CabanaProduct::create($request->only('hours', 'weekend_price', 'workingday_price', 'allow_vip', 'vip_prices'));
        alert()->success('Successfully Created.', 'Created');
        return redirect()->route('admin.cabana-products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $product = CabanaProduct::where('id', $id)->first();
        return view('admin.cabana.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $exist = CabanaProduct::where('hours', $request->hours)->where('id', '<>', $id)->first();
        if ($exist) {
            alert()->error('This hour is already added.', 'Exist');
            return redirect()->back()->withInput();
        }
        $product = CabanaProduct::findOrFail($id);
        $input = $request->only(['hours', 'weekend_price', 'workingday_price', 'vip_prices']);
        if ($request->has('allow_vip')) {
            $input['allow_vip'] = true;
        } else {
            $input['allow_vip'] = false;
        }
        $product->fill($input)->save();
        alert()->success('Successfully Updated.', 'Updated');
        return redirect()->route('admin.cabana-products.index');
    }

    public function changeStatus(Request $request) {
        $product = CabanaProduct::find($request->id);
        $product->status = ($product->status == 1) ? 0 : 1;
        $product->save();
        return response()->json(["status" => true, "message" => 'Successfully changed']);
    }

}
