<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Auth;

class AdminLoginController extends Controller {

    public function __construct() {
        //defining our middleware for this controller
        $this->middleware('guest:admin', ['except' => ['logout']]);
    }

    //function to show admin login form
    public function showLoginForm() {
        return view('auth.admin-login');
    }

    //function to login admins
    public function login(Request $request) {
        //validate the form data
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6',
            'g-recaptcha-response' => 'required'
        ]);
        $secretKey = env('CAPTCHA_SERVER_KEY');
        $captcha = $_POST['g-recaptcha-response'];
        // post request to server
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) . '&response=' . urlencode($captcha);
        $response = file_get_contents($url);
        $responseKeys = json_decode($response, true);

        if (empty($responseKeys["success"])) {
            return redirect()->back()->withInput($request->only('email', 'remember','g-recaptcha-response'))->with('growl', [trans('web.captcha_error'), 'danger']);
        }
        //attempt to login the admins in
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            //if successful redirect to admin dashboard
            Auth::guard('admin')->user()->update([ 'last_login' => Carbon::now()->toDateTimeString() ]);
            //return redirect()->intended(route('admin.dashboard'));
            return redirect()->route('admin.dashboard');
        }
        //if unsuccessfull redirect back to the login for with form data
        return redirect()->back()->withInput($request->only('email', 'remember'))->with('growl', [trans('auth.failed'), 'danger']);
    }

    public function logout() {
        Auth::guard('admin')->logout();

        return redirect('/admin');
    }

}
