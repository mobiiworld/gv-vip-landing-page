<?php

namespace App\Http\Controllers;

use App;

class ApiController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void`
     */
    public function __construct() {
        $this->middleware(['guestApi']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function setApiLanguage($param) {

        App::setLocale($param);

        return $param;
    }

    public function getLanguages() {
        $languages = App\Language::OrderBY('name')->get();

        $data = [];
        $i = 0;
        foreach ($languages as $language) {
            $data[$i]['id'] = $language->id;
            $data[$i]['name'] = $language->name;
            $data[$i]['code'] = $language->code;
            $i++;
        }
        return $this->successResponse('', $data);
    }

    public function getCurrentLanguage() {
        $language = App\Language::where('code', App::getLocale())->first();
        return $language;
    }

}
