<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnerOffer extends Model {

    use Uuids;

    public $incrementing = false;
    protected $table = 'partner_offers';
    protected $primaryKey = 'id';

    protected $fillable = [
        'pack_id', 'parent_merchant_name_en', 'parent_merchant_name_ar', 'merchant_name_en', 'merchant_name_ar', 'offer_name_en', 'offer_name_ar', 'valid_from', 'valid_to','promo_code','code_type', 't_and_c', 'link'
    ];
}
