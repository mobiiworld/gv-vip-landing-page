<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

    use Uuids;

    protected $table = 'notifications';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $casts = [
        'titles' => 'array',
        'messages' => 'array',
    ];

}
