<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackBlockHistory extends Model
{
    use Uuids;
    public $incrementing = false;    
    protected $primaryKey = 'id';
}
