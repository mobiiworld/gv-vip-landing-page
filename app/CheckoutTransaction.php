<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckoutTransaction extends Model
{
    protected $connection = 'mysql2';

    protected $table = 'gv_checkout_transaction';
}
