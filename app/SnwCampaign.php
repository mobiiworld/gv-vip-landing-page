<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SnwCampaign extends Model {

    use Uuids;

    protected $table = 'snw_campaigns';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $casts = [
        'names' => 'array',
    ];

    public function prizes() {
        return $this->hasMany(SnwPrize::class, 'campaign_id', 'id');
    }

    public function images() {
        return $this->hasMany(SnwDayImage::class, 'campaign_id', 'id');
    }

}
