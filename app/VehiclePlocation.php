<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehiclePlocation extends Model {

    use Uuids;

    protected $table = 'vehicle_plocations';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $casts = [
        'zone_names' => 'array',
        'names' => 'array',
        'descriptions' => 'array',
        'extras' => 'array',
    ];

    public function images() {
        return $this->hasMany(VehiclePimage::class, 'plocation_id', 'id');
    }
    public function services() {
        return $this->hasMany(VehiclePlocationService::class, 'vehicle_plocation_id', 'id');
    }

}
