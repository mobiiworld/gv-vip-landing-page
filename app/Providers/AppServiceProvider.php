<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
//use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider {

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        if ((env('APP_ENV') == 'staging') || (env('APP_ENV') == 'production')) {
            \URL::forceScheme('https');
        }
        //Schema::defaultStringLength(191);

        if(env('APP_ENV') != 'production'){ //log all queries in development
            \DB::listen(function($queryExecuted) {

                // \Log::channel('queryLog')->info($sql->sql);
                // \Log::channel('queryLog')->info($sql->bindings);
                // \Log::channel('queryLog')->info($sql->time);

                $sql = $queryExecuted->sql;
                $bindings = $queryExecuted->bindings;
                $time = $queryExecuted->time;

                if($time > 5){
                    try {
                        foreach ($bindings as $val) {
                            $sql = preg_replace('/\?/', "'{$val}'", $sql, 1);
                        }

                        \Log::channel('queryLog')->info( $time . '  ' . $sql);
                    } catch (\Exception $e) {
                        //  be quiet on error
                    }
                }
            });
        }
    }

}
