<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wonderpass extends Model {

    use Uuids;

    public $incrementing = false;
    protected $table = 'wonderpasses';
    protected $primaryKey = 'id';
    protected $fillable = [ 'pack_id', 'card_number', 'balance_points', 'carnaval_ride_points', 'ripleys_count'
    ];

}
