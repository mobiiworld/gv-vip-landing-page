<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FitnessUser extends Model {

    use Uuids;

    protected $table = 'fitness_users';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $guarded = [];

}
