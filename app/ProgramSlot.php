<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramSlot extends Model
{
    use Uuids;
    public $incrementing = false;    
    protected $primaryKey = 'id';

    protected $fillable = [
        'program_id', 'slot_id','table_id', 'program_date'
    ];

    public function slot() {
        return $this->hasOne(Slot::class, 'id', 'slot_id');
    }

     public function fTable() {
        return $this->hasOne(VipTable::class, 'id', 'table_id');
    }
    
    public function bookings() {
        return $this->hasOne(BookSlot::class, 'id', 'booking_id');
    }
}
