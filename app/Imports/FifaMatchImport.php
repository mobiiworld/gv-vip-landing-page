<?php

namespace App\Imports;

use App\FifaMatch;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class FifaMatchImport implements ToModel, WithHeadingRow {

    public $data;
    public $sId;
    private $rows = 0;

    public function __construct($sid) {
        $this->data = collect();
        $this->sId = $sid;
    }

    public function model(array $row) {
        if (isset($row['id']) && !empty($row['id'])) {
            $model = FifaMatch::where('id', trim($row['id']))->first();
            if (empty($model)) {
                $model = new FifaMatch();
                $model->season_id = $this->sId;
            }
        } else {
            $model = new FifaMatch();
            $model->season_id = $this->sId;
        }
        $model->titles = json_decode(json_encode(['en' => $row['title_english'], 'ar' => $row['title_arabic']]));
        $model->match_date = is_string($row['date']) ? date('Y-m-d', strtotime($row['date'])) : \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['date'])->format('Y-m-d');
        $model->match_time = is_string($row['time']) ? date('H:i:00', strtotime($row['time'])) : \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['time'])->format('H:i:00');
        $model->performance_id = $row['performance_id'];
        $this->data->push($model);
        ++$this->rows;
        return $model;
    }

    public function getRowCount(): int {
        return $this->rows;
    }

}
