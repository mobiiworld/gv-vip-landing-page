<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CancelledBookingSlot extends Model {

    use Uuids;

    public $incrementing = false;
    protected $primaryKey = 'id';

}
