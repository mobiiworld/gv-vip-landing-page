<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FitnessPrize extends Model {

    protected $table = 'fitness_prizes';
    protected $primaryKey = 'id';
    protected $casts = [
        'names' => 'array',
        'descriptions' => 'array'
    ];

    public function getIconAttribute($value) {
        return !empty($value) ? cdn('admin/fitness_season/icons/' . $value) : null;
    }

}
