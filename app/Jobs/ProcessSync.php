<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Traits\GvpTraits;
use App\Traits\DrupalTraits;
use App\Traits\AccountActivationTraits;
use Log;


class ProcessSync implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use AccountActivationTraits,
        GvpTraits, DrupalTraits;
    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::channel('syncCurl')->info(date('Y-m-d H:i:s'));
            Log::channel('syncCurl')->info('Job execution start.');
            
            $userPack = \App\Pack::select('packs.id', 'packs.prefix', 'packs.pack_number','packs.category', 'packs.sale_id', 'users.id as user_id', 'users.first_name', 'users.last_name', 'users.email', 'users.mobile', 'users.mobile_dial_code', 'users.gender', 'users.dob', 'users.nationality_id', 'users.recieve_updates','user_packs.id as user_pack_id')
                ->join('user_packs', 'user_packs.pack_id', 'packs.id')
                ->join('users', 'users.id', 'user_packs.user_id')
                ->where('packs.id', $this->data->packId)->first();
            
            try{
                $this->createDrupalUser($userPack->id);
                $mail_data = $userPack->toArray();
                $mail_data['pack_type'] = trans("web.pack.{$userPack->category}");
                $configurations = \App\ApiConfiguration::where('name', 'send_vip_activated_email')->first();
                if ($configurations->value == "enable") {
                    \Mail::to($userPack->email)
                        ->send(new \App\Mail\PackActivated($mail_data));
                }
            }catch(\Exception $e){
                
            }
            
            if(env("ENABLE_EXTERNAL_SYNC",true)){
                Log::channel('syncCurl')->info('Enabled');
                $this->activateAccount($userPack);
                $this->syncUsertoDesigna($userPack->user_pack_id);
            }
           
            
           Log::channel('syncCurl')->info('Job execution.');
        } catch (\Exception $e) {
            Log::channel('syncCurl')->error($e->getMessage());
        }
    }
}
