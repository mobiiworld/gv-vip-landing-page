<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehiclePimage extends Model {

    use Uuids;

    protected $table = 'vehicle_pimages';
    protected $primaryKey = 'id';
    public $incrementing = false;

    public function getImageAttribute($value) {
        return !empty($value) ? cdn('vpl/' . $value) : null;
    }

}
