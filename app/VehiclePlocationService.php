<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehiclePlocationService extends Model {

    use Uuids;

    protected $table = 'vehicle_plocation_services';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $casts = [
        'descriptions' => 'array',
    ];

    public function getIconAttribute($value) {
        return !empty($value) ? cdn('service_icons/' . $value) : null;
    }

    public function service() {
        return $this->hasOne(ParkingServices::class, 'id', 'parking_service_id');
    }

}
