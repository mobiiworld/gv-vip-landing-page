<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCarPack extends Model
{
    use Uuids;
    

    public $incrementing = false;    
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_car_id',
        'pack_id',
    ];
}
