<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarCountry extends Model {
    protected $table = 'car_countries';
    protected $primaryKey = 'id';
}
