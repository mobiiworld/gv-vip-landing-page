<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehiclePsetting extends Model {

    protected $table = 'vehicle_psettings';
    protected $primaryKey = 'id';
    protected $casts = [
        'api_data' => 'array',
    ];
    public $timestamps = false;

}
