<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pack extends Model {
use Uuids;
    public $incrementing = false;
    protected $table = 'packs';
    protected $primaryKey = 'id';
}
