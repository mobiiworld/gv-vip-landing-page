<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiConfiguration extends Model
{
    protected $fillable = ['name','value'];
}
