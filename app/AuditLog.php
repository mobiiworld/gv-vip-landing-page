<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditLog extends Model {
    protected $table = 'audit_log';
    protected $primaryKey = 'id';
    protected $casts = [
        'old_values' => 'array',
        'new_values' => 'array'
        
    ];
    public function getAdminDetail()    {
        return $this->hasOne(Admin::class, 'id', 'admin_id');
    }
}
