<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPack extends Model
{
    use Uuids;
    

    public $incrementing = false;    
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id', 'pack_id',
    ];
}
