<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MajlisBookingStatusHistory extends Model {

    use Uuids;

    protected $table = 'majlis_booking_status_histories';
    public $incrementing = false;
    protected $primaryKey = 'id';

}
