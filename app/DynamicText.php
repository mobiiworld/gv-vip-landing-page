<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DynamicText extends Model
{
    protected $fillable = ['slug', 'text_en', 'text_ar'];

}
