<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emailtemplate extends Model
{
    protected $fillable = ['type', 'subject_en','subject_ar', 'body_en', 'body_ar'];
    
}
