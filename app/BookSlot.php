<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookSlot extends Model {

    use Uuids;

    public $incrementing = false;
    protected $primaryKey = 'id';
    protected $casts = [
        'payment_extras' => 'array'
    ];

    public function admin() {
        return $this->hasOne(Admin::class, 'id', 'admin_id');
    }
}
