<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $table = 'seasons';
    protected $primaryKey = 'id';

    public function getPackActivationDisabledImageAttribute($value) {
        return !empty($value) ? cdn('season/' . $value) : null;
    }
}
