<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Handler extends ExceptionHandler {

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
            //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception) {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception) {
        if ($request->expectsJson()) {
            if ($exception instanceof UnauthorizedHttpException) {
                $preException = $exception->getPrevious();
                if ($preException instanceof
                        \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                    return systemResponse(trans('api.token.expired'));
                } else if ($preException instanceof
                        \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                    return systemResponse(trans('api.token.invalid'));
                } else if ($preException instanceof
                        \Tymon\JWTAuth\Exceptions\TokenBlacklistedException) {
                    return systemResponse(trans('api.token.blacklisted'));
                }
                if ($exception->getMessage() === 'Token not provided') {
                    return systemResponse(trans('api.token.notexist'));
                }
            }
            return systemResponse($exception->getMessage());
        }
        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception) {
        if ($request->expectsJson()) {
            return systemResponse(trans('api.unauthorized'));
        }

        $guard = array_get($exception->guards(), 0);

        switch ($guard) {
            case 'admin':
                $login = 'admin.login';
                break;
            default:
                $login = 'home'; //This is where users are always redirected after a successful login, guards array is always empty
                break;
        }
        return redirect()->guest(route($login));
    }

}
