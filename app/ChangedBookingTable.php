<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChangedBookingTable extends Model {

    use Uuids;

    public $incrementing = false;
    protected $primaryKey = 'id';

    public function admin() {
        return $this->hasOne(Admin::class, 'id', 'admin_id');
    }

}
