<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlateCategory extends Model {
    protected $table = 'plate_categories';
    protected $primaryKey = 'id';
}
