<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SnwPrizeCoupon extends Model {

    use Uuids;

    protected $table = 'snw_prize_coupons';
    protected $primaryKey = 'id';
    public $incrementing = false;
}
