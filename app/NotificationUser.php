<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationUser extends Model {

    use Uuids;

    protected $table = 'notification_users';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $guarded = [];

}
