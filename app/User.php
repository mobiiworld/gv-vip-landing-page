<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

//use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable {

    use Uuids;
    use Notifiable;

    //use SoftDeletes;

    protected $primaryKey = 'id';
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    function activeToken() {
        return $this->hasOne(UserActiveToken::class, "user_id", "id");
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }

    public function residence() {
        return $this->hasOne(Country::class, 'id', 'residence_id');
    }

    public function pack() {
        return $this->hasOne(Pack::class, 'id', 'pack_id');
    }

    /*public function cars() {
        return $this->hasMany(NewUserCar::class, 'user_id', 'id')
                        ->with('country:id,code,name_en,name_ar')
                        ->with('emirate:id,code,name_en,name_ar')
                        ->where(function($q) {
                            $q->whereNull('designa_card_uid')
                            ->orWhere('designa_setv_status', 0)
                            ->orWhere('designa_acc_status', 0);
                        })->orderBY('created_at','desc');
    }
     public function userCars() {
        return $this->hasMany(NewUserCar::class, 'user_id', 'id')
                        ->with('country:id,code,name_en,name_ar')
                        ->with('emirate:id,code,name_en,name_ar')
                        ->orderBY('created_at','desc');
    }*/

}
