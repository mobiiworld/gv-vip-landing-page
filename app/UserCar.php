<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class UserCar extends Model {

    use Uuids;
    use LogsActivity;

    public $incrementing = false;
    protected $table = 'user_cars';
    protected $primaryKey = 'id';

    public function country() {
        return $this->hasOne(CarCountry::class, 'id', 'country_id');
    }

    public function emirate() {
        return $this->hasOne(Emirate::class, 'id', 'emirate_id');
    }

    protected static $logName = 'user-car';
    protected static $logAttributes = ['country_id', 'emirate_id', 'plate_prefix', 'plate_number'];
    protected static $logOnlyDirty = true; // to log only the attributes which are updated

    public function getDescriptionForEvent(string $eventName): string
    {
        return "User car has been {$eventName}";
    }
}
