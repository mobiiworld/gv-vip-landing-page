<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommonConfiguration extends Model
{
    protected $fillable = ['name','value'];
}
