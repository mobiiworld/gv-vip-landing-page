<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slot extends Model {

    use Uuids;

    protected $table = 'slots';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $casts = [
        'vgs_pcodes' => 'array',
    ];

    // protected $fillable = [
    //     'id', 'name', 'priority', 'status'
    // ];
}
