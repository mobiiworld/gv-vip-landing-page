<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaveMyParking extends Model {

    use Uuids;

    public $incrementing = false;
    protected $primaryKey = 'id';
    protected $table = 'save_my_parkings';

}
