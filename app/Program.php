<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model {

    use Uuids;

    public $incrementing = false;
    protected $primaryKey = 'id';
    protected $casts = [
        'vgs_pcodes' => 'array',
    ];

}
