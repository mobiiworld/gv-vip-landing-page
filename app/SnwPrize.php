<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SnwPrize extends Model {

    use Uuids;

    protected $table = 'snw_prizes';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $casts = [
        'names' => 'array',
        'descriptions' => 'array',
    ];

    function campaign() {
        return $this->hasOne(SnwCampaign::class, "id", "campaign_id");
    }

    public function coupons() {
        return $this->hasMany(SnwPrizeCoupon::class, 'prize_id', 'id');
    }

}
