<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FitnessDailyTracks extends Model {

    use Uuids;

    protected $table = 'fitness_daily_tracks';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $casts = [
        'sf_request' => 'array',
        'sf_response' => 'array'
    ];

}
