<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CabanaProduct extends Model {

    use Uuids;

    public $incrementing = false;
    protected $table = 'cabana_products';
    protected $primaryKey = 'id';
    protected $casts = [
        'vip_prices' => 'array'
    ];
    protected $guarded = [];

}
