<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FifaMatch extends Model {

    use Uuids;

    public $incrementing = false;
    protected $table = 'fifa_matches';
    protected $primaryKey = 'id';
    
    protected $casts = [
        'titles' => 'array',
    ];

}
