<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CabanaDay extends Model {

    use Uuids;

    public $incrementing = false;
    protected $table = 'cabana_days';
    protected $primaryKey = 'id';

}
