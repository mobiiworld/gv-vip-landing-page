<?php

namespace App\Traits;

use Intervention\Image\Facades\Image;
use Storage;
use File;

/**
 * This is a trait use full to store image either in s3 or in local storage
 *
 * @author Shainu
 * Usage : $resizedImage = $this->multiImage($request->file('avatar'), 'avatar', 'multi_');
 */
trait ImageTraits {



    private function resize($image, $path) {
        $imageName = rand(1, time()) . '.' . $image->getClientOriginalExtension();

        try {
            if (!env('CDN_ENABLED', false)) {
                $image = $image->getRealPath();
                $path = $path . '/';
                $disk = 'public';
            } else {
                $path = $path . '/';
                $disk = 'azure';
            }
            $img = Image::make($image);
            Storage::disk($disk)->put($path . $imageName, $img->stream()->detach(), 'public');

            return $imageName;
        } catch (\Exception $e) {
            return false;
        }
    }

    private function resizeCropImage($image, $path) {
        $imageName = rand(1, time()) . '.' . "png";
        try {
            if (!env('CDN_ENABLED', false)) {
                $path = $path . '/';
                $disk = 'public';
            } else {
                $path = $path . '/';
                $disk = 'azure';
            }
            $img = Image::make($image);
            Storage::disk($disk)->put($path . $imageName, $img->stream()->detach(), 'public');

            return $imageName;
        } catch (\Exception $e) {
            return false;
        }
    }

}
