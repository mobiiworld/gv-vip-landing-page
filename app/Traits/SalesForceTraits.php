<?php
namespace App\Traits;

trait SalesForceTraits
{
 
    function getAccessToken($refenrence_id=NULL){
        $config = \App\CommonConfiguration::select('name', 'value')->whereIn('name',['saleforce-url','salesforce-token'])->get()->keyBy('name')->toArray();
        $headers = ['Content-Type: application/x-www-form-urlencoded', 'content: application/x-www-form-urlencoded'];
        $requestData = http_build_query( [
          'grant_type' => 'urn:ietf:params:oauth:grant-type:jwt-bearer',
          'assertion' => $config['salesforce-token']['value'],
        ]);
        $url = $config['saleforce-url']['value'].'/services/oauth2/token?'.$requestData;
        $response = curlRequestPost($url,[],$headers);      
        $sales_force_log = new \App\SalesforceLog;
        $sales_force_log->type = 'authentication';        
        $sales_force_log->response = json_encode($response);
        $sales_force_log->sale_request = json_encode($requestData);        
        $sales_force_log->save();  
        if (isset($response["access_token"])) {
          return $response;
        }
        return false;
        
        // $requestData = "grant_type=password" .
        //   "&client_id=" . env('SALES_FORCE_CLIENT_ID') .
        //   "&client_secret=" . env('SALES_FORCE_CLIENT_SECRET') .
        //   "&username=" . env('SALES_FORCE_USERNAME') .
        //   "&password=" . env('SALES_FORCE_PASSWORD'); 

          /*$requestData = [
            'grant_type' => 'password',
            'client_id' => env('SALES_FORCE_CLIENT_ID'),
            'client_secret' => env('SALES_FORCE_CLIENT_SECRET'),
            'username' => env('SALES_FORCE_USERNAME'),
            'password' => env('SALES_FORCE_PASSWORD'),
          ];
          $query_string = http_build_query($requestData);
          $url = env('SALES_FORCE_AUTH_URL').$query_string;
          $response = curlRequestPost($url,[],[]);

         
          if (isset($response["access_token"])) {
            return $response['access_token'];
          }
          return false;*/
      }

     function vipSubmissionToSalesForce($user,$pack,$agree_offers='True',$source='Website',$log_type="vip_activation",$platform=''){
        $access = $this->getAccessToken($pack->id);
        
        if(empty($access) || !isset($access["access_token"])){
            return false;
        }
        $locale = app()->getLocale();
        $country = \App\Country::where('id',$user->nationality_id)->first();
        $nationality = (isset($country->name_en)) ? $country->name_en : '';
        $residence = \App\Country::where('id',$user->residence_id)->first();
        $countryOfResidence = (isset($residence->name_en)) ?  $residence->name_en : '';
        $remirate = \App\Emirate::where('id',$user->emirate_id)->first();
        $emirateOfResidence = (isset($remirate->name_en)) ? $remirate->name_en : '';
        if(empty($residence) || (!empty($residence) && $residence->code != 'UAE' && $residence->code != 'ARE')){            
          $nationality = '';
        }
        // $countryOfResidence = "United Arab Emirates";
        // $nationality = "India";
        // $emirateOfResidence = "Dubai";
        $mobile_number = $user->mobile_dial_code.$user->mobile;
        $dial_code = (substr(trim($user->mobile_dial_code), 0, 1) === '+') ? $user->mobile_dial_code : '+'.$user->mobile_dial_code;
        $param = [  
                'firstName' => $user->first_name,
                'lastName' => $user->last_name,
                'email' => $user->email,                      
                'language' => ($locale == 'ar') ? "AR" : "EN",
                'countryCode' => $dial_code,
                'phone' => $user->mobile,                 
                'asset' => 'Global Village' ,
                'packNumber' => (int) $pack->pack_number,
                'personalisedURL' => route('my.activated.pack', [$locale, $user->user_pack_id]),
                'productName' => trans("web.sales_pack_categories.{$pack->category}"),
                'acceptTerms' => true,               
                
        ];
        // if(!empty($user->gender)){
        //   $param['gender'] = ucfirst($user->gender);
        // }
        if(!empty($user->dob)){
          $param['birthdate'] = ($user->dob != NULL)  ? date("Y-m-d",strtotime($user->dob)) : '';
        }
        if(!empty($platform)){
          $param['platform'] = $platform;
        }
        if(!empty($source)){
          $param['source'] = $source;
        }
        if(!empty($agree_offers)){
          $param['receiveUpdates'] = $agree_offers;
        }
        if(!empty($countryOfResidence)){
          $param['country'] = $countryOfResidence;
        }
        if(!empty($nationality)){
          $param['nationality'] = $nationality;
        }
        if(!empty($emirateOfResidence)){
          $param['state'] = $emirateOfResidence;
        }
        
        $url = $access["instance_url"].'/services/apexrest/consent/vipactivation';
        $accessToken = $access["access_token"];
        $headers = array(
          "Accept: application/json",
          "Content-Type: application/json",
          "Authorization: Bearer $accessToken",
        );
        
        $response = curlRequestPost($url,['data'=>[$param]],$headers,'PATCH');

        $sales_force_log = new \App\SalesforceLog;
        $sales_force_log->type = $log_type;
        $sales_force_log->refenrence_id = $pack->id;
        $sales_force_log->response = json_encode($response);
        $sales_force_log->sale_request = json_encode($param);
        $sales_force_log->user_id = $user->id;
        $sales_force_log->save();
        //dd($response);
        if (isset($response[0]) && isset($response[0]["id"]) && !empty($response[0]["id"])) {
            return $response[0]["id"];
        }else{
          \Log::info('Salesforce Error: '.json_encode($response));
        }

        return false;
     }
     
     function fitnessEmail($param){
       
        $access = $this->getAccessToken();
        
        if(empty($access) || !isset($access["access_token"])){
            return false;
        }
        $accessToken = $access["access_token"];
        $url = env('SALES_FORCE_VIP_BASE_URL').'StepChallenge';
        $headers = array(
          "Accept: application/json",
          "Content-Type: application/json",
          "Authorization: Bearer $accessToken",
        );
        
        $response = curlRequestPost($url,$param,$headers);
        return $response;
     }

}
