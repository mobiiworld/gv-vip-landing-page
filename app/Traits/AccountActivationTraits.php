<?php
namespace App\Traits;

trait AccountActivationTraits
{

   function activateAccount($userPack){
    /*
    fields in user table - account_id, account_activation_status(default 0)
    Steps - 
    1. Account Search , If email is same as in the result then save the account id in table, otherwise create new account and save the account id in table
    2. Assign Accountid & sale id - account activation status as 1
    3. Save code - - account activation status as 2
    4. Outbound - status 3
    */
    \Log::channel('syncCurl')->info('Sync Started - '.date('Y-m-d H:i:s'));
    $configurations = \App\ApiConfiguration::all()->keyBy('name');
    //dd($configurations);
    if($userPack->account_id != NULL){
        $accountId = $userPack->account_id ;
    }else{
        \Log::channel('syncCurl')->info('Account search');
        $accountId = $this->accountSearch($userPack,$configurations);
       
        if($accountId == NULL){
            \Log::channel('syncCurl')->info('Account create');
            $accountId = $this->createAccount($userPack,$configurations);
        }
        if($accountId != NULL){
            // $user = \App\User::find($userPack->user_id);
            // $user->account_id = $accountId;
            // $user->save();
            $userActivatedPack = \App\UserPack::find($userPack->user_pack_id);
            $userActivatedPack->account_id = $accountId;
            $userActivatedPack->save();
        }
    }
    
    // dd($accountId);
    if($accountId != NULL){
        \Log::channel('syncCurl')->info('Assign accountid & sale id');
        //if($userPack->account_activation_status > 0 ){
            if( $this->assignAccountSaleId($accountId,$userPack->sale_id,$configurations)){
                if($userPack->account_activation_status == NULL){
                    // $user = \App\User::find($userPack->user_id);
                    // $user->account_activation_status = 1;
                    // $user->save();
                    $userActivatedPack = \App\UserPack::find($userPack->user_pack_id);
                    $userActivatedPack->account_activation_status = 1;
                    $userActivatedPack->save();
                    $userPack->account_activation_status = 1;
                }
            }
            
            
            \Log::channel('syncCurl')->info('Save code');
            if($userPack->account_activation_status > 0 ){
                if($this->savecode($userPack,$configurations)){
                    if($userPack->account_activation_status == 1){
                        // $user = \App\User::find($userPack->user_id);
                        // $user->account_activation_status = 2;
                        // $user->save();
                        $userActivatedPack = \App\UserPack::find($userPack->user_pack_id);
                        $userActivatedPack->account_activation_status = 2;
                        $userActivatedPack->save();
                        $userPack->account_activation_status = 2;
                    }
                }
                
                \Log::channel('syncCurl')->info('Out bound');
                if($userPack->account_activation_status == 2 ){
                    if( $this->outboundApi($userPack->sale_id,$configurations)){
                        // $user = \App\User::find($userPack->user_id);
                        // $user->account_activation_status = 3;
                        // $user->save();
                        $userActivatedPack = \App\UserPack::find($userPack->user_pack_id);
                        $userActivatedPack->account_activation_status = 3;
                        $userActivatedPack->save();
                    }
                    
                }
            }
        //}
    }
    \Log::channel('syncCurl')->info('Sync finished - '.date('Y-m-d H:i:s'));

   }
   function salesSearch(){
        $url = config('accountConfig.pprUrl')."&cmd=SALESEARCH";
        $requestData = array(
            'Header' => array('WorkstationId' => config('accountConfig.workStationId')),
            'Request' => array(
                'PagePos' => 1,
                'RecordPerPage' => 50,
                'SaleCode' => '2DVEXJJB'
            )
        );
        $header = array('Content-Type: application/json');
        $response = $this->getData($url,$requestData,$header);
       
       // dd($response->Answer->SaleList[0]->SaleId);
        $saleId = (isset($response->Answer->SaleList[0]->SaleId)) ? $response->Answer->SaleList[0]->SaleId : NULL;
        dd( $saleId);
        
   }

   function accountSearch($user,$configurations){
        $url = config('accountConfig.pprUrl')."&cmd=ACCOUNT";
        $email = $user->email;
        $requestData = array(
            'Header' => array('WorkstationId' => $configurations['work_station_id']->value),
            'Request' => array(
                'Command' => 'Search',
                'Search' => array(
                    "FullText" => $email,
                    "AccountStatus" => 1
                )
            )
        );
        $header = array('Content-Type: application/json');
        $response = $this->getData($url,$requestData,$header);
        //dd($response);
        $accountId = NULL;
        if(isset($response->Answer->Search->AccountList)){
            $accounts = JSON_DECODE(JSON_ENCODE($response->Answer->Search->AccountList),TRUE) ;
            if(!empty($accounts)){
                $key = array_search($email, array_column($accounts, 'EmailAddress'));
                $accountId = isset($accounts[$key]['AccountId']) ? $accounts[$key]['AccountId'] : NULL;
            }
            
        }
        //0BBD4A48-3E0F-DC87-5FF1-017406C6DAA0
       // $accountId = (isset($response->Answer->Search->AccountList[0]->AccountId)) ? $response->Answer->Search->AccountList[0]->AccountId : NULL;
        return $accountId;

   }

   function createAccount($user,$configurations){
        $url = config('accountConfig.pprUrl')."&cmd=ACCOUNT";
        $mArray = array('meta_data_first_name','meta_data_last_name','meta_data_age_group','meta_data_gender','meta_data_nationality','meta_data_mobile','meta_data_email','meta_data_keep_updated','meta_data_terms');
        $metadataList = array();
        $i = 0;
        foreach ($configurations as $key => $val) {
            if(in_array($key,$mArray)){
                $metadataList[$i]['MetaFieldCode'] = $val->value;
                switch ($key) {
                    case 'meta_data_first_name':
                        $metadataList[$i]['Value'] = $user->first_name;
                        break;
                    case 'meta_data_last_name':
                        $metadataList[$i]['Value'] = $user->last_name;
                        break;
                    case 'meta_data_age_group':
                        $age = $this->calculateAge($user->dob);
                        $metadataList[$i]['Value'] = $this->getAgeGroup($age);
                        break;
                    case 'meta_data_gender':
                        $metadataList[$i]['Value'] = $user->gender;
                        break;
                    case 'meta_data_nationality':
                        $country = \App\Country::find($user->nationality_id);
                        $metadataList[$i]['Value'] = $country->name_en;
                        break;
                    case 'meta_data_mobile':
                        $metadataList[$i]['Value'] = $user->mobile_dial_code.$user->mobile;
                        break;
                    case 'meta_data_email':
                        $metadataList[$i]['Value'] = $user->email;
                        break;
                    case 'meta_data_keep_updated':
                        $metadataList[$i]['Value'] = $user->recieve_updates;
                        break;
                    case 'meta_data_terms':
                        $metadataList[$i]['Value'] = 1;
                        break;

                    default:
                        $metadataList[$i]['Value'] = 1;
                        break;
                }
                $i++;
            }
            
        }
        $requestData = array(
            'Header' => array('WorkstationId' => $configurations['work_station_id']->value),
            'Request' => array(
                'Command' => 'SaveAccount',
                'SaveAccount' => array(
                    "AccountStatus" => 1,
                    "EntityType" => $configurations['account_entity_type']->value,
                    "EmailAddress" => $user->email,
                    "MetaDataList" => $metadataList
                )
            )
        );
        $header = array('Content-Type: application/json');
        $response = $this->getData($url,$requestData,$header);
        //dd($requestData);
        $accountId = (isset($response->Answer->SaveAccount->AccountId)) ? $response->Answer->SaveAccount->AccountId : NULL;

        return $accountId;
        
   }

   function assignAccountSaleId($accountId,$saleId,$configurations){
        $url = config('accountConfig.pprUrl')."&cmd=SALE";
        $requestData = array(
            'Header' => array('WorkstationId' => $configurations['work_station_id']->value),
            'Request' => array(
                'Command' => "AddAccount",
                "AddAccount" => array(
                    "SaleId" => $saleId,
                    "AccountId" => $accountId,
                    "SaleAccountType" => $configurations['sale_account_type']->value
                   // "SaleDateTime" => date("Y-m-d H:i:s")
                )
            )
        );
        $header = array('Content-Type: application/json');
        $response = $this->getData($url,$requestData,$header);
        //dd($response->Header->StatusCode);
        if(isset($response->Header->StatusCode) && $response->Header->StatusCode == 200){
            return true;
        }
         return false;
   }

   function savecode($userPack,$configurations){
        $url = config('accountConfig.pprUrl')."&cmd=CODEALIAS";
        $requestData = array(
            'Header' => array('WorkstationId' => $configurations['work_station_id']->value),
            'Request' => array(
                'Command' => "SaveCodeAliases",
                "SaveCodeAliases" => array(
                    "EntityType" => $configurations['save_code_alias_entity_type']->value,
                    "EntityId" => $userPack->sale_id,
                    "CodeAliasList" => array(
                        array(
                            "CodeAlias" => $userPack->prefix.$userPack->pack_number,
                            "CodeAliasTypeCode" => $configurations['code_alias_type_code']->value,
                        )
                    )
                )
            )
        );
        //$userCars = \App\UserCar::where('user_id',$userPack->user_id)->get();
        $userCars = \App\UserCarPack::join("new_user_cars","new_user_cars.id","user_car_packs.user_car_id")->select('new_user_cars.*')->where("user_car_packs.pack_id",$userPack->id)->get();
        if(!empty($userCars)){
            $cnt = 1;
            foreach($userCars as $car){
                $country = \App\CarCountry::find($car->country_id);
                $cCode = (isset($country->code)) ? "C:".$country->code : '';
                $emirate = \App\Emirate::find($car->emirate_id);
                $emCode = (isset($emirate->code)) ? "-E:".$emirate->code : '';
                $codeAlias = $cCode.$emCode."-NC:".$car->plate_prefix."-N:".$car->plate_number;
                $codeAliasType = $configurations['car_code_alias_type_code']->value;
                if($cnt > 1){
                    $codeAliasType = $configurations['car_code_alias_type_code2']->value;
                }
                $codeArray = array('CodeAlias'=>$codeAlias, 'CodeAliasTypeCode'=> $codeAliasType);
                $requestData['Request']['SaveCodeAliases']['CodeAliasList'][] = $codeArray;
                $cnt++;

            }

        }
       // dd($requestData);
        $header = array('Content-Type: application/json');
        $response = $this->getData($url,$requestData,$header);
        //dd($response);
        if(isset($response->Header->StatusCode) && $response->Header->StatusCode == 200){
            return true;
        }
        return false;
   }

   function outboundApi($saleId,$configurations){
        $url = config('accountConfig.pprUrl')."&cmd=OUTBOUND";
        $requestData = array(
            'Header' => array('WorkstationId' => $configurations['work_station_id']->value),
            'Request' => array(
                'Command' => "TriggerEvent",
                "TriggerEvent" => array(
                    "TriggerList" => array(array(
                            "Sale" => array(
                                "SaleId" => $saleId
                            )
                        )
                    )
                )
            )
        );
        $header = array('Content-Type: application/json');
        $response = $this->getData($url,$requestData,$header);
        if(isset($response->Header->StatusCode) && $response->Header->StatusCode == 200){
            return true;
        }
        return false;
   }

   function calculateAge($dob){
        if(!empty($dob)){
            $birthdate = new \DateTime($dob);
            $today   = new \DateTime('today');
            $age = $birthdate->diff($today)->y;
            return $age;
        }else{
            return 0;
        }
   }

   function getAgeGroup($age){
        
        $ageGroup = config('accountConfig.ageGroup');
        if($age < 18){
            return $ageGroup[0];
        }elseif($age > 60){
            return $ageGroup[4];
        }elseif($age >= 18 && $age < 25){
          return $ageGroup[1];
        }elseif($age >= 25 && $age < 40){
          return $ageGroup[2];
        }elseif($age >= 40 && $age < 60){
          return $ageGroup[3];
        }else{
            return "18-24";
        }
   }

   function getData($url,$requestData,$header=array(),$xml=FALSE){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if($xml == FALSE){
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        }else{
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestData);
        }
        
        if(!empty($header)){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }else{
            curl_setopt($ch, CURLOPT_HTTPHEADER, false);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        $output = curl_exec($ch);
        \Log::channel('syncCurl')->info('Request');
        \Log::channel('syncCurl')->info($url);
        \Log::channel('syncCurl')->info($requestData);
        \Log::channel('syncCurl')->info('Response');
        \Log::channel('syncCurl')->info($output);
        if(curl_errno($ch)){
            \Log::channel('syncCurl')->info('Error');
            \Log::channel('syncCurl')->info(curl_errno($ch));
            \Log::channel('syncCurl')->info(curl_error($ch));
        }
        $info = curl_getinfo($ch);
        /*if($url == "http://ppr-gvsnapp.gvnet.local/service?format=json&cmd=SALE"){
            dd($requestData);
        }*/
        // dd($output);
        $configurations = \App\ApiConfiguration::where('name', 'vgs_log')->first();
        if (!empty($configurations) && $configurations->value == "enable") {
            $vgsLog = new \App\VgsLog;
            $vgsLog->type = 'vip-activation-journey';
            $vgsLog->url = $url;
            $vgsLog->request_param = json_encode($requestData);
            $vgsLog->response_param = $output;
            $vgsLog->save();
        }

        return json_decode($output);
   }

   function updateVGSAccount($user,$configurations){
        $url = config('accountConfig.pprUrl')."&cmd=ACCOUNT";
        $mArray = array('meta_data_first_name','meta_data_last_name','meta_data_age_group','meta_data_gender','meta_data_nationality','meta_data_mobile','meta_data_email','meta_data_keep_updated','meta_data_terms');
        $metadataList = array();
        $i = 0;
        foreach ($configurations as $key => $val) {
            if(in_array($key,$mArray)){
                $metadataList[$i]['MetaFieldCode'] = $val->value;
                switch ($key) {
                    case 'meta_data_first_name':
                        $metadataList[$i]['Value'] = $user->first_name;
                        break;
                    case 'meta_data_last_name':
                        $metadataList[$i]['Value'] = $user->last_name;
                        break;
                    case 'meta_data_age_group':
                        $age = $this->calculateAge($user->dob);
                        $metadataList[$i]['Value'] = $this->getAgeGroup($age);
                        break;
                    case 'meta_data_gender':
                        $metadataList[$i]['Value'] = $user->gender;
                        break;
                    case 'meta_data_nationality':
                        $country = \App\Country::find($user->nationality_id);
                        $metadataList[$i]['Value'] = $country->name_en;
                        break;
                    case 'meta_data_mobile':
                        $metadataList[$i]['Value'] = $user->mobile_dial_code.$user->mobile;
                        break;
                    case 'meta_data_email':
                        $metadataList[$i]['Value'] = $user->email;
                        break;
                    case 'meta_data_keep_updated':
                        $metadataList[$i]['Value'] = $user->recieve_updates;
                        break;
                    case 'meta_data_terms':
                        $metadataList[$i]['Value'] = 1;
                        break;

                    default:
                        $metadataList[$i]['Value'] = 1;
                        break;
                }
                $i++;
            }
            
        }
        $requestData = array(
            'Header' => array('WorkstationId' => $configurations['work_station_id']->value),
            'Request' => array(
                'Command' => 'SaveAccount',
                'SaveAccount' => array(
                    "AccountStatus" => 1,
                    "AccountId" => $user->account_id,
                    "EntityType" => $configurations['account_entity_type']->value,
                    "EmailAddress" => $user->email,
                    "MetaDataList" => $metadataList
                )
            )
        );
        $header = array('Content-Type: application/json');
        $response = $this->getData($url,$requestData,$header);
        //dd($requestData);
        $accountId = (isset($response->Answer->SaveAccount->AccountId)) ? $response->Answer->SaveAccount->AccountId : NULL;

        return $accountId;
        
   }
}
