<?php

namespace App\Traits;
use Illuminate\Http\Request;

trait VipTraits {

  function generateGuestOtp(Request $request){
    $season = \App\Season::where('status',1)->first();
    if(empty($season)){            
        return errorResponse( trans('web.pack.notfound'));
    }
    if(!$season->pack_activation_enabled){
        $message = trans('web.pack.notfound');
        if(!empty($season->pack_activation_disbaled_msg_en)){
            $message = (app()->getLocale() == 'ar' && !empty($season->pack_activation_disbaled_msg_ar)) ? $season->pack_activation_disbaled_msg_ar : $season->pack_activation_disbaled_msg_en;
        }
        
        return errorResponse( $message ,['show_alert'=>true]);
    }

    $seasonId =(!(empty($season))) ? $season->id : NULL;
    $pack = \App\Pack::where('pack_number', $request->vip_pack_number)
            ->where('activation_code', $request->activation_code)
            ->where('season_id',$seasonId)
            ->where('status', 1)
            ->first();
    if(empty($pack)){
        return errorResponse( trans('web.pack.notfound'));
    }
    if ($pack->used) {
        return errorResponse( trans('web.pack.used'));
    }
    \App\UserOtp::where('email',$request->email)->delete();
    try {
            $otp = generateNumericOTP(6);

            $mail_data['otp'] = $otp;
            $mail_data['subject'] = trans('passwords.otp.subject');
            \Mail::to($request->email)
                    ->send(new \App\Mail\UserOtpEmail($mail_data));
            
            $user_otp = new \App\UserOtp;
            $user_otp->email = $request->email;
            $user_otp->otp = $otp;
            $user_otp->save();
            return successResponse( trans('passwords.otp.sent_success'));
    } catch (\Exception $e) {
        //echo $e->getMessage();
        //dd($e);
    }
    return errorResponse( trans('passwords.otp.sent_failed'));
  }

  function validateGuestOtp(Request $request){
    $user_otp = \App\UserOtp::where('email',$request->email)->first();
    if(!empty($user_otp) && $user_otp->otp == $request->otp){
        //return successResponse( trans('passwords.otp.validate_success'),['otp_id' => $user_otp->id]);
        return ['statusCode' => 1000, 'otp_id' => $user_otp->id];
    }
    //return errorResponse( trans('passwords.otp.validate_failed'));
    return ['statusCode' => 2000];
  }

  function formatoffers( $pack){
        $offers = \App\PartnerOffer::select('id','parent_merchant_name_en','parent_merchant_name_ar','merchant_name_en','merchant_name_ar','offer_name_en','offer_name_ar','valid_from','valid_to','promo_code','code_type','t_and_c','consumed','link','open_status','closed_message_en','closed_message_ar','pack_id as pId')
            ->addselect(\DB::raw('LOWER(REPLACE(partner_offers.merchant_name_en, " ", "")) as offerName'))
            ->addselect(\DB::raw('LOWER(REPLACE(partner_offers.code_type, " ", "")) as offerCodeType'))

            ->where('pack_id',$pack->pId)
            ->where('active_status',1)
            //->where('open_status',1)
            //->orderByRaw("FIELD(merchant_name_en,'Expo 2020 Dubai', 'Roxy Cinemas','Laguna Waterpark','The Green Planet','DPR - All Parks','Bollywood Parks Dubai','Motiongate Dubai','LEGOLAND Dubai','LEGOLAND WaterPark')")
            ->orderby('ordering','asc')
            ->orderby('created_at','asc')
            ->get()
            ;
        $finalOffers = [];
        $off_names = ['ibaa' => 'insideburjalarab', 'roxy' => 'roxycinemas'];
        foreach($offers as $offr){
            $offr->offerName = (isset($off_names[$offr->offerName])) ? $off_names[$offr->offerName] : $offr->offerName;
             $offr->download_url = route('pack-offer-download',[app()->getLocale(),$offr->id]);
                $qrCodeText = NULL;
                $offr->link = NULL;

                $offers_tile = [];
                if($offr->offerName == 'seabreeze'){
                    if($pack->category == 'gold'){
                        $qrCodeText = trans('offers.qr_text.seabreeze.silver');
                        $offers_tile = array( trans('offers.seabreeze.gold.offer1'));
                    }elseif($pack->category == 'private'){
                        $qrCodeText = trans('offers.qr_text.seabreeze.private');
                        $offers_tile = array( trans('offers.seabreeze.private.offer1'));
                    }elseif($pack->category == 'diamond'){
                        $qrCodeText = trans('offers.qr_text.seabreeze.private');
                        $offers_tile = array( trans('offers.seabreeze.diamond.offer1'));
                    }elseif($pack->category == 'platinum'){
                        $qrCodeText = trans('offers.qr_text.seabreeze.silver');
                        $offers_tile = array( trans('offers.seabreeze.platinum.offer1'));
                    }elseif($pack->category == 'silver' || $pack->category == 'complimentary'){
                        $qrCodeText = trans('offers.qr_text.seabreeze.silver');
                        $offers_tile = array(trans('offers.seabreeze.silver.offer1'));
                    }
                }

                if($offr->offerName == 'insideburjalarab' || $offr->offerName == 'ibaa'){
                    $qrCodeText = trans('offers.qr_text.ibaa');
                    //$offers_tile = array(trans('offers.ibaa.offer1'));
                    if($pack->category == 'private' ){
                        $offers_tile = array( trans('offers.ibaa.private.offer1'));
                    }elseif($pack->category == 'platinum' || $pack->category == 'diamond' ){
                        $offers_tile = array( trans('offers.ibaa.platinum.offer1'));
                    }elseif( $pack->category == 'gold' || $pack->category == 'complimentary'){
                        $offers_tile = array( trans('offers.ibaa.gold.offer1'));
                    }
                }

                if($offr->offerName == 'roxycinemas' || $offr->offerName == 'roxy'){
                    $qrCodeText = trans('offers.qr_text.roxy');
                    if($pack->category == 'private'  ){
                        $offers_tile = array( trans('offers.roxy.private.offer1'));
                    }elseif($pack->category == 'platinum' || $pack->category == 'diamond'){
                        $offers_tile = array( trans('offers.roxy.platinum.offer1'));
                    }elseif( $pack->category == 'gold' || $pack->category == 'silver' || $pack->category == 'complimentary'){
                        $offers_tile = array( trans('offers.roxy.silver.offer1'));
                    }
                }

                if($offr->offerName == 'dubaiparksandresorts' || $offr->offerName == 'dpr'){
                    $qrCodeText = trans('offers.qr_text.dpr');
                    if($pack->category == 'private'){
                        $offers_tile = array( trans('offers.dpr.private.offer1'),
                         trans('offers.dpr.private.offer2'),
                        trans('offers.dpr.private.offer3'),
                         trans('offers.dpr.private.offer4'), trans('offers.dpr.private.offer5'));
                    }elseif($pack->category == 'platinum'){
                        $offers_tile = array( trans('offers.dpr.platinum.offer1'),
                         trans('offers.dpr.platinum.offer2'),
                         trans('offers.dpr.platinum.offer3'),
                         trans('offers.dpr.platinum.offer4'),
                         trans('offers.dpr.platinum.offer5'));
                    }elseif( $pack->category == 'diamond'){
                        $offers_tile = array( trans('offers.dpr.diamond.offer1'),
                            trans('offers.dpr.diamond.offer2'),
                            trans('offers.dpr.diamond.offer3'),
                            trans('offers.dpr.diamond.offer4'),
                            trans('offers.dpr.diamond.offer5'));                        
                    }elseif( $pack->category == 'gold'){
                        $offers_tile = array( trans('offers.dpr.gold.offer1'),
                            trans('offers.dpr.gold.offer2'),
                            trans('offers.dpr.gold.offer3'),
                            trans('offers.dpr.gold.offer4'),
                            trans('offers.dpr.gold.offer5'));                        
                    }elseif($pack->category == 'silver' || $pack->category == 'complimentary'){
                        $offers_tile = array( trans('offers.dpr.silver.offer1'),
                         trans('offers.dpr.silver.offer2'),
                         trans('offers.dpr.silver.offer3'),
                         trans('offers.dpr.silver.offer4'),
                         trans('offers.dpr.silver.offer5'));
                    }
                }

                $offr->qrCodeText = $qrCodeText;

                $merchant = new \StdClass;
                $merchant->parent_merchant_name_en = $offr->parent_merchant_name_en;
                $merchant->parent_merchant_name_ar = $offr->parent_merchant_name_ar;
                $merchant->merchant_name_en = $offr->merchant_name_en;
                $merchant->merchant_name_ar = $offr->merchant_name_ar;
                $merchant->open_status = (!empty($offr->open_status)) ? true : false;
                $merchant->offerName = $offr->offerName;
                $merchant->closed_message_en = $offr->closed_message_en;
                $merchant->closed_message_ar = $offr->closed_message_ar;
                if(!empty($offers_tile)){
                    $finalOffers[$offr->offerName]['merchant'] = $merchant;
                    $finalOffers[$offr->offerName]['offers_tile'] = $offers_tile;
                    $finalOffers[$offr->offerName]['offers_list'][] = $offr;
                }
                
        }
        $finalOffers = array_values($finalOffers);
        return $finalOffers;
    }

    function highestVIPpack($seasonId, $email){
        
        $data = NULL;
        $packs = \App\Pack::select('packs.id as pId','packs.prefix','packs.pack_number','packs.sale_id','users.id as user_id','users.first_name','users.last_name','users.email','users.mobile','users.mobile_dial_code','users.gender','users.dob','users.recieve_updates','users.account_id','packs.category','packs.car_activation_allowed','packs.park_entry_count','packs.wonderpass_count','packs.parking_voucher_count','packs.cabana_total_count','packs.cabana_remaining_count','packs.cabana_discount_percentage','user_packs.id as upid')
        ->where('packs.season_id',$seasonId)
        ->join('user_packs', 'user_packs.pack_id', 'packs.id')
        ->join('users', 'users.id', 'user_packs.user_id');
        $packs->where('users.email',$email)
        ->selectRaw('(CASE WHEN packs.category = "platinum plus" THEN "platinum" WHEN (packs.category = "mini" OR packs.category = "mini pack") THEN "silver" ELSE packs.category END) as packCategory');
        $packs = $packs->orderByRaw("FIELD(packCategory,'private','diamond','platinum', 'gold','silver','complimentary', 'mini')")
        ->first();
        if(!empty($packs)) {
            $colorcodes = config('globalconstants.pack_categories_colorcodes');
            $colorcodesBack = config('globalconstants.pack_categories_background_colorcodes');
            $catagories = config('globalconstants.pack_categories');
            $data['packs'] = (!empty($packs)) ? $packs : NULL;
            $data['top_category'] = (isset($packs->packCategory)) ? $packs->packCategory : NULL;
            $data['top_category_id'] = (isset($packs->packCategory) && isset($catagories[$packs->packCategory])) ? $catagories[$packs->packCategory] : NULL;
            $data['user_type'] = (isset($catagories[$packs->packCategory])) ? $catagories[$packs->packCategory] : NULL;
            $data['category_color'] = (isset($colorcodes[$packs->packCategory])) ? $colorcodes[$packs->packCategory] : NULL;
            $data['category_background_color'] = (isset($colorcodesBack[$packs->packCategory])) ? $colorcodesBack[$packs->packCategory] : NULL;
        }
        return $data;
    }

    function getVIPUserPacks($email,$include_offers=true,$order='highest'){
        $lan = app()->getLocale();
        
        $catagories = config('globalconstants.pack_categories');
        $colorcodes = config('globalconstants.pack_categories_colorcodes');
        $colorcodesBack = config('globalconstants.pack_categories_background_colorcodes');
        $season = \App\Season::where('status',1)->first();
        $seasonId = (!(empty($season))) ? $season->id : NULL;
        $packs = \App\Pack::select('packs.id as pId','packs.prefix','packs.pack_number','packs.sale_id','users.id as user_id','users.first_name','users.last_name','users.email','users.mobile','users.mobile_dial_code','users.gender','users.dob','users.recieve_updates','users.account_id','packs.category','users.blocked_status as is_vip_blocked','packs.cabana_total_count','packs.cabana_remaining_count','packs.cabana_discount_percentage','packs.inpark_taxi_voucher','packs.carwash_services_voucher','packs.porter_services_voucher','packs.stunt_show','user_packs.id as upid','packs.car_activation_allowed')
        ->selectRaw('(CASE WHEN packs.category = "platinum plus" THEN "platinum" WHEN (packs.category = "mini" OR packs.category = "mini pack") THEN "silver" ELSE packs.category END) as packCategory')
        ->where('packs.season_id',$seasonId)
        ->join('user_packs', 'user_packs.pack_id', 'packs.id')
        ->join('users', 'users.id', 'user_packs.user_id');
       $packs->where('users.email',$email);
        if($order == 'highest'){
            $packs->orderByRaw("FIELD(packCategory,'private','diamond','platinum', 'gold','silver','complimentary', 'mini')");
        }else{
            $packs->orderByRaw("FIELD(packCategory,'mini','complimentary','silver','gold','platinum','diamond','private')");
        }
        
        $packs = $packs->get()->each(function($item) use($lan,$catagories,$colorcodes,$colorcodesBack,$include_offers){
            $item->activated_url = route('my.activated.pack',[app()->getLocale(),$item->upid]).'?source=mobile';
            $item->top_category_id = (isset($packs->packCategory) && isset($catagories[$packs->packCategory])) ? $catagories[$packs->packCategory] : NULL;
            $wonderpasses = \App\Wonderpass::select('card_number','balance_points','carnaval_ride_points','ripleys_count')->where('pack_id',$item->pId)->get();
                $item->wonderpasses = !empty($wonderpasses) ? $wonderpasses : NULL ;
                $item->wonderpass_count = !empty($wonderpasses) ? count($wonderpasses) : 0 ;
            if($include_offers){
                $item->category_type = (isset($catagories[$item->category])) ? $catagories[$item->category] : 1;
                $item->category_color = (isset($colorcodes[$item->category])) ? $colorcodes[$item->category] : NULL;
                $item->category_background_color = (isset($colorcodesBack[$item->category])) ? $colorcodesBack[$item->category] : NULL;
                
                

                $item->offers = NULL ;
                $offers = $this->formatoffers($item);
                $item->partneroffers = !empty($offers) ? $offers : NULL ;

                $cars = [];
                $userCars = \App\UserCarPack::join("new_user_cars","new_user_cars.id","user_car_packs.user_car_id")->select('new_user_cars.*','user_car_packs.designa_card_uid','user_car_packs.designa_card_id','user_car_packs.designa_park_id','user_car_packs.designa_setv_status','user_car_packs.designa_acc_status','user_car_packs.id as ucp_id')->where("user_car_packs.pack_id",$item->pId)->get();
                //dd($userCars);
                if(!empty($userCars )){
                    foreach($userCars as $us){
                        $country = \App\CarCountry::find($us->country_id);
                        $emairate = null;
                        if(!empty($us->emirate_id)){
                            $emirate = \App\Emirate::find($us->emirate_id);
                        }                        
                        $cars[] = array(
                            'id' => $us->id,
                            'type' => $us->type,
                            'plate_prefix' => $us->plate_prefix,
                            'plate_number' => $us->plate_number,
                            'country' => isset($country->code) ? $country->code : NULL,
                            'emirate' => isset($emirate->code) ? $emirate->code : NULL ,
                        );
                    }
                }

                $item->cars = !empty($cars) ? $cars : NULL ;
            }
            
        });

        return $packs;
    }
  

}
