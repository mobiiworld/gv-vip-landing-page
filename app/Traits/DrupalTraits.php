<?php
namespace App\Traits;

trait DrupalTraits
{

  function createDrupalUser($packId){
        if(env('ENABLE_DRUPAL_SYNC') == FALSE){
            return false;
        }
        $url = env('ENABLE_DRUPAL_URL','https://gv-web.dev.mobiiworld.com/syncvipuser')  ;
        $pack = \App\Pack::select('packs.id as pId', 'packs.prefix', 'packs.pack_number', 'packs.sale_id', 'users.id as user_id', 'users.first_name', 'users.last_name', 'users.email', 'users.mobile', 'users.mobile_dial_code',  'users.account_id','packs.category','users.gender','users.dob','users.nationality_id','packs.drupal_user_id','users.residence_id','users.emirate_id','users.recieve_updates')
                ->join('user_packs', 'user_packs.pack_id', 'packs.id')
                ->join('users', 'users.id', 'user_packs.user_id')
                ->where('packs.id', $packId)
                ->first();
        $season = \App\Season::where('status',1)->first();
        $seasonNo = (!(empty($season))) ? $season->season_number : "25";
        if(!empty($pack)){
            //$catagories = array('silver'=>100,'gold'=>101,'platinum'=>102,'platinum plus'=>103,'mini'=>10,'mini pack'=>11);
            $catagories = config('globalconstants.pack_categories');
            $wonderpasses = \App\Wonderpass::select('card_number as mediaCode')->where('pack_id',$pack->pId)->get()->toArray();
            $country = \App\Country::find($pack->residence_id);
            $nationality = \App\Country::find($pack->nationality_id);
            $emirate = \App\Emirate::find($pack->emirate_id);
            $genderArray = array("male"=>1,'female'=>2,"Male"=>1,'Female'=>2);
            $name = (app()->getLocale() == 'ar') ? 'name_ar' : 'name_en';
            $requestData = array(
                'id' => $pack->drupal_user_id ,
                'promotion' => $pack->recieve_updates ,
                'email' => $pack->email ,
                'firstName' => $pack->first_name,
                'lastName' => $pack->last_name,
                'mobile' => $pack->mobile,
                'mobileCode' => $pack->mobile_dial_code,
                'accountId' => $pack->account_id,
                'packId' => $pack->pack_number,
                'dateofbirth' => date("d-m-Y",strtotime($pack->dob)), //
                'country' => (!empty($country)) ? $country->elq_code : '',
                'nationality' => (!empty($nationality)) ? $nationality->elq_code : '',
                'emirate' => (!empty($emirate)) ?  $emirate->$name : '',
                'genderid' => isset($genderArray[$pack->gender]) ? $genderArray[$pack->gender] : '',
                'type' => isset($catagories[$pack->category]) ? $catagories[$pack->category] : 1,
                'saleId' => $pack->email,
                'wonderpasses' => $wonderpasses,
                'season_number' => $seasonNo
            );
            $header = array('Content-Type: application/json','GV-Language: '.app()->getLocale(), 'GV-Server-token: '.env("GV_WEB_SERVER_TOKEN"));
            $response = $this->getDrupalData($url,$requestData,$header);

            if(isset($response->data->uid)){
                $userId = $pack->user_id;
                // $pack = \App\Pack::find($packId);
                // $pack->drupal_user_id = $response->data->uid;
                // $pack->save();

                $user = \App\User::find($userId);
                $user->drupal_user_id = $response->data->uid;
                $user->save();

                \App\NewUserCar::where('user_id', $userId)                            
                             ->update(['drupal_user_id' => $response->data->uid]);

                //Update user cars
                // \App\UserCar::where('user_id', $userId)
                //              ->update(['drupal_user_id' => $response->data->uid]);                


                return json_encode($response);
            }
        }

        return false;

  }

  function removeDrupalUser($email, $wonderpasses){
    $url = env('E_API_URL').'/removevip';

    $season = \App\Season::where('status',1)->first();
    $seasonId = (!empty($season)) ?  $season->id : NULL;
    //\DB::enableQueryLog();
    $packs = \App\Pack::select('packs.id as pId')
    ->selectRaw('(CASE WHEN packs.category = "platinum plus" THEN "platinum" WHEN (packs.category = "mini" OR packs.category = "mini pack") THEN "silver" ELSE packs.category END) as packCategory')
    ->join('user_packs', 'user_packs.pack_id', 'packs.id')
    ->join('users', 'users.id', 'user_packs.user_id')
    ->where('users.email',$email)
    ->where('packs.season_id',$seasonId)
    ->where('users.season_id',$seasonId)
    ->where('users.blocked_status','no')
    ->orderByRaw("FIELD(packCategory,'private','diamond','platinum', 'gold','silver','complimentary', 'mini')")
    ->first();
    $catagories = config('globalconstants.pack_categories');
    $type = (!empty($packs) && isset($catagories[$packs->packCategory])) ? $catagories[$packs->packCategory] : 0;
    $requestData = array(
        'email' => $email,
        'type' => (string) $type,
        'wonderpasses' => $wonderpasses
    );
    //dd($requestData);
    $header = array('Content-Type: application/json','GV-Language: '.app()->getLocale(), 'GV-Server-token: '.env("GV_WEB_SERVER_TOKEN"));
    $response = $this->getDrupalData($url,$requestData,$header);
    return true;
  }

  function getDrupalData($url,$requestData,$header=array(),$xml=FALSE){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if($xml == FALSE){
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        }else{
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestData);
        }

        if(!empty($header)){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }else{
            curl_setopt($ch, CURLOPT_HTTPHEADER, false);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        \Log::channel('syncCurl')->info('Request');
        \Log::channel('syncCurl')->info($url);
        \Log::channel('syncCurl')->info($header);
        \Log::channel('syncCurl')->info($requestData);
        \Log::channel('syncCurl')->info('Response');
        \Log::channel('syncCurl')->info($output);
        if(curl_errno($ch)){
            \Log::channel('syncCurl')->info('Error');
            \Log::channel('syncCurl')->info(curl_errno($ch));
            \Log::channel('syncCurl')->info(curl_error($ch));
        }
        $info = curl_getinfo($ch);
        /*if($url == "http://ppr-gvsnapp.gvnet.local/service?format=json&cmd=SALE"){
            dd($requestData);
        }*/
        // dd($output);
        return json_decode($output);
   }

   function getDrupalUser($email){
    $url = env('E_API_URL').'/get-user';
    $requestData = array(
        'email' => $email
    );
    /*
    $countryMapping = array("AF" => "AFG",
                            "AL" => "ALB",
                            "DZ" => "DZA",
                            "AS" => "ASM",
                            "AD" => "AND",
                            "AO" => "AGO",
                            "AI" => "AIA",
                            "AQ" => "ATA",
                            "AG" => "ATG",
                            "AR" => "ARG",
                            "AM" => "ARM",
                            "AW" => "ABW",
                            "AU" => "AUS",
                            "AT" => "AUT",
                            "AZ" => "AZE",
                            "BS" => "BHS",
                            "BH" => "BHR",
                            "BD" => "BGD",
                            "BB" => "BRB",
                            "BY" => "BLR",
                            "BE" => "BEL",
                            "BZ" => "BLZ",
                            "BJ" => "BEN",
                            "BM" => "BMU",
                            "BT" => "BTN",
                            "BO" => "BOL",
                            "BA" => "BIH",
                            "BW" => "BWA",
                            "BV" => "BVT",
                            "BR" => "BRA",
                            "IO" => "IOT",
                            "BN" => "BRN",
                            "BG" => "BGR",
                            "BF" => "BFA",
                            "BI" => "BDI",
                            "KH" => "KHM",
                            "CM" => "CMR",
                            "CA" => "CAN",
                            "CV" => "CPV",
                            "KY" => "CYM",
                            "CF" => "CAF",
                            "TD" => "TCD",
                            "CL" => "CHL",
                            "CN" => "CHN",
                            "CX" => "CXR",
                            "CC" => "CCK",
                            "CO" => "COL",
                            "KM" => "COM",
                            "CG" => "COG",
                            "CK" => "COK",
                            "CR" => "CRI",
                            "HR" => "HRV",
                            "CU" => "CUB",
                            "CY" => "CYP",
                            "CZ" => "CZE",
                            "CD" => "COD",
                            "DK" => "DNK",
                            "DJ" => "DJI",
                            "DM" => "DMA",
                            "DO" => "DOM",
                            "EC" => "ECU",
                            "EG" => "EGY",
                            "SV" => "SLV",
                            "GQ" => "GNQ",
                            "ER" => "ERI",
                            "EE" => "EST",
                            "ET" => "ETH",
                            "FK" => "FLK",
                            "FO" => "FRO",
                            "FJ" => "FJI",
                            "FR" => "FRA",
                            "GF" => "GUF",
                            "PF" => "PYF",
                            "TF" => "ATF",
                            "GA" => "GAB",
                            "GE" => "GEO",
                            "GS" => "SGS",
                            "DE" => "DEU",
                            "GH" => "GHA",
                            "GI" => "GIB",
                            "GR" => "GRC",
                            "GL" => "GRL",
                            "GD" => "GRD",
                            "GP" => "GLP",
                            "GU" => "GUM",
                            "GT" => "GTM",
                            "GN" => "GIN",
                            "GW" => "GNB",
                            "GY" => "GUY",
                            "HT" => "HTI",
                            "VA" => "VAT",
                            "HN" => "HND",
                            "HK" => "HKG",
                            "HU" => "HUN",
                            "IS" => "ISL",
                            "IN" => "IND",
                            "ID" => "IDN",
                            "IR" => "IRN",
                            "IQ" => "IRQ",
                            "IE" => "IRL",
                            "IL" => "ISR",
                            "IT" => "ITA",
                            "CI" => "CIV",
                            "JM" => "JAM",
                            "JP" => "JPN",
                            "JO" => "JOR",
                            "KZ" => "KAZ",
                            "KE" => "KEN",
                            "KI" => "KIR",
                            "KW" => "KWT",
                            "KG" => "KGZ",
                            "LA" => "LAO",
                            "LV" => "LVA",
                            "LB" => "LBN",
                            "LS" => "LSO",
                            "LR" => "LBR",
                            "LY" => "LBY",
                            "LI" => "LIE",
                            "LT" => "LTU",
                            "LU" => "LUX",
                            "MO" => "MAC",
                            "MK" => "MKD",
                            "MG" => "MDG",
                            "MW" => "MWI",
                            "MY" => "MYS",
                            "MV" => "MDV",
                            "ML" => "MLI",
                            "MT" => "MLT",
                            "MH" => "MHL",
                            "MQ" => "MTQ",
                            "MR" => "MRT",
                            "MU" => "MUS",
                            "YT" => "MYT",
                            "MX" => "MEX",
                            "FM" => "FSM",
                            "MC" => "MCO",
                            "MN" => "MNG",
                            "ME" => "MNE",
                            "MS" => "MSR",
                            "MA" => "MAR",
                            "MZ" => "MOZ",
                            "MM" => "MMR",
                            "NA" => "NAM",
                            "NR" => "NRU",
                            "NP" => "NPL",
                            "NL" => "NLD",
                            "NC" => "NCL",
                            "NZ" => "NZL",
                            "NI" => "NIC",
                            "NE" => "NER",
                            "NG" => "NGA",
                            "NU" => "NIU",
                            "NF" => "NFK",
                            "KP" => "PRK",
                            "MP" => "MNP",
                            "NO" => "NOR",
                            "OM" => "OMN",
                            "PK" => "PAK",
                            "PW" => "PLW",
                            "PS" => "PSE",
                            "PA" => "PAN",
                            "PG" => "PNG",
                            "PY" => "PRY",
                            "PE" => "PER",
                            "PH" => "PHL",
                            "PN" => "PCN",
                            "PL" => "POL",
                            "PT" => "PRT",
                            "PR" => "PRI",
                            "QA" => "QAT",
                            "RE" => "REU",
                            "RO" => "ROU",
                            "RU" => "RUS",
                            "RW" => "RWA",
                            "SH" => "SHN",
                            "KN" => "KNA",
                            "LC" => "LCA",
                            "PM" => "SPM",
                            "WS" => "WSM",
                            "SM" => "SMR",
                            "ST" => "STP",
                            "SA" => "SAU",
                            "SN" => "SEN",
                            "RS" => "SRB",
                            "SC" => "SYC",
                            "SL" => "SLE",
                            "SG" => "SGP",
                            "SK" => "SVK",
                            "SI" => "SVN",
                            "SB" => "SLB",
                            "SO" => "SOM",
                            "ZA" => "ZAF",
                            "KR" => "KOR",
                            "SS" => "SSD",
                            "ES" => "ESP",
                            "LK" => "LKA",
                            "VC" => "VCT",
                            "SD" => "SDN",
                            "SR" => "SUR",
                            "SJ" => "SJM",
                            "SZ" => "SWZ",
                            "SE" => "SWE",
                            "CH" => "CHE",
                            "SY" => "SYR",
                            "TW" => "TWN",
                            "TJ" => "TJK",
                            "TZ" => "TZA",
                            "TH" => "THA",
                            "TL" => "TLS",
                            "TG" => "TGO",
                            "TK" => "TKL",
                            "TO" => "TON",
                            "TT" => "TTO",
                            "TN" => "TUN",
                            "TR" => "TUR",
                            "TM" => "TKM",
                            "TC" => "TCA",
                            "TV" => "TUV",
                            "UM" => "UMI",
                            "UG" => "UGA",
                            "UA" => "UKR",
                            "AE" => "UAE",
                            "GB" => "GBR",
                            "US" => "USA",
                            "UY" => "URY",
                            "UZ" => "UZB",
                            "VU" => "VUT",
                            "VE" => "VEN",
                            "VN" => "VNM",
                            "VG" => "VGB",
                            "VI" => "VIR",
                            "WF" => "WLF",
                            "EH" => "ESH",
                            "YE" => "YEM",
                            "ZM" => "ZMB",
                            "ZW" => "ZWE",
                            );*/
    $countries = \App\Country::select('code','elq_code')->get();
    $countryMapping = [];
    foreach($countries as $coun){
        $countryMapping[$coun->elq_code] = $coun->code;
    }
    //dd($countryMapping);
    $uniqueKey = env('DRUPAL_UNIQUE_KEY','7f3^`H_Gx~7neyl73TZykP_sY`J)Jp');
    $header = array('Content-Type: application/json','GV-Language: '.app()->getLocale(),'GV-Unique-Key: '.$uniqueKey);
    $user = [];
    try{
        $response = $this->getDrupalData($url,$requestData,$header);
         //dd($response)   ;
        if(isset($response->data->firstname)){
            $userData = $response->data;
           // dd($userData);
           $userData->mobile = ($userData->mobile != NULL) ? str_replace(' ', '', $userData->mobile) : NULL;
            $user['email'] = $email ;
            $user['first_name'] = $userData->firstname;
            $user['last_name'] = $userData->lastname;
            $user['mobile_dial_code'] = $userData->mobile_country_code;
            $user['mobile'] =  ($userData->mobile != NULL  && $userData->mobile_country_code != NULL) ? (strpos($userData->mobile, $userData->mobile_country_code) != false ? substr($userData->mobile, strpos($userData->mobile, $userData->mobile_country_code) + strlen($userData->mobile_country_code)) : $userData->mobile) : $userData->mobile;
            
            $user['user_mobile'] =  ($userData->mobile_country_code != NULL) ? $userData->mobile_country_code.$userData->mobile : $userData->mobile;
            $user['gender'] = strtolower($userData->gender);
            $user['dob'] = ($userData->dob != NULL) ? date('d-m-Y',strtotime($userData->dob)) : NULL;
            $countryCode = (!empty($userData->nationality) && isset($countryMapping[$userData->nationality])) ? $countryMapping[$userData->nationality] : NULL;
            $country = \App\Country::where('code',$countryCode)->first();
            $user['nationality_id'] = (!empty($country)) ? $country->id : NULL;
            $user['nationality_code'] = (!empty($country)) ? $country->code : NULL;
            $residenceCode = (!empty($userData->country_of_residence) && isset($countryMapping[$userData->country_of_residence])) ? $countryMapping[$userData->country_of_residence] : NULL;
            $residence = \App\Country::where('code',$residenceCode)->first();
            $user['residence_id'] = (!empty($residence)) ? $residence->id : NULL;
            $user['residence_code'] = (!empty($residence)) ? $residence->code : NULL;
            $emirate = \App\Emirate::where('name_en',trim($userData->user_city))->orwhere('code',trim($userData->user_city))->orwhere('name_ar',trim($userData->user_city))->first();
            $user['emirate_id'] = (!empty($emirate)) ? $emirate->id : NULL;
            $user['emirate_code'] = (!empty($emirate)) ? $emirate->code : NULL;
            //$user['residence_id'] = NULL;
            $user['vip_user'] = 'no';
            $user['drupal_user_id'] = (isset($userData->user_id)) ? $userData->user_id : NULL;
            $user['promotion'] = (isset($userData->promotion) && $userData->promotion == 1) ? true : false;
            //dd($user);
        }
    }catch(\Exception $e){

    }
    
    return $user;
  }

  function blockDrupalUser($drupalUserId,$blockStatus,$unblockedCount,$packNumber, $userId){
        $url = env('E_API_URL').'/blockVipUser';
        $requestData = array(
            'id' => $drupalUserId ,
            'vipUserId' => $userId ,
            'block_status' => $blockStatus,
            'vip_blocked' => ($unblockedCount > 0) ? 'no' : 'yes',
            'packNumber' => $packNumber
        );
        $uniqueKey = env('DRUPAL_UNIQUE_KEY','7f3^`H_Gx~7neyl73TZykP_sY`J)Jp');
        $header = array('Content-Type: application/json','GV-Language: '.app()->getLocale(),'GV-Unique-Key: '.$uniqueKey, 'GV-Server-token: '.env("GV_WEB_SERVER_TOKEN"));
        $response = $this->getDrupalData($url,$requestData,$header);
       // dd($response);
    }



    function validateDrupalUser($drupalUserId, $userToekn){
        $url = env('E_API_URL').'/validate-user';
        
        $requestData = array(
            'drupal_user_id' => $drupalUserId,
            'user_token' => $userToekn

        );
        //dd($requestData);
        $uniqueKey = env('DRUPAL_UNIQUE_KEY','7f3^`H_Gx~7neyl73TZykP_sY`J)Jp');
        $header = array('Content-Type: application/json','GV-Language: '.app()->getLocale(), 'GV-Unique-Key: '.$uniqueKey);
        $response = $this->getDrupalData($url,$requestData,$header);
        
        return (isset($response->statusCode) && $response->statusCode == 1000) ? true : false;        
    }
    
    function getNotUsedCars($drupal_user_id,$seasonId, $email,$limit){
        /*$current_season_user_ids = \App\User::where('season_id',$seasonId)->where('email',$email)->whereNotNull('pack_id')->pluck('id');
        $usedCars = [];
        if(count($current_season_user_ids) > 0){
            $usedCars = \App\UserCar::where('drupal_user_id',$drupal_user_id)->whereIn('user_id',$current_season_user_ids)->orderby('created_at','desc')->distinct('plate_number','country_id','emirate_id','plate_prefix','type')->get();            
        }

        $carData = \App\UserCar::where('drupal_user_id',$drupal_user_id)->orderby('created_at','desc')->select('plate_number','country_id','emirate_id','plate_prefix','type');
        if(count($usedCars) > 0){
            foreach($usedCars as $used){
                $string = '( CASE WHEN  country_id = '.$used->country_id.' AND  plate_prefix = "'.$used->plate_prefix.'" AND plate_number = "'.$used->plate_number.'"';
                if(!empty($used->type)){
                    $string .= ' AND  type = '.$used->type;
                }
                if(!empty($used->emirate_id)){
                    $string .= ' AND  emirate_id = '.$used->emirate_id;
                }
                $string .= ' THEN FALSE ELSE TRUE END)';
                $carData->whereRaw($string);
            }
        }

        $carData = $carData->distinct()->limit($limit)->get();
        */
        $user = \App\User::where('season_id',$seasonId)->where('email',$email)->first();
        $usedCarIds = [];
        if(!empty($user)){
            $userPacks = \App\UserPack::where('user_id',$user->id)->pluck('pack_id')->toArray();
            if(!empty($userPacks)){
                $usedCarIds = \App\UserCarPack::whereIn('pack_id',$userPacks)->pluck('user_car_id')->toArray();
            }
        }
        $carData = \App\NewUserCar::select('id','plate_number','country_id','emirate_id','plate_prefix','type');
        $carData->where('drupal_user_id',$drupal_user_id);
        if(!empty($usedCarIds)){
            $carData->whereNotIn('id',$usedCarIds);
        }
        
        $carData = $carData->limit($limit)->get();
        
        return $carData;


    }

}