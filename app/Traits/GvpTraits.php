<?php

namespace App\Traits;

/**
 * @author Shainu
 */
trait GvpTraits {

    public function syncUsertoDesigna($id) {
        
        $errArray = [];
        $config = \App\ApiConfiguration::select('name', 'value')->get()->keyBy('name')->toArray();
        $userPack = \App\Pack::select('packs.id', 'packs.prefix', 'packs.pack_number', 'packs.sale_id', 'users.id as user_id', 'users.first_name', 'users.last_name', 'users.email', 'users.mobile', 'users.mobile_dial_code', 'users.gender', 'users.dob', 'users.residence_id', 'users.nationality_id', 'users.recieve_updates','user_packs.id as user_pack_id','user_packs.designa_customer_uid')
                ->join('user_packs', 'user_packs.pack_id', 'packs.id')
                ->join('users', 'users.id', 'user_packs.user_id')
                ->where('user_packs.id', $id)->first();
        $userPack->residence = (!empty($userPack->residence_id)) ? \App\Country::find($userPack->residence_id) : NULL;
        $cars = \App\UserCarPack::join("new_user_cars","new_user_cars.id","user_car_packs.user_car_id")->select('new_user_cars.*','user_car_packs.designa_card_uid','user_car_packs.designa_card_id','user_car_packs.designa_park_id','user_car_packs.designa_setv_status','user_car_packs.designa_acc_status','user_car_packs.id as ucp_id')->where("user_car_packs.pack_id",$userPack->id)->get()->each(function($car){
            $car->country = \App\CarCountry::find($car->country_id);
            $car->emirate = \App\Emirate::find($car->emirate_id);
        });
        $userActivatedPack = \App\UserPack::find($userPack->user_pack_id);
        if ($userPack) {
            try {
                if (empty($userActivatedPack->designa_customer_uid)) {
                    $details3 = '';
                    foreach ($cars as $car) {
                        $details3 .= ($car->country) ? $car->country->code : null;
                        if ($car->emirate) {
                            $details3 .= '$' . $car->emirate->code;
                        }
                        $pl = generatePlateCode($car->plate_prefix,$car->plate_number,'$');
                        $details3 .= '$' . $pl . '^';
                    }

                    $userRequest = [
                        'user' => $config['designa_user']['value'],
                        'pwd' => $config['designa_password']['value'],
                        'lastName' => $userPack->last_name,
                        'firstName' => $userPack->first_name,
                        'country' => (isset($userPack->residence) && !empty($userPack->residence)) ? $userPack->residence->code : '',
                        'phone' => $userPack->mobile_dial_code . $userPack->mobile,
                        'email' => $userPack->email,
                        'detail1' => $userPack->prefix . $userPack->pack_number,
                        'detail2' => $userPack->id,
                        'detail3' => trim($details3, '^'),
                        'erroLog' => true
                    ];
                    $xml = $this->createCustomer($userRequest);
                    if (!empty($xml)) {
                        $xml = simplexml_load_string($xml);
                        $xml->registerXPathNamespace('ns1', 'http://www.designa.de/');
                        $xml = $xml->xpath('//ns1:createCustomerFullResponse/ns1:createCustomerFullResult');
                        if (isset($xml[0])) {
                            $xml = $xml[0];
                            $json = json_encode($xml);
                            $response = json_decode($json, true);
                            if (isset($response['@attributes'])) {
                                $userActivatedPack->designa_customer_uid = $response['@attributes']['UId'];
                                $userActivatedPack->designa_customer_id = $response['@attributes']['ID'];
                                $userActivatedPack->save();
                            }
                        } else {
                            array_push($errArray, ['request' => $userRequest, 'response' => $xml]);
                        }
                    }
                }//if designa user uid null
                //Create Designa Card for each car
                if (!empty($userActivatedPack->designa_customer_id)) {
                    foreach ($cars as $car) {
                        $ucar = \App\UserCarPack::find($car->ucp_id);
                        if (empty($ucar->designa_card_uid)) {
                            $cardRequest = [
                                'user' => $config['designa_user']['value'],
                                'pwd' => $config['designa_password']['value'],
                                'personID' => $userActivatedPack->designa_customer_id,
                                'cardType' => $config['designa_card_type']['value'],
                                'carparkID' => $config['designa_carpark_id']['value'],
                                'groupID' => $config['designa_group_id']['value'],
                                'applicID' => $config['designa_application_id']['value'],
                                'expiryDate' => trim($config['designa_expiration_date']['value']),
                                'erroLog' => true
                            ];

                            $xml = $this->createCard($cardRequest);
                            if (!empty($xml)) {
                                $xml = simplexml_load_string($xml);
                                $xml->registerXPathNamespace('ns1', 'http://www.designa.de/');
                                $xml = $xml->xpath('//ns1:createCardResponse/ns1:createCardResult');
                                if (isset($xml[0])) {
                                    $xml = $xml[0];
                                    $json = json_encode($xml);
                                    $response = json_decode($json, true);
                                    if (isset($response['@attributes'])) {
                                        $ucar->designa_card_uid = $response['@attributes']['UId'];
                                        $ucar->designa_card_id = $response['@attributes']['ID'];
                                        $ucar->designa_park_id = isset($response['Carpark']['@attributes']) ? $response['Carpark']['@attributes']['UId'] : null;
                                    }
                                } else {
                                    array_push($errArray, ['request' => $cardRequest, 'response' => $xml]);
                                }
                            }
                        }//designa_card_uid empty

                        if ($ucar->designa_setv_status == 0) {
                            $ccd = (!empty($car->country) ? $car->country->code : null);
                            $ecd = (!empty($car->emirate) ? $car->emirate->code : null);
                            $vehicleRequest = [
                                'user' => $config['designa_user']['value'],
                                'pwd' => $config['designa_password']['value'],
                                'personGuid' => $userActivatedPack->designa_customer_uid,
                                'CountryCode' => $ccd . $ecd,
                                'LicensePlate' => generatePlateCode($car->plate_prefix, $car->plate_number),
                                'VIPCard' => $ucar->designa_card_uid,
                                'erroLog' => true
                            ];
                            $xml = $this->setVehicle($vehicleRequest);
                            if (!empty($xml)) {
                                $xml = simplexml_load_string($xml);
                                $xml->registerXPathNamespace('ns1', 'http://www.designa.de/');
                                $xml = $xml->xpath('//ns1:setVehicleResponse/ns1:setVehicleResult');
                                if (isset($xml[0])) {
                                    $xml = $xml[0];
                                    $json = json_encode($xml);
                                    $response = json_decode($json, true);
                                    if (isset($response[0])) {
                                        if ($response[0] == 0) {
                                            $ucar->designa_setv_status = 1;
                                        }
                                    }
                                } else {
                                    array_push($errArray, ['request' => $vehicleRequest, 'response' => $xml]);
                                }
                            }
                        }//if designa set vehicle not done

                        if ($ucar->designa_acc_status == 0) {
                            $ccd = (!empty($car->country) ? $car->country->code : null);
                            $ecd = (!empty($car->emirate) ? $car->emirate->code : null);
                            $pl = generatePlateCode( $car->plate_prefix,$car->plate_number,' ');
                            $assignRequest = [
                                'user' => $config['designa_user']['value'],
                                'pwd' => $config['designa_password']['value'],
                                'cardUId' => $ucar->designa_card_uid,
                                'cardCarrierKindId' => $config['designa_card_carrier_kind_id']['value'],
                                'carrierString' => $ccd . $ecd . '-' . $pl,
                                'erroLog' => true
                            ];
                            $xml = $this->assignCardCarrier($assignRequest);
                            if (!empty($xml)) {
                                $xml = simplexml_load_string($xml);
                                $xml->registerXPathNamespace('ns1', 'http://www.designa.de/');
                                $xml = $xml->xpath('//ns1:assignCardCarrierResponse/ns1:assignCardCarrierResult');
                                if (isset($xml[0])) {
                                    $xml = $xml[0];
                                    $json = json_encode($xml);
                                    $response = json_decode($json, true);
                                    if (isset($response[0])) {
                                        if ($response[0] == 0) {
                                            $ucar->designa_acc_status = 1;
                                        }
                                    }
                                } else {
                                    array_push($errArray, ['request' => $assignRequest, 'response' => $xml]);
                                }
                            }
                        }//if designa assign card career not done
                        $ucar->save();
                    }
                }//if designa_customer_id not null
                return response()->json(["status" => true, "message" => 'User cars synced.', 'responseLog' => $errArray]);
            } catch (\Exception $e) {
                return response()->json(["status" => false, "message" => $e->getMessage()]);
            }
        } else {
            return response()->json(["status" => false, "message" => 'User has no cars to sync.']);
        }
    }

    private function createCustomer(&$userRequest) {
        //$url = (env('APP_ENV') == 'production') ? 'http://192.168.2.39/webservice/ServiceOperation.asmx' : 'https://extapp.gvnet.local:8443/webservice/serviceOperation.asmx';
        $url = config('services.designa_base_url');
        $url = $url . '?op=createCustomerFull';

        $xml = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' .
                '<soap:Body>' .
                '<createCustomerFull xmlns="http://www.designa.de/">' .
                '<user>' . $userRequest['user'] . '</user>' .
                '<pwd>' . $userRequest['pwd'] . '</pwd>' .
                '<lastName>' . $userRequest['lastName'] . '</lastName>' .
                '<firstName>' . $userRequest['firstName'] . '</firstName>' .
                '<country>' . $userRequest['country'] . '</country>' .
                '<city></city>' .
                '<zipCode></zipCode>' .
                '<street></street>' .
                '<phone>' . $userRequest['phone'] . '</phone>' .
                '<email>' . $userRequest['email'] . '</email>' .
                '<detail1>' . $userRequest['detail1'] . '</detail1>' .
                '<detail2>' . $userRequest['detail2'] . '</detail2>' .
                '<detail3>' . $userRequest['detail3'] . '</detail3>' .
                '<detail4></detail4>' .
                '<detail5></detail5>' .
                '<detail6></detail6>' .
                '</createCustomerFull> </soap:Body>' .
                '</soap:Envelope>';

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Begin SSL
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        //End SSL
        $response = curl_exec($ch);
        \Log::channel('syncCurl')->info('Designa Create customer'.date('Y-m-d H:i:s'));
        \Log::channel('syncCurl')->info($xml);
        \Log::channel('syncCurl')->info('Response');
        \Log::channel('syncCurl')->info($response);
        if (curl_errno($ch)) {
            \Log::channel('syncCurl')->info('Error');
            \Log::channel('syncCurl')->info(curl_errno($ch));
            \Log::channel('syncCurl')->info(curl_error($ch));
            curl_close($ch);
            return null;
        }
        curl_close($ch);
        return $response;
    }

    private function createCard(&$cardRequest) {
        //$url = (env('APP_ENV') == 'production') ? 'http://192.168.2.39/webservice/ServiceOperation.asmx' : 'https://extapp.gvnet.local:8443/webservice/serviceOperation.asmx';
        $url = config('services.designa_base_url');
        $url = $url . '?op=createCard';

        $xml = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' .
                '<soap:Body>' .
                '<createCard xmlns="http://www.designa.de/">' .
                '<user>' . $cardRequest['user'] . '</user>' .
                '<pwd>' . $cardRequest['pwd'] . '</pwd>' .
                '<personID>' . $cardRequest['personID'] . '</personID>' .
                '<cardType>' . $cardRequest['cardType'] . '</cardType>' .
                '<carparkID>' . $cardRequest['carparkID'] . '</carparkID>' .
                '<groupID>' . $cardRequest['groupID'] . '</groupID>' .
                '<applicID>' . $cardRequest['applicID'] . '</applicID>' .
                '<creationDate>' . date('Y-m-d') . 'T00:00:00</creationDate>' .
                '<expiryDate>' . $cardRequest['expiryDate'] . 'T00:00:00</expiryDate>' .
                '</createCard> </soap:Body>' .
                '</soap:Envelope>';

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Begin SSL
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        //End SSL
        $response = curl_exec($ch);
        \Log::channel('syncCurl')->info('Designa Create Card'.date('Y-m-d H:i:s'));
        \Log::channel('syncCurl')->info($xml);
        \Log::channel('syncCurl')->info('Response');
        \Log::channel('syncCurl')->info($response);
        if (curl_errno($ch)) {
            \Log::channel('syncCurl')->info('Error');
            \Log::channel('syncCurl')->info(curl_errno($ch));
            \Log::channel('syncCurl')->info(curl_error($ch));
            curl_close($ch);
            return null;
        }
        curl_close($ch);
        return $response;
    }

    private function setVehicle(&$vehicleRequest) {
        //$url = (env('APP_ENV') == 'production') ? 'http://192.168.2.39/webservice/ServiceOperation.asmx' : 'https://extapp.gvnet.local:8443/webservice/serviceOperation.asmx';
        $url = config('services.designa_base_url');
        $url = $url . '?op=vehicle';

        $xml = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' .
                '<soap:Body>' .
                '<setVehicle xmlns="http://www.designa.de/">' .
                '<user>' . $vehicleRequest['user'] . '</user>' .
                '<pwd>' . $vehicleRequest['pwd'] . '</pwd>' .
                '<personGuid>' . $vehicleRequest['personGuid'] . '</personGuid>' .
                '<vehicleData>' .
                '<PersonUId>' . $vehicleRequest['personGuid'] . '</PersonUId>' .
                '<CountryCode>' . $vehicleRequest['CountryCode'] . '</CountryCode>' .
                '<LicensePlate>' . $vehicleRequest['LicensePlate'] . '</LicensePlate>' .
                '<Type></Type>' .
                '<Make></Make>' .
                '<Color></Color>' .
                '<Remark></Remark>' .
                '<Additional1></Additional1>' .
                '<Additional2></Additional2>' .
                '<VIPCard>' . $vehicleRequest['VIPCard'] . '</VIPCard>' .
                '<WhitelistCards>' .
                '<guid>' . $vehicleRequest['VIPCard'] . '</guid>' .
                '<guid>' . $vehicleRequest['VIPCard'] . '</guid>' .
                '</WhitelistCards></vehicleData></setVehicle></soap:Body>' .
                '</soap:Envelope>';

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Begin SSL
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        //End SSL
        $response = curl_exec($ch);
        \Log::channel('syncCurl')->info('Designa Set Vehicle'.date('Y-m-d H:i:s'));
        \Log::channel('syncCurl')->info($xml);
        \Log::channel('syncCurl')->info('Response');
        \Log::channel('syncCurl')->info($response);
        if (curl_errno($ch)) {
            \Log::channel('syncCurl')->info('Error');
            \Log::channel('syncCurl')->info(curl_errno($ch));
            \Log::channel('syncCurl')->info(curl_error($ch));
            curl_close($ch);
            return null;
        }
        curl_close($ch);
        return $response;
    }

    private function assignCardCarrier(&$assignRequest) {
        //$url = (env('APP_ENV') == 'production') ? 'http://192.168.2.39/webservice/ServiceOperation.asmx' : 'https://extapp.gvnet.local:8443/webservice/serviceOperation.asmx';
        $url = config('services.designa_base_url');
        $url = $url . '?op=assignCardCarrier';

        $xml = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' .
                '<soap:Body>' .
                '<assignCardCarrier xmlns="http://www.designa.de/">' .
                '<user>' . $assignRequest['user'] . '</user>' .
                '<pwd>' . $assignRequest['pwd'] . '</pwd>' .
                '<cardUId>' . $assignRequest['cardUId'] . '</cardUId>' .
                '<cardCarrierKindId>' . $assignRequest['cardCarrierKindId'] . '</cardCarrierKindId>' .
                '<carrierPrefix></carrierPrefix>' .
                '<carrierString>' . $assignRequest['carrierString'] . '</carrierString>' .
                '</assignCardCarrier></soap:Body>' .
                '</soap:Envelope>';

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Begin SSL
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        //End SSL
        $response = curl_exec($ch);
        \Log::channel('syncCurl')->info('Designa Set Vehicle'.date('Y-m-d H:i:s'));
        \Log::channel('syncCurl')->info($xml);
        \Log::channel('syncCurl')->info('Response');
        \Log::channel('syncCurl')->info($response);
        if (curl_errno($ch)) {
            \Log::channel('syncCurl')->info('Error');
            \Log::channel('syncCurl')->info(curl_errno($ch));
            \Log::channel('syncCurl')->info(curl_error($ch));
            curl_close($ch);
            return null;
        }
        curl_close($ch);
        return $response;
    }

}
