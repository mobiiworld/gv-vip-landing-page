<?php

namespace App\Traits;

trait VehicleCountTraits {

    function getVGSVehicleCount($vgsids) {
        $token = $this->getToken();
        if (!empty($token)) {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => config('accountConfig.parkingCount.url') . '/report/live-inside?source=locations&data[]=' . implode('&data[]=', $vgsids),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_HTTPHEADER => array('Authorization: Bearer ' . $token),
            ));
            $response = curl_exec($curl);
            if (curl_errno($curl)) {
                return null;
            }
            curl_close($curl);
            $response = json_decode($response);
            if (isset($response->data)) {
                return $response->data;
            }
        }
        return null;
    }

    private function getToken() {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => config('accountConfig.parkingCount.url') . '/auth/login',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{"api_key": "' . config('accountConfig.parkingCount.apiKey') . '"}',
            CURLOPT_HTTPHEADER => array(
                'Accept: application/json',
                'Content-Type: application/json'
            ),
        ));
        $response = curl_exec($curl);
        if (curl_errno($curl)) {
            return null;
        }
        curl_close($curl);
        $response = json_decode($response);
        if (isset($response->access_token)) {
            return $response->access_token;
        } else {
            return null;
        }
    }

    private function parkingDetails() {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://192.168.2.54:8033/api/ParkingDetails',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        if (curl_errno($curl)) {
            return null;
        }
        curl_close($curl);
        $response = json_decode($response);
        return $response;
    }

}
