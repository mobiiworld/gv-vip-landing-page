<?php
namespace App\Traits;

trait PackBlockTraits
{
 

   function getDataVgs($url,$requestData,$header=array(),$type=NULL){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        
        if(!empty($header)){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }else{
            curl_setopt($ch, CURLOPT_HTTPHEADER, false);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        $output = curl_exec($ch);
        \Log::channel('syncCurl')->info('Request');
        \Log::channel('syncCurl')->info($url);
        \Log::channel('syncCurl')->info($requestData);
        \Log::channel('syncCurl')->info('Response');
        \Log::channel('syncCurl')->info($output);
        if(curl_errno($ch)){
            \Log::channel('syncCurl')->info('Error');
            \Log::channel('syncCurl')->info(curl_errno($ch));
            \Log::channel('syncCurl')->info(curl_error($ch));
        }
        $info = curl_getinfo($ch);
        /*if($url == "http://ppr-gvsnapp.gvnet.local/service?format=json&cmd=SALE"){
            dd($requestData);
        }*/
        // dd($output);
       /* if($type != NULL){
            $vgsLog = new \App\VgsLog;
            $vgsLog->type = $type;
            $vgsLog->url = $url;
            $vgsLog->request_param = json_encode($requestData);
            $vgsLog->response_param = $output;
            $vgsLog->save();
        }*/
        
        return json_decode($output);
   }


    function blockPackVgs($pack,$configurations,$bStatus,$historyId){
        $saleId = $pack->sale_id;
        //$saleId = '2C78283D-EED6-AA19-04E8-017B4E77B0BC'; //test id -given in postman collection
        $saleSearch = $this->saleSearchForBlock($saleId,$configurations,'pack-block-sale-search');
        $mediaCodes = [];
        $mCodes =  [];
        $status = 'Sale search API Started';
        if(isset($saleSearch->Header->StatusCode) && $saleSearch->Header->StatusCode == 200){
            $status = 'Sale search API Success';
            $tickeList = isset($saleSearch->Answer->LoadEntSale->Sale->Sale->TicketList) ? $saleSearch->Answer->LoadEntSale->Sale->Sale->TicketList : array();
            /*
            Entry ticket and Parking Voucher needs to satisfy the below condition, then we can apply the block media otherwise you shouldn’t change the status. :
            Needs to be check the below marked fields (MainMediaStatus = 0, MediaCodeType = 0, FirstUsageDateTime = null and productId).

            Wonderpass needs to satisfy the below condition, then we can apply the block media otherwise you shouldn’t change the status.:
            Needs to be check the below marked fields (MainMediaStatus = 0, MediaCodeType = 0 and ProductId).

            */
            $checkStatus = ($bStatus == 'blocked') ? 0 : 1010;
            /*$entryParking = ['B313650D-FA13-92B9-4D7C-0179EF483BAA','E0B901B0-D10F-D6B2-4336-0179E597CE53','03C7CBAD-3C77-E0CC-496C-0179EF4675EF','0F393EDD-787B-AEE8-4F14-0179EF4899AD','57E82DB6-F496-515F-4A8A-0179EF470103','4CF1E88F-0558-BFFD-34DD-0179E584C68B','2A90ED2C-9F27-DD8C-4BE6-0179EF476D0F','410D06FA-E607-5FE3-74B4-0179EF9A607C','410D06FA-E607-5FE3-74B4-0179EF9A607C','3D845D05-FF0F-9917-4444-0179E59857CC','01B1FCE1-D849-3BE8-6FE8-0179EF982911','9985EF1D-102E-4E05-7614-0179EF9ADF6A','1C8E5248-AB5C-664D-7180-0179EF98ED1D','A72A4F43-633C-2E82-3A49-0179E58F58B0','9A97D026-6B06-516D-7318-0179EF99E8EC'];
            $wonderPass = ['12563EBD-BF3C-F2F2-594C-0179EF5160E5','CB8BAE5D-7499-04D0-44DD-0179E598B96A','11471506-F2FC-CC90-563A-0179EF503F5E','9820E673-175D-BE89-5B31-0179EF5200E6','CB7E354D-3E27-7170-52A9-0179EF4DF068','467ADF99-66A9-EDA5-36EC-0179E58BAF85','D80B9952-558B-D1F1-5767-0179EF50CF87'];*/
            $entryParking = \App\VgsProductConfig::whereIn('slug',['entry_ticket','parking_voucher'])->pluck('product_id')->toArray();
            $wonderPass = \App\VgsProductConfig::where('slug','vip_wonder_pass')->pluck('product_id')->toArray();
           
            if(!empty($tickeList)){
                foreach($tickeList as $tick){
                    if($tick->MainMediaStatus == $checkStatus &&  $tick->FirstUsageDateTime == NULL && in_array($tick->ProductId,$entryParking )){
                        if(isset($tick->MainMediaCodeList) && !empty($tick->MainMediaCodeList)){
                            foreach($tick->MainMediaCodeList as $med){
                                if($med->MediaCodeType == 0){
                                    $mediaCodes[] = ['MediaCode' => $med->MediaCode];
                                    $mCodes[] =  $med->MediaCode;
                                }
                            }
                        }
                    }
                    if($tick->MainMediaStatus == $checkStatus &&  in_array($tick->ProductId,$wonderPass )){
                        if(isset($tick->MainMediaCodeList) && !empty($tick->MainMediaCodeList)){
                            foreach($tick->MainMediaCodeList as $med){
                                if($med->MediaCodeType == 0){
                                    $mediaCodes[] = ['MediaCode' => $med->MediaCode];
                                    $mCodes[] =  $med->MediaCode;
                                }
                            }
                        }
                    }
                }
            }  

            if(!empty($mediaCodes)){
                $mediaStatus = ($bStatus == 'blocked') ? 1010 : 0;
                $mResponse = $this->mediaBlock($mediaCodes,$configurations,$mediaStatus);
                if(isset($mResponse->Header->StatusCode) && $mResponse->Header->StatusCode == 200){
                    $status = 'Media API Success';
                }else{
                    $status = 'Media API Failed';
                }
               // dd($mResponse );
            }else{
                $status = 'Sale search API No Media Codes Found';
            }          
        }else{
            $status = 'Sale search API No Data';
        }
        
        $blockHistory = \App\PackBlockHistory::find($historyId);
        $blockHistory->vgs_status = $status;
        $blockHistory->media_codes = (!empty($mCodes)) ? json_encode($mCodes) : NULL;
        $blockHistory->save();    
   }

   function saleSearchForBlock($saleId,$configurations,$type=NULL){
        $url = config('accountConfig.pprUrl')."&cmd=SALE";
        $requestData = array(
            'Header' => array('WorkstationId' => $configurations['work_station_id']->value),
            'Request' => array(
                'Command' => "LoadEntSale",
                "LoadEntSale" => array(
                    "SaleId" => $saleId
                )
            )
        );
        $header = array('Content-Type: application/json');
        $response = $this->getDataVgs($url,$requestData,$header,$type);
       // dd($response);
        if(isset($response->Header->StatusCode) && $response->Header->StatusCode == 200){
            return $response;
        }
         return false;
   }

   function mediaBlock($mediaCodes,$configurations,$mediaStatus=1010){
        $url = config('accountConfig.pprUrl')."&cmd=Media";
        $requestData = array(
            'Header' => array('WorkstationId' => $configurations['work_station_id']->value),
            'Request' => array(
                'Command' => "ChangeMediaStatus",
                "ChangeMediaStatus" => array(
                    "MediaList" => $mediaCodes,
                    "MediaStatus" => $mediaStatus
                )
            )
        );
        $header = array('Content-Type: application/json');
        $response = $this->getDataVgs($url,$requestData,$header,'pack-block-media-block');
       // dd($response);
        if(isset($response->Header->StatusCode) && $response->Header->StatusCode == 200){
            return $response;
        }
         return false;
   }

   function getUsageDetails($pack,$configurations){
        if(!env("ENABLE_EXTERNAL_API",true)){
            return ['entryUnused'=>1525,'entryUsed'=>0, 'parkingUnused'=>3, 'parkingUsed' =>0 ,'usedEntryPercent' =>0, 'usedParkingPercent' =>0,'totalEntry' => 1525, 'totalParking' => 3,'unUsedEntryPercent'=>100,'unUsedParkingPercent' => 100];
        }
        $saleId = $pack->sale_id;
        $entryUsed = $entryUnused = $parkingUsed = $parkingUnused = [];
        //$saleId = '014CFD75-65C4-3FC6-1CC1-01742510BE09'; //test id -given by ananth 


        try {
            $saleSearch = $this->saleSearchForBlock($saleId,$configurations,NULL);
            //dd($saleSearch);

            

            if(isset($saleSearch->Header->StatusCode) && $saleSearch->Header->StatusCode == 200){
                $status = 'Sale search API Success';
                $tickeList = isset($saleSearch->Answer->LoadEntSale->Sale->Sale->TicketList) ? $saleSearch->Answer->LoadEntSale->Sale->Sale->TicketList : array();
                $mediaList = isset($saleSearch->Answer->LoadEntSale->Sale->Sale->MediaList) ? $saleSearch->Answer->LoadEntSale->Sale->Sale->MediaList : array();

               
               /**
               changed  as discussed with Ananth - checking in MediaList & added PortfolioUsageCount - Amrutha 21 oct 2022
                * Used ticket needs to check MediaCodeType = 0 , PortfolioUsageCount = 1 and ProductId.
                * Unused ticket needs to check MainMediaStatus = 0, MediaCodeType = 0 , PortfolioUsageCount = 0 and ProductId.
               **/
               
                $entry = \App\VgsProductConfig::whereIn('slug',['entry_ticket'])->pluck('product_id')->toArray();
                $parking = \App\VgsProductConfig::whereIn('slug',['parking_voucher'])->pluck('product_id')->toArray();
                
               
               $i = 0;
                if(!empty($mediaList)){
                    foreach($mediaList as $tkey=>$tick){                   
                        
                        if( in_array($tick->MainProductId,$entry )){
                            if(isset($tick->MediaCodeList) && !empty($tick->MediaCodeList)){
                                foreach($tick->MediaCodeList as $med){
                                    if($med->MediaCodeType == 0 && isset($tick->PortfolioUsageCount)){
                                        if($tick->MediaStatus == 0 &&  $tick->PortfolioUsageCount == 0){ //Unused
                                            $entryUnused[$i] = $tick->MainProductId;
                                        }
                                        if($mediaList[$tkey]->PortfolioUsageCount != 0){ //Used
                                            $entryUsed[$i] = $tick->MainProductId;
                                        }
                                    }
                                }

                            }                       
                        }

                        if( in_array($tick->MainProductId,$parking )){
                            if(isset($tick->MediaCodeList) && !empty($tick->MediaCodeList)){
                                foreach($tick->MediaCodeList as $med){
                                    if($med->MediaCodeType == 0 && isset($tick->PortfolioUsageCount)){
                                        if($tick->MediaStatus == 0 &&  $tick->PortfolioUsageCount == 0){ //Unused
                                            $parkingUnused[$i] = $tick->MainProductId;
                                        }
                                        if($mediaList[$tkey]->PortfolioUsageCount != 0){ //Used
                                            $parkingUsed[$i] = $tick->MainProductId;
                                        }
                                    }
                                }

                            }                       
                        }
                        $i++;
                    }
                }    
            }
        }
        catch(\Exception $e) {
          
        }
        
       
        //$pack->park_entry_count, $pack->parking_voucher_count insted this, used count total from vgs
        $entryUsedCount = count($entryUsed);
        $entryUnusedCount = count($entryUnused);
        $parkingUsedCount = count($parkingUsed);
        $parkingUnusedCount = count($parkingUnused);
        $totalEntry = $entryUsedCount + $entryUnusedCount;
        $totalParking = $parkingUsedCount + $parkingUnusedCount;
        $usedEntryPercent = ($totalEntry > 0) ? $entryUsedCount/ $totalEntry : 0;
        $usedParkingPercent  = ( $totalParking > 0) ? $parkingUsedCount/ $totalParking : 0;
        $unUsedEntryPercent = ($totalEntry > 0) ? $entryUnusedCount/ $totalEntry : 0;
        $unUsedParkingPercent  = ( $totalParking > 0) ? $parkingUnusedCount/ $totalParking : 0;
        //dd($totalEntry,$totalParking,$entryUsed,$parkingUsed);
        //$usedParkingPercent = 0.5;
        return ['entryUnused'=>$entryUnusedCount,'entryUsed'=>$entryUsedCount, 'parkingUnused'=>$parkingUnusedCount, 'parkingUsed' =>$parkingUsedCount ,'usedEntryPercent' =>$usedEntryPercent, 'usedParkingPercent' =>$usedParkingPercent,'totalEntry' => $totalEntry, 'totalParking' => $totalParking,'unUsedEntryPercent'=>$unUsedEntryPercent,'unUsedParkingPercent' => $unUsedParkingPercent];
   }

   public function getMediaUseCount($configurations,$mediaCode){
        if(!env("ENABLE_EXTERNAL_API",true)){
            return ['walletBanalce' => 1525, 'ripleyBalance' => 0,'popoverText' =>0, 'carnavalBal' => 0];
        }

        $returnBalance = $carnavalBal = 0;
        $popoverText = '';
        $ripleyBalance = 0;
            try {
                    $url = config('accountConfig.pprUrl')."&cmd=PORTFOLIO";
                    $requestData = array(
                        'Header' => array('WorkstationId' => $configurations['work_station_id']->value),
                        'Request' => array(
                            'Command' => "GetPortfolioBalance",
                            "GetPortfolioBalance" => array(
                                "MediaCode" => $mediaCode
                            )
                        )
                    );
                    $header = array('Content-Type: application/json');
                    $response = $this->getDataVgs($url,$requestData,$header,NULL);
                   // dd($response);
                    
                    if(isset($response->Answer->GetPortfolioBalance->SlotList) && !empty($response->Answer->GetPortfolioBalance->SlotList)) {
                      foreach($response->Answer->GetPortfolioBalance->SlotList as $balance) {
                       
                        if ($balance->MembershipPointCode == "#WALLET") {
                          $returnBalance = $balance->SlotBalance;
                        }
                        $walletBalance = (isset($response->Answer->GetPortfolioBalance->WalletBalance)) ? $response->Answer->GetPortfolioBalance->WalletBalance : 'undefined';
                        if ($balance->MembershipPointCode == "play_bonus" && $walletBalance !== 'undefined') {
                          $carnavalBal = $balance->SlotBalance + $walletBalance;
                          
                        }
                        
                      }
                    }

                    //Ripleys count 
                    
                    $url = config('accountConfig.ripleyCountUrl')."?CardNumber=".$mediaCode;
                    $header = array('Content-Type: application/json');
                    $ripleyResponse = $this->getDataVgsGetMethod($url,$header,NULL);
                    //dd($ripleyResponse );
                    
                    if (!empty($ripleyResponse)) {
                        //$balance = $ripleyResponse[0];
                        $comma = '';
                        foreach($ripleyResponse as $balance){
                            if(!empty($balance->GamePlayBalance) && $balance->GamePlayBalance !== NULL) {
                              $ripleyBalance = $ripleyBalance + $balance->GamePlayBalance;
                              $popoverText .= $comma.$balance->GameName.' - '.$balance->GamePlayBalance;
                            }
                            $comma = ', ';
                        }
                    }
            }
            catch(\Exception $e) {
              
            }
        
        return ['walletBanalce' => $returnBalance, 'ripleyBalance' => $ripleyBalance,'popoverText' =>$popoverText, 'carnavalBal' => $carnavalBal];
   }


   function getDataVgsGetMethod($url,$header=array()){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if(!empty($header)){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }else{
            curl_setopt($ch, CURLOPT_HTTPHEADER, false);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);       
        $output = curl_exec($ch);
        \Log::channel('syncCurl')->info('Request');
        \Log::channel('syncCurl')->info($url);
        
        \Log::channel('syncCurl')->info('Response');
        \Log::channel('syncCurl')->info($output);
        if(curl_errno($ch)){
            \Log::channel('syncCurl')->info('Error');
            \Log::channel('syncCurl')->info(curl_errno($ch));
            \Log::channel('syncCurl')->info(curl_error($ch));
           // dd(curl_error($ch));
        }
        $info = curl_getinfo($ch);
       
        
        return json_decode($output);
   }

}
