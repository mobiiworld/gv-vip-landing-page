<?php

namespace App\Traits;
use Illuminate\Http\Request;

trait CarTraits {

    function getDesignaData($url,$xml){

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Begin SSL
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        //End SSL
        $response = curl_exec($ch);
        \Log::channel('syncCurl')->info(date('Y-m-d H:i:s'));
        \Log::channel('syncCurl')->info($url);
        \Log::channel('syncCurl')->info($xml);
        \Log::channel('syncCurl')->info('Response');
        \Log::channel('syncCurl')->info($response);

        $configurations = \App\ApiConfiguration::where('name', 'vgs_log')->first();
        if (!empty($configurations) && $configurations->value == "enable") {
            $vgsLog = new \App\VgsLog;
            $vgsLog->type = 'designa-apis';
            $vgsLog->url = $url;
            $vgsLog->request_param = "$xml";
            $vgsLog->response_param = $response;
            $vgsLog->save();
        }

        if (curl_errno($ch)) {
            \Log::channel('syncCurl')->info('Error');
            \Log::channel('syncCurl')->info(curl_errno($ch));
            \Log::channel('syncCurl')->info(curl_error($ch));
            curl_close($ch);
            return null;
        }
        curl_close($ch);
        return $response;
   }

   function syncCar($carId, Request $request,$userCarPack){
        try {
            \Log::channel('syncCurl')->info('Card by carrier');
           if($this->getCardByCarrier($carId)){
            \Log::channel('syncCurl')->info('set vehicle');
                if($this->setDesignaVehicle($carId,$request,$userCarPack)){
                    \Log::channel('syncCurl')->info('update card carrier');
                    return $this->updatecardcarrier($carId,$request,$userCarPack);
                }
           }
            return false;
        }catch (\Exception $e) {
            return false;
        }
   }

   function getCardByCarrier($carId){
        //$url = (env('APP_ENV') == 'production') ? 'http://192.168.2.39/webservice/ServiceOperation.asmx' : 'https://extapp.gvnet.local:8443/webservice/serviceOperation.asmx';
        $url = config('services.designa_base_url');
        $url = $url . '?op=getcardbycarrier';
         try {
            $configurations = \App\ApiConfiguration::whereIn('name',['designa_user','designa_password'])->get()->keyBy('name');
            $car = \App\NewUserCar::find($carId);
            $ccd = (!empty($car->country) ? $car->country->code : null);
            $ecd = (!empty($car->emirate) ? $car->emirate->code : null);
            $pl = generatePlateCode($car->plate_prefix,$car->plate_number);
            $carrier = $ccd . $ecd . '-' . $pl;
            $xml = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema
                        instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                         <soap:Body>
                         <getCardByCarrier xmlns="http://www.designa.de/">
                         <user>'.$configurations['designa_user']->value.'</user>
                         <pwd>'.$configurations['designa_password']->value.'</pwd>
                         <cardCarrierNr>'.$carrier.'</cardCarrierNr>
                         </getCardByCarrier>
                         </soap:Body>
                    </soap:Envelope> ';
            $response = $this->getDesignaData($url,$xml);
            if (!empty($response)) {
               // \Log::channel('syncCurl')->info($response);
                $response = simplexml_load_string($response);
                $response->registerXPathNamespace('ns1', 'http://www.designa.de/');
                $response = $response->xpath('//ns1:getCardByCarrierResponse/ns1:getCardByCarrierResult');
                if (isset($response[0])) {
                    return true;
                }
            }
            return false;
         }catch (\Exception $e) {
            return false;
        }
   }

   function setDesignaVehicle($carId,Request $request,$userCarPack){
        ///$url = (env('APP_ENV') == 'production') ? 'http://192.168.2.39/webservice/ServiceOperation.asmx' : 'https://extapp.gvnet.local:8443/webservice/serviceOperation.asmx';
        $url = config('services.designa_base_url');
        $url = $url . '?op=setvehicle';
        try {
            $configurations = \App\ApiConfiguration::whereIn('name',['designa_user','designa_password'])->get()->keyBy('name');
            $car = \App\NewUserCar::find($carId);
            $user = \App\User::find($car->user_id);
            if(empty($user) && !empty($car->drupal_user_id)){
                $user = \App\User::where('drupal_user_id',$car->drupal_user_id)->first();
            }
            $userPack = \App\UserPack::where('user_id',$user->id)->where('pack_id',$userCarPack->pack_id)->first();
            //$ccd = (!empty($car->country) ? $car->country->code : null);
            //$ecd = (!empty($car->emirate) ? $car->emirate->code : null);
            $country = \App\CarCountry::where('code',$request->countryCode)->first();
            $emirate = \App\Emirate::where('code',$request->cityCode)->first();
            $ccd = (!empty($country) ? $country->code : null);
            $ecd = (!empty($emirate) ? $emirate->code : null);
            $pl = generatePlateCode($request->plate_prefix,$request->plate_number);
            $xml = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema
                        instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                         <soap:Body>
                         <setVehicle xmlns="http://www.designa.de/">
                         <user>'.$configurations['designa_user']->value.'</user>
                         <pwd>'.$configurations['designa_password']->value.'</pwd>
                         <personGuid>'.$userPack->designa_customer_uid.'</personGuid>  <vehicleData>
                         <PersonUId>'.$userPack->designa_customer_uid.'</PersonUId>  <CountryCode>'.$ccd . $ecd.'</CountryCode>
                         <LicensePlate>'.$pl.'</LicensePlate>
                         <Type></Type>
                         <Make></Make>
                         <Color></Color>
                         <Remark></Remark>
                         <Additional1></Additional1>
                         <Additional2></Additional2>
                         <VIPCard>'.$userCarPack->designa_card_uid.'</VIPCard>
                         <WhitelistCards>
                         <guid>'.$userCarPack->designa_card_uid.'</guid>
                         <guid>'.$userCarPack->designa_card_uid.'</guid>
                         </WhitelistCards>
                         </vehicleData>
                         </setVehicle>
                         </soap:Body>
                        </soap:Envelope>';

                        //echo $xml; exit;
            $response = $this->getDesignaData($url,$xml);
            if (!empty($response)) {
                $response = simplexml_load_string($response);
                $response->registerXPathNamespace('ns1', 'http://www.designa.de/');
                $response = $response->xpath('//ns1:setVehicleResponse/ns1:setVehicleResult');
                if (isset($response[0])) {
                    return true;
                }
            }
            return false;
        }catch (\Exception $e) {
            return false;
        }
   }

   function updatecardcarrier($carId,Request $request,$userCarPack){
         //$url = (env('APP_ENV') == 'production') ? 'http://192.168.2.39/webservice/ServiceOperation.asmx' : 'https://extapp.gvnet.local:8443/webservice/serviceOperation.asmx';
        $url = config('services.designa_base_url');
        $url = $url . '?op=updatecardcarrier';
        try {
            $configurations = \App\ApiConfiguration::whereIn('name',['designa_user','designa_password','designa_card_carrier_kind_id'])->get()->keyBy('name');
            $car = \App\NewUserCar::find($carId);
            $user = \App\User::find($car->user_id);
            $ccd = (!empty($car->country) ? $car->country->code : null);
            $ecd = (!empty($car->emirate) ? $car->emirate->code : null);
            $pl = generatePlateCode($car->plate_prefix,$car->plate_number);
            $oldCarrier = $ccd . $ecd . '-' . $pl;

            $country = \App\CarCountry::where('code',$request->countryCode)->first();
            $emirate = \App\Emirate::where('code',$request->cityCode)->first();
            $newcountry = (!empty($country) ? $country->code : null);
            $newEmirate = (!empty($emirate) ? $emirate->code : null);
            $pl = generatePlateCode($request->plate_prefix,$request->plate_number);
            $newCarrier = $newcountry . $newEmirate . '-' . $pl;
            $xml = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema
                        instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                         <soap:Body>
                         <updateCardCarrier xmlns="http://www.designa.de/">
                         <user>'.$configurations['designa_user']->value.'</user>
                         <pwd>'.$configurations['designa_password']->value.'</pwd>
                         <cardUId>'.$userCarPack->designa_card_uid.'</cardUId>
                         <cardCarrierKindIdOld>'.$configurations['designa_card_carrier_kind_id']->value.'</cardCarrierKindIdOld>
                         <carrierStringOld>'.$oldCarrier.'</carrierStringOld>
                         <cardCarrierKindIdNew>'.$configurations['designa_card_carrier_kind_id']->value.'</cardCarrierKindIdNew>
                         <carrierStringNew>'.$newCarrier.'</carrierStringNew>
                         </updateCardCarrier>
                         </soap:Body>
                        </soap:Envelope>';
            $response = $this->getDesignaData($url,$xml);
            if (!empty($response)) {
                $response = simplexml_load_string($response);
                $response->registerXPathNamespace('ns1', 'http://www.designa.de/');
                $response = $response->xpath('//ns1:updateCardCarrierResponse/ns1:updateCardCarrierResult');
                if (isset($response[0]) && $response[0] == 0) {
                    return true;
                }
            }
            return false;
        }catch (\Exception $e) {
            return false;
        }
   }

}
