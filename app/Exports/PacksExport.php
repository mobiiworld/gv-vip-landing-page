<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PacksExport implements FromCollection,WithHeadings
{

	protected $input;

    public function __construct(array $input)
    {
        $this->input = $input;
    }

    public function headings(): array
    {
        return [
            'Pack Prefix',
            'Pack Number',
            'Sale Id',
            'First Name',
            'Last Name',
            'Email',
            'Mobile Code',
            'Mobile',
            'Gender',
            'DOB',
            'Nationality',
            'Country of Residence',
            'Receive Updates',
            'Account Id',
            'Designa Customer UID',
            'Designa Customer Id',
            
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
    	$input = $this->input;
        $designaStatus = $input['designa_status'];
        $vgsStatus = $input['vgs_status'];
        $limit = $input['limit'];
        $seasonId = $input['season'];

        $packs = \App\Pack::select( 'packs.prefix', 'packs.pack_number', 'packs.sale_id',  'users.first_name', 'users.last_name', 'users.email', 'users.mobile_dial_code','users.mobile',  'users.gender', 'users.dob', 'nation.name_en as nationality','res.name_en as residence', 'users.recieve_updates', 'users.account_id', 'user_packs.designa_customer_uid', 'user_packs.designa_customer_id')
                ->leftJoin('user_packs', 'user_packs.pack_id', 'packs.id')
                ->leftJoin('users', 'users.id', 'user_packs.user_id')
                ->join('countries as nation','nation.id','users.nationality_id')
                ->join('countries as res','res.id','users.residence_id')
                ->where('packs.season_id',$seasonId )

                ;
        if ($designaStatus != '') {
            if ($designaStatus == 'synced') {
                $packs->whereNotNull('user_packs.designa_customer_id');
                $packs->whereExists(function ($query) {
                    $query->select(\DB::raw(1))
                            ->from('user_car_packs as uc')
                            ->whereRaw("uc.pack_id = packs.id AND designa_card_uid IS NOT NULL AND  designa_setv_status = 1 AND designa_acc_status = 1");
                });
            } else {
                //$packs->whereNull('users.designa_customer_id');
                $packs->whereExists(function ($query) {
                    $query->select(\DB::raw(1))
                            ->from('user_car_packs as uc')
                            ->whereRaw("uc.pack_id = packs.id AND (designa_card_uid IS NULL OR  designa_setv_status = 0 OR designa_acc_status = 0)");
                });
            }
        }
        
        if ($vgsStatus != '') {
            if ($vgsStatus == 'synced') {
                $packs->where('user_packs.account_activation_status', 3);
            } else {
                $packs->where('user_packs.account_activation_status', '!=', 3);
            }
        }
        if ($limit == 'valid') {
            $configurations = \App\ApiConfiguration::where('name','valid_pack_limit')->first();
            $packs->where('packs.pack_number','<=',(isset($configurations->value) ? $configurations->value : 10000) );
        }

        return  $packs->get();
    }
}
