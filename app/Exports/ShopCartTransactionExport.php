<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;


class ShopCartTransactionExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $input;

    public function __construct(array $input)
    {
        $this->input = $input;
    }

    public function headings(): array
    {
        return [
            'Order ID',
            'Order Ref No.',
            'Amount',
            'Email',
            'First Name',
            'Last Name',
            'Mobile',
            'Date/Time Added',
            'Transaction Status',
            'Payment Status',
        ];
    }
    public function collection()
    {
    	$input = $this->input;
        $transaction_status = $input['transaction_status'];
        $payment_status = $input['payment_status'];
        $plate_form = $input['plate_form'];
        $start_date = $input['start_date'];
        $end_date = $input['end_date'];
        $payment_status = $input['payment_status'];
        $firstname = $input['firstname'];
        $lastname = $input['lastname'];
        $email = $input['email'];
        $mobile = $input['mobile'];

        $transaction = DB::Connection('mysql2')
            ->table('gv_shopcart_transaction')
            ->join('gv_user_payment_sessions', 'gv_shopcart_transaction.shopcartid', '=', 'gv_user_payment_sessions.shopcartid')
            ->select('gv_shopcart_transaction.salecode',
                'gv_shopcart_transaction.order_id',
                'gv_shopcart_transaction.totalAmount',
                'gv_shopcart_transaction.email',
                'gv_shopcart_transaction.firstname',
                'gv_shopcart_transaction.lastname',
                'gv_shopcart_transaction.mobile',
                'gv_shopcart_transaction.created_date',
                // 'gv_shopcart_transaction.transaction_status',
                // 'gv_user_payment_sessions.status as payment_status',
            )
            ->selectRaw('(CASE WHEN gv_shopcart_transaction.transaction_status = 1 THEN "Completed" WHEN gv_shopcart_transaction.transaction_status = 0 THEN "Not Completed" END) as transaction_status')
            ->selectRaw('(CASE WHEN gv_user_payment_sessions.status = 1 THEN "Created" WHEN gv_user_payment_sessions.status = 2 THEN "Initiated" WHEN gv_user_payment_sessions.status = 3 THEN "Authenticate" WHEN gv_user_payment_sessions.status = 4 THEN "Paid" END) as payment_status');

        if(!empty($transaction_status)){
            $status = ($transaction_status == "active")?1:0;
            $transaction = $transaction->where('gv_shopcart_transaction.status',$status);
        }
        if(!empty($payment_status)){
            $transaction_status =  $payment_status;
            $transaction = $transaction->where('gv_user_payment_sessions.status',$transaction_status);
        }
        if(!empty($plate_form)){
            $plate_form =  $plate_form;
            $transaction = $transaction->where('gv_user_payment_sessions.platform',$plate_form);
        }
        if(!empty($start_date) && !empty($end_date)){
            $start_date = date('Y-m-d',strtotime($start_date));
            $end_date = date('Y-m-d',strtotime($end_date));
            $transaction =  $transaction->whereDate('gv_shopcart_transaction.created_date','>=',$start_date)->whereDate('gv_shopcart_transaction.created_date','<=',$end_date);
        }else{
          if(empty($transaction_status) && empty($payment_status)
            && empty($plate_form) && empty($salecode)
            && empty($order_ref_no) && empty($firstname)
            && empty($lastname) && empty($email)
            && empty($mobile) ){

              $start_date = date('Y-m-d');
              $transaction =  $transaction->whereDate('gv_shopcart_transaction.created_date','>=',$start_date);
          }
        }
        if(!empty($salecode)){
            $search_val = $salecode;
            $transaction = $transaction->where('gv_shopcart_transaction.salecode', 'LIKE', '%' . $search_val . '%');
        }
        if(!empty($order_ref_no)){
            $search_val = $order_ref_no;
            $transaction = $transaction->where('gv_shopcart_transaction.order_id', 'LIKE', '%' . $search_val . '%');
        }
        if(!empty($firstname)){
            $search_val = $firstname;
            $transaction = $transaction->where('gv_shopcart_transaction.firstname', 'LIKE', '%' . $search_val . '%');
        }
        if(!empty($lastname)){
            $search_val = $lastname;
            $transaction = $transaction->where('gv_shopcart_transaction.lastname', 'LIKE', '%' . $search_val . '%');
        }
        if(!empty($email)){
            $search_val = $email;
            $transaction = $transaction->where('gv_shopcart_transaction.email', 'LIKE', '%' . $search_val . '%');
        }
        if(!empty($mobile)){
            $search_val = $mobile;
            $transaction = $transaction->where('gv_shopcart_transaction.mobile', 'LIKE', '%' . $search_val . '%');
        }

        $transaction =  $transaction->groupBy('gv_shopcart_transaction.shopcartid')->orderBy('gv_shopcart_transaction.created_date','desc')->get();
        return  $transaction;

    }
}
