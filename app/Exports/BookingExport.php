<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class BookingExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $input;

    public function __construct(array $input)
    {
        $this->input = $input;
    }

    public function headings(): array
    {
        return [
            'Program Date',
           
            'Vgs Product Code',
            'First Name',
            'Last Name',
            'Email',
            'Mobile',
            'Shop Cart Id',
            'Sale Code',
            'Amount',
            'Booking Type',
            'Booking Id',
            'Created Date',
            'Status',
            'Table',
            'Slot',
        ];
    }
    public function collection()
    {
    	$input = $this->input;
        $startDate = $input['startDate'];
        $endDate = $input['endDate'];

        $hourlyQuery = \App\BookSlot::where('book_slots.status','order_confirmed')
                                    ->where('book_slots.booking_type','hourly')
                                    ->rightJoin('program_slots','program_slots.booking_id','=','book_slots.id')
                                    ->join('slots','slots.id','=','program_slots.slot_id')
                                    ->join('vip_tables as vt','vt.id','=','program_slots.table_id')
                                    ->select('book_slots.program_date','book_slots.vgs_product_codes as productCode','book_slots.first_name','book_slots.last_name','book_slots.email','book_slots.mobile','book_slots.shop_cart_id','book_slots.sale_code','book_slots.amount','book_slots.booking_type','book_slots.id as bookingId','book_slots.created_at','book_slots.status','vt.name as tableName','slots.name as slotName');
        $dailyQuery = \App\BookSlot::where('book_slots.status','order_confirmed')
                                    ->where('book_slots.booking_type','daily')
                                    ->rightJoin('program_slots','program_slots.booking_id','=','book_slots.id')
                                    ->join('programs','programs.id','=','program_slots.program_id')
                                    ->join('slots','slots.id','=','program_slots.slot_id')
                                    ->join('vip_tables as vt','vt.id','=','program_slots.table_id')
                                    ->select('book_slots.program_date','book_slots.vgs_product_codes as productCode','book_slots.first_name','book_slots.last_name','book_slots.email','book_slots.mobile','book_slots.shop_cart_id','book_slots.sale_code','book_slots.amount','book_slots.booking_type','book_slots.id as bookingId','book_slots.created_at','book_slots.status');
        $dailyQuery->selectRaw('GROUP_CONCAT(DISTINCT vt.name) as tableName')->selectRaw("'Full Evening' as slotName");
        if($startDate != ''){
            $hourlyQuery = $hourlyQuery->whereDate('book_slots.program_date','>=',date('Y-m-d',strtotime($startDate)));
            $dailyQuery = $dailyQuery->whereDate('book_slots.program_date','>=',date('Y-m-d',strtotime($startDate)));
        }
        if($endDate != ''){
            $hourlyQuery = $hourlyQuery->whereDate('book_slots.program_date','<=',date('Y-m-d',strtotime($endDate)));
            $dailyQuery = $dailyQuery->whereDate('book_slots.program_date','<=',date('Y-m-d',strtotime($endDate)));
        }

        $allBooking = $hourlyQuery->union($dailyQuery)->orderBy('program_date','desc')
                        ->get();
        return  $allBooking;
        
    }
}
