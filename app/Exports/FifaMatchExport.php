<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class FifaMatchExport implements FromCollection, WithHeadings {

    protected $input;

    public function __construct(array $input) {
        $this->input = $input;
    }

    public function headings(): array {
        return [
            'id',
            'Title English',
            'Title Arabic',
            'Date',
            'Time',
            'Performance ID'
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection() {
        $input = $this->input;

        $matches = \App\FifaMatch::select('id', 'titles->en as eName', 'titles->ar as aName', \DB::raw("DATE_FORMAT(match_date,'%d-%m-%Y') as mDate"), 'match_time', 'performance_id')
                ->where('season_id', $input['seasonId'])
                ->where('match_date', '>=', $input['startDate']);

        if (!empty($input['endDate'])) {
            $matches->where('match_date', '>=', $input['endDate']);
        }
        return $matches->orderBy('match_date')->get();
    }

}
