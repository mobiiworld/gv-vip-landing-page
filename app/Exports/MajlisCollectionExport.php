<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class MajlisCollectionExport implements FromCollection, WithHeadings {

    protected $input;

    public function __construct(array $input) {
        $this->input = $input;
    }

    public function headings(): array {
        return [
            'Name',
            'Email',
            'Sale Code',
            'Payment Mode',
            'Amount',
            'Created At'
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection() {
        $input = $this->input;

        $bookings = \App\BookSlot::select('admins.name', 'admins.email', 'book_slots.sale_code', \DB::raw("CASE WHEN book_slots.payment_mode=1 THEN 'CASH' ELSE 'CARD' END AS pmode"), 'book_slots.amount', \DB::raw("DATE_FORMAT(book_slots.created_at,'%Y-%m-%d') as cDate"))
                ->join('admins', 'admins.id', 'book_slots.admin_id')
                ->where('book_slots.program_id', $input['programId'])
                ->whereNotNull('book_slots.admin_id');
        if (!empty($input['startDate'])) {
            $bookings->where('book_slots.operational_date', '>=', date("Y-m-d", strtotime($input['startDate'])));
        }
        if (!empty($input['endDate'])) {
            $bookings->where('book_slots.operational_date', '<=', date("Y-m-d", strtotime($input['endDate'])));
        }

        return $bookings->orderBy('book_slots.created_at')->get();
    }

}
