<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPayment extends Model
{
    protected $connection = 'mysql2';

    protected $table = 'gv_user_payments';
}
