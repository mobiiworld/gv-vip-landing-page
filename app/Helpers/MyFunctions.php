<?php

use Illuminate\Support\Facades\Log;

function successResponse($status, $data = null) {
    if (!empty($data)) {
        return response()->json([
                    "statusCode" => 1000,
                    "message" => $status,
                    "data" => $data,
        ]);
    } else {
        return response()->json([
                    "statusCode" => 1000,
                    "message" => $status
        ]);
    }
}

function errorResponse($status, $errorMessage = null) {
    return response()->json([
                "statusCode" => 2000,
                "message" => $status,
                "errorMessage" => $errorMessage
    ]);
}

function errorResponseWithData($status, $data = NULL, $errorMessage = null) {
    return response()->json([
                "statusCode" => 2000,
                "message" => $status,
                "data" => $data,
                "errorMessage" => $errorMessage
    ]);
}

function systemResponse($status) {
    return response()->json([
                "statusCode" => 401,
                "message" => $status
    ]);
    //return response()->json(['error' => $status, 401]);
}

function manageAuditLogs($array_data) {
    try {
        $audit_type = $array_data['audit_type'];
        $admin_id = $array_data['admin_id'];
        $admin_type = $array_data['admin_type'];
        $event = $array_data['event'];
        $old_values = $array_data['old_values'];
        $new_values = $array_data['new_values'];
        $url = $array_data['url'];
        $table_id = $array_data['table_id'];
        $table_name = $array_data['table_name'];

        $admin_logs = new \App\AuditLog();
        $admin_logs->audit_type = $audit_type;
        $admin_logs->admin_id = $admin_id;
        $admin_logs->admin_type = $admin_type;
        $admin_logs->event = $event;
        $admin_logs->old_values = $old_values;
        $admin_logs->new_values = $new_values;
        $admin_logs->url = $url;
        $admin_logs->table_id = $table_id;
        $admin_logs->table_name = $table_name;
        $admin_logs->save();
    } catch (\Exception $e) {
        Log::error('addAdminLogs', ['Exception' => $e->getTraceAsString()]);
    }
}

function getPprefixes($request) {
    switch ($request->type) {
        case 'country':
            $prefix = \App\CarCountry::select('plate_prefix_new as plate_prefix')
                    ->where('id', $request->id)
                    ->first();
            break;
        default:
            $prefix = App\Emirate::select('plate_prefix')
                    ->where('id', $request->id)
                    ->first();
            break;
    }

    // return json_decode($prefix->plate_prefix);
    return $prefix->plate_prefix;
}

function random_strings($length_of_string) {
    // random_bytes returns number of bytes
    // bin2hex converts them into hexadecimal format
    return substr(bin2hex(random_bytes($length_of_string)),
            0, $length_of_string);
}

function cdn($asset) {
    //Check if we added cdn's to the config file
    if (!env('CDN_ENABLED', false)) {
        return Storage::disk('public')->url($asset);
    } else {
        // return Storage::disk('azure')->url(env('CDN_FILE_DIR', 'dev/test/') . $asset);
        return Storage::disk('azure')->url($asset);
    }
}

function partner_logos() {
    return [
        'expo2020dubai' => asset('web/images/expo-2021-dubai.png'),
        'roxycinemas' => asset('web/images/roxycinemas.jpg'),
        'roxy' => asset('web/images/roxycinemas.jpg'),
        'lagunawaterpark' => asset('web/images/lagunawaterparks.jpg'),
        'thegreenplanet' => asset('web/images/greenplanet.jpg'),
        'dpr-allparks' => asset('web/images/dubaiparks.jpg'),
        'dubaiparksandresorts' => asset('web/images/dubaiparks.jpg'),
        'dpr' => asset('web/images/dubaiparks.jpg'),
        'bollywoodparksdubai' => asset('web/images/bollywoodparks.jpg'),
        'motiongatedubai' => asset('web/images/motiongate.jpg'),
        'legolanddubai' => asset('web/images/legoland.jpg'),
        'legolandwaterpark' => asset('web/images/legolandwaterpark.jpg'),
        'seabreeze' => asset('web/images/sea-breeze.jpg'),
        'insideburjalarab' => asset('web/images/ibaa.jpg'),
        'ibaa' => asset('web/images/ibaa.jpg'),
    ];
}

function curlRequestPost($url, $requestData, $header = [],$method='POST') {
    if (empty($url)) {
        return false;
    }
    try {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        $params = (!empty($requestData)) ? json_encode($requestData) : NULL;
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        if (!empty($header)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        $output = curl_exec($ch);
        /* \Log::channel('syncCurl')->info('Request');
          \Log::channel('syncCurl')->info(json_encode($header));
          \Log::channel('syncCurl')->info($url);
          \Log::channel('syncCurl')->info($params);
          \Log::channel('syncCurl')->info('Response');
          \Log::channel('syncCurl')->info($output);
          if(curl_errno($ch)){
          \Log::channel('syncCurl')->error('Error');
          \Log::channel('syncCurl')->error(curl_errno($ch));
          \Log::channel('syncCurl')->error(curl_error($ch));
          }
          $info = curl_getinfo($ch); */
        //dd($output,$info);
        return json_decode($output, true);
    } catch (\Exception $e) {
        \Log::channel('syncCurl')->error('Exception');
        \Log::channel('syncCurl')->error($e);
    }
    return false;
}

function vgsMajlisPcodes($caseCheck, &$program) {
    switch ($caseCheck) {
        case 'standard|1':
            $data['2_seater'] = [
                "amount" => $program->vgs_pcodes['standard']['2_seater']['amount'],
                "product_id" => $program->vgs_pcodes['standard']['2_seater']['smoking'],
                "product_id_vip" => $program->vgs_pcodes['standard']['2_seater']['vip_smoking'],
            ];
            $data['4_seater'] = [
                "amount" => $program->vgs_pcodes['standard']['4_seater']['amount'],
                "product_id" => $program->vgs_pcodes['standard']['4_seater']['smoking'],
                "product_id_vip" => $program->vgs_pcodes['standard']['4_seater']['vip_smoking'],
            ];
            $data['6_seater'] = [
                "amount" => $program->vgs_pcodes['standard']['6_seater']['amount'],
                "product_id" => $program->vgs_pcodes['standard']['6_seater']['smoking'],
                "product_id_vip" => $program->vgs_pcodes['standard']['6_seater']['vip_smoking'],
            ];
            break;
        case 'standard|2':
            $data['2_seater'] = [
                "amount" => $program->vgs_pcodes['standard']['2_seater']['amount'],
                "product_id" => $program->vgs_pcodes['standard']['2_seater']['nonsmoking'],
                "product_id_vip" => $program->vgs_pcodes['standard']['2_seater']['vip_nonsmoking'],
            ];
            $data['4_seater'] = [
                "amount" => $program->vgs_pcodes['standard']['4_seater']['amount'],
                "product_id" => $program->vgs_pcodes['standard']['4_seater']['nonsmoking'],
                "product_id_vip" => $program->vgs_pcodes['standard']['4_seater']['vip_nonsmoking'],
            ];
            $data['6_seater'] = [
                "amount" => $program->vgs_pcodes['standard']['6_seater']['amount'],
                "product_id" => $program->vgs_pcodes['standard']['6_seater']['nonsmoking'],
                "product_id_vip" => $program->vgs_pcodes['standard']['6_seater']['vip_nonsmoking'],
            ];
            break;
        case 'premium|1':
            $data['2_seater'] = [
                "amount" => $program->vgs_pcodes['premium']['2_seater']['amount'],
                "product_id" => $program->vgs_pcodes['premium']['2_seater']['smoking'],
                "product_id_vip" => $program->vgs_pcodes['premium']['2_seater']['vip_smoking'],
            ];
            $data['4_seater'] = [
                "amount" => $program->vgs_pcodes['premium']['4_seater']['amount'],
                "product_id" => $program->vgs_pcodes['premium']['4_seater']['smoking'],
                "product_id_vip" => $program->vgs_pcodes['premium']['4_seater']['vip_smoking'],
            ];
            $data['6_seater'] = [
                "amount" => $program->vgs_pcodes['premium']['6_seater']['amount'],
                "product_id" => $program->vgs_pcodes['premium']['6_seater']['smoking'],
                "product_id_vip" => $program->vgs_pcodes['premium']['6_seater']['vip_smoking'],
            ];
            break;
        case 'premium|2':
            $data['2_seater'] = [
                "amount" => $program->vgs_pcodes['premium']['2_seater']['amount'],
                "product_id" => $program->vgs_pcodes['premium']['2_seater']['nonsmoking'],
                "product_id_vip" => $program->vgs_pcodes['premium']['2_seater']['vip_nonsmoking'],
            ];
            $data['4_seater'] = [
                "amount" => $program->vgs_pcodes['premium']['4_seater']['amount'],
                "product_id" => $program->vgs_pcodes['premium']['4_seater']['nonsmoking'],
                "product_id_vip" => $program->vgs_pcodes['premium']['4_seater']['vip_nonsmoking'],
            ];
            $data['6_seater'] = [
                "amount" => $program->vgs_pcodes['premium']['6_seater']['amount'],
                "product_id" => $program->vgs_pcodes['premium']['6_seater']['nonsmoking'],
                "product_id_vip" => $program->vgs_pcodes['premium']['6_seater']['vip_nonsmoking'],
            ];
            break;
        default:
            $data = [
                "2_seater" => [
                    "amount" => null,
                    "product_id" => null,
                    "product_id_vip" => null
                ],
                "4_seater" => [
                    "amount" => null,
                    "product_id" => null,
                    "product_id_vip" => null
                ],
                "6_seater" => [
                    "amount" => null,
                    "product_id" => null,
                    "product_id_vip" => null
                ]
            ];
            break;
    }
    return $data;
}

function generateNumericOTP($n) {
      
    // Take a generator string which consist of
    // all numeric digits
    $generator = "1357902468";
  
    // Iterate for n-times and pick a single character
    // from generator and append it to $result
      
    // Login for generating a random character from generator
    //     ---generate a random number
    //     ---take modulus of same with length of generator (say i)
    //     ---append the character at place (i) from generator to result
  
    $result = "";
  
    for ($i = 1; $i <= $n; $i++) {
        $result .= substr($generator, (rand()%(strlen($generator))), 1);
    }
  
    // Return result
    return $result;
}

function generatePlateCode($plate_prefix,$plate_number,$append=' '){
    $exclude_plates = \App\RemovedPlatePrefix::select(\DB::raw("LOWER(plate_prefix) as plate_prefix"))->pluck('plate_prefix')->toArray();
    if(!empty($exclude_plates) && in_array(strtolower($plate_prefix),$exclude_plates)){
        $plate_prefix = $append = '';
    }
    return $plate_prefix . $append . $plate_number;
}

function sendCommonEmail($data){
    $locale = app()->getLocale();
    $email_template = \App\Emailtemplate::where('type', $data['type'])->first();
    $sk = 'subject_'.$locale;
    $subject = $email_template->{$sk};
    $bk = 'body_'.$locale;
    $body = $email_template->{$bk};
    switch ($data['type']){
        case 'fitness-success' : 
                    $body = str_replace(':user_name', $data['user_name'], $body);
                    $body = str_replace(':coupon_code', $data['coupon_code'], $body);
                    break;
        case 'fitness-failed' : 
            $body = str_replace(':user_name', $data['user_name'], $body);
            break;
    }
    $mail_data = [
        'subject' => $subject,
        'template' => $body,
    ];
    \Mail::to($data['email'])
                    ->send(new \App\Mail\SendCommonMail($mail_data));
}