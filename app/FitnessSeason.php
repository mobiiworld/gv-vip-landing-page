<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FitnessSeason extends Model {

    protected $table = 'fitness_seasons';
    protected $primaryKey = 'id';
    protected $casts = [
        'names' => 'array',
        'target_steps' => 'array',
        'program_name' => 'array',
        'program_description' => 'array',
        'popup_titles' => 'array',
        'popup_messages' => 'array'
    ];

    public function prizes() {
        return $this->hasMany(FitnessPrize::class, 'fitness_season_id', 'id');
    }

    public function seasonImages() {
        return $this->hasMany(FitnessSeasonImages::class, 'fitness_season_id', 'id');
    }

    public function dailyWinners() {
        return $this->hasMany(FitnessDailyTracks::class, 'fitness_season_id', 'id')->where('status', 1);
    }

    public function monthlyWinners() {
        return $this->hasMany(FitnessMonthlyTrack::class, 'fitness_season_id', 'id')->where('status', 1);
    }

    public function seasonWinners() {
        return $this->hasMany(FitnessUser::class, 'fitness_season_id', 'id')->where('is_winner', 1);
    }

    public function participants() {
        return $this->hasMany(FitnessUser::class, 'fitness_season_id', 'id');
    }

}
