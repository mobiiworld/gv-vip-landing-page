<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PackActivated extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    public $template;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $locale = app()->getLocale();
        $cat = $this->data['category'];
        $type = 'vip-activated-'.$cat;
        $email_template = \App\Emailtemplate::where('type', $type)->first();
        if(empty($email_template)){
            $email_template = \App\Emailtemplate::where('type', 'vip-activated')->first();
        }
        
        if(!empty($email_template)){
            $banner_image = asset('web/images/email/common_'.app()->getLocale().'.jpg');
            $images = [
                'diamond' => asset('web/images/email/diamond_'.app()->getLocale().'.jpg'),
                'gold' => asset('web/images/email/gold_'.app()->getLocale().'.jpg'),
                'silver' => asset('web/images/email/silver_'.app()->getLocale().'.jpg'),
                'platinum' => asset('web/images/email/platinum_'.app()->getLocale().'.jpg'),
                //'private' => asset('web/images/email/private_'.app()->getLocale().'.jpg'),
               
            ];
            if(isset($images[$this->data['category']])){
                $banner_image = $images[$this->data['category']];
            }
            $sk = 'subject_'.$locale;
            $subject = $email_template->{$sk};
            $bk = 'body_'.$locale;
            $body = $email_template->{$bk};
            $subject = str_replace(':pack_number', $this->data['pack_number'], $subject);
            $body = str_replace(':user_name', $this->data['first_name'], $body);
            $body = str_replace(':user_pack', $this->data['pack_type'], $body);
            $body = str_replace(':banner_image', $banner_image, $body);
            $activated_url = route('my.activated.pack',[app()->getLocale(),$this->data['user_pack_id']]);
            $body = str_replace(':activated_url', $activated_url, $body);
            $this->template = $body;
            return $this->subject($subject)->view('emails.template');
        }
    }
}
