<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CarMail extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * The OTP instance.
     *
     * @var UserOtp
     */
    public $user;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $data) {
          $this->user = $user;
          $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {

        if($this->data['lang'] == 'ar'){
            return $this->subject($this->data['subject'])->markdown('emails.edit-car-ar');
        }else{
            return $this->subject($this->data['subject'])->markdown('emails.edit-car-en');
        }
    }

}
