<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserOtpEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $template;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $locale = app()->getLocale();
        $email_template = \App\Emailtemplate::where('type', 'otp')->first();
        if(!empty($email_template)){
            $sk = 'subject_'.$locale;
            $subject = $email_template->{$sk};
            $bk = 'body_'.$locale;
            $body = $email_template->{$bk};
            $this->template = str_replace(':user_otp', $this->data['otp'], $body);
            return $this->subject($subject)->view('emails.template');
        }else{
            if(app()->getLocale() == 'ar'){
                return $this->subject(trans('passwords.otp.subject'))->markdown('emails.otp-ar');
            }else{
                return $this->subject(trans('passwords.otp.subject'))->markdown('emails.otp-en');
            }
        }
    }
}
