<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPaymentSessions extends Model
{
    protected $connection = 'mysql2';

    protected $table = 'gv_user_payment_sessions';

    public function getUserPayment(){
        return $this->hasOne('App\UserPayment','sessionid','id');
    }
    public function getCarInfo(){
        return $this->hasOne('App\GvCarInfo','uniquegeneratedid','shopcartid');
    }
}
