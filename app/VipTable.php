<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VipTable extends Model
{
    use Uuids;
   
    protected $primaryKey = 'id';
    public $incrementing = false;
}
