<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParkingServices extends Model {

    protected $table = 'parking_services';
    protected $casts = [
        'names' => 'array',
        'descriptions' => 'array'
    ];

    public function getIconAttribute($value) {
        return !empty($value) ? cdn('service_icons/' . $value) : null;
    }

}
