<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VgsLog extends Model
{
    use Uuids;
    public $incrementing = false;    
    protected $primaryKey = 'id';
}
