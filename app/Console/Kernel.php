<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

        /**
         * The Artisan commands provided by your application.
         *
         * @var array
         */
        protected $commands = [
                Commands\DesignaCreateCustomer::class,
                Commands\syncAccount::class,
                Commands\RamadanTableOccupancy::class,
                Commands\ClearUserParkings::class,
        ];

        /**
         * Define the application's command schedule.
         *
         * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
         * @return void
         */
        protected function schedule(Schedule $schedule)
        {
                /* commented on 30/11/2020 - Amrutha -  Akhil
        $schedule->command('designa:createcustomer')
                ->everyFiveMinutes()
                ->withoutOverlapping()
                ->onOneServer();
        $schedule->command('sync:account')
                ->everyFiveMinutes()
                ->withoutOverlapping()
                ->onOneServer();*/
                // $schedule->command('ramadan:releaseBooking')
                //         ->everyMinute()
                //         ->withoutOverlapping()
                //         ->onOneServer();

                $schedule->command('travel:signatureRemider')
                        ->dailyAt('01:00')
                        ->withoutOverlapping()
                        ->onOneServer();

                $schedule->command('travel:contractBeforeExpiryRemider')
                        ->dailyAt('01:00')
                        ->withoutOverlapping()
                        ->onOneServer();

                $schedule->command('travel:contractAfterExpiryRemider')
                        ->dailyAt('01:00')
                        ->withoutOverlapping()
                        ->onOneServer();

                $schedule->command('travel:tradeLicenceExpiryRemider')
                        ->dailyAt('01:00')
                        ->withoutOverlapping()
                        ->onOneServer();

                // $schedule->command('ramadan:occupancy')
                //         ->everyFiveMinutes()
                //         ->unlessBetween('02:00', '17:45')
                //         ->withoutOverlapping()
                //         ->onOneServer();

                // $schedule->command('shows:autosync')
                //         ->everyMinute()
                //         ->withoutOverlapping()
                //         ->onOneServer();

                // $schedule->command('social:feed_update')
                //         ->everyThirtyMinutes()
                //         ->withoutOverlapping()
                //         ->onOneServer();

                // $schedule->command('school:paymentReminder')
                //         ->dailyAt('03:00')
                //         ->withoutOverlapping()
                //         ->onOneServer();
                // $schedule->command('school:paymentExpired')
                //         ->dailyAt('03:00')
                //         ->withoutOverlapping()
                //         ->onOneServer();

                $schedule->command('clear:userparkings')
                        ->dailyAt('06:00')
                        ->withoutOverlapping()
                        ->onOneServer();
                        
                // $schedule->command('openweather:update')
                //         ->everyFifteenMinutes()
                //         ->withoutOverlapping()
                //         ->onOneServer();
               

                $schedule->command('wp:status-update')
                        ->dailyAt('03:00')
                        ->withoutOverlapping()
                        ->onOneServer();

                $schedule->command('media_qrcode:remove')
                        ->dailyAt('01:00')
                        ->withoutOverlapping()
                        ->onOneServer();

                $schedule->command('fazaa:status-update')
                        ->everyThirtyMinutes()
                        ->withoutOverlapping()
                        ->onOneServer();
        }

        /**
         * Register the commands for the application.
         *
         * @return void
         */
        protected function commands()
        {
                $this->load(__DIR__ . '/Commands');

                require base_path('routes/console.php');
        }
}
