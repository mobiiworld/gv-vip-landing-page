<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdateSocialFeeds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'social:feed_update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Social Feed Update';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
          $userKey = env('TAGBOX_USERKEY');
          $url = env('TAGBOX_URL');
          $limit = env('TAGBOX_LIMIT',30);
          try{
              $updated_ids = [];
              $q1= \DB::Connection('mysql2')->table('information_schema.tables')->where(['table_schema' => env('DB_DATABASE_SERV2'), 'table_name' => 'gv_taggbox_widget_id'])->first();
              if(!empty($q1)){
                $widgets = \DB::Connection('mysql2')->table('gv_taggbox_widget_id')->select('wzid')->distinct()->get();
                //dd($widgets);
                if(!empty($widgets)){
                  foreach($widgets as $wid){
                      $wallId = $wid->wzid;
                      if(!empty($userKey) && !empty($wallId) && !empty($url)){
                        $api_link = $url. $wallId . '?user_key=' . $userKey . '&format=json&limit='.$limit;
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_HEADER, 0);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_URL, $api_link);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                        curl_setopt($ch, CURLOPT_VERBOSE, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        $response = curl_exec($ch);
                        //\Log::channel('syncCurl')->info($api_link);
                        //\Log::channel('syncCurl')->info($response);
                        curl_close($ch);
                        $data = json_decode($response);

                        $feeds = (isset($data->responseCode) && $data->responseCode == 200 && isset($data->responseData) && !empty($data->responseData)) ? $response : NULL;
                        if(!empty($feeds)){
                          \DB::Connection('mysql2')->table('gv_taggbox_widget_id')->where('wzid',$wallId)->update(['feeds'=>$feeds,'updated_at'=>time()]);
                          $updated_ids[] = $wallId;
                        }
                        
                    }
                  }
                }
                if(env('APP_ENV') != 'production'){
                  \Log::channel('syncCurl')->info('Social feed synced');
                  \Log::channel('syncCurl')->info(json_encode($updated_ids));
                }
              }else{
                \Log::channel('syncCurl')->info('Table not exists');
              }
              

          }catch(\Exception $e){
            \Log::channel('syncCurl')->error($e);
          }
          
          
          
    }
}
