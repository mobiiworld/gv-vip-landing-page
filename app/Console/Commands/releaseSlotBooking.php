<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ProgramSlot;
use App\BookSlot;

class releaseSlotBooking extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ramadan:releaseBooking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'releasing the idle booking slots';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $pendingMins = "5 min";
        $underPaymentMins = "15 min";

        $pendingTime = new \DateTime();
        $pendingTime->modify('-' . $pendingMins);
        $pendingDate = $pendingTime->format('Y-m-d H:i:s');

        $underPaymentTime = new \DateTime();
        $underPaymentTime->modify('-' . $underPaymentMins);
        $underPaymentDate = $underPaymentTime->format('Y-m-d H:i:s');

        /*
          // tHis created for the edge case user payment succeded but failed to update order_confirmed 11/apr/2022
          $pendingPaymentBookings = BookSlot::where("status","!=",'order_confirmed')->where('created_at','<=',$underPaymentDate)->get();
          if(!empty($pendingPaymentBookings)){
          foreach($pendingPaymentBookings as $book){
          $payment = \DB::Connection('mysql2')
          ->table('gv_user_payments')
          ->where('majlis_reference_id', $book->id)
          ->first();

          $slotIds = explode(",",$book->program_slot_id);
          if(!empty($slotIds)){
          if($payment->payment_status == 1){
          ProgramSlot::where('booking_id',$book->id)->whereIn('id',$slotIds)->update(['program_status'=>3]);
          $book->status = 'order_confirmed';
          $book->save();
          }
          }
          }
          } */


        $pendingBookings = BookSlot::where("status", 'pending')->where('created_at', '<=', $pendingDate)->get();
        //dd($pendingBookings);
        if (!empty($pendingBookings)) {
            foreach ($pendingBookings as $pending) {
                $slotIds = explode(",", $pending->program_slot_id);
                if (!empty($slotIds)) {
                    ProgramSlot::where('booking_id', $pending->id)->whereIn('id', $slotIds)->update(['program_status' => 1, 'booking_id' => NULL]);
                    $pending->status = 'pending_timeout';
                    $pending->save();

                    $history = new \App\MajlisBookingStatusHistory();
                    $history->booking_id = $pending->id;
                    $history->status = 'pending_timeout';
                    $history->description = 'Cronjob status update.Status was pending.';
                    $history->save();
                }
            }
        }


        $underPaymentBookings = BookSlot::where("status", 'waiting_confirmation')->where('status_updated_at', '<=', $underPaymentDate)->get();
        if (!empty($underPaymentBookings)) {
            foreach ($underPaymentBookings as $book) {
                $slotIds = explode(",", $book->program_slot_id);
                if (!empty($slotIds)) {
                    ProgramSlot::where('booking_id', $book->id)->whereIn('id', $slotIds)->update(['program_status' => 1, 'booking_id' => NULL]);
                    $book->status = 'waiting_confirmation_timeout';
                    $book->save();

                    $history = new \App\MajlisBookingStatusHistory();
                    $history->booking_id = $book->id;
                    $history->status = 'waiting_confirmation_timeout';
                    $history->description = 'Cronjob status update.Status was waiting confirmation.';
                    $history->save();
                }
            }
        }


        $pendingPaymentBookings = BookSlot::where("status", 'waiting_payment')->where('status_updated_at', '<=', $underPaymentDate)->get();
        if (!empty($pendingPaymentBookings)) {
            foreach ($pendingPaymentBookings as $book) {
                $payment = \DB::Connection('mysql2')
                        ->table('gv_user_payments')
                        ->where('majlis_reference_id', $book->id)
                        ->first();

                $slotIds = explode(",", $book->program_slot_id);
                if (!empty($slotIds)) {
                    if (!empty($payment) && $payment->payment_status == 1) {
                        ProgramSlot::where('booking_id', $book->id)->whereIn('id', $slotIds)->update(['program_status' => 3]);
                        $book->status = 'order_confirmed';
                        $book->save();

                        //check for vip free table;
                        if ($book->is_vip && ($book->payment_extras['vipCount'] > 0)) {
                            $season = \App\Season::where('status', 1)->first();
                            $majlisVipTableCountUsed = $book->payment_extras['vipCount'];

                            $userPacks = \App\Pack::select('packs.id', 'packs.remaining_tables')
                                    ->join('users', 'users.pack_id', 'packs.id')
                                    ->where(function ($q) use ($book) {
                                        $q->where('packs.drupal_user_id', $book->booking_user_id)
                                        ->orWhere('users.email', $book->email);
                                    })
                                    ->where('packs.season_id', $season->id)
                                    ->where('packs.remaining_tables', '>', 0);
                            if ($book->payment_extras['vipType'] == 'fullday') {
                                $userPacks->whereIn('packs.category', ['private', 'platinum', 'diamond'])
                                        ->orderByRaw("FIELD(packs.category,'private','platinum','diamond')");
                            } else {
                                $userPacks->whereIn('packs.category', ['gold', 'silver', 'complimentary'])
                                        ->orderByRaw("FIELD(packs.category,'gold','silver','complimentary')");
                            }
                            $userPacks->get()->each(function ($pack)use (&$majlisVipTableCountUsed) {
                                if ($majlisVipTableCountUsed > 0) {
                                    if ($pack->remaining_tables <= $majlisVipTableCountUsed) {
                                        $majlisVipTableCountUsed = $majlisVipTableCountUsed - $pack->remaining_tables;
                                        $pack->remaining_tables = 0;
                                        $pack->save();
                                    } else {
                                        $pack->remaining_tables = $pack->remaining_tables - $majlisVipTableCountUsed;
                                        $majlisVipTableCountUsed = 0;
                                        $pack->save();
                                    }
                                }
                            });
                        }

                        $history = new \App\MajlisBookingStatusHistory();
                        $history->booking_id = $book->id;
                        $history->status = 'order_confirmed';
                        $history->description = 'Cronjob status update.Status was waiting payment.';
                        $history->save();
                    } else {
                        ProgramSlot::where('booking_id', $book->id)->whereIn('id', $slotIds)->update(['program_status' => 1, 'booking_id' => NULL]);
                        $book->status = 'waiting_payment_timeout';
                        $book->save();

                        $history = new \App\MajlisBookingStatusHistory();
                        $history->booking_id = $book->id;
                        $history->status = 'waiting_payment_timeout';
                        $history->description = 'Cronjob status update.Status was waiting payment.';
                        $history->save();
                    }
                }
            }
        }
    }

}
