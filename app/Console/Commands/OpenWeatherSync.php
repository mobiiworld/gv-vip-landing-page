<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class OpenWeatherSync extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'openweather:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Open Weather Sync';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('GV_WEB_URL').'update-weather-data',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'GV-Unique-Key: '.env('DRUPAL_UNIQUE_KEY'),
                'cache-control: no-cache',
            ),
        ));
        
        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response);
    }
}
