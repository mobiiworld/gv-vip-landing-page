<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Traits\AccountActivationTraits;

class syncAccount extends Command
{
    use AccountActivationTraits;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:account';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync user packs and update account_id & account_activation_status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $season = \App\Season::where('status',1)->first();        
        $seasonId = (!(empty($season))) ? $season->id : NULL;
        $packs = \App\Pack::select('packs.id','packs.prefix','packs.pack_number','packs.sale_id','users.id as user_id','users.first_name','users.last_name','users.email','users.mobile','users.mobile_dial_code','users.gender','users.dob','users.nationality_id','users.recieve_updates','users.account_activation_status','users.account_id')
        ->leftJoin('users','users.pack_id','packs.id')
        ->where('users.account_activation_status','!=',3)
        ->where('packs.season_id',$seasonId)
        ->get();

        if(!empty($packs)){
            foreach ($packs as $key => $userPack) {
                $this->activateAccount($userPack);
            }
        }


    }
}
