<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class AutoSyncShows extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shows:autosync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show Auto Sync cron';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $url = env('GV_WEB_URL').'show-sync-cron';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($resp);
    }

}
