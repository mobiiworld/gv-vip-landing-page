<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MediaImageRemoveCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'media_qrcode:remove';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove media images ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $uniqueKey = env('DRUPAL_UNIQUE_KEY','7f3^`H_Gx~7neyl73TZykP_sY`J)Jp');
        $header = array('Content-Type: application/json','GV-Language: '.app()->getLocale(),'GV-Unique-Key: '.$uniqueKey);

        $url = env('GV_WEB_URL').'delete-qrcodes-images';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);

        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($resp);
    }
}
