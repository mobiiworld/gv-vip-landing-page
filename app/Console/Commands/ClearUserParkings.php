<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ClearUserParkings extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:userparkings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This is to clear user save parkings history.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $configurations = \App\ApiConfiguration::where('name', 'clear_user_parkings_cron')->first();
        if ($configurations->value == "enable") {
            \DB::table('save_my_parkings')->truncate();
        }
    }

}
