<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\BookSlot;
use App\ApiConfiguration;

class RamadanTableOccupancy extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ramadan:occupancy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check table occupied or not';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $configurations = ApiConfiguration::where('name', 'work_station_id')->first();
        BookSlot::select('id', 'sale_code')
                ->whereRaw('NOW() >= actual_program_dt AND NOW() <= CONCAT(DATE_ADD(program_date,INTERVAL 1 DAY)," 02:00:00")')
                //->whereRaw('"2022-03-18 20:20:00" >= actual_program_dt AND "2022-03-18 20:20:00" <= CONCAT(DATE_ADD(program_date,INTERVAL 1 DAY)," 02:00:00")')
                ->whereNotNull('program_slot_id')
                ->whereNotNull('sale_code')
                ->where('is_occupied', false)
                ->get()->each(function ($book)use (&$configurations) {
            if (!empty($this->checkOccupancy($configurations, $book->sale_code))) {
                $book->is_occupied = true;
                $book->save();
            }
        });
    }

    private function checkOccupancy(&$configurations, $SaleCode) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => config('accountConfig.pprUrl') . "&cmd=SALE",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode([
                'Header' => ["WorkstationId" => $configurations->value],
                'Request' => [
                    'Command' => 'LoadEntSale',
                    'LoadEntSale' => ['SaleCode' => $SaleCode]
                ]
            ]),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'cache-control: no-cache',
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response);
        if (isset($response->Answer->LoadEntSale->Sale->Sale->MediaList)) {
            $mediaCodes = $response->Answer->LoadEntSale->Sale->Sale->MediaList;
            $LastUsageDateTime = null;
            foreach ($mediaCodes as $mediaCode) {
                if (isset($mediaCode->LastUsageDateTime)) {
                    $LastUsageDateTime = $mediaCode->LastUsageDateTime;
                    break;
                }
            }
            //$LastUsageDateTime = $response->Answer->LoadEntSale->Sale->Sale->MediaList[0]->LastUsageDateTime;
            return $LastUsageDateTime;
        } else {
            return null;
        }
        return null;
    }

}
