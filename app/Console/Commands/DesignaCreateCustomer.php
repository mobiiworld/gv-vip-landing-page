<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ApiConfiguration;
use App\User;
use App\UserCar;
use App\Traits\GvpTraits;

class DesignaCreateCustomer extends Command {

    use GvpTraits;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'designa:createcustomer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create designa customer account and set cars';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $config = ApiConfiguration::select('name', 'value')->get()->keyBy('name')->toArray();
        $season = \App\Season::where('status',1)->first();        
        $seasonId = (!(empty($season))) ? $season->id : NULL;
        User::select('id', 'first_name', 'last_name', 'email', 'mobile_dial_code', 'mobile', 'residence_id', 'pack_id', 'designa_customer_uid', 'designa_customer_id')
                ->with('residence:id,name_en,code,dial_code')
                ->with('pack:id,sale_id,prefix,pack_number')
                ->with('cars:id,user_id,country_id,emirate_id,plate_prefix,plate_number,designa_card_uid,designa_park_id')
                ->whereHas('cars')
                //->where('id', '0afb2a30-13a1-11eb-890c-930a6455c1f1')
                ->where('season_id',$seasonId)
                ->get()
                ->each(function($user) use(&$config) {
                    try {
                        if (empty($user->designa_customer_uid)) {
                            $details3 = '';
                            foreach ($user->cars as $car) {
                                $details3 .= ($car->country) ? $car->country->code : null;
                                if ($car->emirate) {
                                    $details3 .= '$' . $car->emirate->code;
                                }
                                $details3 .= '$' . $car->plate_prefix . '$' . $car->plate_number . '^';
                            }

                            $userRequest = [
                                'user' => $config['designa_user']['value'],
                                'pwd' => $config['designa_password']['value'],
                                'lastName' => $user->last_name,
                                'firstName' => $user->first_name,
                                'country' => $user->residence->code,
                                'phone' => $user->mobile_dial_code . $user->mobile,
                                'email' => $user->email,
                                'detail1' => $user->pack->prefix . $user->pack->pack_number,
                                'detail2' => $user->id,
                                'detail3' => trim($details3, '^'),
                            ];
                            $xml = $this->createCustomer($userRequest);
                            if (!empty($xml)) {
                                $xml = simplexml_load_string($xml);
                                $xml->registerXPathNamespace('ns1', 'http://www.designa.de/');
                                $xml = $xml->xpath('//ns1:createCustomerFullResponse/ns1:createCustomerFullResult');
                                if (isset($xml[0])) {
                                    $xml = $xml[0];
                                    //$xml = $xml->xpath('//ns1:createCustomerFullResponse/ns1:createCustomerFullResult')[0];
                                    $json = json_encode($xml);
                                    $response = json_decode($json, true);
                                    if (isset($response['@attributes'])) {
                                        $user->designa_customer_uid = $response['@attributes']['UId'];
                                        $user->designa_customer_id = $response['@attributes']['ID'];
                                        $user->save();
                                    }
                                }
                            }
                        }//if designa user uid null
                        //Create Designa Card for each car
                        if (!empty($user->designa_customer_id)) {
                            foreach ($user->cars as $car) {
                                if (empty($car->designa_card_uid)) {
                                    $cardRequest = [
                                        'user' => $config['designa_user']['value'],
                                        'pwd' => $config['designa_password']['value'],
                                        'personID' => $user->designa_customer_id,
                                        'cardType' => $config['designa_card_type']['value'],
                                        'carparkID' => $config['designa_carpark_id']['value'],
                                        'groupID' => $config['designa_group_id']['value'],
                                        'applicID' => $config['designa_application_id']['value'],
                                        'expiryDate' => trim($config['designa_expiration_date']['value']),
                                    ];

                                    $xml = $this->createCard($cardRequest);
                                    if (!empty($xml)) {
                                        $xml = simplexml_load_string($xml);
                                        $xml->registerXPathNamespace('ns1', 'http://www.designa.de/');
                                        $xml = $xml->xpath('//ns1:createCardResponse/ns1:createCardResult');
                                        if (isset($xml[0])) {
                                            $xml = $xml[0];
                                            //$xml = $xml->xpath('//ns1:createCardResponse/ns1:createCardResult')[0];
                                            $json = json_encode($xml);
                                            $response = json_decode($json, true);
                                            if (isset($response['@attributes'])) {
                                                $car->designa_card_uid = $response['@attributes']['UId'];
                                                $car->designa_card_id = $response['@attributes']['ID'];
                                                $car->designa_park_id = isset($response['Carpark']['@attributes']) ? $response['Carpark']['@attributes']['UId'] : null;
                                            }
                                        }
                                    }
                                }//designa_card_uid empty

                                if ($car->designa_setv_status == 0) {
                                    $ccd = (!empty($car->country) ? $car->country->code : null);
                                    $ecd = (!empty($car->emirate) ? $car->emirate->code : null);
                                    $vehicleRequest = [
                                        'user' => $config['designa_user']['value'],
                                        'pwd' => $config['designa_password']['value'],
                                        'personGuid' => $user->designa_customer_uid,
                                        'CountryCode' => $ccd . $ecd,
                                        'LicensePlate' => $car->plate_prefix . ' ' . $car->plate_number,
                                        'VIPCard' => $car->designa_card_uid,
                                    ];
                                    $xml = $this->setVehicle($vehicleRequest);
                                    if (!empty($xml)) {
                                        $xml = simplexml_load_string($xml);
                                        $xml->registerXPathNamespace('ns1', 'http://www.designa.de/');
                                        $xml = $xml->xpath('//ns1:setVehicleResponse/ns1:setVehicleResult');
                                        if (isset($xml[0])) {
                                            $xml = $xml[0];
                                            //$xml = $xml->xpath('//ns1:setVehicleResponse/ns1:setVehicleResult')[0];
                                            $json = json_encode($xml);
                                            $response = json_decode($json, true);
                                            if (isset($response[0])) {
                                                if ($response[0] == 0) {
                                                    $car->designa_setv_status = 1;
                                                }
                                            }
                                        }
                                    }
                                }//if designa set vehicle not done

                                if ($car->designa_acc_status == 0) {
                                    $ccd = (!empty($car->country) ? $car->country->code : null);
                                    $ecd = (!empty($car->emirate) ? $car->emirate->code : null);
                                    $assignRequest = [
                                        'user' => $config['designa_user']['value'],
                                        'pwd' => $config['designa_password']['value'],
                                        'cardUId' => $car->designa_card_uid,
                                        'cardCarrierKindId' => $config['designa_card_carrier_kind_id']['value'],
                                        'carrierString' => $ccd . $ecd . '-' . $car->plate_prefix . ' ' . $car->plate_number,
                                    ];
                                    $xml = $this->assignCardCarrier($assignRequest);
                                    if (!empty($xml)) {
                                        $xml = simplexml_load_string($xml);
                                        $xml->registerXPathNamespace('ns1', 'http://www.designa.de/');
                                        $xml = $xml->xpath('//ns1:assignCardCarrierResponse/ns1:assignCardCarrierResult');
                                        if (isset($xml[0])) {
                                            $xml = $xml[0];
                                            //$xml = $xml->xpath('//ns1:assignCardCarrierResponse/ns1:assignCardCarrierResult')[0];
                                            $json = json_encode($xml);
                                            $response = json_decode($json, true);
                                            if (isset($response[0])) {
                                                if ($response[0] == 0) {
                                                    $car->designa_acc_status = 1;
                                                }
                                            }
                                        }
                                    }
                                }//if designa assign card career not done
                                $car->save();
                            }
                        }//if designa_customer_id not null
                    } catch (\Exception $e) {
                        echo $e->getMessage();
                    }
                });
    }

}
