<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FitnessMonthlyTrack extends Model {

    use Uuids;

    protected $table = 'fitness_monthly_tracks';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $guarded = [];

}
