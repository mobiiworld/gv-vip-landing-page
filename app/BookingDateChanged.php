<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingDateChanged extends Model {

    use Uuids;

    protected $table = 'booking_date_changed';
    public $incrementing = false;
    protected $primaryKey = 'id';

    public function admin() {
        return $this->hasOne(Admin::class, 'id', 'admin_id');
    }

}
