<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleParea extends Model {

    protected $table = 'vehicle_parea';
    protected $primaryKey = 'id';

}
