<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingCartTransaction extends Model
{
    protected $connection = 'mysql2';

    protected $table = 'gv_shopcart_transaction';

    public function getSessionDetail()
    {
        return $this->hasOne(UserPaymentSessions::class, 'shopcartid', 'shopcartid')->orderBy('created_date','desc');
    }
    public function getCheckoutDetail()
    {
        return $this->hasOne(CheckoutTransaction::class, 'shopcartid', 'shopcartid');
    }
}
