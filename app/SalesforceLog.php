<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesforceLog extends Model
{
    use Uuids;
    protected $primaryKey = 'id';
    public $incrementing = false;
}
