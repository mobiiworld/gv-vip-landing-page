<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FitnessSeasonImages extends Model {


    protected $table = 'fitness_seasons_images';
    protected $primaryKey = 'id';
    public function getImageAttribute($value) {
        return !empty($value) ? cdn('admin/fitness_season/'. $value) : null;
    }


}
