<?php
return [
	'1_roxy' => [
		'title' => 'Roxy Cinemas',
		'first_title' => '',
		'content' => ' 
			<p>	خصم بنسبة 50% على تذاكر روكسي للفئة البلاتنية 
			</p>
			<p>	الخصم يتطبق على تذاكر روكسي للفئة البلاتنية فقط
			</p>
			<p>	للاستفادة من هذا الخصم، يجب تقديم الكود إلى موظفي سينما روكسي مع بطاقة القرية العالمية الفعلية أو الافتراضية
			</p>
			<p>	لا يمكن نقل التذاكر، أو بيعها ، أو استردادها، أو تبديلها
			</p>
			<p>	هذا الخصم غير ساري أثناء العطلات
			</p>
			<p>	لا يمكن استخدام هذا الخصم مع أي خصم آخر أو عرض ترويجي آخر
			</p>
			<p>	يسري العرض من الاثنين إلى الخميس فقط
			</p>
		',
	],
	'2_dpr' => [
		'title' => 'DPR',
		'first_title' => '',
		'content' => '
			<p>	يرجى تقديم رمز الاستجابة السريعة (QR Code) عند مدخل دبي باركس آند ريزورتس للدخول إلى الوجهة الترفيهية.
						</p>
				<p>	تذكرة دخول لوجهة واحدة صالحة في موشنجيت دبي، ليجولاند دبي، ليجولاند ووتر بارك، أو عالم ريال مدريد.
				</p>
				<p>	القسيمة صالحة للاستخدام مرة واحدة فقط.
				</p>
				<p>	لا يمكن استبدال القسائم بالنقود.
				</p>
				<p>	لا يمكن إعادة بيع القسائم.
				</p>
				<p>	يمكن استخدام القسائم حتى 31 أغسطس 2025 خلال ساعات العمل الرسمية.
				</p>
				<p>	القسيمة صالحة لعدد الأشخاص المذكور في القسيمة.
				</p>
				<p>	قم بتقديم تطبيق القرية العالمية الخاص بك للحصول على موقف سيارات VIP مجاني في ريفرلاند دبي.
				</p>
				<p>	يخضع جميع الضيوف الذين يدخلون الوجهة الترفيهية للقوانين واللوائح المعروضة عند المدخل الرئيسي وفي خدمات الزوار.
				</p>
				<p>	عدم الالتزام بهذه القواعد قد يؤدي إلى فقدان الضيف حقه في البقاء في الوجهة الترفيهية.
				</p>
				<p>	لمزيد من الشروط والأحكام، يرجى زيارة https://www.dubaiparksandresorts.com/en/terms-conditions 
				</p>
		',		
		
	],
	'3_ibaa' => [
		'title' => 'IBAA',
		'first_title' => '',
		'content' => '
				<h2>     *الشروط والأحكام     </h2>
				<p>	مكتبيتم الدخول إلى جولة داخل برج العرب عبر التذاكر مقابل فندق جميرا بيتش
				كود خصم جولة داخل برج العرب يتم استخدام هذا الكود فقط في صالة جولة داخل برج العرب  
				</p>
				<p>	كود الخصم صالح لتذكرة واحدة لجولة لشخص بالغ فقط 
				</p>
				<p>	كود الخصم صالح حتى 31 أغسطس 2025 فقط ، بدون أي تمديد 
				</p>
				<p>	لا يسري العرض خلال العطلات الرسمية أو المناسبات الخاصة ) ديسمبر
				2024 – يونيو 2025 ) – يعتمد توفر التذاكر على الأوقات المتاحة 
				
				</p>
				<p>	لا يسري هذا العرض بالتزامن مع العروض الاخرى.
				
				</p>
				<p>	التذاكر غير قابلة للإسترجاع - سيتم فرض رسوم على أي تغيير في التاريخ والوقت بقيمة 30 درهما إماراتيا لكل تذكرة
				</p>
				<p>
				يجب على الضيوف الوصول قبل 15 دقيقة من بدء جولتهم
				</p>
				<p>
				تنطبق تذاكر البالغين على الأفراد الذين تبلغ أعمارهم 13 عاماً أو أكثر
				</p>
				<p>
				تذاكر الأطفال(من عمر 4 سنوات إلى 12 سنة)
				</p>
				<p>
				يدخل الأطفال بعمر 3 أعوام مجاناً برفقة حامل تذكرة بالغ؛ ويتم إصدار التذاكر المجانية داخل برج العرب في لاونج التذاكر (قد يُطلب إثبات سن الطفل) 
				</p>
				<p>
				يجب أن يكون جميع الأطفال والمراهقين الذين تبلغ أعمارهم 16 عاماً أو أقل برفقة شخص بالغ (من سن 21 عامًا فأكثر)
				</p>
				<p>
				يجب على حاملي التذاكر ارتداء ملابس تحترم العادات والتقاليد المحلية
				</p>
				<p>
				لا يجوز التقاط أي صور في الصالة الرئيسية أو في الطابق الاول في فندق برج العرب 

				</p>
				<p>
				مواقف السيارات محددودة. وينصح بحاملي التذاكر بإستخدام وسائل النقل العام 
				</p>
				<p>
				هذه القسيمة غير قابلة للتحويل وغير قابلة للاسترداد ولا يمكن استبدالها نقدًا جزئيًا أو كليًا
				</p>
				<h2> قواعد اللباس  </h2>
				<p>
					يُرجى مُراعاة خصوصية وراحة ضيوف الفندق أثناء التواجد في الأماكن العامّة.
				</p>
				<p>	لا يُسمح باستخدام آلات التصوير بالوميض ."الفلاش" أو كاميرات التصوير الاحترافية
 
				</p>
				<p>	برج العرب هو عنوان ورمز للفخامة لذا يُرجى ارتداء ملابس أنيقة تُناسب المكان
				</p>
				<p>	ممنوع ارتداء ملابس واحذية الرياضة والشواطئ
				</p>
				<p>	إذا قمت بحجز تجربة عشاء بعد الجولة، يُرجى الرجوع إلى المطعم للحصول على .معلومات حول قواعد ارتداء الزيّ
				</p>

		',
	],
	'4_sea_subed' => [
		'title' => 'الشروط والأحكام',
		'first_title' => '',
		'content' => '
			<p>	قسيمة سرير التشمس في سي بريز أحد العروض المجانية المدرجة في باقات كبار الشخصيات البلاتينية والذهبية والفضية ("قسيمة التشمس").
			</p>
			<p>	قسائم سرير التشمس صالحة للفترة من 18 أكتوبر 2023 إلى 31 أغسطس 2023 ويمكن استخدامها في سي بريز - ذا بيتش
			</p>
			<p>	تشمل قسيمة سرير التشمس وحدة استحمام واحدة ومنشفة واحدة (تستلزم كل منشفة دفع وديعة نقدية قدرها 50 درهماً إماراتياً).
			</p>
			<p>	تتم خدمة الضيوف على أساس من يأتي أولاً يُخدم أولاً، رهنًا بتوفر أسرة التشمس.
			</p>
			<p>	على الضيف تقديم قسيمة سرير التشمس إلى موظف الشباك عند مدخل سي بريز - ذا بيتش.
			</p>
			<p>	لا تُستعمل قسيمة سرير التشمس لحجوزات المجموعة أو الشركات ولا يمكن استخدامها مع أي عرض ترويجي أو بطاقة خصم أو عرض أو قسيمة أخرى.
			</p>
			<p>	قيمة قسيمة سرير التشمس غير قابلة للاسترداد ولن تصدر القرية العالمية أو سي بريز بديل للتذاكر المفقودة أو المسروقة.
			</p>
			<p>	الضيوف مسؤولون عن أغراضهم الشخصية في جميع الأوقات. ولا تقبل القرية العالمية ولا سي بريز تحمل أي مسؤولية في حالة فقدان الأغراض الشخصية
			</p>
			<p>	يخضع الدخول لجميع الشروط والأحكام والقواعد واللوائح القياسية المعروضة في سي بريز - ذا بيتش
			</p>
			<p>	المأكولات والمشروبات في قسيمة التشمس، وسيكون لها رسومًا منفصلة.
			</p>
		'
	],
	'5_sea_cabana' => [
		'title' => 'الشروط والأحكام',
		'first_title' => '',
		'content' => '
			<p>	قسيمة كابانا سي بريز أحد العروض المجانية المدرجة في باقات كبار الشخصيات الخاصة والماسية.
			</p>
			<p>	قسيمة كابانا سي بريز صالحة للفترة من 18 أكتوبر 2023 إلى 31 أغسطس 2024 ويمكن استخدامها في سي بريز - ذا بيتش
			</p>
			<p>	يمكن لكل كابانا وكابانا كبار الشخصيات في سي بريز استضافة عدد 4 أشخاص بحد أقصى.
			</p>
			<p>	تشمل قسيمة كابانا سي بريز 4 وحدات استحمام و4 مناشف (تستلزم كل منشفة دفع وديعة نقدية قدرها 50 درهماً إماراتياً).
			</p>
			<p>	يتم خدمة الضيوف على أساس من يأتي أولاً يُخدم أولاً، رهنًا بتوفر المواقع.
			</p>
			<p>	يمكن حجز كابانا سي بريز مسبقًا بالاتصال على الرقم 0509456253 أو بإرسال رسالة إلكترونية إلى besnik@seabreeze.ae.
			</p>
			<p>	الحجوزات نهائية ولن يحق للضيوف إعادة استخدام قسيمة كابانا سي بريز المطبقة على الحجز في حالة عدم الحضور في تاريخ ووقت الحجز.
			</p>
			<p>	لن تُمدد أوقات الحجز في حالة تأخر وصول الضيوف.
			</p>
			<p>	يجب على الضيف تقديم قسيمة كابانا سي بريز إلى موظف الشباك عند مدخل سي بريز - ذا بيتش.
			</p>
			<p>	لا تُستعمل قسيمة كابانا سي بريز لحجوزات المجموعة أو الشركات ولا يمكن استخدامها مع أي عرض ترويجي أو بطاقة خصم أو عرض أو قسيمة أخرى.
			</p>
			<p>	قيمة قسيمة كابانا سي بريز غير قابلة للاسترداد ولن تصدر القرية العالمية أو سي بريز بديل للتذاكر المفقودة أو المسروقة.
			</p>
			<p>	الضيوف مسؤولون عن أغراضهم الشخصية في جميع الأوقات. ولا تقبل القرية العالمية ولا سي بريز تحمل أي مسؤولية في حالة فقدان الأغراض الشخصية.
			</p>
			<p>	يخضع الدخول لجميع الشروط والأحكام والقواعد واللوائح القياسية المعروضة في سي بريز - ذا بيتش
			</p>
			<p>	المأكولات والمشروبات غير مُدرجة في قسائم سي بريز وسيكون لها رسومًا منفصلة.
			</p>

		'
	],
	
];
