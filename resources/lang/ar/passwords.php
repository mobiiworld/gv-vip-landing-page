<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least eight characters and match the confirmation.',
    'reset' => 'Your password has been reset!',
    'sent' => 'We have e-mailed your password reset link!',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",
    'otp' => [
        'subject' => 'التحقق من حسابك مع كلمة المرور المعدة" ',
        'title' => 'لمة المرور المعدة للاستخدام لمرة واحدة"  ',
        'content' => 'ان الوقت للتحقق من حسابك! كلمة المرور الخاصة بك هي: :user_otp .<br/><br/>

                    يرجى عدم مشاركة هذا الرمز مع أي شخص آخر. إذا لم تكن قد تقدمت بطلب كلمة المرور المعدة للاستخدام لمرة واحدة OTP، يرجي تجاهل هذا البريد الإلكتروني.<br/><br/>

                    إذا كان لديك أي استفسار أو كنت في حاجة للمساعدة، يرجى مراسلتنا عبر البريد الإلكتروني: info@globalvillage.ae  أو الاتصال بنا على الرقم: +971 4 362 4114
                    ',
        'sent_failed' => 'لم يتم ارسال رمز التحقق  ',
        'sent_success' => 'ارسلنا لكم كلمة مرور لمرة واحدة على البريد الإلكتروني‎  ',
        'validate_success' => 'OTP validated successfully.',
        'validate_failed' => ' رمز التحقق الذي قمت بإدخاله خاطئ، الرجاء المحاولة مرة أخرى  ',
        'popup_title' => 'Validate OTP',
        'popup_input' => 'ارسلنا لكم بريد إلكتروني يتضمن رمز التفعيل. يرجى ادخاله هنا.',
        'popup_btn' => ' موافق  ',
        'unauthorized' => ' لم يتم التحقق من البريد الالكتروني، الرجاء المحاولة مرة أخرى  ',
    ],

];
