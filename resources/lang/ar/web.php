<?php

return [
	'email_mismatch_error' => "تأكيد البريد الإلكتروني غير مطابق‎",
	'captcha_error' => "يرجى التأكيد أنك لست برنامج روبوت‎",
	'designa_error_after_payment' => 'ar Payment is done Sucessfully and please contact customer care for number plate update.',
    'something_wrong' => 'هنالك خطأ ما!',
    'unauthorized' => 'غير مصرح.',
    'required_fields' => 'يرجى ملء كافة البيانات المطلوبة. تواجه مشكلة في تفعّيل الباقة؟ لا تقلق، راسلنا على: vip@globalvillage.ae',
    'any' => 'أي',
    'read_terms' => "يرجى قراءة وتأكيد الشروط والأحكام",
    'email_not_matched' => "البريد الإلكتروني غير متطابق",
    'pack' => [
        'car_type_placeholder' => "اختر نوع السيارة",
        'car_type_hatch_back' => "سيارة دفع رباعي",
        'car_type_sedan' => "سيارة سيدان",
		//		'mini' => 'الباقة المصغرة',
        'mini' => 'الباقة الفضية',
        'platinum' => 'الباقة البلاتينية',
        'gold' => 'الباقة الذهبية',
        'silver' => 'الباقة الفضية',
        'notfound' => "عذراً، لم نتمكن من العثور على بيانات الباقة التي أدخلتها. دعنا نساعدك في تفعيل الباقة! راسلنا على البريد الإلكتروني  vip@globalvillage.ae ",
        'used' => "يرجى العلم أن الباقة التي أدخلتها قد تم تفعيلها مسبقاً. اضغط على الرابط الذي أرسل إلى بريدك الإلكتروني المسجل لدينا.

هل ما زلت تريد المساعدة؟ راسلنا على: vip@globalvillage.ae",
        'nationality_placeholder' => "اختر الجنسية",
        'residence_placeholder' => "اختر بلد الإقامة",
        'car_country_placeholder' => "اختر الدولة",
        'car_emirate_placeholder' => "اختر الإمارة",
        'plate_category_placeholder' => "اختر فئة اللوحة",
        'plate_number_placeholder' => "تحديد",
        'no_more_car_allowed' => "لا يُسمح بمزيد من السيارات لهذه الباقة",
        'car_allowed' => "يمكن أن تقبل هذه الحزمة الحد الأقصى: عدد السيارات",
        'modal_head' => ' تم تفعيل باقة كبار الشخصيات  ',
        'congrats' => "تهانينا،",
        'congrats_1' => "ومرحبا بكم في عالم من الامتيازات!",
        'thank_msg_1' => "تم تفعيل",
        'thank_msg_2' => "الخاصة بك",
        'thank_msg_3' => "عيش تجربة القرية العالمية كنجم!",
        'platinum plus' => 'الباقة البلاتينية',
        //'mini pack' => 'Mini Pack',
        'mini pack' => 'الباقة الفضية',
        //'complementary' => 'Complementary',
        //'complementary' => 'الباقة الفضية',
        'complementary' => ' امتيازات خاصة لكبار الشخصيات  ',
        'complimentary' => ' امتيازات خاصة لكبار الشخصيات  ',
        //'complimentary' => 'الباقة الفضية',
        'private' => 'خاص   ',
        //'private' => 'الباقة الفضية',
        'diamond' => 'ماسي',
        'deleted' => "You have deleted the account from Global Village, so can't able to activate the pack",
    ],
    'pack_cat_for_popup' => [
        'platinum' => 'الباقة البلاتينية',
        'gold' => 'الباقة الذهبية',       
        'mini' => 'الباقة الفضية',
        'silver' => 'الباقة الفضية',
        'complementary' => ' امتيازات خاصة لكبار الشخصيات  ',     
        'complimentary' => ' امتيازات خاصة لكبار الشخصيات  ',      
        'private' => 'خاص   ',
        'diamond' => 'ماسي',
    ],
    'email_info' => 'إذا كان لديك بالفعل حساب معنا ، يرجى استخدام نفس عنوان البريد الإلكتروني لإكمال هذا النموذج.',
    'dob_error' => 'يجب ألا يقل العمر عن 18 سنة  ',
    'offer_links' => [
        'dubaiparksandresorts' => 'https://www.dubaiparksandresorts.com/ar/GVSeason26-VIPOffers',
        'dpr' => 'https://www.dubaiparksandresorts.com/ar/GVSeason26-VIPOffers',
        'dpr-allparks' => 'https://www.dubaiparksandresorts.com/ar/GVSeason26-VIPOffers',
        'bollywoodparksdubai' => 'https://www.dubaiparksandresorts.com/ar/GVSeason26-VIPOffers',
        'motiongatedubai' => 'https://www.dubaiparksandresorts.com/ar/GVSeason26-VIPOffers',
        'legolanddubai' => 'https://www.dubaiparksandresorts.com/ar/GVSeason26-VIPOffers',
        'legolandwaterpark' => 'https://www.dubaiparksandresorts.com/ar/GVSeason26-VIPOffers',
    ],
    'sales_pack_categories' => [
        'mini' => 'الباقة الفضية',
        'platinum' => 'الباقة البلاتينية',
        'gold' => 'الباقة الذهبية',
        'silver' => 'الباقة الفضية',
        'platinum plus' => 'الباقة البلاتينية',
        'mini pack' => 'الباقة الفضية',
        'complementary' => 'Complimentary',
        'complimentary' => 'Complimentary',
        'private' => 'Private',
        'diamond' => 'Diamond',
    ],
    'activation_success' =>
        "<p>تهانينا  :user_name على تفعيل باقة كبار الشخصيات   :pack_type ذات الرقم  :pack_number </p><p>
الآن أصبح بإمكانكم الاستمتاع بعالم أكثر روعة على طريقة كبار الشخصيات.   </p>"
    ,
];
