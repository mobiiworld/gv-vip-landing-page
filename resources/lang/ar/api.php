<?php

return [
    'something_wrong' => 'حدث خطأ ما!',
    'unauthorized' => 'Unauthorized.',
    'token' => [
        'expired' => 'Your session expired.Please Login.',
        'invalid' => 'Ivalid Token.',
        'blacklisted' => 'Token blacklisted',
        'notexist' => 'Authorization Token Required.'
    ],
    'required_fields' => 'Please fill all mandatory fields.',
    'registration_success' => 'Registration completed.',
    'invalid_credentials' => 'These credentials do not match our records.',
    'login_success' => 'Successfully logedin.',
    'no_data' => 'There is no data to display.',
    'data_updated' => 'Updated successfully.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'user' => [
        'exists' => 'This user already exists.',
        'mobile_exists' => 'This mobile number already exists.',
        'email_exists' => 'This email already exists.',
        'not_found'=>'User not found.',
        'car_not_found'=>'Car not found.',
        'invalid_car_country'=>'Invalid Country code.',
        'invalid_emirate'=>'Invalid Emirates code.'
    ],
    'slot_unavailable' => 'عذرًا، الطاولة التي تفضلها غير متوفرة في الوقت أو التاريخ المُحدد. الرجاء اختيار طاولة مختلفة أو تغيير التاريخ / الوقت.',
    'program_unavailable' => 'Program not available',
    'booking_timeout' => 'Booking Timeout',
    'success' => 'Success',
    'no_offfer' => 'No Offer',
    'already_done' => 'Already used',
    'fitness' => [
        'success' => 'لقد بدأت التحدي بنجاح.',
        'synced' => 'Steps successfully synced.',
        'season_expired' => 'Season ended.',
        'challenge_exist' => 'لقد شاركت بالتحدي اليوم!',
        'user_notfound' => 'There is no such user found.',
        'day_success' => 'تهانينا! لقد أكملت التحدي بنجاح. يرجى التحقق من البريد الإلكتروني الخاص بك لمعرفة تفاصيل الجائزة التي حصلت عليها.',
        'day_failed' => 'أحسنت! اقتربت جداً من تحقيق هدفك',
        'month_success' => 'You have completed your goal for the month.Please check your email for more details.',
        'day_month_success' => 'You have completed your goal for the day and for the month.Please check your email for more details.',
        'season_success' => 'You have completed your goal.Please check your email for more details.',
        'geofence' => 'يمكنك بدء أو إنهاء التحدي داخل القرية العالمية فقط.',
        'no_active_challenge' => 'لم تقم بعد بأي تحدي اليوم.',
    ],
    'cabana' => [
        'no_slots' => 'لا توجد كابانا متاحة في التاريخ الذي الذي تم اختياره. يرجى اختيار تاريخ آخر.',
        'not_started' => 'ستتوفر الكابانا من الساعة 6 عصراً فصاعداً.',
        'exceeds' => 'الساعات المطلوبة تتجاوز فترة تشغيل كابانا',
        'no_product' => 'Cabana for requested hours are not available.'
    ]
];
