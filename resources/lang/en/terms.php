<?php
return [
	'1_roxy' => [
		'title' => 'Roxy Cinemas',
		'first_title' => '',
		'content' => '<p>	50% off on ROXY Cinemas Platinum tickets</p>
						<p>	Discount is applicable for ROXY Cinemas Platinum tickets only. </p>
						<p>	To avail this discount, the code must be presented to ROXY Cinemas staff member at the Box Office along with the physical or virtual Global Village card</p>
						<p>	Tickets cannot be transferred, resold, refunded or exchanged</p>
						<p>	This discount is not valid during public holidays</p>
						<p>	This discount cannot be used in conjunction with any other discount or promotional offer</p>
						<p>	Offer is Valid From Monday to Thursday only</p>
		',		
		
	],
	'2_dpr' => [
		'title' => 'DPR',
		'first_title' => '',
		'content' => '
				<p>	Present the QR code at the entrance to Dubai Parks and Resorts to enter the park.
				</p>
				<p>	Single Park Entry Ticket valid at Motiongate Dubai, Legoland Dubai, Legoland Waterpark, or Real Madrid World.
				</p>
				<p>	Redemption is valid for one-time usage only.
				</p>
				<p>	Vouchers cannot be exchanged for cash.
				</p>
				<p>	The voucher cannot be resold.
				</p>
				<p>	The voucher can be redeemed until 31st August 2025 during normal hours of operation.
				</p>
				<p>	Redemption is valid for the number of people mentioned on the voucher.
				</p>
				<p>	Present your Global Village app to receive complimentary VIP parking at Riverland Dubai.
				</p>
				<p>	All guests entering the theme park(s) are subject to the rules and regulations of entry as displayed at the main entrance and within Guest Services. 
				</p>
				<p>	Failure to adhere to the rules of entry may lead to the visitor forfeiting their right to remain in the theme park.
				</p>
				<p>	For more terms and conditions, please visit https://www.dubaiparksandresorts.com/en/terms-conditions. 
				</p>
		',	
	],
	'3_ibaa' => [
		'title' => 'IBAA',
		'first_title' => '',
		'content' => '
			<h2>Terms and Conditions apply</h2>				 
			<p>	Entrance to the Inside Burj Al Arab Tour is via the Inside Burj Al Arab Ticketing Lounge next to Jumeirah Beach Hotel  
			</p>
			<p>	Promo Code for Inside Burj Al Arab Tour may only be redeemed at the Inside Burj Al Arab Ticket Lounge
			</p>
			<p>	Promo Code is valid for 1 Adult Tour Ticket only 
			</p>
			<p>	Promo Code is valid till 31 August 2025 only, without any extension 
			</p>
			<p>	Not applicable during Public Holidays, Blackout Dates (Dec 2024 and Jan 2025) special events – all tickets are subject to availability 
			</p>
			<p>	Not valid in conjunction with any other offers or promotions
			</p>
			<p>	Tickets are nonrefundable – date or time changes can be done a maximum of two times for a charge of AED 30 after which they become invalid  
			</p>
			<p>	Guests should arrive 15 minutes before the start of IBAA Tour
			</p>
			<p>	Adult tickets apply to individuals age 13+ 
			</p>
			<p>	Child tickets (for children aged 4 to 12)
			</p>
			<p>	Children 3 years old and under are free and should be accompanied by adult ticket-holder; complimentary tickets are issued at the Inside Burj Al Arab Ticketing Lounge (child\'s proof of age may be required) 
			</p>
			<p>	All children and teens age 16 and under must be accompanied by an adult (age 21+) 
			</p>
			<p>	Ticket-holders should dress in a manner that is respectful of local customs 
			</p>
			<p>	No photographs may be taken in the main Atrium or on the first floor of the Burj Al Arab 
			</p>
			<p>	Parking is limited and subject to availability.  Ticket Holders are advised to use Public Transport 
			</p>
			<p>	This voucher non-transferable, non-refundable and cannot be exchanged for cash in part or full  
			</p>
			<h2>Dress Code & Etiquette </h2>
			<p>Please respect the privacy and comfort of our in-house guests whilst in public spaces.</p>
			<p>	No flash photography or professional cameras are permitted. 
			</p>
			<p>	Burj Al Arab is the original home of luxury - Dress to impress! Smart/casual/elegant attire. Tailored shorts are allowed. No beachwear. No crocs or slippers.
			</p>
			<p>	If you have booked an onward dining experience, please consult individual restaurants for specific dress code information. </p>
 

		',	
	],
	'4_sea_subed' => [
		'title' => 'Sea Breeze Sunbed voucher',
		'first_title' => '',
		'content' => '
			<p>	Sea Breeze Sunbed voucher is one of the complimentary offers included in Platinum, Gold and Silver VIP Packs (“Sunbed Voucher”).
			</p>
			<p>	Sunbed Vouchers are valid from 16th October 2024 until 31st August 2025 and can be used in Sea Breeze - The Beach
			</p>
			<p>	Sunbed Vouchers include 1 shower token and 1 towel (each towel requires a cash deposit of AED 50).
			</p>
			<p>	Guests will be allocated at first come, first serve basis, subject to availability.
			</p>
			<p>	Guests must present the Sunbed Voucher to the cashier at the entrance of Sea Breeze- The Beach.
			</p>
			<p>	Sunbed Vouchers do not apply to group or corporate bookings and cannot be used in combination with any other promotion, discount card, offer or voucher.
			</p>
			<p>	Sunbed Voucher is not refundable and Global Village or Sea Breeze will not replace lost or stolen tickets.
			</p>
			<p>	Guests are responsible for their own personal belongings at all times. Neither Global Village or Sea Breeze accept responsibility in the event of lost personal belongings.
			</p>
			<p>	Admission is subject to all standard terms and conditions & rules and regulations displayed at Sea Breeze - The Beach
			</p>
			<p>	Food and beverage are not included in the Sunbed Voucher and will be charged separately.
			</p>
		'
	],
	'5_sea_cabana' => [
		'title' => 'Sea Breeze Cabana Voucher & Premium Cabana',
		'first_title' => '',
		'content' => '
			<p>	Sea Breeze Cabana Voucher is one of the complimentary offers included in Private and Diamond VIP Packs.
			</p>
			<p>	Sea Breeze Cabana Voucher are valid from 16th October 2024 until 31st August 2025 and can be used in Sea Breeze - The Beach
			</p>
			<p>	Each Sea Breeze Cabana and VIP Cabana can host a maximum 4 people.
			</p>
			<p>	Sea Breeze Cabana Voucher includes 4 shower tokens and 4 towels (each towel requires a cash deposit of AED 50).
			</p>
			<p>	Guests will be allocated on a first come, first serve basis subject to availability.
			</p>
			<p>	Sea Breeze Cabana may be booked in advance by calling 0509456253 or sending an email to besnik@seabreeze.ae.
			</p>
			<p>	Bookings are final and guests will not be entitled re-use the Sea Breeze Cabana Voucher applied to the reservation in case of a no-show on the date and time of the reservation
			</p>
			<p>	Reservations times will not be extended in case of late guest arrival
			</p>
			<p>	Guest must present the Sea Breeze Cabana Voucher to the cashier at the entrance of Sea Breeze- The Beach.
			</p>
			<p>	Sea Breeze Cabana Voucher does not apply to group or corporate bookings and cannot be used in combination with any other promotion, discount card, offer or voucher.
			</p>
			<p>	Sea Breeze Cabana Voucher is not refundable and Sea Breeze or Global Village will not replace lost or stolen tickets.
			</p>
			<p>	Guests are responsible for their own personal belongings at all times. Neither Global Village or Sea Breeze accept responsibility in the event of loss of personal belongings
			</p>
			<p>	Admission is subject to all standard terms and conditions & rules and regulations displayed at Sea Breeze - The Beach
			</p>
			<p>	Food and beverage are not included in the Sea Breeze Cabana Voucher and will be charged separately.
			</p>
		'
	],
	
];
