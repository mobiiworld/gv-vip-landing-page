<?php

return [
    'something_wrong' => 'Something went wrong!.',
    'unauthorized' => 'Unauthorized.',
    'token' => [
        'expired' => 'Your session expired.Please Login.',
        'invalid' => 'Ivalid Token.',
        'blacklisted' => 'Token blacklisted',
        'notexist' => 'Authorization Token Required.'
    ],
    'required_fields' => 'Please fill all mandatory fields.',
    'registration_success' => 'Registration completed.',
    'invalid_credentials' => 'These credentials do not match our records.',
    'login_success' => 'Successfully logedin.',
    'no_data' => 'There is no data to display.',
    'data_updated' => 'Updated successfully.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'user' => [
        'exists' => 'This user already exists.',
        'mobile_exists' => 'This mobile number already exists.',
        'email_exists' => 'This email already exists.',
        'not_found'=>'User not found.',
        'car_not_found'=>'Car not found.',
        'invalid_car_country'=>'Invalid Country code.',
        'invalid_emirate'=>'Invalid Emirates code.'
    ],
    'slot_unavailable' => 'Sorry, your table preference is not available at the time or date you have selected. Please select a different table or change the date/time.',
    'program_unavailable' => 'Program not available',
    'booking_timeout' => 'Booking Timeout',
    'success' => 'Success',
    'no_offfer' => 'No Offer',
    'already_done' => 'Already used',
    'fitness' => [
        'success' => 'You have successfully begun the challenge.',
        'synced' => 'Steps successfully synced.',
        'season_expired' => 'Season ended.',
        'challenge_exist' => 'You have already participated in today’s challenge.',
        'user_notfound' => 'There is no such user found.',
        'day_success' => 'Congratulations! You have completed the challenge. Please check your email for your prize details.',
        'day_failed' => 'Great work! You were almost there.',
        'month_success' => 'You have completed your goal for the month.Please check your email for more details.',
        'day_month_success' => 'You have completed your goal for the day and for the month.Please check your email for more details.',
        'season_success' => 'You have completed your goal.Please check your email for more details.',
        'geofence' => 'You can start or end the challenge only in the Global Village premises.',
        'no_active_challenge' => 'You have no active challenge for today.',
    ],
    'cabana' => [
        'no_slots' => 'All cabanas are booked for your selected date. Please select another date to continue.',
        'not_started' => 'Cabana will be available from 06 PM onwards.',
        'exceeds' => 'Your required hours exceed our cabana operation time.',
        'no_product' => 'Cabana for requested hours are not available.'
    ]
];
