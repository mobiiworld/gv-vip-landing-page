<?php

return [
    'dpr' => [
        'silver' => [
            'offer1' => 'Single park Entry Ticket 2',
            'offer2' => '30% discount on single park entry ticket - up to 4 pax',
            'offer3' => 'Real Madrid World Entry Ticket – 2',
            'offer4' => 'Neon Galaxy 1 hour pass - 2',
            'offer5' => ' Complimentary VIP parking at Riverland Dubai',
        ],
        'gold' => [
            'offer1' => 'Single park Entry Ticket 2',
            'offer2' => '30% discount on single park entry ticket - up to 4 pax',
            'offer3' => 'Real Madrid World Entry Ticket – 2',
            'offer4' => 'Neon Galaxy 1 hour pass - 2',
            'offer5' => ' Complimentary VIP parking at Riverland Dubai',
        ],
        'diamond' => [
            'offer1' => 'Single park Entry Ticket 4',
            'offer2' => '30% discount on single park entry ticket - up to 4 pax',
            'offer3' => 'Real Madrid World Entry Ticket – 4',
            'offer4' => 'Neon Galaxy 1 hour pass - 4',
            'offer5' => ' Complimentary VIP parking at Riverland Dubai',
        ],
        'platinum' => [
            'offer1' => 'Single park Entry Ticket 4',
            'offer2' => '30% discount on single park entry ticket - up to 4 pax',
            'offer3' => 'Real Madrid World Entry Ticket – 4',
            'offer4' => 'Neon Galaxy 1 hour pass - 4',
            'offer5' => ' Complimentary VIP parking at Riverland Dubai',
        ],
        'private' => [
            'offer1' => 'Single park Entry Ticket 6',
            'offer2' => '30% discount on single park entry ticket - up to 4 pax',
            'offer3' => 'Real Madrid World Entry Ticket – 6',
            'offer4' => 'Neon Galaxy 1 hour pass - 6',
            'offer5' => ' Complimentary VIP parking at Riverland Dubai',
        ],
    ],
   'ibaa' => [
        'offer1' => 'Inside Burj Al Arab Tour Tickets ',
        'private' => [
            'offer1' => 'Inside Burj Al Arab Tour Tickets - 6',
        ],
        'platinum' => [
            'offer1' => 'Inside Burj Al Arab Tour Tickets - 4',
        ],
        'gold' => [
            'offer1' => 'Inside Burj Al Arab Tour Tickets - 2',
        ],
   ],
   'roxy' => [
        'private' => [
            'offer1' => 'Platinum Entry Ticket - 50% Discount Voucher - 6',
        ],
        'platinum' => [
            'offer1' => 'Platinum Entry Ticket - 50% Discount Voucher - 4',
        ],
        'silver' => [
            'offer1' => 'Platinum Entry Ticket - 50% Discount Voucher - 2',
        ],
   ],
   'seabreeze' => [
        'silver' => [
            'offer1' => 'One sunbed voucher for 2 people',
        ],
        'gold' => [
            'offer1' => 'One sunbed voucher for 2 people',
        ],
        'diamond' => [
            'offer1' => 'one cabana voucher for 2 people ',
        ],
        'platinum' => [
            'offer1' => 'One sunbed voucher for 2 people',
        ],
        'private' => [
            'offer1' => 'One VIP cabana voucher for 2 people',
        ],
    ],

    'qr_text' => [
        'dpr' => 'Please present this code at Ticket Counter to avail offer',
        'ibaa' => 'Please present this code at IBAA Ticketing Lounge to avail offer.',
        'roxy' => 'Please present this code at counter to avail offer',
        'seabreeze' => [
            'private' => 'Please call <a href="tel:050-9456253"> 050-9456253 </a> to 
reserve your Cabana',
            'silver' => 'Please present this code at Sea Breeze counter to redeem your 
offer',
        ],
    ]
];
