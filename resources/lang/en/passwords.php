<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least eight characters and match the confirmation.',
    'reset' => 'Your password has been reset!',
    'sent' => 'We have e-mailed your password reset link!',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",
    'otp' => [
        'subject' => 'Verify your account with this OTP',
        'title' => 'Here’s your OTP',
        'content' => 'Time to verify your account! Your one time passcode is :user_otp .<br/><br/>

                    Please do not share this code with anyone. If you did not request for OTP, please ignore this email.<br/><br/>

                    We’re here to help if you have any questions, please email us at <a href="mailto:info@globalvillage.ae">info@globalvillage.ae</a> or call us on <a href="tel:+971 4 362 4114">+971 4 362 4114</a><br/><br/>',
        'sent_failed' => 'OTP sending failed',
        'sent_success' => 'OTP has been sent to your Email',
        'validate_success' => 'OTP validated successfully.',
        'validate_failed' => 'The OTP you entered is incorrect, Please try again.',
        'popup_title' => 'Validate OTP',
        'popup_input' => 'We have sent an OTP to your email. Enter it here to verify your email address',
        'popup_btn' => 'Enter',
        'unauthorized' => 'Your email is still pending validation. Please try again.',
    ],

];
