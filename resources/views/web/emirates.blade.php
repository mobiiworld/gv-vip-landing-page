<?php

if (!$emirates->isEmpty()) {
    $options = '<option value="">' . trans("web.pack.car_emirate_placeholder") . '</option>';
    foreach ($emirates as $emirate) {
        $options .= '<option value="' . $emirate->id . '">' . $emirate->name . '</option>';
    }
} else {
    //$options = '<option value="">' . trans("web.any") . '</option>';
    $options = '';
}
echo $options;
?>