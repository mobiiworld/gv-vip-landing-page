<link rel="icon" type="image/png" href="{{asset('web/images/favicon.ico')}}" sizes="16x16">
<link rel="stylesheet" type="text/css" href="{{asset('web/css/main.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('web/css/custom.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('web/css/fonts.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('web/css/style.css?ver=2')}}">