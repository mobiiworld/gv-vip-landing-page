<footer>
	<div class="footer-top">
		<div class="container">
			<div class="footer-top-content">
				<div class="opening-hours">
					<h6><img src="{{asset('web/images/time.svg')}}" alt="" /> <b>{{trans('content.footer_season')}}</b></h6>
					<div>
						<p class="mb-1 mt-1"><span>{{trans('content.footer_season_time')}}</span> </p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6 col-12">
                    <div class="footer-logo">
                        <a href="https://www.globalvillage.ae/en">
							<img src="{{asset('web/images/footer-logo.svg')}}" alt="Global Village">
                        </a>
                    </div>
				</div>
				<div class="col-lg-3 col-md-6 col-12">
					<div class="quick-links">
						<h5 class="footer-title">{{trans('content.corporate')}}</h5>    
						<ul class="clearfix">
							<li class="menu-item">
								<a href="https://www.globalvillage.ae/{{app()->getLocale()}}/about-us" data-drupal-link-system-path="node/30">{{trans('content.about_us')}}</a>
							</li>
							<li class="menu-item">
								<a href="https://gv.force.com/PRM" title="Partner Portal Access" target="_blank" rel="noopener">{{trans('content.partner_portel_access')}}</a>
							</li>
							{{-- <li class="menu-item">
								<a href="https://schools.globalvillage.ae/{{app()->getLocale()}}" title="Global Village Schools Site" target="_blank" rel="noopener">{{trans('content.for_school')}}</a>
							</li> --}}
							<li class="menu-item">
								<a href="https://business.globalvillage.ae/{{app()->getLocale()}}" target="_blank" rel="noopener">{{trans('content.for_business')}}</a>
							</li>
							<li class="menu-item">
								<a href="https://www.globalvillage.ae/{{app()->getLocale()}}/travel-trade" data-drupal-link-system-path="node/8552">{{trans('content.for_travel_trade')}}</a>
							</li>
						</ul>
					</div>
				</div>
				
				<div class="col-lg-3 col-md-6 col-12">
					<div class="contact-us">
						<div class="region region-footer-third">
							<h5 class="footer-title">{{trans('content.contact_us')}}</h5>
							
							{!! trans('content.share_your_experince_text') !!}
							<p><a class="btn" href="https://www.globalvillage.ae/{{app()->getLocale()}}/contact-us">{{trans('content.feedback_form')}}</a></p>
						</div>
					</div>
				</div>
			
				<div class="col-lg-3 col-md-6 col-12">
					<div class="social">
						<h5 class="footer-title">{{trans('content.connect')}}</h5>
						<ul>
							<li><a href="https://www.facebook.com/GlobalVillageAE/" target="_blank"><i class="icon-facebook"></i></a></li>
							<li><a href="https://twitter.com/GlobalVillageAE" target="_blank">
								<svg width="15" height="15" viewBox="0 0 1200 1227" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M714.163 519.284L1160.89 0H1055.03L667.137 450.887L357.328 0H0L468.492 681.821L0 1226.37H105.866L515.491 750.218L842.672 1226.37H1200L714.137 519.284H714.163ZM569.165 687.828L521.697 619.934L144.011 79.6944H306.615L611.412 515.685L658.88 583.579L1055.08 1150.3H892.476L569.165 687.854V687.828Z" fill="white"></path></svg>
							</a></li>
							<li><a href="http://instagram.com/globalvillageuae" target="_blank"><i class="icon-instagram"></i></a></li>
							<li><a href="https://www.linkedin.com/company/global-village-dubai/" target="_blank"><i class="icon-linkedin"></i></a></li>
							<li><a href="https://www.youtube.com/user/GlobalVillageAE" target="_blank"><i class="icon-youtube"></i></a></li>
							<li><a href="https://www.tiktok.com/@globalvillageae" target="_blank"><i class="icon-tiktok"></i></a></li>
							<li><a href="https://www.snapchat.com/add/globalvillageme" target="_blank"><i class="icon-sanpchat"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-12">
                    <div class="footer-links">
                          	<div class="region region-copyright">
								<ul class="clearfix">
									@if (app()->getLocale() == 'en')
										<li class="menu-item">
											<a
												href="https://privacy.dubaiholding.com/en/privacy-notice/customers---dubai-holding-entertainment-llc">{{ trans('content.privacy_policy') }}</a>
										</li>
									@else
										<li class="menu-item">
											<a
												href="https://privacy.dubaiholding.com/ar/privacy-notice/customers---dubai-holding-entertainment-llc">{{ trans('content.privacy_policy') }}</a>
										</li>
									@endif
									<li class="menu-item">
										<a
											href="https://www.globalvillage.ae/{{ app()->getLocale() }}/terms-use">{{ trans('content.terms_of_use') }}</a>
									</li>
								</ul>
	  						</div>

	                    </div>
	                </div>
			</div>
		</div>
	</div>
</footer>
