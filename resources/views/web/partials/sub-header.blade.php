<header>
    <div class="container">
        <div class="header-items">
            <?php
            $headerLogoUrl =  url(app()->getLocale());
            ?>
            @if(app()->getLocale() == 'en')
            <a href="{{$headerLogoUrl}}" class="gv-logo"><img src="{{asset('web/images/logo.svg')}}" alt="Global Village" /></a>
            @else
                <a href="{{$headerLogoUrl}}" class="gv-logo"><img src="{{asset('web/images/logo-arabic.svg')}}" alt="Global Village" /></a>
            @endif 
            <h5>{{trans('content.header-text1')}} <span>{{trans('content.header-text2')}}</span></h5>
            <?php
            $route = Route::getCurrentRoute();
            $params = array();
            $routeName = 'home';
            if(isset($route->parameters)){
                $routeParameters = $route->parameters;
                $shifted = array_shift($routeParameters);
                $routeName = $route->getName();
                $params = array_values($routeParameters);
            }
            ?>
            @if(app()->getLocale() == 'en')
            <a href="{{ route($routeName, array_merge(['ar'], $params)) }}">العربية</a>
            @else
            <a href="{{ route($routeName, array_merge(['en'], $params)) }}">English</a>
            @endif 
        </div>
    </div>
</header>
