<!doctype html>
<html lang="{{app()->getLocale()}}">
    <head>
        <title>{{env('APP_NAME', 'Global Village')}}</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        @include('web.partials.header')
        @yield('css')

        @if(!empty($completionPage))
            <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '582107468989322');
            fbq('track', 'CompleteRegistration');
            </script>
            <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=582107468989322&ev=PageView&noscript=1"
            /></noscript>
        @else
            <script>
              !function(f,b,e,v,n,t,s)
              {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
              n.callMethod.apply(n,arguments):n.queue.push(arguments)};
              if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
              n.queue=[];t=b.createElement(e);t.async=!0;
              t.src=v;s=b.getElementsByTagName(e)[0];
              s.parentNode.insertBefore(t,s)}(window, document,'script',
              'https://connect.facebook.net/en_US/fbevents.js');
              fbq('init', '582107468989322');
              fbq('track', 'PageView');
              </script>
              <noscript><img height="1" width="1" style="display:none"
              src="https://www.facebook.com/tr?id=582107468989322&ev=PageView&noscript=1"
              /></noscript>
        @endif

       @if(env('APP_ENV') == 'production')

        <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-M5324B6');</script>
            <!-- End Google Tag Manager -->
            
        <script>
        window.dataLayer = window.dataLayer || [];
        </script>
         @endif

    </head>
    <!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-56446414-1"></script>

<script>

  window.dataLayer = window.dataLayer || [];

  function gtag(){dataLayer.push(arguments);}

  gtag('js', new Date());



  gtag('config', 'UA-56446414-1');

</script>
    <body>
    @if(env('APP_ENV') == 'production')
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M5324B6"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
     @endif

        @if(request()->source==null)
        @include('web.partials.sub-header')
        @endif
        @yield('content')
        @if(request()->source==null)
        @include('web.partials.footer')
        @endif
        <script type="text/javascript">
            var _elqQ = _elqQ || [];
            // _elqQ.push(['elqSetSiteId', '681364326']);
            // _elqQ.push(['elqTrackPageView']);
            // (function () {
            //     function async_load() {
            //         var s = document.createElement('script');
            //         s.type = 'text/javascript';
            //         s.async = true;
            //         s.src = '//img06.en25.com/i/elqCfg.min.js';
            //         var x = document.getElementsByTagName('script')[0];
            //         x.parentNode.insertBefore(s, x);
            //     }
            //     if (window.addEventListener)
            //         window.addEventListener('DOMContentLoaded', async_load, false);
            //     else if (window.attachEvent)
            //         window.attachEvent('onload', async_load);
            // })();
          var copiedText = "{{trans('content.copied')}}";
        </script>


        <script type="text/javascript" src="{{asset('web/js/main.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/sweetalert.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('web/js/custom.js?ver=1.001')}}"></script>
		<script src="https://www.google.com/recaptcha/api.js?hl={{app()->getLocale()}}" async defer></script>
        @yield('js')
    </body>
</html>
