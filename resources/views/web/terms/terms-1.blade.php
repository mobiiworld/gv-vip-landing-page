<div class="modal-body">
	<div class="modal-body-title">
		<h4 id="modalOfferName">{{trans('terms.1_roxy.title')}}</h4>
	</div>
	<div class="row justify-content-center">
		<div class="col-lg-11">
			<div class="terms-content">
				<h6>{{trans('terms.1_roxy.first_title')}}</h6>
				{!!  trans('terms.1_roxy.content')  !!}
				
			</div> 
		</div>
	</div>
</div>
