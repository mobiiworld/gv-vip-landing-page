<div class="modal-body">
	<div class="modal-body-title">
		<h4 id="modalOfferName">{{trans('terms.16_ibaa.title')}}</h4>
	</div>
	<div class="row justify-content-center">
		<div class="col-lg-11">
			<div class="terms-content">
				{!!  trans('terms.16_ibaa.content')  !!}
				
			</div> 
		</div>
	</div>
</div>
