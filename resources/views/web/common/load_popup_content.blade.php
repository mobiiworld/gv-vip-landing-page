<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" >@if($result['extra_action'] == "add") {{trans('content.add-car-detail')}} @else {{trans('content.update-car-detail')}} @endif</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            @if($result['is_adding_car'] == 1)
            <form id="carForm" class="carForm">
                <div class="row">
                
                   <div class="col-lg-12 col-md-12 col-12">
                    <div class="form-group">
                    {{--    <label>{{trans('content.car-type')}}</label>  --}}
                        <div class="car-selection">
                            <div class="position-relative">
                                <input type="radio" id="suv" name="type" value="1" @if( !empty($result['action_id']) && $result['user_car']->type == 1 || empty($result['action_id'])) checked="checked" @endif class="cartype_class suv">
                                <label for="suv">
                                    <img src="{{asset('web/images/suv.svg')}}" alt="">
                                    <span>{{trans('content.suv')}}</span>
                                </label>
                            </div>
                            <div class="position-relative">
                                <input type="radio" id="sudan" name="type" value="2"  @if( !empty($result['action_id']) && $result['user_car']->type == 2) checked="checked" @endif  class="cartype_class sudan">
                                <label for="sudan">
                                    <img src="{{asset('web/images/sedan.svg')}}" alt="">
                                    <span>{{trans('content.sedan')}}</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
              <div class="row">
                  <input type="hidden" name="user_car_id" value="{{$result['action_id']}}">
                  <input type="hidden" name="user_id" value="{{$result['extra_id']}}">
                  <input type="hidden" name="pack_id" value="{{$result['pack_id']}}">
                  <div class="col-lg-6 col-md-6 col-12">
                    <div class="form-group">
                        <label>{{trans('content.contry-car-register')}}<span>*</span></label>
                        <select class="form-control custom-select carCountry" id="car_country" name="car_country">
                            <option value="">{{trans('web.pack.car_country_placeholder')}}</option>
                            @foreach($result['car_countries'] as $ccountry)
                                <option value="{{$ccountry->id}}" @if(!empty($result['action_id']) && $result['user_car']->country_id == $ccountry->id) selected @endif>{{$ccountry->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                  <div class="col-lg-6 col-md-6 col-12" id="emirate_pop_div" @if(!empty($result['action_id']) && !empty($result['user_car']->emirate_id )) style="display:block;" @else style="display:none;" @endif>
                    <div class="form-group">
                        <label>{{trans('content.emirates-car-register')}}<span>*</span></label>
                        <select class="form-control custom-select carEmirate" id="emirate" name="emirate">
                            <option value="">{{trans('web.pack.car_emirate_placeholder')}}</option>
                            @if(!empty($result['action_id']))
                                @if(sizeof($result['emirate'])>0)
                                    @foreach($result['emirate'] as $emirate)
                                        <option value="{{$emirate->id}}" @if(!empty($result['action_id']) && $result['user_car']->emirate_id == $emirate->id) selected @endif >{{$emirate->name}}</option>
                                    @endforeach
                                @else
                                    <option value="" selected>{{trans("web.any")}}</option>
                                @endif
                            @endif
                        </select>
                    </div>
                </div>
                  <div class="col-lg-6 col-md-6 col-12">
                    <div class="form-group">
                        <label>{{trans('content.plate-code')}}<span>*</span></label>
                        <select class="form-control custom-select carNumber" name="plate_prefix">
                            <option value="">{{trans('web.pack.plate_number_placeholder')}}</option>
                            @if(!empty($result['action_id']))
                                @foreach($result['plate_prefixes'] as $k=>$v)
                                        <option value="{{$v}}" @if(!empty($result['action_id']) && $result['user_car']->plate_prefix == $v) selected @endif >{{$v}}</option>
                                @endforeach

                            @endif
                        </select>
                    </div>
                </div>
                  <div class="col-lg-6 col-md-6 col-12">
                    <div class="form-group">
                        <label>{{trans('content.plate-number')}}<span>*</span></label>
                        <input type="text" class="form-control" value="@if(isset($result['user_car'])){{$result['user_car']->plate_number}}@endif" placeholder="{{trans('content.enter-plate-number')}}" name="plate_number" oninput="platenumberOnly(this.id);" maxlength="10" id="plate_number" />
                    </div>
                </div>
               </div>
             </form>
                <div class="second-step-error" style="display: none;"></div>
                <div class="cm-success-message" style="display: none;"></div>
                @else
                <span>Car activation limit is over now</span>
            @endif
            
            @if($result['is_adding_car'] == 1)
            <button type="submit" class="btn btn-theme-bordered submit_car_detail" >{{trans('content.submit')}}</button>
            @endif

        </div>
    </div>

</div>