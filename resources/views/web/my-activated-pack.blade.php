@extends('web.partials.master')
@section('content')
<section class="inner-banner">
    <div class="container">
        <h1>{{trans('content.banner-text2')}}</h1>
    </div>
</section>
<section class="vip-pack gold-pack">
   
    <div class="vip-pack-content">
        <div class="card">
            @if($is_activate_pack)
            <div class="card-header"><span>{{trans("web.pack.".$pack->category)}}</span> <a href="{{route('home',app()->getLocale())}}" class="btn btn-theme-bordered" target="_blank">{{trans('content.activate-another-pack')}}</a></div>
            @endif
            <div class="card-body mt-0 p-0">
                <div class="gold-pack-content">
                    <ul>
                        <li class="multi-column">
                            <div class="left-side">
                                <label class="primary-label"><b>{{trans('content.pack-number')}}</b></label>
                            </div>
                            <div class="right-side">
                                <span>{{$pack->pack_number}}</span> 
                            </div>
                        </li>
                        @if($activeSeason == 'yes')

                        <?php
                        $unUsedParking = $usageDetails['parkingUnused']; //$pack->park_entry_count
                        $unUsedEntry = $usageDetails['entryUnused']; //$pack->parking_voucher_count

                        $wonderpassClass = config('globalconstants.pack_categories_wonderpass_class');
                        $wpCalss = (isset($wonderpassClass[$pack->category])) ? $wonderpassClass[$pack->category] : 'general';
                        ?>
                        <li>
                            <h6><b>{{trans('content.in_your_pack')}}</b></h6>
                            
                            <div class="my-tickets">
                                @if(isset($usageDetails['totalEntry']) && $usageDetails['totalEntry'] > 0 )
                                <div class="ticket-count">
                                    <div id="entrycircle"></div>
                                    <h5>{{$unUsedEntry}} <small>{{trans('content.ticket')}}</small></h5>
                                    <p class="ticket-title">{{trans('content.park-entry')}}</p>
                                    <p class="ticket-desc">{{trans('content.entry_subdesc')}}</p>
                                    <!-- <span>{{trans('content.park-entry')}}</span> -->
                                </div>
                                @endif
                                @if(isset($usageDetails['totalParking']) && $usageDetails['totalParking'] > 0 )
                                <div class="ticket-count">
                                    
                                    <div id="parkingcircle"></div>
                                    <h5>
                                        
                                        {{$unUsedParking}} 
                                       
                                        <small>{{trans('content.parking')}}</small></h5>
                                    <p class="ticket-title">{{trans('content.vip-parking')}}</p>
                                    <!-- <span>{{trans('content.park-entry')}}</span> -->
                                    <p class="ticket-desc">{{trans('content.parking_subdesc')}}</p>
                                </div>
                                @endif
                                <?php /* ?>
                                @if(isset($usageDetails['total_majlis_tables']) && $usageDetails['total_majlis_tables'] > 0)
                                <div class="ticket-count">
                                    
                                    <div id="tablecircle"></div>
                                    <h5>{{$usageDetails['remaining_tables']}} <small>{{trans('content.table')}}</small></h5>
                                    <p class="ticket-title">{{trans('content.majlis-table')}}</p>
                                    <p class="ticket-desc">{{trans('content.majlis_subdesc')}}</p>
                                    <!-- <span>{{trans('content.majlis-table')}}</span> -->
                                </div>
                                @endif
                                
                                @if($pack->cabana_remaining_count > 0)
                                    <div class="ticket-count">
                                    
                                        <div id="cabanacircle"></div>
                                        <h5>{!! trans("content.cabana_tile.".$pack->category, ['rCount' => $pack->cabana_remaining_count, 'cabana_discount_percentage' => $pack->cabana_discount_percentage]) !!} </h5>
                                        
                                        <p class="ticket-title">{{trans('content.cabana_tile.title')}}</p>
                                        <p class="ticket-desc">{{trans('content.cabana_tile.description')}}</p>
                                       
                                    </div>
                                @endif
                                <?php */ ?>
                               @if($pack->inpark_taxi_voucher > 0)
                                <div class="ticket-count">
                                   <!--  <div id="inparkcircle"></div> -->
                                    <h5 class="ticket-circle">
                                    <span>{{$pack->inpark_taxi_voucher}}</span> 
                                    <small>
                                   <?php /* ?>  {{trans('content.inpark.circle_content')}}<? */ ?>
                                    </small>
                                </h5>
                                    <p class="ticket-title">{{trans('content.inpark.title')}}</p>   
                                    <p class="ticket-desc">{{trans('content.inpark.description')}}</p>                                    
                                </div>
                                @endif
                                @if($pack->carwash_services_voucher > 0)
                                <div class="ticket-count">
                                    <!-- <div id="carwashServicescircle"></div> -->
                                    <h5 class="ticket-circle">
                                    <span>{{$pack->carwash_services_voucher}}</span> 
                                    <small>
                                       <?php /* ?>  {{trans('content.carwash_services.circle_content')}}<? */ ?>
                                    </small>
                                </h5>
                                    <p class="ticket-title">{{trans('content.carwash_services.title')}}</p>   
                                    <p class="ticket-desc">{{trans('content.carwash_services.description')}}</p>                                    
                                </div>
                                @endif
                                @if($pack->porter_services_voucher > 0)
                                <div class="ticket-count">
                                    <!-- <div id="porterServicescircle"></div> -->
                                    <h5 class="ticket-circle">
                                    <span>{{$pack->porter_services_voucher}} </span>
                                        <small>
                                          <?php /* ?>   {{trans('content.porter_services.circle_content')}}<? */ ?>
                                        </small>
                                    </h5>
                                    <p class="ticket-title">{{trans('content.porter_services.title')}}</p>   
                                    <p class="ticket-desc">{{trans('content.porter_services.description')}}</p>                                    
                                </div>
                                @endif
                                @if($pack->stunt_show > 0)
                                <div class="ticket-count">
                                    <!-- <div id="stuntshowcircle"></div> -->
                                    <h5 class="ticket-circle">

                                    <span>{{$pack->stunt_show}} </span>
                                        <small>
                                        <?php /* ?> {{trans('content.stunt_show.circle_content')}} <? */ ?>
                                        </small>
                                    </h5>

                                    <p class="ticket-title">{{trans('content.stunt_show.title')}}</p>    
                                    <p class="ticket-desc">{{trans('content.stunt_show.description')}}</p>                               
                                </div>
                               @endif
                            </div>
                        </li>
                        @endif
                        @if(count($cards) > 0)
                        @php ($i = 1)
                        <li>
                            <label class="primary-label"><b>{{trans('content.wonderpass')}}</b></label>
                            <div class="my-wonderpass">
                                @foreach($cards as $card)
                                <div class="wonderpass-count">
                                    <div class="wonderpass-title">{{trans('content.card')}} {{$i}}</div>
                                    <div class="card-details {{$wpCalss}} ">
                                        <div class="top">
                                            <h5>{{$card->card_number}}</h5>
                                        </div>
                                        <div class="middle">
                                            <h6>{{trans('content.balance')}}</h6>
                                            <div>
                                                <div class="list">
                                                    <label>{{trans('content.multi-use')}}</label>
                                                    <h5>{{$card->balance_points}} <small>{{trans('content.points')}}</small></h5>
                                                </div>
                                                <div class="list">
                                                    <label>{{trans('content.carnavel_bonus')}}</label>
                                                    <h5>{{$card->carnavalBal}} <small>{{trans('content.points')}}</small></h5>
                                                </div>
                                                <div class="list">
                                                    <label>{{trans('content.ripleys')}}</label>
                                                    <h5>
                                                        {{$card->ripleys_count}} 
                                                        <small>{{trans('content.entries')}}</small>
                                                        @if($card->ripleys_count > 0)
                                                        <a href="javascript:;">
                                                            <i class="icon-info" data-toggle="tooltip"  title="{{$card->popoverText}}" ></i>
                                                        </a>
                                                        @endif
                                                    </h5>
                                                </div>
                                            </div>
                                        </div>
                                        @if($is_activate_pack)
                                        <div class="bottom">
                                            <a href="{{env('E_API_URL')}}/{{app()->getLocale()}}/buy?amc={{$card->card_number}}" target="_blank">{{trans('content.topup')}}</a>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                @php ($i++)
                                @endforeach
                            </div>
                        </li>
                        @endif
                        @if(count($partnerOffers) > 0)
                        <?php 
                            $partner_logos = partner_logos();
                        ?>
                        <li >
                            <label class="primary-label"><b>{{trans('content.partner-offer')}}</b></label>
                                 <div class="slider two-thumbnails">
                                       
                                    @foreach($partnerOffers as $key=>$offers)
                                    <?php 
                                        $logo = (isset($partner_logos[$key])) ? $partner_logos[$key] : '';
                                        
                                    ?>
                                    @if($key == 'seabreeze')
                                    <div class="complementary-offer meraas">
                                        <div class="complementary-offer-content" data-offername="{{$offers->merchant_name_en}}">
                                            <img class="offer-logo" src="{{$logo}}" alt="{{$offers->merchant_name_en}}"  @if($offers->open_status == 0) data-toggle="tooltip" title="{{$offers->closed_message}}" @endif />
                                            <h6 class="offer-name"></h6>
                                            @if($pack->category == 'gold')
                                                <p>{{trans('offers.seabreeze.gold.offer1')}}</p>                                             
                                            @elseif($pack->category == 'private')
                                                <p>{{trans('offers.seabreeze.private.offer1')}}</p>
                                            @elseif( $pack->category == 'diamond')
                                                <p>{{trans('offers.seabreeze.diamond.offer1')}}</p>
                                            @elseif($pack->category == 'platinum')
                                                <p>{{trans('offers.seabreeze.platinum.offer1')}}</p>
                                            @elseif($pack->category == 'silver' || $pack->category == 'complimentary')
                                                <p>{{trans('offers.seabreeze.silver.offer1')}}</p>
                                               
                                            @endif
                                        </div>
                                    </div>
                                    @endif

                                    @if(($key == 'insideburjalarab' || $key == 'ibaa') && $pack->category != 'silver')
                                    <div class="complementary-offer meraas">
                                        <div class="complementary-offer-content" data-offername="{{$offers->merchant_name_en}}">
                                            <img class="offer-logo" src="{{$logo}}" alt="{{$offers->merchant_name_en}}"  @if($offers->open_status == 0) data-toggle="tooltip" title="{{$offers->closed_message}}" @endif />
                                            <h6 class="offer-name"></h6>                                           
                                               
                                            @if($pack->category == 'private'  )
                                            <p>{{trans('offers.ibaa.private.offer1')}}</p>
                                           
                                           @elseif($pack->category == 'platinum' || $pack->category == 'diamond')
                                           <p>{{trans('offers.ibaa.platinum.offer1')}}</p>
                                           @elseif( $pack->category == 'gold'  || $pack->category == 'complimentary')
                                           <p>{{trans('offers.ibaa.gold.offer1')}}</p>
                                           
                                           @endif
                                        </div>
                                    </div>
                                    @endif

                                  
                                   @if($key == 'roxycinemas' || $key == 'roxy')
                                    <div class="complementary-offer meraas">
                                        <div class="complementary-offer-content" data-offername="{{$offers->merchant_name_en}}">
                                            <img class="offer-logo" src="{{$logo}}" alt="{{$offers->merchant_name_en}}"  @if($offers->open_status == 0) data-toggle="tooltip" title="{{$offers->closed_message}}" @endif  />
                                            <h6 class="offer-name"></h6>
                                            @if($pack->category == 'private'  )
                                            <p>{{trans('offers.roxy.private.offer1')}}</p>
                                           
                                           @elseif($pack->category == 'platinum' || $pack->category == 'diamond')
                                           <p>{{trans('offers.roxy.platinum.offer1')}}</p>
                                           @elseif( $pack->category == 'gold' || $pack->category == 'silver' || $pack->category == 'complimentary')
                                           <p>{{trans('offers.roxy.silver.offer1')}}</p>
                                           
                                           @endif
                                        </div>
                                    </div>
                                    @endif
                                   
                                   
                                    @if($key == 'dpr-allparks' || $key == 'dubaiparksandresorts' || $key == 'dpr')
                                    <div class="complementary-offer drb">
                                        <div class="complementary-offer-content" data-offername="{{$offers->merchant_name_en}}">
                                            <img class="offer-logo" src="{{$logo}}" alt="{{$offers->merchant_name_en}}"  @if($offers->open_status == 0) data-toggle="tooltip" title="{{($offers->closed_message != '') ? $offers->closed_message : trans('content.dpr_all_park-alert')}}" @endif />
                                            <h6 class="offer-name"></h6>
                                             @if($pack->category == 'private' )
                                            <p>{{trans('offers.dpr.private.offer1')}}</p>
                                            <p>{{trans('offers.dpr.private.offer2')}}</p>
                                            <p>{{trans('offers.dpr.private.offer3')}}</p>
                                            <p>{{trans('offers.dpr.private.offer4')}}</p>
                                            <p>{{trans('offers.dpr.private.offer5')}}</p>
                                           @elseif($pack->category == 'platinum')
                                           <p>{{trans('offers.dpr.platinum.offer1')}}</p>
                                            <p>{{trans('offers.dpr.platinum.offer2')}}</p>
                                            <p>{{trans('offers.dpr.platinum.offer3')}}</p>
                                            <p>{{trans('offers.dpr.platinum.offer4')}}</p>
                                            <p>{{trans('offers.dpr.platinum.offer5')}}</p>
                                            @elseif($pack->category == 'diamond')
                                           <p>{{trans('offers.dpr.diamond.offer1')}}</p>
                                            <p>{{trans('offers.dpr.diamond.offer2')}}</p>
                                            <p>{{trans('offers.dpr.diamond.offer3')}}</p>
                                            <p>{{trans('offers.dpr.diamond.offer4')}}</p>
                                            <p>{{trans('offers.dpr.diamond.offer5')}}</p>
                                            @elseif($pack->category == 'gold')
                                           <p>{{trans('offers.dpr.gold.offer1')}}</p>
                                            <p>{{trans('offers.dpr.gold.offer2')}}</p>
                                            <p>{{trans('offers.dpr.gold.offer3')}}</p>
                                            <p>{{trans('offers.dpr.gold.offer4')}}</p>
                                            <p>{{trans('offers.dpr.gold.offer5')}}</p>
                                            @elseif( $pack->category == 'silver' || $pack->category == 'complimentary')
                                           <p>{{trans('offers.dpr.silver.offer1')}}</p>
                                            <p>{{trans('offers.dpr.silver.offer2')}}</p>
                                            <p>{{trans('offers.dpr.silver.offer3')}}</p>
                                            <p>{{trans('offers.dpr.silver.offer4')}}</p>
                                            <p>{{trans('offers.dpr.silver.offer5')}}</p>
                                           @endif
                                        </div>
                                    </div>
                                    @endif
                                  
                                    @endforeach
                                
                                </div>
                        </li>
                        @endif
                         @if(env('SHOW_REGISTERED_CAR') == TRUE)
                         <?php $field = 'name_'.app()->getLocale(); ?>
                         <li class="multi-column">
                            <label class="primary-label"><b>{{trans('content.car-list')}}</b></label>
                            <div class="car-list">
                                @foreach($cars as $car)
                                <div class="list">
                                    @if($car->type == 2)
                                        <img src="{{asset('web/images/sedan.svg')}}" alt="">
                                    @else
                                        <img src="{{asset('web/images/suv.svg')}}" alt="">
                                    @endif
                                   
                                    <h6>{{$car->country->$field}}</h6>
                                    <h6>{{(isset($car->emirate)) ? $car->emirate->$field : ''}}</h6>
                                    <h6>{{$car->plate_prefix.' '.$car->plate_number}}</h6>
                                     <?php /* ?>
                                    <div class="contex-menu">
                                        <a href="javascript:void(0);" class="contex-icon"><img
                                                src="{{asset('web/images/edit.svg')}}" alt=""></a>
                                        <div class="menu-items">
                                            <ul>
                                               
                                              
                                                <li>
                                                    
                                       <a href="javascript:void(0);" data-popup-action-type="manage-car" data-extra-action="edit" data-popup-action-id="{{$car->ucp_id}}" data-extra-id="{{$user->id}}" class="change-car open-popup-content">{{trans('content.change-car')}} </a>
                                      
                                                </li>
                                                
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    <?php */ ?>
                                </div>
                                @endforeach

                                 @if($is_adding_car == 1)
                                    <div class="text-center mt-2">
                                        <a href="javascript:void(0);" data-popup-action-type="manage-car" data-extra-action="add" data-popup-action-id="" data-extra-id="{{$user->id}}"  class="add-car open-popup-content" data-pack_id="{{$pack->id}}"><i class="icon-plus"></i> @if(sizeof($cars)>0) {{trans('content.add-another-car')}} @else  {{trans('content.add-car')}} @endif</a>
                                     </div>
                                 @endif
                            </div>
                            <?php /* ?>
                            <div class="left-side">
                                <label class="primary-label"><b>{{trans('content.car-list')}}</b></label>
                            </div>
                             <div class="right-side">
                                 @foreach($user->userCars as $car)
                                    <div class="car-details">
                                        <span>{{$car->plate_prefix.' '.$car->plate_number}}</span>
                                       {{--
                                       <a href="javascript:void(0);" data-popup-action-type="manage-car" data-extra-action="edit" data-popup-action-id="{{$car->ucp_id}}" data-extra-id="{{$user->id}}"  data-pack_id="{{$pack->id}}" class="change-car open-popup-content">{{trans('content.change-car')}} </a>
                                      --}}
                                       @if($car->type == 2)
                                          <label for="sudan">
                                            <img src="{{asset('web/images/sedan.svg')}}" alt="">
                                            <span>{{trans('content.sedan')}}</span>
                                        </label>
                                        @else
                                        <label for="suv">
                                            <img src="{{asset('web/images/suv.svg')}}" alt="">
                                            <span>{{trans('content.suv')}}</span>
                                        </label>
                                        @endif
                                    </div>
                                 @endforeach
                                 @if($is_adding_car == 1)
                                <div class="text-center mt-2">
                                    <a href="javascript:void(0);" data-popup-action-type="manage-car" data-extra-action="add" data-popup-action-id="" data-extra-id="{{$user->id}}"  data-pack_id="{{$pack->id}}"  class="add-car open-popup-content"><i class="icon-plus"></i> @if(sizeof($cars)>0) {{trans('content.add-another-car')}} @else  {{trans('content.add-car')}} @endif</a>
                                 </div>
                                 @endif

                             </div>
                             <?php */ ?>
                        </li>
                         @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">

    </div>
</section>
<div class="modal fade" id="load_model_content" tabindex="-1" role="dialog"></div>
<section class="fix-height"></section>
@include('web.offer-modal')
@include('web.TandC-modals')
@stop
@section("css")
<style type="text/css">
#entrycircle,#parkingcircle,#tablecircle,#cabanacircle,#inparkcircle,#carwashServicescircle,#porterServicescircle,#stuntshowcircle {
  
  width: 150px;
  height: 75px;
}
.slick-disabled {
    opacity: 0;
    pointer-events:none;
}
</style>
@stop
@section('js')
<script type="text/javascript" src="{{asset('web/js/progressbar.js')}}"></script>
<script type="text/javascript">


if($("#entrycircle").length > 0){
    var bar = new ProgressBar.SemiCircle(entrycircle, {
      strokeWidth: 6,
      easing: 'easeInOut',
      duration: 1400,
      color: '#0dd40d',
      trailColor: '#eee',
      trailWidth: 1,
      svgStyle: null

    });
    bar.animate("{{$usageDetails['unUsedEntryPercent']}}");  // Number from 0.0 to 1.0
}

if($("#parkingcircle").length > 0){
var park_bar = new ProgressBar.SemiCircle(parkingcircle, {
  strokeWidth: 6,
  easing: 'easeInOut',
  duration: 1400,
  color: '#0dd40d',
  trailColor: '#eee',
  trailWidth: 1,
  svgStyle: null
});
var prk = "{{$usageDetails['unUsedParkingPercent']}}";
park_bar.animate(parseFloat(prk));  // Number from 0.0 to 1.0
}
if($("#tablecircle").length > 0){
    var table_bar = new ProgressBar.SemiCircle(tablecircle, {
      strokeWidth: 6,
      easing: 'easeInOut',
      duration: 1400,
      color: '#0dd40d',
      trailColor: '#eee',
      trailWidth: 1,
      svgStyle: null
    });
    var table = "{{$usageDetails['remaining_tables_percent']}}";
    table_bar.animate(parseFloat(table));  // Number from 0.0 to 1.0
}

if($("#cabanacircle").length > 0){
    var cabana_bar = new ProgressBar.SemiCircle(cabanacircle, {
      strokeWidth: 6,
      easing: 'easeInOut',
      duration: 1400,
      color: '#0dd40d',
      trailColor: '#eee',
      trailWidth: 1,
      svgStyle: null
    });
    var cabana = "{{($pack->cabana_remaining_count > 0) ? $pack->cabana_remaining_count/ $pack->cabana_total_count : 0}}";
    cabana_bar.animate(parseFloat(cabana));  // Number from 0.0 to 1.0
}
/*

if($("#inparkcircle").length > 0){
    var inpark_bar = new ProgressBar.SemiCircle(inparkcircle, {
      strokeWidth: 6,
      easing: 'easeInOut',
      duration: 1400,
      color: '#0dd40d',
      trailColor: '#eee',
      trailWidth: 1,
      svgStyle: null
    });
    var inpark = "{{($pack->inpark_taxi_voucher > 0) ? $pack->inpark_taxi_voucher : 0}}";
    inpark_bar.animate(1.0);  // Number from 0.0 to 1.0
}

if($("#porterServicescircle").length > 0){
    var porter_bar = new ProgressBar.SemiCircle(porterServicescircle, {
      strokeWidth: 6,
      easing: 'easeInOut',
      duration: 1400,
      color: '#0dd40d',
      trailColor: '#eee',
      trailWidth: 1,
      svgStyle: null
    });
    var porter = "{{($pack->porter_services_voucher > 0) ? $pack->porter_services_voucher : 0}}";
    porter_bar.animate(1.0);  // Number from 0.0 to 1.0
}

if($("#carwashServicescircle").length > 0){
    var car_bar = new ProgressBar.SemiCircle(carwashServicescircle, {
      strokeWidth: 6,
      easing: 'easeInOut',
      duration: 1400,
      color: '#0dd40d',
      trailColor: '#eee',
      trailWidth: 1,
      svgStyle: null
    });
    var carb = "{{($pack->carwash_services_voucher > 0) ? $pack->carwash_services_voucher : 0}}";
    car_bar.animate(1.0);  // Number from 0.0 to 1.0
}

if($("#stuntshowcircle").length > 0){
    var stunt_bar = new ProgressBar.SemiCircle(stuntshowcircle, {
      strokeWidth: 6,
      easing: 'easeInOut',
      duration: 1400,
      color: '#0dd40d',
      trailColor: '#eee',
      trailWidth: 1,
      svgStyle: null
    });
    var stunt = "{{($pack->stunt_show > 0) ? $pack->stunt_show : 0}}";
    stunt_bar.animate(1.0);  // Number from 0.0 to 1.0
}
*/
$(document).ready(function(){
    $('.contex-icon').on('click', function () {
      $(this).next('.menu-items').toggle();
    });
    // My tickets Slider With Arrow
    $('.my-tickets').slick({
        dots: false,
        arrows: true,
        infinite: false,
        autoplay: true,
        speed: 1000,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    centerPadding: '15px',
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 576,
                settings: {
                    centerPadding: '15px',
                    slidesToShow: 1,
                }
            }
        ]
    });

});
</script>
<script type="text/javascript">

     var closedOffers = @json($closedOffers);
    //complementary-offer click
    $(document).on('click', 'div.complementary-offer-content', function (e) {
        var offer_name = $(this).data('offername');  
        /*if( (offer_name == 'LEGOLAND WaterPark') || (offer_name == 'Bollywood Parks Dubai')
        || offer_name == 'DPR - All Parks' ){
            return false;
        }*/

        if(closedOffers.indexOf(offer_name) > -1){
            return false;
        }

        if (offer_name != '' && offer_name != undefined) {

            //push to datlayer
             dataLayer.push(
                {'event': 'vip_user_page',
                    'eventCategory':'my_purchase', 
                    'eventAction': 'complimentary_offer', 
                    'eventLabel': offer_name , 
                    'pageType': 'vip_user_page',
                    'Language': '{{app()->getLocale()}}'
                }); 

            /*console.log(
               {'event': 'vip_user_page',
                    'eventCategory':'my_purchase', 
                    'eventAction': 'complimentary_offer', 
                    'eventLabel': offer_name , 
                    'pageType': 'vip_user_page',
                    'Language': '{{app()->getLocale()}}'
                });*/

            var offerLogo = $(this).find('img.offer-logo').attr('src');
            var offerName = $(this).find('h6.offer-name').html();
            $modal = $('#modalOffersList');
            $modal.find('#modalOfferLogo').attr('src', offerLogo);
            $modal.find('#modalOfferName').html(offerName);
            //Get offrs
            $.ajax({
                url: "{!!route('pack.offers',app()->getLocale())!!}",
                dataType: 'html',
                data: {'pack_id': '{{$pack->id}}', 'offer_name': offer_name},
                cache: false,
                method: 'POST',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                beforeSend: function () {
                },
                success: function (response) {
                    //show modal
                    $modal.find('#offerList').html(response);
                    $modal.modal('show');
                },
                error: function (response) {
                    $(".second-step-error").html('Something went wrong!').show();
                },
                complete: function () {
                }
            });
        }
    });
    // Two Thumbnails Slider
    $('.two-thumbnails').slick({
        dots: false,
        arrows: true,
        infinite: true,
        autoplay: true,
        speed: 1000,
        slidesToShow: 3,
        slidesToScroll: 3,
        pauseOnHover:true,
        pauseOnFocus:true,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    dots: true,
                    arrows: false,
                    centerMode: true,
                    centerPadding: '10px',
                    slidesToShow: 1,
                }
            }
        ]
    });

    // Three Thumbnails Slider
    function sliderInit() {
        $('.three-thumbnails').slick({
            dots: false,
            arrows: true,
            infinite: true,
            autoplay: true,
            speed: 1000,
            slidesToShow: 3,
            slidesToScroll:3,
            pauseOnHover:true,
            pauseOnFocus:true,
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '15px',
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '15px',
                        slidesToShow: 1,
                    }
                }
            ]
        });
    }
    $(document).on('click', '.open-popup-content', function(){
        var action_type = $(this).attr('data-popup-action-type');
        var action_id = $(this).attr('data-popup-action-id');
        var extra_action = $(this).attr('data-extra-action');
        var extra_id = $(this).attr('data-extra-id');
        var pack_id = $(this).attr('data-pack_id');

        var load_model_url = "{!!route('load-popup-content',app()->getLocale())!!}";
        $.ajax({
            type: "POST",
            url: load_model_url,
            data: {
                extra_action: extra_action,
                action_type: action_type,
                action_id: action_id,
                extra_id: extra_id,
                pack_id: pack_id,
                _token: '{{ csrf_token() }}',

            },
            success: function(response){
                $('#load_model_content').html(response.data);
                $('#load_model_content').modal('show');

            },
            error: function(response){
                    $(".cm-success-message").html('').hide();
                    $(".first-step-error").html(response.responseJSON.message).show();
            },
            failure: function (errMsg) {
                $(".cm-success-message").html('').hide();
                $(".first-step-error").html(errMsg).show();

            }
        });
    });
    $(document).on('change', '.carCountry', function (e) {
        var countryId = $(this).val();
        var ediv = $(this).parent().parent().parent('div').find('.carEmirate');
        var ndiv = $(this).parent().parent().parent('div').find('.carNumber');
        if (countryId != '') {
            ediv.html('<option value="">Loading....</option>');
            //get Emirates
            $.ajax({
                url: "{!! route('get.emirates',app()->getLocale()) !!}",
                dataType: 'html',
                data: {'id': countryId},
                cache: false,
                method: 'POST',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                beforeSend: function () {
                },
                success: function (response) {
                    $("#emirate_pop_div").hide();
                    if(response != ''){
                        ediv.html(response);
                        $("#emirate_pop_div").show();
                    }
                },
                error: function (response) {
                },
                complete: function () {
                }
            });
            if (countryId == 1) {
                ndiv.html('<option value="">{{trans("web.pack.plate_number_placeholder")}}</option>');
            } else {
                ndiv.html('<option value="">Loading....</option>');
                //get plate number Prefix
                listPrefix('country', countryId, ndiv);
            }
        } else {
            ediv.html('<option value="">{{trans("web.pack.car_emirate_placeholder")}}</option>');
            ndiv.html('<option value="">{{trans("web.pack.plate_number_placeholder")}}</option>');
        }
    });
    $(document).on('change', '.carEmirate', function (e) {
        var emirateId = $(this).val();
        var ndiv = $(this).parent().parent().parent('div').find('.carNumber');
        if (emirateId != '') {
            ndiv.html('<option value="">Loading....</option>');
            listPrefix('city', emirateId, ndiv);
        } else {
            ndiv.html('<option value="">{{trans("web.pack.plate_number_placeholder")}}</option>');
        }
    });
    function listPrefix(type, id, ndiv) {
        $.ajax({
            url: "{!! route('get.platePrefix',app()->getLocale()) !!}",
            dataType: 'html',
            data: {'id': id, 'type': type},
            cache: false,
            method: 'POST',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            beforeSend: function () {
            },
            success: function (response) {
                ndiv.html(response);
            },
            error: function (response) {
            },
            complete: function () {
            }
        });
    }

    $(document).on('click', '.submit_car_detail', function(){
        $('.submit_car_detail').attr('disabled', true);
       var postData = $('#carForm').serialize()
        $.ajax({
            url: "{!!route('manage-car-detail',app()->getLocale())!!}",
            type: 'POST',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            data: postData,
            beforeSend: function () {
                $('.submit_car_detail').attr('disabled', true);
            },
            success: function (response) {
                if(response.status == 'success'){
                    $('.submit_car_detail').attr('disabled', false);
                    $(".first-step-error").html('').hide();
                    $(".second-step-error").html('').hide();
                    $(".cm-success-message").html(response.message).show();

                    setTimeout(function(){
                        location.reload();
                    }, 1500);
                }else{
                    //$('.submit_car_detail').attr('disabled', false);
                    $(".second-step-error").html(response.message).show();
                    $(".cm-success-message").html('').hide();
                }



            },
            error: function (response) {
                $('.submit_car_detail').attr('disabled', false);
                $(".cm-success-message").html('').hide();
                $(".first-step-error").html(response.responseJSON.message).show();
            },
            complete: function () {
                $('.submit_car_detail').attr('disabled', false);
            }
        });
    });
    function platenumberOnly(id) {
        // Get element by id which passed as parameter within HTML element event
        var element = document.getElementById(id);
        // Use numbers only pattern, from 0 to 9
        var regex = /[^0-9]/gi;
        // This removes any other character but numbers as entered by user
        element.value = element.value.replace(regex, "");
    }
    $(document).on('click',".offerHide",function(eve){
        eve.preventDefault();
        var url = $(this).data('href');
        var parent = $(this).closest('li');
        var elem = $(this);
        $.ajax({
            url: url,
            dataType: 'json',
           
            cache: false,
            method: 'GET',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            beforeSend: function () {
            },
            success: function (response) {
                if(response.status == 'success'){
                    if($(parent).find('.consumed-div').length > 0){
                        $(parent).find('.consumed-div').replaceWith(response.html);
                    }else{
                        $(elem).replaceWith(response.html);
                    }
                    
                    
                }
            },
            error: function (response) {
            },
            complete: function () {
            }
        });

    });
</script>
@stop
