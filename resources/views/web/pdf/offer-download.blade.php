<!doctype html>
<html lang="{{app()->getLocale()}}">
<head>
<title>Global Village</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,400;0,700;1,600&display=swap" rel="stylesheet">
<!-- <link href="//db.onlinewebfonts.com/c/827efd408950e4d94af5e60b9a37c5b8?family=Omnes+Semibold" rel="stylesheet" type="text/css"/> -->

  @if(app()->getLocale() == 'ar')
	<style>
		body {
			font-family: DejaVu Sans, sans-serif;
		}
	</style>
	@else
	<style>
		body {
			font-family: 'Poppins', sans-serif;
		}
	</style>

	@endif

<style type="text/css">
body {
	margin: 0 auto;
    padding: 0;
	width: 18cm;
	height: 25cm;
	color: #4E4953;
}
.top-content { text-align: center; }
.offer-logo { margin-bottom: 20px; }
.offer-name { margin-bottom: 50px; }
.offer-name h3 {
    font-size: 18px;
	line-height: 20px;
    margin: 0 0 15px;
}
.offer-name h5 {
    font-size: 18px;
	line-height: 20px;
    margin: 0;
}
.offer-name h5 span { color: #A6A4A9; }
.qr-code {
    margin: 0 auto 100px;
    width: 350px;
}
.qr-code img { width: 200px; }
.qr-code h6 {
    font-size: 24px;
    font-weight: 600;
    margin: 10px 0 0;
}
.qr-code p {
    font-size: 15px;
    color: #A6A4A9;
}

.modal-body-title { display: none; }
.terms-content h6 {
    margin-bottom: 10px;
	font-size: 16px;
    font-weight: 600;
    color: var(--text-primary);
	font-family: 'Poppins', sans-serif;
}
.terms-content p {
	margin-top: 0;
	margin-bottom: 0;
	font-size: 12px;
	display: list-item;
}
.terms-content p a { color: var(--orange); }
 .page-break {     page-break-after: always; }
</style>

</head>

@if(app()->getLocale() == 'ar')
<body dir="rtl">
@else
	<body >
@endif

	<?php
	/*
		$images = array(
			'Roxy Cinemas' => 'roxycinemas.jpg',
			'Laguna Waterpark' => 'lagunawaterparks.jpg',
			'The Green Planet' => 'greenplanet.jpg',
			'DPR - All Parks' => 'dubaiparks.jpg',
			'Bollywood Parks Dubai' => 'bollywoodparks.jpg',
			'Motiongate Dubai' => 'motiongate.jpg',
			'LEGOLAND Dubai' => 'legoland.jpg',
			'LEGOLAND WaterPark' => 'legolandwaterpark.jpg',
			'Expo 2020 Dubai' => 'expo-2021-dubai.png',
		);
		$images = array(
			'roxycinemas' => 'roxycinemas.jpg',
			'lagunawaterpark' => 'lagunawaterparks.jpg',
			'thegreenplanet' => 'greenplanet.jpg',
			'dpr' => 'dubaiparks.jpg',
			'dpr-allparks' => 'dubaiparks.jpg',
			'dubaiparksandresorts' => 'dubaiparks.jpg',
			'bollywoodparksdubai' => 'bollywoodparks.jpg',
			'motiongatedubai' => 'motiongate.jpg',
			'legolanddubai' => 'legoland.jpg',
			'legolandwaterpark' => 'legolandwaterpark.jpg',
			'expo2020dubai' => 'expo-2021-dubai.png',
			'seabreeze' => 'expo-2021-dubai.png',
		);
		*/
		$images = partner_logos();
		$img = (isset($images[$offer->offerName])) ? $images[$offer->offerName] : 'dubaiparks.jpg';

		$tandc = $offer->t_and_c;
	 ?>
<div class="main-content">
	<div class="top-content">
		
			@if($offer->offerName == 'thegreenplanet' || $offer->offerName == 'roxycinemas' || $offer->offerName == 'lagunawaterpark')
				<h4 style="color:red;">{{trans('content.other_not_for_sale')}}</h4>
			@else
				<h4 style="color:red;">{{trans('content.expo_2020.download')}}</h4>
			@endif
			
		<div class="offer-logo">
			<img src="{{$img}}" alt="{{$offer->merchant_name_en}}" />
		</div>
		<div class="offer-name">
			
			<h3>{{$offer->offer_name}}</h3>
			
			<h5><span>{{trans('content.valid-until')}}</span> {{date('d m Y',strtotime($offer->valid_to))}}</h5>
			
		</div>
		<br/><br/><br/>
		<div class="qr-code">
			@if(strtolower(str_replace(' ', '', $offer->code_type)) == 'qrcode' || strtolower(str_replace(' ', '', $offer->code_type)) == 'mediacode')
			@php($img = base64_encode(QrCode::format('svg')->size(300)->generate($offer->promo_code)))
				<img src="data:image/svg+xml;base64, {!! $img !!}">
			@elseif(strtolower(str_replace(' ', '', $offer->code_type)) == 'barcode')
				<img src="data:image/png;base64, {!! DNS1D::getBarcodePNG($offer->promo_code, 'C39+',3,800) !!}">
			@endif
			<h6>{{$offer->promo_code}}</h6>
				@if($offer->offerName == 'insideburjalarab' || $offer->offerName == 'ibaa')
                    <p>{!! trans('offers.qr_text.ibaa') !!}</p>
                @elseif($offer->offerName == 'seabreeze')
                    @if($pack->category == 'gold')
                        <p>{!! trans('offers.qr_text.seabreeze.silver') !!}</p>
                    @elseif($pack->category == 'private')
                        <p>{!! trans('offers.qr_text.seabreeze.private') !!}</p>
                    @elseif($pack->category == 'diamond')
                        <p>{!! trans('offers.qr_text.seabreeze.private') !!}</p>
                    @elseif($pack->category == 'platinum')
                        <p>{!! trans('offers.qr_text.seabreeze.silver') !!}</p>
                    @else
                        <p>{!! trans('offers.qr_text.seabreeze.silver') !!}</p>
                    @endif
                @elseif($offer->offerName == 'dubaiparksandresorts' || $offer->offerName == 'dpr')
					<p>{!! trans('offers.qr_text.dpr') !!}</p>
                @elseif($offer->offerName == 'roxy' || $offer->offerName == 'roxycinemas')
                     <p>{!! trans('offers.qr_text.roxy') !!}</p>
                  @endif
		</div>
		<div class="page-break"></div>
	</div>


@if(View::exists('web.terms.terms-'.$tandc))
@include('web.terms.terms-'.$tandc)
@endif

</div>
</body>
</html>
