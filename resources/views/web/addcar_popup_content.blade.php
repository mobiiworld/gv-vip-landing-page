<div class="modal fade" id="load_model_content" role="dialog">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" >
                {{trans('content.add-car')}}
                <span>{{trans('content.add_car_info_text')}}</span>
            </h4>
            
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            
            <form id="carForm" class="carForm">
                <div class="row">
                
                   <div class="col-lg-12 col-md-12 col-12">
                    <div class="form-group">
                   
                        <div class="car-selection">
                            <div class="position-relative">
                                <input type="radio" id="suv" name="type" value="1" class="cartype_class suv">
                                <label for="suv">
                                    <img src="{{asset('web/images/suv.svg')}}" alt="">
                                    <span>{{trans('content.suv')}}</span>
                                </label>
                            </div>
                            <div class="position-relative">
                                <input type="radio" id="sudan" name="type" value="2"  class="cartype_class sudan">
                                <label for="sudan">
                                    <img src="{{asset('web/images/sedan.svg')}}" alt="">
                                    <span>{{trans('content.sedan')}}</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
              <div class="row">
                  
                  <div class="col-lg-6 col-md-6 col-12">
                    <div class="form-group">
                        <label>{{trans('content.contry-car-register')}}<span>*</span></label>
                        <select class="form-control custom-select carCountry" id="popup_car_country" name="popup_car_country">
                            <option value="">{{trans('web.pack.car_country_placeholder')}}</option>
                            @foreach($car_countries as $ccountry)
                                <option value="{{$ccountry->id}}" >{{$ccountry->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                  <div class="col-lg-6 col-md-6 col-12" id="emirate_pop_div">
                    <div class="form-group">
                        <label>{{trans('content.emirates-car-register')}}<span>*</span></label>
                        <select class="form-control custom-select carEmirate" id="popup_emirate" name="popup_emirate">
                            <option value="">{{trans('web.pack.car_emirate_placeholder')}}</option>
                           
                        </select>
                    </div>
                </div>
                  <div class="col-lg-6 col-md-6 col-12">
                    <div class="form-group">
                        <label>{{trans('content.plate-code')}}<span>*</span></label>
                        <select class="form-control custom-select carNumber" name="popup_plate_prefix" id="popup_plate_prefix">
                            <option value="">{{trans('web.pack.plate_number_placeholder')}}</option>
                            
                        </select>
                    </div>
                </div>
                  <div class="col-lg-6 col-md-6 col-12">
                    <div class="form-group">
                        <label>{{trans('content.plate-number')}}<span>*</span></label>
                        <input type="text" class="form-control" value="" placeholder="{{trans('content.enter-plate-number')}}" name="popup_plate_number" oninput="platenumberOnly(this.id);" maxlength="10" id="popup_plate_number" />
                    </div>
                </div>
               </div>
             </form>
                <div class="second-step-error-popup" style="display: none;">{{trans('web.required_fields')}}</div>
                <div class="cm-success-message" style="display: none;"></div>
               
            
           
            <button type="submit" class="btn btn-theme-bordered submit_car_detail" id="sub_car_detail_btn" >{{trans('content.submit')}}</button>
           
        </div>
    </div>

</div>

</div>