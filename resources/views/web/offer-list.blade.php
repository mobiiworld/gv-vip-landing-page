@if(!$offers->isEmpty())
<div class="slider three-thumbnails">
    @foreach($offers as $offer)
    <div class="complementary-offer-list">
        <div class="top">
            <ul>
                <li>
                    <label>{{trans('content.offer')}}</label>
                    @if($offer->consumed == "yes")
                    <div class="consumed-div">
                        <span class="consumed-class">{{trans('content.consumed')}}</span>
                        <a href="javascript:void(0)" data-href="{{ url(app()->getLocale().'/offer-hide/'.$offer->id) }}" class="offerHide offer-unhide">{{trans('content.unhide')}}</a>
                    </div>
                        
                    @else
                        <a href="javascript:void(0)" data-href="{{ url(app()->getLocale().'/offer-hide/'.$offer->id) }}" class="offerHide offer-hide">{{trans('content.hide')}}</a>
                    @endif
                    <p>{{$offer->offer_name}}</p>
                </li>
                <li>
                    <label>{{trans('content.valid-from')}}</label>
                    <p>{{date('d m Y',strtotime($offer->valid_from))}}</p>
                </li>
                <li>
                    <label>{{trans('content.valid-until')}}</label>
                    <p>{{date('d m Y',strtotime($offer->valid_to))}}</p>
                </li>
            </ul>
        </div>
        <div class="bottom">
            <div class="fixed-height">
                @if(strtolower(str_replace(' ', '', $offer->code_type)) == 'qrcode' || strtolower(str_replace(' ', '', $offer->code_type)) == 'mediacode')
                    @php($img = base64_encode(QrCode::format('svg')->size(300)->generate($offer->promo_code)))
                    <div class="promo-code">
                        <label>{{trans('content.promo-code')}}</label>
                       
                        <a href="{{ url(app()->getLocale().'/offer-download/'.$offer->id) }}"><i class="icon-download"></i> {{trans('content.download')}}</a>
                      <?php /* ?>   
                        <a href="javascript:;" class="copy-promo"><i class="icon-copy"></i> {{trans('content.copy')}}</a>
                        <input type="hidden" value="{{$offer->promo_code}}"><?php */ ?>
                    </div>
                    <div class="qr-code">
                        <img src="data:image/svg+xml;base64, {!! $img !!}">
                        <span>{{$offer->promo_code}}</span>
                        
                    </div>
                   
                @elseif(strtolower(str_replace(' ', '', $offer->code_type)) == 'barcode')

                    <div class="promo-code">
                        <label>{{trans('content.promo-code')}}</label>
                        <a href="{{ url(app()->getLocale().'/offer-download/'.$offer->id) }}" ><i class="icon-download"></i> {{trans('content.download')}}</a>
                    </div>
                    <div class="qr-code">
                        <img src="data:image/png;base64, {!! DNS1D::getBarcodePNG($offer->promo_code, 'C39+',5,1000) !!}">
                        <span>{{$offer->promo_code}}</span>
                    </div>

                   
                @else
                    <div class="promo-code">
                        <label>{{trans('content.promo-code')}}</label>
                        @if($offer->offerName != 'lagunawaterpark' && $offer->offerName != 'thegreenplanet')
                            <a href="javascript:;" class="copy-promo"><i class="icon-copy"></i> {{trans('content.copy')}}</a>
                        @endif
                        <input type="hidden" value="{{$offer->promo_code}}">
                    </div>
                    <div class="qr-code">
                        <h2>{{$offer->promo_code}}</h2>
                    </div>
                                        
                @endif

                @if($offer->offerName == 'insideburjalarab' || $offer->offerName == 'ibaa')
                    <p>{!! trans('offers.qr_text.ibaa') !!}</p>
                @elseif($offer->offerName == 'seabreeze')
                    @if($pack->category == 'gold')
                        <p>{!! trans('offers.qr_text.seabreeze.silver') !!}</p>
                    @elseif($pack->category == 'private')
                        <p>{!! trans('offers.qr_text.seabreeze.private') !!}</p>
                    @elseif($pack->category == 'diamond')
                        <p>{!! trans('offers.qr_text.seabreeze.private') !!}</p>
                    @elseif($pack->category == 'platinum')
                        <p>{!! trans('offers.qr_text.seabreeze.silver') !!}</p>
                    @else
                        <p>{!! trans('offers.qr_text.seabreeze.silver') !!}</p>
                    @endif
                @elseif($offer->offerName == 'dubaiparksandresorts' || $offer->offerName == 'dpr')
                <p>{!! trans('offers.qr_text.dpr') !!}</p>
                @elseif($offer->offerName == 'roxy' || $offer->offerName == 'roxycinemas')
                     <p>{!! trans('offers.qr_text.roxy') !!}</p>
                
                @endif
            </div>
            

        </div>
        @if(View::exists('web.terms.terms-'.$offer->t_and_c))
		<div class="terms-conditions">
			<a href="javascript:;" data-toggle="modal" data-target="#termsModal-{{$offer->t_and_c}}">{{trans('content.terms-conditions')}}</a> {{trans('content.apply')}}
		</div>
        @endif
    </div>
    @endforeach
</div>
<script type="text/javascript">
    sliderInit();
</script>
@else
<div class="no-offers">
	<img src="{{asset('web/images/gift.svg')}}" />
	<p>{{trans('content.no-offers')}}</p>
</div>
@endif
