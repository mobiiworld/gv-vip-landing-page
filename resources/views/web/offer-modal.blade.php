<div class="modal fade" id="modalOffersList" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content offers">
            <a href="javascript:;" class="cross" data-dismiss="modal"><i class="icon-cross"></i></a>
            <div class="modal-body">
                <div class="modal-body-title">
                    <img id="modalOfferLogo" src="" alt="" />
                    <h4 id="modalOfferName">{{trans('content.offer-name')}}</h4>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-11 col-md-10 col-12" id="offerList">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>