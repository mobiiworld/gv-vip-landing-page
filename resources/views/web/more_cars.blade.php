<?php
$locale = app()->getLocale();
$car_countries = \App\CarCountry::select('id', "name_{$locale} as name", 'code')->get()->keyBy('id');

?>
@if(count($carData) > 0 || count($otherCars) > 0)
	<?php 
		
		$prefix = 'country';
		$carCnt = 1;
        $loopCnt = 0;
	 ?>
    
   
    
      
        @if(count($carData) > 0 )
        @foreach($carData as $more)
        
            <div class="list">
                    <input type="checkbox" id="list_reg_{{$loopCnt}}" class="checkbox_car_list" name="registered_car_old[{{$loopCnt}}]" value="1" checked="checked">
                    <label for="list_reg_{{$loopCnt}}">
                       
                        <div class="car-details">
                            @if( $more->type == 2) 
                                <img src="{{asset('web/images/sedan.svg')}}" alt="" />
                            @else
                                <img src="{{asset('web/images/suv.svg')}}" alt="" />
                            @endif
                            <small>{{(isset($car_countries[$more->country_id]) ? $car_countries[$more->country_id]->name : '')}}</small>

                            @if(!empty($more->emirate_id))
                                <?php 
                                    $emirates = \App\Emirate::select('id', "name_{$locale} as name", 'code')
                                    ->where('id', $more->emirate_id)
                                    ->first();
                                ?>
                                @if(!empty($emirates))
                                    <small>{{$emirates->name}}</small>
                                @endif
                            @endif
                            <small>{{$more->plate_prefix}}</small>
                            <small>{{$more->plate_number}}</small>
                        </div>
                    </label>
               <!--  <a href="javascript:void(0)"><i class="icon-cross"></i></a> -->
               <input type="hidden" name="car_id[{{$loopCnt}}]" value="{{$more->id}}">
               <input type="hidden" name="car_type[{{$loopCnt}}]" value="{{$more->type}}">
               <input type="hidden" name="car_country[{{$loopCnt}}]" value="{{$more->country_id}}">
               <input type="hidden" name="emirate[{{$loopCnt}}]" value="{{$more->emirate_id}}">
               <input type="hidden" name="plate_prefix[{{$loopCnt}}]" value="{{$more->plate_prefix}}">
               <input type="hidden" name="plate_number[{{$loopCnt}}]" value="{{$more->plate_number}}">
            </div>
       

       <?php $loopCnt++; ?>
    @endforeach
    @endif

    @if(count($otherCars) > 0 )
	   @foreach($otherCars as $more)

             

            <div class="list">
                    <input type="checkbox" id="list_reg_{{$loopCnt}}" class="checkbox_car_list" name="registered_car_old[{{$loopCnt}}]" value="1">
                    <label for="list_reg_{{$loopCnt}}">
                       
                        <div class="car-details">
                            @if( $more->type == 2) 
                                <img src="{{asset('web/images/sedan.svg')}}" alt="" />
                            @else
                                <img src="{{asset('web/images/suv.svg')}}" alt="" />
                            @endif
                            <small>{{(isset($car_countries[$more->country_id]) ? $car_countries[$more->country_id]->name : '')}}</small>

                            @if(!empty($more->emirate_id))
                                <?php 
                                    $emirates = \App\Emirate::select('id', "name_{$locale} as name", 'code')
                                    ->where('id', $more->emirate_id)
                                    ->first();
                                ?>
                                @if(!empty($emirates))
                                    <small>{{$emirates->name}}</small>
                                @endif
                            @endif
                            <small>{{$more->plate_prefix}}</small>
                            <small>{{$more->plate_number}}</small>
                        </div>
                    </label>
               <!--  <a href="javascript:void(0)"><i class="icon-cross"></i></a> -->
               <input type="hidden" name="car_id[{{$loopCnt}}]" value="{{$more->id}}">
               <input type="hidden" name="car_type[{{$loopCnt}}]" value="{{$more->type}}">
               <input type="hidden" name="car_country[{{$loopCnt}}]" value="{{$more->country_id}}">
               <input type="hidden" name="emirate[{{$loopCnt}}]" value="{{$more->emirate_id}}">
               <input type="hidden" name="plate_prefix[{{$loopCnt}}]" value="{{$more->plate_prefix}}">
               <input type="hidden" name="plate_number[{{$loopCnt}}]" value="{{$more->plate_number}}">
            </div>
       

       <?php $loopCnt++; ?>
	@endforeach
    @endif
   
@else 

    <div class="list remcarMain">
                <input type="checkbox" id="list_reg_{{$lastAddedCnt}}" class="checkbox_car_list" name="registered_car[{{$lastAddedCnt}}]" value="1" checked="true">
                <label for="list_reg_{{$lastAddedCnt}}">
                   <!--  <span class="title">Choose your registered car</span> -->
                    <div class="car-details">
                        @if( $more->type == 2) 
                            <img src="{{asset('web/images/sedan.svg')}}" alt="" />
                        @else
                            <img src="{{asset('web/images/suv.svg')}}" alt="" />
                        @endif
                        <small>{{(isset($car_countries[$more->country_id]) ? $car_countries[$more->country_id]->name : '')}}</small>

                        @if(!empty($more->emirate_id))
                            <?php 
                                $emirates = \App\Emirate::select('id', "name_{$locale} as name", 'code')
                                ->where('id', $more->emirate_id)
                                ->first();
                            ?>
                            @if(!empty($emirates))
                                <small>{{$emirates->name}}</small>
                            @endif
                        @endif
                        <small>{{$more->plate_prefix}}</small>
                        <small>{{$more->plate_number}}</small>
                    </div>
                </label>
            <a href="javascript:void(0)" class="removeCarSingle"><i class="icon-cross"></i></a>
           <input type="hidden" name="car_type[{{$lastAddedCnt}}]" value="{{$more->type}}">
           <input type="hidden" name="car_country[{{$lastAddedCnt}}]" value="{{$more->country_id}}">
           <input type="hidden" name="emirate[{{$lastAddedCnt}}]" value="{{$more->emirate_id}}">
           <input type="hidden" name="plate_prefix[{{$lastAddedCnt}}]" value="{{$more->plate_prefix}}">
           <input type="hidden" name="plate_number[{{$lastAddedCnt}}]" value="{{$more->plate_number}}">
    </div>

@endif