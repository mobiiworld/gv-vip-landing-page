@extends('web.partials.master')
@section('content')
<section class="inner-banner" style="">
    <div class="container">
        <h1>{{trans('content.banner-text1')}}</h1>
    </div>
</section>

<section class="starting-text">
   
    <div class="container">
        <span>{{trans('content.starting-text')}}</span>
    </div>
</section>

<section class="vip-pack">
    <div class="vip-pack-content">
        <form id="packForm" class="packForm">
            <div class="first-step">
                <div class="card">
                    <div class="card-header"><span>{{trans('content.pack-details')}}</span></div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>{{trans('content.pack-number')}}<span>*</span> <i class="icon-info iconIfnoImage" data-toggle="popover-hover"
                              data-img="{{asset('web/images/s29/image1-'.app()->getLocale().'.jpg')}}"></i></label>
                            <input type="text" class="form-control formelementDis removeSpaces" placeholder="{{trans('content.enter-pack-number')}}" name="vip_pack_number" id="vip_pack_number" maxlength="20" />
                        </div>
                        <div class="form-group">
                            <label>{{trans('content.activation-code')}}<span>*</span> <i class="icon-info  iconIfnoImage" data-toggle="popover-hover"
                              data-img="{{asset('web/images/s29/image2-'.app()->getLocale().'.jpg')}}"></i></label>
                            <input type="text" class="form-control formelementDis removeSpaces" placeholder="{{trans('content.enter-activation-code')}}" name="activation_code" id="activation_code" maxlength="20" />
                        </div>
                        <div class="form-group mb-0">
                            <label>{{trans('content.email')}}<span>*</span><i class="icon-info popoverContent" data-toggle="popover-hover"
                              data-content="{{trans('web.email_info')}}"></i> </label>
                            <input type="email" class="form-control formelementDis" placeholder="{{trans('content.enter-email')}}" name="email" id="email" autocomplete="off"/>
                        </div>
                    </div>
                </div>
               
                <div class="first-step-error" id="first-step-error" style="display: none;"></div>
                <div class="action-button1">
                    <button type="button" class="btn btn-theme " id="NextOtp">{{trans('content.next')}}</button>
                </div>
               
            </div>
            <div class="second-step" style="display: none;">
                <div class="card">
                    <div class="card-header"><span>{{trans('content.your-details')}}</span></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-12 paddingright">
                                <div class="form-group">
                                    <label>{{trans('content.first-name')}}<span>*</span></label>
                                    <input type="text" id="first_name" class="form-control" placeholder="{{trans('content.enter-first-name')}}" name="first_name" />
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12 paddingleft">
                                <div class="form-group">
                                    <label>{{trans('content.last-name')}}<span>*</span></label>
                                    <input type="text" class="form-control" placeholder="{{trans('content.enter-last-name')}}" name="last_name" id="last_name" />
                                </div>
                            </div>
                            {{--
                            <div class="col-lg-6 col-md-6 col-12 paddingright">
                                <div class="form-group">
                                    <label>{{trans('content.email')}}<span>*</span></label>
                                    <input type="email" class="form-control" placeholder="{{trans('content.enter-email')}}" name="email" id="email" autocomplete="off" />
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12 paddingleft">
                                <div class="form-group">
                                    <label>{{trans('content.confirm-email')}}<span>*</span></label>
                                    <input type="email" class="form-control" placeholder="{{trans('content.confirm-email')}}" name="confirm_email" id="confemail" onblur="confirmEmail()" autocomplete="off"/>
									<div class="errorTxt"></div>
                                </div>
                            </div>
                            --}}
                            <div class="col-lg-6 col-md-6 col-12 paddingright">
                                <div class="form-group">
                                    <label>{{trans('content.mobile-number')}}<span>*</span></label>
                                    
                                            <input type="tel" class="form-control" placeholder="{{trans('content.enter-mobile-number')}}" name="mobile" maxlength="15" id="mobile_number"  oninput="return this.value = this.value.replace(/[^0-9 .]/g, '').replace(/(\..*)\./g, '$1');" />
                                            <input type="hidden" name="mobile_dial_code" id="mobile_dial_code" value="971">
                                        

                                </div>
                            </div>
{{-- 
                            <div class="col-lg-6 col-md-6 col-12 paddingleft">
                                <div class="form-group">
                                    <label>{{trans('content.gender')}}</label>
                                    <div class="gender-choose">
                                        <input type='radio' id='male' name='gender' value="male">
                                        <label for='male'>{{trans('content.male')}}</label>
                                        <input type='radio' id='female' name='gender' value="female">
                                        <label for='female'>{{trans('content.female')}}</label>
                                    </div>
                                </div>
                            </div>
                             --}}
                             <div class="col-lg-6 col-md-6 col-12 paddingright">
                                <div class="form-group ">
                                    <label>{{trans('content.birthdate')}}</label>
                                    <input type="text" class="form-control" id="DateofBirth" placeholder="{{trans('content.date-format')}}" name="dob" autocomplete="off" />
                                    <img src="{{asset('web/images/calendar.svg')}}" class="calendar">
                                </div>
                            </div>
                            
                            <div class="col-lg-6 col-md-6 col-12 paddingleft">
                                <div class="form-group">
                                    <label>{{trans('content.country-residence')}}</label>
                                    <select class="form-control custom-select" name="residence" id="residence">
                                        <option value="">{{trans('web.pack.residence_placeholder')}}</option>
                                        @foreach($countries as $country)
                                                <option value="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-lg-6 col-md-6 col-12 paddingright residence_emirate_div" id="residence_emirate_div" style="display: none;">
                                <div class="form-group mb-0">
                                    <label>{{trans('content.emirate-residence')}}</label>
                                    <select class="form-control custom-select" name="emirate_residence" id="emirate_residence">
                                        <option value="">{{trans('content.emirate_residence_placeholder')}}</option>
                                         @foreach($emirates as $emr)
                                            <option value="{{$emr->id}}">{{$emr->name}}</option>
                                           @endforeach
                                        
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-12 paddingleft residence_emirate_div" style="display: none;">
                                <div class="form-group">
                                    <label>{{trans('content.nationality')}}</label>
                                    <select class="form-control custom-select" name="nationality" id="nationality">
                                        <option value="">{{trans('web.pack.nationality_placeholder')}}</option>
                                        @foreach($countries as $country)
                                           
                                                <option value="{{$country->id}}">{{$country->name}}</option>
                                           
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
                <div class="card ">
                    <div class="card-header"><span>{{trans('content.register-car')}}</span></div>
                    <div class="card-body">
                        <div class="added-cars">
                             <span class="title" id="reg_car_sub_title" >{{trans('content.add_new_car')}}</span>
                            <div class="listing" id="template" >
                                
                            </div>
                        </div>
                        <div class="add-another-car mt-5">
                            <a href="javascript:;" id="AddCar"><i class="icon-plus"></i> {{trans('content.add-car')}}</a>
                        </div>
                        <div class="cars-allowed-error" style="display: none;"></div>
                    </div>
                </div>

                <div class="checkbox-section">

                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="checkbox1" name="terms">
                        <label class="custom-control-label" for="checkbox1">{{trans('content.agree1')}}
                          @if(app()->getLocale() == 'en')
                          <a href="https://www.globalvillage.ae/en/terms-use" target="_blank">
                          @else
                          <a href="https://www.globalvillage.ae/ar/terms-use" target="_blank">
                          @endif

                            {{trans('content.agree2')}}</a>
                           {{trans('content.agree3')}}</label>
                    </div>
                    
                    <div class="custom-control custom-checkbox agree_offer_div">
                        <input type="checkbox" class="custom-control-input" id="checkbox2" name="agree_offers">
                        <label class="custom-control-label" for="checkbox2">
                            <?php
                            $locale = app()->getLocale();
                            $success_data = \App\DynamicText::whereIn('slug',['agree-offers'])->get()->keyBy('slug')->toArray();
                            $agree_offers = trans('content.agree4');
                            if(!empty($success_data['agree-offers'])){
                                $agree_offers = $success_data['agree-offers']['text_'.$locale];
                            }

                            ?>
                            {!! $agree_offers !!}
                        </label>
                    </div>
                </div>

				<div class="form-group mt-3">
					<div class="g-recaptcha" data-sitekey="{{env('CAPTCHA_CLIENT_KEY')}}"></div>
				</div>

                <div class="second-step-error" style="display: none;"></div>
                <div class="action-button2">
                    <button type="button" class="btn btn-theme-bordered" id="Cancel">{{trans('content.cancel')}}</button>
                    <button class="btn btn-theme packSubmit" type="submit">{{trans('content.active-pack')}}</button>
                    <input type="hidden" name="otp_id" id="user_otp" value="">
                </div>
            </div>
        </form>
    </div>
</section>
<div id="pre_loader_main"  style="display: none;" ><div id="pre_loader"><div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div></div></div>
<section class="fix-height"></section>

<?php
    $locale = app()->getLocale();
    $success_image = asset('web/images/congrulation.svg');
    $success_title = trans('web.pack.modal_head');
    
    $success_btn = trans('content.ok');
    $success_data = \App\DynamicText::whereIn('slug',['web-activation-success-title','web-activation-success-message','web-activation-success-image','web-activation-success-button'])->get()->keyBy('slug')->toArray();
    if(!empty($success_data['web-activation-success-title'])){
        $success_title = $success_data['web-activation-success-title']['text_'.$locale];
    }
    
    if(!empty($success_data['web-activation-success-image'])){
        $success_image = $success_data['web-activation-success-image']['text_'.$locale];
    }
    if(!empty($success_data['web-activation-success-button'])){
        $success_btn = $success_data['web-activation-success-button']['text_'.$locale];
    }
?>
<div class="modal fade" id="VIPActivated" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content congrulation">
            <div class="modal-body">
                <h4>{{$success_title}}</h4>
                <img src="{{$success_image}}" alt="" />
                <?php /* ?>
                <p>{{trans('web.pack.congrats')}} <span class="user-name">--</span>&nbsp;{{trans('web.pack.congrats_1')}}</p>
                <p>{{trans('web.pack.thank_msg_1')}} <span class="pack-type">--</span> <span class="pack-id">--</span> {{trans('web.pack.thank_msg_2')}}.</p>
				<p>{{trans('web.pack.thank_msg_3')}}</p>
                <?php */ ?>
                <div id="success_content"></div>
                <a class="pack-url" href="#"><button class="btn btn-theme">{{$success_btn}}</button></a>
            </div>
        </div>
    </div>
</div>


{{-- <div class="modal fade vip-pack-notavailable-popup" id="VIPErrorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content congrulation ">
            <div class="modal-body">
                @if($popup_image != '')
                <img src="{{$popup_image}}" alt="" />
                @endif
                @if($popup_title != '')
                    <h4>{{$popup_title}}</h4>
                @endif
               
                <p id="vip_disabled_error_message">{!! $message !!}</p>
                <button class="btn btn-theme" id="error_button_close">{{trans('content.ok')}}</button>
            </div>
        </div>
    </div>
</div> --}}

<div class="modal fade vip-pack-notavailable-popup" id="VIPErrorModal" tabindex="-1" aria-hidden="true" data-keyboard="false" @if($season->fixed_popup) data-backdrop="static" @endif>
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
           
            <div class="modal-body">   
                @if(!$season->fixed_popup)
                    <a href="javascript:;" data-dismiss="modal" class="close">
                        <svg width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.349067 19.5847C0.0645635 19.8857 0.0779769 20.3604 0.379026 20.6449C0.680076 20.9294 1.15476 20.916 1.43926 20.6149L0.349067 19.5847ZM10.4643 11.065C10.7488 10.764 10.7354 10.2893 10.4343 10.0048C10.1333 9.72029 9.65858 9.73371 9.37408 10.0348L10.4643 11.065ZM9.37412 10.0348C9.08962 10.3358 9.10303 10.8105 9.40408 11.095C9.70513 11.3795 10.1798 11.3661 10.4643 11.065L9.37412 10.0348ZM19.4893 1.51514C19.7738 1.21409 19.7604 0.739405 19.4594 0.454902C19.1583 0.170398 18.6836 0.183812 18.3991 0.484861L19.4893 1.51514ZM10.4643 10.0348C10.1798 9.73374 9.70513 9.72033 9.40408 10.0048C9.10303 10.2893 9.08962 10.764 9.37412 11.0651L10.4643 10.0348ZM18.3991 20.615C18.6836 20.916 19.1583 20.9294 19.4594 20.6449C19.7604 20.3604 19.7738 19.8857 19.4893 19.5847L18.3991 20.615ZM9.37408 11.065C9.65858 11.3661 10.1333 11.3795 10.4343 11.095C10.7354 10.8105 10.7488 10.3358 10.4643 10.0348L9.37408 11.065ZM1.43926 0.484861C1.15476 0.183812 0.680076 0.170398 0.379026 0.454902C0.0779769 0.739405 0.0645635 1.21409 0.349067 1.51514L1.43926 0.484861ZM1.43926 20.6149L10.4643 11.065L9.37408 10.0348L0.349067 19.5847L1.43926 20.6149ZM10.4643 11.065L19.4893 1.51514L18.3991 0.484861L9.37412 10.0348L10.4643 11.065ZM9.37412 11.0651L18.3991 20.615L19.4893 19.5847L10.4643 10.0348L9.37412 11.0651ZM10.4643 10.0348L1.43926 0.484861L0.349067 1.51514L9.37408 11.065L10.4643 10.0348Z" fill="white"></path>
                        </svg>
                    </a> 
                @endif            
               
                @if($popup_image != '')
                <div class="logo-wrapper">                    
                    <img src="{{ $popup_image }}" class="popup_logo">
                </div>
                @endif
                @if($popup_title != '')
                    <h4>{{$popup_title}}</h4>
                @endif
                <div class="off-popup-content" id="vip_disabled_error_message">
                    {!! $message !!}
                </div>                   
               
                <button data-dismiss="modal" class="btn btn-theme pay-card new-btn-theme-btn" onclick="window.location.href='https://www.globalvillage.ae/{{app()->getLocale()}}/register-interests'" id="error_button_close">{{trans('content.register_interests')}}</button>
                
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="validateOTP" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <a href="javascript:;" data-dismiss="modal" id="validate_modal_close"><i class="icon-cross"></i></a>
            <div class="modal-body">
                <h3></h3>
                <form id= "validate-otp">
                    <div class="form-group">
                        <label>{{trans('passwords.otp.popup_input')}}</label>
                        <input type="text" class="form-control removeSpaces" id="otp-input" placeholder="" required="required" />
            <div class="error"></div>
                    </div>
                    <button type="button" class="btn btn-theme w-100" id="Next">{{trans('passwords.otp.popup_btn')}}</button>
                </form>
         <div id="validate-otp-msg" class=" first-step-error" style="display: none;"></div>
            </div>
        </div>
    </div>
</div>
@include("web.addcar_popup_content")

@stop
@section("css")

<link rel="stylesheet" href="{{asset('web/css/intlTelInput.css')}}">
<style type="text/css">
.ajax-progress-throbber .throbber {
     display: inline;
       padding: 1px 5px 2px;
      background: transparent url("{{asset('web/images/throbber-active.gif')}}") no-repeat 0 center;
    }
   .ajax-progress-throbber .message {
      display: inline;
      padding: 1px 5px 2px;
     }
     #pre_loader_main{
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(192, 192, 192, 0.5);
        z-index: 99999 !important;
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .swal-modal{
        width: 40% !important;
    }

</style>
@stop
@section('js')

<script src="{{asset('web/js/intlTelInput.js')}}"></script>

<script type="text/javascript">

    @if(!$season->pack_activation_enabled || $show_register_popup)
        $('#VIPErrorModal').modal('show');
    @endif

    function showAlert(message){        
        //$("#vip_disabled_error_message").html(message);
        //$("#VIPErrorModal").show();
        $('#VIPErrorModal').modal('show');
    }
    function elqGetGuidCookieValue() {
        var name, value, index, cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            index = cookies[i].indexOf('=');
            if (index > 0 && cookies[i].length > index + 1) {
                name = cookies[i].substr(0, index).trim();
                if (name == 'ELOQUA') {
                    value = cookies[i].substr(index + 1);
                    var subCookies = value.split("&");
                    for (var l = 0; 1 < subCookies.length; l++) {
                        var subCookie = subCookies[l].split("=");
                        if (subCookie.length == 2 && subCookie[0] == 'GUID') {
                            return subCookie[1];
                        }
                    }
                }
            }
        }
        return '';
    }

    $('#DateofBirth').datepicker({
		uiLibrary: 'bootstrap4',
		format: 'dd-mm-yyyy',
		maxDate: function() {
            var date = new Date();
            date.setDate(date.getDate() - 3650);
            return new Date(date.getFullYear(), date.getMonth(), date.getDate());
        }
	});

    var car_activation_allowed = 0;
    var addedCar = 1;
    var setemirate = 0;
    var emirateId = '';
    var setPrefix = 0;
    var cPrefix = '';
    var uaeId = '{{$uaeId}}';
    var lastAddedCnt = 0;

    
    function showRemoveCarButton(){
    	//$(".removeCarSingle").show();
        //$(".removeCarSingle").first().hide();   

    }

    /*function addMoreCarFunction(){
    	//var copy = $('<div class="copy"></div>');
        //$('#template').append(copy.append(contents));
        $.ajax({
                url: "{!!route('addMoreCar',app()->getLocale())!!}",
                method: 'POST',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                data: {'lastAddedCnt': lastAddedCnt },
                beforeSend: function () {
                	$("#pre_loader_main").show();
                    
                },
                success: function (response) {
                	$("#pre_loader_main").hide();
                	console.log(response.status);
                    if (response.status) {
                    	$('#template').append(response.carhtml);
                    	lastAddedCnt++;
                    	if (addedCar >= car_activation_allowed) {
                    		$(".second-step").find('#AddCar').hide();
                    	}
                    }
                }
            });
        lastAddedCnt++;
    }*/
    var iti;
    $(document).ready(function () {
        $('.removeSpaces').on('paste', function(e) {
            // Prevent the default paste behavior
            e.preventDefault();
            
            // Get the text being pasted
            let pasteText = (e.originalEvent.clipboardData || window.clipboardData).getData('text');
            
            // Remove spaces from the pasted text
            pasteText = pasteText.replace(/\s+/g, '');

            // Insert the modified text
            document.execCommand('insertText', false, pasteText);
        });

    	$("#error_button_close").on("click",function(){
            $('#VIPErrorModal').modal('hide');
        });
        
    	var input = document.querySelector("#mobile_number");
	      iti = window.intlTelInput(input, {
	          separateDialCode: true,
	          utilsScript: "{{asset('web/js/utils.js')}}",
	          initialCountry : "ae",
	          preferredCountries : ["ae","us", "gb"]
	      });
	      input.addEventListener('countrychange', function(e) {
              var dialCode = iti.getSelectedCountryData().dialCode;
              $('#mobile_dial_code').val(dialCode)
              
          });
    	$(document).on("click",".removeCarSingle",function () {
            if (addedCar > 1) {
                addedCar--;
            }
            $(this).closest(".remcarMain").remove();
            $(".cars-allowed-error").html('').hide();
            showRemoveCarButton();
         //    if (addedCar < car_activation_allowed) {
        	// 	$(".second-step").find('#AddCar').show();
        	// }
            showAddCarButton();
        });
    	$("#packForm").on("keypress", function (event) {
            
            var keyPressed = event.keyCode || event.which;
            if (keyPressed === 13) {
               
                event.preventDefault();
                return false;
            }
        });
    	$("#residence").trigger("change");
    	$(document).on("change","#residence",function(){
    		if($(this).val() == uaeId){
    			$(".residence_emirate_div").show();
	    	}else{
	    		$(".residence_emirate_div").hide();
	    	}

    	})
        contents = $('#template').html();
        $("#AddCar").click(function () {
            addedCar++;
            if ($(".checkbox_car_list:checked").length >= car_activation_allowed) {
                //$(".cars-allowed-error").html('{{trans("web.pack.no_more_car_allowed")}}').show();
                
                //addedCar--;
                $(".checkbox_car_list:checked").first().prop('checked',false);
            }
            /* else {
                
                //$("#RemoveCar").show();
                //addMoreCarFunction();
                //showRemoveCarButton();
               
               
            }*/
             $("#load_model_content").modal('show');
        });
        $("#RemoveCar").click(function () {
            if (addedCar > 1) {
                addedCar--;
            }
            $(".copy").last().remove();
            $(".cars-allowed-error").html('').hide();
            if($(".carCountry").length == 1){
            	$("#RemoveCar").hide();
            }
        });

            $('#otp-input').on('input change', function() {
                if($(this).val() != '') {
                    $('#Next').prop('disabled', false);
                } else {
                    $('#Next').prop('disabled', true);
                }
            });


        $("#validate_modal_close").click(function(){
            $("#NextOtp").prop('disabled',false);
            $("#email").prop('disabled',false);
            $(".formelementDis").prop('disabled',false);
        });

        $("#NextOtp").click(function(e){
            $("#first-step-error").html('').hide();
            $("#validate-otp-msg").html('').hide();
            $("#otp-input").val('');
            $("#email").prop('disabled',true);
            $(".formelementDis").prop('disabled',true);
            var thisB = $(this);
            $.ajax({
                url: "{!!route('generateOtp',app()->getLocale())!!}",
                method: 'POST',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                data: {'vip_pack_number': $("#vip_pack_number").val(),
                 'activation_code': $("#activation_code").val(),
                 'email': $("#email").val()
               },
                beforeSend: function () {
                    $("#pre_loader_main").show();
                    thisB.attr('disabled', true);
                },
                success: function (response) {
                    
                    if (response.statusCode == 1000) {

                        $("#validateOTP").modal("show");
                        $("#Next").prop('disabled',true);
                    }else{
                        // console.log(response.errorMessage.show_alert);
                        // console.log(response.errorMessage.length);
                        $("#email").prop('disabled',false);
                        $(".formelementDis").prop('disabled',false);
                        if(response.errorMessage != null &&  response.errorMessage.show_alert){
                            showAlert(response.message);
                        }else{
                            $("#first-step-error").html(response.message).show();
                        }   
                        thisB.attr('disabled', false);                     
                    }
                    $("#pre_loader_main").hide();
                }
            });
        });
        $("#Next").click(function (e) {

        	 $("#email").prop('disabled',true);
        	 $(".formelementDis").prop('disabled',true);
            //if($("#vip_pack_number").val() > 20){
                //$(".second-step").find('#AddCar').hide();
           // }

            if($('#vip_pack_number').val() != '' && $('#activation_code').val() != ''  && $('#activaemailtion_code').val() != '') {
               
                 //push to datlayer
                   /* dataLayer.push(
                   {'event': 'activate_vip_page',
                        'eventCategory':'pack_details', 
                        'eventAction': 'next', 
                        'eventLabel': "{{url('')}}/{{app()->getLocale()}}", 
                        'pageType': 'activate_vip_page',
                        'Language': '{{app()->getLocale()}}'
                    }); */
               

               /*console.log(
                    {'event': 'activate_vip_page',
                            'eventCategory':'pack_details', 
                            'eventAction': 'next', 
                            'eventLabel': vurl , 
                            'pageType': 'activate_vip_page',
                            'Language': '{{app()->getLocale()}}'
                        }); */
            }

            function vip_pre_activate(){

                dataLayer.push(
                   {'event': 'activate_vip_page',
                        'eventCategory':'pack_details', 
                        'eventAction': 'next', 
                        'eventLabel': "{{url('')}}/{{app()->getLocale()}}", 
                        'pageType': 'activate_vip_page',
                        'Language': '{{app()->getLocale()}}'
                    });

              /*  console.log(
                     {'event': 'activate_vip_page',
                        'eventCategory':'pack_details', 
                        'eventAction': 'next', 
                        'eventLabel': "{{url('')}}/{{app()->getLocale()}}", 
                        'pageType': 'activate_vip_page',
                        'Language': '{{app()->getLocale()}}'
                    }); */

                    return true;
            }

            //Ajax to check the code availability
            var thisB = $(this);
            $(".agree_offer_div").show();
            $("#checkbox2").attr("checked",false);
            $.ajax({
                url: "{!!route('check.activationCode',app()->getLocale())!!}",
                method: 'POST',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                data: {'vip_pack_number': $("#vip_pack_number").val(),
                 'activation_code': $("#activation_code").val(),
                 'email': $("#email").val(),
                 'otp' : $("#otp-input").val()
               },
                beforeSend: function () {
                	$("#pre_loader_main").show();
                    thisB.attr('disabled', true);
                },
                success: function (response) {
                    if (response.status) {
                        
                        vip_pre_activate();
                        $("#validateOTP").modal("hide");
                        car_activation_allowed = response.car_activation_allowed;
                        $("#validate-otp-msg").html('').hide();
                        $(".action-button1").hide();
                        $(".second-step").show();
                        $("#user_otp").val(response.otp_id);
                        if(typeof response.user.first_name !== 'undefined'){
                            if(response.user.promotion){
                                $(".agree_offer_div").hide();
                                $("#checkbox2").attr("checked",true);
                            }
                        	$("#nationality").val('');
                        	var usr = response.user;
                        	$("#first_name").val(usr.first_name);
                        	$("#last_name").val(usr.last_name);
                        	//$("#mobile_dial_code").val(usr.mobile_dial_code);
                        	iti.setNumber(usr.user_mobile);

                        	$("#mobile_number").trigger("change");
                        	$("#DateofBirth").val(usr.dob);
                        	
                        	if(usr.gender == 'male'){
                        		$("#male").prop('checked',true);
                        	}else if(usr.gender == 'female'){
                        		$("#female").prop('checked',true);
                        	}
                        	$("#nationality").val(usr.nationality_id);
	                        $("#residence").val(usr.residence_id);
	                        $("#emirate_residence").val(usr.emirate_id);
	                        if(usr.residence_id == uaeId){
				    			$(".residence_emirate_div").show();
					    	}else{
					    		$(".residence_emirate_div").hide();
					    	}
                        	/*if(usr.vip_user == 'yes'){
	                        	

	                        	if(response.car != null && typeof response.car.country_id !== 'undefined'){
	                        		var car = response.car;
	                        		$("#car_country").val(car.country_id);
	                        		$("#car_type").val(car.type);
	                        		$("#plate_number").val(car.plate_number);
	                        		if(car.emirate_id != "" && car.emirate_id != null){
	                        			setemirate = 1;
	                        			emirateId = car.emirate_id;
	                        			$("#car_emirate").html(response.emirate_html);
	                        			$("#car_emirate").val(car.emirate_id);
	                        			$("#plate_prefix").html(response.prefix_html);
	                        			$("#plate_prefix").val(car.plate_prefix);
	                        		}else{
	                        			$("#plate_prefix").html(response.prefix_html);
	                        			$("#plate_prefix").val(car.plate_prefix);
	                        			$("#car_emirate").closest('.caremDiv').hide();
	                        			
	                        		}
	                        		
	                        		setPrefix = 1;
	                        		cPrefix = car.plate_prefix;

	                        		$('#template').append(response.carhtml);
	                        		 addedCar  = response.carCount;

	                        		 if(response.carCount > 1){
	                        		 	//$("#RemoveCar").show();
	                        		 	
	                        		 }
	                        	}
	                        }*/
                        }
                        //$("#reg_car_sub_title").hide();
                        //if(response.carhtml != ''){
                        $("#reg_car_sub_title").html(response.add_car_text);
                        //}
                        $('#template').append(response.carhtml);
                        addedCar  = response.carCount;
                        lastAddedCnt  = response.carCount;
                        lastAddedCnt++;
                        /*if (addedCar < car_activation_allowed) {
                        	$(".second-step").find('#AddCar').show();
                        }

                        if (addedCar >= car_activation_allowed) {
                    		$(".second-step").find('#AddCar').hide();
                    	}*/
                        $(".second-step").find('#AddCar').show();
                        
                    } else {
                        $("#validate-otp-msg").html(response.message).show();
                        
                       
                        //$("#email").prop('disabled',false);
                        //$(".formelementDis").prop('disabled',false);
                    }
                },
                error: function (response) {
                    $("#validate-otp-msg").html('Something went wrong!').show();
                    
                    //$("#email").prop('disabled',false);
                    //$(".formelementDis").prop('disabled',false);
                    $("#pre_loader_main").hide();
                },
                complete: function () {
                    thisB.attr('disabled', false);
                    $("#pre_loader_main").hide();
                }
            });
        });
        /*$(document).on('change', '.carCountry', function (e) {
            var countryId = $(this).val();
            var caremDiv = $(this).parent().parent().parent().parent('div').find('.caremDiv');
            var ediv = $(this).parent().parent().parent().parent('div').find('.carEmirate');
            var ndiv = $(this).parent().parent().parent().parent('div').find('.carNumber');
            if (countryId != '') {
                ediv.html('<option value="">Loading....</option>');
                //get Emirates
                $.ajax({
                    url: "{!! route('get.emirates',app()->getLocale()) !!}",
                    dataType: 'html',
                    data: {'id': countryId},
                    cache: false,
                    method: 'POST',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    beforeSend: function () {
                    },
                    success: function (response) {
                    	$(caremDiv).hide();
                    	if(response != ''){
                    		$(caremDiv).show();
                    		ediv.html(response);
                    		if(setemirate){
                    			ediv.val(emirateId).trigger('change');
                    		}
                    		setemirate = 0;
                    		emirateId = '';
                    	}
                        
                    },
                    error: function (response) {
                    },
                    complete: function () {
                    	if (countryId == 1) {
		                    ndiv.html('<option value="">{{trans("web.pack.plate_number_placeholder")}}</option>');
		                } else {
		                    ndiv.html('<option value="">Loading....</option>');
		                    //get plate number Prefix
		                    listPrefix('country', countryId, ndiv);
		                }
                    }
                });
                
            } else {
                ediv.html('<option value="">{{trans("web.pack.car_emirate_placeholder")}}</option>');
                ndiv.html('<option value="">{{trans("web.pack.plate_number_placeholder")}}</option>');
            }
        });
        $(document).on('change', '.carEmirate', function (e) {
            var emirateId = $(this).val();
            var ndiv = $(this).parent().parent().parent().parent('div').find('.carNumber');
            if (emirateId != '') {
                ndiv.html('<option value="">Loading....</option>');
                listPrefix('city', emirateId, ndiv);
            } else {
                ndiv.html('<option value="">{{trans("web.pack.plate_number_placeholder")}}</option>');
            }
        });*/

        $(document).on('change', '.carCountry', function (e) {
            var countryId = $(this).val();
            var ediv = $(this).parent().parent().parent('div').find('.carEmirate');
            var ndiv = $(this).parent().parent().parent('div').find('.carNumber');
            if (countryId != '') {
                ediv.html('<option value="">Loading....</option>');
                //get Emirates
                $.ajax({
                    url: "{!! route('get.emirates',app()->getLocale()) !!}",
                    dataType: 'html',
                    data: {'id': countryId},
                    cache: false,
                    method: 'POST',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    beforeSend: function () {
                    },
                    success: function (response) {
                        $("#emirate_pop_div").hide();
                        if(response != ''){
                            ediv.html(response);
                            $("#emirate_pop_div").show();
                        }
                    },
                    error: function (response) {
                    },
                    complete: function () {
                    }
                });
                if (countryId == 1) {
                    ndiv.html('<option value="">{{trans("web.pack.plate_number_placeholder")}}</option>');
                } else {
                    ndiv.html('<option value="">Loading....</option>');
                    //get plate number Prefix
                    listPrefix('country', countryId, ndiv);
                }
            } else {
                ediv.html('<option value="">{{trans("web.pack.car_emirate_placeholder")}}</option>');
                ndiv.html('<option value="">{{trans("web.pack.plate_number_placeholder")}}</option>');
            }
        });
        $(document).on('change', '.carEmirate', function (e) {
            var emirateId = $(this).val();
            var ndiv = $(this).parent().parent().parent('div').find('.carNumber');
            if (emirateId != '') {
                ndiv.html('<option value="">Loading....</option>');
                listPrefix('city', emirateId, ndiv);
            } else {
                ndiv.html('<option value="">{{trans("web.pack.plate_number_placeholder")}}</option>');
            }
        });
        $(document).on('submit', 'form.packForm', function (e) {



            //var postData = $('#packForm').serialize();
            $("#email").prop('disabled',false);
            $("#pre_loader_main").show();
            $(".formelementDis").prop('disabled',false);
            $(".checkbox_car_list").prop('disabled',false);
            e.preventDefault();
            var postData = new FormData(this);
            postData.append('elqCustomerGUID', elqGetGuidCookieValue());
            var thisB = $(this).find('button.packSubmit');
            thisB.attr('disabled', true);
            $.ajax({
                url: "{!!route('pack.activate',app()->getLocale())!!}",
                processData: false,
                contentType: false,
                type: 'POST',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                data: postData,
                beforeSend: function () {
                    thisB.attr('disabled', true);
                    $("#pre_loader_main").show();
                },
                success: function (response) {
                    if (response.status) {

                        //push to datlayer
                        
                        dataLayer.push(
                            {'event': 'activate_vip_page',
                                    'eventCategory':'pack_details', 
                                    'eventAction': 'activate_vip_pack', 
                                    'eventLabel': "{{url('')}}/{{app()->getLocale()}}", 
                                    'pageType': 'activate_vip_page',
                                    'Language': '{{app()->getLocale()}}'
                                }); 

                        //show modal
                        var pckUrl = response.data.pack_url;
                        @if(request()->source)
                            pckUrl = pckUrl+'?source={{request()->source}}';
                        @endif    
                        //$('#VIPActivated').find('span.user-name').html(response.data.name);
                        //$('#VIPActivated').find('span.pack-type').html(response.data.pack_type);
                        //$('#VIPActivated').find('span.pack-id').html(response.data.pack_id);
                        $('#VIPActivated').find('#success_content').html(response.data.success_message);
                        $('#VIPActivated').find('a.pack-url').attr('href', pckUrl);
                        $('#VIPActivated').modal('show');
                    } else {
                        //check the response have car_activation_allowed
                        if (response.hasOwnProperty('car_activation_allowed')) {
                            car_activation_allowed = response.car_activation_allowed;
                        }
                        $(".second-step-error").html(response.message).show();
                       
                        $("#email").prop('disabled',true);
                        grecaptcha.reset();

                    }
                },
                error: function (response) {
                    $(".second-step-error").html('Something went wrong!').show();
                    
                    $("#email").prop('disabled',true);
                    $("#pre_loader_main").hide();
                },
                complete: function () {
                	$("#pre_loader_main").hide();
                    thisB.attr('disabled', false);
                }
            });
        });

        $(document).on("click","#Cancel",function(){
        	$("#email").prop('disabled',false);
        	$(".formelementDis").prop('disabled',false);
        	$('#packForm')[0].reset();
        	$("#first-step").show();
        	$("#second-step").hide();
        	$(".copy").remove();
        	$(".action-button1").show();
    	     $(".second-step").hide();
    	     $('#template').html('');
        	addedCar = 1;
            grecaptcha.reset();
        });

        $("#load_model_content").on("hidden.bs.modal",function(){
            $("#carForm")[0].reset();
        });

        $("#sub_car_detail_btn").on("click", function(event){
            event.preventDefault();
            addMoreCarFunction();
        });

        $(document).on("click", ".checkbox_car_list", function(){
            $(".cars-allowed-error").hide();
            if($(".checkbox_car_list:checked").length > car_activation_allowed){
               
                //$(this).prop('checked',false);
                //$(".cars-allowed-error").html('{{trans("web.pack.no_more_car_allowed")}}').show();
                $(".checkbox_car_list:checked").not(this).first().prop('checked',false);
            }
            showAddCarButton();
        });
    });
function showAddCarButton(){
    // if($(".checkbox_car_list:checked").length >= car_activation_allowed){
    //     $(".second-step").find('#AddCar').hide();
    // }else{
    //     $(".second-step").find('#AddCar').show();
    // }
}
function addMoreCarFunction(){
        var car_type = $(".cartype_class:checked").val();
        var car_country = $("#popup_car_country").val();
        var emirate = $("#popup_emirate").val();
        var plate_prefix = $("#popup_plate_prefix").val();
        var plate_number = $("#popup_plate_number").val();
        
        if(car_type == '' || car_country == '' || plate_prefix == '' || plate_number == '' || car_type == undefined){
            $(".second-step-error-popup").show();
            return false;
        }

        $.ajax({
                url: "{!!route('addMoreCar',app()->getLocale())!!}",
                method: 'POST',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                data: {'lastAddedCnt': lastAddedCnt, car_type : car_type, car_country: car_country,  emirate : emirate, plate_prefix : plate_prefix, plate_number : plate_number},
                beforeSend: function () {
                    $("#pre_loader_main").show();
                    
                },
                success: function (response) {
                    $("#pre_loader_main").hide();
                    
                    if (response.status) {
                        $('#template').append(response.carhtml);
                        lastAddedCnt++;
                        $("#load_model_content").modal('hide');
                        showAddCarButton();
                        // if (addedCar >= car_activation_allowed) {
                        //     $(".second-step").find('#AddCar').hide();
                        // }
                    }
                }
            });
        lastAddedCnt++;
}

    function listPrefix(type, id, ndiv) {
        $.ajax({
            url: "{!! route('get.platePrefix',app()->getLocale()) !!}",
            dataType: 'html',
            data: {'id': id, 'type': type},
            cache: false,
            method: 'POST',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            beforeSend: function () {
            },
            success: function (response) {
                ndiv.html(response);
                if(setPrefix){
                	ndiv.val(cPrefix);
                }
                setPrefix = 0;
                cPrefix = '';
            },
            error: function (response) {
            },
            complete: function () {
            }
        });
    }
</script>
<script>
$('.iconIfnoImage').popover({
	html: true,
	trigger: 'hover',
	placement: 'bottom',
	content: function () {
			return '<img src="' + $(this).data('img') + '" class="w-100" />'; 
	}
});
$('.popoverContent').popover({
	trigger: 'hover',
});

$('#confemail').on("cut copy paste",function(e) {
	e.preventDefault();
});

function confirmEmail() {
	var email = document.getElementById("email").value
	var confemail = document.getElementById("confemail").value
	if(email != confemail) {
		$('.errorTxt').html("{{trans('web.email_mismatch_error')}}");
	}else{
    $('.errorTxt').html('');
  }
}

function mobilenumberOnly(id) {
    // Get element by id which passed as parameter within HTML element event
    var element = document.getElementById(id);
    // Use numbers only pattern, from 0 to 9
    var regex = /[^0-9]/gi;
    // This removes any other character but numbers as entered by user
    element.value = element.value.replace(regex, "");
}

function platenumberOnly(id) {
    // Get element by id which passed as parameter within HTML element event
    var element = document.getElementById(id);
    // Use numbers only pattern, from 0 to 9
    var regex = /[^0-9]/gi;
    // This removes any other character but numbers as entered by user
    element.value = element.value.replace(regex, "");
}
</script>



@stop
