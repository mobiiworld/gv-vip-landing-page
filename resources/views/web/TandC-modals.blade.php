@for($i=1;$i<=17;$i++)
<div class="modal fade" id="termsModal-{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content offers">
            <a href="javascript:;" class="cross" data-dismiss="modal"><i class="icon-cross"></i></a>
            @if(View::exists('web.terms.terms-'.$i))
            @include('web.terms.terms-'.$i)
            @endif
        </div>
    </div>
</div>
@endfor
