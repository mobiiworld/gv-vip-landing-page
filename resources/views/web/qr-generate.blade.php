@if(!empty($code))
@php($img = base64_encode(QrCode::format('svg')->size(300)->generate($code)))
<div class="qr-code">
    <img src="data:image/svg+xml;base64, {!! $img !!}">
    <br>
    <br>
    <br>
    <b>{{$code}}</b>
</div>
@endif
