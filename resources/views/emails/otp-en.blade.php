<!DOCTYPE html
   PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office"
   style="width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0">
   <head>
      <meta charset="UTF-8">
      <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
      <meta content="width=device-width, initial-scale=1" name="viewport">
      <meta name="x-apple-disable-message-reformatting">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="telephone=no" name="format-detection">
      <title>headline</title>
      <!--[if (mso 16)]>
      <style type="text/css">
         a {text-decoration: none;}
      </style>
      <![endif]--> 
      <!--[if gte mso 9]>
      <style>sup { font-size: 100% !important; }</style>
      <![endif]--> 
      <!--[if gte mso 9]>
      <xml>
         <o:OfficeDocumentSettings>
            <o:AllowPNG></o:AllowPNG>
            <o:PixelsPerInch>96</o:PixelsPerInch>
         </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
      <style type="text/css">
         @import url('https://fonts.googleapis.com/css2?family=Poppins&display=swap');
         [style*="Poppins"] {
         font-family: 'Poppins', Arial, sans-serif !important
         }
         #outlook a {
         padding: 0;
         }
         .ExternalClass {
         width: 100%;
         }
         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
         line-height: 100%;
         }
         .es-button {
         mso-style-priority: 100 !important;
         text-decoration: none !important;
         }
         a[x-apple-data-detectors] {
         color: inherit !important;
         text-decoration: none !important;
         font-size: inherit !important;
         font-family: inherit !important;
         font-weight: inherit !important;
         line-height: inherit !important;
         }
         .es-desk-hidden {
         display: none;
         float: left;
         overflow: hidden;
         width: 0;
         max-height: 0;
         line-height: 0;
         mso-hide: all;
         }
         @media only screen and (max-width:600px) {
         p, ul li, ol li, a {
         font-size: 11px !important;
         line-height: 150% !important
         }
         h1 {
         font-size: 23px !important;
         text-align: center;
         line-height: 120% !important
         }
         h2 {
         font-size: 14px !important;
         text-align: center;
         line-height: 120% !important
         }
         h3 {
         font-size: 11px !important;
         text-align: center;
         line-height: 120% !important
         }
         h1 a {
         font-size: 23px !important
         }
         h2 a {
         font-size: 14px !important
         }
         h3 a {
         font-size: 11px !important
         }
         .es-menu td a {
         font-size: 16px !important
         }
         .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a {
         font-size: 16px !important
         }
         .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a {
         font-size: 11px !important
         }
         .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a {
         font-size: 12px !important
         }
         *[class="gmail-fix"] {
         display: none !important
         }
         .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 {
         text-align: center !important
         }
         .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 {
         text-align: right !important
         }
         .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 {
         text-align: left !important
         }
         .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img {
         display: inline !important
         }
         .es-button-border {
         display: inline-block !important
         }
         a.es-button {
         font-size: 16px !important;
         display: inline-block !important
         }
         .es-btn-fw {
         border-width: 10px 0px !important;
         text-align: center !important
         }
         .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right {
         width: 100% !important
         }
         .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header {
         width: 100% !important;
         max-width: 600px !important
         }
         .es-adapt-td {
         display: block !important;
         width: 100% !important
         }
         .adapt-img {
         width: 100% !important;
         height: auto !important
         }
         .es-m-p0 {
         padding: 0px !important
         }
         .es-m-p0r {
         padding-right: 0px !important
         }
         .es-m-p0l {
         padding-left: 0px !important
         }
         .es-m-p0t {
         padding-top: 0px !important
         }
         .es-m-p0b {
         padding-bottom: 0 !important
         }
         .es-m-p20b {
         padding-bottom: 20px !important
         }
         .es-mobile-hidden, .es-hidden {
         display: none !important
         }
         tr.es-desk-hidden, td.es-desk-hidden, table.es-desk-hidden {
         width: auto !important;
         overflow: visible !important;
         float: none !important;
         max-height: inherit !important;
         line-height: inherit !important
         }
         tr.es-desk-hidden {
         display: table-row !important
         }
         table.es-desk-hidden {
         display: table !important
         }
         td.es-desk-menu-hidden {
         display: table-cell !important
         }
         .es-menu td {
         width: 1% !important
         }
         table.es-table-not-adapt, .esd-block-html table {
         width: auto !important
         }
         table.es-social {
         display: inline-block !important
         }
         table.es-social td {
         display: inline-block !important
         }
         }
      </style>
   </head>
   <body
      style="width:100%;font-family:arial, 'Poppins', Arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#F0F0F0; padding:0;Margin:0">
      <div class="es-wrapper-color" style="background-color:#F0F0F0">
         <!--[if gte mso 9]>
         <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
            <v:fill type="tile" color="#f0f0f0"></v:fill>
         </v:background>
         <![endif]-->
         <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0"
            style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top">
            <tr style="border-collapse:collapse">
               <td valign="top" style="padding:0;Margin:0">
                  <table class="es-content" cellspacing="0" cellpadding="0" align="center"
                     style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
                     <tbody>
                        <tr style="border-collapse:collapse">
                           <td align="center" style="padding:0;Margin:0">
                              <table class="es-content-body" cellspacing="0" cellpadding="0" align="center"
                                 style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px">
                                 <tbody>
                                    <tr style="border-collapse:collapse">
                                       <td align="left" bgcolor="#ffffff"
                                          style="Margin:0;padding-top:10px;padding-bottom:15px;padding-left:20px;padding-right:20px;background-color:#FFFFFF">
                                          <!--[if mso]>
                                          <table style="width:560px" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td style="width:270px" valign="top">
                                                   <![endif]-->
                                                   <table cellpadding="0" cellspacing="0" class="es-left" align="left"
                                                      style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
                                                      <tbody>
                                                         <tr style="border-collapse:collapse">
                                                            <td align="left" style="padding:0;Margin:0;width:270px">
                                                               <table cellpadding="0" cellspacing="0" width="100%"
                                                                  role="presentation"
                                                                  style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                                  <tbody>
                                                                     <tr style="border-collapse:collapse">
                                                                        <td class="es-m-txt-c" align="left"
                                                                           style="padding:0;Margin:0;padding-left:10px;padding-right:10px">
                                                                           <h3
                                                                              style="Margin:0;line-height:13px;mso-line-height-rule:exactly;font-family:arial, 'Poppins';font-size:11px;font-style:normal;font-weight:normal;color:#333333"> Not seeing images? <a
                                                                              target="_blank"
                                                                              style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'Poppins';font-size:11px;text-decoration:underline;color:#333333"
                                                                              href="#">View online</a> </h3>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                   <!--[if mso]>
                                                </td>
                                                <td style="width:20px"></td>
                                                <td style="width:270px" valign="top">
                                                   <![endif]-->
                                                   <table cellpadding="0" cellspacing="0" class="es-right"
                                                      align="right"
                                                      style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right">
                                                      <tbody>
                                                         <tr style="border-collapse:collapse">
                                                            <td align="left" style="padding:0;Margin:0;width:270px">
                                                               <table cellpadding="0" cellspacing="0" width="100%"
                                                                  role="presentation"
                                                                  style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                                  <tbody>
                                                                     <tr style="border-collapse:collapse">
                                                                        <td dir="rtl" class="es-m-txt-c"
                                                                           align="right"
                                                                           style="padding:0;Margin:0;padding-left:10px;padding-right:10px">
                                                                           <h3
                                                                              style="Margin:0;line-height:13px;mso-line-height-rule:exactly;font-family:arial, 'Poppins';font-size:11px;font-style:normal;font-weight:normal;color:#333333"> تفضل النسخة العربية؟ <a
                                                                              target="_blank"
                                                                              style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'Poppins';font-size:11px;text-decoration:underline;color:#333333"
                                                                              href="#">انقر هنا</a></h3>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                   <!--[if mso]>
                                                </td>
                                             </tr>
                                          </table>
                                          <![endif]-->
                                       </td>
                                    </tr>
                                    <tr style="border-collapse:collapse">
                                       <td align="left"
                                          style="padding:0;Margin:0;padding-bottom:10px;padding-left:20px;padding-right:20px">
                                          <table cellpadding="0" cellspacing="0" width="100%"
                                             style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                             <tbody>
                                                <tr style="border-collapse:collapse">
                                                   <td align="center" valign="top"
                                                      style="padding:0;Margin:0;width:560px">
                                                      <table cellpadding="0" cellspacing="0" width="100%"
                                                         role="presentation"
                                                         style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                         <tbody>
                                                            <tr style="border-collapse:collapse">
                                                               <td align="center"
                                                                  style="padding:0;Margin:0;font-size:0px"><img src="https://globalvillage.ae/sites/business/themes/custom/global_village_business/images/logos/email-logo.png"
                                                                  alt=""
                                                                  style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                  width="278" class="adapt-img"></td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <!-- Always on Highlight--> 
                  <!-- Block Main Highlight-->
                  <table align="center" cellpadding="0" cellspacing="0" class="es-content"
                     style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
                     <tbody>
                        <tr style="border-collapse:collapse">
                           <td align="center" style="padding:0;Margin:0">
                              <table align="center" cellpadding="0" cellspacing="0" class="es-content-body"
                                 style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px">
                                 <!--Main content-->
                                 <tbody>
                                    <tr style="border-collapse:collapse">
                                       <td align="left" bgcolor="#ffffff"
                                          style="padding:0;Margin:0;background-color:#FFFFFF">
                                          <!-- Always on Highlight-->
                                          <table cellpadding="0" cellspacing="0"
                                             style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"
                                             width="100%">
                                             <tbody>
                                                <tr style="border-collapse:collapse">
                                                   <td align="center"
                                                      style="padding:0;Margin:0;width:600px" valign="top">
                                                      <table cellpadding="0" cellspacing="0"
                                                         style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"
                                                         width="100%">
                                                         <tbody>
                                                            <tr style="border-collapse:collapse">
                                                               <td align="center"
                                                                  style="padding:0;Margin:0;font-size:0px"><img alt="" class="adapt-img"
                                                                  src="https://globalvillage.ae/themes/custom/global_village/images/email-image.jpg"
                                                                  style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                  width="600" /></td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                    <tr style="border-collapse:collapse">
                                       <td align="left" bgcolor="#ffffff"
                                          style="padding:0;Margin:0;padding-top:20px;background-color:#FFFFFF">
                                          <table cellpadding="0" cellspacing="0"
                                             style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"
                                             width="100%">
                                             <tbody>
                                                <tr style="border-collapse:collapse">
                                                   <td align="center"
                                                      style="padding:0;Margin:0;width:600px" valign="top">
                                                      <table cellpadding="0" cellspacing="0"
                                                         style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"
                                                         width="100%">
                                                         <tbody>
                                                            <tr style="border-collapse:collapse">
                                                               <td align="center"
                                                                  style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px">
                                                                  <h1
                                                                     style="Margin:0;line-height:30px;mso-line-height-rule:exactly;font-family:arial, 'Poppins';font-size:25px;font-style:normal;font-weight:normal;color:#333333"> {!!  trans('passwords.otp.title')  !!}</h1>
                                                                  <br />
                                                                  <br />
                                                               </td>
                                                            </tr>
                                                            <tr style="border-collapse:collapse">
                                                               <td align="center"
                                                                  style="padding:0;margin:0;padding-left:15px;padding-right:15px">
                                                                  <h2
                                                                     style="margin:0;line-height:17px;mso-line-height-rule:exactly;font-family:arial, 'Poppins';font-size:14px;font-style:normal;font-weight:normal;color:#333333;text-align:left;"> {!!  trans('passwords.otp.content',['user_otp'=>$data['otp']])  !!}</h2>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <table align="center" cellpadding="0" cellspacing="0" class="es-content"
                     style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
                     <tbody>
                        <tr style="border-collapse:collapse">
                           <td align="center" style="padding:0;Margin:0">
                              <table align="center" bgcolor="#ffffff" cellpadding="0" cellspacing="0"
                                 class="es-content-body"
                                 style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px">
                                 <tbody>
                                    <tr style="border-collapse:collapse">
                                       <td align="left" bgcolor="#ffffff"
                                          style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;background-color:#FFFFFF">
                                          <table cellpadding="0" cellspacing="0"
                                             style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"
                                             width="100%">
                                             <tbody>
                                                <tr style="border-collapse:collapse">
                                                   <td align="center"
                                                      style="padding:0;Margin:0;width:600px" valign="top"></td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <!-- EO Block--> 
                  <!-- Footer -->
                  <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0"
                     style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top">
                     <tbody>
                        <tr style="border-collapse:collapse">
                           <td valign="top" style="padding:0;Margin:0">
                              <table align="center" cellpadding="0" cellspacing="0" class="es-content"
                                 style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
                                 <tbody>
                                    <tr style="border-collapse:collapse">
                                       <td align="center" style="padding:0;Margin:0" class="">
                                          <table class="es-content es-mobile-hidden" cellspacing="0"
                                             cellpadding="0" align="center"
                                             style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
                                             <tbody>
                                                <tr style="border-collapse:collapse">
                                                   <td align="center" style="padding:0;Margin:0;">
                                                      <table class="es-content-body" cellspacing="0"
                                                         cellpadding="0" bgcolor="#ffffff" align="center"
                                                         style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px">
                                                         <tbody>
                                                            <tr style="border-collapse:collapse">
                                                               <td align="left" bgcolor="#034a82"
                                                                  style="padding:5px;Margin:0;background-color:#034A82">
                                                                  <!--[if mso]>
                                                                  <table style="width:580px" cellpadding="0" cellspacing="0">
                                                                     <tr>
                                                                        <td style="width:98px" valign="top">
                                                                           <![endif]-->
                                                                           <table cellpadding="0"
                                                                              cellspacing="0" class="es-left"
                                                                              align="left"
                                                                              style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
                                                                              <tbody>
                                                                                 <tr
                                                                                    style="border-collapse:collapse">
                                                                                    <td class="es-m-p0r"
                                                                                       align="center"
                                                                                       style="padding:0;Margin:0;width:90px">
                                                                                       <table
                                                                                          cellpadding="0"
                                                                                          cellspacing="0"
                                                                                          width="100%"
                                                                                          role="presentation"
                                                                                          style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                                                          <tbody>
                                                                                             <tr
                                                                                                style="border-collapse:collapse">
                                                                                                <td align="center"
                                                                                                   style="padding:0;Margin:0;font-size:0px"><a target="_blank"
                                                                                                   href="https://www.globalvillage.ae/plan-your-visit/?elqTrackId=7c537117e1464931bb7464a73e0c3c88&elqTrack=true"
                                                                                                   style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Poppins, Arial, sans-serif;font-size:14px;text-decoration:underline;color:#0B5394"><img
                                                                                                   src="https://images.visit.globalvillage.ae/EloquaImages/clients/DubaiHoldingLLC/%7B6920a06c-7747-4adb-b614-16ad3f7891f2%7D_footer-p1.jpg"
                                                                                                   alt=""
                                                                                                   style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                                                   width="90"></a></td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </td>
                                                                                    <td class="es-hidden"
                                                                                       style="padding:0;Margin:0;width:8px"></td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                           <!--[if mso]>
                                                                        </td>
                                                                        <td style="width:98px" valign="top">
                                                                           <![endif]-->
                                                                           <table cellpadding="0"
                                                                              cellspacing="0" class="es-left"
                                                                              align="left"
                                                                              style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
                                                                              <tbody>
                                                                                 <tr
                                                                                    style="border-collapse:collapse">
                                                                                    <td align="center"
                                                                                       style="padding:0;Margin:0;width:90px">
                                                                                       <table
                                                                                          cellpadding="0"
                                                                                          cellspacing="0"
                                                                                          width="100%"
                                                                                          role="presentation"
                                                                                          style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                                                          <tbody>
                                                                                             <tr
                                                                                                style="border-collapse:collapse">
                                                                                                <td align="center"
                                                                                                   style="padding:0;Margin:0;font-size:0px"><a target="_blank"
                                                                                                   href="https://www.globalvillage.ae/en/pavilions-selfie-spots?elqTrackId=c782ce3a6592496aaecbbed2cf2c488c&elqTrack=true"
                                                                                                   style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Poppins, Arial, sans-serif;font-size:14px;text-decoration:underline;color:#0B5394"><img
                                                                                                   src="https://images.visit.globalvillage.ae/EloquaImages/clients/DubaiHoldingLLC/%7Bac38883c-a140-41f4-b4c0-d3fac294b6ca%7D_footer-p2.jpg"
                                                                                                   alt=""
                                                                                                   style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                                                   width="90"></a></td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </td>
                                                                                    <td class="es-hidden"
                                                                                       style="padding:0;Margin:0;width:8px"></td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                           <!--[if mso]>
                                                                        </td>
                                                                        <td style="width:98px" valign="top">
                                                                           <![endif]-->
                                                                           <table cellpadding="0"
                                                                              cellspacing="0" class="es-left"
                                                                              align="left"
                                                                              style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
                                                                              <tbody>
                                                                                 <tr
                                                                                    style="border-collapse:collapse">
                                                                                    <td align="center"
                                                                                       style="padding:0;Margin:0;width:90px">
                                                                                       <table
                                                                                          cellpadding="0"
                                                                                          cellspacing="0"
                                                                                          width="100%"
                                                                                          role="presentation"
                                                                                          style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                                                          <tbody>
                                                                                             <tr
                                                                                                style="border-collapse:collapse">
                                                                                                <td align="center"
                                                                                                   style="padding:0;Margin:0;font-size:0px"><a target="_blank"
                                                                                                   href="https://www.globalvillage.ae/en/shows-events?elqTrackId=06a2a3a895564f898cc67460bed2dc8a&elqTrack=true"
                                                                                                   style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Poppins, Arial, sans-serif;font-size:14px;text-decoration:underline;color:#0B5394"><img
                                                                                                   src="https://images.visit.globalvillage.ae/EloquaImages/clients/DubaiHoldingLLC/%7Bd3609310-de39-4a75-9cd6-5498f441f8bd%7D_footer-p3.jpg"
                                                                                                   alt=""
                                                                                                   style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                                                   width="90"></a></td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </td>
                                                                                    <td class="es-hidden"
                                                                                       style="padding:0;Margin:0;width:8px"></td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                           <!--[if mso]>
                                                                        </td>
                                                                        <td style="width:98px" valign="top">
                                                                           <![endif]-->
                                                                           <table cellpadding="0"
                                                                              cellspacing="0" class="es-left"
                                                                              align="left"
                                                                              style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
                                                                              <tbody>
                                                                                 <tr
                                                                                    style="border-collapse:collapse">
                                                                                    <td align="center"
                                                                                       style="padding:0;Margin:0;width:90px">
                                                                                       <table
                                                                                          cellpadding="0"
                                                                                          cellspacing="0"
                                                                                          width="100%"
                                                                                          role="presentation"
                                                                                          style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                                                          <tbody>
                                                                                             <tr
                                                                                                style="border-collapse:collapse">
                                                                                                <td align="center"
                                                                                                   style="padding:0;Margin:0;font-size:0px"><a target="_blank"
                                                                                                   href="https://www.globalvillage.ae/en/dining?elqTrackId=458a1dd4d4ea427e908ea058a59fda92&elqTrack=true"
                                                                                                   style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Poppins, Arial, sans-serif;font-size:14px;text-decoration:underline;color:#0B5394"><img
                                                                                                   src="https://images.visit.globalvillage.ae/EloquaImages/clients/DubaiHoldingLLC/%7B8e8fe94a-e1d0-4d75-982d-5ebff45674a0%7D_footer-p4.jpg"
                                                                                                   alt=""
                                                                                                   style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                                                   width="90"></a></td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </td>
                                                                                    <td class="es-hidden"
                                                                                       style="padding:0;Margin:0;width:8px"></td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                           <!--[if mso]>
                                                                        </td>
                                                                        <td style="width:90px" valign="top">
                                                                           <![endif]-->
                                                                           <table cellpadding="0"
                                                                              cellspacing="0" class="es-left"
                                                                              align="left"
                                                                              style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
                                                                              <tbody>
                                                                                 <tr
                                                                                    style="border-collapse:collapse">
                                                                                    <td align="left"
                                                                                       style="padding:0;Margin:0;width:90px">
                                                                                       <table
                                                                                          cellpadding="0"
                                                                                          cellspacing="0"
                                                                                          width="100%"
                                                                                          role="presentation"
                                                                                          style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                                                          <tbody>
                                                                                             <tr
                                                                                                style="border-collapse:collapse">
                                                                                                <td align="center"
                                                                                                   style="padding:0;Margin:0;font-size:0px"><a target="_blank"
                                                                                                   href="https://www.globalvillage.ae/en/carnaval?elqTrackId=436dec1404b143cd8bc51c3b76dba0e2&elqTrack=true"
                                                                                                   style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Poppins, Arial, sans-serif;font-size:14px;text-decoration:underline;color:#0B5394"><img
                                                                                                   src="https://images.visit.globalvillage.ae/EloquaImages/clients/DubaiHoldingLLC/%7B6ac12643-d06b-4555-bcf3-fa9d271c939e%7D_footer-p5.jpg"
                                                                                                   alt=""
                                                                                                   style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                                                   width="90"></a></td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                           <!--[if mso]>
                                                                        </td>
                                                                        <td style="width:8px"></td>
                                                                        <td style="width:90px" valign="top">
                                                                           <![endif]-->
                                                                           <table cellpadding="0"
                                                                              cellspacing="0" class="es-right"
                                                                              align="right"
                                                                              style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right">
                                                                              <tbody>
                                                                                 <tr
                                                                                    style="border-collapse:collapse">
                                                                                    <td align="left"
                                                                                       style="padding:0;Margin:0;width:90px">
                                                                                       <table
                                                                                          cellpadding="0"
                                                                                          cellspacing="0"
                                                                                          width="100%"
                                                                                          role="presentation"
                                                                                          style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                                                          <tbody>
                                                                                             <tr
                                                                                                style="border-collapse:collapse">
                                                                                                <td align="center"
                                                                                                   style="padding:0;Margin:0;font-size:0px"><a target="_blank"
                                                                                                   href="https://globalvillage.ae/en/buy?elqTrackId=2bfda80ee3024150a3f6629744c01912&elqTrack=true"
                                                                                                   style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Poppins, Arial, sans-serif;font-size:14px;text-decoration:underline;color:#0B5394"><img
                                                                                                   src="https://images.visit.globalvillage.ae/EloquaImages/clients/DubaiHoldingLLC/%7B24845187-de0a-4d22-8dcb-a5043e1a99f0%7D_footer-p6.jpg"
                                                                                                   alt=""
                                                                                                   style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                                                   width="90"></a></td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                           <!--[if mso]>
                                                                        </td>
                                                                     </tr>
                                                                  </table>
                                                                  <![endif]-->
                                                               </td>
                                                            </tr>
                                                            <tr style="border-collapse:collapse">
                                                               <td class="es-footer" align="center"
                                                                  style="padding:0;Margin:0;table-layout:fixed !important;width:100%;background-color:#F6F6F6;background-repeat:repeat;background-position:center top;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"
                                                                  cellspacing="0" cellpadding="0"></td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <!--[if !mso]><!-- -->
                                          <table class="es-content es-desk-hidden" cellspacing="0"
                                             cellpadding="0" align="center"
                                             style="display:none;float:left;overflow:hidden;width:100%;max-height:0;line-height:0;mso-hide:all;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important">
                                             <tbody>
                                                <tr style="border-collapse:collapse">
                                                   <td align="center" style="padding:0;Margin:0;">
                                                      <table class=" es-content-body" cellspacing="0" cellpadding="0" align="center"
                                                         style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:600px">
                                                         <tbody>
                                                            <tr style="border-collapse:collapse">
                                                               <td class="esdev-adapt-off" align="left"
                                                                  bgcolor="#034a82"
                                                                  style="padding:5px;Margin:0;background-color:#034A82">
                                                                  <table cellpadding="0" cellspacing="0"
                                                                     class="esdev-mso-table"
                                                                     style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:580px">
                                                                     <tbody>
                                                                        <tr style="border-collapse:collapse">
                                                                           <td class="esdev-mso-td" valign="top"
                                                                              style="padding:0;Margin:0">
                                                                              <table cellpadding="0"
                                                                                 cellspacing="0" class="es-left"
                                                                                 align="left"
                                                                                 style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
                                                                                 <tbody>
                                                                                    <tr
                                                                                       style="border-collapse:collapse">
                                                                                       <td class="es-m-p0r"
                                                                                          align="center"
                                                                                          style="padding:0;Margin:0;width:192px">
                                                                                          <table
                                                                                             cellpadding="0"
                                                                                             cellspacing="0"
                                                                                             width="100%"
                                                                                             role="presentation"
                                                                                             style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                                                             <tbody>
                                                                                                <tr
                                                                                                   style="border-collapse:collapse">
                                                                                                   <td align="center"
                                                                                                      style="padding:0;Margin:0;font-size:0px">
                                                                                                      <a target="_blank"
                                                                                                         href="https://www.globalvillage.ae/plan-your-visit/?elqTrackId=7c537117e1464931bb7464a73e0c3c88&elqTrack=true"
                                                                                                         style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Poppins, Arial, sans-serif;font-size:14px;text-decoration:underline;color:#0B5394"><img
                                                                                                         src="https://images.visit.globalvillage.ae/EloquaImages/clients/DubaiHoldingLLC/%7B4491fae0-abd7-43ed-8594-5383bb551227%7D_footer-p1-m.jpg"
                                                                                                         alt=""
                                                                                                         style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                                                         width="92"></a>
                                                                                                   </td>
                                                                                                </tr>
                                                                                             </tbody>
                                                                                          </table>
                                                                                       </td>
                                                                                       <td
                                                                                          style="padding:0;Margin:0;width:3px">
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                           <td class="esdev-mso-td" valign="top"
                                                                              style="padding:0;Margin:0">
                                                                              <table cellpadding="0"
                                                                                 cellspacing="0" class="es-left"
                                                                                 align="left"
                                                                                 style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
                                                                                 <tbody>
                                                                                    <tr
                                                                                       style="border-collapse:collapse">
                                                                                       <td align="center"
                                                                                          style="padding:0;Margin:0;width:191px">
                                                                                          <table
                                                                                             cellpadding="0"
                                                                                             cellspacing="0"
                                                                                             width="100%"
                                                                                             role="presentation"
                                                                                             style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                                                             <tbody>
                                                                                                <tr
                                                                                                   style="border-collapse:collapse">
                                                                                                   <td align="center"
                                                                                                      style="padding:0;Margin:0;font-size:0px">
                                                                                                      <a target="_blank"
                                                                                                         href="https://www.globalvillage.ae/en/pavilions-selfie-spots?elqTrackId=c782ce3a6592496aaecbbed2cf2c488c&elqTrack=true"
                                                                                                         style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Poppins, Arial, sans-serif;font-size:14px;text-decoration:underline;color:#0B5394"><img
                                                                                                         src="https://images.visit.globalvillage.ae/EloquaImages/clients/DubaiHoldingLLC/%7Bcb67fdd3-a202-4cb5-9d32-3d471c95d9fd%7D_footer-p2-m.jpg"
                                                                                                         alt=""
                                                                                                         style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                                                         width="92"></a>
                                                                                                   </td>
                                                                                                </tr>
                                                                                             </tbody>
                                                                                          </table>
                                                                                       </td>
                                                                                       <td
                                                                                          style="padding:0;Margin:0;width:3px">
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                           <td class="esdev-mso-td" valign="top"
                                                                              style="padding:0;Margin:0">
                                                                              <table cellpadding="0"
                                                                                 cellspacing="0" class="es-right"
                                                                                 align="right"
                                                                                 style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right">
                                                                                 <tbody>
                                                                                    <tr
                                                                                       style="border-collapse:collapse">
                                                                                       <td align="center"
                                                                                          style="padding:0;Margin:0;width:191px">
                                                                                          <table
                                                                                             cellpadding="0"
                                                                                             cellspacing="0"
                                                                                             width="100%"
                                                                                             role="presentation"
                                                                                             style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                                                             <tbody>
                                                                                                <tr
                                                                                                   style="border-collapse:collapse">
                                                                                                   <td align="center"
                                                                                                      style="padding:0;Margin:0;font-size:0px">
                                                                                                      <a target="_blank"
                                                                                                         href="https://www.globalvillage.ae/en/shows-events?elqTrackId=06a2a3a895564f898cc67460bed2dc8a&elqTrack=true"
                                                                                                         style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Poppins, Arial, sans-serif;font-size:14px;text-decoration:underline;color:#0B5394"><img
                                                                                                         src="https://images.visit.globalvillage.ae/EloquaImages/clients/DubaiHoldingLLC/%7Bd3609310-de39-4a75-9cd6-5498f441f8bd%7D_footer-p3.jpg"
                                                                                                         alt=""
                                                                                                         style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                                                         width="91"></a>
                                                                                                   </td>
                                                                                                </tr>
                                                                                             </tbody>
                                                                                          </table>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                            <tr style="border-collapse:collapse">
                                                               <td class="es-footer" align="center"
                                                                  style="padding:0;Margin:0;table-layout:fixed !important;width:100%;background-color:#F6F6F6;background-repeat:repeat;background-position:center top;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"
                                                                  cellspacing="0" cellpadding="0"></td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <!--<![endif]--> 
                                          <!--[if !mso]><!-- -->
                                          <table class="es-content es-desk-hidden" cellspacing="0" cellpadding="0"
                                             align="center"
                                             style="display:none;float:left;overflow:hidden;width:100%;max-height:0;line-height:0;mso-hide:all;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important">
                                             <tbody>
                                                <tr style="border-collapse:collapse">
                                                   <td align="center" bgcolor="#ffffff"
                                                      style="padding:0;Margin:0;background-color:#FFFFFF">
                                                      <table class="es-content-body" cellspacing="0" cellpadding="0"
                                                         bgcolor="#ffffff" align="center"
                                                         style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px">
                                                         <tbody>
                                                            <tr style="border-collapse:collapse">
                                                               <td class="esdev-adapt-off" align="left"
                                                                  bgcolor="#034a82"
                                                                  style="padding:5px;Margin:0;background-color:#034A82">
                                                                  <table cellpadding="0" cellspacing="0"
                                                                     class="esdev-mso-table"
                                                                     style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:580px">
                                                                     <tbody>
                                                                        <tr style="border-collapse:collapse">
                                                                           <td class="esdev-mso-td" valign="top"
                                                                              style="padding:0;Margin:0">
                                                                              <table cellpadding="0"
                                                                                 cellspacing="0" class="es-left"
                                                                                 align="left"
                                                                                 style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
                                                                                 <tbody>
                                                                                    <tr
                                                                                       style="border-collapse:collapse">
                                                                                       <td align="center"
                                                                                          style="padding:0;Margin:0;width:192px">
                                                                                          <table
                                                                                             cellpadding="0"
                                                                                             cellspacing="0"
                                                                                             width="100%"
                                                                                             role="presentation"
                                                                                             style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                                                             <tbody>
                                                                                                <tr
                                                                                                   style="border-collapse:collapse">
                                                                                                   <td align="center"
                                                                                                      style="padding:0;Margin:0;font-size:0px">
                                                                                                      <a target="_blank"
                                                                                                         href="https://www.globalvillage.ae/en/dining?elqTrackId=458a1dd4d4ea427e908ea058a59fda92&elqTrack=true"
                                                                                                         style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Poppins, Arial, sans-serif;font-size:14px;text-decoration:underline;color:#0B5394"><img
                                                                                                         src="https://images.visit.globalvillage.ae/EloquaImages/clients/DubaiHoldingLLC/%7B12c7b347-0b30-47bc-a3ab-9af3df952fea%7D_footer-p4-m.jpg"
                                                                                                         alt=""
                                                                                                         style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                                                         width="91"></a>
                                                                                                   </td>
                                                                                                </tr>
                                                                                             </tbody>
                                                                                          </table>
                                                                                       </td>
                                                                                       <td
                                                                                          style="padding:0;Margin:0;width:3px">
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                           <td class="esdev-mso-td" valign="top"
                                                                              style="padding:0;Margin:0">
                                                                              <table cellpadding="0"
                                                                                 cellspacing="0" class="es-left"
                                                                                 align="left"
                                                                                 style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
                                                                                 <tbody>
                                                                                    <tr
                                                                                       style="border-collapse:collapse">
                                                                                       <td align="left"
                                                                                          style="padding:0;Margin:0;width:191px">
                                                                                          <table
                                                                                             cellpadding="0"
                                                                                             cellspacing="0"
                                                                                             width="100%"
                                                                                             role="presentation"
                                                                                             style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                                                             <tbody>
                                                                                                <tr
                                                                                                   style="border-collapse:collapse">
                                                                                                   <td align="center"
                                                                                                      style="padding:0;Margin:0;font-size:0px">
                                                                                                      <a target="_blank"
                                                                                                         href="https://www.globalvillage.ae/en/carnaval?elqTrackId=436dec1404b143cd8bc51c3b76dba0e2&elqTrack=true"
                                                                                                         style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Poppins, Arial, sans-serif;font-size:14px;text-decoration:underline;color:#0B5394"><img
                                                                                                         src="https://images.visit.globalvillage.ae/EloquaImages/clients/DubaiHoldingLLC/%7B9ea58232-458c-4b6d-ac3d-1d1d7c553026%7D_footer-p5-m.jpg"
                                                                                                         alt=""
                                                                                                         style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                                                         width="90"></a>
                                                                                                   </td>
                                                                                                </tr>
                                                                                             </tbody>
                                                                                          </table>
                                                                                       </td>
                                                                                       <td
                                                                                          style="padding:0;Margin:0;width:3px">
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                           <td class="esdev-mso-td" valign="top"
                                                                              style="padding:0;Margin:0">
                                                                              <table cellpadding="0"
                                                                                 cellspacing="0" class="es-right"
                                                                                 align="right"
                                                                                 style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right">
                                                                                 <tbody>
                                                                                    <tr
                                                                                       style="border-collapse:collapse">
                                                                                       <td align="left"
                                                                                          style="padding:0;Margin:0;width:191px">
                                                                                          <table
                                                                                             cellpadding="0"
                                                                                             cellspacing="0"
                                                                                             width="100%"
                                                                                             role="presentation"
                                                                                             style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                                                             <tbody>
                                                                                                <tr
                                                                                                   style="border-collapse:collapse">
                                                                                                   <td align="center"
                                                                                                      style="padding:0;Margin:0;font-size:0px">
                                                                                                      <a target="_blank"
                                                                                                         href="https://globalvillage.ae/en/buy?elqTrackId=148e5fdc689b4329a656887211217d0e&elqTrack=true"
                                                                                                         style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Poppins, Arial, sans-serif;font-size:14px;text-decoration:underline;color:#0B5394"><img
                                                                                                         src="https://images.visit.globalvillage.ae/EloquaImages/clients/DubaiHoldingLLC/%7B24845187-de0a-4d22-8dcb-a5043e1a99f0%7D_footer-p6.jpg"
                                                                                                         alt=""
                                                                                                         style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                                                         width="90"></a>
                                                                                                   </td>
                                                                                                </tr>
                                                                                             </tbody>
                                                                                          </table>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                            <tr style="border-collapse:collapse">
                                                               <td class="es-footer" align="center"
                                                                  style="padding:0;Margin:0;table-layout:fixed !important;width:100%;background-color:#F6F6F6;background-repeat:repeat;background-position:center top;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"
                                                                  cellspacing="0" cellpadding="0"></td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <!--<![endif]-->
                                          <table align="center" cellpadding="0" cellspacing="0" class="es-footer"
                                             style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top">
                                             <tbody>
                                                <tr style="border-collapse:collapse">
                                                   <td align="center" bgcolor="#ffffff"
                                                      style="padding:0;Margin:0;background-color:#FFFFFF">
                                                      <table align="center" cellpadding="0" cellspacing="0"
                                                         class="es-footer"
                                                         style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:#F0F0F0;background-repeat:repeat;background-position:center top">
                                                         <tbody>
                                                            <tr style="border-collapse:collapse">
                                                               <td align="center" style="padding:0;Margin:0;">
                                                                  <table align="center" bgcolor="#26a151"
                                                                     cellpadding="0" cellspacing="0"
                                                                     class="es-footer-body"
                                                                     style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#26A151;width:600px">
                                                                     <tbody>
                                                                        <tr style="border-collapse:collapse">
                                                                           <td align="left"
                                                                              style="Margin:0;padding-bottom:5px;padding-top:20px;padding-left:20px;padding-right:20px">
                                                                              <table cellpadding="0"
                                                                                 cellspacing="0"
                                                                                 style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"
                                                                                 width="100%">
                                                                                 <tbody>
                                                                                    <tr
                                                                                       style="border-collapse:collapse">
                                                                                       <td align="center"
                                                                                          style="padding:0;Margin:0;width:560px"
                                                                                          valign="top">
                                                                                          <table
                                                                                             cellpadding="0"
                                                                                             cellspacing="0"
                                                                                             style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"
                                                                                             width="100%">
                                                                                             <tbody>
                                                                                                <tr
                                                                                                   style="border-collapse:collapse">
                                                                                                   <td align="center"
                                                                                                      bgcolor="transparent"
                                                                                                      class="es-m-txt-c"
                                                                                                      style="padding:0;Margin:0;font-size:0px;background-color:transparent">
                                                                                                      <table
                                                                                                         cellpadding="0"
                                                                                                         cellspacing="0"
                                                                                                         class="es-table-not-adapt es-social"
                                                                                                         style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                                                                         <tbody>
                                                                                                            <tr
                                                                                                               style="border-collapse:collapse">
                                                                                                               <td align="center"
                                                                                                                  style="padding:0;Margin:0;padding-right:10px"
                                                                                                                  valign="top"><a href="https://www.facebook.com/GlobalVillageAE/?elqTrackId=f3e0fe521b574a6f8d24fa989fbef859"
                                                                                                                  style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Arial, sans-serif;font-size:13px;text-decoration:none;color:#FFFFFF"
                                                                                                                  target="_blank"><img
                                                                                                                  alt="Fb"
                                                                                                                  height="32"
                                                                                                                  src="http://image.explore.globalvillage.ae/lib/fe2d11737364047c701278/m/1/52fa6ff6-5630-4321-847c-7f6327abe7cd.png"
                                                                                                                  style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                                                                  title="Facebook"
                                                                                                                  width="32" /></a></td>
                                                                                                               <td align="center"
                                                                                                                  style="padding:0;Margin:0;padding-right:10px"
                                                                                                                  valign="top"><a href="https://twitter.com/GlobalVillageAE?elqTrackId=8719c965a0954c15acbb526556f79a85"
                                                                                                                  style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Arial, sans-serif;font-size:13px;text-decoration:none;color:#FFFFFF"
                                                                                                                  target="_blank"><img
                                                                                                                  alt="Tw"
                                                                                                                  height="32"
                                                                                                                  src="https://image.explore.globalvillage.ae/lib/fe2d11737364047c701278/m/1/89e8e48c-6f51-4619-b9a8-405da62408a9.png"
                                                                                                                  style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                                                                  title="Twitter"
                                                                                                                  width="32" /></a></td>
                                                                                                               <td align="center"
                                                                                                                  style="padding:0;Margin:0;padding-right:10px"
                                                                                                                  valign="top"><a href="https://www.instagram.com/globalvillageuae/?elqTrackId=c1136896ba4e4aad9693d7b0eee11652"
                                                                                                                  style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Arial, sans-serif;font-size:13px;text-decoration:none;color:#FFFFFF"
                                                                                                                  target="_blank"><img
                                                                                                                  alt="Inst"
                                                                                                                  height="32"
                                                                                                                  src="https://image.explore.globalvillage.ae/lib/fe2d11737364047c701278/m/1/1cacb5f7-0546-4a8b-a051-dd8e1c981a60.png"
                                                                                                                  style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                                                                  title="Instagram"
                                                                                                                  width="32" /> </a></td>
                                                                                                               <td align="center"
                                                                                                                  style="padding:0;Margin:0;padding-right:10px"
                                                                                                                  valign="top"><a href="https://www.youtube.com/user/GlobalVillageAE?elqTrackId=4ed590584c774af5b87d423f24e9fc2f"
                                                                                                                  style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Arial, sans-serif;font-size:13px;text-decoration:none;color:#FFFFFF"
                                                                                                                  target="_blank"><img
                                                                                                                  alt="Yb"
                                                                                                                  height="32"
                                                                                                                  src="https://image.explore.globalvillage.ae/lib/fe2d11737364047c701278/m/1/c7fd849f-d906-451e-b363-eb6b8c36627e.png"
                                                                                                                  style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                                                                  title="Youtube"
                                                                                                                  width="32" /></a></td>
                                                                                                               <td align="center"
                                                                                                                  style="padding:0;Margin:0;padding-right:10px"
                                                                                                                  valign="top"><a href="https://www.snapchat.com/add/globalvillageme?elqTrackId=4ed590584c774af5b87d423f24e9fc2f"
                                                                                                                  style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Arial, sans-serif;font-size:13px;text-decoration:none;color:#FFFFFF"
                                                                                                                  target="_blank"><img
                                                                                                                  alt="Sc"
                                                                                                                  height="32"
                                                                                                                  src="https://image.explore.globalvillage.ae/lib/fe2d11737364047c701278/m/1/e678efe2-57e5-4f21-9d84-e85b14558ccc.png"
                                                                                                                  style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                                                                  title="SnapChat"
                                                                                                                  width="32" /></a>
                                                                                                            </tr>
                                                                                                         </tbody>
                                                                                                      </table>
                                                                                                   </td>
                                                                                                </tr>
                                                                                                <tr
                                                                                                   style="border-collapse:collapse">
                                                                                                   <td align="center"
                                                                                                      class="es-m-txt-c"
                                                                                                      style="Margin:0;padding-top:15px;padding-bottom:5px;padding-left:5px;padding-right:5px">
                                                                                                      <p
                                                                                                         style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:12px;font-family:arial, Poppins, sans-serif;line-height:16px;color:#FFFFFF"> <strong><a
                                                                                                         class="view"
                                                                                                         data-targettype="sysaction"
                                                                                                         href="https://app.visit.globalvillage.ae/e/es?s=~~eloqua..type--emailfield..syntax--siteid..encodeFor--url~~&e=~~eloqua..type--emailfield..syntax--elqemailsaveguid..encodeFor--url~~&elqTrackId=ef5e870bed694b8aa2be94d17bccf646"
                                                                                                         style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Arial, sans-serif;font-size:12px;text-decoration:none;color:#FFFFFF"
                                                                                                         target="_blank">Web
                                                                                                         Version</a></strong>&nbsp;&nbsp;&nbsp;<strong> </strong> 
                                                                                                      </p>
                                                                                                   </td>
                                                                                                </tr>
                                                                                                <tr
                                                                                                   style="border-collapse:collapse">
                                                                                                   <td align="center"
                                                                                                      style="padding:0;Margin:0;padding-bottom:5px;padding-top:5px">
                                                                                                      <h3
                                                                                                         style="Margin:0;line-height:13px;mso-line-height-rule:exactly;font-family:arial, Poppins, sans-serif;font-size:11px;font-style:normal;font-weight:normal;color:#FFFFFF"> &copy;
                                                                                                         {{date('Y')}}
                                                                                                         Global
                                                                                                         Village.
                                                                                                         All
                                                                                                         Right
                                                                                                         Reserved
                                                                                                         ✦
                                                                                                         Exit
                                                                                                         37,
                                                                                                         Sheikh
                                                                                                         Mohammed
                                                                                                         Bin
                                                                                                         Zayed
                                                                                                         Road
                                                                                                         and
                                                                                                         Emirates
                                                                                                         Road
                                                                                                         Dubai,
                                                                                                         United
                                                                                                         Arab
                                                                                                         Emirates 
                                                                                                      </h3>
                                                                                                   </td>
                                                                                                </tr>
                                                                                             </tbody>
                                                                                          </table>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <table align="center" cellpadding="0" cellspacing="0" class="es-content"
                                             style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
                                             <tbody>
                                                <tr style="border-collapse:collapse">
                                                   <td align="center" bgcolor="#ffffff"
                                                      style="padding:0;Margin:0;background-color:#FFFFFF">
                                                      <table align="center" bgcolor="#ffffff" cellpadding="0"
                                                         cellspacing="0" class="es-content-body"
                                                         style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px">
                                                         <tbody>
                                                            <tr style="border-collapse:collapse">
                                                               <td align="left" style="padding:0;Margin:0">
                                                                  <table cellpadding="0" cellspacing="0"
                                                                     style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"
                                                                     width="100%">
                                                                     <tbody>
                                                                        <tr style="border-collapse:collapse">
                                                                           <td align="center"
                                                                              style="padding:0;Margin:0;width:600px"
                                                                              valign="top">
                                                                              <table cellpadding="0"
                                                                                 cellspacing="0"
                                                                                 style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"
                                                                                 width="100%">
                                                                                 <tbody>
                                                                                    <tr
                                                                                       style="border-collapse:collapse">
                                                                                       <td align="center"
                                                                                          style="padding:0;Margin:0;font-size:0px"></td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </table>
      </div>
   </body>
</html>