@extends('adminlte::page')

@section('title', 'Vehicle Parking Locations')

@section('content_header')
<h1>Vehicle Parking Locations</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <h3>&nbsp;</h3>
                <div class="box-tools">
                    @if(auth('admin')->user()->hasRole('super-admin','admin') || auth('admin')->user()->hasPermissionTo('vehicle count_create','admin'))
                    <a href="{{route('admin.vehicle-plocations.create')}}" class="btn btn-sm btn-primary">Add New</a>
                    @endif
                    @if(auth('admin')->user()->hasRole('super-admin','admin') || auth('admin')->user()->hasPermissionTo('vehicle count_update','admin'))
                    <a href="{{route('admin.vpl.sync.count')}}" class="btn btn-sm btn-warning sync-count">Sync Latest Count</a>
                    <a href="{{route('admin.clear.user.parkings.cronstatus')}}" class="btn btn-sm btn-warning user-parkings"><span>{{strtoupper(($configurations->value=='enable')?'disable':'enable')}}</span> cron for "clear user parkings"</a>
                    @endif
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive" style="overflow-x: hidden;">
                    <table class="table table-striped table-bordered table-hover" id="list-table">
                        <thead>
                            <tr>
                                <th>Parking ID</th>
                                <th>Zone</th>
                                <th>Name</th>
                                <th>Total Capacity</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section("css")

<link rel="stylesheet" href="{{asset('css/bootstrap4-toggle.min.css')}}">
@stop
@section('js')
<script src="{{asset('js/bootstrap4-toggle.min.js')}}"></script>
<script type='text/javascript'>
$(function () {
    oTable = $('#list-table').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        ordering: false,
        responsive: true,
        lengthChange: true,
        ajax: {
            url: "{!! route('admin.vehicle-plocations.datatable') !!}",
            type: 'post',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'}
        },

        columns: [

            {data: 'vgs_id', name: 'vgs_id'},
            {data: 'zone_names.en', name: 'zone_names->en'},
            {data: 'names.en', name: 'names->en'},
            {data: 'total_capacity', name: 'total_capacity'},
            {data: 'status', name: 'status', orderable: false, searchable: false},
            {data: 'actions', name: 'actions', orderable: false, searchable: false}
        ],
        fnDrawCallback: function () {
            $(".bsSwitch").bootstrapToggle({
                size: "sm",
                onstyle: "success",
                offstyle: "danger",
                //on: 'Enabled',
                //off: 'Disabled'
            });
        }
    });

    $(document).on('click', 'a.sync-count', function (e) {
        e.preventDefault();
        var url = $(this).attr('href')
        Pace.restart();
        Pace.track(function () {
            $.ajax({
                url: url,
                method: 'post',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                success: function (response) {
                    swal(response.message);
                },
                error: function (response) {
                    swal('Something went wrong.');
                }
            });
        });
    });

    $(document).on('click', 'a.user-parkings', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        var ths = $(this);
        Pace.restart();
        Pace.track(function () {
            $.ajax({
                url: url,
                method: 'post',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                success: function (response) {
                    if (response.status) {
                        ths.children('span').html(response.old_state);
                        swal(response.current_state);
                    } else {
                        swal(response.message);
                    }
                },
                error: function (response) {
                    swal('Something went wrong.');
                }
            });
        });
    });

    $(document).on('change', '.bsSwitch', function () {
        var id = $(this).data('id');
        var st = $(this).data('st');
        $.ajax({
            url: "{!! route('admin.vehicle-plocations.status') !!}",
            dataType: 'json',

            cache: false,
            method: 'POST',
            data: {id: id, st: st},
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            beforeSend: function () {

            },
            success: function (response) {
                oTable.draw();
            },
            error: function (response) {

            },
            complete: function () {

            }
        });
    });

});
</script>
@stop
