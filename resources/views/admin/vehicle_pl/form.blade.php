<div class="row">
    <div class="col-md-2">
        <div class="form-group {{ $errors->has('is_custom') ? 'has-error' : '' }}">
            {{ Form::label('is_custom', 'Set vehicles in') }}
            {!! Form::select('is_custom', [0=>'API',1=>'Manually'],null, ['class' => 'form-control', 'id' => 'is_custom']) !!}
            {!! $errors->first('is_custom','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group {{ $errors->has('vgs_id') ? 'has-error' : '' }}">
            {{ Form::label('vgs_id', 'Parking ID') }}
            {{ Form::text('vgs_id', null, array('class' => 'form-control')) }}
            {!! $errors->first('vgs_id','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group {{ $errors->has('total_capacity') ? 'has-error' : '' }}">
            {{ Form::label('total_capacity', 'Total Capacity') }}
            {{ Form::number('total_capacity', null, array('class' => 'form-control')) }}
            {!! $errors->first('total_capacity','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group {{ $errors->has('beco_name') ? 'has-error' : '' }}">
            {{ Form::label('beco_name', 'Beco Name') }}
            {{ Form::text('beco_name', null, array('class' => 'form-control')) }}
            {!! $errors->first('beco_name','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group {{ $errors->has('latitude') ? 'has-error' : '' }}">
            {{ Form::label('latitude', 'Latitude') }}
            {{ Form::text('latitude', null, array('class' => 'form-control')) }}
            {!! $errors->first('latitude','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group {{ $errors->has('longitude') ? 'has-error' : '' }}">
            {{ Form::label('longitude', 'Longitude') }}
            {{ Form::text('longitude', null, array('class' => 'form-control')) }}
            {!! $errors->first('longitude','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('zone_names.en') ? 'has-error' : '' }}">
            {{ Form::label('zone_names', 'Zone Name (en)') }}
            {{ Form::text('zone_names[en]', null, array('class' => 'form-control', 'dir'=>'ltr')) }}
            {!! $errors->first('zone_names.en','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('zone_names.ar') ? 'has-error' : '' }}">
            {{ Form::label('zone_names', 'Zone Name (ar)') }}
            {{ Form::text('zone_names[ar]', null, array('class' => 'form-control', 'dir'=>'rtl')) }}
            {!! $errors->first('zone_names.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('names.en') ? 'has-error' : '' }}">
            {{ Form::label('name', 'Name (en)') }}
            {{ Form::text('names[en]', null, array('class' => 'form-control', 'dir'=>'ltr')) }}
            {!! $errors->first('names.en','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
        <div class="form-group {{ $errors->has('descriptions.en') ? 'has-error' : '' }}">
            {{ Form::label('descriptions', 'Description (en)') }}
            {{ Form::textarea('descriptions[en]', null, array('class' => 'form-control', 'dir'=>'ltr')) }}
            {!! $errors->first('descriptions.en','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('names.ar') ? 'has-error' : '' }}">
            {{ Form::label('names', 'Name (ar)') }}
            {{ Form::text('names[ar]', null, array('class' => 'form-control', 'dir'=>'rtl')) }}
            {!! $errors->first('names.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
        <div class="form-group {{ $errors->has('descriptions.ar') ? 'has-error' : '' }}">
            {{ Form::label('descriptions', 'Description (ar)') }}
            {{ Form::textarea('descriptions[ar]', null, array('class' => 'form-control', 'dir'=>'rtl')) }}
            {!! $errors->first('descriptions.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        {{ Form::checkbox('is_undefined_location', null, null, ['class'=>'','id'=>'is_undefined_location'] ) }}
        {{ Form::label('is_undefined_location', 'Is undefined location') }}
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 hideLocation">
        <input id="area_cordinates_map" name="area_cordinates_map" value="" type="hidden">
        <input  type=button id="remove_polygon" class="center-block btn btn-sm btn-danger" value="Remove polygon"><br/>
        {!! $errors->first('area_cordinates_map','<p class="text-danger"><strong>:message</strong></p>') !!}
        <div id="map-canvas"></div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-3">
        {{ Form::checkbox('extras[pay_park]', null, null, ['class'=>'checkboxRole minimal-red'] ) }}
        {{ Form::label('pay_park', 'Pay for parking') }}
    </div>
    <div class="col-md-3 hideLocation">
        {{ Form::checkbox('extras[car_wash]', null, null, ['class'=>'checkboxRole minimal-red'] ) }}
        {{ Form::label('car_wash', 'Car Wash') }}
    </div>
    <div class="col-md-3 ">
        {{ Form::checkbox('extras[directions]', null, null, ['class'=>'checkboxRole minimal-red'] ) }}
        {{ Form::label('directions', 'Get Directions') }}
    </div>
</div>
<br>
<div class="row hideLocation">
    <div class="col-md-12" style="clear: left;">
        <div class="box box-primary box-solid">
            <div class="box-header">
                <h3 class="box-title">Services</h3>
            </div>
            <div class="box-body">
                @foreach($services as $service)
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {{ Form::checkbox('services['.$service->id.']', null, null, ['class'=>'checkboxRole minimal-red'] ) }}
                            {{ Form::label($service->id, $service->name) }}
                        </div>
                        <div class="panel-body pad-0">
                            <div class="col-md-12 pad-0">
                                <div class="form-group">
                                    {{ Form::label('bename', 'Beco Name') }}
                                    {{ Form::text('bename['.$service->id.']', null, array('class' => 'form-control')) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('sdescriptions', 'Description (en)') }}
                                    {{ Form::textarea('sdescriptions['.$service->id.'][en]', null, array('class' => 'form-control','rows'=>2)) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('sdescriptions', 'Description (ar)') }}
                                    {{ Form::textarea('sdescriptions['.$service->id.'][ar]', null, array('class' => 'form-control', 'dir'=>'rtl','rows'=>2)) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div> 
</div>
@if(isset($vpl))
<div class="row">
    <div class="col-md-2">
        <div class="form-group {{ $errors->has('display_order') ? 'has-error' : '' }}">
            {{ Form::label('display_order', 'Display Order') }}
            {{ Form::number('display_order', null, array('class' => 'form-control')) }}
            {!! $errors->first('display_order','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
</div>
@endif
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="form-group ">
                <label for="import_file">Images(English) (Recommended : 750x338)</label>
                <input class="form-control" accept="image/png, image/gif, image/jpeg" multiple autocomplete="off" name="images[en][]" type="file" id="images">
            </div>
        </div>
    </div>
    @if(isset($vpl))
    <div class="col-md-12">
        <ul class="sortable-images" data-lang="en" style="list-style:none; padding:0;">
            <?php $sImages = $vpl->images()->where('language', 'en')->orderBy('display_order')->get(); ?>
            @foreach($sImages as $b)
            <li style="display: inline; margin:15px; float:left;" class="row-en" data-id="{{ $b->id }}">
                <div class="box box-default box-solid">
                    <div class="box-body">
                        <img name="{{$b->id}}" style="height: 150px;width: 150px" src="{{$b->image}}" alt="user-avatar" class="img-fluid">
                    </div>
                    <div class="box-footer">
                        <div class="text-center">
                            <a href="{{route('admin.vpl.delete.image',$b->id)}}" class="btn btn-danger btn-xs destroy">
                                <i class="fas fa-trash"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="col-md-6">
        <div class="form-group">
            <div class="form-group ">
                <label for="import_file">Images(Arabic) (Recommended : 750x338)</label>
                <input class="form-control" accept="image/png, image/gif, image/jpeg" multiple autocomplete="off" name="images[ar][]" type="file" id="images">
            </div>
        </div>
    </div>
    @if(isset($vpl))
    <div class="col-md-12">
        <ul class="sortable-images" data-lang="ar" style="list-style:none; padding:0;">
            <?php $sImages = $vpl->images()->where('language', 'ar')->orderBy('display_order')->get(); ?>
            @foreach($sImages as $b)
            <li style="display: inline; margin:15px; float:left;" class="row-ar" data-id="{{ $b->id }}">
                <div class="box box-default box-solid">
                    <div class="box-body">
                        <img name="{{$b->id}}" style="height: 150px;width: 150px" src="{{$b->image}}" alt="user-avatar" class="img-fluid">
                    </div>
                    <div class="box-footer">
                        <div class="text-center">
                            <a href="{{route('admin.vpl.delete.image',$b->id)}}" class="btn btn-danger btn-xs destroy">
                                <i class="fas fa-trash"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
    @endif
</div>

@foreach($day_array as $key => $_day)
@if(!empty($vpl->id))
@php($data = App\VehiclePlocationTiming::where('vehicle_plocation_id', $vpl->id)->where('day', $key)->first())
@endif
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <div class="col-md-12">
                <label for="sunday">{{ $_day }}</label>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <div class="form-group ">
                        <label for="start_time[{{ $key }}]">Start Time</label>
                        <input class="form-control timepicker"  autocomplete="off" name="start_time[{{ $key }}]" value="{{ !empty($data) ? $data->start_time : '' }}" type="text" id="start_time_{{ $key }}">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <div class="form-group ">
                        <label for="end_time[{{ $key }}]">End Time</label>
                        <input class="form-control timepicker"  autocomplete="off" name="end_time[{{ $key }}]" type="text" value="{{ !empty($data) ? $data->end_time : '' }}" id="end_time_{{ $key }}">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <div class="form-group ">
                        <label for="is_next_day[{{ $key }}]">Next day</label>
                        <input  autocomplete="off" name="is_next_day[{{ $key }}]" type="checkbox" @if( !empty($data) && $data->is_next_day == 1) checked @endif id="is_next_day_{{ $key }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
