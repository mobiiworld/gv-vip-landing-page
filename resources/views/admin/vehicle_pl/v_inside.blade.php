@extends('adminlte::page')

@section('title', 'Vehicle Parkings')

@section('content_header')
<h1> Set Vehicles In</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12 ">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-tools">
                </div>
            </div>
            {{ Form::open(array('url' => 'admin/vehicle-inside','id'=>'insideForm')) }}
            <div class="box-body">
                @foreach($locations as $location)
                <div class="row">
                    <div class="col-md-6">
                        <label>{{$location->zone_name}} - {{$location->name}} - (Total Capacity : {{$location->total_capacity}})</label>
                        <div class="form-group">
                            <span class="minus btn btn-info"><i class="fa fa-fw fa-minus"></i></span>
                            <input id="{{$location->id}}" type="number" class="text-center insideVals" name="insides[{{$location->id}}]" value="{{$location->inside}}">
                            <span class="plus btn btn-info" capacity="{{$location->total_capacity}}"><i class="fa fa-fw fa-plus"></i></span>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="box-footer">
                {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
                {{ Form::submit('Reset to 0', array('class' => 'btn btn-primary pull-right reset')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop
@section('js')
<script type='text/javascript'>
    $('.box-body').on('keyup', '.insideVals', function () {
        $capacity = parseInt($(this).parent('div').find('span.plus').attr('capacity'));
        if ($(this).val() > $capacity) {
            alert('Yo can enter upto ' + $capacity);
            $(this).val(0);
        }
    });
    $('.box-body').on('click', '.plus', function () {
        $capacity = parseInt($(this).attr('capacity'));
        $addedValue = parseInt($(this).siblings('input').val()) + 1;
        if ($addedValue <= $capacity) {
            $(this).siblings('input').val($addedValue);
        }
    });
    $('.box-body').on('click', '.minus', function () {
        var x = parseInt($(this).siblings('input').val());
        if (x >= 1) {
            $(this).siblings('input').val(parseInt($(this).siblings('input').val()) - 1);
        }
    });
    $('.box-footer').on('click', '.reset', function (e) {
        e.preventDefault();
        $("#insideForm").find('input.insideVals').val(0);
        $("#insideForm").submit();
    });
</script>
@stop
