@extends('adminlte::page')

@section('title', 'Add Parking Service')

@section('content_header')
<h1> Add Parking Service</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12 ">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-tools">
                    <a href="{{route('admin.parking-services.index')}}" class="btn btn-sm btn-default">Back</a>
                </div>
            </div>

            {{ Form::open(array('url' => 'admin/parking-services','enctype' => 'multipart/form-data')) }}
            <div class="box-body">
                @include("admin.vehicle_pl.services.form")

            </div>
            <div class="box-footer">
                {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop