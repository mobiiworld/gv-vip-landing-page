@extends('adminlte::page')

@section('title', 'Parking Services')

@section('content_header')
<h1><i class="fa fa-car"></i> Parking Services</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3>&nbsp;</h3>
                <div class="box-tools">
                    <a href="{{route('admin.parking-services.create')}}" class="btn btn-sm btn-primary">Create</a>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive" style="overflow-x: hidden;">
                    <table class="table table-striped table-bordered table-hover" id="fitness-seasons-table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section('js')
<script type='text/javascript'>
    $(function () {
        oTable = $('#fitness-seasons-table').DataTable({
            processing: true,
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: false,
            info: true,
            autoWidth: false,
            responsive: true,
            serverSide: true,
            //autoWidth: false,
            ajax: {
                url: "{!! route('admin.parking.services.datatable') !!}",
                type: 'post',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'}
            },
            columns: [
                {data: 'names.en', name: 'names->en'},
                {data: 'actions', name: 'actions', orderable: false, searchable: false}
            ],
            language: {
                searchPlaceholder: "Name"
            }
        });
    });
</script>
@stop
