<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('names.en') ? 'has-error' : '' }}">
            {{ Form::label('names', 'Name (en)') }}
            {{ Form::text('names[en]', null, array('class' => 'form-control', 'dir'=>'ltr')) }}
            {!! $errors->first('names.en','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('names.ar') ? 'has-error' : '' }}">
            {{ Form::label('names', 'Name (ar)') }}
            {{ Form::text('names[ar]', null, array('class' => 'form-control', 'dir'=>'rtl')) }}
            {!! $errors->first('names.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('descriptions.en') ? 'has-error' : '' }}">
            {{ Form::label('descriptions', 'Description (en)') }}
            {{ Form::textarea('descriptions[en]', null, array('class' => 'form-control', 'dir'=>'ltr')) }}
            {!! $errors->first('descriptions.en','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('descriptions.ar') ? 'has-error' : '' }}">
            {{ Form::label('descriptions', 'Description (ar)') }}
            {{ Form::textarea('descriptions[ar]', null, array('class' => 'form-control', 'dir'=>'rtl')) }}
            {!! $errors->first('descriptions.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group ">
            <label for="icon">Icon (Recommended : 100x100)</label>
            <input class="form-control" accept="image/png, image/gif, image/jpeg" autocomplete="off" name="icon" type="file" id="icon">
            {!! $errors->first('icon','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    @if((isset($service) && $service->icon != NULL))
    <div class="col-md-6">
        <div class="form-group ">
            <img src="{{$service->icon}}" class="img-square img-size-30 mt-2 pt-2" width="100"/>
        </div>
    </div>
    @endif
</div>