@extends('adminlte::page')

@section('title', 'Parking Area')

@section('content_header')
<h1>Parking Area</h1>
@stop

@section('content')
<div class="row">

    <div class="col-md-12">
        <div class="box box-primary">
            {{ Form::model($parea,array('url' => route('admin.parking.area.save'), 'method' => 'POST')) }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('latitude') ? 'has-error' : '' }}">
                            {{ Form::label('latitude', 'Center Latitude') }}
                            {{ Form::text('latitude', null, array('class' => 'form-control')) }}
                            {!! $errors->first('latitude','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('longitude') ? 'has-error' : '' }}">
                            {{ Form::label('longitude', 'Center Longitude') }}
                            {{ Form::text('longitude', null, array('class' => 'form-control')) }}
                            {!! $errors->first('longitude','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="form-group m-form__group row">
                                {{ Form::label('region', 'Region') }}
                                {{ Form::text('searchmap', null, array('class' => 'form-control','placeholder'=>"Search", 'id'=>"searchmap",)) }}
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="form-group m-form__group row">

                            <div class="col-lg-12 col-md-12 col-sm-12">

                                <input id="area_cordinates_map" name="area_cordinates_map" value="" type="hidden">

                                <input  type=button id="remove_polygon" class="pull-right" value="Remove polygon"><br/>
                                {!! $errors->first('area_cordinates_map','<p class="text-danger"><strong>:message</strong></p>') !!}
                                <div id="map-canvas"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
            </div>
            {{ Form::close() }}

        </div>
    </div>
</div>
@stop
@section('css')
<style>
    #map-canvas {
        width: 100%;
        height: 500px;
    }
</style>
@stop
@section('js')
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('API_MAP_KEY', 'AIzaSyBpC8Nftwn_ZbBh42oKbCwDgygVi8B271Y') }}&libraries=places,drawing&language=en&callback=initialize" async defer></script>
<script type='text/javascript'>
var placeSearch, autocomplete, map, _myPolygon;
function initialize() {
initMap();
initAutocomplete();
}
function initMap() {

var arr = {{$mapCordinates}};
var polygonCoords = [];
if (arr != null) {
for (var j = 0; j < arr.length; j++) {
polygonCoords.push(new google.maps.LatLng(arr[j][0], arr[j][1]));
}
}

map = new google.maps.Map(document.getElementById('map-canvas'), {
center: {
lat: 25.2048,
        lng: 55.2708
        },
        zoom: 10
        });
if (polygonCoords.length != 0) {
_myPolygon = new google.maps.Polyline({
path: polygonCoords,
        draggable: false,
        editable: false,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        map: map
        });
// Create the bounds object
var bounds = new google.maps.LatLngBounds();
// Get paths from polygon and set event listeners for each path separately
_myPolygon.getPath().forEach(function (path, index) {
bounds.extend(path);
});
// Fit Polygon path bounds
map.fitBounds(bounds);
}
var drawingManager = new google.maps.drawing.DrawingManager({
drawingMode: google.maps.drawing.OverlayType.PLOYGON,
        drawingControl: (polygonCoords.length == 0) ? true : false,
        drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: ['polygon']
                },
        polygonOptions: {
        draggable: true,
                editable: true,
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.35
                }
});
polyline = google.maps.event.addListener(drawingManager, 'polygoncomplete', function (shape) {
if (shape == null || (!(shape instanceof google.maps.Polygon)))
        return;
var coordinatesArray = shape.getPath().getArray();
$('#area_cordinates_map').val(JSON.stringify(coordinatesArray));
drawingManager.setOptions({drawingMode: null, drawingControl: false});
_myPolygon = shape;
});
drawingManager.setMap(map);
$("#remove_polygon").click(function () {
_myPolygon.setMap(null);
drawingManager.setOptions({drawingMode: null, drawingControl: true});
});
var input = document.getElementById('searchmap');
google.maps.event.addDomListener(input, 'keydown', function (event) {
if (event.keyCode === 13) {
event.preventDefault();
}
});
}

function initAutocomplete() {
// Create the autocomplete object, restricting the search to geographical
// location types.
autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */
                (document.getElementById('searchmap')));
// When the user selects an address from the dropdown, populate the address
// fields in the form.
autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
// Get the place details from the autocomplete object.
var place = autocomplete.getPlace();
if (!place.geometry) {
// User entered the name of a Place that was not suggested and
// pressed the Enter key, or the Place Details request failed.
window.alert("No details available for input: '" + place.name + "'");
return;
} else {
map.setCenter(place.geometry.location);
map.setZoom(17);
}
}

function pan() {
var panPoint = new google.maps.LatLng(document.getElementById("lat").value, document.getElementById("lng").value);
map.setCenter(panPoint);
map.setZoom(17);
}

</script>
@stop