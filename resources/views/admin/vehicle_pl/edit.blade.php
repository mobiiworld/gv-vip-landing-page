@extends('adminlte::page')

@section('title', 'Vehicle Parking Locations')

@section('content_header')
<h1>Edit Vehicle Parking Locations</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-tools">
                    <a href="{{route('admin.vehicle-plocations.index')}}" class="btn btn-sm btn-default">Back</a>
                </div>
            </div>
            {{ Form::model($vpl,array('url' => route('admin.vehicle-plocations.update',$vpl->id),'enctype' => 'multipart/form-data', 'method' => 'PUT')) }}
            <div class="box-body">
                @include("admin.vehicle_pl.form")
            </div>
            <div class="box-footer">
                {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop
@section('css')
<style>
    #map-canvas {
        width: 100%;
        height: 500px;
    }
</style>
<link rel="stylesheet" href="{{asset('vendor/adminlte/plugins/iCheck/all.css')}}">
@stop
@section('js')
<script>
$(document).ready(function(){
    changeLocationFields();
    $("#is_undefined_location").on("click",function(){
        console.log("sdfsdf");
        changeLocationFields();
    });
})
function changeLocationFields(){
    console.log($("#is_undefined_location").is(":checked"));
    if($("#is_undefined_location").is(":checked")){
        $(".hideLocation").hide();
    }else{
        $(".hideLocation").show();
    }
}
</script>
<script src="{{asset('vendor/adminlte/plugins/iCheck/icheck.min.js')}}"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css" id="theme" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('API_MAP_KEY', 'AIzaSyBpC8Nftwn_ZbBh42oKbCwDgygVi8B271Y') }}&libraries=places,drawing&language=en&callback=initialize" async defer></script>
<script>
$(function () {
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass: 'iradio_minimal-red'
    })
});
$('.timepicker').timepicker();
var placeSearch, autocomplete, map, _myPolygon;
function initialize() {
    initMap();
    initAutocomplete();
}
function initMap() {
    var arr = {{$mapCordinates}};

    var polygonCoords = [];
    if (arr != null) {
        for (var j = 0; j < arr.length; j++) {
            polygonCoords.push(new google.maps.LatLng(arr[j][0], arr[j][1]));
        }
    }

    map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: {
            lat: 25.2048,
            lng: 55.2708
        },
        zoom: 10
    });
    
    if(arr.length!=0){
    _myPolygon = new google.maps.Polyline({
        path: polygonCoords,
        draggable: false,
        editable: false,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        map: map
    });
// Create the bounds object
    var bounds = new google.maps.LatLngBounds();
// Get paths from polygon and set event listeners for each path separately
    _myPolygon.getPath().forEach(function (path, index) {
        bounds.extend(path);
    });
// Fit Polygon path bounds
    map.fitBounds(bounds);
}
    var drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.PLOYGON,
        drawingControl: (arr.length==0)?true:false,
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            drawingModes: ['polygon']
        },
        polygonOptions: {
            draggable: true,
            editable: true,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35
        }
    });
    polyline = google.maps.event.addListener(drawingManager, 'polygoncomplete', function (shape) {
        if (shape == null || (!(shape instanceof google.maps.Polygon)))
            return;
        var coordinatesArray = shape.getPath().getArray();
        $('#area_cordinates_map').val(JSON.stringify(coordinatesArray));
        drawingManager.setOptions({drawingMode: null, drawingControl: false});
        _myPolygon = shape;
    });
    drawingManager.setMap(map);
    $("#remove_polygon").click(function () {
        _myPolygon.setMap(null);
        drawingManager.setOptions({drawingMode: null, drawingControl: true});
    });
}
</script>
@stop
