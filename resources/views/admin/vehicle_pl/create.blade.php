@extends('adminlte::page')

@section('title', 'Vehicle Parking Locations')

@section('content_header')
<h1> Add Vehicle Parking Location</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12 ">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-tools">
                    <a href="{{route('admin.vehicle-plocations.index')}}" class="btn btn-sm btn-default">Back</a>
                </div>
            </div>
            {{ Form::open(array('url' => 'admin/vehicle-plocations','enctype' => 'multipart/form-data')) }}
            <div class="box-body">
                @include("admin.vehicle_pl.form")
            </div>
            <div class="box-footer">
                {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop
@section('css')
<style>
    #map-canvas {
        width: 100%;
        height: 500px;
    }
</style>
<link rel="stylesheet" href="{{asset('vendor/adminlte/plugins/iCheck/all.css')}}">
@stop
@section('js')
<script src="{{asset('vendor/adminlte/plugins/iCheck/icheck.min.js')}}"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css" id="theme" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('API_MAP_KEY', 'AIzaSyBpC8Nftwn_ZbBh42oKbCwDgygVi8B271Y') }}&libraries=places,drawing&language=en&callback=initialize" async defer></script>
<script>
$(document).ready(function(){
    changeLocationFields();
    $("#is_undefined_location").on("click",function(){
        changeLocationFields();
    });
})
function changeLocationFields(){
    if($("#is_undefined_location").is(":checked")){
        $(".hideLocation").hide();
    }else{
        $(".hideLocation").show();
    }
}
$(function () {
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass: 'iradio_minimal-red'
    })
});
$('.timepicker').timepicker();

var placeSearch, autocomplete, map, _myPolygon;
function initialize() {
    initMap();
    initAutocomplete();
}
function initMap() {
    map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: {
            lat: 25.2048,
            lng: 55.2708
        },
        zoom: 10
    });
    var drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.PLOYGON,
        drawingControl: true,
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            drawingModes: ['polygon']
        },
        polygonOptions: {
            draggable: true,
            editable: true,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35
        }
    });
    polyline = google.maps.event.addListener(drawingManager, 'polygoncomplete', function (shape) {
        if (shape == null || (!(shape instanceof google.maps.Polygon)))
            return;
        var coordinatesArray = shape.getPath().getArray();
        $('#area_cordinates_map').val(JSON.stringify(coordinatesArray));
        drawingManager.setOptions({drawingMode: null, drawingControl: false});
        _myPolygon = shape;
    });
    drawingManager.setMap(map);
    $("#remove_polygon").click(function () {
        $('#area_cordinates_map').val("");
        _myPolygon.setMap(null);
        drawingManager.setOptions({drawingMode: null, drawingControl: true});
    });
}
</script>
@stop
