@extends('adminlte::page')

@section('title', 'Add new')

@section('content_header')
<h1> Add new</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-tools">
                    <a href="{{route('admin.removed-plate-prefixes.index')}}" class="btn btn-sm btn-default">Back</a>
                </div>
            </div>
           
            {{ Form::open(array('url' => 'admin/removed-plate-prefixes')) }}
            <div class="box-body">
                @include("admin.removed_plate.form")
              
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}
                    </div>
                </div>
            </div>
            {{ Form::close() }}

                
        </div>
    </div>
</div>
@stop
