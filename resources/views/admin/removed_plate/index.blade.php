@extends('adminlte::page')

@section('title', 'Plate prefix not to updated in designa')

@section('content_header')
<h1> </h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <h3>&nbsp;</h3>
                <div class="box-tools">
                    <a href="{{route('admin.removed-plate-prefixes.create')}}" class="btn btn-sm btn-primary">Create new</a>
                </div>

            </div>
            <div class="box-body">
            
                
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="list-table">

                        <thead>
                            <tr>
                                <th>Plate prefix</th>
                               
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($plates))
                                @foreach($plates as $text)
                                    <tr>
                                        <td>{{$text->plate_prefix}}</td>
                                        
                                        
                                        <td>
                                            <a href="{{route('admin.removed-plate-prefixes.destroy', $text->id)}}" class="btn btn-sm btn-danger destroy">Delete</a>
                                            
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>

                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section("css")

<link rel="stylesheet" href="{{asset('css/bootstrap4-toggle.min.css')}}">
@stop
@section('js')
<script src="{{asset('js/bootstrap4-toggle.min.js')}}"></script>
<script type='text/javascript'>
    $(function () {
        
        oTable = $('#list-table').DataTable({
            ordering: false,
            serverSide: false,
            
        });

        $('#list-table').on('click', '.destroy', function (e) {
            e.preventDefault();
            var parent = $(this).closest('tr');
            var href = $(this).attr('href');

            swal({
                title: "{{ trans('myadmin.confirm-delete') }}",
                text: "Once deleted, you will not be able to recover this file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: href,
                                method: 'DELETE',
                                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                                beforeSend: function () {
                                    Pace.restart();//Pace.start();
                                },
                                success: function () {
                                    $(parent).remove();
                                    swal("Done!", "Plate prefix successfully deleted.", "success");
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    swal("Error deleting!", "Please try again", "error");
                                },
                                complete: function () {
                                    Pace.stop();
                                }
                            });
                        } else {
                            return true;
                        }
                    });
        });
        
    });

</script>    
@stop