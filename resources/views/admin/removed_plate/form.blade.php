<div class="form-group {{ $errors->has('plate_prefix') ? 'has-error' : '' }}">
    {{ Form::label('plate_prefix', 'Plate prefix') }}
    {{ Form::text('plate_prefix', null, array('class' => 'form-control', 'required'=>"required")) }}
     {!! $errors->first('plate_prefix','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>
              