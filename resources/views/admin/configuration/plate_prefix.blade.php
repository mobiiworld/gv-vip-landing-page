@extends('adminlte::page')

@section('title', 'Plate Prefix')

@section('content_header')
<h1> Plate Prefix</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <h3>&nbsp;</h3>
                
                
            </div>
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="pack-table">

                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Value</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($platePrefixes as $pp)
                                <tr>
                                    <td>{{$pp->displayName}}</td>
                                    <?php $prefix = join(", ",array_keys(json_decode($pp->platePrefix,true))); ?>
                                    <td>{{$prefix}}</td>
                                    <td><a href="{{route('admin.platePrefixEdit',[$pp->type,$pp->code])}}" class="btn btn-xs btn-primary">Edit</a></td>
                                </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
