@extends('adminlte::page')

@section('title', 'Edit Plate Prefix')

@section('content_header')
<h1>Edit  Plate Prefix</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-primary">
            <div class="box-header with-border">
                <a href="{{ route('admin.platePrefix') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
            <div class="box-body">
                {{ Form::open( array('url' => route('admin.platePrefixUpdate', [$type,$code]), 'method' => 'POST')) }}

                <div class="form-group {{ $errors->has('prefix') ? 'has-error' : '' }}">
                    {{ Form::label('prefix','Plate Prefix') }}
                     <?php $prefix = join(", ",array_keys(json_decode($plate->plate_prefix,true))); ?>
                    {{ Form::textarea('prefix', $prefix, array('class' => 'form-control')) }}
                    {!! $errors->first('prefix','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>

                {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
@stop

@section("js")
<script type="text/javascript">
    $(document).ready(function() {

        $('textarea').keypress(function(event) {

            if (event.keyCode == 13 || event.which == 13 ) {
                event.preventDefault();
            }
        });
    });

</script>
@stop