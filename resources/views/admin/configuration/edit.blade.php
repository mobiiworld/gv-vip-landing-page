@extends('adminlte::page')

@section('title', 'Edit Configuration')

@section('content_header')
<h1>Edit Configuration</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-primary">
            <div class="box-header with-border">
                <a href="{{ route('admin.configuration.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
            <div class="box-body">
                {{ Form::model($configuration, array('route' => array('admin.configuration.update', $configuration->id), 'method' => 'PUT')) }}

                <div class="form-group {{ $errors->has('value') ? 'has-error' : '' }}">
                    {{ Form::label('value', $configuration->name) }}
                    {{ Form::text('value', null, array('class' => 'form-control')) }}
                    {!! $errors->first('value','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>

                {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
@stop