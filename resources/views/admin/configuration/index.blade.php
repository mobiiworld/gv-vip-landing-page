@extends('adminlte::page')

@section('title', 'Configurations')

@section('content_header')
<h1> Configurations</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <h3>&nbsp;</h3>
                
                
            </div>
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="pack-table">

                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Value</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($configurations as $con)
                                <tr>
                                    <td>{{$con->name}}</td>
                                    <td>{{$con->value}}</td>
                                    <td><a href="{{route('admin.configuration.edit',$con->id)}}" class="btn btn-xs btn-primary">Edit</a></td>
                                </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
