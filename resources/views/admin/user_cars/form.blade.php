
            <div class="box-body">
                <div id="template">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('type', 'Type') }}
                                {{ Form::select('type',[1=>'SUV',2=>'Sedan'], null, array('class' => 'form-control')) }}
                                {!! $errors->first('type','<p class="text-danger"><strong>:message</strong></p>') !!}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('country_id', 'Country of car registration') }}
                                {{ Form::select('country_id', $car_countries,null, array('class' => 'form-control carCountry')) }}
                                {!! $errors->first('country_id','<p class="text-danger"><strong>:message</strong></p>') !!}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('emirate_id', 'Emirate of car registration') }}
                                {{ Form::select('emirate_id', $emirates,null, array('class' => 'form-control carEmirate')) }}
                                {!! $errors->first('emirate_id','<p class="text-danger"><strong>:message</strong></p>') !!}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('plate_prefix', 'Plate Code') }}
                                {{ Form::select('plate_prefix', $pprefixes,null, array('class' => 'form-control carNumber')) }}
                                {!! $errors->first('plate_prefix','<p class="text-danger"><strong>:message</strong></p>') !!}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('plate_number', 'Plate Number') }}
                                {{ Form::text('plate_number', null, array('class' => 'form-control')) }}
                                {!! $errors->first('plate_number','<p class="text-danger"><strong>:message</strong></p>') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <input type='hidden' name='referer' value='{{ $referer }}' />
                <input type='hidden' name='pack_id' value='{{ request()->packId }}' />
            </div>
             