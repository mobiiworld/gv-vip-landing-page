@extends('adminlte::page')

@section('title', 'Edit User Car Details')

@section('content_header')
<h1>Edit User Car Details</h3>
@stop

@section('content')
<?php
$referer = route('admin.packs.show', request()->packId);
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <a href="{{ $referer }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
            {{ Form::model($ucar, array('route' => array('admin.user-cars.update', $ucar->id), 'method' => 'PUT')) }}
            @include('admin.user_cars.form')
            <div class="box-footer">
                <input type='hidden' name='referer' value='{{ $referer }}' />
                <input type='hidden' name='pack_id' value='{{ request()->packId }}' />
                {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop
@section('js')
<script type='text/javascript'>
    $(document).on('change', '.carCountry', function (e) {
        var countryId = $(this).val();
        var ediv = $(this).parent().parent().parent('div').find('.carEmirate');
        var ndiv = $(this).parent().parent().parent('div').find('.carNumber');
        if (countryId != '') {
            ediv.html('<option value="">Loading....</option>');
            //get Emirates
            $.ajax({
                url: "{!! route('get.emirates',app()->getLocale()) !!}",
                dataType: 'html',
                data: {'id': countryId},
                cache: false,
                method: 'POST',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                beforeSend: function () {
                },
                success: function (response) {
                    ediv.html(response);
                },
                error: function (response) {
                },
                complete: function () {
                }
            });
            if (countryId == 1) {
                ndiv.html('<option value="">Select</option>');
            } else {
                ndiv.html('<option value="">Loading....</option>');
                //get plate number Prefix
                listPrefix('country', countryId, ndiv);
            }
        } else {
            ediv.html('<option value="">Select</option>');
            ndiv.html('<option value="">Select</option>');
        }
    });
    $(document).on('change', '.carEmirate', function (e) {
        var emirateId = $(this).val();
        var ndiv = $(this).parent().parent().parent('div').find('.carNumber');
        if (emirateId != '') {
            ndiv.html('<option value="">Loading....</option>');
            listPrefix('city', emirateId, ndiv);
        } else {
            ndiv.html('<option value="">Select</option>');
        }
    });

    function listPrefix(type, id, ndiv) {
        $.ajax({
            url: "{!! route('get.platePrefix',app()->getLocale()) !!}",
            dataType: 'html',
            data: {'id': id, 'type': type},
            cache: false,
            method: 'POST',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            beforeSend: function () {
            },
            success: function (response) {
                ndiv.html(response);
            },
            error: function (response) {
            },
            complete: function () {
            }
        });
    }
</script>
@stop