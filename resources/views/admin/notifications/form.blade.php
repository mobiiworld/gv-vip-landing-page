<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('titles.en') ? 'has-error' : '' }}">
            {{ Form::label('titles', 'Title (en)') }}
            {{ Form::text('titles[en]', null, array('class' => 'form-control', 'dir'=>'ltr')) }}
            {!! $errors->first('titles.en','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('titles.ar') ? 'has-error' : '' }}">
            {{ Form::label('titles', 'Title (ar)') }}
            {{ Form::text('titles[ar]', null, array('class' => 'form-control', 'dir'=>'rtl')) }}
            {!! $errors->first('titles.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('messages.en') ? 'has-error' : '' }}">
            {{ Form::label('messages', 'Message (en)') }}
            {{ Form::textarea('messages[en]', null, array('class' => 'form-control', 'dir'=>'ltr')) }}
            {!! $errors->first('messages.en','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('messages.ar') ? 'has-error' : '' }}">
            {{ Form::label('messages', 'Message (ar)') }}
            {{ Form::textarea('messages[ar]', null, array('class' => 'form-control', 'dir'=>'rtl')) }}
            {!! $errors->first('messages.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
</div>