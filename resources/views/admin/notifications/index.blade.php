@extends('adminlte::page')

@section('title', 'Notifications')

@section('content_header')
<h1>Notifications</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <h3>&nbsp;</h3>
                <div class="box-tools">
                    @if(auth('admin')->user()->hasRole('super-admin','admin') || auth('admin')->user()->hasPermissionTo('notifications_create','admin'))
                    <a href="{{route('admin.notifications.create')}}" class="btn btn-sm btn-primary">Add New</a>
                    @endif
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive" style="overflow-x: hidden;">
                    <table class="table table-striped table-bordered table-hover" id="list-table">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Message</th>
                                <th>Created At</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('js')
<script type='text/javascript'>
    $(function () {
        oTable = $('#list-table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ordering: false,
            responsive: true,
            lengthChange: true,
            ajax: {
                url: "{!! route('admin.notifications.datatable') !!}",
                type: 'post',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'}
            },
            columns: [
                {data: 'titles.en', name: 'titles->en'},
                {data: 'messages.en', name: 'messages->en'},
                {data: 'created_at', name: 'created_at', searchable: false},
                {data: 'actions', name: 'actions', searchable: false}
            ]
        });

        $('#list-table').on('click', '.destroy', function (e) {
            e.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "{{ trans('myadmin.confirm-delete') }}",
                text: "Once deleted, you will not be able to recover this data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: href,
                                method: 'DELETE',
                                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                                beforeSend: function () {
                                    Pace.restart();//Pace.start();
                                },
                                success: function () {
                                    oTable.ajax.reload();
                                    swal("Done!", "Successfully deleted.", "success");
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    swal("Error deleting!", "Please try again", "error");
                                },
                                complete: function () {
                                    Pace.stop();
                                }
                            });
                        } else {
                            return true;
                        }
                    });
        });
    });
</script>    
@stop