@extends('adminlte::page')

@section('title', 'Parking Transaction')

@section('content_header')
<h1><i class="fa fa-car"></i> Parking Transaction</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header "><div id="table_buttons" class="box-tools"></div></div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            {{ Form::label('transaction_status', 'Status') }}
                            {!! Form::select('transaction_status', $parkingTransactionStatus,null, ['class' => 'form-control', 'id' => 'transaction_status']) !!}
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Platform</label>
                            <select class="form-control" name="platform" id="platform">
                                <option value="" >All</option>
                                <option value="web">Web</option>
                                <option value="android">Android</option>
                                <option value="ios">Ios</option>
                            </select>
                        </div>
                    </div>

                    <div class="input-daterange" id="global_dates">
                        <div class="col-md-2">
                            <div class="form-group" >
                                {{ Form::label('longt', 'Start Date') }}
                                {{ Form::text("start_date", date('m/d/Y',strtotime("-5 days")), array('placeholder'=>'Start Date','id'=>'start_date',"autocomplete" => "off", 'class' => 'start_end_date form-control')) }}
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" >
                                {{ Form::label('longt', 'End Date') }}
                                {{ Form::text("end_date", date('m/d/Y'), array('placeholder'=>'End Date','id'=>'end_date',"autocomplete" => "off", 'class' => 'start_end_date form-control')) }}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="shop-cart-transaction-table">
                        <thead>
                            <tr>
                                <th>Date/Time Added</th>
                                <th>Email</th>
                                <th>Shopcart ID</th>
                                <th>Order ID</th>
                                <th>Amount</th>
                                <th>Receipt</th>
                                <th>Status</th>
                                <th>Ticket Number</th>
                                <th>Plate Info</th>
                                <th>Plate Number</th>
                                <th>Country Code</th>
                                <th>City Code</th>
                                <th>Platform</th>
                                <!--<th>Actions</th>-->
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section('js')
<script type="text/javascript" src="{{asset('vendor/datatables-plugins/buttons/js/dataTables.buttons.min.js')}}"></script>
<script type="text/javascript" src="{{asset('vendor/datatables-plugins/buttons/js/buttons.bootstrap4.min.js')}}"></script>

<script src="{{asset('vendor/datatables-plugins/jszip/jszip.min.js')}}"></script>
<script src="{{asset('vendor/datatables-plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('vendor/datatables-plugins/pdfmake/vfs_fonts.js')}}"></script>

<script type="text/javascript" src="{{asset('vendor/datatables-plugins/buttons/js/buttons.html5.min.js')}}"></script>
<script type="text/javascript" src="{{asset('vendor/datatables-plugins/buttons/js/buttons.print.min.js')}}"></script>
<script type="text/javascript" src="{{asset('vendor/datatables-plugins/buttons/js/buttons.colVis.min.js')}}"></script>

<link href="{{ asset('css/bootstrap-datepicker.css')}}" id="theme" rel="stylesheet">
<script src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
<script type='text/javascript'>
$(function () {
    $("#global_dates").datepicker({
        toggleActive: !0
    });
});
var dt ;
$(function () {
    dt = $('#shop-cart-transaction-table').DataTable({
        processing: true,
        serverSide: true,
        ordering: false,
        autoWidth: false,
        ajax: {
            url: "{!! route('admin.parking.transaction.datatable') !!}",
            type: 'post',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            data: function (d) {
                d.transaction_status = $('#transaction_status').val();
                d.start_date = $('#start_date').val();
                d.end_date = $('#end_date').val();
                d.platform = $('#platform').val();
            }
        },
        columns: [
            {data: 'created_date', name: 'gv_user_payment_sessions.created_date', searchable: false},
            {data: 'email', name: 'gv_shopcart_transaction.email'},
            {data: 'shopcartid', name: 'gv_user_payment_sessions.shopcartid'},
            {data: 'order_id', name: 'gv_user_payments.order_id'},
            {data: 'amount', name: 'gv_user_payment_sessions.amount'},
            {data: 'receipt', name: 'gv_user_payments.receipt'},
            {data: 'status', name: 'gv_user_payment_sessions.status', searchable: false},
            {data: 'ticketNumber', name: 'gv_parking_details.ticketNumber'},
            {data: 'platePrefix', name: 'gv_parking_details.platePrefix'},
            {data: 'plateNumber', name: 'gv_parking_details.plateNumber'},
            {data: 'countryCode', name: 'gv_parking_details.countryCode'},
            {data: 'cityCode', name: 'gv_parking_details.cityCode'},
            {data: 'platform', name: 'gv_user_payment_sessions.platform', searchable: false},
                    //{data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        columnDefs: [
            {
                render: function (data, type, row) {
                    return data + '</br>' + row['plateNumber'] + '</br>' + row['countryCode'] + '</br>' + row['cityCode'];
                },
                targets: 8
            },
            {visible: false, targets: [9, 10, 11]
            }
        ],
        dom: 'Bfrtip',               
        buttons: [ 
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: ['thead th:not(.noExport)']
                },
                "action": newexportaction,
                orientation: 'landscape',
                title : 'Parking',
                className : 'btn-primary',                        
                pageSize : 'LEGAL',
            },
            
            {
                extend: 'excelHtml5',                   
                action: newexportaction,    
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                },                
                title  : 'Parking',                    
                //text  : 'Export',                    
                createEmptyCells : true,
                className : 'btn-primary'
                
            }
        ],

        initComplete: function(settings, json) {
            
            dt.buttons().container().appendTo('#table_buttons')
        }
    });
    $(document).on("change", "#start_date, #end_date, #transaction_status, #platform", function () {
        dt.ajax.reload();
    });

    function newexportaction(e, dt, button, config) {
        var self = this;
        var oldStart = dt.settings()[0]._iDisplayStart;
        dt.one('preXhr', function (e, s, data) {
            // Just this once, load all data from the server...
            data.start = 0;
            data.length = '{{$total}}';
            dt.one('preDraw', function (e, settings) {
                // Call the original action function
                if (button[0].className.indexOf('buttons-copy') >= 0) {
                    $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                    $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                    $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                    $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-print') >= 0) {
                    $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
                }
                dt.one('preXhr', function (e, s, data) {
                    // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                    // Set the property to what it was before exporting.
                    settings._iDisplayStart = oldStart;
                    data.start = oldStart;
                });
                // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                setTimeout(dt.ajax.reload, 0);
                // Prevent rendering of the full data to the DOM
                return false;
            });
        });

        // Requery the server with the new one-time export settings
        dt.ajax.reload();
    }
}
);
</script>
@stop