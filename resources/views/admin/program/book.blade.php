@extends('adminlte::page')

@section('title', 'Book Slots')

@section('content_header')
<h1> Book Slots</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-tools">
                    <a href="{{route('admin.programs.index')}}" class="btn btn-sm btn-default">Back</a>
                </div>
            </div>

            <div class="box-body">
                {{ Form::open(array('url' => 'admin/programs','id'=>'myBookForm')) }}
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group ">
                            <label>Program Date</label>
                            <input type="text" name="programDate" id='programDate' required="" class="filter form-control datepicker" placeholder="Select Date" value=""/>
                        </div>
                    </div>
                    <div class="col-md-4" style="display:none;">
                        <div class="form-group">
                            {{ Form::label('isVip', 'User Type') }}
                            {!! Form::select('isVip', [0=>'Normal',1=>'VIP'],null, ['class' => 'filter form-control', 'id' => 'isVip']) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('table_type', 'Table Type') }}
                            {!! Form::select('table_type', config('globalconstants.types'),null, ['class' => 'filter form-control', 'id' => 'table_type']) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('smokingType', 'Smoking Type') }}
                            {!! Form::select('smokingType', [1=>'Smoking',2=>'Non Smoking'],null, ['class' => 'filter form-control', 'id' => 'smokingType']) !!}
                        </div>
                    </div>
                </div>
                <hr />
                <div class="row tableSelect">
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('tableId2', 'Table 2 Seater') }}
                            {!! Form::select('tableId2', $tables->where('seat_type',2)->pluck('name','id')->prepend('---', ''),null, ['class' => 'filter form-control tableIds', 'id' => 'tableId2']) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('tableId4', 'Table 4 Seater') }}
                            {!! Form::select('tableId4', $tables->where('seat_type',4)->pluck('name','id')->prepend('---', ''),null, ['class' => 'filter form-control tableIds', 'id' => 'tableId4']) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('tableId6', 'Table 6 Seater') }}
                            {!! Form::select('tableId6', $tables->where('seat_type',6)->pluck('name','id')->prepend('---', ''),null, ['class' => 'filter form-control tableIds', 'id' => 'tableId6']) !!}
                        </div>
                    </div>
                    <div class="col-md-12 text-center text-bold text-blue">
                        <div class="form-group">
                            --OR--
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        {{ Form::label('noOfTables2', 'Number of tables (2 Seater)') }}
                        <div class="input-group">
                            <span class="input-group-addon minus"><i class="fa fa-minus"></i></span>
                            {{ Form::number('noOfTables2', 0, array('id'=>'noOfTables2','class' => 'filter form-control text-center tableNos', 'required'=>"required", 'min'=>0)) }}
                            <span class="input-group-addon plus"><i class="fa fa-plus"></i></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        {{ Form::label('noOfTables4', 'Number of tables (4 Seater)') }}
                        <div class="input-group">
                            <span class="input-group-addon minus"><i class="fa fa-minus"></i></span>
                            {{ Form::number('noOfTables4', 0, array('id'=>'noOfTables4','class' => 'filter form-control text-center tableNos', 'required'=>"required", 'min'=>0)) }}
                            <span class="input-group-addon plus"><i class="fa fa-plus"></i></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        {{ Form::label('noOfTables6', 'Number of tables (6 Seater)') }}
                        <div class="input-group">
                            <span class="input-group-addon minus"><i class="fa fa-minus"></i></span>
                            {{ Form::number('noOfTables6', 0, array('id'=>'noOfTables6', 'class' => 'filter form-control text-center tableNos', 'required'=>"required", 'min'=>0)) }}
                            <span class="input-group-addon plus"><i class="fa fa-plus"></i></span>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px;">
                        <div class="form-group">
                            {{ Form::button('Search', array('class' => 'btn btn-primary btn-block','id'=>'searchSlot','style:display:block;')) }}
                        </div>
                    </div>
                </div>
                <div id='slotDiv'>

                </div>
                {{ Form::close() }}
            </div>
            <div class="overlay" style='display: none;'>
                <i class="fa fa-fw fa-spinner fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop
@section("css")
<link rel="stylesheet" href="{{URL::asset('css/daterangepicker.css')}}">
<style>
    .checkbox-border-disabled{
        color: red;
        text-decoration: line-through;
    }
    .checkbox-border-disabled input[type=checkbox]{
        outline: 1px solid red;
    }
    .checkbox-border-enabled{
        color: #07f;
    }
    .checkbox-border-enabled input[type=checkbox]{
        outline: 1px solid #07f;
    }
</style>
@stop
@section("js")
<script src="{{URL::asset('js/moment.min.js')}}"></script>
<script src="{{URL::asset('js/daterangepicker.min.js')}}"></script>
<script src="{{URL::asset('js/jquery.inputmask.js')}}"></script>
<script src="{{URL::asset('js/jquery.inputmask.extensions.js')}}"></script>
<script type="text/javascript">
$(function () {
    $('.box-body').on('click', '.plus', function () {
        $addedValue = parseInt($(this).siblings('input').val()) + 1;
        $(this).siblings('input').val($addedValue).trigger("change");
    });
    $('.box-body').on('click', '.minus', function () {
        var x = parseInt($(this).siblings('input').val());
        if (x >= 1) {
            $(this).siblings('input').val(parseInt($(this).siblings('input').val()) - 1).trigger("change");
        }
    });
    $('[data-mask]').inputmask();
});
function getFormatDate(today) {
    var dd = today.getDate();
    var mm = today.getMonth() + 1;

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    return dd + '/' + mm + '/' + yyyy;
}
var todayDate = getFormatDate(new Date());
var d = new Date();
d.setHours(d.getHours() - 2);
$('.datepicker').daterangepicker({
    singleDatePicker: true,
    //autoUpdateInput: false,
    autoApply: true,
    minDate: d,
    locale: {
        //format: 'YYYY-MM-DD'
        //format: 'DD/MM/YYYY'
    }
    //format: 'yyyy-mm-dd'
});
$(document).on("change", ".filter", function (e) {
    $("#slotDiv").html('');
    $("#searchSlot").show();
});
$(document).on("change", ".tableNos", function (e) {
    $('.tableIds').val('');
});
$(document).on("change", ".tableIds", function (e) {
    $(".tableNos").val(0);
});
/*$(document).on("change", "#bookingType", function (e) {
 $('.tableIds').val('');
 if ($(this).val() == 'hourly') {
 $('.tableSelect').show();
 } else {
 $('.tableSelect').hide();
 }
 });*/
$(document).on('change', '#slotDiv input.solt-check', function () {
    var total = 0;
    var nT4 = (!$('#tableId4').val() == '') ? 1 : $('#noOfTables4').val();
    var nT6 = (!$('#tableId6').val() == '') ? 1 : $('#noOfTables6').val();
    var nT2 = (!$('#tableId2').val() == '') ? 1 : $('#noOfTables2').val();
    $('#slotDiv input.solt-check').each(function () {
        if ($(this).is(":checked")) {
            total += ($(this).data('a4') * nT4) + ($(this).data('a6') * nT6) + ($(this).data('a2') * nT2);
        }
    });
    $('#slotDiv input#amount').val(total);
});
$(document).on("change", "#paymentMode", function (e) {
    var mode = $(this).val();
    switch (mode) {
        case '1':
            $('#slotDiv div#cardDetails').hide();
            //$('#slotDiv input#cardNumber').val('').attr('required', false);
            $('#slotDiv input#approvalCode').val('').attr('required', false);
            $('#slotDiv select#cardType').val('').attr('required', false);
            break;
        case '2':
            $('#slotDiv div#cardDetails').show();
            //$('#slotDiv input#cardNumber').val('').attr('required', 'required');
            $('#slotDiv input#approvalCode').val('').attr('required', 'required');
            $('#slotDiv select#cardType').val('').attr('required', 'required');
            break;

    }
});
$(document).on("click", "button#searchSlot", function (e) {
    var tableId2 = $('#tableId2').val();
    var tableId4 = $('#tableId4').val();
    var tableId6 = $('#tableId6').val();
    var noOfTables4 = $('#noOfTables4').val();
    var noOfTables6 = $('#noOfTables6').val();
    var noOfTables2 = $('#noOfTables2').val();
    if ((tableId2 == '' && tableId4 == '' && tableId6 == '') && (noOfTables4 == 0 && noOfTables6 == 0 && noOfTables2 == 0)) {
        swal('Please select atleast 1 table');
    } else {
        var programDate = $('#programDate').val();
        var isVip = $('#isVip').val();
        var table_type = $('#table_type').val();
        var smokingType = $('#smokingType').val();
        //var paymentMode = $('#paymentMode').val();

        $.ajax({
            url: '{!! url(route("admin.programs.slots.book",request()->id)) !!}',
            method: 'POST',
            data: {
                'programId': '{{$program->program_uid}}',
                'programDate': programDate,
                'isVip': isVip,
                'table_type': table_type,
                'smokingType': smokingType,
                'tableId2': tableId2,
                'tableId4': tableId4,
                'tableId6': tableId6,
                'noOfTables4': noOfTables4,
                'noOfTables6': noOfTables6,
                'noOfTables2': noOfTables2,
                //'paymentMode': paymentMode
            },
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            beforeSend: function () {
                $("div.overlay").toggle();
                $("#searchSlot").toggle();
            },
            success: function (responce)
            {
                $("#slotDiv").html(responce);
                $('[data-mask]').inputmask();
            },
            complete: function () {
                $("div.overlay").toggle();
            },
            dataType: 'html'
        });
    }
});
$("#myBookForm").submit(function (event) {
    event.preventDefault();
    if ($('#slotDiv input.solt-check:checked').length < 1) {
        swal('Please select your slots');
        return false;
    }
    var slotNames = [];
    $('#slotDiv input.solt-check').each(function () {
        if ($(this).is(":checked")) {
            slotNames.push($(this).data('label'));
        }
    });
    var data = $(this).serializeArray();
    data.push({name: "programId", value: '{{$program->program_uid}}'});
    data.push({name: "slotNames", value: slotNames});
    //var data = $(this).serialize() + "&programId={{$program->program_uid}}" + "&slotNames=" + slotNames;
    $.ajax({
        url: '{!! url(route("admin.programs.book.confirm")) !!}',
        method: 'POST',
        data: data,
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        beforeSend: function () {
            $("div.overlay").toggle();
        },
        success: function (responce)
        {
            if (responce.status) {
                location.reload();
            } else {
                swal(responce.message);
            }
        },
        complete: function () {
            $("div.overlay").toggle();
            $("#searchSlot").show();
        }
    });
});
$("#smokingType,#table_type").on('change', function (event) {
    $.ajax({
        url: '{!! url(route("admin.tables.get.by.type")) !!}',
        method: 'POST',
        data: {type: $('#table_type').val(), sType: $('#smokingType').val()},
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        beforeSend: function () {
            $("div.overlay").toggle();
            $('.tableIds').empty();
            $('.tableIds').append('<option value="">---</option>');
        },
        success: function (response)
        {
            $.each(response.data.table2, function () {
                $('#tableId2').append($("<option />").val(this.id).text(this.name));
            });
            $.each(response.data.table4, function () {
                $('#tableId4').append($("<option />").val(this.id).text(this.name));
            });
            $.each(response.data.table6, function () {
                $('#tableId6').append($("<option />").val(this.id).text(this.name));
            });
        },
        complete: function () {
            $("div.overlay").toggle();
        }
    });
});
</script>
@stop