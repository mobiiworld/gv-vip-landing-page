@extends('adminlte::page')

@section('title', 'Programs')

@section('content_header')
<h1> Programs</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <h3>&nbsp;</h3>
                <div class="box-tools">
                    <a href="{{route('admin.programs.create')}}" class="btn btn-sm btn-primary">Create Program</a>
                </div>

            </div>
            <div class="box-body">
                <div class="row">
                    
                   <div class="col-md-2">
                        <div class="form-group">
                            <label>Status</label>
                            <select class="form-control filter" name="status" id="status">
                                <option value="" >All</option>
                                <option value="1" >Enabled</option>
                                <option value="2" >Disabled</option>                                
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="list-table">

                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Status</th>                               
                                <th></th>
                            </tr>
                        </thead>


                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section("css")

<link rel="stylesheet" href="{{asset('css/bootstrap4-toggle.min.css')}}">
@stop
@section('js')
<script src="{{asset('js/bootstrap4-toggle.min.js')}}"></script>
<script type='text/javascript'>
    $(function () {
        
        oTable = $('#list-table').DataTable({
            processing: true,
            serverSide: true,
            //autoWidth: false,
            ordering: false,
            ajax: {
                url: '{!! route('admin.programs.datatable') !!}',
                type: 'post',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                 data: function ( d ) {
                     d.status = $("#status").val();
                },
            },
           
            columns: [
                {data: 'program_uid', name: 'program_uid'},
                {data: 'name', name: 'name'},
                {data: 'status', name: 'status', orderable: false, searchable: false},
                {data: 'actions', name: 'actions', orderable: false, searchable: false}
            ],
            fnDrawCallback: function () {
                $(".bsSwitch").bootstrapToggle({
                    size: "sm",
                    onstyle: "success",
                    offstyle: "danger",
                    on: 'Enabled',
                    off: 'Disabled'
                });
            }
        });
        $(".filter").on("change",function(){
            oTable.draw();
        });

       $(document).on('change', '.bsSwitch', function () {
            $.ajax({
                url: '{!! route('admin.programs.changeStatus') !!}',
                dataType: 'json',
               
                cache: false,
                method: 'POST',
                data : { id : $(this).data('id')},
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                beforeSend: function () {
                   
                },
                success: function (response) {
                    oTable.draw();
                },
                error: function (response) {
                    
                },
                complete: function () {
                    
                }
            });
       });

    });
</script>    
@stop