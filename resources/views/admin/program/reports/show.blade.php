<div class="nav-tabs-custom">
    <ul class="nav nav-tabs bg-gray">
        <li class="active"><a href="#details" id="details-tab" data-toggle="tab">Details</a></li>
        <li><a href="#qrcode" id="qrcode-tab" data-scode="{{$booking->sale_code}}" data-toggle="tab">QR Codes</a></li>
        <li><a href="#timeline" id="timeline-tab" data-toggle="tab">Audit Log</a></li>
    </ul>    
    <div class="tab-content">
        <div class="active tab-pane" id="details">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Program Date</label>
                                    <div class="input-group date">
                                        <input type="text" class="form-control datepicker" id="pDateToChange" name="pDateToChange" value="{{$booking->program_date}}">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <label>Special Notes</label>
                                    <textarea class="form-control" id="pNoteToChange" name="pNoteToChange" rows="3">{{$booking->special_notes}}</textarea>
                                </div>
                            </div>
                        </div>
                        @if (auth('admin')->user()->hasRole('super-admin', 'admin') || auth('admin')->user()->hasPermissionTo('Ramadan Majlis_change booking date', 'admin'))
                        @if(($booking->program_date >= date('Y-m-d')))
                        <div class="box-footer">
                            <button type="button" data-bid="{{$booking->id}}" id="pChangeDetailButton" class="btn btn-primary">Submit</button> 
                        </div>
                        @endif
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">User Type</h3>
                        </div>
                        <div class="box-body text-capitalize">{{$booking->is_vip?'VIP':'Normal'}}</div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Table Type</h3>
                        </div>
                        <div class="box-body text-capitalize">{{$booking->table_type}}</div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Amount Paid</h3>
                        </div>
                        <div class="box-body">AED {{$booking->amount}}</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <div class="col-md-6">
                                <h3 class="profile-username">{{$booking->first_name.' '.$booking->last_name}}</h3>
                                <p class="text-muted p-0 m-0">{{$booking->email}}</p>
                                <p class="text-muted p-0 m-0">{{$booking->mobile}}</p>
                            </div>
                            @if($booking->admin_id)
                            <div class="col-md-6 text-right">
                                <h3 class="profile-username">Booked Onsite</h3>
                                <p class="text-muted p-0 m-0">{{$booking->admin->name}}</p>
                                <p class="text-muted p-0 m-0">{{$booking->admin->email}}</p>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Booked Tables</h3>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="list-table">
                                    <thead>
                                        <tr>
                                            <th class="text-center text-blue">Table</th>
                                            <th class="text-center text-blue">Slot</th>
                                            <th class="text-center text-blue">Seats</th>
                                            <th class="text-center text-blue">Smoking</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($tables as $t=>$table)
                                        <tr>
                                            <td class="text-center">{{$t}}-{{$table[0]->type}}</td>
                                            <td class="text-center">{{$table->sortBy('priority')->pluck('name')->join(', ')}}</td>
                                            <td class="text-center">{{$table[0]->seat_type}}</td>
                                            <td class="text-center">{{($table[0]->smoking_status==2)?'No':'Yes'}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane" id="qrcode"></div>

        <div class="tab-pane" id="timeline">
            <ul class="timeline timeline-inverse">
                @foreach($timelines as $k=>$timeline)
                <?php
                usort($timeline, function ($a, $b) {
                    return strtotime($b['time']) - strtotime($a['time']);
                });
                ?>
                <li class="time-label">
                    <span class="bg-red">{{date('d M. Y',strtotime($k))}}</span>
                </li>
                @foreach($timeline as $tm)
                <li>
                    <i class="fa fa-user bg-blue"></i>
                    <div class="timeline-item">
                        <span class="time"><i class="fa fa-clock"></i> {{date('h:i a',strtotime($tm['time']))}}</span>
                        <h3 class="timeline-header">{{$tm['heading']}}</h3>
                        <div class="timeline-body">
                            {!! $tm['description'] !!}
                            <p class="text-muted">
                                By : {{$tm['admin']->name}} | {{$tm['admin']->email}}
                            </p>
                        </div>
                    </div>
                </li>
                @endforeach
                @endforeach
            </ul>
        </div>
    </div>
</div>