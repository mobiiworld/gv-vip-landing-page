<div class="row">
    <div class="col-md-12">
        {{ Form::open(array('route' => 'admin.program.changed.table.store','id'=>'tableForm')) }}
        <div class="box box-primary">
            <div class="box-body">
                <p class="text-blue text-center">{{$extras['tabelDetails']}}</p>
                <p class="text-red text-center">{!! $extras['slotNames'] !!}</p>
                @if($availableTables->isEmpty())
                @php($emClass='hidden')
                <div class="callout callout-warning">
                    <h4>No more tables available.</h4>
                </div>
                @else
                @php($emClass='')
                <div class="form-group text-center">
                    @foreach ($availableTables as $availableTable)
                    <label style="margin-right: 10px;">
                        {{ Form::radio('toTableId',  $availableTable->table_id,  null, ['class'=>'checkboxTable flat-red'] ) }}
                        {{ $availableTable->name}}
                    </label>
                    @endforeach
                </div>
                @endif
            </div>
            <div class="box-footer {{$emClass}}">
                <input type="hidden" name="booking_id" value="{{$extras['bookingId']}}" />
                <input type="hidden" name="tid_changed_from" value="{{$extras['fromTableId']}}" />
                <input type="hidden" name="slot_ids" value="{{$extras['slotIds']}}" />
                <input type="hidden" name="released_pslot_ids" value="{{$extras['fromPslots']}}" />
                <div class="form-group pull-right">
                    {{ Form::submit('Submit', array('class' => 'btn btn-primary','id'=>"change-table-form")) }}
                </div>
            </div>
        </div> 
        {{ Form::close() }}
    </div>
</div>