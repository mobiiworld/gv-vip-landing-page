@extends('adminlte::page')
@section('title', 'Daily Onsite Collections')
@section('content_header')
<h1>Daily Onsite Collections</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <div class="col-md-4">
                    <div class="form-group ">
                        <label>From Date</label>
                        <input type="text" name="programFDate" id='programFDate' required="" class="filter form-control datepicker" placeholder="Select From Date" value=""/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group ">
                        <label>To Date</label>
                        <input type="text" name="programTDate" id='programTDate' required="" class="filter form-control datepicker" placeholder="Select To Date" value=""/>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <input type="button" name="export" class="form-control btn btn-primary" value="Export" id="export_btn">
                    </div>
                </div>
            </div>
            <div class="box-body">

                <div class="table-responsive overflow-hidden" style="overflow-x: hidden;">
                    <table class="table table-striped table-bordered table-hover overflow-hidden" id="list-table">
                        <thead>
                            <tr>
                                <th>SL.No</th>
                                <th>Name</th>
                                <th>Email</th>                               
                                <th>Cash</th>
                                <th>Card</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section("css")
<style>
    td.dt-control {
        background: url('{{asset("images/details_open.png")}}') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.dt-control {
        background: url('{{asset("images/details_close.png")}}') no-repeat center center;
    }
</style>
<link rel="stylesheet" href="{{URL::asset('css/daterangepicker.css')}}">
@stop
@section('js')
<script src="{{URL::asset('js/moment.min.js')}}"></script>
<script src="{{URL::asset('js/daterangepicker.min.js')}}"></script>
<script type='text/javascript'>
$(function () {
    //var d = new Date();
    //d.setHours(d.getHours() - 2);
    $('.datepicker').daterangepicker({
        singleDatePicker: true,
        autoApply: true,
        //minDate: null,
    });

    oTable = $('#list-table').DataTable({
        processing: true,
        serverSide: true,
        ordering: false,
        paging: false,
        searching: false,
        info: false,
        ajax: {
            url: "{!! route('admin.program.onsite.collections') !!}",
            type: 'POST',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            data: function (d) {
                d.opFDate = $("#programFDate").val();
                d.opTDate = $("#programTDate").val();
                d.pid = '{{$program->id}}';
            },
        },

        columns: [
            {
                data: null,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
                searchable: false
            },
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'totCash', name: 'admin_id', searchable: false},
            {data: 'totCard', name: 'totCard', searchable: false},
            {
                className: 'dt-control',
                data: null,
                defaultContent: '',
                searchable: false
            },
        ]
    });

    $(document).on('change', '.filter', function () {
        oTable.draw();
    });

    $('#list-table tbody').on('click', 'td.dt-control', function () {
        var tr = $(this).closest('tr');
        var row = oTable.row(tr);

        if (row.child.isShown()) {
            row.child.hide();
            tr.removeClass('shown');
        } else {
            oTable.rows().every(function () {
                var row = this;
                if (row.child.isShown()) {
                    row.child.hide();
                    $(this.node()).removeClass('shown');
                }
            });
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });
});

function format(rowData) {
    var div = $('<div/>')
            .addClass('loading')
            .text('Loading...');

    $.ajax({
        url: "{!! route('admin.program.onsite.collection.details') !!}",
        type: 'POST',
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
        data: {
            adminId: rowData.admin_id,
            opFDate: $("#programFDate").val(),
            opTDate: $("#programTDate").val(),
            pid: '{{$program->id}}'
        },
        dataType: 'html',
        success: function (response) {
            div.html(response)
                    .removeClass('loading');
        }
    });
    return div;
}

$("#export_btn").on("click", function () {
    var url = "{!! route('admin.program.onsite.collections.export') !!}";
    var startDate = $("#programFDate").val();
    var endDate = $("#programTDate").val();
    url += '?pid={{$program->id}}' + '&startDate=' + startDate + '&endDate=' + endDate;
    window.location.href = url;
});
</script>    
@stop