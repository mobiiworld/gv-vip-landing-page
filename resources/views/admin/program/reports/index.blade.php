@extends('adminlte::page')

@section('title', 'Day Report')

@section('content_header')
<h1>Programs Daily Report</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <div class="table-responsive no-padding">
                    <table class="table tb-color-code">
                        <thead>
                            <tr>
                                <th colspan="5" class="text-center">Slot Color Code</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="td-empty">Empty</td>   
                                <td class="td-booked">Booked</td>   
                                <td class="td-occupied">Occupied</td>   
                                <td class="td-about-expire">About to expire</td>   
                                <td class="td-expired">Expired</td>   
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="input-group">
                            <input type="text" name="programDate" id="programDate" required="" class="filter form-control datepicker" placeholder="Select Date" value="">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-info btn-flat" id="btnReload"><i class="fa fa-fast-backward fa-retweet"></i> Reload</button>
                                <?php $user = Auth::guard('admin')->user(); ?>
                                @if($user->hasRole('super-admin','admin') || $user->hasPermissionTo('Ramadan Majlis_booking','admin'))
                                <a class="btn btn-warning" style="margin-left: 5px;" href="{{route('admin.programs.slots.book', $program->id)}}">Book Slots</a>
                                @endif
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body" id="slotDiv">

            </div>
            <div class="overlay" style='display: none;'>
                <i class="fa fa-fw fa-spinner fa-spin"></i>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="bookingModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Booking Details</h3>
            </div>
            <div class="modal-body myModalBody"></div>
        </div>
    </div>
</div>
@stop
@section("css")
<style>
    .myModalBody{
        max-height: 75vh;
        overflow-y: auto;
    }
    .dropdown-menu{
        min-width: 50% !important;
    }
    .table-responsive{
        max-height: 500px !important;
    }
    #list-table thead tr th {
        background-color: white;
        position: sticky;
        z-index: 100;
        top: 0;
    }
    #list-table td:not(:empty) {
        cursor: pointer !important;
        /*padding: 0px;*/
        text-align: center !important;
        vertical-align: middle !important;
    }
    .onsite-container{
        position: relative !important;
        width: 100%;
    }
    .td-expired{
        background-color: red;
        color: black;
    }
    .td-booked{
        background-color: green;
        color: white;
    }
    .td-empty{
        background-color: gray;
        color: white;
    }
    .td-occupied{
        background-color: yellow;
        color: black;
    }
    .td-about-expire{
        background-image: linear-gradient(to right, red, yellow);
    }
    .onsite{
        position: absolute !important;
        top: 0px;
        left: 0px;
        font-size: 10px;
    }
    th.standard{
        color: #0073b7;
    }
    th.premium{
        color: #00a65a;
    }
    .tb-color-code{
        font-size: 12px;
    }
    .tb-color-code th{
        padding: 2px !important;
        text-align: center;
    }
    .tb-color-code td{
        padding: 2px !important;
        text-align: center;
    }
    .open ul.dropdown-menu {
        display: block;
    }
</style>
<link rel="stylesheet" href="{{asset('vendor/adminlte/plugins/iCheck/all.css')}}">
<link rel="stylesheet" href="{{URL::asset('css/daterangepicker.css')}}">
@stop
@section('js')
<script src="{{asset('vendor/adminlte/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{URL::asset('js/moment.min.js')}}"></script>
<script src="{{URL::asset('js/daterangepicker.min.js')}}"></script>
<script type='text/javascript'>
$(function () {
    $('.datepicker').daterangepicker({
        singleDatePicker: true,
        autoApply: true,
        locale: {
            format: 'YYYY-MM-DD'
        }
    });
    getSlots();
    $(document).on('change', '#programDate', function () {
        getSlots();
    });

    $('#btnReload').on('click', function () {
        getSlots();
    });

    setInterval(function checkSession() {
        getSlots();
    }, 300000); //Every 10 sec 10000.Every 5 minutes 300000.Every minute 60000

    $(document).on('click', '#slotDiv #list-table td.show-bookings,a.show-bookings', function (event) {
        event.preventDefault();
        var bId = $(this).attr('bid');
        if (bId != '') {
            $("#bookingModal").modal('show');
            Pace.restart();
            Pace.track(function () {
                $.ajax({
                    url: "{!! route('admin.program.day.report.details') !!}",
                    dataType: 'html',
                    cache: false,
                    method: 'POST',
                    data: {bid: bId},
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    success: function (response) {
                        $("#bookingModal").find('.modal-title').html('Booking Details');
                        $("#bookingModal").find('div.modal-body').html(response);
                        $("#bookingModal").modal('show');
                        $('.datepicker').daterangepicker({
                            singleDatePicker: true,
                            autoApply: true,
                            locale: {
                                format: 'YYYY-MM-DD'
                            }
                        });
                    },
                    error: function (response) {
                        swal(response);
                    }
                });
            });
        }
    });
    $("#bookingModal").on("hidden.bs.modal", function () {
        $("#bookingModal").find('div.modal-body').html('');
    })
});
function getSlots() {
    $.ajax({
        url: "{!! route('admin.program.day.report') !!}",
        dataType: 'html',
        cache: false,
        method: 'POST',
        data: {pid: '{{$program->id}}', pdate: $('#programDate').val()},
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
        beforeSend: function () {
            $("div.overlay").toggle();
        },
        success: function (response) {
            $("#slotDiv").html(response);
            $('#slotDiv #list-table tbody tr td').each(function () {
                var colSpan = 1;
                var tdClass1 = tdClass2 = $(this).attr('class').split(' ').pop();
                while (($(this).attr('bid') != '') && ($(this).attr('bid') == $(this).next().attr('bid'))) {
                    tdClass2 = $(this).next().attr('class').split(' ').pop();
                    $(this).next().remove();
                    colSpan++;
                }
                $(this).attr('colSpan', colSpan).removeClass(tdClass1).addClass(tdClass2);
                if ($(this).is('.td-expired')) {
                    $(this).html($(this).find('div.dropdown-toggle').html());
                    $(this).addClass('show-bookings');
                }
            });
        },
        complete: function () {
            $("div.overlay").toggle();
        }
    });
}
$(document).on('click', '#slotDiv #list-table tbody tr td li a.change-table', function (event) {
    event.preventDefault();
    //if ($(this).parent().parent().parent().parent('td').is('.td-occupied, .td-booked')) {
    var bId = $(this).parent().parent().parent().parent().attr('bid');
    var tId = $(this).attr('data-tid');
    if (bId != '') {
        $("#bookingModal").modal('show');
        Pace.restart();
        Pace.track(function () {
            $.ajax({
                url: "{!! route('admin.program.change.table') !!}",
                dataType: 'html',
                cache: false,
                method: 'POST',
                data: {bid: bId, tId: tId},
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                success: function (response) {
                    $("#bookingModal").find('.modal-title').html('Change Table');
                    $("#bookingModal").find('div.modal-body').html(response);
                    $("#bookingModal").modal('show');
                    $('input[type="radio"].flat-red').iCheck({
                        checkboxClass: 'icheckbox_flat-green',
                        radioClass: 'iradio_flat-green'
                    });
                },
                error: function (response) {
                    swal(response);
                }
            });
        });
    }
    //}
});
$(document).on('click', "#bookingModal #change-table-form", function (event) {
    event.preventDefault();
    if ($('#bookingModal input.checkboxTable:checked').length < 1) {
        swal('Please select your table');
        return false;
    }
    var data = $(this).closest('form#tableForm').serializeArray();
    Pace.restart();
    Pace.track(function () {
        $.ajax({
            url: '{!! url(route("admin.program.changed.table.store")) !!}',
            method: 'POST',
            data: data,
            success: function (responce)
            {
                if (responce.status) {
                    $('#bookingModal').modal('hide');
                    getSlots();
                }
                swal(responce.message);
            }
        });
    });
});

$(document).on('click', '#bookingModal #pChangeDetailButton', function () {
    var bId = $(this).attr('data-bid');
    var tDate = $(this).parent().parent().find('input[name=pDateToChange]').val();
    var tNotes = $(this).parent().parent().find('textarea[name=pNoteToChange]').val();
    Pace.restart();
    Pace.track(function () {
        $.ajax({
            url: '{!! url(route("admin.program.change.date")) !!}',
            method: 'POST',
            data: {'booking_id': bId, 'to_date': tDate, 's_notes': tNotes},
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            success: function (responce)
            {
                if (responce.status) {
                    $('#bookingModal').modal('hide');
                    getSlots();
                }
                swal(responce.message);
            }
        });
    });
});

$(document).on('click', '#slotDiv #list-table tbody tr td li a.release-slots', function (event) {
    event.preventDefault();
    //if ($(this).parent().parent().parent().parent('td').is('.td-occupied, .td-booked')) {
    var bId = $(this).parent().parent().parent().parent().attr('bid');
    var tId = $(this).attr('data-tid');
    if (bId != '') {
        $("#bookingModal").modal('show');
        Pace.restart();
        Pace.track(function () {
            $.ajax({
                url: "{!! route('admin.program.get.release.slots') !!}",
                dataType: 'html',
                cache: false,
                method: 'GET',
                data: {bid: bId, tId: tId},
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                success: function (response) {
                    $("#bookingModal").find('.modal-title').html('Release Table Slots');
                    $("#bookingModal").find('div.modal-body').html(response);
                    $("#bookingModal").modal('show');
                    $('input[type="checkbox"].flat-red').iCheck({
                        checkboxClass: 'icheckbox_flat-green',
                        radioClass: 'iradio_flat-green'
                    });
                },
                error: function (response) {
                    swal(response);
                }
            });
        });
    }
    //}
});

$(document).on('click', "#bookingModal #release-slot-form", function (event) {
    event.preventDefault();
    if ($('#bookingModal input.checkboxSlot:checked').length < 1) {
        swal('Please select your slots');
        return false;
    }
    var data = $(this).closest('form#slotForm').serializeArray();
    Pace.restart();
    Pace.track(function () {
        $.ajax({
            url: '{!! url(route("admin.programs.booked-slot.release")) !!}',
            method: 'POST',
            data: data,
            success: function (responce)
            {
                if (responce.status) {
                    $('#bookingModal').modal('hide');
                    getSlots();
                }
                swal(responce.message);
            }
        });
    });
});
$(document).on('show.bs.tab', function (e) {
    if (e.target.id === 'qrcode-tab') {
        var saleCode = $(e.target).attr('data-scode');
        var contentPane = $('#bookingModal').find('#qrcode');
        contentPane.html('<p class="text-center">Loading Content.Please wait.....</p>');
        $.ajax({
            url: "{!! route('admin.program.get.qrcodes') !!}",
            dataType: 'html',
            cache: false,
            method: 'POST',
            data: {saleCode: saleCode},
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            success: function (response) {
                contentPane.html(response);
            },
            error: function (response) {
                swal(response);
            }
        });
    }
});
</script>    
@stop