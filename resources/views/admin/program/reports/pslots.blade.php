<div class="table-responsive">
    <table class="table table-bordered" id="list-table" style="height:fit-content">
        <thead>
            <tr>
                <th style="width: 160px;"><div class="pull-left">Tables <i class="fa fa-arrow-down text-sm"></i></div><div class="pull-right">Slots <i class="fa fa-arrow-right text-sm"></i></div></th>
                @foreach($sHeads as $sHead)
                <th class="text-center text-blue">{{$sHead}}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($pSlots as $t=>$slots)
            <?php $slots = $slots->keyBy('name'); ?>
            <tr>
                <th class="{{$pSlots[$t][0]->type}}">
                    {{$t.(($pSlots[$t][0]->smoking_status==1)?'(S)':'')}}
                </th>
                @foreach($sHeads as $sHead) 
                @if(isset($slots[$sHead]->name))
                <?php
                $tdClass = 'td-empty';
                if (!empty($slots[$sHead]->booking_id)) {
                    $bDetails = $slots[$sHead]->bookings()->select('id', 'first_name', 'is_occupied')->first();
                } else {
                    $bDetails = null;
                }
                if ($slots[$sHead]->end_time <= date('Y-m-d H:i:00')) {
                    $tdClass = 'td-expired';
                } else {
                    if (!empty($bDetails)) {
                        //can check about to expire in 15 mnts aswell
                        if (((strtotime($slots[$sHead]->end_time) - strtotime(now())) / 60) <= 15) {
                            $tdClass = 'td-about-expire';
                        } else {
                            if ($bDetails->is_occupied) {
                                $tdClass = 'td-occupied';
                            } else {
                                $tdClass = 'td-booked';
                            }
                        }
                    }
                }
                ?>
                <td class="{{$tdClass}}" bid="{{$slots[$sHead]->booking_id}}">
                    @if(!empty($bDetails))
                    <div class="dropdown" style="text-align: center; vertical-align: middle; height: 100%; width: 100%;">
                        <div style="text-align: center; vertical-align: middle; height: 100%; width: 100%;" class="dropdown-toggle" data-toggle="dropdown" data-boundary="window" aria-haspopup="true" aria-expanded="false">
                            {!! $bDetails->first_name !!}
                        </div >
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a bid="{{$slots[$sHead]->booking_id}}" class="show-bookings">View Details</a></li>
                            <li><a data-tid="{{$slots[$sHead]->tableId}}" class="change-table">Change Table</a></li>
                            <li><a data-tid="{{$slots[$sHead]->tableId}}" class="release-slots">Release Slots</a></li>
                        </ul>
                    </div>
                    @endif
                </td>
                @else
                <td class="dummy" style="background-color: black;" bid=""></td>
                @endif
                @endforeach
            </tr>
            @endforeach
        </tbody>
    </table>
</div>