<div class="active tab-pane" id="activity">
<div class="row">
    <div class="col-md-4">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Program Date</h3>
            </div>
            <div class="box-body">
                @if(($booking->program_date >= date('Y-m-d')) && (date('Y-m-d H:i:00') < $booking->actual_program_dt))
                <div class="input-group date">
                    <input type="text" data-bid="{{$booking->id}}" class="form-control pull-left datepicker" id="pDateToChange" name="pDateToChange" value="{{$booking->program_date}}">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                </div>
                @else
                {{$booking->program_date}}
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Table Type</h3>
            </div>
            <div class="box-body text-capitalize">{{$booking->table_type}}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Amount Paid</h3>
            </div>
            <div class="box-body">AED {{$booking->amount}}</div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body box-profile">
                <div class="col-md-6">
                    <h3 class="profile-username">{{$booking->first_name.' '.$booking->last_name}}</h3>
                    <p class="text-muted p-0 m-0">{{$booking->email}}</p>
                    <p class="text-muted p-0 m-0">{{$booking->mobile}}</p>
                </div>
                @if($booking->admin_id)
                <div class="col-md-6 text-right">
                    <h3 class="profile-username">Booked Onsite</h3>
                    <p class="text-muted p-0 m-0">{{$booking->admin->name}}</p>
                    <p class="text-muted p-0 m-0">{{$booking->admin->email}}</p>
                </div>
                @endif
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Notes or Special requests</h3>
            </div>
            <div class="box-body">
                <p>{{$booking->special_notes}}</p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Booked Tables</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="list-table">
                        <thead>
                            <tr>
                                <th class="text-center text-blue">Table</th>
                                <th class="text-center text-blue">Slot</th>
                                <th class="text-center text-blue">Seats</th>
                                <th class="text-center text-blue">Smoking</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tables as $t=>$table)
                            <tr>
                                <td class="text-center">{{$t}}-{{$table[0]->type}}</td>
                                <td class="text-center">{{$table->sortBy('priority')->pluck('name')->join(', ')}}</td>
                                <td class="text-center">{{$table[0]->seat_type}}</td>
                                <td class="text-center">{{($table[0]->smoking_status==2)?'No':'Yes'}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div> 
        </div>
    </div>
</div>
</div>
@if(!$cTables->isEmpty())
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Cancelled Tables</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="list-table">
                        <thead>
                            <tr>
                                <th class="text-center text-blue">Slot</th>
                                <th class="text-center text-blue">Table</th>
                                <th class="text-center text-blue">Seats</th>
                                <th class="text-center text-blue">Smoking</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cTables as $table)
                            <tr>
                                <td class="text-center">{{$table->name}}</td>
                                <td class="text-center">{{$table->tName.'-'.$table->type}}</td>
                                <td class="text-center">{{$table->seat_type}}</td>
                                <td class="text-center">{{($table->smoking_status==2)?'No':'Yes'}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div> 
        </div>
    </div>
</div>
@endif
@if(!$rTables->isEmpty())
<div class="row">
    <div class="col-md-12">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Released Tables</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="list-table">
                        <thead>
                            <tr>
                                <th class="text-center text-blue">Slot</th>
                                <th class="text-center text-blue">Table</th>
                                <th class="text-center text-blue">Seats</th>
                                <th class="text-center text-blue">Smoking</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rTables as $table)
                            <tr>
                                <td class="text-center">{{$table->name}}</td>
                                <td class="text-center">{{$table->tName.'-'.$table->type}}</td>
                                <td class="text-center">{{$table->seat_type}}</td>
                                <td class="text-center">{{($table->smoking_status==2)?'No':'Yes'}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div> 
        </div>
    </div>
</div>
@endif