<div class="row">
    <div class="col-md-12">
        {{ Form::open(array('route' => 'admin.program.changed.table.store','id'=>'slotForm')) }}
        <div class="box box-primary">
            <div class="box-body">
                <p class="text-blue text-center">{{$extras['tabelDetails']}}</p>
                @if($existTableSlots->isEmpty())
                @php($emClass='hidden')
                <div class="callout callout-warning">
                    <h4>No slots available.</h4>
                </div>
                @else
                @php($emClass='')
                <div class="form-group text-center">
                    @foreach ($existTableSlots as $existTableSlot)
                    <label style="margin-right: 10px;">
                        {{ Form::checkbox('slotIds[]',  $existTableSlot->id,  null, ['class'=>'checkboxSlot flat-red'] ) }}
                        {{ $existTableSlot->name}}
                    </label>
                    @endforeach
                </div>
                @endif
            </div>
            <div class="box-footer {{$emClass}}">
                <input type="hidden" name="booking_id" value="{{$extras['bookingId']}}" />
                <div class="form-group pull-right">
                    {{ Form::submit('Submit', array('class' => 'btn btn-primary','id'=>"release-slot-form")) }}
                </div>
            </div>
        </div> 
        {{ Form::close() }}
    </div>
</div>