<table class="table table-striped">
    <thead>
        <tr>
            <th class="text-center text-blue">Sale Code</th>
            <th class="text-center text-blue">Amount</th>
            <th class="text-center text-blue">Payment Mode</th>
            <th class="text-center text-blue">Created At</th>
        </tr>
    </thead>
    <tbody>
        @foreach($bookings as $booking)
        <tr>
            <td class="text-center">{{$booking->sale_code}}</td>
            <td class="text-center">{{$booking->amount}}</td>
            <td class="text-center">{{$booking->pmode}}</td>
            <td class="text-center">{{date('h:i:s a M/d/Y',strtotime($booking->created_at))}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
