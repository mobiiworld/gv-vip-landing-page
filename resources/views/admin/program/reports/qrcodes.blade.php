@foreach($mediaCodes as $qrCode)
<div class="row">
    <div class="col-md-4">
        <div class="box box-default">
            <div class="box-header text-center">
                <h3 class="box-title">{{$qrCode->MainProductName}}</h3>
            </div>
            <div class="box-body text-center">{!! QrCode::size(150)->generate($qrCode->MediaCodes) !!}</div>
            <div class="box-footer text-center">{{$qrCode->MediaCodes}}</div>
        </div>
    </div>
</div>
@endforeach