@extends('adminlte::page')

@section('title', 'Add Program')

@section('content_header')
<h1> Add Program</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-tools">
                    <a href="{{route('admin.programs.index')}}" class="btn btn-sm btn-default">Back</a>
                </div>
            </div>

            {{ Form::open(array('url' => 'admin/programs','enctype' => 'multipart/form-data')) }}
            <div class="box-body">
                @include("admin.program.form")
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::close() }}


        </div>
    </div>
</div>
@stop
