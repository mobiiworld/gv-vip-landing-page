@extends('adminlte::page')

@section('title', 'Majlis Bookings')

@section('content_header')
<h1>{{$program->name}} - Bookings</h1>
@stop

@section('content') 
<div class="box">
    <div class="box-body">
        <div class="col-md-3">
            <div class="form-group" >
                <label>Name</label>
                <input type="text" name="name" id="name" class="form-control">

            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group" >
                <label>Email</label>
                <input type="text" name="email" id="email" class="form-control">                     
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group" >
                <label>Sale Code</label>
                <input type="text" name="saleCode" id="saleCode" class="form-control">                     
            </div>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <label>&nbsp;</label>
                <input type="button" name="find_btn" class="form-control btn btn-primary filter" value="Find" id="find_btn">
            </div>
        </div>
    </div>                    
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="list-table">
                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>                               
                                <th>Email</th>                               
                                <th>Mobile</th>                               
                                <th>Is VIP</th>
                                <th>Amount</th>
                                <th>Sale Code</th>
                                <th>Program Date</th>
                                <th>Status</th>
                                <th>Created At</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('js')
<script type='text/javascript'>
    $(function () {

        oTable = $('#list-table').DataTable({
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            ajax: {
                url: "{!! route('admin.majlis.bookings.datatable') !!}",
                type: 'post',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                data: function (d) {
                    d.programId = "{{$program->id}}";
                    d.name = $("#name").val();
                    d.email = $("#email").val();
                    d.saleCode = $("#saleCode").val();
                }
            },
            columns: [
                {data: 'first_name', name: 'first_name'},
                {data: 'last_name', name: 'last_name'},
                {data: 'email', name: 'email'},
                {data: 'mobile', name: 'mobile'},
                {data: 'is_vip', name: 'is_vip'},
                {data: 'amount', name: 'amount'},
                {data: 'sale_code', name: 'sale_code'},
                {data: 'program_date', name: 'program_date'},
                {data: 'status', name: 'status'},
                {data: 'created_at', name: 'created_at'}
            ]
        });
        $(".filter").on("click", function () {
            oTable.draw();
        });
    });
</script>    
@stop