<?php
$showForm = true;
?>
@if(empty($response['slots']))
<p class="text-center text-red">No slots available.</p>
<?php $showForm = false; ?>
@else
<div class="row">
    <div class="col-md-12">
        <h4>Select Time Slots</h4>
        <div class="form-group">
            @foreach($response['slots'] as $slot)
            <div class="checkbox-inline">
                <label class="checkbox-border-{{$slot['isAvailable']?'enabled':'disabled'}}">
                    <input type="checkbox" data-label="{{$slot['slotName']}}" data-a4="{{$response['program']['amount_4']}}" data-a6="{{$response['program']['amount_6']}}" data-a2="{{$response['program']['amount_2']}}" class="solt-check" name="slots[]" value="{{$slot['id']}}" @if(!$slot['isAvailable']) disabled @endif>
                    {{$slot['slotName']}}
                </label>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endif

@if($showForm)
<h4>User Details</h4>
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            {{ Form::label('paymentMode', 'Payment Mode') }}
            {!! Form::select('paymentMode', [1=>'Cash',2=>'Card'],null, ['class' => 'form-control', 'id' => 'paymentMode']) !!}
        </div>
    </div>
    <div id="cardDetails" style="display: none;">
        <div class="col-md-3">
            <div class="form-group">
                <label>Card Type<em class="text-red">*</em></label>
                {!! Form::select('cardType', [''=>'-Select-', 'MASTERCARD'=>'Master Card', 'VISA'=>'Visa'],null, ['class' => 'form-control', 'id' => 'cardType']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Card first and last 4 digit</label>
                <input name="cardNumber" id="cardNumber" type="text" class="form-control" data-inputmask="'mask': ['9999/9999','9999/9999']" data-mask="">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Approval Code<em class="text-red">*</em></label>
                {{ Form::text('approvalCode', null, array('id'=>'approvalCode','class' => 'form-control')) }}
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label>Amount(AED)<em class="text-red">*</em></label>
            {{ Form::text('amount', 0, array('id'=>'amount','class' => 'form-control','readonly'=>true)) }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="firstName">First Name<em class='text-red'>*</em></label>
            {{ Form::text('firstName', null, array('id'=>'firstName','class' => 'form-control', 'required'=>"required")) }}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="lastName">Last Name<em class='text-red'>*</em></label>
            {{ Form::text('lastName', null, array('id'=>'lastName','class' => 'form-control', 'required'=>"required")) }}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="lastName">Email<em class='text-red'>*</em></label>
            {{ Form::email('email', null, array('id'=>'email','class' => 'form-control', 'required'=>"required")) }}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {{ Form::label('mobile', 'Mobile') }}
            {{ Form::text('mobile', null, array('id'=>'mobile','class' => 'form-control')) }}
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            {{ Form::label('specialNotes', 'Notes or Special requests') }}
            {{ Form::textarea('specialNotes', null, array('id'=>'specialNotes','class' => 'form-control','rows' => 1)) }}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>&nbsp;</label><br />
            {{ Form::submit('Book Now', array('class' => 'btn btn-primary','id'=>'bookSlot')) }}
        </div>
    </div>
</div>
@endif