@extends('adminlte::page')

@section('title', 'Program Slots')

@section('content_header')
<h1> Program  Slots</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <h3>&nbsp;</h3>
                <div class="box-tools">
                    <a href="{{route('admin.programs.slots.create', $programId)}}" class="btn btn-sm btn-primary">Create Program Slots</a>
                    <a href="{{route('admin.programs.index')}}" class="btn btn-sm btn-default">Back</a>

                </div>
                <div class="box">
                    <div class="box-body">
                        <div class="col-md-4">
                            <div class="form-group" >
                                <label>Start Date(Program Date)</label>
                                <input type="text" name="program_date" id="program_date_start" class="form-control datepickerExport">

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" >
                                <label>End Date(Program Date)</label>
                                <input type="text" name="program_date" id="program_date_end" class="form-control datepickerExport">                                
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <input type="button" name="export" class="form-control btn btn-primary" value="Export" id="export_btn">
                            </div>
                        </div>


                    </div>                    
                </div>

            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group" >
                            <label>Date</label>
                            <input type="text" name="program_date" id="program_date" class="form-control datepicker">

                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Slots Type</label>
                            <select class="form-control filter" name="slots" id="slots">
                                <option value="" >All</option>
                                @foreach($slots as $slot)
                                <option value="{{$slot->id}}" >{{$slot->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Table</label>
                            <select class="form-control filter" name="tables" id="tables">
                                <option value="" >All</option>
                                @foreach($tables as $tab)
                                <option value="{{$tab->id}}" >{{$tab->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Status</label>
                            <select class="form-control filter" name="status" id="status">
                                <option value="" >All</option>
                                <option value="1" >Enabled</option>
                                <option value="2" >Disabled</option>                                
                            </select>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="list-table">

                        <thead>
                            <tr>

                                <th>Date</th>
                                <th>Slot Type</th>
                                <th>Table</th>
                                <th>Table No</th>
                                <th>Status</th>                               
                                <th>Booking Status</th>                               
                                <th>Table Status</th>                               
                                <th>Slot Status</th>                               
                               <!--  <th></th> -->
                            </tr>
                        </thead>


                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section("css")
<link href="{{ asset('css/bootstrap-datepicker.css')}}"  rel="stylesheet">
<link rel="stylesheet" href="{{asset('css/bootstrap4-toggle.min.css')}}">
<style>
    .cancel-book{
        padding: 5px;
        font-size: 13px;
    }
    .release-book{
        padding: 5px;
        font-size: 13px;
    }
</style>
@stop
@section("js")
<script src="{{URL::asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('js/bootstrap4-toggle.min.js')}}"></script>

<script type='text/javascript'>
$(function () {

    oTable = $('#list-table').DataTable({
        processing: true,
        serverSide: true,
        //autoWidth: false,
        ordering: false,
        ajax: {
            url: "{!! route('admin.programs.slots.datatable',$programId) !!}",
            type: 'post',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            data: function (d) {
                d.slot = $("#slots").val();
                d.table = $("#tables").val();
                d.program_date = $("#program_date").val();
                d.status = $("#status").val();
            },
        },
        columns: [

            {data: 'program_date', name: 'program_date'},
            {data: 'slot', name: 'slot', orderable: false, searchable: false},
            {data: 'tableName', name: 'tableName', orderable: false, searchable: false},
            {data: 'tableNumber', name: 'tableNumber', orderable: false, searchable: false},
            {data: 'status', name: 'status', orderable: false, searchable: false},
            {data: 'bookingStatus', name: 'bookingStatus', orderable: false, searchable: false},
            {data: 'tableStatus', name: 'tableStatus', orderable: false, searchable: false},
            {data: 'slotStatus', name: 'slotStatus', orderable: false, searchable: false},
                    // {data: 'actions', name: 'actions', orderable: false, searchable: false}
        ],
        fnDrawCallback: function () {
            $(".bsSwitch").bootstrapToggle({
                size: "sm",
                onstyle: "success",
                offstyle: "danger",
                on: 'Enabled',
                off: 'Disabled'
            });
        }
    });
    $(".filter").on("change", function () {
        oTable.draw();
    });
    $('.datepickerExport').datepicker({
        toggleActive: !0,
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        orientation: "auto bottom",
        autoclose: true,
    });
    $('.datepicker').datepicker({
        toggleActive: !0,
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        orientation: "auto bottom",
        autoclose: true,
    }).on("changeDate", function (e) {
        oTable.draw();
    });
    $(document).on('change', '.bsSwitch', function () {
        $.ajax({
            url: "{!! route('admin.programs.slots.changeStatus',$programId) !!}",
            dataType: 'json',
            cache: false,
            method: 'POST',
            data: {id: $(this).data('id')},
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            beforeSend: function () {

            },
            success: function (response) {
                oTable.draw();
            },
            error: function (response) {

            },
            complete: function () {

            }
        });
    });
    $("#export_btn").on("click", function () {
        var url = "{!! route('admin.programs.slots.export',$programId) !!}";
        var startDate = $("#program_date_start").val();
        var endDate = $("#program_date_end").val();
        url += '?startData=' + startDate + '&endDate=' + endDate;
        window.location.href = url;
    });
});
$(document).on('click', '.cancel-book', function () {
    var ths = $(this);
    var psid = ths.data('id');
    swal({
        title: "Cancel?",
        text: "Are you sure you want to cancel the booking?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {

            $.ajax({
                url: "{!! route('admin.programs.booked-slot.cancel') !!}",
                dataType: 'json',
                cache: false,
                method: 'POST',
                data: {id: psid},
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                beforeSend: function () {

                },
                success: function (response) {
                    if (response.status) {
                        ths.closest('td').html('');
                    } else {
                        swal(response.message);
                    }
                },
                error: function (response) {

                },
                complete: function () {

                }
            });
        } else {
            return true;
        }
    });
});
$(document).on('click', '.release-book', function () {
    var ths = $(this);
    var psid = ths.data('id');
    swal({
        title: "Release?",
        text: "Are you sure you want to release the table?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {

            $.ajax({
                url: "{!! route('admin.programs.booked-slot.release') !!}",
                dataType: 'json',
                cache: false,
                method: 'POST',
                data: {id: psid},
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                beforeSend: function () {

                },
                success: function (response) {
                    if (response.status) {
                        ths.closest('td').html('');
                    } else {
                        swal(response.message);
                    }
                },
                error: function (response) {

                },
                complete: function () {

                }
            });
        } else {
            return true;
        }
    });
});
</script>    
@stop