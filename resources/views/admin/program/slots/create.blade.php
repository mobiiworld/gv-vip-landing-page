@extends('adminlte::page')

@section('title', 'Add Program Slot')

@section('content_header')
<h1> Add Program Slot</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-tools">
                    <a href="{{route('admin.programs.slots.index',$programId)}}" class="btn btn-sm btn-default">Back</a>
                </div>
            </div>
           {{ Form::open(array('url' => route('admin.programs.slots.store',$programId),'id'=> "MyForm")) }}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Select Date</label>
                                <input type="text" name="date" class="form-control datepicker" placeholder="Select Date" value=""/>
                             </div>
                             </div>
                                 <div class="col-md-2"><div class="form-group text-center text-bold text-blue" style="margin-top: 26px; font-size: 20px;">-Or-</div></div>
                          <div class="col-md-5">
                              <div class="form-group">
                                <label>Date Range</label>
                                <input type="text" name="daterange" class="form-control daterange" placeholder="Select Date range" value=""/>
                         </div>
                         </div>
                         </div>
<div class="row">
                         <div class="col-md-12">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <label class="checkbox-inline"><input type="checkbox" class="slot_all" value="">Select All Slots</label>
                                </div>
                                <div class="box-body">
                                    <div class="form-check">
                                        @foreach($slots as $slot)
                                            <label class="checkbox-inline">
                                            <input type="checkbox" class="slot_sub require-one" value="{{$slot->id}}" name="slots[]"><b>{{$slot->name}}</b></label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        <div class="error" id="slot_error" style="color:red"></div>
                         </div>

                         <div class="col-md-12">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                        <label class="checkbox-inline"><input type="checkbox" value="" class="table_all">Select All Tables</label>
                                </div>
                                <div class="box-body">
                                    <div class="form-check">
                                        @foreach($tables as $k=>$tabs)
                                            <div class="box box-solid">
                                                <div class="box-header with-border">
                                                    <label class="text-sm">Table {{$k}} Seater</label>
                                                </div>
                                            <div class="box-body">
                                        @foreach($tabs as $tab)
                                            <label class="checkbox-inline"><input type="checkbox" class="table_sub" value="{{$tab->id}}" name="tables[]"><b>{{$tab->name}}</b></label>
                                        @endforeach
                                            </div>
                                        </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                            <div class="error" id="table_error" style="color:red"></div>
                         </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="row clearfix">
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}
                            </div>
                        </div>
                </div>
                {{ Form::close() }}


        </div>
    </div>
</div>
@stop
@section("css")
<link rel="stylesheet" href="{{URL::asset('css/daterangepicker.css')}}">
<link rel="stylesheet" href="{{URL::asset('css/datepicker3.css')}}">
<link rel="stylesheet" href="{{URL::asset('css/daterangepicker-bs3.css')}}">
@stop
@section("js")
<script src="{{URL::asset('js/moment.min.js')}}"></script>
<script src="{{URL::asset('js/daterangepicker.min.js')}}"></script>
<script src="{{URL::asset('js/bootstrap-datepicker.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"> </script>
<script type="text/javascript">

   	var checkboxes = $('.require-one');
	var checkbox_names = $.map(checkboxes, function(e, i) {
		return $(e).attr("name")
	}).join(" ");

    $(document).ready(function() {
        $("#MyForm").validate({
        groups: {
			checks: checkbox_names
		},
        rules: {
            {{--  'date': {
                required: true,
            },  --}}
            'slots[]': {
                required: true,
            },
            'tables[]':{
                required: true,
            },
        },
        messages: {
       //'date': "Please enter a Date.",
        'slots[]': "Please select at least one slot.",
        'tables[]': "Please selectat least one table.",
        },
        errorPlacement: function(error, element) {
            console.log(element);
            if(element[0].classList[0] == 'slot_sub'){
                $('#slot_error').append(error);
            }
            if(element[0].classList[0] == 'table_sub'){

			$('#table_error').append(error);
            }
		}
        });
        });
//var todayDate = new Date().toISOString().slice(0,10); // YYYY-MM-DD

function getFormatDate(today){
    //console.log(d);
   // return  d.getDate() + '/' + parseInt(d.getMonth())+1 + '/' + d.getFullYear();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    return yyyy + '/' + mm + '/' + dd;
    //return dd + '/' + mm + '/' + yyyy;
}
var todayDate = getFormatDate(new Date());

  $(document).ready(function() {




    $(".slot_all").on("click",function(){
        if($(this).is(":checked")){
            $(".slot_sub").attr("checked",true);
        }else{
            $(".slot_sub").attr("checked",false);
        }
    })
    $(".table_all").on("click",function(){
        if($(this).is(":checked")){
            $(".table_sub").attr("checked",true);
        }else{
            $(".table_sub").attr("checked",false);
        }
    })
 $('.daterange').daterangepicker("setDate", null);

 $('.datepicker').click(function(){
     $('.daterange').val("");
 });
  $('.daterange').click(function(){
   //  $('.datepicker').val("");
 });

 if($('.datepicker').val() != null){
     $('.daterange').val("");
 }

    $('.datepicker').daterangepicker({
      singleDatePicker : true,
      //autoUpdateInput: false,
       autoApply: true,
      minDate: new Date(),
      locale: {
             format: 'DD-MM-YYYY'
            }
    });

     $('.daterange').daterangepicker({
      minDate: new Date(),
      format: 'YYYY-MM-DD',
      autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      }
    });

    $('.daterange').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        $('.datepicker').val("");
    });

    $('.daterange').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
         $('.datepicker').daterangepicker({
            singleDatePicker : true,
            //autoUpdateInput: false,
            autoApply: true,
            minDate: todayDate,
            format: 'YYYY-MM-DD'
            });
       
    });

    // $('.daterange').daterangepicker({
    //   minDate: todayDate,
    //   autoUpdateInput: false,
    //   format: 'DD/MM/YYYY'
    // });

    // $('.daterange').on('apply.daterangepicker', function(ev, picker) {
    //     $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    // });

    // $('.daterange').on('cancel.daterangepicker', function(ev, picker) {
    //     $(this).val('');
    // });
  })
</script>
@stop
