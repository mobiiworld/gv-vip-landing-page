<div class="row">
<div class="col-md-4">
<div class="form-group {{ $errors->has('program_uid') ? 'has-error' : '' }}">
    {{ Form::label('program_uid', 'Id') }}
    {{ Form::text('program_uid', null, array('class' => 'form-control', 'required'=>"required")) }}
    {!! $errors->first('program_uid','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>
</div>
<div class="col-md-4">
<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    {{ Form::label('name', 'Name') }}
    {{ Form::text('name', null, array('class' => 'form-control', 'required'=>"required")) }}
    {!! $errors->first('name','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>
</div>
<div class="col-md-12">
<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
    {{ Form::label('description', 'Description') }}
    {{ Form::textarea('description', null, array('class' => 'form-control')) }}
    {!! $errors->first('description','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>
</div>
<div class="col-md-3">
<div class="form-group {{ $errors->has('hours_before_booking') ? 'has-error' : '' }}">
    {{ Form::label('hours_before_booking', 'Hours before booking') }}
    {{ Form::number('hours_before_booking', null, array('class' => 'form-control')) }}
    {!! $errors->first('hours_before_booking','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>
</div>
<div class="col-md-3">
<div class="form-group {{ $errors->has('home_display_hours') ? 'has-error' : '' }}">
    {{ Form::label('home_display_hours', 'Hours before displaying home page') }}
    {{ Form::number('home_display_hours', null, array('class' => 'form-control')) }}
    {!! $errors->first('home_display_hours','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>
</div>
<div class="col-md-3">
<div class="form-group {{ $errors->has('suggesetd_table_count') ? 'has-error' : '' }}">
    {{ Form::label('suggesetd_table_count', 'Suggesetd Table Count') }}
    {{ Form::number('suggesetd_table_count', null, array('class' => 'form-control','min'=>1, 'required'=>"required")) }}
    {!! $errors->first('suggesetd_table_count','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>
</div>
<div class="col-md-3 hidden">
<div class="form-group {{ $errors->has('full_evening_table_limit') ? 'has-error' : '' }}">
    {{ Form::label('full_evening_table_limit', 'Full Evening Table Count') }}
    {{ Form::number('full_evening_table_limit', null, array('class' => 'form-control','min'=>1)) }}
    {!! $errors->first('full_evening_table_limit','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>
</div>
<div class="col-md-3">
<div class="form-group {{ $errors->has('max_weight_count') ? 'has-error' : '' }}">
    {{ Form::label('max_weight_count', 'Max Weight Count') }}
    {{ Form::number('max_weight_count', null, array('class' => 'form-control','min'=>1, 'required'=>"required")) }}
    {!! $errors->first('max_weight_count','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>
</div>
<div class="col-md-4">
<div class="form-group {{ $errors->has('has_priority') ? 'has-error' : '' }}">
    {{ Form::label('has_priority', 'Has priority') }}
    {{ Form::checkbox('has_priority','1',((isset($program->has_priority) && $program->has_priority == 0) ? FALSE : TRUE)) }}
    {!! $errors->first('has_priority','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>
</div>
</div>

<div class="col-md-12" style="margin-left: 0px; padding-left: 0px;margin-right: 0px; padding-right: 0px;">
    <div class="box box-primary box-solid">
        <div class="box-header">
            <h3 class="box-title">VGS Product Code - 2 Seater</h3>
        </div>
        <div class="box-body">
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.standard.2_seater.nonsmoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.standard.2_seater.nonsmoking', 'Standard Non Smoking') }}
                    {{ Form::text('vgs_pcodes[standard][2_seater][nonsmoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.standard.2_seater.nonsmoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.standard.2_seater.smoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.standard.2_seater.smoking', 'Standard Smoking') }}
                    {{ Form::text('vgs_pcodes[standard][2_seater][smoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.standard.2_seater.smoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.standard.2_seater.vip_nonsmoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.standard.2_seater.vip_nonsmoking', 'Standard VIP Non Smoking') }}
                    {{ Form::text('vgs_pcodes[standard][2_seater][vip_nonsmoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.standard.2_seater.vip_nonsmoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>    
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.standard.2_seater.vip_smoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.standard.2_seater.vip_smoking', 'Standard VIP Smoking') }}
                    {{ Form::text('vgs_pcodes[standard][2_seater][vip_smoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.standard.2_seater.vip_smoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>    
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.premium.2_seater.nonsmoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.premium.2_seater.nonsmoking', 'Premium Non Smoking') }}
                    {{ Form::text('vgs_pcodes[premium][2_seater][nonsmoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.premium.2_seater.nonsmoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.premium.2_seater.smoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.premium.2_seater.smoking', 'Premium Smoking') }}
                    {{ Form::text('vgs_pcodes[premium][2_seater][smoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.premium.2_seater.smoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.premium.2_seater.vip_nonsmoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.premium.2_seater.vip_nonsmoking', 'Premium VIP Non Smoking') }}
                    {{ Form::text('vgs_pcodes[premium][2_seater][vip_nonsmoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.premium.2_seater.vip_nonsmoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>    
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.premium.2_seater.vip_smoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.premium.2_seater.vip_smoking', 'Premium VIP Smoking') }}
                    {{ Form::text('vgs_pcodes[premium][2_seater][vip_smoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.premium.2_seater.vip_smoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>    

            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.standard.2_seater.amount') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.standard.2_seater.amount', 'Standard Amount') }}
                    {{ Form::text('vgs_pcodes[standard][2_seater][amount]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.standard.2_seater.amount','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.premium.2_seater.amount') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.premium.2_seater.amount', 'Premium Amount') }}
                    {{ Form::text('vgs_pcodes[premium][2_seater][amount]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.premium.2_seater.amount','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-left: 0px; padding-left: 0px;margin-right: 0px; padding-right: 0px;">
    <div class="box box-primary box-solid">
        <div class="box-header">
            <h3 class="box-title">VGS Product Code - 4 Seater</h3>
        </div>
        <div class="box-body">
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.standard.4_seater.nonsmoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.standard.4_seater.nonsmoking', 'Standard Non Smoking') }}
                    {{ Form::text('vgs_pcodes[standard][4_seater][nonsmoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.standard.4_seater.nonsmoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.standard.4_seater.smoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.standard.4_seater.smoking', 'Standard Smoking') }}
                    {{ Form::text('vgs_pcodes[standard][4_seater][smoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.standard.4_seater.smoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.standard.4_seater.vip_nonsmoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.standard.4_seater.vip_nonsmoking', 'Standard VIP Non Smoking') }}
                    {{ Form::text('vgs_pcodes[standard][4_seater][vip_nonsmoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.standard.4_seater.vip_nonsmoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>    
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.standard.4_seater.vip_smoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.standard.4_seater.vip_smoking', 'Standard VIP Smoking') }}
                    {{ Form::text('vgs_pcodes[standard][4_seater][vip_smoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.standard.4_seater.vip_smoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>    
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.premium.4_seater.nonsmoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.premium.4_seater.nonsmoking', 'Premium Non Smoking') }}
                    {{ Form::text('vgs_pcodes[premium][4_seater][nonsmoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.premium.4_seater.nonsmoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.premium.4_seater.smoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.premium.4_seater.smoking', 'Premium Smoking') }}
                    {{ Form::text('vgs_pcodes[premium][4_seater][smoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.premium.4_seater.smoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.premium.4_seater.vip_nonsmoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.premium.4_seater.vip_nonsmoking', 'Premium VIP Non Smoking') }}
                    {{ Form::text('vgs_pcodes[premium][4_seater][vip_nonsmoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.premium.4_seater.vip_nonsmoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>    
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.premium.4_seater.vip_smoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.premium.4_seater.vip_smoking', 'Premium VIP Smoking') }}
                    {{ Form::text('vgs_pcodes[premium][4_seater][vip_smoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.premium.4_seater.vip_smoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>    

            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.standard.4_seater.amount') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.standard.4_seater.amount', 'Standard Amount') }}
                    {{ Form::text('vgs_pcodes[standard][4_seater][amount]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.standard.4_seater.amount','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.premium.4_seater.amount') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.premium.4_seater.amount', 'Premium Amount') }}
                    {{ Form::text('vgs_pcodes[premium][4_seater][amount]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.premium.4_seater.amount','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-left: 0px; padding-left: 0px;margin-right: 0px; padding-right: 0px;">
    <div class="box box-primary box-solid">
        <div class="box-header">
            <h3 class="box-title">VGS Product Code - 6 Seater</h3>
        </div>
        <div class="box-body">
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.standard.6_seater.nonsmoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.standard.6_seater.nonsmoking', 'Standard Non Smoking') }}
                    {{ Form::text('vgs_pcodes[standard][6_seater][nonsmoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.standard.6_seater.nonsmoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.standard.6_seater.smoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.standard.6_seater.smoking', 'Standard Smoking') }}
                    {{ Form::text('vgs_pcodes[standard][6_seater][smoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.standard.6_seater.smoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.standard.6_seater.vip_nonsmoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.standard.6_seater.vip_nonsmoking', 'Standard VIP Non Smoking') }}
                    {{ Form::text('vgs_pcodes[standard][6_seater][vip_nonsmoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.standard.6_seater.vip_nonsmoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>    
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.standard.6_seater.vip_smoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.standard.6_seater.vip_smoking', 'Standard VIP Smoking') }}
                    {{ Form::text('vgs_pcodes[standard][6_seater][vip_smoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.standard.6_seater.vip_smoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>    
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.premium.6_seater.nonsmoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.premium.6_seater.nonsmoking', 'Premium Non Smoking') }}
                    {{ Form::text('vgs_pcodes[premium][6_seater][nonsmoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.premium.6_seater.nonsmoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.premium.6_seater.smoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.premium.6_seater.smoking', 'Premium Smoking') }}
                    {{ Form::text('vgs_pcodes[premium][6_seater][smoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.premium.6_seater.smoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.premium.6_seater.vip_nonsmoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.premium.6_seater.vip_nonsmoking', 'Premium VIP Non Smoking') }}
                    {{ Form::text('vgs_pcodes[premium][6_seater][vip_nonsmoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.premium.6_seater.vip_nonsmoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>    
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.premium.6_seater.vip_smoking') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.premium.6_seater.vip_smoking', 'Premium VIP Smoking') }}
                    {{ Form::text('vgs_pcodes[premium][6_seater][vip_smoking]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.premium.6_seater.vip_smoking','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>    

            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.standard.6_seater.amount') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.standard.6_seater.amount', 'Standard Amount') }}
                    {{ Form::text('vgs_pcodes[standard][6_seater][amount]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.standard.6_seater.amount','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('vgs_pcodes.premium.6_seater.amount') ? 'has-error' : '' }}">
                    {{ Form::label('vgs_pcodes.premium.6_seater.amount', 'Premium Amount') }}
                    {{ Form::text('vgs_pcodes[premium][6_seater][amount]', null, array('class' => 'form-control ')) }}
                    {!! $errors->first('vgs_pcodes.premium.6_seater.amount','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
            </div>
        </div>
    </div>
</div>