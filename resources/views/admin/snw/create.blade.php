@extends('adminlte::page')

@section('title', 'Shake and Win Campaigns')

@section('content_header')
<h1>Add Shake and Win Campaign</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12 ">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-tools">
                    <a href="{{route('admin.snw-campaigns.index')}}" class="btn btn-sm btn-default">Back</a>
                </div>
            </div>
            {{ Form::open(array('url' => 'admin/snw-campaigns')) }}
            <div class="box-body">
                @include("admin.snw.form")
            </div>
            <div class="box-footer">
                {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop