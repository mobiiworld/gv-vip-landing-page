@extends('adminlte::page')

@section('title', 'Shake and Win Campaign Prizes')

@section('content_header')
<h4>
    <a href="{{route('admin.snw-campaigns.index')}}">{{$data->campaign->name}}</a> <i class="fa fa-angle-right"></i>
    <a href="{{route('admin.snw-prizes.index')}}?campaign={{$data->campaign->id}}">Prizes</a> <i class="fa fa-angle-right"></i> Edit
</h4>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            {{ Form::model($data,array('url' => route('admin.snw-prizes.update',$data->id), 'method' => 'PUT')) }}
            <div class="box-body">
                @include("admin.snw.prizes.form")
            </div>
            <div class="box-footer">
                {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop