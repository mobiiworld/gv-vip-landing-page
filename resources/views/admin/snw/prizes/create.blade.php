@extends('adminlte::page')

@section('title', 'Shake and Win Campaign Prizes')

@section('content_header')
<h4>
    <a href="{{route('admin.snw-campaigns.index')}}">{{$campaign->name}}</a> <i class="fa fa-angle-right"></i>
    <a href="{{route('admin.snw-prizes.index')}}?campaign={{$campaign->id}}">Prizes</a> <i class="fa fa-angle-right"></i> Add New
</h4>
@stop

@section('content')
<div class="row">
    <div class="col-md-12 ">
        <div class="box box-primary">
            {{ Form::open(array('url' => "admin/snw-prizes?campaign=".$campaign->id)) }}
            <div class="box-body">
                @include("admin.snw.prizes.form")
            </div>
            <div class="box-footer">
                {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop