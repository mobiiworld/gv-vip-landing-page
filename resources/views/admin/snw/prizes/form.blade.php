<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('names.en') ? 'has-error' : '' }}">
            {{ Form::label('name', 'Name (en)') }}
            {{ Form::text('names[en]', null, array('class' => 'form-control', 'dir'=>'ltr')) }}
            {!! $errors->first('names.en','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
        <div class="form-group {{ $errors->has('descriptions.en') ? 'has-error' : '' }}">
            {{ Form::label('descriptions', 'Description (en)') }}
            {{ Form::textarea('descriptions[en]', null, array('class' => 'form-control', 'dir'=>'ltr')) }}
            {!! $errors->first('descriptions.en','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('names.ar') ? 'has-error' : '' }}">
            {{ Form::label('names', 'Name (ar)') }}
            {{ Form::text('names[ar]', null, array('class' => 'form-control', 'dir'=>'rtl')) }}
            {!! $errors->first('names.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
        <div class="form-group {{ $errors->has('descriptions.ar') ? 'has-error' : '' }}">
            {{ Form::label('descriptions', 'Description (ar)') }}
            {{ Form::textarea('descriptions[ar]', null, array('class' => 'form-control', 'dir'=>'rtl')) }}
            {!! $errors->first('descriptions.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('vgs_id') ? 'has-error' : '' }}">
            {{ Form::label('eloqua_form', 'Eloqua Form') }}
            {{ Form::text('eloqua_form', null, array('class' => 'form-control')) }}
            {!! $errors->first('eloqua_form','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="form-group ">
                <label for="import_file">Coupons(Excel File)</label>
                <input class="form-control" accept="application/vnd.ms-excel" autocomplete="off" name="import_file" type="file" id="import_file">
            </div>
        </div>
    </div>
</div>