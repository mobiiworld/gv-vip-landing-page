@extends('adminlte::page')

@section('title', 'Shake and Win Campaigns')

@section('content_header')
<h1>Shake and Win Campaigns</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <h3>&nbsp;</h3>
                <div class="box-tools">
                    @if(auth('admin')->user()->hasRole('super-admin','admin') || auth('admin')->user()->hasPermissionTo('shake and win_create','admin'))
                    <a href="{{route('admin.snw-campaigns.create')}}" class="btn btn-sm btn-primary">Add New</a>
                    @endif
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive" style="overflow-x: hidden;">
                    <table class="table table-striped table-bordered table-hover" id="list-table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Status</th>                               
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section("css")

<link rel="stylesheet" href="{{asset('css/bootstrap4-toggle.min.css')}}">
@stop
@section('js')
<script src="{{asset('js/bootstrap4-toggle.min.js')}}"></script>
<script type='text/javascript'>
$(function () {
    oTable = $('#list-table').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        ordering: false,
        responsive: true,
        lengthChange: true,
        ajax: {
            url: "{!! route('admin.snw-campaigns.datatable') !!}",
            type: 'post',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'}
        },
        columns: [
            {data: 'names.en', name: 'names->en'},
            {data: 'start_date', name: 'start_date', searchable: false},
            {data: 'end_date', name: 'end_date', searchable: false},
            {data: 'status', name: 'status', searchable: false},
            {data: 'actions', name: 'actions', searchable: false}
        ],
        fnDrawCallback: function () {
            $(".bsSwitch").bootstrapToggle({
                size: "sm",
                onstyle: "success",
                offstyle: "danger",
                on: 'Enabled',
                off: 'Disabled'
            });
        }
    });

    $(document).on('change', '.bsSwitch', function () {
        var id = $(this).data('id');
        var st = $(this).data('st');
        $.ajax({
            url: "{!! route('admin.snw.status') !!}",
            dataType: 'json',
            cache: false,
            method: 'POST',
            data: {id: id, module: 'campaigns'},
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            beforeSend: function () {

            },
            success: function (response) {
                oTable.draw();
            },
            error: function (response) {

            },
            complete: function () {

            }
        });
    });

});
</script>    
@stop