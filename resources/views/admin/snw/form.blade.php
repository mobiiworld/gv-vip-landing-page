<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('names.en') ? 'has-error' : '' }}">
            {{ Form::label('name', 'Name (en)') }}
            {{ Form::text('names[en]', null, array('class' => 'form-control', 'dir'=>'ltr')) }}
            {!! $errors->first('names.en','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('names.ar') ? 'has-error' : '' }}">
            {{ Form::label('names', 'Name (ar)') }}
            {{ Form::text('names[ar]', null, array('class' => 'form-control', 'dir'=>'rtl')) }}
            {!! $errors->first('names.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('start_date') ? 'has-error' : '' }}" >
            {{ Form::label('start_date', 'Start Date') }}
            {{ Form::text("start_date",  null, array('placeholder'=>'Start Date','id'=>'start_date',"autocomplete" => "off", 'class' => 'start_end_date form-control')) }}
            {!! $errors->first('start_date','<p class="text-danger"><strong>:message</strong></p>') !!}

        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('end_date') ? 'has-error' : '' }}" >
            {{ Form::label('end_date', 'End Date') }}
            {{ Form::text("end_date", null, array('placeholder'=>'End Date','id'=>'end_date',"autocomplete" => "off", 'class' => 'start_end_date form-control')) }}
            {!! $errors->first('end_date','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
</div>
@section('css')
<link href="{{ asset('css/bootstrap-datepicker.css')}}" id="theme" rel="stylesheet">
@stop
@section('js')
<script src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
<script type='text/javascript'>
$(function () {
    $(".start_end_date").datepicker({
        toggleActive: !0,
        format: 'yyyy-mm-dd',
        orientation:'bottom',
        todayHighlight:true
    });
});
</script>
@stop