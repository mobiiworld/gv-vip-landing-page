@extends('adminlte::page')

@section('title', 'Shake and Win Campaigns')

@section('content_header')
<h1>Edit Shake and Win Campaign</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-tools">
                    <a href="{{route('admin.snw-campaigns.index')}}" class="btn btn-sm btn-default">Back</a>
                </div>
            </div>
            {{ Form::model($data,array('url' => route('admin.snw-campaigns.update',$data->id), 'method' => 'PUT')) }}
            <div class="box-body">
                @include("admin.snw.form")
            </div>
            <div class="box-footer">
                {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop