@extends('adminlte::page')

@section('title', 'Dynamic texts')

@section('content_header')
<h1> Dynamic texts</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <h3>&nbsp;</h3>
                <div class="box-tools">
                    {{-- <a href="{{route('admin.dynamic-texts.create')}}" class="btn btn-sm btn-primary">Create new</a> --}}
                </div>

            </div>
            <div class="box-body">
            
                
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="list-table">

                        <thead>
                            <tr>
                                <th>Slug</th>
                                <th>Text (English)</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($texts))
                                @foreach($texts as $text)
                                    <tr>
                                        <td>{{$text->slug}}</td>
                                        <td>{!! $text->text_en !!}</td>
                                        
                                        <td>
                                            <a href="{{route('admin.dynamic-texts.edit', $text->id)}}" class="btn btn-sm btn-primary">Edit</a>
                                            
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>

                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section("css")

<link rel="stylesheet" href="{{asset('css/bootstrap4-toggle.min.css')}}">
@stop
@section('js')
<script src="{{asset('js/bootstrap4-toggle.min.js')}}"></script>
<script type='text/javascript'>
    $(function () {
        
        oTable = $('#list-table').DataTable({
            ordering: false,
            serverSide: false,
            
        });
        
    });

</script>    
@stop