

@if(!isset($texts))
<div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
    {{ Form::label('slug', 'Slug') }}
    {{ Form::text('slug', null, array('class' => 'form-control', 'required'=>"required" )) }}
     {!! $errors->first('slug','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>
@else
<div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
    {{ Form::label('slug', 'Slug') }}
    {{ Form::text('slug', null, array('class' => 'form-control', "readonly")) }}
     {!! $errors->first('slug','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>
@endif


<div class="form-group {{ $errors->has('text_en') ? 'has-error' : '' }}">
    {{ Form::label('text_en', 'Text (English)') }}
    {{ Form::textarea('text_en', null, array('class' => 'form-control', 'required'=>"required")) }}
     {!! $errors->first('text_en','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>

<div class="form-group {{ $errors->has('text_ar') ? 'has-error' : '' }}">
    {{ Form::label('text_ar', 'Text (Arabic)') }}
    {{ Form::textarea('text_ar', null, array('class' => 'form-control', 'required'=>"required")) }}
     {!! $errors->first('text_ar','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>
                     