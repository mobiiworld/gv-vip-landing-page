@extends('adminlte::page')

@section('title', 'Add new Tables')

@section('content_header')
<h1><i class='fa fa-user-plus'></i> Add Table</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-primary">
            <div class="box-header with-border">
                <a href="{{ url('/admin/tables') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
            <div class="box-body">

                {{ Form::open(array('url' => 'admin/tables')) }}

                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    {{ Form::label('name', 'Name') }}
                    {{ Form::text('name', '', array('class' => 'form-control')) }}
                    {!! $errors->first('name','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>

                <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                    {{ Form::label('type', 'Type') }}
                   <select class="form-control" name="type" id="type">
                        @if(!empty($types))
                            @foreach($types as $key=>$em)
                                <option value="{{$key}}" >{{$em}} </option>
                            @endforeach
                        @endif
                    </select>
                    {!! $errors->first('type','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>

                <div class="form-group {{ $errors->has('smoking_status') ? 'has-error' : '' }}">
                    {{ Form::label('smoking_status', 'Smoking status') }}
                   <select class="form-control" name="smoking_status" id="smoking_status">
                       
                        <option value="2" >No</option>
                        <option value="1" >Yes</option>
                           
                    </select>
                    {!! $errors->first('smoking_status','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
                <?php $seat_types = config('globalconstants.seat_types'); ?>
                <div class="form-group {{ $errors->has('seat_type') ? 'has-error' : '' }}">
                    {{ Form::label('seat_type', 'Seat type') }}
                   <select class="form-control" name="seat_type" id="seat_type">
                       
                        @if(!empty($seat_types))
                            @foreach($seat_types as $key=>$em)
                                <option value="{{$key}}" >{{$em}} </option>
                            @endforeach
                        @endif
                           
                    </select>
                    {!! $errors->first('seat_type','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
              <div class="form-group {{ $errors->has('table_number') ? 'has-error' : '' }}">
                    {{ Form::label('table_number', 'Table No') }}
                    {{ Form::number('table_number', '', array('class' => 'form-control allow_numeric')) }}
                    {!! $errors->first('table_number','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
                <div class="form-group {{ $errors->has('admin_only') ? 'has-error' : '' }}">
                    {{ Form::checkbox('admin_only',  null,  null, ['class'=>'minimal-red'] ) }}
                    {{ Form::label('admin_only', 'Allow Booking for Admin only') }}
                </div>

                {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
@stop
@section('js')
<script>
 $(document).ready(function() {
        $('.allow_numeric').keypress(function (event) {
            return isNumber(event, this)
        });
    });

     // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // Check minus and only once.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // Check dot and only once.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>
@endsection