@extends('adminlte::page')

@section('title', 'Tables')

@section('content_header')
<h1><i class="fa fa-users"></i> Tables</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <a href="{{ route('admin.tables.create') }}" class="btn btn-success btn-sm">Add Table</a>
            </div>
            <div class="box-body">
                
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            {{ Form::label('seat_type', 'Seat Type') }}
                            {!! Form::select('seat_type', [0=>'-Any-',2=>'2 Seater',4=>'4 Seater', 6=>'6 Seater'],null, ['class' => 'form-control', 'id' => 'seat_type']) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {{ Form::label('type', 'Table Type') }}
                            {!! Form::select('type', [''=>'-Any-']+$types,null, ['class' => 'form-control', 'id' => 'type']) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {{ Form::label('smoking_status', 'Smoke Type') }}
                            {!! Form::select('smoking_status', [0=>'-Any-',1=>'Smoking', 2=>'Non Smoking'],null, ['class' => 'form-control', 'id' => 'smoking_status']) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="start_date">&nbsp</label>
                            <button class="btn btn-sm btn-primary form-control filter">Filter</button>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="users-table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Seat Type</th>
                                <th>Table No</th>
                                <th>Type</th>
                                <th>Smoking Type</th>                               
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section("css")

<link rel="stylesheet" href="{{asset('css/bootstrap4-toggle.min.css')}}">
@stop
@section('js')
<script src="{{asset('js/bootstrap4-toggle.min.js')}}"></script>
<script src="{{asset('vendor/adminlte/plugins/iCheck/icheck.min.js')}}"></script>
<script type='text/javascript'>
$(function () {
    oTable = $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        //autoWidth: false,
         ordering: false,
         pageLength: 50,
        ajax: {
            url: "{!! route('admin.tables.datatable') !!}",
            type: 'post',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            data: function (d) {
                d.seat_type = $('#seat_type').val();
                d.type = $('#type').val();
                d.smoking_status = $('#smoking_status').val();
            }
        },
        columns: [
            {data: 'name', name: 'name'},
            {data: 'seat_type', name: 'seat_type'},
            {data: 'table_number', name: 'table_number'},
            {data: 'type', name: 'type'},
            {data: 'smoking_status', name: 'smoking_status', searchable: false},            
            {data: 'status', name: 'status', orderable: false, searchable: false},
            {data: 'actions', name: 'actions', orderable: false, searchable: false}
        ],
        fnDrawCallback: function () {
            $(".bsSwitch").bootstrapToggle({
                size: "sm",
                onstyle: "success",
                offstyle: "danger",
                on: 'Enabled',
                off: 'Disabled'
            });
        }
    });
    $(document).on("click", "button.filter", function () {
        oTable.ajax.reload();
    });
     $(document).on('change', '.bsSwitch', function () {
            $.ajax({
                url: '{!! route('admin.tables.changeStatus') !!}',
                dataType: 'json',
               
                cache: false,
                method: 'POST',
                data : { id : $(this).data('id')},
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                beforeSend: function () {
                   
                },
                success: function (response) {
                    oTable.draw();
                },
                error: function (response) {
                    
                },
                complete: function () {
                    
                }
            });
       });
});
</script>    
@stop