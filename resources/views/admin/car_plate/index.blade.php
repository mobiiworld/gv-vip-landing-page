@extends('adminlte::page')

@section('title', 'Car Plate Information')

@section('content_header')
<h1><i class="fa fa-car"></i> Car Plate Information</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header "><div id="table_buttons" class="box-tools"></div></div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Payment Status</label>
                            <select class="form-control" name="payment_status" id="payment_status">
                                <option value="" >All</option>
                                <option value="1">Created</option>
                                <option value="2">Initiated</option>
                                <option value="3">Authenticate</option>
                                <option value="4">Paid</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Platform</label>
                            <select class="form-control" name="plate_form" id="plate_form">
                                <option value="" >All</option>
                                <option value="web">Web</option>
                                <option value="android">Android</option>
                                <option value="ios">Ios</option>
                                <option value="applepay">Applepay</option>
                            </select>
                        </div>
                    </div>
                    <div class="input-daterange" id="global_dates">
                        <div class="col-md-2">
                            <div class="form-group" >
                                {{ Form::label('longt', 'Start Date') }}
                                {{ Form::text("start_date", date('m/d/Y',strtotime("-1 days")), array('placeholder'=>'Start Date','id'=>'start_date',"autocomplete" => "off", 'class' => 'start_end_date form-control')) }}
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" >
                                {{ Form::label('longt', 'End Date') }}
                                {{ Form::text("end_date", date('m/d/Y'), array('placeholder'=>'End Date','id'=>'end_date',"autocomplete" => "off", 'class' => 'start_end_date form-control')) }}
                            </div>
                        </div>
                    </div>
                      <?php /* ?>
                     <div class="col-md-2">
                        <div class="form-group">
                            <label>Designa Status</label>
                            <select class="form-control" name="sync_status" id="sync_status">
                                <option value="" >All</option>
                                <option value="1">Synced</option>
                                <option value="2">Not synced</option>
                            </select>
                        </div>
                    </div>
                    <?php */ ?>
                </div>
                <div class="table-responsive">

                    <table class="table table-striped table-bordered table-hover" id="car-plate-table">

                        <thead>
                            <tr>
                                <th>Sale Id</th>
                                <th>Pack Id</th>
                               <th>Pack Number</th> 
                                <th>Email</th>
                                <th>Platform</th>
                                <th>Old Plate Info</th>
                                <th>New Plate Info</th>
                                <th>Amount</th>
                                <?php /* ?> <th>Designa Status</th> <?php */ ?>
                                <th>Payment Status</th>
                                <th>Designa update Status</th>
                                <th>Date/Time Added</th>
                            </tr>
                        </thead>


                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section('js')
<script type="text/javascript" src="{{asset('vendor/datatables-plugins/buttons/js/dataTables.buttons.min.js')}}"></script>
<script type="text/javascript" src="{{asset('vendor/datatables-plugins/buttons/js/buttons.bootstrap4.min.js')}}"></script>

<script src="{{asset('vendor/datatables-plugins/jszip/jszip.min.js')}}"></script>
<script src="{{asset('vendor/datatables-plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('vendor/datatables-plugins/pdfmake/vfs_fonts.js')}}"></script>

<script type="text/javascript" src="{{asset('vendor/datatables-plugins/buttons/js/buttons.html5.min.js')}}"></script>
<script type="text/javascript" src="{{asset('vendor/datatables-plugins/buttons/js/buttons.print.min.js')}}"></script>
<script type="text/javascript" src="{{asset('vendor/datatables-plugins/buttons/js/buttons.colVis.min.js')}}"></script>
<link href="{{ asset('css/bootstrap-datepicker.css')}}" id="theme" rel="stylesheet">
    <script src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
<script type='text/javascript'>
 var dt;
$(function () {
    $(function () {
            $("#global_dates").datepicker({
                toggleActive: !0
            });
        });
    dt = $('#car-plate-table').DataTable({
        processing: true,
        serverSide: true,
        //autoWidth: false,
        ajax: {
            url: '{!! route('admin.car-plate-info.datatable') !!}',
            type: 'post',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            data: function ( d ) {
                if( $('#payment_status').val() != "") {
                    d.payment_status = $('#payment_status').val();
                }
                if( $('#plate_form').val() != "") {
                    d.plate_form = $('#plate_form').val();
                }
                if( $('#sync_status').val() != "") {
                    d.sync_status = $('#sync_status').val();
                }
                if( $('#start_date').val() != "") {
                    d.start_date = $('#start_date').val();
                }
                if( $('#end_date').val() != "") {
                    d.end_date = $('#end_date').val();
                }
            }
        },
        columns: [
            {data: 'saleid', name: 'gv_car_info.saleid'},
            {data: 'packid', name: 'gv_car_info.packid'},
             {data: 'packnumber', name: 'gv_car_info.packnumber'}, 
            {data: 'email', name: 'gv_car_info.email'},
            {data: 'platform', name: 'gv_user_payment_sessions.platform'},
            {data: 'old_plate_number', name: 'old_plate_number'},
            {data: 'new_plate_number', name: 'new_plate_number'},
            {data: 'amount', name: 'gv_user_payment_sessions.amount'},
            <?php /* ?>    {data: 'designa_status', name: 'gv_car_info.designa_status', orderable: false, searchable: false},
-        <?php */ ?>

            {data: 'status', name: 'gv_user_payment_sessions.status', orderable: false, searchable: false},
            {data: 'designa_sync_status', name: 'gv_car_info.designa_sync_status', orderable: false, searchable: false},
            {data: 'created_date', name: 'gv_user_payment_sessions.created_date', orderable: false, searchable: false}
        ],
        dom: 'Bfrtip',               
                buttons: [ 
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ['thead th:not(.noExport)']
                        },
                        "action": newexportaction,
                        orientation: 'landscape',
                        title : 'Car plate info',
                        className : 'btn-primary',                        
                        pageSize : 'LEGAL',
                    },
                    
                    {
                        extend: 'excelHtml5',                   
                        action: newexportaction,    
                        exportOptions: {
                            columns: "thead th:not(.noExport)"
                        },                
                        title  : 'Car plate info',                    
                        //text  : 'Export',                    
                        createEmptyCells : true,
                        className : 'btn-primary'
                        
                    }
                ],

                initComplete: function(settings, json) {
                    
                    dt.buttons().container().appendTo('#table_buttons')
                }
    });
    function newexportaction(e, dt, button, config) {
        var self = this;
        var oldStart = dt.settings()[0]._iDisplayStart;
        dt.one('preXhr', function (e, s, data) {
            // Just this once, load all data from the server...
            data.start = 0;
            data.length = '{{$total}}';
            dt.one('preDraw', function (e, settings) {
                // Call the original action function
                if (button[0].className.indexOf('buttons-copy') >= 0) {
                    $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                    $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                    $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                    $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-print') >= 0) {
                    $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
                }
                dt.one('preXhr', function (e, s, data) {
                    // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                    // Set the property to what it was before exporting.
                    settings._iDisplayStart = oldStart;
                    data.start = oldStart;
                });
                // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                setTimeout(dt.ajax.reload, 0);
                // Prevent rendering of the full data to the DOM
                return false;
            });
        });

        // Requery the server with the new one-time export settings
        dt.ajax.reload();
    }
    $(document).on("change", "#payment_status, #plate_form, #sync_status, #start_date, #end_date", function() {
        dt.ajax.reload();
    });

    $(document).on("click", ".btnSyncDesigna", function () {
        var id = $(this).data('id');
        var thisBtn = $(this);
        $.ajax({
            url: "{!! route('admin.car-info.sync.designa') !!}",
            dataType: 'json',
            data: {'id': id},
            cache: false,
            method: 'POST',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            beforeSend: function () {
                thisBtn.attr('disabled', true);
            },
            success: function (response) {
                if (response.status) {
                    /*var ereLog = JSON.stringify(response.responseLog);
                    if (ereLog.indexOf('request') !== -1) {
                        swal("Sync failed", ereLog, "warning");
                    } else {*/
                        swal("Success", "Synced", "success");
                        dt.draw(false);
                  //  }
                } else {
                    swal("Sync failed", response.message, "error");
                }
            },
            error: function (response) {
                swal("Sync failed", 'Something went wrong!', "error");
            },
            complete: function () {
                thisBtn.attr('disabled', false);
            }
        });
    });

});
</script>
@stop
