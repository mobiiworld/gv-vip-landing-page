@extends('adminlte::page')

@section('title', 'Offers')

@section('content_header')
<h1> Offers</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <h3>&nbsp;</h3>
                <div class="box-tools">
                    <a href="{{route('admin.import','offers')}}" class="btn btn-sm btn-success">Import Offers</a>
                    
                </div>

            </div>
            <div class="box-body">
                 
                
           
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="pack-table">

                        <thead>
                            <tr>
                              <!--   <th>Order</th> -->
                                <th>Parent Merchant Name</th>
                                <th>Merchant Name</th>
                               <!--  <th>Offer Name</th> -->
                               <!--  <th>Valid From</th>
                                <th>Valid To</th> -->
                                
                                <th>Active Status</th>
                                <th>Open Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>


                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section('js')

<script type='text/javascript'>
    $(function () {
       
        oTable = $('#pack-table').DataTable({
            processing: true,
            serverSide: true,
            //autoWidth: false,
            ordering: false,
            ajax: {
                url: '{!! route('admin.offers.datatable') !!}',
                type: 'post',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                 data: function ( d ) {
                   
                },
            },
           
            columns: [
                //{data: 'ordering', name: 'ordering'},
                {data: 'parent_merchant_name_en', name: 'parent_merchant_name_en'},
                {data: 'merchant_name_en', name: 'merchant_name_en'},
               // {data: 'offer_name_en', name: 'offer_name_en'},
                // {data: 'valid_from', name: 'valid_from'},
                // {data: 'valid_to', name: 'valid_to'},
                {data: 'active_status', name: 'active_status', orderable: false, searchable: false},
                {data: 'open_status', name: 'open_status', orderable: false, searchable: false},
                {data: 'actions', name: 'actions', orderable: false, searchable: false}
            ]
        });
        $(".filter").on("change",function(){
            oTable.draw();
        });

        $(document).on("click", ".statusChange", function (event) {
            event.preventDefault();
            var url = $(this).attr('href');
            var parent = $(this).data('parent');
            var merchant = $(this).data('merchant');
            $.ajax({
                url: url,
                dataType: 'json',
                data: {'parent': parent, 'merchant' : merchant},
                cache: false,
                method: 'POST',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                beforeSend: function () {
                },
                success: function (response) {
                    swal("Success", response.message, "success");
                    oTable.draw(false);
                },
                error: function (response) {
                },
                complete: function () {
                    oTable.draw(false);
                }
            });
        });

    });
</script>    
@stop