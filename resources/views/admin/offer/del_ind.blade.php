@extends('adminlte::page')

@section('title', 'Offers')

@section('content_header')
<h1> Offers</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <h3>&nbsp;</h3>
                <div class="box-tools">
                    <a href="{{route('admin.import','offers')}}" class="btn btn-sm btn-success">Import Offers</a>
                    
                </div>

            </div>
            <div class="box-body">
                 
                
           
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="pack-table">

                        <thead>
                            <tr>
                              <!--   <th>Order</th> -->
                                <th>Parent Merchant Name</th>
                                <th>Merchant Name</th>
                               <!--  <th>Offer Name</th> -->
                               <!--  <th>Valid From</th>
                                <th>Valid To</th> -->
                                
                                <th>Active Status</th>
                                <th>Open Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>


                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section('js')

<script type='text/javascript'>
    $(function () {
       
        oTable = $('#pack-table').DataTable({
            processing: true,
            serverSide: true,
            //autoWidth: false,
            ordering: false,
            ajax: {
                url: '{!! route('admin.offersdelete.datatable') !!}',
                type: 'post',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                 data: function ( d ) {
                   
                },
            },
           
            columns: [
                //{data: 'ordering', name: 'ordering'},
                {data: 'parent_merchant_name_en', name: 'parent_merchant_name_en'},
                {data: 'merchant_name_en', name: 'merchant_name_en'},
               // {data: 'offer_name_en', name: 'offer_name_en'},
                // {data: 'valid_from', name: 'valid_from'},
                // {data: 'valid_to', name: 'valid_to'},
                {data: 'active_status', name: 'active_status', orderable: false, searchable: false},
                {data: 'open_status', name: 'open_status', orderable: false, searchable: false},
                {data: 'actions', name: 'actions', orderable: false, searchable: false}
            ]
        });
        $(".filter").on("change",function(){
            oTable.draw();
        });

        $('#pack-table').on('click', '.destroy', function (e) {
            e.preventDefault();

            var href = $(this).attr('href');

            swal({
                title: "{{ trans('myadmin.confirm-delete') }}",
                text: "Once deleted, you will not be able to recover this file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: href,
                                method: 'DELETE',
                                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                                beforeSend: function () {
                                    Pace.restart();//Pace.start();
                                },
                                success: function () {
                                    oTable.ajax.reload();
                                    swal("Done!", "User successfully deleted.", "success");
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    swal("Error deleting!", "Please try again", "error");
                                },
                                complete: function () {
                                    Pace.stop();
                                }
                            });
                        } else {
                            return true;
                        }
                    });
        });

    });
</script>    
@stop