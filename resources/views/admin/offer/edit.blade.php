@extends('adminlte::page')

@section('title', 'Edit Offer Message - For Closed')

@section('content_header')
<h1>Edit Offer Message - For Closed</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-primary">
            <div class="box-header with-border">
                <a href="{{ route('admin.offers.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
            <div class="box-body">
                {{ Form::open( array('url' => route('admin.offers.update',[ $note->parent_merchant_name_en, strtolower(str_replace(' ', '',$note->merchant_name_en))]), 'method' => 'POST')) }}

                <div class="form-group {{ $errors->has('closed_message_en') ? 'has-error' : '' }}">
                    {{ Form::label('closed_message_en', 'Message - En') }}
                    {{ Form::text('closed_message_en', $note->closed_message_en, array('class' => 'form-control')) }}
                    {!! $errors->first('closed_message_en','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
                <div class="form-group {{ $errors->has('closed_message_ar') ? 'has-error' : '' }}">
                    {{ Form::label('closed_message_ar', 'Message - Ar') }}
                    {{ Form::text('closed_message_ar', $note->closed_message_ar, array('class' => 'form-control')) }}
                    {!! $errors->first('closed_message_ar','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
                <div class="form-group {{ $errors->has('ordering') ? 'has-error' : '' }}">
                    {{ Form::label('ordering', 'Ordering') }}
                    {{ Form::number('ordering', $note->ordering, array('class' => 'form-control')) }}
                    {!! $errors->first('ordering','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
                {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
@stop