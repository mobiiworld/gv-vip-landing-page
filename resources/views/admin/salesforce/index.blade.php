@extends('adminlte::page')

@section('title', 'Salesforce Log')

@section('content_header')
<h1><i class="fa fa-users"></i> Salesforce Log</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="users-table">

                        <thead>
                            <tr>
                                <th>Type</th>
                                <th>Reference Id</th>
                                <th>Created At</th>
                                <th class="text-wrap width-200">Request</th>
                                <th class="text-wrap width-200">Response</th>
                               
                            </tr>
                        </thead>


                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section("css")
<style type="text/css">
.text-wrap{
    white-space:normal;
}
.width-200{
    width:200px;
}
</style>
@stop
@section('js')
<script src="{{asset('vendor/adminlte/plugins/iCheck/icheck.min.js')}}"></script>
<script type='text/javascript'>
$(function () {
    oTable = $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        ordering: false,
        
        ajax: {
            url: '{!! route('admin.saleforce.datatable') !!}',
            type: 'post',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'}
        },
        columns: [
            {data: 'type', name: 'type'},
            {data: 'refenrence_id', name: 'refenrence_id'},
            {data: 'created_at', name: 'created_at'},
            {data: 'sale_request', name: 'sale_request'},
            {data: 'response', name: 'response'},
           
        ]
    });

   
});
</script>    
@stop