@extends('adminlte::page')

@section('title', 'Edit ')

@section('content_header')
<h1>Edit </h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-primary">
            <div class="box-header with-border">
                <a href="{{ route('admin.email-templates.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
            <div class="box-body">
                {{ Form::model($templates, array('route' => array('admin.email-templates.update', $templates->id), 'method' => 'PUT')) }}

                @include("admin.email_templates.form")

                {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
@stop