

@if(!isset($templates))
<div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
    {{ Form::label('type', 'Type') }}
    {{ Form::text('type', null, array('class' => 'form-control', 'required'=>"required" )) }}
     {!! $errors->first('type','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>
@else
<div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
    {{ Form::label('type', 'Type') }}
    {{ Form::text('type', null, array('class' => 'form-control', "readonly")) }}
     {!! $errors->first('type','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>
@endif


<div class="form-group {{ $errors->has('subject_en') ? 'has-error' : '' }}">
    {{ Form::label('subject_en', 'Subject (English)') }}
    {{ Form::text('subject_en', null, array('class' => 'form-control', 'required'=>"required")) }}
     {!! $errors->first('subject_en','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>

<div class="form-group {{ $errors->has('subject_ar') ? 'has-error' : '' }}">
    {{ Form::label('subject_ar', 'Subject (Arabic)') }}
    {{ Form::text('subject_ar', null, array('class' => 'form-control', 'required'=>"required")) }}
     {!! $errors->first('subject_ar','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>
  

<div class="form-group {{ $errors->has('body_en') ? 'has-error' : '' }}">
    {{ Form::label('body_en', 'Body (English)') }}
    {{ Form::textarea('body_en', null, array('class' => 'form-control', 'required'=>"required")) }}
     {!! $errors->first('body_en','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>

<div class="form-group {{ $errors->has('body_ar') ? 'has-error' : '' }}">
    {{ Form::label('body_ar', 'Body (Arabic)') }}
    {{ Form::textarea('body_ar', null, array('class' => 'form-control', 'required'=>"required")) }}
     {!! $errors->first('body_ar','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>