@extends('adminlte::page')

@section('title', 'Slots')

@section('content_header')
<h1><i class="fa fa-users"></i> Slots Type</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <?php $user = Auth::guard('admin')->user(); ?>

               
                <a href="{{ route('admin.slots.create') }}" class="btn btn-success btn-sm">Add Slot Type</a>
               
            </div>
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="users-table">

                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Start time</th>
                                <th>End time</th>
                                <th>Priority</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>


                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section("css")

<link rel="stylesheet" href="{{asset('css/bootstrap4-toggle.min.css')}}">
@stop
@section('js')
<script src="{{asset('js/bootstrap4-toggle.min.js')}}"></script>
<script src="{{asset('vendor/adminlte/plugins/iCheck/icheck.min.js')}}"></script>
<script type='text/javascript'>
$(function () {
    oTable = $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        //autoWidth: false,
         ordering: false,
        ajax: {
            url: '{!! route('admin.slots.datatable') !!}',
            type: 'post',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'}
        },
        columns: [
            {data: 'name', name: 'name'},
            {data: 'start_time', name: 'start_time'},
            {data: 'end_time', name: 'end_time'},
            {data: 'priority', name: 'priority'},
            {data: 'status', name: 'status', orderable: false, searchable: false},
            {data: 'actions', name: 'actions', orderable: false, searchable: false}
        ],
        fnDrawCallback: function () {
            $(".bsSwitch").bootstrapToggle({
                size: "sm",
                onstyle: "success",
                offstyle: "danger",
                on: 'Enabled',
                off: 'Disabled'
            });
        }
    });
    $(".filter").on("change",function(){
        oTable.draw();
    });
     $(document).on('change', '.bsSwitch', function () {
            $.ajax({
                url: '{!! route('admin.slots.changeStatus') !!}',
                dataType: 'json',

                cache: false,
                method: 'POST',
                data : { id : $(this).data('id')},
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                beforeSend: function () {

                },
                success: function (response) {
                    oTable.draw();
                },
                error: function (response) {

                },
                complete: function () {

                }
            });
       });
});
</script>
@stop
