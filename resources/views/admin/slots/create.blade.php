@extends('adminlte::page')

@section('title', 'Add new Slot')

@section('content_header')
<h1><i class='fa fa-user-plus'></i> Add Slot Type</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <a href="{{ url('/admin/slots') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
            <div class="box-body">
                @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
                @endif
                {{ Form::open(array('url' => 'admin/slots')) }}

                <div class="row">
                <div class="col-md-4">
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    {{ Form::label('name', 'Name') }}
                    {{ Form::text('name', '', array('class' => 'form-control')) }}
                    {!! $errors->first('name','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
                </div>

                <div class="col-md-4">
                <div class="form-group {{ $errors->has('priority') ? 'has-error' : '' }}">
                    {{ Form::label('priority', 'Priority') }}
                    {{ Form::text('priority', '', array('class' => 'form-control allow_numeric')) }}
                    {!! $errors->first('priority','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
                </div>
                
                <div class="col-md-4">    
                <div class="form-group {{ $errors->has('weight_count') ? 'has-error' : '' }}" style="display: none;">
                    {{ Form::label('weight_count', 'Weight Count') }}
                    {{ Form::number('weight_count', 1, array('class' => 'form-control allow_numeric')) }}
                    {!! $errors->first('weight_count','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
                </div>
                </div>

                <div class="row">
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('starttime') ? 'has-error' : '' }}">
                        <label>Start time</label>
                        <input type="text" name="starttime" class="form-control timepicker" required="true" value="{{old('starttime')}}">
                        {!! $errors->first('starttime','<p class="text-danger"><strong>:message</strong></p>') !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('actual_start_time') ? 'has-error' : '' }}">
                        {{ Form::checkbox('nextDay',  null,  null, ['class'=>'nextDay minimal-red'] ) }}
                        {{ Form::label('nextDay', 'Next Day') }}
                        <input type="text" name="actual_start_time" class="form-control timepicker2" @if(empty(old('actual_start_time'))) disabled="disabled" @endif placeholder="Actula Start Time" value="{{old('actual_start_time',null)}}">
                        {!! $errors->first('actual_start_time','<p class="text-danger"><strong>:message</strong></p>') !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('actual_end_time') ? 'has-error' : '' }}">
                        <label>&nbsp;</label>
                        <input type="text" name="actual_end_time" class="form-control timepicker2" @if(empty(old('actual_start_time'))) disabled="disabled" @endif placeholder="Actula End Time" value="{{old('actual_end_time',null)}}">
                        {!! $errors->first('actual_end_time','<p class="text-danger"><strong>:message</strong></p>') !!}
                    </div>
                </div>
                </div>

                <div class="row">
                <div class="col-md-4">
                <div class="form-group {{ $errors->has('endtime') ? 'has-error' : '' }}">
                    <label>End time</label>
                    <input type="text" name="endtime" class="form-control timepicker" required="true" value="{{old('endtime')}}">
                    {!! $errors->first('endtime','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
                </div>
                </div>



                {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
@stop
@section("css")
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/css/bootstrap.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
<link rel="stylesheet" href="{{URL::asset('css/bootstrap-timepicker.min.css')}}">
@stop
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script src="{{URL::asset('js/bootstrap-timepicker.min.js')}}"></script>

<script>
$(document).ready(function () {
$('.timepicker').timepicker({
timeFormat: 'h:i a',
        showSecond: true,
        showMeridian: true,
        minuteStep: 1,
        ampm: true

});
$('.timepicker2').timepicker({
    defaultTime: null,
        timeFormat: 'h:i a',
        showSecond: true,
        showMeridian: true,
        minuteStep: 1,
        ampm: true

});
function timeToInt(time) {
var now = new Date();
var dt = (now.getMonth() + 1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + time;
var d = new Date(dt);
console.log(d);
return d;
}

function checkDates() {
if (($('#open_time').val() == '') || ($('#close_time').val() == ''))
        return true;
var start = timeToInt($('#open_time').val());
var end = timeToInt($('#close_time').val());
console.log($('#open_time').val());
console.log(start, end);
if ((start == - 1) || (end == - 1)) {
return false;
}

if (start >= end) {
return false;
}
return true;
}
});
$(document).ready(function () {

$('.timepicker').on('change', function () {
$('#myform').bootstrapValidator('revalidateField', 'open_time');
$('#myform').bootstrapValidator('revalidateField', 'close_time');
});
$('#myform').bootstrapValidator({

message: 'This value is not valid',
        feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
        },
        fields: {

        open_time: {
        validators: {
        notEmpty: {
        message: 'Please select open time'
        },
                callback: {
                message: "Start time should be lower than end time",
                        callback: function (value, validator, $field) {
                        return checkDates();
                        }
                }
        }
        },
                close_time: {
                validators: {
                notEmpty: {
                message: 'Please select close time'
                },
                        callback: {
                        message: "Close time should be greater than end time",
                                callback: function (value, validator, $field) {
                                return checkDates();
                                }
                        }
                }
                },
        }

});
});</script>


<script>
    $(document).ready(function () {
    $('.allow_numeric').keypress(function (event) {
    return isNumber(event, this)
    });
    {{--  $('.timepicker').timepicker({
    showMeridian : true,
            minuteStep : 1
    }
    ); --}}
    });
    // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {

    var charCode = (evt.which) ? evt.which : event.keyCode

            if (
                    (charCode != 45 || $(element).val().indexOf('-') != - 1) && // Check minus and only once.
                    (charCode != 46 || $(element).val().indexOf('.') != - 1) && // Check dot and only once.
                    (charCode < 48 || charCode > 57))
            return false;
    return true;
    }
    
    $('input.nextDay').on('click', function(){
    is_checked = $(this).is(':checked');
  if(is_checked){ $('input.timepicker2').attr('disabled',false); }
           else {  $('input.timepicker2').val('').attr('disabled',true);}
});
</script>
@endsection