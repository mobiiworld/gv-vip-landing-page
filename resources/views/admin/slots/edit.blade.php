@extends('adminlte::page')

@section('title', 'Edit Slot')

@section('content_header')
<h1><i class='fa fa-user-plus'></i> Edit {{$user->name}}</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <a href="{{ url('/admin/slots') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
            <div class="box-body">
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
                {{ Form::model($user, array('route' => array('admin.slots.update', $user->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with user data --}}

                <div class="row">
                <div class="col-md-4">
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    {{ Form::label('name', 'Name') }}
                    {{ Form::text('name', null, array('class' => 'form-control')) }}
                    {!! $errors->first('name','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
                </div>
               
                <div class="col-md-4">
                <div class="form-group {{ $errors->has('priority') ? 'has-error' : '' }}">
                    {{ Form::label('priority', 'Priority') }}
                    {{ Form::text('priority', null, array('class' => 'form-control allow_numeric')) }}
                    {!! $errors->first('priority','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
                </div>

                <div class="col-md-4">
                <div class="form-group {{ $errors->has('weight_count') ? 'has-error' : '' }}" style="display: none;">
                    {{ Form::label('weight_count', 'Weight Count') }}
                    {{ Form::number('weight_count', 1, array('class' => 'form-control allow_numeric')) }}
                    {!! $errors->first('weight_count','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
                </div>
                </div>

                <div class="row">
                <div class="col-md-4">
                  <div class="form-group {{ $errors->has('start_time') ? 'has-error' : '' }}">
                    {{ Form::label('start_time', 'Start time') }}
                    {{ Form::text('start_time', null, array('class' => 'form-control timepicker','required'=>"true")) }}                   
                    {!! $errors->first('start_time','<p class="text-danger"><strong>:message</strong></p>') !!}
                 </div>
                 </div>
                
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('actual_start_time') ? 'has-error' : '' }}">
                        {{ Form::checkbox('nextDay',  null,  $user->actual_start_time?1:null, ['class'=>'nextDay minimal-red'] ) }}
                        {{ Form::label('nextDay', 'Next Day') }}
                        <input type="text" name="actual_start_time" class="form-control timepicker2" @if(empty(old('nextDay',$user->actual_start_time))) disabled="disabled" @endif placeholder="Actula Start Time" value="{{old('actual_start_time',$user->actual_start_time)}}">
                        {!! $errors->first('actual_start_time','<p class="text-danger"><strong>:message</strong></p>') !!}
                    </div>
                </div>
                    
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('actual_end_time') ? 'has-error' : '' }}">
                        <label>&nbsp;</label>
                        <input type="text" name="actual_end_time" class="form-control timepicker2" @if(empty(old('nextDay',$user->actual_start_time))) disabled="disabled" @endif placeholder="Actula End Time" value="{{old('actual_end_time',$user->actual_end_time)}}">
                        {!! $errors->first('actual_end_time','<p class="text-danger"><strong>:message</strong></p>') !!}
                    </div>
                </div>    
                </div>
                  
                <div class="row">
                <div class="col-md-4">
                  <div class="form-group {{ $errors->has('end_time') ? 'has-error' : '' }}">
                    {{ Form::label('end_time', 'End time') }}
                    {{ Form::text('end_time', null, array('class' => 'form-control timepicker','required'=>"true")) }}                   
                    {!! $errors->first('endtime','<p class="text-danger"><strong>:message</strong></p>') !!}
                 </div>
                 </div>
                 </div>
               
                {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
@stop
@section("css")
<link rel="stylesheet" href="{{URL::asset('css/bootstrap-timepicker.min.css')}}">
@stop
@section('js')
<script src="{{URL::asset('js/bootstrap-timepicker.min.js')}}"></script>
<script>
 $(document).ready(function() {
        $('.timepicker').timepicker({
            showMeridian : true,
            minuteStep : 1
        });
        $('.timepicker2').timepicker({
    defaultTime: null,
        timeFormat: 'h:i a',
        showSecond: true,
        showMeridian: true,
        minuteStep: 1,
        ampm: true

});
        $('.allow_numeric').keypress(function (event) {
            return isNumber(event, this)
        });
    });

     // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // Check minus and only once.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // Check dot and only once.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
    $('input.nextDay').on('click', function(){
    is_checked = $(this).is(':checked');
  if(is_checked){ $('input.timepicker2').attr('disabled',false); }
           else {  $('input.timepicker2').val('').attr('disabled',true);}
});
</script>
@endsection