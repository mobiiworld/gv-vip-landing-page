@extends('adminlte::page')

@section('title', 'Admin Users')

@section('content_header')
    <h1><i class="fa fa-shopping-cart"></i> Shopping Cart Transaction - Finance/Admin</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Transaction Status</label>
                                <select class="form-control" name="transaction_status" id="transaction_status">
                                    <option value="" >All</option>
                                    <option value="active">Active</option>
                                    <option value="inactive">Inactive</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Payment Status</label>
                                <select class="form-control" name="payment_status" id="payment_status">
                                    <option value="" >All</option>
                                    <option value="1">Created</option>
                                    <option value="2">Initiated</option>
                                    <option value="3">Authenticate</option>
                                    <option value="4">Paid</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Platform</label>
                                <select class="form-control" name="plate_form" id="plate_form">
                                    <option value="" >All</option>
                                    <option value="web">Web</option>
                                    <option value="android">Android</option>
                                    <option value="ios">Ios</option>
                                    <option value="applepay">Apple pay</option>
                                    <option value="googlepay">Google pay</option>
                                </select>

                            </div>
                        </div>
                        <div class="input-daterange" id="global_dates">
                            <div class="col-md-2">
                                <div class="form-group" >
                                    {{ Form::label('longt', 'Start Date') }}
                                    {{ Form::text("start_date", date('m/d/Y',strtotime("-1 days")), array('placeholder'=>'Start Date','id'=>'start_date',"autocomplete" => "off", 'class' => 'start_end_date form-control')) }}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group" >
                                    {{ Form::label('longt', 'End Date') }}
                                    {{ Form::text("end_date", date('m/d/Y'), array('placeholder'=>'End Date','id'=>'end_date',"autocomplete" => "off", 'class' => 'start_end_date form-control')) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('Email', 'Email') }}
                                {{ Form::text("email", null, array('placeholder'=>'Email','id'=>'email',"autocomplete" => "off", 'class' => 'form-control')) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('Order Id', 'Order Id') }}
                                {{ Form::text("salecode", null, array('placeholder'=>'Order Id','id'=>'salecode',"autocomplete" => "off", 'class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('Reference Number', 'Reference Number') }}
                                {{ Form::text("order_ref_no", null, array('placeholder'=>'Reference Number','id'=>'order_ref_no',"autocomplete" => "off", 'class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('First Name', 'First Name') }}
                                {{ Form::text("firstname", null, array('placeholder'=>'First Name','id'=>'firstname',"autocomplete" => "off", 'class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('Last Name', 'Last Name') }}
                                {{ Form::text("lastname", null, array('placeholder'=>'Last Name','id'=>'lastname',"autocomplete" => "off", 'class' => 'form-control')) }}
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('Mobile Number', 'Mobile Number') }}
                                {{ Form::text("mobile", null, array('placeholder'=>'Mobile Number','id'=>'mobile',"autocomplete" => "off", 'class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('Season No', 'Season No') }}
                                <select class="form-control" name="season_no" id="season_no">
                                    <option value="27">Season 27</option>
                                    <option value="26">Season 26</option>
                                    <option value="25">Season 25</option>
                                </select>
                            </div>
                        </div>
                        {{--  <div class="col-md-2">
                            <div class="form-group">
                                <label for="" style="width: 100%;height: 20px;"></label>
                                <button class="btn btn-primary" id="exportBtn">Export</button>
                            </div>

                        </div>  --}}
                    </div>

                    <div class="table-responsive">

                        <table class="table table-striped table-bordered table-hover" id="shop-cart-transaction-table">

                            <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Order Ref No.</th>
                                <th>Amount</th>
                                <th>Email</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Phone Number</th>
                                <th>Date/Time Added</th>
                                <th>Transaction Status</th>
                                <th>Payment Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <link href="{{ asset('css/bootstrap-datepicker.css')}}" id="theme" rel="stylesheet">
    <script src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
    <script type='text/javascript'>
        $(function () {
            $("#global_dates").datepicker({
                toggleActive: !0
            });
        });
        $(function () {
            oTable = $('#shop-cart-transaction-table').DataTable({
                processing: true,
                serverSide: true,
                columnDefs: [
                    { "width": "150px", "targets": 9 }
                ],
                //autoWidth: false,
                ajax: {
                    url: '{!! route('admin.shop-cart-history-transaction.datatable') !!}',
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    data: function ( d ) {
                        if( $('#payment_status').val() != "") {
                            d.payment_status = $('#payment_status').val();
                        }
                        if( $('#transaction_status').val() != "") {
                            d.transaction_status = $('#transaction_status').val();
                        }
                        if( $('#start_date').val() != "") {
                            d.start_date = $('#start_date').val();
                        }
                        if( $('#end_date').val() != "") {
                            d.end_date = $('#end_date').val();
                        }
                        if( $('#salecode').val() != "") {
                            d.salecode = $('#salecode').val();
                        }
                        if( $('#order_ref_no').val() != "") {
                            d.order_ref_no = $('#order_ref_no').val();
                        }
                        if( $('#plate_form').val() != "") {
                            d.plate_form = $('#plate_form').val();
                        }
                        if( $('#firstname').val() != "") {
                            d.firstname = $('#firstname').val();
                        }
                        if( $('#lastname').val() != "") {
                            d.lastname = $('#lastname').val();
                        }
                        if( $('#email').val() != "") {
                            d.email = $('#email').val();
                        }
                        if( $('#mobile').val() != "") {
                            d.mobile = $('#mobile').val();
                        }
                        d.season_no = $('#season_no').val();

                    }
                },
                columns: [
                    {data: 'salecode', name: 'gv_shopcart_transaction.salecode'},
                    {data: 'order_id', name: 'gv_shopcart_transaction.order_id'},
                    {data: 'totalAmount', name: 'gv_shopcart_transaction.totalAmount'},
                    {data: 'email', name: 'gv_shopcart_transaction.email'},
                    {data: 'firstname', name: 'gv_shopcart_transaction.firstname'},
                    {data: 'lastname', name: 'gv_shopcart_transaction.lastname'},
                    {data: 'mobile', name: 'gv_shopcart_transaction.mobile'},
                    {data: 'created_date', name: 'gv_shopcart_transaction.created_date', orderable: false, searchable: false},
                    {data: 'transaction_status', name: 'transaction_status', orderable: false, searchable: false},
                    {data: 'payment_status', name: 'payment_status', orderable: false, searchable: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
            $(document).on("change", "#payment_status, #start_date, #end_date, #transaction_status, #plate_form, #season_no", function() {
                oTable.ajax.reload();
            });
            $(document).on("keyup", "#salecode, #order_ref_no, #mobile, #firstname, #lastname, #email", function() {
                oTable.ajax.reload();
            });

        });




    </script>
@stop
