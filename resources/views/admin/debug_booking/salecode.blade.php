@extends('adminlte::page')

@section('title', 'Booking')

@section('content_header')
<h1><i class="fa fa-key"></i> Booking
    @stop

    @section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header">
                    <?php $yesterday = date("Y-m-d",strtotime("-1 day")); ?>
                    <a href="{{ URL::to('admin/debug-booking/order_confirmed') }}?created_date={{$yesterday}}" class="btn btn-success  btn-sm">Confirmed</a>
                    <a href="{{ URL::to('admin/debug-booking/salecode') }}?created_date={{$yesterday}}" class="btn btn-success  btn-sm">Salecode Missing</a>
                </div>
                <div class="box-body">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="debug-table">
                            <thead>
                                <tr>
                                    <th>Salecode</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Product</th>
                                    <th>shopcartid</th>
                                    <th>Booking Id </th>
                                    <th>Ticket</th>
                                    <th>Created</th>
                                </tr>
                            </thead>

                            <tbody>
                               @if(!empty($results))
                                @foreach($results as $res)
                                    <tr @if($res->salecode_present == 'not exists') style="background-color:#FF0000" @endif >
                                        <td>{{$res->salecode}}</td>
                                        <td>{{$res->email}}</td>
                                        <td>{{$res->salecode_present}}</td>
                                        <td>{{$res->mainproductname}}</td>
                                        <td>{{$res->shopcartid}}</td>
                                        <td>{{$res->majlisBookingRefId}}</td>
                                        <td>{{$res->majlis_ticket_id}}</td>
                                        <td>{{$res->created_date}}</td>
                                        
                                    </tr>
                                @endforeach
                               @endif
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @stop
    @section('js')
<script type='text/javascript'>
    $(function () {
        
        oTable = $('#debug-table').DataTable({
            processing: true,
            paging: true,
            ordering: false,
            searching: true,
            pageLength : 100
            
        });
    });
    </script>

    @stop