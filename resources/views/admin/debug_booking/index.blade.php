@extends('adminlte::page')

@section('title', 'Booking')

@section('content_header')
<h1><i class="fa fa-key"></i> Booking
    @stop

    @section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header">
                    <?php $yesterday = date("Y-m-d",strtotime("-1 day")); ?>
                    <a href="{{ URL::to('admin/debug-booking/order_confirmed') }}?created_date={{$yesterday}}" class="btn btn-success btn-sm">Confirmed</a>
                    <a href="{{ URL::to('admin/debug-booking/salecode') }}?created_date={{$yesterday}}" class="btn btn-success  btn-sm">Salecode Missing</a>
                </div>
                <div class="box-body">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="debug-table">
                            <thead>
                                <tr>
                                    <th>Booking Id</th>
                                    <th>Salecode</th>
                                    <th>Created Date</th>
                                    <th>Program Date</th>
                                    <th>Booking Pslots</th>
                                    <th>Pslots</th>
                                    <th>Ticket</th>
                                </tr>
                            </thead>

                            <tbody>
                               @if(!empty($results))
                                @foreach($results as $res)
                                    <tr>
                                        <td>{{$res->id}}</td>
                                        <td>{{$res->sale_code}}</td>
                                        <td>{{$res->created_at}}</td>
                                        <td>{{$res->program_date}}</td>
                                        <td>{{$res->bookedPslots}}</td>
                                        <td>{{$res->slotcnt}}</td>
                                        <td>{{$res->ticket_ids}}</td>
                                    </tr>
                                @endforeach
                               @endif
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @stop
    @section('js')
<script type='text/javascript'>
    $(function () {
        
        oTable = $('#debug-table').DataTable({
            processing: true,
            paging: true,
            ordering: false,
            searching: true,
            pageLength : 100
            
        });
    });
    </script>

    @stop