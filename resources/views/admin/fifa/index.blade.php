@extends('adminlte::page')

@section('title', 'Fifa Matches')

@section('content_header')
<h1>Fifa Matches</h1>
@stop

@section('content') 
<div class="box">
    <div class="box-body">
        <div class="col-md-4">
            <div class="form-group" >
                <label>Start Date</label>
                <input type="text" name="date_start" id="date_start" class="form-control filter datepickerExport">

            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group" >
                <label>End Date</label>
                <input type="text" name="date_end" id="date_end" class="form-control filter datepickerExport">                                
            </div>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <label>&nbsp;</label>
                <input type="button" name="export" class="form-control btn btn-primary" value="Export" id="export_btn">
            </div>
        </div>


    </div>                    
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-body">
                <div class="table-responsive" style="overflow-x: hidden;">
                    <table class="table table-striped table-bordered table-hover" id="list-table">

                        <thead>
                            <tr>
                                <th>Title English</th>
                                <th>Title Arabic</th>
                                <th>Date</th>                               
                                <th>Time</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section("css")
<link href="{{ asset('css/bootstrap-datepicker.css')}}"  rel="stylesheet">
@stop
@section('js')
<script src="{{URL::asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
<script type='text/javascript'>
$('.datepickerExport').datepicker({
    toggleActive: !0,
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
    orientation: "auto bottom",
    autoclose: true,
});
$(function () {

    oTable = $('#list-table').DataTable({
        processing: true,
        serverSide: true,
        ordering: false,
        searching: false,
        ajax: {
            url: "{!! route('admin.fifa-matches.datatable') !!}",
            type: 'post',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            data: function (d) {
                d.date_start = $("#date_start").val();
                d.date_end = $("#date_end").val();
            },
        },
        columns: [
            {data: 'titles.en', name: 'titles->en', orderable: false, className: "dt-let"},
            {data: 'titles.ar', name: 'titles->ar', orderable: false, className: "dt-right"},
            {data: 'match_date', name: 'match_date', orderable: false, searchable: false, className: "dt-center"},
            {data: 'match_time', name: 'match_time', orderable: false, searchable: false, className: "dt-center"},
            {data: 'actions', name: 'actions', orderable: false, searchable: false, className: "dt-center"}
        ]
    });
    $(".filter").on("change", function () {
        oTable.draw();
    });
    $("#export_btn").on("click", function () {
        var url = "{!! route('admin.fifa-matches.export') !!}";
        var startDate = $("#date_start").val();
        var endDate = $("#date_end").val();
        url += '?startDate=' + startDate + '&endDate=' + endDate;
        window.location.href = url;
    });

    $('#list-table').on('click', '.destroy', function (e) {
        e.preventDefault();

        var href = $(this).attr('href');

        swal({
            title: "{{ trans('myadmin.confirm-delete') }}",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: href,
                            method: 'DELETE',
                            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                            beforeSend: function () {
                                Pace.restart();//Pace.start();
                            },
                            success: function () {
                                oTable.ajax.reload();
                                swal("Done!", "Successfully deleted.", "success");
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                swal("Error deleting!", "Please try again", "error");
                            },
                            complete: function () {
                                Pace.stop();
                            }
                        });
                    } else {
                        return true;
                    }
                });
    });
});
</script>    
@stop