@extends('adminlte::page')

@section('title', 'Import Offers')

@section('content_header')
<h1><i class='fa fa-plus'></i> Import Offers</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-primary showForm">
            <div class="box-header">
            </div>
            <div class="box-body">

                {{ Form::open(array('route' => 'admin.import.offers','files'=>'true','class'=>'showLoader')) }}

                <div class="form-group {{ $errors->has('import_file') ? 'has-error' : '' }}">
                    {{ Form::label('import_file', 'Excel') }}
                    {{ Form::file('import_file',array('class' => 'form-control','autocomplete' => 'off')) }}
                    {!! $errors->first('import_file','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
                <input name="type" type="hidden" value="{{request()->type}}" />
                {{ Form::submit('SUBMIT', array('class' => 'btn btn-primary')) }}
                <p class="help-block"><a href="{{URL::to( '/samples/pack_offers_sample.csv')}}" download="pack_offers_sample.csv" class="text-sm"><b>Download</b></a> Sample CSV Format.</p>
                {{ Form::close() }}

            </div>
            <div class="overlay hidden">
                <i class="fa fa-spinner fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop
@section('js')
<script type='text/javascript'>
    $(function () {
        $(".showLoader").on("submit", function () {
            $(".showForm").find("div.overlay").removeClass('hidden');
            $(this).find(":submit").attr('value', 'Submitted').prop('disabled', true);
        });
    });
</script>
@stop
