@if($result['action_type'] == "send-mail")
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #3c8dbc;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span style="color: white" aria-hidden="true">&times;</span></button>
                <h4 style="color: white" class="modal-title">Send Mail</h4>
            </div>
            <form role="form" id="send-mail-form" action="{{url('admin/shop-cart-transaction')}}" method="post" data-parsley-validate>
                {!! csrf_field() !!}
                <input name="cart_transaction_id" id="cart_transaction_id" type="hidden" value="{{$result['action_id']}}">
                <input name="action_type" id="action_type" type="hidden" value="{{$result['action_type']}}">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" name="sEmail" required id="sEmail" value="@if($result['cart_detail']){{$result['cart_detail']->email}}@endif" class="form-control"  placeholder="Enter email">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary submit_button_action disabled_button">Send</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
@elseif($result['action_type'] == "generate-ticket")
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #3c8dbc;">
                <button  type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span style="color: white" aria-hidden="true">&times;</span></button>
                <h4 style="color: white" class="modal-title">Generate Ticket</h4>
            </div>
            <form role="form" id="generate-ticket-form" action="{{url('admin/shop-cart-transaction')}}" method="post" data-parsley-validate>
                {!! csrf_field() !!}
                <input name="cart_transaction_id" id="cart_transaction_id" type="hidden" value="{{$result['action_id']}}">
                <input name="action_type" id="action_type" type="hidden" value="{{$result['action_type']}}">

                <div class="modal-body">

                <p>Are you sure to generate ticket?</p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary disabled_button">Generate Ticket</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
@elseif($result['action_type'] == "check-payment-status")
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #3c8dbc;">
                <button  type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span style="color: white" aria-hidden="true">&times;</span></button>
                <h4 style="color: white" class="modal-title"> Check Payment Status</h4>
            </div>
            <form role="form" id="check-payment-status-form" action="{{url('admin/shop-cart-transaction')}}" method="post" data-parsley-validate>
                {!! csrf_field() !!}
                <input name="cart_transaction_id" id="cart_transaction_id" type="hidden" value="{{$result['action_id']}}">
                <input name="action_type" id="action_type" type="hidden" value="{{$result['action_type']}}">

                <div class="modal-body">
                    <div class="response_data"> <p>Are you sure check the payment status?</p></div>
                     <textarea  rows="10" cols="92" class="show_json_data" style="display: none;">

                     </textarea>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary disabled_button">Check</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>

@endif
