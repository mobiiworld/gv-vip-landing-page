@extends('adminlte::page')

@section('title', 'Product Id')

@section('content_header')
<h1> Seasons</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <h3>&nbsp;</h3>
                <div class="box-tools">
                    <a href="{{route('admin.pack-product-config.create')}}" class="btn btn-sm btn-primary">Create Product Id</a>
                </div>

            </div>
            <div class="box-body">
            
                
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="list-table">

                        <thead>
                            <tr>
                                <th>Slug</th>
                                <th>Product</th>
                                <th>Product Name</th>
                                <th>Product ID</th>                               
                                <th></th>
                            </tr>
                        </thead>


                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section("css")

<link rel="stylesheet" href="{{asset('css/bootstrap4-toggle.min.css')}}">
@stop
@section('js')
<script src="{{asset('js/bootstrap4-toggle.min.js')}}"></script>
<script type='text/javascript'>
    $(function () {
        
        oTable = $('#list-table').DataTable({
            processing: true,
            serverSide: true,
            //autoWidth: false,
            ordering: false,
            ajax: {
                url: '{!! route('admin.pack-product-config.datatable') !!}',
                type: 'post',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                 data: function ( d ) {
                     
                },
            },
           
            columns: [
               
                {data: 'slug', name: 'slug'},
                {data: 'product', name: 'product'},
                {data: 'product_name', name: 'product_name'},
                {data: 'product_id', name: 'product_id'},
                
                {data: 'actions', name: 'actions', orderable: false, searchable: false}
            ],
           
        });
        

        $('#list-table').on('click', '.destroy', function (e) {
            e.preventDefault();
            var href = $(this).attr('href');

            swal({
                title: "{{ trans('myadmin.confirm-delete') }}",
                text: "Once deleted, you will not be able to recover this file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: href,
                                method: 'DELETE',
                                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                                beforeSend: function () {
                                    Pace.restart();//Pace.start();
                                },
                                success: function () {
                                    oTable.ajax.reload();
                                    swal("Done!", "User successfully deleted.", "success");
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    swal("Error deleting!", "Please try again", "error");
                                },
                                complete: function () {
                                    Pace.stop();
                                }
                            });
                        } else {
                            return true;
                        }
                    });
        });
      

    });

</script>    
@stop