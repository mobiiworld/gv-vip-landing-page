

<div class="form-group {{ $errors->has('product') ? 'has-error' : '' }}">
    {{ Form::label('product', 'Product') }}
    {{ Form::text('product', null, array('class' => 'form-control', 'required'=>"required")) }}
     {!! $errors->first('product','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>

<div class="form-group {{ $errors->has('product_name') ? 'has-error' : '' }}">
    {{ Form::label('product_name', 'Product Name') }}
    {{ Form::text('product_name', null, array('class' => 'form-control', 'required'=>"required")) }}
     {!! $errors->first('product_name','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>

<div class="form-group {{ $errors->has('product_id') ? 'has-error' : '' }}">
    {{ Form::label('product_id', 'Product ID') }}
    {{ Form::text('product_id', null, array('class' => 'form-control', 'required'=>"required")) }}
     {!! $errors->first('product_id','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>
<div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
	{{ Form::label('slug', 'Slug') }}
    {{ Form::select('slug', $pack_product_slug,null, array('class' => 'form-control', 'required'=>"required")) }}
     {!! $errors->first('slug','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>                      