@extends('adminlte::page')

@section('title', 'Packs')

@section('content_header')
<h1> Packs</h1>
@stop

@section('content')
               

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Offers</h3>
                
            </div>
            <div class="box-body">
               <table class="table">
                <thead>
                    <th>Merchant </th>
                       <th>Offer</th>
                       <th>select All<input type="checkbox" name="offerdownload[]" value="1" class="checkall"></th>
                </thead>
                   <tbody>
                       @foreach($allOffers as $offer)
                            <tr>
                                <td>{{ ucfirst($offer->merchant_name_en)}}</td>
                                <td>{{ $offer->offer_name}}</td>
                                <td><input type="checkbox" name="offerdownload[]" value="{{$offer->id}}" class="singlecheck"></td>
                            </tr>
                       @endforeach
                   </tbody>
                   <tfoot>
                       <a href="javascript:void(0)" class="btn btn-primary" id="download">Download Selected</a>
                   </tfoot>
               </table>
            </div>
        </div>
    </div>
</div>

@stop

@section("js")
<script type="text/javascript">
    $(document).ready(function(){
        
        $(".checkall").on("click",function(){
            $(".singlecheck").prop('checked',false);
            if($(this).is(':checked')){
                $(".singlecheck").prop('checked',true);
            }
        })
        $("#download").on("click",function(){
            if($(".singlecheck:checked").length >0){
                var str = '';
                var apnd = '';
                $(".singlecheck:checked").each(function(){
                    str = str+apnd+$(this).val();
                    apnd = '|';
                })
                window.location.href = '{{route('admin.season.downloadOffer',$packId)}}?ids='+str;
            }else{
                alert("select atleast one");
            }
        });
    });
</script>
@stop