@extends('adminlte::page')

@section('title', 'Seasons')

@section('content_header')
<h1> Seasons</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <h3>&nbsp;</h3>
                <div class="box-tools">
                    <a href="{{route('admin.season.create')}}" class="btn btn-sm btn-primary">Create season</a>
                </div>

            </div>
            <div class="box-body">
            
                
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="list-table">

                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Season Number</th>
                                <th>Status</th>                               
                                <th>Pack Activation</th>                               
                                <th></th>
                            </tr>
                        </thead>


                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section("css")

<link rel="stylesheet" href="{{asset('css/bootstrap4-toggle.min.css')}}">
@stop
@section('js')
<script src="{{asset('js/bootstrap4-toggle.min.js')}}"></script>
<script type='text/javascript'>
    $(function () {
        
        oTable = $('#list-table').DataTable({
            processing: true,
            serverSide: true,
            //autoWidth: false,
            ordering: false,
            ajax: {
                url: '{!! route('admin.season.datatable') !!}',
                type: 'post',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                 data: function ( d ) {
                     d.status = $("#status").val();
                },
            },
           
            columns: [
               
                {data: 'name', name: 'name'},
                {data: 'season_number', name: 'season_number'},
                {data: 'status', name: 'status', orderable: false, searchable: false},
                {data: 'pack_activation_enabled', name: 'pack_activation_enabled', orderable: false, searchable: false},
                {data: 'actions', name: 'actions', orderable: false, searchable: false}
            ],
            fnDrawCallback: function () {
                $(".bsSwitch").bootstrapToggle({
                    size: "sm",
                    onstyle: "success",
                    offstyle: "danger",
                    on: 'Enabled',
                    off: 'Disabled'
                });
                $(".bsSwitchPackActv").bootstrapToggle({
                    size: "sm",
                    onstyle: "success",
                    offstyle: "danger",
                    on: 'Enabled',
                    off: 'Disabled'
                });
            }
        });
        $(".filter").on("change",function(){
            oTable.draw();
        });

       $(document).on('change', '.bsSwitch', function () {
            var id = $(this).data('id');
            var st = $(this).data('st');
            if(st == 'inactive'){
                swal({
                  title: "Are you sure?",
                  text: "Once activated, all packs, offers, imports and exports based on this season!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((ch) => {
                  if (ch) {
                    changeSeasonStatus(id,st);
                  }else{
                    oTable.draw();
                  }
                });
            }else{
                changeSeasonStatus(id,st);
            }
                        
       });

       $(document).on('change', '.bsSwitchPackActv', function () {
            var id = $(this).data('id');
            var st = $(this).data('st');
            swal({
              title: "Are you sure?",
              text: "",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((ch) => {
              if (ch) {
                changePackActivation(id,st);
              }else{
                oTable.draw();
              }
            });
                        
       });

    });

function changePackActivation(id,st){
    $.ajax({
        url: '{!! route('admin.season.changePackActivation') !!}',
        dataType: 'json',
       
        cache: false,
        method: 'POST',
        data : { id : id, st : st},
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
        beforeSend: function () {
           
        },
        success: function (response) {
            if(response.status == 'success'){
                swal("Success", response.message, "success");
            }else{
                swal("Error", response.message, "error");
            }
            oTable.draw();
        },
        error: function (response) {
            
        },
        complete: function () {
            
        }
    });
}

function changeSeasonStatus(id,st){
    $.ajax({
        url: '{!! route('admin.season.changeStatus') !!}',
        dataType: 'json',
       
        cache: false,
        method: 'POST',
        data : { id : id, st : st},
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
        beforeSend: function () {
           
        },
        success: function (response) {
            if(response.status == 'success'){
                swal("Success", response.message, "success");
            }else{
                swal("Error", response.message, "error");
            }
            oTable.draw();
        },
        error: function (response) {
            
        },
        complete: function () {
            
        }
    });
}
</script>    
@stop