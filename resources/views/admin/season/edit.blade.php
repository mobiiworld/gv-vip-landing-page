@extends('adminlte::page')

@section('title', 'Edit Season')

@section('content_header')
<h1> Edit Season</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-tools">
                    <a href="{{route('admin.season.index')}}" class="btn btn-sm btn-default">Back</a>
                </div>
            </div>
            

            {{ Form::model($season,array('url' => route('admin.season.update',$season->id),'method'=>'put')) }}
            <div class="box-body">
                @include("admin.season.form")
              
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
                    </div>
                </div>
            </div>
            {{ Form::close() }}

                
        </div>
    </div>
</div>
@stop
