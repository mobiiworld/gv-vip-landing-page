

<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    {{ Form::label('name', 'Name') }}
    {{ Form::text('name', null, array('class' => 'form-control', 'required'=>"required")) }}
     {!! $errors->first('name','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>

<div class="form-group {{ $errors->has('season_number') ? 'has-error' : '' }}">
    {{ Form::label('season_number', 'Season Number') }}
    {{ Form::number('season_number', null, array('class' => 'form-control')) }}
     {!! $errors->first('season_number','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>

<div class="form-group {{ $errors->has('pack_activation_disbaled_msg_en') ? 'has-error' : '' }}">
    {{ Form::label('pack_activation_disbaled_msg_en', 'Pack Activation Disbaled Msg (en)') }}
    {{ Form::text('pack_activation_disbaled_msg_en', null, array('class' => 'form-control')) }}
     {!! $errors->first('pack_activation_disbaled_msg_en','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>
<div class="form-group {{ $errors->has('pack_activation_disbaled_msg_ar') ? 'has-error' : '' }}">
    {{ Form::label('pack_activation_disbaled_msg_ar', 'Pack Activation Disbaled Msg (ar)') }}
    {{ Form::text('pack_activation_disbaled_msg_ar', null, array('class' => 'form-control')) }}
     {!! $errors->first('pack_activation_disbaled_msg_ar','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>                      