@extends('adminlte::page')

@section('title', 'VIP Pack Activation Settings')

@section('content_header')
<h1> VIP Pack Activation Settings</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-success">
            <div class="box-header">
                
            </div>
            

            {{ Form::model($season,array('url' => route('admin.current-season-update',$season->id),'method'=>'post', 'files'=>true)) }}
            <div class="box-body">
                <div class="form-group {{ $errors->has('pack_activation_enabled') ? 'has-error' : '' }}">
                   
                    {{ Form::checkbox('pack_activation_enabled', '1', $season->pack_activation_enabled, array('class' => '')) }}
                     {{ Form::label('pack_activation_enabled', 'Enable Pack Activation ') }}
                     {!! $errors->first('pack_activation_enabled','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div> 
                <div class="form-group {{ $errors->has('pack_activation_disabled_title_en') ? 'has-error' : '' }}">
                    {{ Form::label('pack_activation_disabled_title_en', 'Pack Activation Disbaled Title (en)') }}
                    {{ Form::text('pack_activation_disabled_title_en', null, array('class' => 'form-control')) }}
                     {!! $errors->first('pack_activation_disabled_title_en','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
                <div class="form-group {{ $errors->has('pack_activation_disabled_title_ar') ? 'has-error' : '' }}">
                    {{ Form::label('pack_activation_disabled_title_ar', 'Pack Activation Disbaled Title (ar)') }}
                    {{ Form::text('pack_activation_disabled_title_ar', null, array('class' => 'form-control')) }}
                     {!! $errors->first('pack_activation_disabled_title_ar','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
                <div class="form-group {{ $errors->has('pack_activation_disbaled_msg_en') ? 'has-error' : '' }}">
                    {{ Form::label('pack_activation_disbaled_msg_en', 'Pack Activation Disbaled Msg (en)') }}
                    {{ Form::textarea('pack_activation_disbaled_msg_en', null, array('class' => 'form-control')) }}
                     {!! $errors->first('pack_activation_disbaled_msg_en','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
                <div class="form-group {{ $errors->has('pack_activation_disbaled_msg_ar') ? 'has-error' : '' }}">
                    {{ Form::label('pack_activation_disbaled_msg_ar', 'Pack Activation Disbaled Msg (ar)') }}
                    {{ Form::textarea('pack_activation_disbaled_msg_ar', null, array('class' => 'form-control')) }}
                     {!! $errors->first('pack_activation_disbaled_msg_ar','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div> 
                
                <div class="form-group ">
                    <label for="image">Image for Popup (Pack activation disabled) </label>
                    <input class="form-control" accept="image/png, image/gif, image/jpeg" autocomplete="off" name="image" type="file" id="image">
                    {!! $errors->first('image','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div>
                
                @if((isset($season) && $season->pack_activation_disabled_image != NULL))
               
                    <div class="form-group ">
                        <img src="{{$season->pack_activation_disabled_image}}" class="img-square img-size-30 mt-2 pt-2" width="100"/>
                    </div>
                
                @endif

                <div class="form-group {{ $errors->has('show_register_popup') ? 'has-error' : '' }}">
                   
                    {{ Form::checkbox('show_register_popup', '1', $season->show_register_popup, array('class' => '')) }}
                     {{ Form::label('show_register_popup', 'Show register interest popup ') }}
                     {!! $errors->first('show_register_popup','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div> 

                <div class="form-group {{ $errors->has('fixed_popup') ? 'has-error' : '' }}">                   
                    {{ Form::checkbox('fixed_popup', '1', $season->fixed_popup, array('class' => '')) }}
                     {{ Form::label('fixed_popup', 'Fixed popup  ') }}
                     {!! $errors->first('fixed_popup','<p class="text-danger"><strong>:message</strong></p>') !!}
                </div> 
                
                
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
                    </div>
                </div>
            </div>
            {{ Form::close() }}

                
        </div>
    </div>
</div>
@stop
