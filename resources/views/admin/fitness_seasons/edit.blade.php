@extends('adminlte::page')

@section('title', 'Edit Season')

@section('content_header')
<h1> Edit Season</h1>
@stop

@section('content')
<style>
    #map-canvas {
        width: 100%;
        height: 500px;
    }

</style>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-tools">
                    <a href="{{route('admin.fitness-season.index')}}" class="btn btn-sm btn-default">Back</a>
                </div>
            </div>
            {{ Form::model($fitness_seasons,array('url' => route('admin.fitness-season.update',$fitness_seasons->id),'enctype' => 'multipart/form-data', 'method' => 'PUT')) }}
            <div class="box-body">
                @include("admin.fitness_seasons.form")
            </div>
            <div class="box-footer">
                {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
            </div>
            {{ Form::close() }}

        </div>
    </div>
</div>

<div class="modal" id="linkImage">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Link to Youtube</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {{ Form::text('imgLink', null, array('class' => 'form-control','id'=>'imgLink')) }}
                    <p class="text-danger"><strong>If value, the image will be treated as Youtube video.</strong></p>
                </div>
            </div>
            <div class="modal-footer">
                    <button type="button" class="btn btn-clear" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-apply linkImageSubmit" data-type="approve">Save</button>
            </div>
        </div>
    </div>
</div>    
@stop
@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<link href="{{ asset('css/bootstrap-datepicker.css')}}" id="theme" rel="stylesheet">
<script src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('API_MAP_KEY', 'AIzaSyBpC8Nftwn_ZbBh42oKbCwDgygVi8B271Y') }}&libraries=places,drawing&language=en&callback=initialize" async defer></script>

<script type='text/javascript'>
$(function () {
$("#global_dates").datepicker({
    toggleActive: !0
});
});
var placeSearch, autocomplete, map, _myPolygon;
function initialize() {
initMap();
initAutocomplete();
}
function initMap() {

var arr = {{$mapCordinates}};
var polygonCoords = [];
if (arr != null) {
for (var j = 0; j < arr.length; j++) {
    polygonCoords.push(new google.maps.LatLng(arr[j][0], arr[j][1]));
}
}



map = new google.maps.Map(document.getElementById('map-canvas'), {
zoom: 10
});
_myPolygon = new google.maps.Polyline({
path: polygonCoords,
draggable: false,
editable: false,
strokeColor: '#FF0000',
strokeOpacity: 0.8,
strokeWeight: 2,
fillColor: '#FF0000',
fillOpacity: 0.35,
map: map
});
// Create the bounds object
var bounds = new google.maps.LatLngBounds();
// Get paths from polygon and set event listeners for each path separately
_myPolygon.getPath().forEach(function (path, index) {
bounds.extend(path);
});
// Fit Polygon path bounds
map.fitBounds(bounds);
var drawingManager = new google.maps.drawing.DrawingManager({
drawingMode: google.maps.drawing.OverlayType.PLOYGON,
drawingControl: false,
drawingControlOptions: {
    position: google.maps.ControlPosition.TOP_CENTER,
    drawingModes: ['polygon']
},
polygonOptions: {
    draggable: true,
    editable: true,
    strokeColor: '#FF0000',
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: '#FF0000',
    fillOpacity: 0.35
}
});
polyline = google.maps.event.addListener(drawingManager, 'polygoncomplete', function (shape) {
if (shape == null || (!(shape instanceof google.maps.Polygon)))
    return;
var coordinatesArray = shape.getPath().getArray();
$('#area_cordinates_map').val(JSON.stringify(coordinatesArray));
drawingManager.setOptions({drawingMode: null, drawingControl: false});
_myPolygon = shape;
});
drawingManager.setMap(map);
$("#remove_polygon").click(function () {
_myPolygon.setMap(null);
drawingManager.setOptions({drawingMode: null, drawingControl: true});
});

var input = document.getElementById('searchmap');
google.maps.event.addDomListener(input, 'keydown', function (event) {
if (event.keyCode === 13) {
event.preventDefault();
}
});
}

function initAutocomplete() {
// Create the autocomplete object, restricting the search to geographical
// location types.
autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */
                (document.getElementById('searchmap')));
// When the user selects an address from the dropdown, populate the address
// fields in the form.
autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
// Get the place details from the autocomplete object.
var place = autocomplete.getPlace();
if (!place.geometry) {
// User entered the name of a Place that was not suggested and
// pressed the Enter key, or the Place Details request failed.
    window.alert("No details available for input: '" + place.name + "'");
    return;
} else {
    map.setCenter(place.geometry.location);
    map.setZoom(17);
}
}

function pan() {
var panPoint = new google.maps.LatLng(document.getElementById("lat").value, document.getElementById("lng").value);
map.setCenter(panPoint);
map.setZoom(17);
}

$("ul.sortable-images").sortable({
items: "li",
cursor: 'move',
opacity: 0.6,
update: function (e) {
    sendOrderToServer($(this).attr('data-lang'));
}
});
function sendOrderToServer(lang) {
var order = [];
var token = $('meta[name="csrf-token"]').attr('content');
$('li.row-'+lang).each(function (index, element) {
    order.push({
        id: $(this).attr('data-id'),
        position: index + 1
    });
});
$.ajax({
    type: "POST",
    dataType: "json",
    url: "{{ url('admin/post-sortable') }}",
    data: {
        order: order,
        _token: token
    },
    success: function (response) {
        if (response.status == "success") {
            console.log(response);
        } else {
            console.log(response);
        }
    }
});
}

var clickedImg;

$('a.imgType').on('click', function (e) {
e.preventDefault();
clickedImg = $(this);
$("#linkImage").find('#imgLink').val($(this).data('link'));
$("#linkImage").modal('show');
});

$('button.linkImageSubmit').on('click', function (e) {
  e.preventDefault();
  var link = $("#linkImage").find('#imgLink').val();
  var id = clickedImg.data('id');
  $.ajax({
    type: "POST",
    dataType: "json",
    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
    url: "{{ url('admin/fitness-season-images-url') }}",
    data: {
        link: link,
        id: id,
    },
    success: function (response) {
        if (response.status) {
            clickedImg.data('link',link);
            clickedImg.html(response.type);
            $("#linkImage").modal('hide');
        } 
    }
});  
});

$('input.rdDefault').change(function() {
        if ($(this).is(':checked')) {
            var id = $(this).val();
            $.ajax({
                type: "POST",
                dataType: "json",
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                url: "{{ url('admin/fitness-season-images-setdefault') }}",
                data: {
                id: id,
                season_id:{{$fitness_seasons->id}}
                },
                success: function (response) {
                    console.log(response);
                }
            });  
        }
});
</script>
@stop