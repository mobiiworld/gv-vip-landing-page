<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('names.en') ? 'has-error' : '' }}">
            {{ Form::label('names', 'Name (en)') }}
            {{ Form::text('names[en]', null, array('class' => 'form-control', 'required'=>"required",'dir'=>'ltr')) }}
            {!! $errors->first('names.en','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
        <div class="form-group {{ $errors->has('popup_titles.en') ? 'has-error' : '' }}">
            {{ Form::label('popup_titles', 'Popup Title (en)') }}
            {{ Form::text('popup_titles[en]', null, array('class' => 'form-control', 'required'=>"required",'dir'=>'ltr')) }}
            {!! $errors->first('popup_titles.en','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
        <div class="form-group {{ $errors->has('popup_messages.en') ? 'has-error' : '' }}">
            {{ Form::label('popup_messages', 'Popup Message (en)') }}
            {{ Form::text('popup_messages[en]', null, array('class' => 'form-control', 'required'=>"required",'dir'=>'ltr')) }}
            {!! $errors->first('popup_messages.en','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
        <div class="form-group {{ $errors->has('target_steps.daily') ? 'has-error' : '' }}">
            {{ Form::label('target_steps', 'Target Steps (Daily)') }}
            {{ Form::number('target_steps[daily]', null, array('class' => 'form-control', 'required'=>"required",'dir'=>'ltr')) }}
            {!! $errors->first('target_steps.daily','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
        <div class="form-group {{ $errors->has('target_steps.season') ? 'has-error' : '' }}">
            {{ Form::label('target_steps', 'Target Steps (Season)') }}
            {{ Form::number('target_steps[season]', null, array('class' => 'form-control', 'required'=>"required",'dir'=>'ltr')) }}
            {!! $errors->first('target_steps.season','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
        <div class="form-group {{ $errors->has('max_ups_prize_count') ? 'has-error' : '' }}">
            {{ Form::label('max_ups_prize_count', 'User Max.Prize count/Season') }}
            {{ Form::number('max_ups_prize_count', null, array('class' => 'form-control', 'required'=>"required", "min"=>1)) }}
            {!! $errors->first('max_ups_prize_count','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
        <div class="form-group">

            <div class="checkbox">
                <label>
                    <b> <input name="is_show_homepage" id="is_show_homepage" value="1" @if(isset($fitness_seasons) && $fitness_seasons->is_show_homepage == 1) checked @endif type="checkbox">Show In Home Page</b>
                </label>

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('names.ar') ? 'has-error' : '' }}">
            {{ Form::label('names', 'Name (ar)') }}
            {{ Form::text('names[ar]', null, array('class' => 'form-control', 'required'=>"required",'dir'=>'rtl')) }}
            {!! $errors->first('names.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
        <div class="form-group {{ $errors->has('popup_titles.ar') ? 'has-error' : '' }}">
            {{ Form::label('popup_titles', 'Popup Title (ar)') }}
            {{ Form::text('popup_titles[ar]', null, array('class' => 'form-control', 'required'=>"required",'dir'=>'rtl')) }}
            {!! $errors->first('popup_titles.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
        <div class="form-group {{ $errors->has('popup_messages.ar') ? 'has-error' : '' }}">
            {{ Form::label('popup_messages', 'Popup Message (ar)') }}
            {{ Form::text('popup_messages[ar]', null, array('class' => 'form-control', 'required'=>"required",'dir'=>'rtl')) }}
            {!! $errors->first('popup_messages.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
        <div class="form-group {{ $errors->has('target_steps.monthly') ? 'has-error' : '' }}">
            {{ Form::label('target_steps', 'Target Steps (Monthly)') }}
            {{ Form::number('target_steps[monthly]', null, array('class' => 'form-control', 'required'=>"required")) }}
            {!! $errors->first('target_steps.monthly','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
        <div class="input-daterange" id="global_dates">
            <div class="col-md-4">
                <div class="form-group {{ $errors->has('start_date') ? 'has-error' : '' }}" >
                    {{ Form::label('start_date', 'Start Date') }}
                    {{ Form::text("start_date",  (isset($fitness_seasons))?date('m/d/Y',strtotime($fitness_seasons->start_date)):date('m/d/Y'), array('placeholder'=>'Start Date','id'=>'start_date',"autocomplete" => "off", 'class' => 'start_end_date form-control')) }}
                    {!! $errors->first('start_date','<p class="text-danger"><strong>:message</strong></p>') !!}

                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group {{ $errors->has('end_date') ? 'has-error' : '' }}" >
                    {{ Form::label('end_date', 'End Date') }}
                    {{ Form::text("end_date", (isset($fitness_seasons))?date('m/d/Y',strtotime($fitness_seasons->end_date)):date('m/d/Y'), array('placeholder'=>'End Date','id'=>'end_date',"autocomplete" => "off", 'class' => 'start_end_date form-control')) }}
                    {!! $errors->first('end_date','<p class="text-danger"><strong>:message</strong></p>') !!}

                </div>
            </div>
            
        </div>
        <div class="col-md-4">
            <div class="form-group {{ $errors->has('duration') ? 'has-error' : '' }}" >
                {{ Form::label('duration', 'Duration (Minutes)') }}
                {{ Form::number("duration", null, array('placeholder'=>'Duration','id'=>'duration',"autocomplete" => "off", 'class' => ' form-control')) }}
                {!! $errors->first('duration','<p class="text-danger"><strong>:message</strong></p>') !!}

            </div>
        </div>
        <div class="input-daterange">
            <div class="col-md-4">
                <div class="form-group {{ $errors->has('start_time') ? 'has-error' : '' }}" >
                    {{ Form::label('start_time', 'Start Time') }}
                    {{ Form::time("start_time",  (isset($fitness_seasons))?date('H:i:s',strtotime($fitness_seasons->start_time)):date('H:i:s'), array('placeholder'=>'Start Time','id'=>'start_time',"autocomplete" => "off", 'class' => ' form-control')) }}
                    {!! $errors->first('start_time','<p class="text-danger"><strong>:message</strong></p>') !!}

                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group {{ $errors->has('end_time') ? 'has-error' : '' }}" >
                    {{ Form::label('end_time', 'End Time') }}
                    {{ Form::time("end_time", (isset($fitness_seasons))?date('H:i:s',strtotime($fitness_seasons->end_time)):date('H:i:s'), array('placeholder'=>'End Time','id'=>'end_time',"autocomplete" => "off", 'class' => ' form-control')) }}
                    {!! $errors->first('end_time','<p class="text-danger"><strong>:message</strong></p>') !!}

                </div>
            </div>
            <div class="col-md-4">
                <label>&nbsp;</label>
                <div class="form-check {{ $errors->has('is_next_day') ? 'has-error' : '' }}" >
                    {{ Form::label('is_next_day', 'Next day',array('class'=>'form-check-label')) }}
                    {{ Form::checkbox("is_next_day",1, (isset($fitness_seasons) && $fitness_seasons->is_next_day == 1) ? true : false, array('id'=>'is_next_day',"autocomplete" => "off", 'class' => ' form-check-input')) }}
                    {!! $errors->first('is_next_day','<p class="text-danger"><strong>:message</strong></p>') !!}

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="box box-primary box-solid collapsed-box">
            <div class="box-header" data-widget="collapse">
                <h3 class="box-title">Fitness Daily Prizes</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('fitness_prizes_names_daily.en') ? 'has-error' : '' }}">
                        {{ Form::label('fitness_prizes_names_daily', 'Name (en)') }}
                        {{ Form::text('fitness_prizes_names_daily[en]', isset($fitness_prizes_daily)?$fitness_prizes_daily->names['en']:null, array('class' => 'form-control', 'dir'=>'ltr')) }}
                        {!! $errors->first('fitness_prizes_names_daily.en','<p class="text-danger"><strong>:message</strong></p>') !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('fitness_prizes_names_daily.ar') ? 'has-error' : '' }}">
                        {{ Form::label('fitness_prizes_names_daily', 'Name (ar)') }}
                        {{ Form::text('fitness_prizes_names_daily[ar]', isset($fitness_prizes_daily)?$fitness_prizes_daily->names['ar']:null, array('class' => 'form-control', 'dir'=>'rtl')) }}
                        {!! $errors->first('fitness_prizes_names_daily.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('fitness_prizes_description_daily.en') ? 'has-error' : '' }}">
                        {{ Form::label('fitness_prizes_description_daily', 'Description (en)') }}
                        {{ Form::textarea('fitness_prizes_description_daily[en]', isset($fitness_prizes_daily)?$fitness_prizes_daily->descriptions['en']:null, array('class' => 'form-control', 'dir'=>'ltr')) }}
                        {!! $errors->first('fitness_prizes_description_daily.en','<p class="text-danger"><strong>:message</strong></p>') !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('fitness_prizes_description_daily.ar') ? 'has-error' : '' }}">
                        {{ Form::label('fitness_prizes_description_daily', 'Description (ar)') }}
                        {{ Form::textarea('fitness_prizes_description_daily[ar]', isset($fitness_prizes_daily)?$fitness_prizes_daily->descriptions['ar']:null, array('class' => 'form-control', 'dir'=>'rtl')) }}
                        {!! $errors->first('fitness_prizes_description_daily.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group ">
                        <label for="import_file">Icon (Recommended : 100x100)</label>
                        <input class="form-control" accept="image/png, image/gif, image/jpeg" autocomplete="off" name="daily_icon" type="file" id="daily_icon">
                    </div>
                </div>
                @if((isset($fitness_prizes_daily) && $fitness_prizes_daily->icon != NULL))
                <div class="col-md-6">
                    <div class="form-group ">
                        <img src="{{$fitness_prizes_daily->icon}}" class="img-square img-size-30 mt-2 pt-2" width="100"/>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="box box-primary box-solid collapsed-box">
            <div class="box-header" data-widget="collapse">
                <h3 class="box-title">Fitness Monthly Prizes</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('fitness_prizes_names_monthly.en') ? 'has-error' : '' }}">
                        {{ Form::label('fitness_prizes_names_monthly', 'Name (en)') }}
                        {{ Form::text('fitness_prizes_names_monthly[en]', isset($fitness_prizes_monthly)?$fitness_prizes_monthly->names['en']:null, array('class' => 'form-control', 'dir'=>'ltr')) }}
                        {!! $errors->first('fitness_prizes_names_monthly.en','<p class="text-danger"><strong>:message</strong></p>') !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('fitness_prizes_names_monthly.ar') ? 'has-error' : '' }}">
                        {{ Form::label('fitness_prizes_names_monthly', 'Name (ar)') }}
                        {{ Form::text('fitness_prizes_names_monthly[ar]', isset($fitness_prizes_monthly)?$fitness_prizes_monthly->names['ar']:null, array('class' => 'form-control', 'dir'=>'rtl')) }}
                        {!! $errors->first('fitness_prizes_names_monthly.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('fitness_prizes_description_monthly.en') ? 'has-error' : '' }}">
                        {{ Form::label('fitness_prizes_description_monthly', 'Description (en)') }}
                        {{ Form::textarea('fitness_prizes_description_monthly[en]', isset($fitness_prizes_monthly)?$fitness_prizes_monthly->descriptions['en']:null, array('class' => 'form-control', 'dir'=>'ltr')) }}
                        {!! $errors->first('fitness_prizes_description_monthly.en','<p class="text-danger"><strong>:message</strong></p>') !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('fitness_prizes_description_monthly.ar') ? 'has-error' : '' }}">
                        {{ Form::label('fitness_prizes_description_monthly', 'Description (ar)') }}
                        {{ Form::textarea('fitness_prizes_description_monthly[ar]', isset($fitness_prizes_monthly)?$fitness_prizes_monthly->descriptions['ar']:null, array('class' => 'form-control', 'dir'=>'rtl')) }}
                        {!! $errors->first('fitness_prizes_description_monthly.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group ">
                        <label for="import_file">Icon (Recommended : 100x100)</label>
                        <input class="form-control" accept="image/png, image/gif, image/jpeg" autocomplete="off" name="monthly_icon" type="file" id="monthly_icon">
                    </div>
                </div>
                @if((isset($fitness_prizes_monthly) && $fitness_prizes_monthly->icon != NULL))
                <div class="col-md-6">
                    <div class="form-group ">
                        <img src="{{$fitness_prizes_monthly->icon}}" class="img-square img-size-30 mt-2 pt-2" width="100"/>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="box box-primary box-solid collapsed-box">
            <div class="box-header" data-widget="collapse">
                <h3 class="box-title">Fitness Season Prizes</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('fitness_prizes_names_season.en') ? 'has-error' : '' }}">
                        {{ Form::label('fitness_prizes_names_season', 'Name (en)') }}
                        {{ Form::text('fitness_prizes_names_season[en]', isset($fitness_prizes_season)?$fitness_prizes_season->names['en']:null, array('class' => 'form-control', 'dir'=>'ltr')) }}
                        {!! $errors->first('fitness_prizes_names_season.en','<p class="text-danger"><strong>:message</strong></p>') !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('fitness_prizes_names_season.ar') ? 'has-error' : '' }}">
                        {{ Form::label('fitness_prizes_names_season', 'Name (ar)') }}
                        {{ Form::text('fitness_prizes_names_season[ar]', isset($fitness_prizes_season)?$fitness_prizes_season->names['ar']:null, array('class' => 'form-control', 'dir'=>'rtl')) }}
                        {!! $errors->first('fitness_prizes_names_season.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('fitness_prizes_description_season.en') ? 'has-error' : '' }}">
                        {{ Form::label('fitness_prizes_description_season', 'Description (en)') }}
                        {{ Form::textarea('fitness_prizes_description_season[en]', isset($fitness_prizes_season)?$fitness_prizes_season->descriptions['en']:null, array('class' => 'form-control', 'dir'=>'ltr')) }}
                        {!! $errors->first('fitness_prizes_description_season.en','<p class="text-danger"><strong>:message</strong></p>') !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('fitness_prizes_description_season.ar') ? 'has-error' : '' }}">
                        {{ Form::label('fitness_prizes_description_season', 'Description (ar)') }}
                        {{ Form::textarea('fitness_prizes_description_season[ar]', isset($fitness_prizes_season)?$fitness_prizes_season->descriptions['ar']:null, array('class' => 'form-control', 'dir'=>'rtl')) }}
                        {!! $errors->first('fitness_prizes_description_season.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group ">
                        <label for="import_file">Icon (Recommended : 100x100)</label>
                        <input class="form-control" accept="image/png, image/gif, image/jpeg" autocomplete="off" name="season_icon" type="file" id="season_icon">
                    </div>
                </div>
                @if((isset($fitness_prizes_season) && $fitness_prizes_season->icon != NULL))
                <div class="col-md-6">
                    <div class="form-group ">
                        <img src="{{$fitness_prizes_season->icon}}" class="img-square img-size-30 mt-2 pt-2" width="100"/>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="form-group m-form__group row">
                {{ Form::label('region', 'Region') }}
                {{ Form::text('searchmap', null, array('class' => 'form-control','placeholder'=>"Search", 'id'=>"searchmap",)) }}
            </div>
        </div>
        <div class="col-md-2"></div>
        <div class="form-group m-form__group row">

            <div class="col-lg-12 col-md-12 col-sm-12">

                <input id="area_cordinates_map" name="area_cordinates_map" value="" type="hidden">

                <input  type=button id="remove_polygon" class="pull-right" value="Remove polygon"><br/>
                {!! $errors->first('area_cordinates_map','<p class="text-danger"><strong>:message</strong></p>') !!}
                <div id="map-canvas"></div>
            </div>
        </div>

    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('program_name.en') ? 'has-error' : '' }}">
            {{ Form::label('program_name', 'Program Name (en)') }}
            {{ Form::text('program_name[en]', isset($fitness_seasons)?$fitness_seasons->program_name['en']:null, array('class' => 'form-control', 'dir'=>'ltr')) }}
            {!! $errors->first('program_name.en','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
        <div class="form-group {{ $errors->has('program_description.en') ? 'has-error' : '' }}">
            {{ Form::label('program_description', 'Program Description (en)') }}
            {{ Form::textarea('program_description[en]', isset($fitness_seasons)?$fitness_seasons->program_description['en']:null, array('class' => 'form-control', 'dir'=>'ltr')) }}
            {!! $errors->first('program_description.en','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('program_name.ar') ? 'has-error' : '' }}">
            {{ Form::label('program_name', 'Program Name (ar)') }}
            {{ Form::text('program_name[ar]', isset($fitness_seasons)?$fitness_seasons->program_name['ar']:null, array('class' => 'form-control', 'dir'=>'rtl')) }}
            {!! $errors->first('program_name.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
        <div class="form-group {{ $errors->has('program_description.ar') ? 'has-error' : '' }}">
            {{ Form::label('program_description', 'Program Description (ar)') }}
            {{ Form::textarea('program_description[ar]', isset($fitness_seasons)?$fitness_seasons->program_description['ar']:null, array('class' => 'form-control', 'dir'=>'rtl')) }}
            {!! $errors->first('program_description.ar','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="form-group ">
                <label for="import_file">Images(English) (Recommended : 750x338)</label>
                <input class="form-control" accept="image/png, image/gif, image/jpeg" multiple autocomplete="off" name="images[en][]" type="file" id="images">
            </div>
        </div>
    </div>
    @if(isset($fitness_seasons))
    <div class="col-md-12">
        <ul class="sortable-images" data-lang="en" style="list-style:none; padding:0;">
            <?php $sImages = $fitness_seasons->seasonImages()->where('language', 'en')->orderBy('index_no')->get(); ?>
            @foreach($sImages as $b)
            <li style="display: inline; margin:15px; float:left;" class="row-en" data-id="{{ $b->id }}">
                <div class="box box-default box-solid">
                    <div class="box-body">
                        <img name="{{$b->id}}" style="height: 150px;width: 150px" src="{{$b->image}}" alt="user-avatar" class="img-fluid">
                    </div>
                    <div class="box-footer">
                        <div class="text-center">
                            <a href="{{url('admin/delete-fitness-season-image/'.$b->id)}}" class="btn btn-danger btn-xs destroy">
                                <i class="fas fa-trash"></i>
                            </a>
                            <a href="#" class="btn btn-primary btn-xs imgType" data-id="{{$b->id}}" data-link="{{$b->url}}">{{$b->type}}</a>
                            <div class="form-check">
                                <input class="form-check-input rdDefault" type="radio" value="{{$b->id}}|{{$b->language}}" name="endefault" @if($b->default) checked @endif>
                                <label class="form-check-label">Default</label>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="col-md-6">
        <div class="form-group">
            <div class="form-group ">
                <label for="import_file">Images(Arabic) (Recommended : 750x338)</label>
                <input class="form-control" accept="image/png, image/gif, image/jpeg" multiple autocomplete="off" name="images[ar][]" type="file" id="images">
            </div>
        </div>
    </div>
    @if(isset($fitness_seasons))
    <div class="col-md-12">
        <ul class="sortable-images" data-lang="ar" style="list-style:none; padding:0;">
            <?php $sImages = $fitness_seasons->seasonImages()->where('language', 'ar')->orderBy('index_no')->get(); ?>
            @foreach($sImages as $b)
            <li style="display: inline; margin:15px; float:left;" class="row-ar" data-id="{{ $b->id }}">
                <div class="box box-default box-solid">
                    <div class="box-body">
                        <img name="{{$b->id}}" style="height: 150px;width: 150px" src="{{$b->image}}" alt="user-avatar" class="img-fluid">
                    </div>
                    <div class="box-footer">
                        <div class="text-center">
                            <a href="{{url('admin/delete-fitness-season-image/'.$b->id)}}" class="btn btn-danger btn-xs destroy">
                                <i class="fas fa-trash"></i>
                            </a>
                            <a href="#" class="btn btn-primary btn-xs imgType" data-id="{{$b->id}}" data-link="{{$b->url}}">{{$b->type}}</a>
                            <div class="form-check">
                                <input class="form-check-input rdDefault" type="radio" value="{{$b->id}}|{{$b->language}}" name="ardefault" @if($b->default) checked @endif>
                                <label class="form-check-label">Default</label>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
    @endif
</div>