@extends('adminlte::page')

@section('title', 'Seasons')

@section('content_header')
<h1><i class="fa fa-tree"></i> Seasons</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3>&nbsp;</h3>
                <div class="box-tools">
                    <a href="{{route('admin.fitness-season.create')}}" class="btn btn-sm btn-primary">Create Season</a>
                </div>

            </div>
            <div class="box-body">


                <div class="table-responsive" style="overflow-x: hidden;">

                    <table class="table table-striped table-bordered table-hover" id="fitness-seasons-table">

                        <thead>
                            <tr>
                                <!--<th>SL.No</th>-->
                                <th>Name</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Daily</th>
                                <th>Monthly</th>
                                <th>Season</th>
                                <th>Participants</th>
                                <th>Is Active</th>
                                <th>Geofence</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section('css')
<style>
    .label{
      float: left;  
    }
    .sec-label{
      margin-top: 2px;
    }
</style>
@stop
@section('js')
<link rel="stylesheet" href="{{ asset('css/custom-checkbox.css') }}">
<script type='text/javascript'>

    $(function () {
        oTable = $('#fitness-seasons-table').DataTable({
            processing: true,
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: false,
            info: true,
            autoWidth: false,
            responsive: true,
            serverSide: true,
            //autoWidth: false,
            ajax: {
                url: "{!! route('admin.fitness-season.datatable') !!}",
                type: 'post',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'}
            },
            columns: [
                /*{
                 data: null,
                 render: function (data, type, row, meta) {
                 return meta.row + meta.settings._iDisplayStart + 1;
                 },
                 searchable: false
                 },*/
                {data: 'names.en', name: 'names->en'},
                {data: 'start_date', name: 'start_date',searcable: false},
                {data: 'end_date', name: 'end_date',searcable: false},
                {data: 'daily_target', name: 'target_steps->daily',searcable: false},
                {data: 'monthly_target', name: 'target_steps->monthly',searcable: false},
                {data: 'season_target', name: 'target_steps->season',searcable: false},
                {data: 'participants_count', name: 'participants_count', searcable: false, className:'text-center'},
                {
                    data: 'is_active',
                    name: 'is_active',
                    orderable: false,
                    searcable: false,
                    className:'text-center',
                    render: function (data, type, row) {
                        if (data == 1) {
                            var text =
                                    '<label class="switch"> <input type="checkbox" checked data-action-type="is_active" value="0" data-id=' +
                                    row.id + '><span class="slider round"></span></label>'
                        } else {
                            var text =
                                    '<label class="switch"> <input type="checkbox" value="1" data-action-type="is_active" data-id=' +
                                    row.id + '><span class="slider round"></span></label>'
                        }
                        return text;
                    }
                },
                {
                    data: 'geofence',
                    name: 'geofence',
                    orderable: false,
                    searcable: false,
                    className:'text-center',
                    render: function (data, type, row) {
                        if (data == 1) {
                            var text =
                                    '<label class="switch"> <input type="checkbox" checked data-action-type="geofence" value="0" data-id=' +
                                    row.id + '><span class="slider round"></span></label>'
                        } else {
                            var text =
                                    '<label class="switch"> <input type="checkbox" value="1" data-action-type="geofence" data-id=' +
                                    row.id + '><span class="slider round"></span></label>'
                        }
                        return text;
                    }
                },
                {data: 'actions', name: 'actions', orderable: false, searchable: false}
            ],
            language: {
                searchPlaceholder: "Name"
            }
        });
        $(document).on('change', 'input[type=checkbox]', function () {

            var value = $(this).val();
            var dataId = $(this).data("id");
            var data_action_type = $(this).attr("data-action-type");
            $.ajax({
                url: '{!! url("admin/fitness-season-status") !!}',
                method: 'POST',
                data: {
                    'dataId': dataId,
                    'dataValue': value,
                    'data_action_type': data_action_type
                }, //POST variable name value
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                success: function (responce)
                {
                    oTable.ajax.reload();
                }
            });
        });

    });
</script>
@stop
