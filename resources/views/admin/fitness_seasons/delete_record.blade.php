@extends('adminlte::page')

@section('title', 'Fitness Daily Tracks')

@section('content_header')
<h1><i class="fa fa-tree"></i> Fitness Daily Tracks</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header ">
                <div class="box-tools pull-right">
                    <a href="{{ route('admin.fitness-track-delete-record') }}" class="btn btn-danger btn-sm delete_record" title="Delete">Delete</a>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            {{ Form::label('sid', 'Season') }}
                            {!! Form::select('sid', $seasons,null, ['class' => 'form-control', 'id' => 'sid']) !!}
                        </div>
                    </div>
                    <div class="input-daterange">
                        <div class="col-md-2">
                            <div class="form-group" >
                                {{ Form::label('start_date', 'Start Date') }}
                                {{ Form::text("start_date", date('m/d/Y'), array('id'=>'start_date',"autocomplete" => "off", 'class' => 'form-control global-dates')) }}
                            </div>
                        </div>
                    </div>
                    <div class="input-daterange">
                        <div class="col-md-2">
                            <div class="form-group" >
                                {{ Form::label('end_date', 'End Date') }}
                                {{ Form::text("end_date", date('m/d/Y'), array('id'=>'end_date',"autocomplete" => "off", 'class' => 'form-control global-dates')) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {{ Form::label('status', 'Status') }}
                            {!! Form::select('status', [0=>'All',1=>'Winner'],null, ['class' => 'form-control', 'id' => 'status']) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="start_date">&nbsp</label>
                            <button class="btn btn-sm btn-primary form-control filter">Filter</button>
                        </div>
                    </div>
                </div>

                <div class="table-responsive" style="overflow-x: hidden;">

                    <table class="table table-striped table-bordered table-hover" id="fitness-seasons-table">
                        <thead>
                            <tr>
                                <th>SL.No</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Steps</th>
                                <th>Coupon Code</th>
                                <th>Day</th>
                                <th>Collected On</th>
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="modal fade in" id="modal-collected">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Mark as Collected</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" value="" name="track_id" id="track_id" />
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('collected_on', 'Date') }}
                            {{ Form::text("collected_on", date('m/d/Y'), array('id'=>'collected_on',"autocomplete" => "off", 'class' => 'global-dates form-control')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary save-track">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@stop
@section('css')
<link href="{{ asset('css/bootstrap-datepicker.css')}}" id="theme" rel="stylesheet">
@stop
@section('js')
<script src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
<script type='text/javascript'>
$(function () {
    $(".global-dates").datepicker({
        autoclose: true
    });
    oTable = $('#fitness-seasons-table').DataTable({
        processing: true,
        paging: true,
        searching: true,
        ordering: false,
        info: true,
        responsive: true,
        serverSide: true,
        autoWidth: false,
        ajax: {
            url: "{!! route('admin.fitness.daily-tracks.dt') !!}",
            type: 'post',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            data: function (d) {
                d.sid = $('#sid').val();
                d.start_date = $('#start_date').val();
                d.end_date = $('#end_date').val();
                d.status = $('#status').val();
            }
        },
        columns: [
            {
                data: null,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
                searchable: false
            },
            {data: 'fname', name: 'fu.fname'},
            {data: 'lname', name: 'fu.lname'},
            {data: 'email', name: 'fu.email'},
            {data: 'steps', name: 'steps', searchable: false},
            {data: 'coupon_code', name: 'coupon_code'},
            {data: 'participated_date', name: 'participated_date', searchable: false},
            {data: 'prize_collected_on', name: 'prize_collected_on', searchable: false}
        ],
        language: {
            searchPlaceholder: "Names,Email,Coupon Code"
        }
    });

    $(document).on("click", "button.filter", function () {
        oTable.ajax.reload();
    });

    $(document).on("click", "a.markCollected", function (e) {
        e.preventDefault();
        $('#track_id').val($(this).data('id'));
        $("#modal-collected").modal('show');
    });

    $("#modal-collected").on("hidden.bs.modal", function () {
        $('#track_id').val('');
    });

    $(document).on("click", "button.save-track", function (e) {
        Pace.restart();
        Pace.track(function () {
            $.ajax({
                url: "{{route('admin.fitness.mark.track','daily')}}",
                dataType: 'json',
                type: 'POST',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                data: {
                    track_id: $('#track_id').val(),
                    collected_on: $('#collected_on').val(),
                },
                success: function (response) {
                    oTable.ajax.reload();
                    $("#modal-collected").modal('hide');
                },
                error: function (response) {
                    swal(response);
                }
            });
        });
    });

    $('.delete_record').click(function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        swal({
            title: "Delete?",
            text: "Are you sure you want to delete records ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                window.location.href = url;
            } else {
                return true;
            }
        });
    });

});
</script>
@stop
