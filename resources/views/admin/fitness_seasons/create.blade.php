@extends('adminlte::page')

@section('title', 'Add Season')

@section('content_header')
<h1> Add Season</h1>
@stop

@section('content')
    <style>
        #map-canvas {
            width: 100%;
            height: 500px;
        }

    </style>
<div class="row">
    <div class="col-md-12 ">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-tools">
                    <a href="{{route('admin.fitness-season.index')}}" class="btn btn-sm btn-default">Back</a>
                </div>
            </div>

            {{ Form::open(array('url' => 'admin/fitness-season','enctype' => 'multipart/form-data')) }}
            <div class="box-body">
                @include("admin.fitness_seasons.form")

            </div>
            <div class="box-footer">
                {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
            </div>
            {{ Form::close() }}

                
        </div>
    </div>
</div>
@stop

@section('js')
    <link href="{{ asset('css/bootstrap-datepicker.css')}}" id="theme" rel="stylesheet">
    <script src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('API_MAP_KEY', 'AIzaSyBpC8Nftwn_ZbBh42oKbCwDgygVi8B271Y') }}&libraries=places,drawing&language=en&callback=initialize" async defer></script>

    <script type='text/javascript'>
        $(function () {
            $("#global_dates").datepicker({
                toggleActive: !0
            });
        });
        var placeSearch, autocomplete, map, _myPolygon;
        function initialize() {
            initMap();
            initAutocomplete();
        }
        function initMap() {
            map = new google.maps.Map(document.getElementById('map-canvas'), {
                center: {
                    lat:25.2048,
                    lng: 55.2708
                },
                zoom: 10
            });
            var drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: google.maps.drawing.OverlayType.PLOYGON,
                drawingControl: true,
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: ['polygon']
                },
                polygonOptions: {
                    draggable: true,
                    editable: true,
                    strokeColor: '#FF0000',
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: '#FF0000',
                    fillOpacity: 0.35
                }
            });
            polyline = google.maps.event.addListener(drawingManager, 'polygoncomplete', function (shape) {
                if (shape == null || (!(shape instanceof google.maps.Polygon)))
                    return;
                var coordinatesArray = shape.getPath().getArray();
                $('#area_cordinates_map').val(JSON.stringify(coordinatesArray));
                drawingManager.setOptions({drawingMode: null, drawingControl: false});
                _myPolygon = shape;
            });
            drawingManager.setMap(map);
            $("#remove_polygon").click(function () {
                $('#area_cordinates_map').val("");
                _myPolygon.setMap(null);
                drawingManager.setOptions({drawingMode: null, drawingControl: true});
            });

            var input = document.getElementById('searchmap');
            google.maps.event.addDomListener(input, 'keydown', function (event) {
                if (event.keyCode === 13) {
                    event.preventDefault();
                }
            });
        }

        function initAutocomplete() {
// Create the autocomplete object, restricting the search to geographical
// location types.
            autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */
                (document.getElementById('searchmap')));
// When the user selects an address from the dropdown, populate the address
// fields in the form.
            autocomplete.addListener('place_changed', fillInAddress);
        }

        function fillInAddress() {
// Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();
            if (!place.geometry) {
// User entered the name of a Place that was not suggested and
// pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            } else {//types
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
        }

        function pan() {
            var panPoint = new google.maps.LatLng(document.getElementById("lat").value, document.getElementById("lng").value);
//map.panTo(panPoint)
            map.setCenter(panPoint);
            map.setZoom(17);
        }
        $(document).on('change', '#images', function () {
$('#image_preview').html('');
var total_file = document.getElementById("images").files.length;
for (var i = 0; i < total_file; i++) {
    var ud_id = Math.floor(Math.random() * 26) + Date.now();
    $('#image_preview').append('<li style="display: inline; margin:15px; float:left;"><div class="box box-default box-solid" id="' + ud_id + '">' +
            '<div class="box-body">' +
            '<img style="height: 150px; width: 150px;" src="' + URL.createObjectURL(event.target.files[i]) + '" alt="user-avatar" class="img-fluid">' +
            '</div>' +
            '<div class="box-footer">' +
            '<div class="text-center">' +
            '<a href="javascript:void(0)" data-ud-id="' + ud_id + '" class="btn btn-danger btn-xs destroy_image">' +
            '<i class="fas fa-trash"></i>' +
            '</a>' +
            '</div>' +
            '</div>' +
            '</div></li>');
}
});
        $(document).on('click', '.destroy_image', function () {
            var dataId = $(this).attr("data-ud-id");
            $('#'+dataId).remove();
        });
      </script>
@stop