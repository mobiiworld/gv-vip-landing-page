<div class="row">
    <div class="col-md-3">
        <div class="form-group {{ $errors->has('hours') ? 'has-error' : '' }}">
            {{ Form::label('hours', 'Hours') }}
            <div class="input-group">
                <span class="input-group-addon minus"><i class="fa fa-minus"></i></span>
                {{ Form::number('hours', isset($product)?null:1, array('class' => 'form-control', 'required'=>"required", "min"=>1, "max"=>10)) }}
                <span class="input-group-addon plus"><i class="fa fa-plus"></i></span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('weekend_price') ? 'has-error' : '' }}">
            {{ Form::label('weekend_price', 'Any Day Price') }}
            {{ Form::text('weekend_price', null, array('class' => 'form-control float-number', 'required'=>"required")) }}
            {!! $errors->first('weekend_price','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('workingday_price') ? 'has-error' : '' }}">
            {{ Form::label('workingday_price', 'Value Price') }}
            {{ Form::text('workingday_price', null, array('class' => 'form-control float-number', 'required'=>"required")) }}
            {!! $errors->first('workingday_price','<p class="text-danger"><strong>:message</strong></p>') !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            {{ Form::checkbox('allow_vip',  1, isset($product)?$product->allow_vip:false,  ['class'=>'allowVip minimal-red'] ) }}
            {{ Form::label('allow_vip', 'Allow VIP') }}
        </div>
    </div>
</div>
<div class="row vipPrices" style="display: none;">
    <div class="col-md-12">
        <div class="box box-primary box-solid">
            <div class="box-header">
                <h3 class="box-title">VIP Prices</h3>
            </div>
            <div class="box-body">
                <div class="col-md-3">
                    <div class="form-group">
                        {{ Form::label('vip_prices[generic_price]', 'Generic Price') }}
                        {{ Form::text('vip_prices[generic_price]', null, array('class' => 'form-control float-number vipPtxt')) }}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {{ Form::label('vip_prices[silver]', 'Silver') }}
                        {{ Form::text('vip_prices[silver]', null, array('class' => 'form-control float-number vipPtxt')) }}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {{ Form::label('vip_prices[gold]', 'Gold') }}
                        {{ Form::text('vip_prices[gold]', null, array('class' => 'form-control float-number vipPtxt')) }}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {{ Form::label('vip_prices[platinum]', 'Platinum') }}
                        {{ Form::text('vip_prices[platinum]', null, array('class' => 'form-control float-number vipPtxt')) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('css')
<link rel="stylesheet" href="{{asset('vendor/adminlte/plugins/iCheck/all.css')}}">
@stop
@section('js')
<script src="{{asset('vendor/adminlte/plugins/iCheck/icheck.min.js')}}"></script>
<script type='text/javascript'>
$(function () {
    $(document).on('click', '.plus', function () {
        $addedValue = parseInt($(this).siblings('input').val()) + 1;
        $(this).siblings('input').val($addedValue);
    });
    $(document).on('click', '.minus', function () {
        var x = parseInt($(this).siblings('input').val());
        if (x > 1) {
            $(this).siblings('input').val(parseInt($(this).siblings('input').val()) - 1);
        }
    });

    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass: 'iradio_minimal-red'
    })
    if ($(".allowVip").is(":checked")) {
        $(".vipPtxt").prop('required', true);
        $(".vipPrices").show()
    } else {
        $(".vipPtxt").prop('required', false);
    }

    $(document).on("ifChanged", ".allowVip", function (e) {
        if ($(this).is(":checked")) {
            $(".vipPtxt").prop('required', true);
            $(".vipPrices").show();
        } else {
            $(".vipPtxt").prop('required', false);
            $(".vipPrices").hide();
        }
        $(".vipPtxt").val('');
    });
});
</script>
@stop


