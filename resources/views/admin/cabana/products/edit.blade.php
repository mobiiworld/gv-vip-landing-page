@extends('adminlte::page')

@section('title', 'Edit Cabana Product')

@section('content_header')
<h1>Edit Cabana Product</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12 ">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-tools">
                    <a href="{{route('admin.cabana-products.index')}}" class="btn btn-sm btn-default">Back</a>
                </div>
            </div>

            {{ Form::model($product,array('url' => route('admin.cabana-products.update',$product->id),'method'=>'put')) }}
            {{ Form::open(array('url' => 'admin/cabana-products')) }}
            <div class="box-body">
                @include("admin.cabana.products.form")
            </div>
            <div class="box-footer">
                {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop