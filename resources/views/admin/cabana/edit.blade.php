@extends('adminlte::page')

@section('title', 'Edit Cabana Day')

@section('content_header')
<h1> Edit Cabana Day</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-tools">
                    <a href="{{route('admin.cabana-days.index')}}" class="btn btn-sm btn-default">Back</a>
                </div>
            </div>


            {{ Form::model($program,array('url' => route('admin.cabana-days.update',$program->id),'method'=>'put')) }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            {{ Form::label('start_dt', 'Start Date') }}
                            {{ Form::text('start_dt', null, array('class' => 'form-control datepicker', 'required'=>"required")) }}
                            {!! $errors->first('start_dt','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('starttime') ? 'has-error' : '' }}">
                            {{ Form::label('start_time', 'Start Time') }}
                            {{ Form::text('start_time', null, array('class' => 'form-control timepicker', 'required'=>"required")) }}
                            {!! $errors->first('start_time','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('endtime') ? 'has-error' : '' }}">
                            {{ Form::label('end_time', 'End Time') }}
                            {{ Form::text('end_time', null, array('class' => 'form-control timepicker','required'=>"required")) }}
                            {{ Form::checkbox('nextDay',  null, null, ['class'=>'nextDay minimal-red'] ) }}
                            {{ Form::label('nextDay', 'Next Day') }}
                            {!! $errors->first('end_time','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop
@section("css")
<link rel="stylesheet" href="{{URL::asset('css/daterangepicker.css')}}">
<link rel="stylesheet" href="{{URL::asset('css/datepicker3.css')}}">
<link rel="stylesheet" href="{{URL::asset('css/daterangepicker-bs3.css')}}">
<link rel="stylesheet" href="{{URL::asset('css/bootstrap-timepicker.min.css')}}">
@stop
@section("js")
<script src="{{URL::asset('js/moment.min.js')}}"></script>
<script src="{{URL::asset('js/daterangepicker.min.js')}}"></script>
<script src="{{URL::asset('js/bootstrap-datepicker.js')}}"></script>
<script src="{{URL::asset('js/bootstrap-timepicker.min.js')}}"></script>
<script type='text/javascript'>
$(function () {
    $('.datepicker').daterangepicker({
        singleDatePicker: true,
        autoApply: true,
        locale: {
            format: 'YYYY-MM-DD'
        }
    });
    $('.timepicker').timepicker({
        timeFormat: 'h:i a',
        showSecond: true,
        showMeridian: true,
        minuteStep: 1,
        ampm: true

    });
});
</script>
@stop