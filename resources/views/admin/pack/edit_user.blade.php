@extends('adminlte::page')

@section('title', 'Edit User Details')

@section('content_header')
<p class="text-bold">Edit User Details</span></p>
@stop

@section('content')
{{ Form::model($user, array('route' => array('admin.packs.saveUser', $userPack->id), 'method' => 'post')) }}
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">User Details</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                            {{ Form::label('first_name', 'First Name') }}
                            {{ Form::text('first_name', null, array('class' => 'form-control','required'=>'required')) }}
                            {!! $errors->first('first_name','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                            {{ Form::label('last_name', 'Last Name') }}
                            {{ Form::text('last_name', null, array('class' => 'form-control','required'=>'required')) }}
                            {!! $errors->first('last_name','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                    @if($user->account_id == NULL)
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            {{ Form::label('email', 'Email') }}
                            {{ Form::email('email', null, array('class' => 'form-control','required'=>'required')) }}
                            {!! $errors->first('email','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                    @endif
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('dob') ? 'has-error' : '' }}">
                            {{ Form::label('dob', 'Date of Birth') }}
                            {{ Form::text('dob', null, array('class' => 'form-control','id'=>'dob','placeholder'=>'DD/MM/YYYY','autocomplete'=>'off','required'=>'required')) }}
                            {!! $errors->first('dob','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                
                    <div class="col-lg-2">
                        <div class="form-group {{ $errors->has('mobile_dial_code') ? 'has-error' : '' }}">
                            {{ Form::label('mobile_dial_code', 'Dial Code') }}
                            {{ Form::select('mobile_dial_code', $dialcodes,null, array('class' => 'form-control','required'=>'required')) }}
                            {!! $errors->first('mobile_dial_code','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
                            {{ Form::label('confirm_email', 'Mobile Number') }}
                            {{ Form::text('mobile', null, array('class' => 'form-control','required'=>'required')) }}
                            {!! $errors->first('mobile','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
                            {{ Form::label('gender', 'Gender') }}
                            {{ Form::select('gender', ['male'=>'Male','female'=>'Female'],null, array('class' => 'form-control','required'=>'required')) }}
                            {!! $errors->first('gender','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('nationality_id') ? 'has-error' : '' }}">
                            {{ Form::label('nationality_id', 'Nationality') }}
                            {{ Form::select('nationality_id', $countries,null, array('class' => 'form-control','required'=>'required')) }}
                            {!! $errors->first('nationality_id','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('residence_id') ? 'has-error' : '' }}">
                            {{ Form::label('residence_id', 'Country of Residence') }}
                            {{ Form::select('residence_id', $countries,null, array('class' => 'form-control','required'=>'required', 'id' => 'cor')) }}
                            {!! $errors->first('residence_id','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                    <div class="col-lg-6" id="emirate_div" @if($uaeId != $user->residence_id ) style="display:none;" @endif>
                        <div class="form-group {{ $errors->has('emirate_id') ? 'has-error' : '' }}">
                            {{ Form::label('emirate_id', 'Emirate of Residence') }}
                            {{ Form::select('emirate_id', $emirate,null, array('class' => 'form-control','id'=>'emirate_id')) }}
                            {!! $errors->first('emirate_id','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
</div>


{{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
{{ Form::close() }}

@stop
@section('js')
<link href="{{ asset('css/bootstrap-datepicker.css')}}" id="theme" rel="stylesheet">
<script src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
<script type='text/javascript'>
$('#dob').datepicker({
    toggleActive: !0,
    uiLibrary: 'bootstrap4',
    format: 'dd-mm-yyyy',
    orientation: "auto bottom",
    maxDate: function () {
        var date = new Date();
        date.setDate(date.getDate() - 3650);
        return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }
});
var uaeId = '{{$uaeId}}';
$(document).on("change","#cor",function(){
    $("#emirate_id").val('');
    $("#emirate_div").hide();
    if($(this).val() == uaeId){
        $("#emirate_div").show();
    }
})
</script>
@stop