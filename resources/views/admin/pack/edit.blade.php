@extends('adminlte::page')

@section('title', 'Edit User Details')

@section('content_header')
<p class="text-bold">VIP Pack number - <span class="text-green">{{$user->pack->pack_number}}</span> | Activation code - <span class="text-green">{{$user->pack->activation_code}}</span></p>
@stop

@section('content')
{{ Form::model($userPack, array('route' => array('admin.packs.update', $userPack->id), 'method' => 'PUT')) }}
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">User Details</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                            {{ Form::label('first_name', 'First Name') }}
                            {{ Form::text('first_name', null, array('class' => 'form-control','required'=>'required')) }}
                            {!! $errors->first('first_name','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                            {{ Form::label('last_name', 'Last Name') }}
                            {{ Form::text('last_name', null, array('class' => 'form-control','required'=>'required')) }}
                            {!! $errors->first('last_name','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            {{ Form::label('email', 'Email') }}
                            {{ Form::email('email', null, array('class' => 'form-control','required'=>'required')) }}
                            {!! $errors->first('email','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('dob') ? 'has-error' : '' }}">
                            {{ Form::label('dob', 'Date of Birth') }}
                            {{ Form::text('dob', null, array('class' => 'form-control','id'=>'dob','placeholder'=>'DD/MM/YYYY','autocomplete'=>'off','required'=>'required')) }}
                            {!! $errors->first('dob','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group {{ $errors->has('mobile_dial_code') ? 'has-error' : '' }}">
                            {{ Form::label('mobile_dial_code', 'Dial Code') }}
                            {{ Form::select('mobile_dial_code', $dialcodes,null, array('class' => 'form-control','required'=>'required')) }}
                            {!! $errors->first('mobile_dial_code','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
                            {{ Form::label('confirm_email', 'Mobile Number') }}
                            {{ Form::text('mobile', null, array('class' => 'form-control','required'=>'required')) }}
                            {!! $errors->first('mobile','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
                            {{ Form::label('gender', 'Gender') }}
                            {{ Form::select('gender', ['male'=>'Male','female'=>'Female'],null, array('class' => 'form-control','required'=>'required')) }}
                            {!! $errors->first('gender','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('nationality_id') ? 'has-error' : '' }}">
                            {{ Form::label('nationality_id', 'Nationality') }}
                            {{ Form::select('nationality_id', $countries,null, array('class' => 'form-control','required'=>'required')) }}
                            {!! $errors->first('nationality_id','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('residence_id') ? 'has-error' : '' }}">
                            {{ Form::label('residence_id', 'Country of Residence') }}
                            {{ Form::select('residence_id', $countries,null, array('class' => 'form-control','required'=>'required')) }}
                            {!! $errors->first('residence_id','<p class="text-danger"><strong>:message</strong></p>') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">User Cars</h3>
            </div>
            <div class="box-body">
                <div id="template">
                    @foreach($user->userCars as $ucar)
                    <?php
                    $emirates = ['' => 'Select'];
                    $pprefixes = ['' => 'Select'];

                    if (!empty($ucar->country_id)) {
                        $emirates = App\Emirate::select('id', "name_en as name")
                                        ->where('country_id', $ucar->country_id)
                                        ->pluck('name', 'id')->prepend('Select');
                        $creq = new \StdClass;
                        if (!empty($ucar->emirate_id)) {
                            $creq->type = 'city';
                            $creq->id = $ucar->emirate_id;
                        } else {
                            $creq->type = 'country';
                            $creq->id = $ucar->country_id;
                        }
                        $pprefixes = ['' => 'Select'] + json_decode(getPprefixes($creq), true);
                    }
                    ?>
                    <div class="row">
                        <input type="hidden" value="{{$ucar->id}}" name="car_id[]">
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('car_country', 'Country of car registration') }}
                                {{ Form::select('car_country[]', $car_countries,$ucar->country_id, array('class' => 'form-control carCountry')) }}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('emirate', 'Emirate of car registration') }}
                                {{ Form::select('emirate[]', $emirates,$ucar->emirate_id, array('class' => 'form-control carEmirate')) }}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('plate_prefix', 'Plate Code') }}
                                {{ Form::select('plate_prefix[]', $pprefixes,$ucar->plate_prefix, array('class' => 'form-control carNumber')) }}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('plate_number', 'Plate Number') }}
                                {{ Form::text('plate_number[]', $ucar->plate_number, array('class' => 'form-control')) }}
                            </div>
                        </div>
                    </div>
                    <hr>
                    @endforeach
                </div>
            </div>
            <div class="box-footer">
                <button type="button" id="AddCar" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add another car</button>
            </div>
        </div>
    </div>
</div>

{{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
{{ Form::close() }}

<div id="hidentemplate" style="display: none;">
    <div class="row">
        <input type="hidden" value="{{$userPack->id}}" name="user_pack_id">
        <input type="hidden" value="new" name="car_id[]">
        <div class="col-lg-6">
            <div class="form-group">
                {{ Form::label('car_country', 'Country of car registration') }}
                {{ Form::select('car_country[]', $car_countries,null, array('class' => 'form-control carCountry')) }}
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                {{ Form::label('emirate', 'Emirate of car registration') }}
                {{ Form::select('emirate[]', ['' => 'Select'],null, array('class' => 'form-control carEmirate')) }}
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                {{ Form::label('plate_prefix', 'Plate Code') }}
                {{ Form::select('plate_prefix[]', ['' => 'Select'],null, array('class' => 'form-control carNumber')) }}
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                {{ Form::label('plate_number', 'Plate Number') }}
                {{ Form::text('plate_number[]', null, array('class' => 'form-control')) }}
            </div>
        </div>
    </div>
    <hr>
</div>
@stop
@section('js')
<link href="{{ asset('css/bootstrap-datepicker.css')}}" id="theme" rel="stylesheet">
<script src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
<script type='text/javascript'>
$('#dob').datepicker({
    toggleActive: !0,
    uiLibrary: 'bootstrap4',
    format: 'dd-mm-yyyy',
    orientation: "auto bottom",
    maxDate: function () {
        var date = new Date();
        date.setDate(date.getDate() - 3650);
        return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }
});
$(document).ready(function () {
    contents = $('#hidentemplate').html();
    $("#AddCar").click(function () {
        $('#template').append(contents);
    });
});
$(document).on('change', '.carCountry', function (e) {
    var countryId = $(this).val();
    var ediv = $(this).parent().parent().parent('div').find('.carEmirate');
    var ndiv = $(this).parent().parent().parent('div').find('.carNumber');
    if (countryId != '') {
        ediv.html('<option value="">Loading....</option>');
        //get Emirates
        $.ajax({
            url: "{!! route('get.emirates',app()->getLocale()) !!}",
            dataType: 'html',
            data: {'id': countryId},
            cache: false,
            method: 'POST',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            beforeSend: function () {
            },
            success: function (response) {
                ediv.html(response);
            },
            error: function (response) {
            },
            complete: function () {
            }
        });
        if (countryId == 1) {
            ndiv.html('<option value="">Select</option>');
        } else {
            ndiv.html('<option value="">Loading....</option>');
            //get plate number Prefix
            listPrefix('country', countryId, ndiv);
        }
    } else {
        ediv.html('<option value="">Select</option>');
        ndiv.html('<option value="">Select</option>');
    }
});
$(document).on('change', '.carEmirate', function (e) {
    var emirateId = $(this).val();
    var ndiv = $(this).parent().parent().parent('div').find('.carNumber');
    if (emirateId != '') {
        ndiv.html('<option value="">Loading....</option>');
        listPrefix('city', emirateId, ndiv);
    } else {
        ndiv.html('<option value="">Select</option>');
    }
});

function listPrefix(type, id, ndiv) {
    $.ajax({
        url: "{!! route('get.platePrefix',app()->getLocale()) !!}",
        dataType: 'html',
        data: {'id': id, 'type': type},
        cache: false,
        method: 'POST',
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
        beforeSend: function () {
        },
        success: function (response) {
            ndiv.html(response);
        },
        error: function (response) {
        },
        complete: function () {
        }
    });
}
</script>
@stop