@extends('adminlte::page')

@section('title', 'Packs')

@section('content_header')
<h1> Packs</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <h3>&nbsp;</h3>
                <div class="box-tools">
                    <?php $user = Auth::guard('admin')->user(); ?>
                    @if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('pack_create', 'admin'))
                    <a href="{{route('admin.import','packs')}}" class="btn btn-sm btn-success">Import Packs</a>
                    @endif
                    <a href="javascript:void(0)" id="export_pack" class="btn btn-sm btn-primary">Export Packs</a>
                    @if(env('ENABLE_DRUPAL_SYNC') == TRUE)
                   <!--  <a href="javascript:void(0)" class="btn btn-sm btn-primary btnSyncDrupalAll">Drupal - Sync all</a> -->
                    @endif

                </div>

            </div>
            <div class="box-body">
                 <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Pack limit</label>
                            <select class="form-control " name="pack_limit" id="pack_limit">
                                <option value="all"  @if($limit == 'all') selected @endif >All</option>
                                <option value="valid" @if($limit == 'valid') selected @endif >Valid</option>
                               
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="info-box">
                          
                            <span class="info-box-text">Total</span>
                            <span class="info-box-number">{{isset($userCounts->total) ? $userCounts->total : 0}}</span>
                         
                        </div>
                    </div>
                    
                    <div class="col-md-2">
                        <div class="info-box">
                         
                            <span class="info-box-text">VGS Synced</span>
                            <span class="info-box-number">{{isset($userCounts->vgs_synced) ? $userCounts->vgs_synced : 0}}</span>
                          
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="info-box">
                          
                            <span class="info-box-text">VGS Not Synced</span>
                            <span class="info-box-number">{{isset($userCounts->vgs_not_synced) ? $userCounts->vgs_not_synced : 0}}</span>
                         
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="info-box">
                         
                            <span class="info-box-text">Designa Synced</span>
                            <span class="info-box-number">{{isset($userCounts->designa_synced) ? $userCounts->designa_synced : 0}}</span>
                          
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="info-box">
                          
                            <span class="info-box-text">Designa Not Synced</span>
                            <span class="info-box-number">{{isset($userCounts->designa_not_synced) ? $userCounts->designa_not_synced : 0}}</span>
                          
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="info-box">
                          
                            <span class="info-box-text">User without cars</span>
                            <span class="info-box-number">{{$userWithoutCar}}</span>
                          
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>VGS Status</label>
                            <select class="form-control filter" name="vgs_status" id="vgs_status">
                                <option value="" >All</option>
                                <option value="synced">Synced</option>
                                <option value="notsynced">Not Synced</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Designa Status</label>
                            <select class="form-control filter" name="designa_status" id="designa_status">
                                <option value="" >All</option>
                                <option value="synced">Synced</option>
                                <option value="notsynced">Not Synced</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Blocked Status</label>
                            <select class="form-control filter" name="blocked_status" id="blocked_status">
                                <option value="" >All</option>
                                <option value="yes">Blocked</option>
                                <option value="no">Unblocked</option>
                            </select>
                        </div>
                    </div>
                  
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="pack-table">

                        <thead>
                            <tr>
                                <th>Pack Number</th>
                                <th>Prefix</th>
                                <th>Activation Code</th>
                                <th>Category</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Actions</th>
                            </tr>
                        </thead>


                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section('js')

<script type='text/javascript'>
    var seasonId = '{{$seasonId}}';
    $(function () {
        $("#pack_limit").on("change",function(){
            var lim = $(this).val();
            var url = '{{route('admin.packs.index')}}';
            window.location.href = url+'?limit='+lim+'&season='+seasonId;
        })
        oTable = $('#pack-table').DataTable({
            processing: true,
            serverSide: true,
            //autoWidth: false,
            ordering: false,
            language: {
                searchPlaceholder: "Pack/Email/Mobile/Name"
            },
            ajax: {
                url: '{!! route('admin.packs.datatable') !!}',
                type: 'post',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                 data: function ( d ) {
                    d.limit = '{{$limit}}';
                    d.seasonId = '{{$seasonId}}';
                    d.vgs_status = $('#vgs_status').val();
                    d.designa_status = $('#designa_status').val();
                    d.blocked_status = $('#blocked_status').val();
                },
            },
           
            columns: [
                {data: 'pack_number', name: 'packs.pack_number'},
                {data: 'prefix', name: 'packs.prefix'},
                {data: 'activation_code', name: 'packs.activation_code'},
                {data: 'category', name: 'packs.category'},
                {data: 'first_name', name: 'users.first_name'},
                {data: 'last_name', name: 'users.last_name'},
                {data: 'email', name: 'users.email'},
                {data: 'mobile', name: 'users.mobile'},
                {data: 'actions', name: 'actions', orderable: false, searchable: false}
            ]
        });
        $(".filter").on("change",function(){
            oTable.draw();
        });

        $(document).on("click", ".btnActivate", function () {
            var id = $(this).data('id');
            $.ajax({
                url: "{!! route('admin.packs.activate') !!}",
                dataType: 'json',
                data: {'id': id},
                cache: false,
                method: 'POST',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                beforeSend: function () {
                },
                success: function (response) {
                    swal("Status", response.message, "success");
                    oTable.draw(false);
                },
                error: function (response) {
                },
                complete: function () {
                    oTable.draw(false);
                }
            });
        });

        $(document).on("click", ".btnSyncDesigna", function () {
            var id = $(this).data('id');
            var thisBtn = $(this);
            $.ajax({
                url: "{!! route('admin.packs.sync.designa') !!}",
                dataType: 'json',
                data: {'id': id},
                cache: false,
                method: 'POST',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                beforeSend: function () {
                    thisBtn.attr('disabled', true);
                },
                success: function (response) {
                    if (response.status) {
                        var ereLog = JSON.stringify(response.responseLog);
                        if (ereLog.indexOf('request') !== -1) {
                            swal("Sync failed", ereLog, "warning");
                        } else {
                            swal("Success", "Synced", "success");
                            oTable.draw(false);
                        }
                    } else {
                        swal("Sync failed", response.message, "error");
                    }
                },
                error: function (response) {
                    swal("Sync failed", 'Something went wrong!', "error");
                },
                complete: function () {
                    thisBtn.attr('disabled', false);
                }
            });
        });

        $(document).on("click", ".btnSyncDrupal", function () {
            var id = $(this).data('id');
            var thisBtn = $(this);
            $.ajax({
                url: "{!! route('admin.packs.syncDrupal') !!}",
                dataType: 'json',
                data: {'id': id},
                cache: false,
                method: 'POST',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                beforeSend: function () {
                    thisBtn.attr('disabled', true);
                },
                success: function (response) {
                    if (response.status) {
                        
                        
                            swal("Success", response.message, "success");
                            oTable.draw(false);
                       
                    } else {
                        swal("Sync failed", response.message, "error");
                    }
                },
                error: function (response) {
                    swal("Sync failed", 'Something went wrong!', "error");
                },
                complete: function () {
                    thisBtn.attr('disabled', false);
                }
            });
        });

        $(document).on("click", ".btnSyncDrupalAll", function () {
            var id = $(this).data('id');
            var thisBtn = $(this);
            $.ajax({
                url: "{!! route('admin.packs.syncAllDrupal') !!}",
                dataType: 'json',
                data: {'id': id},
                cache: false,
                method: 'POST',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                beforeSend: function () {
                    thisBtn.attr('disabled', true);
                },
                success: function (response) {
                    if (response.status) {
                        
                        
                            swal("Success", "Synced", "success");
                            oTable.draw(false);
                       
                    } else {
                        swal("Sync failed", response.message, "error");
                    }
                },
                error: function (response) {
                    swal("Sync failed", 'Something went wrong!', "error");
                },
                complete: function () {
                    thisBtn.attr('disabled', false);
                }
            });
        });

        $(document).on("click", "#export_pack", function(){
            var vgs_status = $('#vgs_status').val();
            var designa_status = $('#designa_status').val();
            var url = '{{route("admin.packs.export")}}';
            var limit = '{{$limit}}';
            var season = '{{$seasonId}}';
            url += '?vgs_status='+vgs_status+'&designa_status='+designa_status+'&limit='+limit+'&season='+season;
            window.location.href = url;

        });

    });
</script>    
@stop