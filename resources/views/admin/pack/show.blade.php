@extends('adminlte::page')

@section('title', 'Packs')

@section('content_header')
<h1> Packs</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="box-tools">
  <?php $user = Auth::guard('admin')->user(); ?>                  
                    @if($pack->upId != NULL && ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('pack_update', 'admin')))
                    <a href="{{route('admin.packs.editUser',$pack->upId)}}" class="btn btn-xs btn-primary">Edit User</a>&nbsp;
                   
                        @if($pack->blocked_status == 'yes' )
                            <a href="{{route('admin.packs.blockUser',$pack->upId)}}" class="btn btn-xs btn-warning btnRBlock">Unblock User</a>&nbsp;
                        @else
                            <a href="{{route('admin.packs.blockUser',$pack->upId)}}" class="btn btn-xs btn-warning btnRBlock">Block </a>&nbsp;
                        @endif
                         <a href="{{route('admin.packs.removeUser',$pack->upId)}}" class="btn btn-xs btn-danger btnRelease">Remove User from pack</a>&nbsp;
                    @endif
                    
                    <a href="{{route('admin.packs.index')}}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                </div>
            </div>
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="pack-table">

                        <tr>
                            <th>Pack Number</th>
                            <td>{{$pack->pack_number}}</td>
                        </tr>
                        <tr>
                            <th>Sale Id</th>
                            <td>{{$pack->sale_id}}</td>
                        </tr>
                        <tr>
                            <th>User Details</th>
                            <td>
                                {{$pack->first_name}} {{$pack->last_name}}
                                @if($pack->email != NULL)
                                <br>
                                {{$pack->email}}
                                @endif
                                @if($pack->mobile != NULL)
                                <br>
                                {{$pack->mobile_dial_code}}  {{$pack->mobile}}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Account Id</th>
                            <td>{{$pack->account_id}}</td>
                        </tr>
                        <tr>
                            <th>Designa Customer Uid</th>
                            <td>{{$pack->designa_customer_uid}}</td>
                        </tr>
                        <tr>
                            <th>Designa Customer Id</th>
                            <td>{{$pack->designa_customer_id}}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">User Cars</h3>
                <div class="box-tools">
                    @if($pack->car_activation_allowed > count($userCars) && $pack->upId != NULL && $pack->blocked_status == 'no'  && ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('pack_update', 'admin')))
                        <a href="{{route('admin.user-cars.create')}}?packId={{$pack->pId}}" class="btn btn-xs btn-primary">Create Car</a>
                    @endif
                    
                </div>
            </div>
            <div class="box-body">
                @foreach($userCars  as $cars)
                <div class="col-md-6">
                    <div class="box box-warning box-solid">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-6 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header">Plate Prefix</h5>
                                        <span class="description-text">{{$cars->plate_prefix}}</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-6 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header">Plate Numer</h5>
                                        <span class="description-text">{{$cars->plate_number}}</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <div class="row">
                                <div class="col-sm-6 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header">Country</h5>
                                        <span class="description-text">{{(isset($cars->country->name_en)) ? $cars->country->name_en : ''}}</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-6 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header">Emirate</h5>
                                        <span class="description-text">{{(isset($cars->emirate->name_en)) ? $cars->emirate->name_en : '---'}}</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th>Designa Card  Uid</th>
                                                    <td>{{$cars->designa_card_uid}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Designa Card Id</th>
                                                    <td>{{$cars->designa_card_id}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Designa Park Id:</th>
                                                    <td>{{$cars->designa_park_id}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                           @if($pack->blocked_status == 'no'  && ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('pack_update', 'admin')))
                            <a href="{{route('admin.user-cars.edit',$cars->id)}}?packId={{$pack->pId}}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-edit"></i></a>
                        @endif
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@if(count($blockHistory) > 0)
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Block History</h3>
                
            </div>
            <div class="box-body">
               <table class="table">
                <thead>
                    <th>Status</th>
                       <th>Date</th>
                       <th>Reference</th>
                       <th>Admin</th>
                     <!--   <th>API Status</th> -->
                </thead>
                   <tbody>
                       @foreach($blockHistory as $block)
                            <tr>
                                <td>{{ ucfirst($block->status)}}</td>
                                <td>{{ date('d M Y, h:i A',strtotime($block->created_at))}}</td>
                                <td>{{ $block->reference_number}}</td>
                                <td>{{ isset($adminList[$block->blocked_by]) ? $adminList[$block->blocked_by]->name : ''}}</td>
                                {{--
                                <td>{{ $block->vgs_status}}</td>
                                 --}}
                            </tr>
                       @endforeach
                   </tbody>
               </table>
            </div>
        </div>
    </div>
</div>
@endif
@stop

@section("js")
<script type="text/javascript">
    $(document).ready(function(){
        $(".btnRelease").on("click", function(event){
            event.preventDefault();
            var url = $(this).attr('href');
            swal({
                title: "Are you sure?",
                text: "Once the user is removed from from the pack, you will not be able to reassign!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                  $.ajax({
                        url: url,
                        dataType: 'json',
                       
                        cache: false,
                        method: 'GET',
                        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                        beforeSend: function () {
                           
                        },
                        success: function (response) {
                            if (response.status) {
                                    swal("Success", response.message, "success");
                                    location.reload();                                    
                               
                            } else {
                                swal("Failed", response.message, "error");
                            }
                        },
                        error: function (response) {
                            swal("Failed", 'Something went wrong!', "error");
                        },
                        complete: function () {
                            
                        }
                    });
                } else {
                    return true;
                }
            });
        });
        
        $(".btnRBlock").on("click", function(event){
            event.preventDefault();
            var url = $(this).attr('href');
            var elem = $(this);
            $(elem).attr('disabled',true);
            swal({
                title: "Are you sure?",
               // text: "Once the user is removed from from the pack, you will not be able to reassign!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Enter Reference Number", {
                      content: "input",
                    })
                    .then((value) => {
                      url = url+'?reference='+value;   
                          $.ajax({
                            url: url,
                            dataType: 'json',
                           
                            cache: false,
                            method: 'GET',
                            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                            beforeSend: function () {
                               
                            },
                            success: function (response) {
                                if (response.status) {
                                        swal("Success", response.message, "success").then((value)=>{
                                            location.reload(); 
                                        });
                                        //                                   
                                   
                                } else {
                                    swal("Failed", response.message, "error");
                                }
                                $(elem).attr('disabled',false);
                            },
                            error: function (response) {
                                swal("Failed", 'Something went wrong!', "error");
                                $(elem).attr('disabled',false);
                            },
                            complete: function () {
                                $(elem).attr('disabled',false);
                            }
                        });                     
                    });
                  
                } else {
                    $(elem).attr('disabled',false);
                    return true;
                }
            });
        });
    });
</script>
@stop