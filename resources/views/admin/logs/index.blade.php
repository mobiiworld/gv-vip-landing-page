@extends('adminlte::page')

@section('title', 'Logs')

@section('content_header')
    <h1>Logs</h1>
@stop

@section('content')
    <div class="row">

        <div class="col-md-12">
            

            <div class="box box-success">

                <div class="box-body">
                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Audit Type</label>
                                <select class="form-control" name="audit_type" id="audit_type">
                                    <option value="" >All</option>
                                    <option value="create-car">Create Car</option>
                                    <option value="edit-car">Edit Car</option>
                                    <option value="edit-user">Edit user</option>
                                   
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="table-responsive">

                        <table class="table table-striped table-bordered table-hover" id="audit-log-table">

                            <thead>
                            <tr>
                                <th></th>
                                <th >Modified By</th>
                                <th >Event</th>
                              <!--   <th width="35%">Old Values</th>
                                <th width="35%">New Values</th> -->
                                <th >Time</th>
                            </tr>
                            </thead>


                        </table>
                    </div>

                </div>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop
@section('js')
    <script type='text/javascript'>
        $(function () {
            
            url_link='{{route("admin.getAllLogs")}}';
            oTable = $('#audit-log-table').DataTable({
                processing: true,
                searching: false,
                ordering: false,
                serverSide: true,
                autoWidth: true,
                ajax: {
                    url: url_link,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
                    data: function ( d ) {
                        if( $('#audit_type').val() != "") {
                            d.audit_type = $('#audit_type').val();
                        }
                       

                    }
                },
                columns: [
                    {
                        "className":      'details-control',
                        "orderable":      false,
                        "sortable":      false,
                        "data":           null,
                        "defaultContent": '<i class="fa fa-plus-circle" aria-hidden="true"></i>'
                    },
                    {data: 'admin_id', name: 'admin_id'},
                    {data: 'event', name: 'event'},
                   // {data: 'old_values', name: 'old_values'},
                   // {data: 'new_values', name: 'new_values'},
                    {data: 'created_at', name: 'created_at'}
                ]
            });

            $(document).on("change", "#audit_type", function() {
                oTable.ajax.reload();
            });

            $('#audit-log-table').on('click', 'td.details-control', function () {
                    $(this).find('i').toggleClass('fa-plus-circle fa-minus-circle');
                    var tr = $(this).closest('tr');
                    var row = oTable.row( tr );
               
                    if ( row.child.isShown() ) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        row.child( format(row.data()) ).show();
                        tr.addClass('shown');
                    }
                } );
        });

function format ( d ) {
    // `d` is the original data object for the row
    return '<table border="0" >'+
        '<tr>'+
            '<th colspan="2">Old Value</th>'+
            '<th colspan="2">New Value</th>'+
            
        '</tr>'+
        '<tr>'+
            '<td colspan="2">'+d.old_values+'</td>'+
            '<td colspan="2">'+d.new_values+'</td>'+
        '</tr>'+
        
    '</table>';
}
    </script>
@stop
@section("css")
<style type="text/css">
td.details-control {
   
    cursor: pointer;
}
</style>
@stop