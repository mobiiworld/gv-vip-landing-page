@extends('adminlte::page')

@section('title', 'Admin Users')

@section('content_header')
    <h1><i class="fa fa-shopping-cart"></i> Shopping Cart Transaction - Finance/Admin</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header "><div id="table_buttons" class="box-tools"></div></div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Transaction Status</label>
                                <select class="form-control" name="transaction_status" id="transaction_status">
                                    <option value="" >All</option>
                                    <option value="active">Active</option>
                                    <option value="inactive">Inactive</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Payment Status</label>
                                <select class="form-control" name="payment_status" id="payment_status">
                                    <option value="" >All</option>
                                    <option value="1">Created</option>
                                    <option value="2">Initiated</option>
                                    <option value="3">Authenticate</option>
                                    <option value="4">Paid</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Platform</label>
                                <select class="form-control" name="plate_form" id="plate_form">
                                    <option value="" >All</option>
                                    <option value="web">Web</option>
                                    <option value="android">Android</option>
                                    <option value="ios">Ios</option>
                                    <option value="applepay">Apple pay</option>
                                    <option value="googlepay">Google pay</option>
                                </select>

                            </div>
                        </div>
                        <div class="input-daterange" id="global_dates">
                            <div class="col-md-2">
                                <div class="form-group" >
                                    {{ Form::label('longt', 'Start Date') }}
                                    {{ Form::text("start_date", date('m/d/Y',strtotime("-1 days")), array('placeholder'=>'Start Date','id'=>'start_date',"autocomplete" => "off", 'class' => 'start_end_date form-control')) }}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group" >
                                    {{ Form::label('longt', 'End Date') }}
                                    {{ Form::text("end_date", date('m/d/Y'), array('placeholder'=>'End Date','id'=>'end_date',"autocomplete" => "off", 'class' => 'start_end_date form-control')) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('Email', 'Email') }}
                                {{ Form::text("email", null, array('placeholder'=>'Email','id'=>'email',"autocomplete" => "off", 'class' => 'form-control')) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('Order Id', 'Order Id') }}
                                {{ Form::text("salecode", null, array('placeholder'=>'Order Id','id'=>'salecode',"autocomplete" => "off", 'class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('Reference Number', 'Reference Number') }}
                                {{ Form::text("order_ref_no", null, array('placeholder'=>'Reference Number','id'=>'order_ref_no',"autocomplete" => "off", 'class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('First Name', 'First Name') }}
                                {{ Form::text("firstname", null, array('placeholder'=>'First Name','id'=>'firstname',"autocomplete" => "off", 'class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('Last Name', 'Last Name') }}
                                {{ Form::text("lastname", null, array('placeholder'=>'Last Name','id'=>'lastname',"autocomplete" => "off", 'class' => 'form-control')) }}
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {{ Form::label('Mobile Number', 'Mobile Number') }}
                                {{ Form::text("mobile", null, array('placeholder'=>'Mobile Number','id'=>'mobile',"autocomplete" => "off", 'class' => 'form-control')) }}
                            </div>
                        </div>
                        {{--  <div class="col-md-2">
                            <div class="form-group">
                                <label for="" style="width: 100%;height: 20px;"></label>
                                <button class="btn btn-primary" id="exportBtn">Export</button>
                            </div>

                        </div>  --}}
                    </div>

                    <div class="table-responsive">

                        <table class="table table-striped table-bordered table-hover" id="shop-cart-transaction-table">

                            <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Order Ref No.</th>
                                <th>Amount</th>
                                <th>Email</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Phone Number</th>
                                <th>Date/Time Added</th>
                                <th>Transaction Status</th>
                                <th>Payment Status</th>
                                <th class="noExport">Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
<script type="text/javascript" src="{{asset('vendor/datatables-plugins/buttons/js/dataTables.buttons.min.js')}}"></script>
<script type="text/javascript" src="{{asset('vendor/datatables-plugins/buttons/js/buttons.bootstrap4.min.js')}}"></script>

<script src="{{asset('vendor/datatables-plugins/jszip/jszip.min.js')}}"></script>
<script src="{{asset('vendor/datatables-plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('vendor/datatables-plugins/pdfmake/vfs_fonts.js')}}"></script>

<script type="text/javascript" src="{{asset('vendor/datatables-plugins/buttons/js/buttons.html5.min.js')}}"></script>
<script type="text/javascript" src="{{asset('vendor/datatables-plugins/buttons/js/buttons.print.min.js')}}"></script>
<script type="text/javascript" src="{{asset('vendor/datatables-plugins/buttons/js/buttons.colVis.min.js')}}"></script>

    <link href="{{ asset('css/bootstrap-datepicker.css')}}" id="theme" rel="stylesheet">
    <script src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
    <script type='text/javascript'>
        function getcurrentDate(){
            var d = new Date();
            var datestring = d.getDate() +''+ (d.getMonth()+1) + ''+ d.getFullYear() + "_" +d.getHours()  +''+  d.getMinutes();
            return datestring;
        }
        var dt;
        $(function () {
            $("#global_dates").datepicker({
                toggleActive: !0
            });
        });
        $(function () {
            dt = $('#shop-cart-transaction-table').DataTable({
                processing: true,
                serverSide: true,
                columnDefs: [
                    { "width": "150px", "targets": 9 }
                ],
                //autoWidth: false,
                ajax: {
                    url: '{!! route('admin.shop-cart-transaction.datatable') !!}',
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    data: function ( d ) {
                        if( $('#payment_status').val() != "") {
                            d.payment_status = $('#payment_status').val();
                        }
                        if( $('#transaction_status').val() != "") {
                            d.transaction_status = $('#transaction_status').val();
                        }
                        if( $('#start_date').val() != "") {
                            d.start_date = $('#start_date').val();
                        }
                        if( $('#end_date').val() != "") {
                            d.end_date = $('#end_date').val();
                        }
                        if( $('#salecode').val() != "") {
                            d.salecode = $('#salecode').val();
                        }
                        if( $('#order_ref_no').val() != "") {
                            d.order_ref_no = $('#order_ref_no').val();
                        }
                        if( $('#plate_form').val() != "") {
                            d.plate_form = $('#plate_form').val();
                        }
                        if( $('#firstname').val() != "") {
                            d.firstname = $('#firstname').val();
                        }
                        if( $('#lastname').val() != "") {
                            d.lastname = $('#lastname').val();
                        }
                        if( $('#email').val() != "") {
                            d.email = $('#email').val();
                        }
                        if( $('#mobile').val() != "") {
                            d.mobile = $('#mobile').val();
                        }


                    }
                },
                columns: [
                    {data: 'salecode', name: 'gv_shopcart_transaction.salecode'},
                    {data: 'order_id', name: 'gv_shopcart_transaction.order_id'},
                    {data: 'totalAmount', name: 'gv_shopcart_transaction.totalAmount'},
                    {data: 'email', name: 'gv_shopcart_transaction.email'},
                    {data: 'firstname', name: 'gv_shopcart_transaction.firstname'},
                    {data: 'lastname', name: 'gv_shopcart_transaction.lastname'},
                    {data: 'mobile', name: 'gv_shopcart_transaction.mobile'},
                    {data: 'created_date', name: 'gv_shopcart_transaction.created_date', orderable: false, searchable: false},
                    {data: 'transaction_status', name: 'transaction_status', orderable: false, searchable: false},
                    {data: 'payment_status', name: 'payment_status', orderable: false, searchable: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                dom: 'Bfrtip',               
                buttons: [ 
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ['thead th:not(.noExport)']
                        },
                        "action": newexportaction,
                        orientation: 'landscape',
                        title : 'Transactions',
                        className : 'btn-primary',
                        pageSize: 'LEGAL'
                    },                    
                    {
                        extend: 'excelHtml5',                   
                        action: newexportaction,    
                        exportOptions: {
                            columns: "thead th:not(.noExport)"
                        },                
                        title  : 'Transactions',                    
                        //text  : 'Export',                    
                        createEmptyCells : true,
                        className : 'btn-primary'                        
                    }
                ],

                initComplete: function(settings, json) {
                    
                    dt.buttons().container().appendTo('#table_buttons')
                }
            });
            $(document).on("change", "#payment_status, #start_date, #end_date, #transaction_status, #plate_form", function() {
                dt.ajax.reload();
            });
            $(document).on("keyup", "#salecode, #order_ref_no, #mobile, #firstname, #lastname, #email", function() {
                dt.ajax.reload();
            });

            function newexportaction(e, dt, button, config) {
                var self = this;
                var oldStart = dt.settings()[0]._iDisplayStart;
                dt.one('preXhr', function (e, s, data) {
                    // Just this once, load all data from the server...
                    data.start = 0;
                    data.length = '{{$total}}';
                    dt.one('preDraw', function (e, settings) {
                        // Call the original action function
                        if (button[0].className.indexOf('buttons-copy') >= 0) {
                            $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
                        } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                            $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                                $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
                                $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
                        } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                            $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                                $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
                                $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
                        } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                            $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                                $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
                                $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
                        } else if (button[0].className.indexOf('buttons-print') >= 0) {
                            $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
                        }
                        dt.one('preXhr', function (e, s, data) {
                            // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                            // Set the property to what it was before exporting.
                            settings._iDisplayStart = oldStart;
                            data.start = oldStart;
                        });
                        // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                        setTimeout(dt.ajax.reload, 0);
                        // Prevent rendering of the full data to the DOM
                        return false;
                    });
                });

                // Requery the server with the new one-time export settings
                dt.ajax.reload();
            }

        });




    </script>
@stop
