@extends('adminlte::page')

@section('title', 'Show Transaction Detail')


@section('content_header')
    <h1>Show Transaction Detail
    <span class="pull-right">
      <a href="javascript:void(0)" style="width:170px;" data-popup-action-type = "check-payment-status" data-popup-action-id = "{{$result['id']}}" style="cursor: pointer;" class="open-popup-content btn btn-warning btn-xs " title="Check Payment Status"><i class="fa fa-reply" aria-hidden="true"></i> Check Payment status</a>

    @if( (!empty($result['cart_detail']->getCheckoutDetail->status)) && ($result['cart_detail']->getCheckoutDetail->status == 1) )
      <a title="Send Mail" href="javascript:void(0)" style="width:105px;"  class="open-popup-content btn btn-primary btn-xs " data-popup-action-type = "send-mail" data-popup-action-id = "{{$result['id']}}"><i class="fa fa-envelope-o"></i> Send Email</a>
    @else
      @if( ( (!empty($result['cart_detail']->getSessionDetail->status)) && ($result['cart_detail']->getSessionDetail->status == 4) ) &&
      ( !empty($result['cart_detail']->status) && ($result['cart_detail']->status == 1)  ))
        <a title="Generate Ticket" href="javascript:void(0)" style="width:95px;"  class="open-popup-content btn btn-success btn-xs " data-popup-action-type = "generate-ticket" data-popup-action-id = "{{$result['id']}}"><i class="fa fa-ticket"></i> Generate Ticket</a>
      @endif
    @endif


    </span>
</h1>
@stop

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#cart_transaction" data-toggle="tab">Shopping Cart Transaction Detail</a></li>
                    <li><a href="#user_session" data-toggle="tab">Session Payment Detail</a></li>
                    <li><a href="#checkout_transaction" data-toggle="tab">Checkout Transaction Detail</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="cart_transaction">
                        <div class="box-body">
                            <div class="row">
                            <div class="form-group col-md-4">
                                <label for="exampleInputEmail1">Shop Cart Id</label><br>
                                <span>@if($result['cart_detail']){{$result['cart_detail']->shopcartid}}@endif</span>
                            </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Account Id</label><br>
                                    <span>@if($result['cart_detail']){{$result['cart_detail']->accountid}}@endif</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">User Id</label><br>
                                    <span>@if($result['cart_detail']){{$result['cart_detail']->userid}}@endif</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Email</label><br>
                                    <span>@if($result['cart_detail']){{$result['cart_detail']->email}}@endif</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Date/Time Added</label><br>
                                    <span>@if($result['cart_detail'] && !empty($result['cart_detail']->created_date)){{date('F d, Y h:ia',strtotime($result['cart_detail']->created_date))}}@endif</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Transaction Status</label><br>
                                    <span>@if($result['cart_detail']){{($result['cart_detail']->status == 1)?'Active':'Inactive'}}@endif</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">First Name</label><br>
                                    <span>@if($result['cart_detail']){{$result['cart_detail']->firstname}}@endif</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Last Name</label><br>
                                    <span>@if($result['cart_detail']){{$result['cart_detail']->lastname}}@endif</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Phone Number</label><br>
                                    <span>@if($result['cart_detail']){{$result['cart_detail']->mobile}}@endif</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="user_session">
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Platform</label><br>
                                    <span>@if($result['cart_detail'] && $result['cart_detail']->getSessionDetail){{$result['cart_detail']->getSessionDetail->platform}}@endif</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">TDS</label><br>
                                    <span>@if($result['cart_detail'] && $result['cart_detail']->getSessionDetail){{$result['cart_detail']->getSessionDetail->tds}}@endif</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Payment Category</label><br>
                                    <span>@if($result['cart_detail'] && $result['cart_detail']->getSessionDetail){{$result['cart_detail']->getSessionDetail->payment_category}}@endif</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Amount</label><br>
                                    <span>@if($result['cart_detail'] && $result['cart_detail']->getSessionDetail){{$result['cart_detail']->getSessionDetail->amount}}@endif</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Date/Time Added</label><br>
                                    <span>@if($result['cart_detail'] && $result['cart_detail']->getSessionDetail && !empty($result['cart_detail']->getSessionDetail->created_date)){{date('F d, Y h:ia',strtotime($result['cart_detail']->getSessionDetail->created_date))}}}@endif</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Payment Status</label><br>
                                    <span>@if($result['cart_detail'])
                                            @if($result['cart_detail']->getSessionDetail)
                                                @if($result['cart_detail']->getSessionDetail->status == 1)
                                                    Created
                                                @elseif($result['cart_detail']->getSessionDetail->status == 2)
                                                    Initiated
                                                @elseif($result['cart_detail']->getSessionDetail->status == 3)
                                                    Authenticate
                                                @elseif($result['cart_detail']->getSessionDetail->status == 4)
                                                    Paid
                                                    @else
                                                    Not Paid
                                                    @endif
                                            @else
                                                Not Paid
                                            @endif

                                      @endif
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="checkout_transaction">
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Sale Code</label><br>
                                    <span>@if($result['cart_detail'] && $result['cart_detail']->getCheckoutDetail){{$result['cart_detail']->getCheckoutDetail->salecode}}@endif</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Sale Id</label><br>
                                    <span>@if($result['cart_detail'] && $result['cart_detail']->getCheckoutDetail){{$result['cart_detail']->getCheckoutDetail->saleid}}@endif</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Transaction Id</label><br>
                                    <span>@if($result['cart_detail'] && $result['cart_detail']->getCheckoutDetail){{$result['cart_detail']->getCheckoutDetail->transactionid}}@endif</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Total Amount</label><br>
                                    <span>@if($result['cart_detail'] && $result['cart_detail']->getCheckoutDetail){{$result['cart_detail']->getCheckoutDetail->totalamount}}@endif</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Total Tax</label><br>
                                    <span>@if($result['cart_detail'] && $result['cart_detail']->getCheckoutDetail){{$result['cart_detail']->getCheckoutDetail->totaltax}}@endif</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Description</label><br>
                                    <span>@if($result['cart_detail'] && $result['cart_detail']->getCheckoutDetail){{$result['cart_detail']->getCheckoutDetail->description}}@endif</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Date/Time Added</label><br>
                                    <span>@if($result['cart_detail'] && $result['cart_detail']->getCheckoutDetail && !empty($result['cart_detail']->getCheckoutDetail->created_date)){{date('F d, Y h:ia',strtotime($result['cart_detail']->getCheckoutDetail->created_date))}}}@endif</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Checkout Status</label><br>
                                    <span>@if($result['cart_detail'] && $result['cart_detail']->getCheckoutDetail){{($result['cart_detail']->getCheckoutDetail->status == 1)?'Completed':'Inactive'}}@endif</span>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Send Email</label><br>
                                    <span>@if($result['cart_detail'] && $result['cart_detail']->getCheckoutDetail){{($result['cart_detail']->getCheckoutDetail->status == 1)?'Active':'Inactive'}}@endif</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->

            <div class="box box-success">

                <div class="box-body">
                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Audit Type</label>
                                <select class="form-control" name="audit_type" id="audit_type">
                                    <option value="" >All</option>
                                    <option value="generate-ticket">Generate Ticket</option>
                                    <option value="check-payment-status">Check Payment Status</option>
                                    <option value="send-mail">Send Mail</option>
                                    <option value="view-transaction">View Transaction</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="table-responsive">

                        <table class="table table-striped table-bordered table-hover" id="audit-log-table">

                            <thead>
                            <tr>
                                <th>Modified By</th>
                                <th>Event</th>
                                <th>Time</th>
                            </tr>
                            </thead>


                        </table>
                    </div>

                </div>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop
@section('js')
    <script type='text/javascript'>
        $(function () {
            transaction_id = '{{$result['id']}}';
            url_link='{{url('admin/show-audit-log/transaction/')}}';
            oTable = $('#audit-log-table').DataTable({
                processing: true,
                serverSide: true,
                //autoWidth: false,
                ajax: {
                    url: url_link,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
                    data: function ( d ) {
                        if( $('#audit_type').val() != "") {
                            d.audit_type = $('#audit_type').val();
                        }
                        d.transaction_id = transaction_id;

                    }
                },
                columns: [

                    {data: 'admin_id', name: 'admin_id'},
                    {data: 'event', name: 'event'},
                    {data: 'created_at', name: 'created_at'}
                ]
            });

            $(document).on("change", "#audit_type", function() {
                oTable.ajax.reload();
            });
        });
    </script>
@stop
