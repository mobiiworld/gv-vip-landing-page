@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('adminlte.skin', 'blue') . '.min.css')}} ">
    @stack('css')
    @yield('css')
@stop

@section('body_class', 'skin-' . config('adminlte.skin', 'blue') . ' sidebar-mini ' . (config('adminlte.layout') ? [
    'boxed' => 'layout-boxed',
    'fixed' => 'fixed',
    'top-nav' => 'layout-top-nav'
][config('adminlte.layout')] : '') . (config('adminlte.collapse_sidebar') ? ' sidebar-collapse ' : ''))

@section('body')
    <div class="wrapper">

        <!-- Main Header -->
        <header class="main-header">
            @if(config('adminlte.layout') == 'top-nav')
            <nav class="navbar navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="navbar-brand">
                            {!! config('adminlte.logo', '<b>Admin</b>LTE') !!}
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            @each('adminlte::partials.menu-item-top-nav', $adminlte->menu(), 'item')
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
            @else
            <!-- Logo -->
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><img src="{{asset('images/logo-small.png')}}" alt="Logo" class="brand-image img-circle elevation-3"></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><img src="{{asset('images/logo.svg')}}" alt="Logo"></span>
            </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle fa5" data-toggle="push-menu" role="button">
                    <span class="sr-only">{{ trans('adminlte::adminlte.toggle_navigation') }}</span>
                </a>
            @endif
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">

                    <ul class="nav navbar-nav">
                        <li class="user user-menu">
                                <?php $avUser = Auth::guard('admin')->user(); ?>  
                                @if($avUser->type == 'admin')  
                                <a href="{{ route('admin.user.profile') }}">
                                    <img src="{{ $avUser->avatar_url }}" class="user-image avatar" alt="User Image"/>
                                    <span class="hidden-xs">{{ $avUser->name }}</span>
                                </a>
                                @endif  
                            </li>
                        <li>
                            @if(config('adminlte.logout_method') == 'GET' || !config('adminlte.logout_method') && version_compare(\Illuminate\Foundation\Application::VERSION, '5.3.0', '<'))
                            <a href="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" class="logout">
                                    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                </a>
                            @else
                            <a href="#" class="logout">
                                    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                            </a>
                                <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                                    @if(config('adminlte.logout_method'))
                                        {{ method_field(config('adminlte.logout_method')) }}
                                    @endif
                                    {{ csrf_field() }}
                                </form>
                            @endif
                        </li>
                        @if(config('adminlte.right_sidebar') and (config('adminlte.layout') != 'top-nav'))
                        <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="#" data-toggle="control-sidebar" @if(!config('adminlte.right_sidebar_slide')) data-controlsidebar-slide="false" @endif>
                                    <i class="{{config('adminlte.right_sidebar_icon')}}"></i>
                                </a>
                            </li>
                        @endif
                    </ul>
                </div>
                @if(config('adminlte.layout') == 'top-nav')
                </div>
                @endif
            </nav>
        </header>

        @if(config('adminlte.layout') != 'top-nav')
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">

            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">

                <!-- Sidebar Menu -->
                <ul class="sidebar-menu" data-widget="tree">
                    @each('adminlte::partials.menu-item', $adminlte->menu(), 'item')
                </ul>
                <!-- /.sidebar-menu -->
            </section>
            <!-- /.sidebar -->
        </aside>
        @endif

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @if(config('adminlte.layout') == 'top-nav')
            <div class="container">
            @endif

            <!-- Content Header (Page header) -->
            <section class="content-header">
                @yield('content_header')
            </section>

            <!-- Main content -->
            <section class="content">

                @yield('content')

            </section>
            <!-- /.content -->
            @if(config('adminlte.layout') == 'top-nav')
            </div>
            <!-- /.container -->
            @endif
        </div>
        <!-- /.content-wrapper -->

        @hasSection('footer')
        <footer class="main-footer">
            @yield('footer')
        </footer>
        @endif

        @if(config('adminlte.right_sidebar') and (config('adminlte.layout') != 'top-nav'))
            <aside class="control-sidebar control-sidebar-{{config('adminlte.right_sidebar_theme')}}">
                @yield('right-sidebar')
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        @endif

    </div>
    <!-- ./wrapper -->
@stop
<div class="modal fade" id="load_model_content" tabindex="-1" role="dialog"></div>

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/toastr.css')}}">
    <script src="{{ asset('js/toastr.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/parsley.css')}}">
    <script src="{{ asset('js/parsley.min.js') }}"></script>
    <script>
        $(document).on('click', '.open-popup-content', function(){
            var action_type = $(this).attr('data-popup-action-type');
            var action_id = $(this).attr('data-popup-action-id');
            var extra_action = $(this).attr('data-extra-action');

            var load_model_url = '{{url('admin/load-popup-content/')}}';
            $.ajax({
                type: "POST",
                url: load_model_url,
                data: {
                    extra_action: extra_action,
                    action_type: action_type,
                    action_id: action_id,
                    _token: $('meta[name=csrf-token]').attr('content')

                },
                success: function(response){
                    $('#load_model_content').html(response.data);
                    $('#load_model_content').modal('show');
                    if(action_type == "send-mail"){
                        submit_all_form("send-mail-form","");
                    }else if(action_type == "generate-ticket"){
                        submit_all_form("generate-ticket-form","");
                    }else if(action_type == "check-payment-status"){
                        submit_all_form("check-payment-status-form","");
                    }
                    loadScript('{{ asset('js/parsley.min.js') }}')
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    if(jqXhr.status == 404) {
                        common_message_function("error","Something went wrong","",is_redirect=0)
                    }
                },
                failure: function (errMsg) {
                    common_message_function("error",errMsg,"",is_redirect=0)

                }
            });
        });
        function loadScript(url) {
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = url;
            document.body.appendChild(script);
        }
        function submit_all_form(form_id, type) {

            $('#' + form_id).parsley().on('field:validated', function () {
                var ok = $('.parsley-error').length === 0;
                $('.bs-callout-info').toggleClass('hidden', !ok);
                $('.bs-callout-warning').toggleClass('hidden', ok);
            }).on('form:submit', function () {
                $(".disabled_button").attr("disabled", true);
                var post_url = $('#' + form_id).attr("action"); //get form action url
                var request_method = $('#' + form_id).attr("method"); //get form GET/POST method
                var form_data = $('#' + form_id).serialize(); //Encode form elements for submission
                $.ajax({
                    url: post_url,
                    type: request_method,
                    data: form_data,
                    success: function (response) {
                        $(".disabled_button").attr("disabled", false);
                        $(".create_member_loader").hide();
                        if (response.status == 1) {
                            //$('#load_model_content').modal('hide');
                            if(response.data !=""){
                                response_html = '<p>Status : '+response.data.status+'</p>';
                                response_html += '<p>Total Authorized Amount : '+response.data.totalAuthorizedAmount+'</p>';
                                response_html += '<p>Total Capture Amount : '+response.data.totalCapturedAmount+'</p>';
                                response_html += '<p>Total Refunded Amount : '+response.data.totalRefundedAmount+'</p>';
                               $('.response_data').html(response_html);
                               $('.show_json_data').css("display",'block');
                               $('.show_json_data').html(JSON.stringify(response.data.json_response))

                            }
                            if(form_id == "generate-ticket-form"){
                                common_message_function("success", response.message, response.title,1);

                            }else{
                                common_message_function("success", response.message, response.title,0);

                            }

                        } else {
                            common_message_function("error", response.message, response.title,0);
                        }
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        if (jqXhr.status == 404) {
                            common_message_function("error", "Something went wrong", "",0)
                        }
                    }
                });
                return false;

            });


        }
        function common_message_function(type,message,title,is_redirect) {

            if(type == "success"){
                toastr.success(message, "");
            }else{
                toastr.warning(message, "");
            }
            if(is_redirect == 1){
                setTimeout(function(){
                    location.reload();
                }, 2000);

            }
        }

    </script>
    @stack('js')
    @yield('js')
@stop
