<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFifaMatchesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('fifa_matches', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('season_id')->nullable();
            $table->foreign('season_id')->references('id')->on('seasons')->onDelete('cascade');
            $table->json('titles')->nullable();
            $table->date('match_date')->nullable();
            $table->time('match_time')->nullable();
            $table->string('performance_id',120)->nullable();
            $table->tinyInteger('status')->default(1)->comment("1: enabled");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('fifa_matches');
    }

}
