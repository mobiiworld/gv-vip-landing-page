<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaxWeightCount extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('slots', function (Blueprint $table) {
            $table->integer('weight_count')->nullable()->after('actual_start_time');
        });
        Schema::table('programs', function (Blueprint $table) {
            $table->integer('max_weight_count')->nullable()->after('home_display_hours');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('slots', function (Blueprint $table) {
            $table->dropColumn(['weight_count']);
        });
        Schema::table('programs', function (Blueprint $table) {
            $table->dropColumn(['max_weight_count']);
        });
    }

}
