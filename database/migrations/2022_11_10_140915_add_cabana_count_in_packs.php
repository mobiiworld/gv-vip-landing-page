<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCabanaCountInPacks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packs', function (Blueprint $table) {
            $table->integer('cabana_total_count')->default(0)->after('drupal_user_id');
            $table->integer('cabana_remaining_count')->default(0)->after('cabana_total_count');
            $table->float('cabana_discount_percentage')->nullable()->after('cabana_remaining_count');
        });

        $season = \App\Season::where('status',1)->first();
        \App\Pack::where('season_id',$season->id)->where('category','silver')->update(['cabana_total_count'=>1, 'cabana_remaining_count' => 1, 'cabana_discount_percentage' => 25]);
        \App\Pack::where('season_id',$season->id)->where('category','gold')->update(['cabana_total_count'=>1, 'cabana_remaining_count' => 1, 'cabana_discount_percentage' => 40 ]);
        \App\Pack::where('season_id',$season->id)->where('category','platinum')->update(['cabana_total_count'=>1, 'cabana_remaining_count' => 1, 'cabana_discount_percentage' => 50 ]);
        \App\Pack::where('season_id',$season->id)->where('category','diamond')->update(['cabana_total_count'=>1, 'cabana_remaining_count' => 1, 'cabana_discount_percentage' => 100 ]);
        \App\Pack::where('season_id',$season->id)->where('category','private')->update(['cabana_total_count'=>2, 'cabana_remaining_count' => 2, 'cabana_discount_percentage' => 100 ]);

        \App\Pack::where('season_id',$season->id)->where('category','complimentary')                
                ->whereNotBetween('pack_number', [10332, 10481]) //media 
                ->whereNotBetween('pack_number', [8468, 8742]) //mini
                ->whereNotBetween('pack_number', [10632, 10681]) //coo vip
                ->update(['cabana_total_count'=>1, 'cabana_remaining_count' => 1, 'cabana_discount_percentage' => 25]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packs', function (Blueprint $table) {
            $table->dropColumn(['cabana_total_count','cabana_remaining_count','cabana_discount_percentage']);
        });
    }
}
