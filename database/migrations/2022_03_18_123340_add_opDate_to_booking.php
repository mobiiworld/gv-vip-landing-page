<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOpDateToBooking extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('book_slots', function (Blueprint $table) {
            $table->date('operational_date')->nullable()->after('payment_extras');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('book_slots', function (Blueprint $table) {
            $table->dropColumn(['operational_date']);
        });
    }

}
