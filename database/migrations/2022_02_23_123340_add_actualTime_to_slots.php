<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActualTimeToSlots extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('slots', function (Blueprint $table) {
            $table->time('actual_start_time')->nullable()->after('end_time');
        });
        Schema::table('book_slots', function (Blueprint $table) {
            $table->uuid('program_id')->nullable()->after('id');
            $table->foreign('program_id')->references('id')->on('programs')->onDelete('cascade');
            $table->dateTime('actual_program_dt')->nullable()->after('program_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('slots', function (Blueprint $table) {
            $table->dropColumn(['actual_start_time']);
        });
        Schema::table('book_slots', function (Blueprint $table) {
            $table->dropColumn(['program_id', 'actual_program_dt']);
        });
    }

}
