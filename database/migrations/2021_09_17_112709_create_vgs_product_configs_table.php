<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVgsProductConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vgs_product_configs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slug',255)->nullable();
            $table->string('product',255)->nullable();
            $table->string('product_name',255)->nullable();
            $table->string('product_id',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vgs_product_configs');
    }
}
