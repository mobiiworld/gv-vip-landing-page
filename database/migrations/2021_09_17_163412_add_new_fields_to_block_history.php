<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToBlockHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pack_block_histories', function (Blueprint $table) {
            $table->string('vgs_status')->after('blocked_by')->nullable();
            $table->text('media_codes')->after('vgs_status')->nullable()->comment('Pending, Sale search Started,Sale search Success,No Media Codes Found,');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pack_block_histories', function (Blueprint $table) {
            //
        });
    }
}
