<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCabanaProductsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('cabana_products', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->integer('hours');
            $table->double('workingday_price', 11, 2);
            $table->double('weekend_price', 11, 2);
            $table->boolean('allow_vip')->default(0);
            $table->json('vip_prices')->nullable();
            $table->boolean('status')->default(1)->comment("1: enabled");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('cabana_products');
    }

}
