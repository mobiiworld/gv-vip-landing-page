<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangesForFitnessNextday extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fitness_seasons', function (Blueprint $table) {
            $table->time('start_time')->nullable()->after('end_date');
            $table->time('end_time')->nullable()->after('start_time');
            $table->tinyInteger('is_next_day')->default(0)->after('end_time');
        });
        Schema::table('fitness_daily_tracks', function (Blueprint $table) {
            $table->date('participated_date')->nullable()->after('steps');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fitness_seasons', function (Blueprint $table) {
            $table->dropColumn(['start_time', 'end_time', 'is_next_day']);
        });
        Schema::table('fitness_daily_tracks', function (Blueprint $table) {
            $table->dropColumn(['participated_date']);
        });
    }
}
