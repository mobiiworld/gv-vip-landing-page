<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewFiledsForPopupOffseason extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seasons', function (Blueprint $table) {
            $table->boolean('show_register_popup')->default(0)->after('status')->comment('show_register_popup : 0 - disabled, 1 -  enabled');            
            $table->boolean('fixed_popup')->default(0)->after('show_register_popup');            
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seasons', function (Blueprint $table) {
            $table->dropColumn(['show_register_popup','fixed_popup']);
        });
    }
}
