<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('book_slots', function (Blueprint $table) {           
            $table->string('first_name')->nullable()->after('booking_user_id');
            $table->string('last_name')->nullable()->after('first_name');
            $table->string('email')->nullable()->after('last_name');
            $table->string('mobile')->nullable()->after('email');
            $table->string('shop_cart_id')->nullable()->after('mobile');
            $table->string('sale_code')->nullable()->after('shop_cart_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('book_slots', function (Blueprint $table) {
            //
        });
    }
}
