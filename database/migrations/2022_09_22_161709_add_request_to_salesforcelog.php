<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRequestToSalesforcelog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salesforce_logs', function (Blueprint $table) {
            $table->text('sale_request')->nullable()->after('refenrence_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salesforce_logs', function (Blueprint $table) {
            $table->dropColumn(['sale_request']);
        });
    }
}
