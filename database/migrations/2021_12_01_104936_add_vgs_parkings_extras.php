<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVgsParkingsExtras extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('vehicle_plocations', function (Blueprint $table) {
            $table->string('beco_name')->nullable()->after('inside');
            $table->double('latitude', 15, 6)->nullable()->after('beco_name');
            $table->double('longitude', 15, 6)->nullable()->after('latitude');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('vehicle_plocations', function (Blueprint $table) {
            $table->dropColumn(['latitude', 'longitude', 'beco_name']);
        });
    }

}
