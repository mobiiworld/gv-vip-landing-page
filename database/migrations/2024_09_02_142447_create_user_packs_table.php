<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_packs', function (Blueprint $table) {
            $table->uuid('id');
            //$table->primary('id');
            $table->uuid('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->uuid('pack_id')->nullable();
            $table->foreign('pack_id')->references('id')->on('packs')->onDelete('cascade');
            $table->string('account_id')->nullable();
            $table->tinyInteger('account_activation_status')->default(0);
            $table->string('designa_customer_uid')->nullable();
            $table->string('designa_customer_id')->nullable();
            $table->string('blocked_status')->default('no')->comment('values-yes,no');
            $table->dateTime('blocked_date')->nullable();
            $table->integer('blocked_by')->unsigned()->nullable();
            $table->string('sales_force_ref_id',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_packs');
    }
}
