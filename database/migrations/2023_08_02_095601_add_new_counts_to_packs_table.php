<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewCountsToPacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packs', function (Blueprint $table) {
            $table->integer('inpark_taxi_voucher')->default(0)->after('cabana_discount_percentage');
            $table->integer('carwash_services_voucher')->default(0)->after('inpark_taxi_voucher');
            $table->integer('porter_services_voucher')->default(0)->after('carwash_services_voucher');
            $table->integer('stunt_show')->default(0)->after('porter_services_voucher');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packs', function (Blueprint $table) {
            $table->dropColumn(['original_category']);
        });
    }
}
