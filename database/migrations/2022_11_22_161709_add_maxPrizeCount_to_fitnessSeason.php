<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaxPrizeCountTofitnessSeason extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('fitness_seasons', function (Blueprint $table) {
            $table->smallInteger('max_ups_prize_count')->default(1)->after('target_steps');
        });
        Schema::table('fitness_daily_tracks', function (Blueprint $table) {
            $table->json('sf_request')->nullable();
            $table->json('sf_response')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('fitness_seasons', function (Blueprint $table) {
            $table->dropColumn(['max_ups_prize_count']);
        });
        Schema::table('fitness_daily_tracks', function (Blueprint $table) {
            $table->dropColumn(['sf_request', 'sf_response']);
        });
    }

}
