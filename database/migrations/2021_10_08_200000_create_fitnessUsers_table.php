<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFitnessUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('fitness_users', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->integer('fitness_season_id')->unsigned()->nullable();
            $table->foreign('fitness_season_id')->references('id')->on('fitness_seasons')->onDelete('cascade');
            $table->string('fname')->nullable();
            $table->string('lname')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile')->nullable();
            $table->integer('drupal_id')->nullable();
            $table->bigInteger('total_steps')->default(0);
            $table->boolean('is_winner')->default(0);
            $table->char('coupon_code', 10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('fitness_users');
    }

}
