<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclePlocationTimingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_plocation_timings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('vehicle_plocation_id', 36);
            $table->foreign('vehicle_plocation_id')->references('id')->on('vehicle_plocations')->onDelete('cascade');
            $table->integer('day')->nullable()->comment('1 => Monday, 2 => Tuesday, 3 => Wednesday, 4 => Thursday, 5 => Friday, 6 => Saturday, 7 => Sunday');
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_plocation_timings');
    }
}
