<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyMajlisTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('programs', function (Blueprint $table) {
            $table->json('vgs_pcodes')->nullable()->after('max_weight_count');
        });
        Schema::table('vip_tables', function (Blueprint $table) {
            $table->boolean('admin_only')->default(0)->after('is_hourly');
        });
        Schema::table('book_slots', function (Blueprint $table) {
            $table->char('table_type', 10)->nullable()->after('is_vip');
            $table->dateTime('status_updated_at')->nullable()->after('is_occupied');
            $table->mediumText('ticket_ids')->nullable()->after('status_updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('programs', function (Blueprint $table) {
            $table->dropColumn(['max_weight_count']);
        });
        Schema::table('vip_tables', function (Blueprint $table) {
            $table->dropColumn(['admin_only']);
        });
        Schema::table('book_slots', function (Blueprint $table) {
            $table->dropColumn(['table_type', 'status_updated_at', 'ticket_ids']);
        });
    }

}
