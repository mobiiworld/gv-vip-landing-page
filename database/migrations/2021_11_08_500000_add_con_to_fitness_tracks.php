<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConToFitnessTracks extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('fitness_daily_tracks', function (Blueprint $table) {
            $table->date('prize_collected_on')->nullable()->after('coupon_code');
        });
        Schema::table('fitness_monthly_tracks', function (Blueprint $table) {
            $table->date('prize_collected_on')->nullable()->after('coupon_code');
        });
        Schema::table('fitness_users', function (Blueprint $table) {
            $table->date('prize_collected_on')->nullable()->after('coupon_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('fitness_daily_tracks', function (Blueprint $table) {
            $table->dropColumn(['prize_collected_on']);
        });
        Schema::table('fitness_monthly_tracks', function (Blueprint $table) {
            $table->dropColumn(['prize_collected_on']);
        });
        Schema::table('fitness_users', function (Blueprint $table) {
            $table->dropColumn(['prize_collected_on']);
        });
    }

}
