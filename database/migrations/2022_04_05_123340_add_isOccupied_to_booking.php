<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsOccupiedToBooking extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('book_slots', function (Blueprint $table) {
            $table->boolean('is_occupied')->default(false)->after('operational_date')->comment('added on 2022-04-05');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('book_slots', function (Blueprint $table) {
            $table->dropColumn(['is_occupied']);
        });
    }

}
