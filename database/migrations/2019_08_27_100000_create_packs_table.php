<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('packs', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('sale_id')->nullable();
            $table->string('activation_code')->nullable()->index();
            $table->char('prefix', 50)->nullable();
            $table->integer('pack_number')->nullable()->index();
            $table->integer('car_activation_allowed')->nullable();
            $table->integer('park_entry_count')->nullable();
            $table->integer('wonderpass_count')->nullable();
            $table->integer('parking_voucher_count')->nullable();
            $table->string('category')->nullable();
            $table->boolean('status')->default(1);
            $table->boolean('used')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('packs');
    }

}
