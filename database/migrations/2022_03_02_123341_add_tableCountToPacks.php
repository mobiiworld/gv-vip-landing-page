<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableCountToPacks extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('packs', function (Blueprint $table) {
            $table->integer('allowed_tables')->default(0)->after('parking_voucher_count');
            $table->integer('remaining_tables')->default(0)->after('allowed_tables');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('packs', function (Blueprint $table) {
            $table->dropColumn(['allowed_tables', 'remaining_tables']);
        });
    }

}
