<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDfieldsToUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('users', function (Blueprint $table) {
            $table->string('designa_customer_uid')->nullable()->after('remember_token');
            $table->string('designa_customer_id')->nullable()->after('designa_customer_uid');
            //$table->tinyInteger('designa_sync_status')->default(0)->after('designa_customer_id')->index();
        });

        Schema::table('user_cars', function (Blueprint $table) {
            $table->string('designa_card_uid')->nullable()->after('plate_number');
            $table->string('designa_card_id')->nullable()->after('designa_card_uid');
            $table->string('designa_park_id')->nullable()->after('designa_card_id');
            $table->tinyInteger('designa_setv_status')->default(0)->after('designa_park_id');
            $table->tinyInteger('designa_acc_status')->default(0)->after('designa_setv_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['designa_customer_uid', 'designa_customer_id', 'designa_sync_status']);
        });
        Schema::table('user_cars', function (Blueprint $table) {
            $table->dropColumn(['designa_card_uid', 'designa_card_id', 'designa_park_id']);
        });
    }

}
