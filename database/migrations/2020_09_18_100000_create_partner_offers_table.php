<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerOffersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('partner_offers', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('pack_id', 36)->nullable();
            $table->foreign('pack_id')->references('id')->on('packs')->onDelete('cascade');
            $table->string('parent_merchant_name_en')->nullable();
            $table->string('parent_merchant_name_ar')->nullable();
            $table->string('merchant_name_en')->nullable();
            $table->string('merchant_name_ar')->nullable();
            $table->string('offer_name_en')->nullable();
            $table->string('offer_name_ar')->nullable();
            $table->date('valid_from')->nullable();
            $table->date('valid_to')->nullable();
            $table->string('promo_code', 191)->nullable()->index();
            $table->char('code_type', 50)->nullable();
            $table->smallInteger('t_and_c')->nullable();
            $table->text('link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('partner_offers');
    }

}
