<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMajlisBookingStatusHistories extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('majlis_booking_status_histories', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('booking_id')->nullable();
            $table->foreign('booking_id')->references('id')->on('book_slots')->onDelete('cascade');
            $table->string('status')->nullable();
            $table->mediumText('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('majlis_booking_status_histories');
    }

}
