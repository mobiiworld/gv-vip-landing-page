<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPolygonsToVpl extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('vehicle_plocations', function (Blueprint $table) {
            $table->polygon('polygons')->nullable()->after('extras');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('vehicle_plocations', function (Blueprint $table) {
            $table->dropColumn(['polygons']);
        });
    }

}
