<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProgramNameToFitnessSeasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fitness_seasons', function (Blueprint $table) {
            $table->json('program_name')->after('in_area')->nullable();
            $table->json('program_description')->after('program_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fitness_seasons', function (Blueprint $table) {
            //
        });
    }
}
