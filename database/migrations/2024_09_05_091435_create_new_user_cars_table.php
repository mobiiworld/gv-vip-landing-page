<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewUserCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_user_cars', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('user_id', 36)->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('drupal_user_id')->nullable();
            //$table->boolean('is_vip')->default(true); if user car pack exists then vip
            $table->tinyInteger('type')->nullable()->comment('1:Hatchback,2:Sedan');
            $table->integer('country_id')->unsigned()->nullable();
            $table->foreign('country_id')->references('id')->on('car_countries')->onDelete('cascade');
            $table->integer('emirate_id')->unsigned()->nullable();
            $table->foreign('emirate_id')->references('id')->on('emirates')->onDelete('cascade');
            $table->integer('plate_category_id')->unsigned()->nullable();
            $table->foreign('plate_category_id')->references('id')->on('plate_categories')->onDelete('cascade');
            $table->string('plate_prefix')->nullable();
            $table->string('plate_number')->nullable();
            $table->string('designa_card_uid')->nullable();
            $table->string('designa_card_id')->nullable();
            $table->string('designa_park_id')->nullable();
            $table->tinyInteger('designa_setv_status')->default(0);
            $table->tinyInteger('designa_acc_status')->default(0); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_user_cars');
    }
}
