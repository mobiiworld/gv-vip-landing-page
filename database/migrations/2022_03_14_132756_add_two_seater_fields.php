<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTwoSeaterFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('slots', function (Blueprint $table) {        
            $table->string('vgs_product_code_2')->nullable()->after('vgs_product_code_6_vns');
            $table->string('vgs_product_code_2_ns')->nullable()->after('vgs_product_code_2');
            $table->string('vgs_product_code_2_vs')->nullable()->after('vgs_product_code_2_ns');
            $table->string('vgs_product_code_2_vns')->nullable()->after('vgs_product_code_2_vs');
            $table->float('amount_2', 11, 2)->nullable()->after('amount_6');
        });
        
        Schema::table('programs', function (Blueprint $table) {   
            $table->string('vgs_product_code_daily_2')->nullable()->after('vgs_product_code_daily_6_vs');
            $table->string('vgs_product_code_daily_2_ns')->nullable()->after('vgs_product_code_daily_2');
            $table->string('vgs_product_code_daily_2_vs')->nullable()->after('vgs_product_code_daily_2_ns');
            $table->string('vgs_product_code_daily_2_vns')->nullable()->after('vgs_product_code_daily_2_vs');
            $table->float('full_amount_2', 11, 2)->nullable()->after('full_amount_6');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('slots', function (Blueprint $table) {
            $table->dropColumn(['vgs_product_code_2', 'vgs_product_code_2_ns', 'vgs_product_code_2_vs', 'vgs_product_code_2_vns','amount_2']);
        });
        Schema::table('programs', function (Blueprint $table) {
            $table->dropColumn(['vgs_product_code_2', 'vgs_product_code_2_ns', 'vgs_product_code_2_vs', 'vgs_product_code_2_vns','full_amount_2']);
        });
    }
}
