<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFitnessDailyTracksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('fitness_daily_tracks', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('fitness_user_id')->nullable();
            $table->foreign('fitness_user_id')->references('id')->on('fitness_users')->onDelete('cascade');
            $table->integer('fitness_season_id')->unsigned()->nullable();
            $table->foreign('fitness_season_id')->references('id')->on('fitness_seasons')->onDelete('cascade');
            $table->string('platform', 100)->nullable();
            $table->bigInteger('steps')->default(0);
            $table->tinyInteger('status')->default(0)->comment('0:failed,1:completed,2:user ended');
            $table->char('coupon_code', 10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('fitness_daily_tracks');
    }

}
