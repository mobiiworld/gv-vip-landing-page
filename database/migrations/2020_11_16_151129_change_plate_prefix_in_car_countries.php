<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePlatePrefixInCarCountries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('car_countries', function (Blueprint $table) {
        //    // $table->dropColumn(['plate_prefix']);
            
        // });
        Schema::table('car_countries', function (Blueprint $table) {
           
            $table->text('plate_prefix_new')->nullable()->after('code');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('car_countries', function (Blueprint $table) {
            //
        });
    }
}
