<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_slots', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');  
            $table->uuid('program_id')->nullable();
            $table->foreign('program_id')->references('id')->on('programs')->onDelete('cascade');
            $table->uuid('slot_id')->nullable();
            $table->foreign('slot_id')->references('id')->on('slots')->onDelete('cascade');
            $table->uuid('table_id')->nullable();
            $table->foreign('table_id')->references('id')->on('vip_tables')->onDelete('cascade');
            $table->date('program_date')->nullable();            
            $table->tinyInteger('program_status')->default(1)->comment('1=>Created,2=>Waiting Confirmation,3=>Confirmed');
            $table->tinyInteger('status')->default(1)->comment('2=>Disable, 1=>Enable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_slots');
    }
}
