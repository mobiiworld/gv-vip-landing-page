<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToProgramsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('programs', function (Blueprint $table) {
            $table->string('vgs_product_code_daily')->nullable()->after('hours_before_booking');
            $table->boolean('has_priority')->default(true)->after('vgs_product_code_daily');
        });

        Schema::table('vip_tables', function (Blueprint $table) {
            $table->integer('table_number')->default(1)->after('type');
        });

        Schema::table('book_slots', function (Blueprint $table) {
            $table->dropColumn('status');
           
        });
        Schema::table('book_slots', function (Blueprint $table) {           
            $table->string('status')->default('pending')->after('booking_user_id')->comment('pending,waiting_confirmation,waiting_payment,order_confirmed,timeout');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('programs_tables', function (Blueprint $table) {
            //
        });
    }
}
