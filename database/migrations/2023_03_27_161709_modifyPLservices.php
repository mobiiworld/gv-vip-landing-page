<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPLservices extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('vehicle_plocation_services', function (Blueprint $table) {
            $table->string('beco_name')->nullable()->after('parking_service_id');
            $table->json('descriptions')->nullable()->after('beco_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('vehicle_plocation_services', function (Blueprint $table) {
            $table->dropColumn(['beco_name','descriptions']);
        });
    }

}
