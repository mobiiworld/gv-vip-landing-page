<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdminIdToBooking extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('book_slots', function (Blueprint $table) {
            $table->integer('admin_id')->unsigned()->nullable()->after('booking_user_id');
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade');
            $table->boolean('is_vip')->nullable()->after('program_slot_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('book_slots', function (Blueprint $table) {
            $table->dropColumn(['admin_id', 'is_vip']);
        });
    }

}
