<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFitnessSeasonsImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fitness_seasons_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fitness_season_id')->unsigned()->nullable();
            $table->foreign('fitness_season_id')->references('id')->on('fitness_seasons')->onDelete('cascade');
            $table->string('image')->nullable();
            $table->integer('index_no')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fitness_seasons_images');
    }
}
