<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultToFitnessImages extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('fitness_seasons_images', function (Blueprint $table) {
            $table->boolean('default')->default(false)->after('index_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('fitness_seasons_images', function (Blueprint $table) {
            $table->dropColumn(['default']);
        });
    }

}
