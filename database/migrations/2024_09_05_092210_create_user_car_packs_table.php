<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCarPacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_car_packs', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('user_car_id')->nullable();
            $table->foreign('user_car_id')->references('id')->on('new_user_cars')->onDelete('cascade');
            $table->uuid('pack_id')->nullable();
            $table->foreign('pack_id')->references('id')->on('packs')->onDelete('cascade');
            $table->string('designa_card_uid')->nullable();
            $table->string('designa_card_id')->nullable();
            $table->string('designa_park_id')->nullable();
            $table->tinyInteger('designa_setv_status')->default(0);
            $table->tinyInteger('designa_acc_status')->default(0); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_car_packs');
    }
}
