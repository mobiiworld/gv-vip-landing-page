<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddZoneToVgsParkings extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('vehicle_plocations', function (Blueprint $table) {
            $table->json('zone_names')->nullable()->after('vgs_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('vehicle_plocations', function (Blueprint $table) {
            $table->dropColumn(['zone_names']);
        });
    }

}
