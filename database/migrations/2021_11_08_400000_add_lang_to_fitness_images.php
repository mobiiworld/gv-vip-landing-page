<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLangToFitnessImages extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('fitness_seasons_images', function (Blueprint $table) {
            $table->char('language',5)->default('en')->after('default');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('fitness_seasons_images', function (Blueprint $table) {
            $table->dropColumn(['language']);
        });
    }

}
