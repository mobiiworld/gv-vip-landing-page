<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email', 191);
            $table->char('mobile_dial_code', 20)->nullable();
            $table->char('mobile', 20)->nullable();
            $table->char('gender', 10)->nullable();
            $table->date('dob')->nullable();
            $table->integer('nationality_id')->unsigned()->nullable();
            $table->foreign('nationality_id')->references('id')->on('countries')->onDelete('cascade');
            $table->integer('residence_id')->unsigned()->nullable();
            $table->foreign('residence_id')->references('id')->on('countries')->onDelete('cascade');
            $table->char('pack_id', 36)->nullable();
            $table->foreign('pack_id')->references('id')->on('packs')->onDelete('cascade');
            $table->boolean('recieve_updates')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }

}
