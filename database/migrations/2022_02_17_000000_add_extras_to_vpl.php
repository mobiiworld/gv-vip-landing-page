<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtrasToVpl extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('vehicle_plocations', function (Blueprint $table) {
            $table->json('extras')->nullable()->after('closed');
        });
        App\VehiclePlocation::whereNotNull('id')->update(['extras'=> json_encode(["pay_park" => false, "car_wash"  => false, "directions" => false])]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('vehicle_plocations', function (Blueprint $table) {
            $table->dropColumn(['extras']);
        });
    }

}
