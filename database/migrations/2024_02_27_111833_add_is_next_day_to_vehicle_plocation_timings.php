<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsNextDayToVehiclePlocationTimings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicle_plocation_timings', function (Blueprint $table) {
            $table->tinyInteger('is_next_day')->default(0)->after('day');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_plocation_timings', function (Blueprint $table) {
            $table->dropColumn(['is_next_day']);
        });
    }
}
