<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFitnessPrizesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('fitness_prizes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fitness_season_id')->unsigned()->nullable();
            $table->foreign('fitness_season_id')->references('id')->on('fitness_seasons')->onDelete('cascade');
            $table->json('names')->nullable();
            $table->json('descriptions')->nullable();
            $table->char('type', 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('fitness_prizes');
    }

}
