<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToProgramAndSlots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('programs', function (Blueprint $table) {
             $table->integer('full_evening_table_limit')->nullable()->after('suggesetd_table_count');
             $table->float('full_amount',11, 2)->nullable()->after('full_evening_table_limit');
        });
        Schema::table('slots', function (Blueprint $table) {
            $table->float('amount',11, 2)->nullable()->after('vgs_product_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
}
