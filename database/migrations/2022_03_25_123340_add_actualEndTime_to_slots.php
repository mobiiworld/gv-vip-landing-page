<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActualEndTimeToSlots extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('slots', function (Blueprint $table) {
            $table->time('actual_end_time')->nullable()->after('actual_start_time');
        });

        \DB::statement("UPDATE slots SET actual_end_time = actual_start_time + INTERVAL 1 HOUR where actual_start_time IS NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('slots', function (Blueprint $table) {
            $table->dropColumn(['actual_end_time']);
        });
    }

}
