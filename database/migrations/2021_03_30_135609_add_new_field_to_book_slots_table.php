<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldToBookSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('book_slots', function (Blueprint $table) {            
            $table->string('booking_type')->nullable()->after('amount');
            $table->text('vgs_product_codes')->nullable()->after('booking_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('book_slots', function (Blueprint $table) {
            //
        });
    }
}
