<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSmokingNewFieldsToVipTablesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('vip_tables', function (Blueprint $table) {
            $table->tinyInteger('smoking_status')->default(2)->comment('2=>Non Smoking, 1=>Smoking')->after('status');
            $table->tinyInteger('seat_type')->default(4)->comment('4=>4 seater, 6=>6 seater')->after('status');
        });

        Schema::table('slots', function (Blueprint $table) {
            $table->float('amount_6', 11, 2)->nullable()->after('amount');
            $table->string('vgs_product_code_6')->nullable()->after('amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('slots', function (Blueprint $table) {
            $table->dropColumn(['vgs_product_code_6', 'amount_6']);
        });

        Schema::table('vip_tables', function (Blueprint $table) {
            $table->dropColumn(['smoking_status', 'seat_type']);
        });
    }

}
