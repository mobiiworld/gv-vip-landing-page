<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCabanaDaysTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('cabana_days', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->dateTime('start_dt')->nullable();
            $table->dateTime('end_dt')->nullable();
            $table->tinyInteger('status')->default(1)->comment("1: enabled");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('cabana_days');
    }

}
