<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToOffers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partner_offers', function (Blueprint $table) {
            $table->boolean('active_status')->default(1)->after('link')->nullable();
            $table->boolean('open_status')->default(1)->after('active_status')->nullable();
            $table->string('closed_message_en',255)->nullable()->after('open_status')->nullable();
            $table->string('closed_message_ar',255)->nullable()->after('closed_message_en')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
}
