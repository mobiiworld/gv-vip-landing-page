<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVipTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vip_tables', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('name',255)->nullable();
            $table->string('type',10)->default('normal')->comment('vip,normal');            
            $table->tinyInteger('status')->default(1)->comment('2=>Disable, 1=>Enable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('furniture_tables');
    }
}
