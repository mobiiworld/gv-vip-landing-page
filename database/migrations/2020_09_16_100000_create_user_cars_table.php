<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCarsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('user_cars', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('user_id', 36)->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('country_id')->unsigned()->nullable();
            $table->foreign('country_id')->references('id')->on('car_countries')->onDelete('cascade');
            $table->integer('emirate_id')->unsigned()->nullable();
            $table->foreign('emirate_id')->references('id')->on('emirates')->onDelete('cascade');
            $table->integer('plate_category_id')->unsigned()->nullable();
            $table->foreign('plate_category_id')->references('id')->on('plate_categories')->onDelete('cascade');
            $table->char('plate_prefix', 10)->nullable();
            $table->string('plate_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('user_cars');
    }

}
