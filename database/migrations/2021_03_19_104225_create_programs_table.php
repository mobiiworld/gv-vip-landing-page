<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');            
            $table->string('program_uid',100)->unique()->nullable();    
            $table->string('name',255)->nullable();        
            $table->text('description')->nullable();        
            $table->integer('hours_before_booking')->nullable();        
            $table->tinyInteger('status')->default(1)->comment('2=>Disable, 1=>Enable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
