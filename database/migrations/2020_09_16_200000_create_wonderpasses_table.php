<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWonderpassesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('wonderpasses', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('pack_id', 36)->nullable();
            $table->foreign('pack_id')->references('id')->on('packs')->onDelete('cascade');
            $table->string('card_number')->nullable();
            $table->integer('balance_points')->nullable();
            $table->integer('carnaval_ride_points')->nullable();
            $table->integer('ripleys_count')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('wonderpasses');
    }

}
