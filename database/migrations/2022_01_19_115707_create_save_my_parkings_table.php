<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaveMyParkingsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('save_my_parkings', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('vehicle_plocation_id')->nullable();
            $table->foreign('vehicle_plocation_id')->references('id')->on('vehicle_plocations')->onDelete('cascade');
            $table->string('vgs_id', 100)->nullable();
            $table->string('slot_number')->nullable();
            $table->integer('drupal_id')->nullable();
            $table->double('latitude', 15, 6)->nullable();
            $table->double('longitude', 15, 6)->nullable();
            $table->text('note')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('save_my_parkings');
    }

}
