<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToSeasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seasons', function (Blueprint $table) {
            $table->string('pack_activation_disabled_title_en')->nullable()->after('pack_activation_enabled');
            $table->string('pack_activation_disabled_title_ar')->nullable()->after('pack_activation_disabled_title_en');
            $table->string('pack_activation_disabled_image')->nullable()->after('pack_activation_disabled_title_ar');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seasons', function (Blueprint $table) {
            $table->dropColumn(['pack_activation_disabled_title_en', 'pack_activation_disabled_title_ar', 'pack_activation_disabled_image']);
        });
    }
}
