<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReleasedBookingSlots extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('released_booking_slots', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('program_slot_id')->nullable();
            $table->foreign('program_slot_id')->references('id')->on('program_slots')->onDelete('cascade');
            $table->uuid('booking_id')->nullable();
            $table->foreign('booking_id')->references('id')->on('book_slots')->onDelete('cascade');
            $table->integer('admin_id')->unsigned()->nullable();
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('released_booking_slots');
    }

}
