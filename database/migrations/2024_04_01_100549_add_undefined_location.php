<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUndefinedLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicle_plocations', function (Blueprint $table) {
            $table->tinyInteger('is_undefined_location')->default(0)->after('vgs_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_plocations', function (Blueprint $table) {
            $table->dropColumn(['is_undefined_location']);
        });
    }
}
