<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVgsParkingsExtras extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('vehicle_pimages', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('plocation_id');
            $table->foreign('plocation_id')->references('id')->on('vehicle_plocations')->onDelete('cascade');
            $table->string('image')->nullable();
            $table->char('language', 5)->default('en');
            $table->integer('display_order')->nullable();
            $table->timestamps();
        });

        Schema::table('vehicle_plocations', function (Blueprint $table) {
            $table->boolean('is_custom')->default(0)->after('id');
            $table->integer('inside')->default(0)->after('total_capacity');
            $table->integer('display_order')->nullable()->after('inside');
        });

        $pExist = App\VehiclePlocation::get();
        $i = 1;
        foreach ($pExist as $pl) {
            $pl->display_order = $i;
            $pl->save();
            $i++;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('vehicle_pimages');
        Schema::table('vehicle_plocations', function (Blueprint $table) {
            $table->dropColumn(['is_custom', 'inside', 'display_order']);
        });
    }

}
