<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_slots', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');  
            $table->date('program_date')->nullable();
            $table->string('booking_user_id')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1=>Pending,2=>Waiting Confirmation,3=>Waiting Payment,4=>Confirmed,5=>Timeout');
            $table->text('program_slot_id')->nullable()->comment('comma seperated values of program slot id');
            $table->timestamps();
        });

        Schema::table('program_slots', function (Blueprint $table) {
            $table->uuid('booking_id')->nullable()->after('program_date');
            $table->foreign('booking_id')->references('id')->on('book_slots')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_slots');
    }
}
