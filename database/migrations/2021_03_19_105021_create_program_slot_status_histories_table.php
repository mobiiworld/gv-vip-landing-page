<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramSlotStatusHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_slot_status_histories', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('program_slot_id')->nullable();
            $table->foreign('program_slot_id')->references('id')->on('program_slots')->onDelete('cascade');
            $table->tinyInteger('program_status')->default(1)->comment('1=>Created,2=>Waiting Confirmation,3=>Confirmed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_slot_status_histories');
    }
}
