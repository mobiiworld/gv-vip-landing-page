<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPopupsTofitnessSeason extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('fitness_seasons', function (Blueprint $table) {
            $table->json('popup_titles')->nullable()->after('program_description');
            $table->json('popup_messages')->nullable()->after('popup_titles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('fitness_seasons', function (Blueprint $table) {
            $table->dropColumn(['popup_titles', 'popup_messages']);
        });
    }

}
