<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGeofenceToFitnessSeasonsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('fitness_seasons', function (Blueprint $table) {
            $table->boolean('geofence')->default(true)->after('is_show_homepage');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('fitness_seasons', function (Blueprint $table) {
            $table->dropColumn(['geofence']);
        });
    }

}
