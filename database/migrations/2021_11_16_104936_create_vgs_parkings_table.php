<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVgsParkingsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('vehicle_plocations', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('vgs_id', 100)->nullable();
            $table->json('names')->nullable();
            $table->json('descriptions')->nullable();
            $table->integer('total_capacity')->default(0);
            $table->boolean('status')->default(1);
            $table->timestamps();
        });

        Schema::create('vehicle_psettings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('api_interval')->default(5)->comment('In minutes');
            $table->dateTime('last_updated_at')->nullable();
            $table->json('api_data')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('vehicle_plocations');
        Schema::dropIfExists('vehicle_psettings');
    }

}
