<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPmodeToBooking extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('book_slots', function (Blueprint $table) {
            $table->tinyInteger('payment_mode')->nullable()->after('is_vip')->comment('1=cash,2=card');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('book_slots', function (Blueprint $table) {
            $table->dropColumn(['payment_mode']);
        });
    }

}
