<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVgsLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vgs_logs', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('type',50)->nullable();
            $table->string('url',255)->nullable();
            $table->text('request_param')->nullable();
            $table->longText('response_param')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vgs_logs');
    }
}
