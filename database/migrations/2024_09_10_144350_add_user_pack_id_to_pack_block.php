<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserPackIdToPackBlock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pack_block_histories', function (Blueprint $table) {
            $table->uuid('pack_user_id')->nullable()->afetr("user_id");
            $table->foreign('pack_user_id')->references('id')->on('user_packs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pack_block', function (Blueprint $table) {
            //
        });
    }
}
