<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVgs6ToPrograms extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('programs', function (Blueprint $table) {
            $table->string('vgs_product_code_daily_6')->nullable()->after('vgs_product_code_daily');
            $table->float('full_amount_6', 11, 2)->nullable()->after('full_amount');
            $table->integer('home_display_hours')->nullable()->after('has_priority');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('programs', function (Blueprint $table) {
            $table->dropColumn(['vgs_product_code_daily_6', 'full_amount_6', 'home_display_hours']);
        });
    }

}
