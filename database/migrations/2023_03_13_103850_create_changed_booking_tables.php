<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChangedBookingTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('changed_booking_tables', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->integer('admin_id')->unsigned()->nullable();
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade');
            $table->uuid('booking_id')->nullable();
            $table->foreign('booking_id')->references('id')->on('book_slots')->onDelete('cascade');
            $table->uuid('tid_changed_from')->nullable();
            $table->foreign('tid_changed_from')->references('id')->on('vip_tables')->onDelete('cascade');
            $table->uuid('tid_changed_to')->nullable();
            $table->foreign('tid_changed_to')->references('id')->on('vip_tables')->onDelete('cascade');
            $table->mediumText('slot_ids')->nullable();
            $table->mediumText('released_pslot_ids')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('changed_booking_tables');
    }

}
