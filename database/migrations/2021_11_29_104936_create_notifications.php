<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotifications extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('notifications', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->json('titles')->nullable();
            $table->json('messages')->nullable();
            $table->smallInteger('type')->default(1)->comment('1:general');
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
        Schema::create('notification_users', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->integer('drupal_id')->nullable();
            $table->dateTime('last_read_at')->nullable();
            $table->timestamps();
            $table->index('drupal_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('notifications');
        Schema::dropIfExists('notification_users');
    }

}
