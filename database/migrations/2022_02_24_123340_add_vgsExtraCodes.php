<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVgsExtraCodes extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('slots', function (Blueprint $table) {
            $table->string('vgs_product_code_ns')->nullable()->after('vgs_product_code');
            $table->string('vgs_product_code_vs')->nullable()->after('vgs_product_code_ns');
            $table->string('vgs_product_code_vns')->nullable()->after('vgs_product_code_vs');

            $table->string('vgs_product_code_6_ns')->nullable()->after('vgs_product_code_6');
            $table->string('vgs_product_code_6_vs')->nullable()->after('vgs_product_code_6_ns');
            $table->string('vgs_product_code_6_vns')->nullable()->after('vgs_product_code_6_vs');
        });
        Schema::table('book_slots', function (Blueprint $table) {
            $table->mediumText('special_notes')->nullable()->after('vgs_product_codes');
        });
        Schema::table('programs', function (Blueprint $table) {
            $table->string('vgs_product_code_daily_ns')->nullable()->after('vgs_product_code_daily');
            $table->string('vgs_product_code_daily_vs')->nullable()->after('vgs_product_code_daily_ns');
            $table->string('vgs_product_code_daily_vns')->nullable()->after('vgs_product_code_daily_vs');

            $table->string('vgs_product_code_daily_6_ns')->nullable()->after('vgs_product_code_daily_6');
            $table->string('vgs_product_code_daily_6_vs')->nullable()->after('vgs_product_code_daily_6_ns');
            $table->string('vgs_product_code_daily_6_vns')->nullable()->after('vgs_product_code_daily_6_vs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('slots', function (Blueprint $table) {
            $table->dropColumn(['vgs_product_code_ns', 'vgs_product_code_vs', 'vgs_product_code_vns', 'vgs_product_code_6_ns', 'vgs_product_code_6_vs', 'vgs_product_code_6_vns']);
        });
        Schema::table('book_slots', function (Blueprint $table) {
            $table->dropColumn(['special_notes']);
        });
        Schema::table('programs', function (Blueprint $table) {
            $table->dropColumn(['vgs_product_code_daily_ns', 'vgs_product_code_daily_vs', 'vgs_product_code_daily_vns', 'vgs_product_code_daily_6_ns', 'vgs_product_code_daily_6_vs', 'vgs_product_code_daily_6_vns']);
        });
    }

}
