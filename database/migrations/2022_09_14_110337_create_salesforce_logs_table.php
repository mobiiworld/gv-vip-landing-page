<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesforceLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salesforce_logs', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('type',20)->nullable()->comment("eg: vip_activation");
            $table->string('refenrence_id',50)->nullable()->comment("if type = vip_activation then id of packs table");
            $table->text('response')->nullable();
            $table->uuid('user_id')->nullable()->comment("if type = vip_activation, id of users table");;
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('sales_force_ref_id',50)->nullable()->after('designa_customer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salesforce_logs');

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['sales_force_ref_id']);
        });
    }
}
