<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUrlToFitnessImages extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('fitness_seasons_images', function (Blueprint $table) {
            $table->mediumText('url')->nullable()->after('image');
            $table->char('type', 50)->default('image')->after('url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('fitness_seasons_images', function (Blueprint $table) {
            $table->dropColumn(['url', 'type']);
        });
    }

}
