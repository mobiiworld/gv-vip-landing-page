<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seasons', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('name')->nullable();
            $table->integer('season_number')->nullable()->unique();
            $table->integer('ordering')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
        });

        Schema::table('packs', function (Blueprint $table) {
            $table->uuid('season_id')->nullable()->index()->after('id');
            $table->foreign('season_id')->references('id')->on('seasons')->onDelete('cascade');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->string('drupal_user_id')->after('pack_id')->index()->nullable();
            $table->uuid('season_id')->nullable()->index()->after('drupal_user_id');
            $table->foreign('season_id')->references('id')->on('seasons')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seasons');
    }
}
