<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVipActivationStatusToSeasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seasons', function (Blueprint $table) {
            $table->boolean('pack_activation_enabled')->default(1)->after('status')->comment('pack_activation_enabled : 0 - disabled, 1 -  enabled');
            $table->string('pack_activation_disbaled_msg_en',255)->nullable()->after('pack_activation_enabled');
            $table->string('pack_activation_disbaled_msg_ar',255)->nullable()->after('pack_activation_disbaled_msg_en');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seasons', function (Blueprint $table) {
            $table->dropColumn(['pack_activation_enabled', 'pack_activation_disbaled_msg_en', 'pack_activation_disbaled_msg_ar']);
        });
    }
}
