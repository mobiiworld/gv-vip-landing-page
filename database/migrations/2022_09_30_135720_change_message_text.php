<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeMessageText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('seasons', function (Blueprint $table) {
            $table->dropColumn(['pack_activation_disbaled_msg_en', 'pack_activation_disbaled_msg_ar']);            

        });
        Schema::table('seasons', function (Blueprint $table) {            
            $table->text('pack_activation_disbaled_msg_en')->nullable()->after('pack_activation_enabled');
            $table->text('pack_activation_disbaled_msg_ar')->nullable()->after('pack_activation_disbaled_msg_en');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
