<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclePlocationServices extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('vehicle_plocation_services', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('vehicle_plocation_id', 36);
            $table->foreign('vehicle_plocation_id')->references('id')->on('vehicle_plocations')->onDelete('cascade');
            $table->bigInteger('parking_service_id')->unsigned()->nullable();
            $table->foreign('parking_service_id')->references('id')->on('parking_services')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('vehicle_plocation_services');
    }

}
