<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class CabanaSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Fifa_create']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Fifa_read']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Fifa_update']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Fifa_delete']);
        
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Cabana_create']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Cabana_read']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Cabana_update']);

        \App\ApiConfiguration::whereIn('name', ['cabana_product_id', 'cabana_slot_rate', 'cabana_full_evening_rate'])->delete();

        \App\ApiConfiguration::firstOrCreate(
                ['name' => 'cabana_search_slot_product_id'],
                ['value' => '73B7C66A-1E34-803B-6096-01826D46872A']
        );
        \App\ApiConfiguration::firstOrCreate(
                ['name' => 'cabana_value_product_id'],
                ['value' => '73B7C66A-1E34-803B-6096-01826D46872A']
        );
        \App\ApiConfiguration::firstOrCreate(
                ['name' => 'cabana_anyday_product_id'],
                ['value' => '73B7C66A-1E34-803B-6096-01826D46872A']
        );
        \App\ApiConfiguration::firstOrCreate(
                ['name' => 'cabana_fullday_product_id'],
                ['value' => '73B7C66A-1E34-803B-6096-01826D46872A']
        );
        \App\ApiConfiguration::firstOrCreate(
                ['name' => 'cabana_vip_product_id'],
                ['value' => '73B7C66A-1E34-803B-6096-01826D46872A']
        );
        \App\ApiConfiguration::firstOrCreate(
                ['name' => 'cabana_fullday_rate'],
                ['value' => 500]
        );

        $cabana = App\CabanaDay::select('id')->first();
        if (empty($cabana)) {
            $begin = new \DateTime(date('Y-m-d'));
            $end = new \DateTime("2023-04-29");

            for ($i = $begin; $i <= $end; $i->modify('+1 day')) {
                $cabana = new App\CabanaDay();

                if (in_array(strtolower($begin->format('l')), ['friday', 'saturday'])) {
                    $endTime = '01:00:00';
                } else {
                    $endTime = '00:00:00';
                }

                $cabana->start_dt = $i->format("Y-m-d 16:00:00");
                $cabana->end_dt = date("Y-m-d {$endTime}", strtotime($cabana->start_dt . ' + 1 days'));
                $cabana->save();
            }
        }

        $cabanaProduct = App\CabanaProduct::select('id')->first();
        $a = 200;
        $v = 150;
        if (empty($cabanaProduct)) {
            for ($i = 1; $i <= 8; $i++) {
                $cabanaProduct = new App\CabanaProduct();
                $cabanaProduct->hours = $i;
                $cabanaProduct->weekend_price = $a;
                $cabanaProduct->workingday_price = $v;
                $cabanaProduct->allow_vip = ($i == 3) ? true : false;
                if ($cabanaProduct->allow_vip) {
                    $cabanaProduct->vip_prices = ["generic_price" => 500, "gold" => 500, "silver" => 500, "platinum" => 500];
                } else {
                    $cabanaProduct->vip_prices = ["generic_price" => null, "gold" => null, "silver" => null, "platinum" => null];
                }
                $cabanaProduct->save();
                $a = $a + 150;
                $v = $v + 150;
            }
        }
    }

}
