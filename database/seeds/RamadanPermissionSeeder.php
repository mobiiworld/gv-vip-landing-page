<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RamadanPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Ramadan Majlis_create']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Ramadan Majlis_read']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Ramadan Majlis_update']);
        

        $role = Role::where(['guard_name' => 'admin', 'name' => 'admin'])->first();
        $role->givePermissionTo('Ramadan Majlis_create');
        $role->givePermissionTo('Ramadan Majlis_read');
        $role->givePermissionTo('Ramadan Majlis_update');
    }
}
