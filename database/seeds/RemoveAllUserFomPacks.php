<?php

use Illuminate\Database\Seeder;

class RemoveAllUserFomPacks extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$season = \App\Season::where('status',1)->first();
    	$seasonId = (!empty($season)) ?  $season->id : NULL;
        \App\User::whereNotNull('id')->where('season_id',$seasonId)->update(['pack_id' => NULL, 'account_id' => NULL]);
        \App\Pack::whereNotNull('id')->where('season_id',$seasonId)->update(['used' => 0, 'drupal_user_id' => NULL]);
    }
}
