<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PackPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'pack_create']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'pack_read']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'pack_update']);
    }
}
