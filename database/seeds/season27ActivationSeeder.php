<?php

use Illuminate\Database\Seeder;

class season27ActivationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        set_time_limit(0);
        Schema::disableForeignKeyConstraints();

        \App\VgsLog::truncate();
        \App\AuditLog::truncate();
        //\App\Pack::truncate();
        //\App\PackBlockHistory::truncate();
        //\App\PartnerOffer::truncate();
        //\App\Wonderpass::truncate();
        

        //\App\User::whereNotNull('pack_id')->update(['pack_id'=>NULL]);
        \App\Season::whereNotNull('id')->update(['status'=>0]);        
        $season = new \App\Season;
        $season->name = 'S27';
        $season->season_number = 27;
        $season->ordering = 3;
        $season->save();

        Schema::enableForeignKeyConstraints();
    }
}
