<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class Amseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\DynamicText::firstOrCreate([
            'slug' => 'web-activation-success-title',
            'text_en' => 'VIP pack Activated',
            'text_ar' =>  ' تم تفعيل باقة كبار الشخصيات  '
        ]);

        \App\DynamicText::firstOrCreate([
            'slug' => 'web-activation-success-message',
            'text_en' => '<p>Congratulations, :user_name on activating your :pack_type VIP pack!</p>
        <p>Your pack, identified with the number :pack_number, is now active.<p>
        <p>Now you can enjoy a more wonderful world in VIP style.</p>',
            'text_ar' => '<p>تهانينا  :user_name على تفعيل باقة كبار الشخصيات   :pack_type ذات الرقم  :pack_number </p><p>
الآن أصبح بإمكانكم الاستمتاع بعالم أكثر روعة على طريقة كبار الشخصيات.   </p>'
        ]);

        \App\DynamicText::firstOrCreate([
            'slug' => 'web-activation-success-image',
            'text_en' => asset('web/images/congrulation.svg'),
            'text_ar' =>  asset('web/images/congrulation.svg')
        ]);

        \App\DynamicText::firstOrCreate([
            'slug' => 'web-activation-success-button',
            'text_en' => 'Okay',
            'text_ar' =>  'موافقة‎',
        ]);
        \App\DynamicText::firstOrCreate([
            'slug' => 'mobile-activation-success-title',
            'text_en' => 'VIP pack Activated',
            'text_ar' =>  ' تم تفعيل باقة كبار الشخصيات  '
        ]);

        \App\DynamicText::firstOrCreate([
            'slug' => 'mobile-activation-success-message',
            'text_en' => 'Congratulations, :user_name on activating your :pack_type VIP pack!
        Your pack, identified with the number :pack_number, is now active.
        Now you can enjoy a more wonderful world in VIP style.',
            'text_ar' => 'تهانينا  :user_name على تفعيل باقة كبار الشخصيات   :pack_type ذات الرقم  :pack_number .
الآن أصبح بإمكانكم الاستمتاع بعالم أكثر روعة على طريقة كبار الشخصيات.   '
        ]);

        \App\DynamicText::firstOrCreate([
            'slug' => 'mobile-activation-success-image',
            'text_en' => asset('web/images/congrulation.png'),
            'text_ar' =>  asset('web/images/congrulation.png')
        ]);

        \App\DynamicText::firstOrCreate([
            'slug' => 'mobile-activation-success-button',
            'text_en' => 'Okay',
            'text_ar' =>  'موافقة‎',
        ]);

        // \App\Emailtemplate::firstOrCreate([
        //     'type' => 'otp',
        //     'subject_en' => 'OTP for Global Village',
        //     'subject_ar' => 'OTP for Global Village',
        //     'body_en' => 'Your OTP is :user_otp',
        //     'body_ar' => 'Your OTP is :user_otp',
        // ]);

        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'dynamic text_create']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'dynamic text_read']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'dynamic text_update']);

        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'removed plate prefix_create']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'removed plate prefix_read']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'removed plate prefix_delete']);

        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'email template_create']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'email template_read']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'email template_update']);
    }
}
