<?php

use Illuminate\Database\Seeder;
use App\Season;
use App\Pack;
use App\User;

class S26Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        set_time_limit(0);
        $season = new Season;
        $season->name = 'S25';
        $season->season_number = 25;
        $season->ordering = 1;
        $season->save();

        $seasonId = $season->id;

        //Pack::whereNotNull('id')->update(['season_id'=>$seasonId]);

        $packs = Pack::all();

        foreach($packs as $pack){
        	$pack->season_id = $seasonId;
        	$pack->save();

        	$user = User::where('pack_id',$pack->id)->first();
        	if(!empty($user)){
        		$user->drupal_user_id = $pack->drupal_user_id;
                $user->season_id = $seasonId;
        		$user->save();
        	}
        }
        User::whereNull('season_id')->update(['season_id'=>$seasonId]);
    }
}
