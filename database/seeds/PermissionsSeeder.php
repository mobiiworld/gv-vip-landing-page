<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'parking transaction_read']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'parking transaction_update']);

        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'slot_create']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'slot_read']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'slot_update']);

        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'fitness_create']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'fitness_read']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'fitness_update']);

        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'fitness_daily tracks']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'fitness_monthly tracks']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'fitness_season tracks']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'vehicle count_create']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'vehicle count_read']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'vehicle count_update']);
        App\VehiclePsetting::firstOrCreate(['id' => 1], ['api_interval' => 5, 'last_updated_at' => date('Y-m-d H:i:s')]);

        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'log_read']);

        /*Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'shake and win_create']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'shake and win_read']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'shake and win_update']);*/
        
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'notifications_create']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'notifications_read']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'notifications_update']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'notifications_delete']);
        
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Ramadan Majlis_change table']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Ramadan Majlis_change booking date']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Ramadan Majlis_find bookings']);
    }

}
