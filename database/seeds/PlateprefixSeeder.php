<?php

use Illuminate\Database\Seeder;
use App\CarCountry;
use App\Emirate;

class PlateprefixSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	CarCountry::whereNotNull('plate_prefix_new')->update(['plate_prefix_new'=>NULL]);
    	Emirate::whereNotNull('plate_prefix')->update(['plate_prefix'=>NULL]);
    //     $abu = array(1 => 1,
				// 		2 => 2,
				// 		4=>4,
				// 		5=>5,
				// 		6=>6,
				// 		7=>7,
				// 		8=>8,
				// 		9=>9,
				// 		10=>10,
				// 		11=>11,
				// 		12=>12,
				// 		13=>13,
				// 		14=>14,
				// 		15=>15,
				// 		16=>16,
				// 		17=>17,
				// 		50=>50,
				// 		'Blue'=>'Blue',
				// 		'Gray' =>'Gray',
				// 		'Green' =>'Green',
				// 		'Red' => 'Red'
				// );
        $abu = [];
       for($i  = 1; $i<100;$i++){
       	$abu[$i] = $i;
       }
       $abu['Blue'] = 'Blue';
       $abu['Gray'] = 'Gray';
       $abu['Green'] = 'Green';
       $abu['Red'] = 'Red';
        Emirate::where('code','AZ')->update(['plate_prefix'=>json_encode($abu)]);
        $dubai = array(
						'A' => 'A',
						'AA' => 'AA',
						'B'=>'B',
						'C'=>'C',
						'D'=>'D',
						'E'=>'E',
						'F'=>'F',
						'G' => 'G',
						'H' =>'H',
						'I'=>'I',
						'J'=>'J',
						'K'=>'K',
						'L'=>'L',
						'M'=>'M',
						'N'=>'N',
						'O'=>'O',
						'P'=>'P',
						'Q'=>'Q',
						'R'=>'R',
						'S'=>'S',
						'T'=>'T',
						'U'=>'U',
						'V'=>'V',
						'W'=>'W',
						'X'=>'X',
						'Y'=>'Y',
						'Z'=>'Z',
						'ابيض' => 'ابيض'
					);
       Emirate::where('code','DU')->update(['plate_prefix'=>json_encode($dubai)]);
       $sharj =  array(1 => 1,
						2 => 2,
						3 => 3,
						4=>4,
						5=>5,
						6=>6,
						7=>7,
						8=>8,
						9=>9,
						'Orange' => 'Orange',
						'White' =>'White'
					);
       Emirate::where('code','SH')->update(['plate_prefix'=>json_encode($sharj)]);
       $ajman = array(
       				'A' => 'A',
					'B'=>'B',
					'C'=>'C',
					'D'=>'D',
					'E'=>'E',
					'F'=>'F',
					'H' =>'H'
					);
      	Emirate::where('code','AJ')->update(['plate_prefix'=>json_encode($ajman)]);
       $umAl = array(
       		'A' => 'A',
			'B'=>'B',
			'C'=>'C',
			'D'=>'D',
			'E'=>'E',
			'F'=>'F',
			'G' => 'G',
			'H' =>'H',
			'I'=>'I',
			'J'=>'J',
			'White'=>'White',
			'X' =>'X'
       );
       Emirate::where('code','UQ')->update(['plate_prefix'=>json_encode($umAl)]);
       $rak = array(
       	'A' => 'A',
		'B'=>'B',
		'C'=>'C',
		'D'=>'D',
		'I'=>'I',
		'K'=>'K',
		'M'=>'m',
		'N'=>'N',
		'RAK-Tower'=>'RAK-Tower',
		'S'=>'S',
		'V'=>'V',
		'White'=>'White',
		'X'=>'X',
		'Y'=>'Y'
       );
       Emirate::where('code','RK')->update(['plate_prefix'=>json_encode($rak)]);

       $fuj = array(
       	'A' => 'A',
		'B'=>'B',
		'C'=>'C',
		'D'=>'D',
		'E'=>'E',
		'F'=>'F',
		'G' => 'G',
		'I'=>'I',
		'K'=>'K',
		'L'=>'L',
		'M'=>'M',
		'P'=>'P',
		'R'=>'R',
		'S'=>'S',
		'T'=>'T',
		'V'=>'V',
		'White'=>'White',
		'X' => 'X'
       );

       Emirate::where('code','FU')->update(['plate_prefix'=>json_encode($fuj)]);

       //countries

       $oman = array(
       	'Political Body' => 'Political Body',
		'Red' =>'Red' ,
		'Yellow' =>'Yellow' 
	);
       CarCountry::where('code','OM')->update(['plate_prefix_new'=>json_encode($oman)]);
       $kuwait = [];
       for($i  = 1; $i<100;$i++){
       	$kuwait[$i] = $i;
       }
       $kuwait['Al Asima'] = 'Al Asima';
       CarCountry::where('code','KWT')->update(['plate_prefix_new'=>json_encode($kuwait)]);
       $bahrain = array(
       	'Orange'=>'Orange',
		'White'=>'White',
		'Yellow' =>'Yellow'
       );
       CarCountry::where('code','BRN')->update(['plate_prefix_new'=>json_encode($bahrain)]);
       $ksa = array(
       		'White'=>'White'
       );
       CarCountry::where('code','KSA')->update(['plate_prefix_new'=>json_encode($ksa)]);

       $qatar = array(
       	1=>1,
		2=>2,
		'Black'=>'Black',
		'Green'=>'Green',
		'White'=>'White',
		'White Silver'=>'White Silver'
       );
       $qtr = CarCountry::where('code','QA')->first();
       if(empty($qtr)){
       	$qtr = new CarCountry;
       	$qtr->code = 'QA';
       }
       $qtr->name_en = 'QATAR';
       $qtr->name_ar = 'دولة قطر';
       $qtr->plate_prefix = 'A to Z';
       $qtr->plate_prefix_new = json_encode($qatar);
       $qtr->save();

    }
}
