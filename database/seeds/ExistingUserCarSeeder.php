<?php

use Illuminate\Database\Seeder;

class ExistingUserCarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userCars = App\UserCar::all(); 
        if(!empty($userCars)){
            foreach($userCars as $car){
                $drupal_user_id = trim($car->drupal_user_id);
                if(empty($drupal_user_id) && !empty($car->user_id)){
                    $user = \App\User::find($car->user_id);
                    if(!empty($user)){
                        $drupal_user_id = trim($user->drupal_user_id);
                    }                    
                }
                if(empty($drupal_user_id)){
                    continue;
                }
                $type = (!empty($car->type)) ? $car->type : 1;
                $emirate_id = (!empty($car->emirate_id)) ? $car->emirate_id : NULL;
                \App\NewUserCar::firstOrCreate([
                    'drupal_user_id' => $drupal_user_id,
                    'type' => $type,
                    'country_id' => $car->country_id,
                    'emirate_id' => $emirate_id,
                    //'plate_category_id' => $car->plate_category_id,
                    'plate_prefix' => trim($car->plate_prefix),
                    'plate_number' => trim($car->plate_number),
                ]);


                
            }
        }
    }
}
