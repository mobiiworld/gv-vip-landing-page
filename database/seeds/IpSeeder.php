<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class IpSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Ramadan Majlis_booking']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Ramadan Majlis_cancel table']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Ramadan Majlis_release table']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Ramadan Majlis_booking summary']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Ramadan Majlis_onsite collection']);
        
        Permission::where('name','Ramadan Majlis_create')->update(['name'=>'Ramadan Majlis_table create']);
        Permission::where('name','Ramadan Majlis_read')->update(['name'=>'Ramadan Majlis_table read']);
        Permission::where('name','Ramadan Majlis_update')->update(['name'=>'Ramadan Majlis_table update']);
        
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Ramadan Majlis_slot create']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Ramadan Majlis_slot read']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Ramadan Majlis_slot update']);
        
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Ramadan Majlis_program create']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Ramadan Majlis_program read']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Ramadan Majlis_program update']);
        
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Ramadan Majlis_change table']);
        Permission::firstOrCreate(['guard_name' => 'admin', 'name' => 'Ramadan Majlis_change booking date']);
        
        \App\ApiConfiguration::firstOrCreate(
                ['name' => 'payment_method_card_admin_id'],
                ['value' => '9115BB97-9B72-3D29-7A0D-016D791328DD']
        );
        \App\ApiConfiguration::firstOrCreate(
                ['name' => 'payment_method_card_admin_id'],
                ['value' => '9115BB97-9B72-3D29-7A0D-016D791328DD']
        );
        \App\ApiConfiguration::firstOrCreate(
                ['name' => 'order_doc_template_id'],
                ['value' => 'A8C6CE55-3223-D492-1DEC-01753B85DB41']
        );
    }

}
