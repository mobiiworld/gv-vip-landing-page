<?php

use Illuminate\Database\Seeder;

class ConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = array(
        	'work_station_id' => 'EB830644-577C-C4D3-4275-016E7EED58DE',
        	'code_alias_type_code' => 'VIP-PACKNO',
        	'car_code_alias_type_code' => 'VIPcarplate',
        	'meta_data_first_name' => 'FT1',
        	'meta_data_last_name' => 'FT3',
        	'meta_data_age_group' => 'AgeGroup',
        	'meta_data_gender' => 'FT19',
        	'meta_data_nationality' => 'NtnltyTxt',
        	'meta_data_mobile' => 'MOBNO',
        	'meta_data_email' => 'FT21',
        	'meta_data_keep_updated' => 'keepupdated',
        	'meta_data_terms' => 'termsagree',
        	'account_entity_type'=>15,
        	'save_code_alias_entity_type'=>21,
        	'sale_account_type'=>2,
            'designa_card_type' => 5,
            'designa_carpark_id' => 21,
            'designa_group_id' => 2,
            'designa_application_id' => 1,
            'designa_user' => 'webserviceuser03',
            'designa_password' => '6Pw@dw!N',
            'designa_card_carrier_kind_id' => 9,
            'designa_expiration_date' => '2020-10-25',
            'car_code_alias_type_code2' => 'VIPcarplate2',
            'valid_pack_limit' => 10000,
            'clear_user_parkings_cron' => 'enable',
        );
        foreach($array as $key=>$value){
        	//\App\ApiConfiguration::create(['name'=>$key,'value'=>$value]);
            \App\ApiConfiguration::firstOrCreate(
                ['name' => $key],
                ['value' =>$value]
            );
        }
    }
}
