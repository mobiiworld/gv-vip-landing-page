<?php

use Illuminate\Database\Seeder;

class PackProductConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       /* $preProduction = array(
			 array('ProductId' => 'B313650D-FA13-92B9-4D7C-0179EF483BAA', 'Product Name' => 'VIP General Admission Complimentary', 'Product' => 'Entry Ticket'),
			 array('ProductId' => 'E0B901B0-D10F-D6B2-4336-0179E597CE53', 'Product Name' => 'VIP General Admission Diamond', 'Product' => 'Entry Ticket'),
			 array('ProductId' => '03C7CBAD-3C77-E0CC-496C-0179EF4675EF', 'Product Name' => 'VIP General Admission Gold', 'Product' => 'Entry Ticket'),
			 array('ProductId' => '0F393EDD-787B-AEE8-4F14-0179EF4899AD', 'Product Name' => 'VIP General Admission Mini', 'Product' => 'Entry Ticket'),
			 array('ProductId' => '57E82DB6-F496-515F-4A8A-0179EF470103', 'Product Name' => 'VIP General Admission Platinum', 'Product' => 'Entry Ticket'),
			 array('ProductId' => '4CF1E88F-0558-BFFD-34DD-0179E584C68B', 'Product Name' => 'VIP General Admission Private', 'Product' => 'Entry Ticket'),
			 array('ProductId' => '2A90ED2C-9F27-DD8C-4BE6-0179EF476D0F', 'Product Name' => 'VIP General Admission Silver', 'Product' => 'Entry Ticket'),
			 array('ProductId' => '12563EBD-BF3C-F2F2-594C-0179EF5160E5', 'Product Name' => 'VIP Wonderpass Complimentary', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => 'CB8BAE5D-7499-04D0-44DD-0179E598B96A', 'Product Name' => 'VIP Wonderpass Diamond', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => '11471506-F2FC-CC90-563A-0179EF503F5E', 'Product Name' => 'VIP Wonderpass Gold', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => '9820E673-175D-BE89-5B31-0179EF5200E6', 'Product Name' => 'VIP Wonderpass Mini', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => 'CB7E354D-3E27-7170-52A9-0179EF4DF068', 'Product Name' => 'VIP Wonderpass Platinum', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => '467ADF99-66A9-EDA5-36EC-0179E58BAF85', 'Product Name' => 'VIP Wonderpass Private', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => 'D80B9952-558B-D1F1-5767-0179EF50CF87', 'Product Name' => 'VIP Wonderpass Silver', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => '410D06FA-E607-5FE3-74B4-0179EF9A607C', 'Product Name' => 'VIP Parking Voucher Complimentary', 'Product' => 'Parking Voucher'),
			array('ProductId' => '3D845D05-FF0F-9917-4444-0179E59857CC', 'Product Name' => 'VIP Parking Voucher Diamond', 'Product' => 'Parking Voucher'),
			 array('ProductId' => '01B1FCE1-D849-3BE8-6FE8-0179EF982911', 'Product Name' => 'VIP Parking Voucher Gold', 'Product' => 'Parking Voucher'),
			 array('ProductId' => '9985EF1D-102E-4E05-7614-0179EF9ADF6A', 'Product Name' => 'VIP Parking Voucher Mini', 'Product' => 'Parking Voucher'),
			array('ProductId' => '1C8E5248-AB5C-664D-7180-0179EF98ED1D', 'Product Name' => 'VIP Parking Voucher Platinum', 'Product' => 'Parking Voucher'),
			 array('ProductId' => 'A72A4F43-633C-2E82-3A49-0179E58F58B0', 'Product Name' => 'VIP Parking Voucher Private', 'Product' => 'Parking Voucher'),
			 array('ProductId' => '9A97D026-6B06-516D-7318-0179EF99E8EC', 'Product Name' => 'VIP Parking Voucher Silver', 'Product' => 'Parking Voucher'),
		); */

		$production = array(
			array('ProductId' => 'FD74E385-F699-0482-1751-017B4995D1A8', 'Product Name' => 'VIP General Admission Private', 'Product' => 'Entry Ticket'),
			array('ProductId' => '931A8B4B-C63D-D86E-18F6-017B49976D1F', 'Product Name' => 'VIP General Admission Diamond', 'Product' => 'Entry Ticket'),
			 array('ProductId' => '483973E4-9E0E-05AC-1A8B-017B49982064', 'Product Name' => 'VIP General Admission Platinum', 'Product' => 'Entry Ticket'),
			 array('ProductId' => '2C0F5657-CFFD-FCD0-1C20-017B4998ACC6', 'Product Name' => 'VIP General Admission Gold', 'Product' => 'Entry Ticket'),
			array('ProductId' => '7F81B080-9EB3-852B-1D3F-017B49993AD8', 'Product Name' => 'VIP General Admission Silver', 'Product' => 'Entry Ticket'),
			 array('ProductId' => 'FD0771B3-1D11-9593-1ED4-017B4999C400', 'Product Name' => 'VIP General Admission Complimentary', 'Product' => 'Entry Ticket'),
			array('ProductId' => 'B0B8FE65-53A1-9EB8-1FF3-017B499A4D0F', 'Product Name' => 'VIP General Admission Mini', 'Product' => 'Entry Ticket'),
			array('ProductId' => '25990A0F-6DD7-FB4F-2782-017B49A21A64', 'Product Name' => 'VIP Wonderpass Complimentary', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => 'E0A93083-D603-DC93-22FE-017B499F3F46', 'Product Name' => 'VIP Wonderpass Diamond', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => 'F3AA4626-435A-22F5-2560-017B49A0B4A7', 'Product Name' => 'VIP Wonderpass Gold', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => 'DAEBF064-FC40-99E1-2929-017B49A2DBE7', 'Product Name' => 'VIP Wonderpass Mini', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => '6CD73C30-00A3-D73D-242F-017B499FF87A', 'Product Name' => 'VIP Wonderpass Platinum', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => 'F4A8D865-1299-B22F-218A-017B499BF9F8', 'Product Name' => 'VIP Wonderpass Private', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => '013D7E62-0436-D38F-2691-017B49A15BC8', 'Product Name' => 'VIP Wonderpass Silver', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => '65BAB5A3-4CDF-254E-48FD-017B49CE4D0F', 'Product Name' => 'VIP Parking Voucher Complimentary', 'Product' => 'Parking Voucher'),
			 array('ProductId' => 'C550E1FA-8D83-F3FF-2C3C-017B49A6ED1F', 'Product Name' => 'VIP Parking Voucher Diamond', 'Product' => 'Parking Voucher'),
			 array('ProductId' => 'BD4AA96B-24D8-AC43-2EBE-017B49A80B60', 'Product Name' => 'VIP Parking Voucher Gold', 'Product' => 'Parking Voucher'),
			 array('ProductId' => 'C04763AF-C69B-8E34-4A23-017B49CECDA8', 'Product Name' => 'VIP Parking Voucher Mini', 'Product' => 'Parking Voucher'),
			 array('ProductId' => 'B4C7CBDE-69DF-E6AC-2DD4-017B49A784B3', 'Product Name' => 'VIP Parking Voucher Platinum', 'Product' => 'Parking Voucher'),
			 array('ProductId' => 'A849BDFD-A52B-FBEC-2AD0-017B49A5A58E', 'Product Name' => 'VIP Parking Voucher Private', 'Product' => 'Parking Voucher'),
			 array('ProductId' => '64829AF6-788C-72C1-2FE4-017B49A8A529', 'Product Name' => 'VIP Parking Voucher Silver', 'Product' => 'Parking Voucher'),
		);

		//S27
		$preProduction = array(
			 array('ProductId' => 'D53EB3CB-41B2-03E1-48E6-0182581E6702', 'Product Name' => 'VIP Parking Voucher Private', 'Product' => 'Parking Voucher'),
			 array('ProductId' => 'B0EFC72B-7A92-B81C-39EE-01825818CD98', 'Product Name' => 'VIP Wonderpass Private', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => '0AE7F2B0-1638-CD28-2D2A-01825811FD9A', 'Product Name' => 'VIP General Admission Private', 'Product' => 'Entry Ticket'),
			 array('ProductId' => 'F48C93C1-C547-EC26-400A-0182581C351B', 'Product Name' => 'VIP Parking Voucher Diamond', 'Product' => 'Parking Voucher'),
			 array('ProductId' => '090FEF3F-5014-F05F-32EF-0182581690B7', 'Product Name' => 'VIP Wonderpass Diamond', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => 'E806086F-6E6C-76AC-2556-0182580FA248', 'Product Name' => 'VIP General Admission Diamond', 'Product' => 'Entry Ticket'),
			 array('ProductId' => '056364A1-EBBD-C286-4DFF-0182581FDF24', 'Product Name' => 'VIP Parking Voucher Platinum', 'Product' => 'Parking Voucher'),
			 array('ProductId' => '6D5CCFA5-4931-FCA2-37C9-018258183540', 'Product Name' => 'VIP Wonderpass Platinum', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => 'A4E91D1D-2DA6-C7FE-2B35-018258117801', 'Product Name' => 'VIP General Admission Platinum', 'Product' => 'Entry Ticket'),
			 array('ProductId' => '5CD060C8-49DB-F8A3-3DFD-0182581BA0D0', 'Product Name' => 'VIP Parking Voucher Complimentary', 'Product' => 'Parking Voucher'),
			 array('ProductId' => 'BBA2011F-A23C-BD18-315E-01825815B688', 'Product Name' => 'VIP Wonderpass Complimentary', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => '32138BBD-C285-F479-235A-0182580EE77C', 'Product Name' => 'VIP General Admission Complimentary', 'Product' => 'Entry Ticket'),
			 array('ProductId' => '728B7720-CC25-2839-4290-0182581CBD8C', 'Product Name' => 'VIP Parking Voucher Gold', 'Product' => 'Parking Voucher'),
			 array('ProductId' => '57C2475F-3B3D-7730-34F3-01825817269D', 'Product Name' => 'VIP Wonderpass Gold', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => '03452679-F0B1-98A4-274C-018258105F21', 'Product Name' => 'VIP General Admission Gold', 'Product' => 'Entry Ticket'),
			 array('ProductId' => '1BB2EFBC-816D-5C59-4BE9-0182581EFA8C', 'Product Name' => 'VIP Parking Voucher Silver', 'Product' => 'Parking Voucher'),
			 array('ProductId' => '198AFC1F-B1F7-E632-3C3F-018258197029', 'Product Name' => 'VIP Wonderpass Silver', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => 'EDB8348E-D5AD-2AB3-2F1F-0182581283B2', 'Product Name' => 'VIP General Admission Silver', 'Product' => 'Entry Ticket'),
			 array('ProductId' => '6DBCB00D-B705-6508-44EE-0182581D48F8', 'Product Name' => 'VIP Parking Voucher Mini', 'Product' => 'Parking Voucher'),
			 array('ProductId' => 'BF868557-F5FD-F90A-35D2-01825817AEE3', 'Product Name' => 'VIP Wonderpass Mini', 'Product' => 'VIP Wonder Pass'),
			 array('ProductId' => '6A7D1F03-E564-BD7A-293F-01825810F898', 'Product Name' => 'VIP General Admission Mini', 'Product' => 'Entry Ticket')
			
			);

		$configArray = [
			'Entry Ticket' => 'entry_ticket',
			'VIP Wonder Pass' => 'vip_wonder_pass',
			'Parking Voucher' => 'parking_voucher'
		];
		$seedArray = (env('PRODUCT_ID_SEEDER_ENV') == 'production') ? $production  : $preProduction;
		foreach ($seedArray as $key => $value) {
			$prduct = new \App\VgsProductConfig;
			$prduct->slug = $configArray[$value['Product']];
			$prduct->product = $value['Product'];
			$prduct->product_name = $value['Product Name'];
			$prduct->product_id = $value['ProductId'];
			$prduct->save();
		}
    }
}
