<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSnwTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('snw_campaigns', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->json('names')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->boolean('status')->default(0)->comment('0:in-active, 1:active');
            $table->timestamps();
        });
        Schema::create('snw_prizes', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('campaign_id');
            $table->foreign('campaign_id')->references('id')->on('snw_campaigns')->onDelete('cascade');
            $table->json('names')->nullable();
            $table->json('descriptions')->nullable();
            $table->string('eloqua_form')->nullable();
            $table->boolean('status')->default(1)->comment('0:in-active, 1:active');
            $table->timestamps();
        });
        Schema::create('snw_prize_coupons', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('campaign_id');
            $table->foreign('campaign_id')->references('id')->on('snw_campaigns')->onDelete('cascade');
            $table->uuid('prize_id');
            $table->foreign('prize_id')->references('id')->on('snw_prizes')->onDelete('cascade');
            $table->string('coupon_code', 100)->nullable();
            $table->boolean('status')->default(1)->comment('0:not used, 1:used');
            $table->timestamps();
        });
        Schema::create('snw_day_images', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('campaign_id');
            $table->foreign('campaign_id')->references('id')->on('snw_campaigns')->onDelete('cascade');
            $table->date('campaign_day')->nullable();
            $table->string('image')->nullable();
            $table->char('language', 5)->default('en');
            $table->timestamps();
        });
        Schema::create('snw_users', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('campaign_id');
            $table->foreign('campaign_id')->references('id')->on('snw_campaigns')->onDelete('cascade');
            $table->integer('drupal_id')->nullable();
            $table->string('fname')->nullable();
            $table->string('lname')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile')->nullable();
            $table->timestamps();
        });
        Schema::create('snw_daily_winners', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('snw_user_id');
            $table->foreign('snw_user_id')->references('id')->on('snw_users')->onDelete('cascade');
            $table->uuid('coupon_id');
            $table->foreign('coupon_id')->references('id')->on('snw_prize_coupons')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('snw_campaigns');
        Schema::dropIfExists('snw_prizes');
        Schema::dropIfExists('snw_prize_coupons');
        Schema::dropIfExists('snw_day_images');
        Schema::dropIfExists('snw_users');
        Schema::dropIfExists('snw_daily_winners');
    }

}
