$(document).ready(function () {
    // $("#Cancel").click(function () {
    //     $(".action-button1").show();
    //     $(".second-step").hide();
    //     car_activation_allowed = 0;
    //     addedCar = 1;
    // });

    // For Tooltip
    $('[data-toggle="tooltip"]').tooltip();
});

// For Slick Position
$('.modal').on('shown.bs.modal', function (e) {
    $('.three-thumbnails').slick('setPosition');
})

$(document).on('click', 'a.copy-promo', function (e) {
    var copyText = $(this).parent().find('input:hidden');
    copyText.attr("type", "text").select();
    document.execCommand('copy');
    copyText.attr("type", "hidden");
    swal(copiedText);
});
