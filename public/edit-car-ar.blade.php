
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html  xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" style="width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;">
 <head>
  <meta charset="UTF-8">
   <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta name="x-apple-disable-message-reformatting">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="telephone=no" name="format-detection">
  <title>headline</title>
  <!--[if (mso 16)]>
    <style type="text/css">
    a {text-decoration: none;}
    </style>
    <![endif]-->
  <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->
  <!--[if gte mso 9]>
<xml>
    <o:OfficeDocumentSettings>
    <o:AllowPNG></o:AllowPNG>
    <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
</xml>
<![endif]-->

  <style type="text/css">
    @import url('https://fonts.googleapis.com/css2?family=Cairo:wght@400;700&display=swap');

    [style*="'Cairo', sans-serif"] {
    font-family: 'Cairo', sans-serif;
}

p {
margin-block-start: 0.5em;
margin-block-end: 0.5em;
}
#outlook a {
	padding:0;
}
.ExternalClass {
	width:100%;
}
.ExternalClass,
.ExternalClass p,
.ExternalClass span,
.ExternalClass font,
.ExternalClass td,
.ExternalClass div {
	line-height:100%;
}
.es-button {
	mso-style-priority:100!important;
	text-decoration:none!important;
}
a[x-apple-data-detectors] {
	color:inherit!important;
	text-decoration:none!important;
	font-size:inherit!important;
	font-family:inherit!important;
	font-weight:inherit!important;
	line-height:inherit!important;
}
.es-desk-hidden {
	display:none;
	float:left;
	overflow:hidden;
	width:0;
	max-height:0;
	line-height:0;
	mso-hide:all;
}
@media only screen and (max-width:600px) {p, ul li, ol li, a { font-size:11px!important; line-height:150%!important } h1 { font-size:23px!important; text-align:center; line-height:120%!important } h2 { font-size:14px!important; text-align:center; line-height:120%!important } h3 { font-size:11px!important; text-align:center; line-height:120%!important } h1 a { font-size:23px!important } h2 a { font-size:14px!important } h3 a { font-size:11px!important } .es-menu td a { font-size:16px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:11px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class="gmail-fix"] { display:none!important } .es-m-txt-c,
.es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:inline-block!important } a.es-button { font-size:16px!important; display:inline-block!important } .es-btn-fw { border-width:10px 0px!important; text-align:center!important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important }
.es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } tr.es-desk-hidden, td.es-desk-hidden, table.es-desk-hidden { width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } tr.es-desk-hidden { display:table-row!important } table.es-desk-hidden { display:table!important } td.es-desk-menu-hidden { display:table-cell!important } .es-menu td { width:1%!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } }
</style>
 </head>
 <body style="width:100%;font-family: 'Cairo', sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#ffffff; padding:0;Margin:0">
  <div class="es-wrapper-color" style="background-color:#F0F0F0">
   <!--[if gte mso 9]>
			<v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
				<v:fill type="tile" color="#f0f0f0"></v:fill>
			</v:background>
		<![endif]-->
   <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top">
     <tr style="border-collapse:collapse">
      <td  valign="top" style="padding:0;Margin:0">






          <table class="es-content" cellspacing="0" cellpadding="0" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
         <tbody><tr style="border-collapse:collapse">
          <td align="center" style="padding:0;Margin:0">
           <table class="es-content-body" cellspacing="0" cellpadding="0" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px">
             <tbody><tr style="border-collapse:collapse">
              <td align="left" bgcolor="#ffffff" style="Margin:0;padding-top:10px;padding-bottom:15px;padding-left:20px;padding-right:20px;background-color:#FFFFFF">
               <!--[if mso]><table style="width:560px" cellpadding="0" cellspacing="0"><tr><td style="width:270px" valign="top"><![endif]-->
               <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
                 <tbody><tr style="border-collapse:collapse">
                  <td align="left" style="padding:0;Margin:0;width:270px">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                     <tbody><tr style="border-collapse:collapse">
                       <td  dir="rtl" class="es-m-txt-c" align="left" style="padding:0;Margin:0;padding-left:10px;padding-right:10px"></td>
                   </tbody></table></td>
                 </tr>
               </tbody></table>
               <!--[if mso]></td><td style="width:20px"></td><td style="width:270px" valign="top"><![endif]-->
               <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right">
                 <tbody><tr style="border-collapse:collapse">
                  <td align="left" style="padding:0;Margin:0;width:270px">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                     <tbody><tr style="border-collapse:collapse">
                      <td  dir="ltr" class="es-m-txt-c" align="right" style="padding:0;Margin:0;padding-left:10px;padding-right:10px"></td>
                     </tr>
                   </tbody></table></td>
                 </tr>
               </tbody></table>
               <!--[if mso]></td></tr></table><![endif]--></td>
             </tr>
             <tr style="border-collapse:collapse">
              <td align="left" style="padding:0;Margin:0;padding-bottom:10px;padding-left:20px;padding-right:20px">
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                 <tbody><tr style="border-collapse:collapse">
                  <td align="center" valign="top" style="padding:0;Margin:0;width:560px">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                     <tbody><tr style="border-collapse:collapse">
                      <td align="center" style="padding:0;Margin:0;font-size:0px"><img src="https://images.visit.globalvillage.ae/EloquaImages/clients/DubaiHoldingLLC/%7B46e93750-6841-49d9-948c-5db05db86f71%7D_Silver_Jubilee_Logo_Hor_360x100px.png" alt="" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="278" class="adapt-img"></td>
                     </tr>
                   </tbody></table></td>
                 </tr>
               </tbody></table></td>
             </tr>
           </tbody></table></td>
         </tr>
       </tbody></table>




        <!-- Block Main Highlight--> <table align="center" cellpadding="0" cellspacing="0" class="es-content" dir="rtl" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
	<tbody>
		<tr style="border-collapse:collapse">
			<td align="center" style="padding:0;Margin:0">
			<table align="center" cellpadding="0" cellspacing="0" class="es-content-body" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"><!--Main content-->
				<tbody>
					<tr style="border-collapse:collapse">
						<td align="left" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"><!-- Always on Highlight-->
						<table cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px" width="100%">
							<tbody>
								<tr style="border-collapse:collapse">
									<td align="center" style="padding:0;Margin:0;width:600px" valign="top">
									<table cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px" width="100%">
										<tbody>
											<tr style="border-collapse:collapse">
												<td align="center" style="padding:0;Margin:0;font-size:0px"><img alt="" class="adapt-img" src="{{url('images/email-banner.jpg')}}" width="600" /></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr style="border-collapse:collapse">
						<td align="left" bgcolor="#ffffff" style="padding:0;Margin:0;padding-top:20px;background-color:#FFFFFF">
						<table cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px" width="100%">
							<tbody>
								<tr style="border-collapse:collapse">
									<td align="center" style="padding:0;Margin:0;width:600px" valign="top">
									<table cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px" width="100%">
										<tbody>
											<tr style="border-collapse:collapse">
												<td align="right" dir="rtl" style="padding:10px 30px;Margin:0;">
												<h1 style="Margin:0;line-height:30px;mso-line-height-rule:exactly;font-family:'Cairo', sans-serif;font-size:25px;font-style:normal;font-weight:normal;color:#333333">Car Details updated successfully</h1>
											</tr>
											<tr style="border-collapse:collapse">
												<td align="right" dir="rtl" style="padding:0 30px;Margin:0;">
												  <p>Please find the details below,</p>
												  <p>Country of car registration: {{$data['carCountry']}}</p>
												  <p>Emirate of car registration: {{$data['carEmirate']}}</p>
												  <p>Plate Code: {{$data['platePrefix']}}</p>
												  <p>Plate Number: {{$data['plateNumber']}}</p>
												  <p></p>
												  <p></p>
												</td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>

<table align="center" cellpadding="0" cellspacing="0" class="es-content" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
	<tbody>
		<tr style="border-collapse:collapse">
			<td align="center" style="padding:0;Margin:0">
			<table align="center" bgcolor="#ffffff" cellpadding="0" cellspacing="0" class="es-content-body" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px">
				<tbody>
					<tr style="border-collapse:collapse">
						<td align="left" bgcolor="#ffffff" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;background-color:#FFFFFF">
						<table cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px" width="100%">
							<tbody>
								<tr style="border-collapse:collapse">
									<td align="center" style="padding:0;Margin:0;width:600px" valign="top">
									<table cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px" width="100%">
										<tbody>
											<tr style="border-collapse:collapse">
												<td dir="rtl" align="center" style="Margin:0;padding-top:10px;padding-left:10px;padding-right:10px;padding-bottom:20px"><!--[if mso]><a dir="rtl" href="#" target="_blank">
	<v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" stripoVmlButton href="#"
                style="height:44px;v-text-anchor:middle;width:153px;" arcsize="7%" stroke="f" fillcolor="#d7182a">
		<w:anchorlock></w:anchorlock>
		<center style='color:#ffffff;font-family:Times New Roman, Arial;font-size:16px;font-weight:700;direction:rtl;'>تعرف على المزيد</center>
	</v:roundrect></a>
<![endif]--><!--[if !mso]><!-- --><!--<![endif]--></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
 <!-- EO Block-->



          <table class="es-content" cellspacing="0" cellpadding="0" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
         <tbody><tr style="border-collapse:collapse">
          <td align="center" style="padding:0;Margin:0">
           <table class="es-content-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px">
             <tbody><tr style="border-collapse:collapse">
              <td align="left" bgcolor="#333333" style="padding:0;Margin:0;padding-top:10px;padding-left:20px;padding-right:20px;background-color:#333333">
               <!--[if mso]><table style="width:560px" cellpadding="0" cellspacing="0"><tr><td style="width:116px" valign="top"><![endif]-->
               <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
                 <tbody><tr style="border-collapse:collapse">
                  <td class="es-m-p0r es-m-p20b" align="center" style="padding:0;Margin:0;width:96px">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                     <tbody><tr style="border-collapse:collapse">
                     <td dir="rtl" align="center" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'Cairo', sans-serif;line-height:21px;color:#FFFFFF"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'Cairo', sans-serif;font-size:14px;text-decoration:none;color:#FFFFFF" href="https://www.globalvillage.ae/ar/plan-your-visit?elqTrackId=e68821232547458a9e5d51b25ffc1c73">خططوا لزيارة القرية العالمية</a></p></td>
                     </tr>
                   </tbody></table></td>
                  <td class="es-hidden" style="padding:0;Margin:0;width:20px"></td>
                 </tr>
               </tbody></table>
               <!--[if mso]></td><td style="width:116px" valign="top"><![endif]-->
               <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
                 <tbody><tr style="border-collapse:collapse">
                  <td class="es-m-p20b" align="center" style="padding:0;Margin:0;width:96px">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                     <tbody><tr style="border-collapse:collapse">
                      <td dir="rtl" align="center" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'Cairo', sans-serif;line-height:21px;color:#FFFFFF"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'Cairo', sans-serif;font-size:14px;text-decoration:none;color:#FFFFFF" href="https://www.globalvillage.ae/ar/pavilions-selfie-spots?elqTrackId=ef9a5e4ab91b455dbf3126d9b6ec0460">الأجنحة والمناطق الخلابة</a></p></td>
                     </tr>
                   </tbody></table></td>
                  <td class="es-hidden" style="padding:0;Margin:0;width:20px"></td>
                 </tr>
               </tbody></table>
               <!--[if mso]></td><td style="width:116px" valign="top"><![endif]-->
               <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
                 <tbody><tr style="border-collapse:collapse">
                  <td class="es-m-p20b" align="center" style="padding:0;Margin:0;width:96px">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                     <tbody><tr style="border-collapse:collapse">
                      <td dir="rtl" align="center" class="es-m-p0t" style="padding:0;Margin:0;padding-top:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'Cairo', sans-serif;line-height:21px;color:#FFFFFF"><a  target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'Cairo', sans-serif;font-size:14px;text-decoration:none;color:#FFFFFF;" href="https://www.globalvillage.ae/ar/dining?elqTrackId=db2ccdfbed5d44dcb107bead3e67ce35">تذوقوا أشهى النكهات</a></p></td>
                     </tr>
                   </tbody></table></td>
                  <td class="es-hidden" style="padding:0;Margin:0;width:20px"></td>
                 </tr>
               </tbody></table>
               <!--[if mso]></td><td style="width:96px" valign="top"><![endif]-->
               <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
                 <tbody><tr style="border-collapse:collapse">
                  <td align="center" class="es-m-p20b" style="padding:0;Margin:0;width:96px">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                     <tbody><tr style="border-collapse:collapse">
                        <td dir="rtl" align="center" class="es-m-p0t" style="padding:0;Margin:0;padding-top:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'Cairo', sans-serif;line-height:21px;color:#FFFFFF"><a href="https://www.globalvillage.ae/ar/shows-events?elqTrackId=ebc2feb2e3df4a73b4868e22dc3cdeee" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'Cairo', sans-serif;font-size:14px;text-decoration:none;color:#FFFFFF">العروض والفعاليات</a></p></td>
                     </tr>
                   </tbody></table></td>
                 </tr>
               </tbody></table>
               <!--[if mso]></td><td style="width:20px"></td><td style="width:95px" valign="top"><![endif]-->
               <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right">
                 <tbody><tr style="border-collapse:collapse">
                  <td align="left" style="padding:0;Margin:0;width:95px">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                     <tbody><tr style="border-collapse:collapse">
                      <td dir="rtl" align="center" class="es-m-p0t" style="padding:0;Margin:0;padding-top:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'Cairo', sans-serif;line-height:21px;color:#FFFFFF"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'Cairo', sans-serif;font-size:14px;text-decoration:none;color:#FFFFFF" href="https://www.globalvillage.ae/ar/carnaval?elqTrackId=b27aff1c49cb43628cfc8f24723f36e2">كرنفال</a></p></td>
                     </tr>
                   </tbody></table></td>
                 </tr>
               </tbody></table>
               <!--[if mso]></td></tr></table><![endif]--></td>
             </tr>
           </tbody></table></td>
         </tr>
       </tbody></table>


       <table align="center" cellpadding="0" cellspacing="0" class="es-footer" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:#f0f0f0;background-repeat:repeat;background-position:center top;">
	<tbody>
		<tr style="border-collapse:collapse;">
			<td align="center" bgcolor="#f0f0f0" style="padding:0;Margin:0;background-color:#f0f0f0;">
			<table align="center" bgcolor="#f0f0f0" cellpadding="0" cellspacing="0" class="es-footer-body" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#333333;" width="600">
				<tbody>
					<tr style="border-collapse:collapse;">
						<td align="left" style="Margin:0;padding-bottom:5px;padding-top:20px;padding-left:20px;padding-right:20px;">
						<table cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;" width="100%">
							<tbody>
								<tr style="border-collapse:collapse;">
									<td align="center" style="padding:0;Margin:0;" valign="top" width="560">
									<table cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;" width="100%">
										<tbody>
											<tr style="border-collapse:collapse;">
												<td align="center" bgcolor="transparent" class="es-m-txt-c" style="padding:0;Margin:0;font-size:0px;background-color:transparent;">
												<table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
													<tbody>
														<tr style="border-collapse:collapse;">
															<td align="center" style="padding:0;Margin:0;padding-right:10px;" valign="top"><a data-targettype="webpage" href="https://www.facebook.com/GlobalVillageAE/?elqTrackId=f3e0fe521b574a6f8d24fa989fbef859" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'Cairo', sans-serif;font-size:13px;text-decoration:none;color:#FFFFFF;" target="_blank"><img alt="Fb" src="https://img04.en25.com/EloquaImages/clients/DubaiHoldingLLC/%7Bb8027b40-06fd-44aa-989a-c62657479f26%7D_facebook-rounded-white.png" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" title="Facebook" width="32" /></a></td>
															<td align="center" style="padding:0;Margin:0;padding-right:10px;" valign="top"><a href="https://twitter.com/GlobalVillageAE?elqTrackId=8719c965a0954c15acbb526556f79a85" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'Cairo', sans-serif;font-size:13px;text-decoration:none;color:#FFFFFF;" target="_blank"><img alt="Tw" src="https://img04.en25.com/EloquaImages/clients/DubaiHoldingLLC/%7Bcaf6d6a2-e6a4-497e-ad99-2b14cd5d4eb0%7D_twitter-rounded-white.png" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" title="Twitter" width="32" /></a></td>
															<td align="center" style="padding:0;Margin:0;padding-right:10px;" valign="top"><a href="https://www.instagram.com/globalvillageuae/?elqTrackId=c1136896ba4e4aad9693d7b0eee11652" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'Cairo', sans-serif;font-size:13px;text-decoration:none;color:#FFFFFF;" target="_blank"><img alt="Inst" src="https://img04.en25.com/EloquaImages/clients/DubaiHoldingLLC/%7Bbbe5002b-4d72-451d-aecb-d186a86f7673%7D_instagram-rounded-white.png" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" title="Instagram" width="32" /></a></td>
															<td align="center" style="padding:0;Margin:0;padding-right:10px;" valign="top"><a href="https://www.youtube.com/user/GlobalVillageAE?elqTrackId=4ed590584c774af5b87d423f24e9fc2f" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'Cairo', sans-serif;font-size:13px;text-decoration:none;color:#FFFFFF;" target="_blank"><img alt="Yb" src="https://img04.en25.com/EloquaImages/clients/DubaiHoldingLLC/%7Bdc009bb0-bb92-4ddc-96f4-a2495461792b%7D_youtube-rounded-white.png" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" title="Youtube" width="32" /></a></td>
															<td align="center" style="padding:0;Margin:0;padding-right:10px;" valign="top"><a href="https://www.snapchat.com/add/globalvillageme?elqTrackId=ad9c331cb7de4f60b6f4b6b590254d23" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'Cairo', sans-serif;font-size:13px;text-decoration:none;color:#FFFFFF;" target="_blank"><img alt="Tw" src="https://img04.en25.com/EloquaImages/clients/DubaiHoldingLLC/%7B4dcd5584-389a-4d2c-9e72-ecf5fc4ee49f%7D_snapchat-rounded-white.png" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" title="SnapChat" width="32" /></a></td>
															<td align="center" style="padding:0;Margin:0;padding-right:10px;" valign="top"><a href="https://www.linkedin.com/company/global-village-dubai/?elqTrackId=68a8ba2c578e4f3b92d2ea4a17171e11" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'Cairo', sans-serif;font-size:13px;text-decoration:none;color:#FFFFFF;" target="_blank"><img alt="Inst" src="https://img04.en25.com/EloquaImages/clients/DubaiHoldingLLC/%7B51609dee-d673-4bb5-a174-06af5a6347e1%7D_linkedin-rounded-white.png" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" title="LinkedIn" width="32" /></a></td>
															<td align="center" style="padding:0;Margin:0;padding-right:10px;" valign="top"><a href="https://wa.me/97143624114?elqTrackId=72444fd8bced4aaeabc93e6f91261b94" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'Cairo', sans-serif;font-size:13px;text-decoration:none;color:#FFFFFF;" target="_blank"><img alt="Inst" src="https://images.visit.globalvillage.ae/EloquaImages/clients/DubaiHoldingLLC/%7B1709efdd-9b10-4fd4-90a6-cd7a99c9b56b%7D_whatsapp-rounded-white.png" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" title="WhatsAapp" width="32" /></a></td>
														</tr>
													</tbody>
												</table>
												</td>
											</tr>
											<tr style="border-collapse:collapse;">
												<td align="center" class="es-m-txt-c" style="Margin:0;padding-top:15px;padding-bottom:5px;padding-left:5px;padding-right:5px;">
												<p dir="rtl" style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:13px;font-family:'Cairo', sans-serif;line-height:20px;color:#FFFFFF;"><strong><a class="view" data-targettype="sysaction" href="#" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'Cairo', sans-serif;font-size:13px;text-decoration:none;color:#FFFFFF;" target="_blank" title="نسخة الويب">نسخة الويب</a>&nbsp;|<span style="color:#ffffff;">&nbsp;</span><span style="color:#ffffff;">&nbsp;</span> <a class="view" data-targettype="sysaction" href="#" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'Cairo', sans-serif;font-size:13px;text-decoration:none;color:#FFFFFF;" target="_blank" title="Web version"> إلغاء الاشتراك </a> </strong></p>
												</td>
											</tr>
											<tr style="border-collapse:collapse;">
												<td align="center" dir="rtl" style="padding:0;Margin:0;padding-bottom:5px;padding-top:5px;">
												<h3 style="Margin:0;line-height:13px;mso-line-height-rule:exactly;font-family:'Cairo', sans-serif;font-size:11px;font-style:normal;font-weight:normal;color:#FFFFFF">&copy; 2020 القرية العالمية، جميع الحقوق محفوظة ✦ تقع القرية العالمية عند مخرج 37 في شارع الشيخ محمد بن زايد وشارع الإمارات في دبي.</h3>
												</td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>

        </td>
     </tr>
   </table>
  </div>
 </body>
</html>
